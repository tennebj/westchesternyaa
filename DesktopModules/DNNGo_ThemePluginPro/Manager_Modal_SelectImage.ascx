﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Manager_Modal_SelectImage.ascx.cs" Inherits="DNNGo.Modules.ThemePluginPro.Manager_Modal_SelectImage" %>
<div class="tabbable">
    <ul id="SelectImageTabs" class="nav nav-tabs tab-padding tab-space-3 tab-blue">
        <li class="active">
            <a href="#panel_tab_list" data-toggle="tab">Media Library </a>
        </li>
        <li style="<% =DemoDisplay %>">
            <a href="#panel_tab_uploader" data-toggle="tab">Upload Files </a>
        </li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane in active" id="panel_tab_list">
            <div class="sidebar-search list-search-image">
                <div class="form-group">
                    <input type="text" placeholder="Search Images">
                    <a href="javascript:;" class="submit_but">
                        <i class="clip-search-3"></i>
                    </a>
                </div>
            </div>
            <div class="templates-list SelectImageList"></div>
            <ul id="templates-paginator"></ul>
        </div>
        <div class="tab-pane" id="panel_tab_uploader"  style="<% =DemoDisplay %>">
            <div id="dropzone" action="<%=ModulePath %>Resource_jQueryFileUpload.ashx?<%=QueryString %>" method="post" class="dropzone" enctype="multipart/form-data"></div>
        </div>
    </div>
</div>
<script id="scriptImage" type="text/x-jquery-tmpl">
     <div class="gallery-img">
        <div class="wrap-image" style="background-image:url('${ThumbnailUrl}')">
            <a class="group1 cboxElement" href="javascript:;" title="${Name}.${Extension}" >
                <img src="${ThumbnailUrl}" alt="" class="img-responsive" data-url="${FileUrl}">
            </a>
              <%if (Multiple) { %>
             <div class="chkbox"></div>
             <% } else { %>
            <div class="radiobox"></div>
             <% }%>
            <div class="tools tools-bottom" style="bottom: 0;">
                <a href="javascript:;" title="${Name}.${Extension}">${Name}.${Extension}
                </a>
            </div>
        </div>
    </div>
</script>
 
    

<script type="text/javascript">
    var PagesGallery = function () {
        //function to Image Picker
        var runImagePicker = function () {
            $('body').on('click', ".wrap-image", function () {
                <%if (!Multiple) { %>
                $('.wrap-image').removeClass('selected').children('a').children('img').removeClass('selected');
                <% }%>
               
                if ($(this).hasClass('selected')) {
                    $(this).removeClass('selected').children('a').children('img').removeClass('selected');
                } else {
                    $(this).addClass('selected').children('a').children('img').addClass('selected');
                }
            });
        };

        return {
            //main function to initiate template pages
            init: function () {
                runImagePicker();
            }
        };
    }();



    var Image_Loader = function (index, ispage) {
        $.getJSON(Module.ModulePath + "Resource_Service.aspx?Token=PictureList", { ModuleId: Module.ModuleId, TabId: Module.TabId, PortalId: Module.PortalId, PageIndex: index, PageSize: 40,FilterFileType: "<% =FilterFileType %>", Search: $(".list-search-image input").val() }, function (data) {
            var Pages = 0;
            $(".templates-list .gallery-img").detach();
            $.each(data, function (i, item) {
                $("#scriptImage").tmpl(item).appendTo($(".templates-list"));
                Pages = item.Pages;
            });
            if (ispage) {
                Bind_Paginator(Pages);
            }

        });
    };
    var Bind_Paginator = function (pages) {
        $("#templates-paginator li").detach();
        if (pages > 0) {
            $("#templates-paginator").bootstrapPaginator({
            bootstrapMajorVersion: 3, currentPage: 1, totalPages: pages,
            onPageChanged: function (e, oldPage, newPage) {
                Image_Loader(newPage, false);
            }
        });
        }
        
    };
 

    $(document).ready(function () {
        Dropzone.options.dropzone = {
            maxFilesize: <%=DotNetNuke.Common.Utilities.Config.GetMaxUploadSize() /1024 / 1024 %>,
            acceptedFiles:"<%=DotNetNuke.Entities.Host.Host.AllowedExtensionWhitelist.ToDisplayString().Replace("*.",".") %>",
            init: function () {
                this.on("queuecomplete", function (data) {
                    Image_Loader(1, true);
                    //上传后刷新列表的回调
                    $('#SelectImageTabs a[href="#panel_tab_list"]').tab('show');
                });
            }
        };

        $(".submit_but").click(function () {
            Image_Loader(1, true);
        });


        Image_Loader(1, true);

        PagesGallery.init();


        $('body').on('click', ".wrap-image", function () {
            var urls = '';
            $("img.img-responsive[class*='selected']").each(function (i) {
                var url = $(this).data("url");
                if (url !== undefined && url != '')
                {
                    if (urls != '') {
                        urls += ",";
                    }
                    urls += url;
                }

            });

        //    $('body').data('urls', urls);

            if (<%=Mode%> == 0) {
                $(parent.document).find("#<%=ReturnControlID%>").attr("data-urls", urls);
            }
            parentCurr.jQuery("#imgaesTEXTURL").val(urls).siblings(".img").css("background-image","url('"+urls+"')");
        });

        var parentCurr = parent;
        parentCurr.jQuery("#MediaAddImages").click(function () {
            var urls = parentCurr.jQuery("#imgaesTEXTURL");
          //  if (urls !== '') {
                parentCurr.applyMediaImage(urls);
                parentCurr.jQuery("#DnnMediaBox").fadeOut();
          //  }
        })




    });
</script>
