﻿//Layout 
data_basic.designs.push({
'thumbnail': 'layout/col-12.jpg',
'category': '999',
'html':`<div class="is-section is-box">
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container is-builder ui-sortable">
                <div class="row">
                    <div class="col-md-12">
                        <div class="is-builder ui-sortable"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>`
},
{
'thumbnail': 'layout/col-3-9.jpg',
'category': '999',
'html':`<div class="is-section is-box">
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container is-builder ui-sortable">
                <div class="row">
                    <div class="col-md-3">
                        <div class="is-builder ui-sortable"></div>
                    </div>
                    <div class="col-md-9">
                        <div class="is-builder ui-sortable"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>`
},
{
'thumbnail': 'layout/col-4-8.jpg',
'category': '999',
'html':`<div class="is-section is-box">
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container is-builder ui-sortable">
                <div class="row">
                    <div class="col-md-4">
                        <div class="is-builder ui-sortable"></div>
                    </div>
                    <div class="col-md-8">
                        <div class="is-builder ui-sortable"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>`
},
{
'thumbnail': 'layout/col-5-7.jpg',
'category': '999',
'html':`<div class="is-section is-box">
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container is-builder ui-sortable">
                <div class="row">
                    <div class="col-md-5">
                        <div class="is-builder ui-sortable"></div>
                    </div>
                    <div class="col-md-7">
                        <div class="is-builder ui-sortable"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>`
},
{
'thumbnail': 'layout/col-6-6.jpg',
'category': '999',
'html':`<div class="is-section is-box">
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container is-builder ui-sortable">
                <div class="row">
                    <div class="col-md-6">
                        <div class="is-builder ui-sortable"></div>
                    </div>
                    <div class="col-md-6">
                        <div class="is-builder ui-sortable"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>`
},
{
'thumbnail': 'layout/col-7-5.jpg',
'category': '999',
'html':`<div class="is-section is-box">
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container is-builder ui-sortable">
                <div class="row">
                    <div class="col-md-7">
                        <div class="is-builder ui-sortable"></div>
                    </div>
                    <div class="col-md-5">
                        <div class="is-builder ui-sortable"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>`
},
{
'thumbnail': 'layout/col-8-4.jpg',
'category': '999',
'html':`<div class="is-section is-box">
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container is-builder ui-sortable">
                <div class="row">
                    <div class="col-md-8">
                        <div class="is-builder ui-sortable"></div>
                    </div>
                    <div class="col-md-4">
                        <div class="is-builder ui-sortable"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>`
},
{
'thumbnail': 'layout/col-9-3.jpg',
'category': '999',
'html':`<div class="is-section is-box">
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container is-builder ui-sortable">
                <div class="row">
                    <div class="col-md-9">
                        <div class="is-builder ui-sortable"></div>
                    </div>
                    <div class="col-md-3">
                        <div class="is-builder ui-sortable"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>`
},
{
'thumbnail': 'layout/col-4-4-4.jpg',
'category': '999',
'html':`<div class="is-section is-box">
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container is-builder ui-sortable">
                <div class="row">
                    <div class="col-md-4">
                        <div class="is-builder ui-sortable"></div>
                    </div>
                    <div class="col-md-4">
                        <div class="is-builder ui-sortable"></div>
                    </div>
                    <div class="col-md-4">
                        <div class="is-builder ui-sortable"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>`
},
{
'thumbnail': 'layout/col-3-6-3.jpg',
'category': '999',
'html':`<div class="is-section is-box">
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container is-builder ui-sortable">
                <div class="row">
                    <div class="col-md-3">
                        <div class="is-builder ui-sortable"></div>
                    </div>
                    <div class="col-md-6">
                        <div class="is-builder ui-sortable"></div>
                    </div>
                    <div class="col-md-3">
                        <div class="is-builder ui-sortable"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>`
},
{
'thumbnail': 'layout/col-3-3-3-3.jpg',
'category': '999',
'html':`<div class="is-section is-box">
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container is-builder ui-sortable">
                <div class="row">
                    <div class="col-md-3">
                        <div class="is-builder ui-sortable"></div>
                    </div>
                    <div class="col-md-3">
                        <div class="is-builder ui-sortable"></div>
                    </div>
                    <div class="col-md-3">
                        <div class="is-builder ui-sortable"></div>
                    </div>
                    <div class="col-md-3">
                        <div class="is-builder ui-sortable"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>`
});


