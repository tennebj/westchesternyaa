﻿/*dnn*/

jQuery(document).ready(function ($) {

	function HasGoogleFont(g) {
		var HTML = '';
		$(".d-wrapper").each(function () {
			HTML += $(this).html();
		})
		if (HTML) {
			g.forEach(function (value, i) {
				if (HTML.indexOf(value.replace("%20", " ")) == -1) {
					g.splice(i, 1);
				}
			})
		}
		return g;
	}

	function HascontentFont(g) {
		for (var key in g) {
			if (!$("." + key).length) {
				delete g[key];
			}
		}
		return g;
	}


	var googleFont = [];
	var contentFont = {};
	$("head link").each(function () {
		if (this.href.indexOf("googleapis") != -1) {
			if ($(this).attr("id") != "contentbox-google") {
				googleFontLink(this.href.split("family=")[1].split("&")[0]);
			} else {
				googleFont.push(this.href.split("family=")[1].split("&")[0]);
			}
		} else if ($(this).data("name") == "contentstyle") {
			contentFont[$(this).data("class")] = this.outerHTML;
		}
	})



	googleFont = googleFont.join("|").split("|");
	googleFont = googleFont.filter(function (s) {
		return s && s.trim();
	});

	googleFont = HasGoogleFont(googleFont);
	contentFont = HascontentFont(contentFont);


	function googleFontLink(g) {
		if (g) {
			$("head").append('<style type="text/css" class="googleapis-style">@import url(\'//fonts.googleapis.com/css?family=' + g + '\')</style>');
		}
	}
	googleFont.forEach(function (v, i) {
		googleFontLink(v);
	})

	function contentFontLink(g) {
		if (g) {
			$("head").append('<style type="text/css" class="contentfont-style">@import url(\'' + $(g).attr("href") + '\')</style>');
		}
	}
	for (var key in contentFont) {
		contentFontLink(contentFont[key]);
	}


	var lastIndex = 0,
		lastlength = 0;

	//Enable editing

	$(".d-wrapper").each(function (index) {
		var e = $(this);
		var index = index;
		var $isbox = '';
		e.data("index", index);
		e.find(".d-placeholder").remove();
		e.append('<div class="d-placeholder"><div><span>Click to add content</span></div></div>');
		e.on("click focus", function () {
			$(".d-wrapper").removeClass("is-wrapper");
			e.addClass("is-wrapper").siblings().removeClass("is-wrapper").blur();

			if (!e.data('contentbox')) {
				e.contentbox({
					framework: 'bootstrap',
					//   disableStaticSection:true,
					modulePath: SkinPath + 'Resource/vendor/content-builder/assets/modules/',
					designPath: SkinPath + 'Resource/vendor/content-builder/assets/designs/',
					contentStylePath: SkinPath + 'Resource/vendor/content-builder/assets/styles/',
					iconselect: SkinPath + 'Resource/vendor/content-builder/assets/ionicons/icons.html',
					snippetData: SkinPath + 'Resource/vendor/content-builder/assets/minimalist-blocks/snippetlist.html',
					coverImageHandler: SaveImageServiceUrl + e.data("mid"),
					/* for uploading section background */
					largerImageHandler: SaveImageServiceUrlByLarger + e.data("mid"),
					/* for uploading larger image */
					moduleConfig: [{
						"moduleSaveImageHandler": SaveImageServiceUrlBySlider + e.data("mid"),
						/* for module purpose image saving (ex. slider) */
					}],
					onRender: function () {
					},
					onChange: function () {

						var timeoutId;
						clearTimeout(timeoutId);
						var id = e.data("mid");
						timeoutId = setTimeout(function () {
							e.find("link").each(function () {

								if (this.href.indexOf("googleapis") != -1) {
									var f = this.href.split("family=")[1].split("&")[0];
									if (f && googleFont.indexOf(f) == -1) {
										googleFont.push(f);
										googleFontLink(f);
									}
								} else if ($(this).data("name") == "contentstyle") {


									var name = $(this).data("class");
									if (!contentFont[name]) {
										contentFont[name] = this.outerHTML;
										contentFontLink(contentFont[name]);
									}
								}

							})
					
							saveImages(id);
						}, 1000);
						if (lastIndex == 0 || lastIndex == lastlength) {
							$("#ContentBuilderSaveHTML").removeAttr("disabled");
						}
					},
					useSidebar: true,
					enableContentStyle: true
				})
				if (index != 0) {
					$("#divCb > svg ~ svg").remove();
					$(".is-sidebar").prevAll(".is-sidebar").remove();
					$(".is-modal.delsectionconfirm ~ .is-modal.delsectionconfirm").remove();
					$(".is-modal.editsection ~ .is-modal.editsection").remove();
					$(".is-modal.editmodule ~ .is-modal.editmodule").remove();
					$(".is-modal.editcustomcode ~ .is-modal.editcustomcode").remove();
					$(".is-modal.editbox ~ .is-modal.editbox").remove();
					$(".is-modal.gradientcolor ~ .is-modal.gradientcolor").remove();
					$(".is-modal.customcolor ~ .is-modal.customcolor").remove();
					$(".is-modal.viewfullhtml ~ .is-modal.viewfullhtml").remove();
					$(".is-modal.pickphoto ~ .is-modal.pickphoto").remove();
					$(".is-modal.applytypography ~ .is-modal.applytypography").remove();
					$(".is-modal.addsection ~ .is-modal.addsection").remove();
					$("#divSidebarSections ~ #divSidebarSections").remove();
					$("#divSidebarSnippets ~ #divSidebarSnippets").remove();
					$("#divSidebarSource ~ #divSidebarSource").remove();
					$("#divSidebarTypography ~ #divSidebarTypography").remove();
					$("#divCustomSidebarArea1 ~ #divCustomSidebarArea1").remove();
					$("#divCustomSidebarArea2 ~ #divCustomSidebarArea2").remove();
				} else {
					e.click();
				}
			}
		}).on("blur blurin", function () {
			//  saveHTML(e);
		})
	})
	$(".d-wrapper").removeClass("is-wrapper");
	$(".d-wrapper").eq(0).addClass("is-wrapper").click();


	$(".d-placeholder").addClass("row-add-initial").on("click", function () {
		$(this).parent().focus();
		$('.is-sidebar-button[data-content="divSidebarSections"]').click();
	})

	$(".applytypography .input-ok.classic").removeClass("input-ok").addClass("dnn-input-ok");

	$(".applytypography .dnn-input-ok").on("click", function () {
		var val = $('input[name=rdoApplyTypoStyle]:checked').val();
		var link = '';
		jQuery('[data-rel="_del"]').remove();

		if (val == 1) {
			for (var key in contentFont) {
				if (jQuery(".d-wrapper").find('.' + key).length > 0) {
					jQuery(".d-wrapper").find('.' + key).removeClass(key);
				}
			}
			$('link[data-name = "contentstyle"]').remove();			
			$(".contentfont-style").remove();
			contentFont = {};
		} 
		var links = document.getElementsByTagName("link");
		for (var i = 0; i < links.length; i++) {
			var src = links[i].href.toLowerCase();
			if (src.indexOf('basetype-') != -1) {
				jQuery(links[i]).attr('data-rel', '_del')
			}
		}

		if (DNNapplyTypography.css != '') {
			link = '<link href="' + jQuery(".is-wrapper").data('contentbox').settings.contentStylePath + DNNapplyTypography.css + '" rel="stylesheet">';
			jQuery('head').append(link);
		}
	
		jQuery(".is-wrapper").data('contentbox').hideModal(jQuery('.is-modal.applytypography'));
		jQuery(".d-wrapper").each(function(){
			if($(this).data('contentbox')){
				$(this).data('contentbox').settings.onChange();
			}
		})

		return false;
	})


	jQuery(".d-wrapper").on('mouseenter mouseleave',".is-box", function (e) {
		$isbox = jQuery(this);
	});


	var fileEmbedImage =$('<button type="button" id="fileEmbedImageButton" style="position:absolute;top:0px;left:0;width:100%;height:50px;opacity: 0;cursor: pointer;"> </button>')
	$("#divImageTool #fileEmbedImage").hide().after(fileEmbedImage);
	fileEmbedImage.on("click",function(){
	 $("#DnnMediaBox").attr("type","image").fadeIn().find("iframe").attr("src",getMediaUrl({mimid:$(".is-wrapper").data("mid")}));
	})

	var fileImage =$('<button type="button" id="fileImageButton" style="position:absolute;top:-30px;left:0;width:100%;height:80px;opacity: 0;cursor: pointer;"> </button>')
	$("#form-upload-larger #fileImage").hide().after(fileImage);
	fileImage.on("click",function(){
	 $("#DnnMediaBox").attr("type","link").fadeIn().find("iframe").attr("src",getMediaUrl({mimid:$(".is-wrapper").data("mid")}));
	})
	var fileCover =$('<button type="button" id="fileCoverButton" style="position:absolute;top:0px;left:0;width:100%;height:50px;opacity: 0;cursor: pointer;"> </button>')
	$("#divboxtool #fileCover").hide().after(fileCover);
	fileCover.on("click",function(){
		$isbox.addClass("current-cover-bg");
	 	$("#DnnMediaBox").attr("type","cover").fadeIn().find("iframe").attr("src",getMediaUrl({mimid:$(".is-wrapper").data("mid")}));
	})


	
	$('#divContentSize .cmd-box-size[data-value="380"]').before('<button class="cmd-box-full" data-value="full">Full</button><button class="cmd-box-auto" data-value="auto">Auto</button>')
	$('#divContentSize .cmd-box-size[data-value="800"]').before('<button class="cmd-box-size" data-value="640">640px</button>').after('<button class="cmd-box-size" data-value="970">970px</button><button class="cmd-box-size" data-value="980">980px</button><button class="cmd-box-size" data-value="1050">1050px</button><button class="cmd-box-size" data-value="1100">1100px</button><button class="cmd-box-size" data-value="1200">1200px</button>')
	
	$('#divContentSize .cmd-box-auto').on("click",function(){
		$isbox.find(".is-container").removeClass("is-content-380 is-content-500 is-content-640 is-content-800 is-content-970 is-content-980 is-content-1050 is-content-1100 is-content-1200 is-container-fluid").css("max-width","");
	})
	$('#divContentSize .cmd-box-full').on("click",function(){
		$isbox.find(".is-container").addClass("is-container-fluid").removeClass("is-content-380 is-content-500 is-content-640 is-content-800 is-content-970 is-content-980 is-content-1050 is-content-1100 is-content-1200").css("max-width","");
	})
	$('#divContentSize .cmd-box-size').on("mouseup",function(){
		$isbox.find(".is-container").removeClass("is-container-fluid")
	})

/* 	$("#divBoxContentContainer").append(`
		<div class="is-settings cmd-addclass-box" style="margin-bottom:0">
			<div>Add Class Name</div>
			<div>
				<button title="Decrease" class="cmd-addclass" data-value="aaaaaaa">aaaaaaa</button>
				<button title="Increase" class="cmd-addclass" data-value="bbbbb">bbbbb</button>
				<button title="Not Set" class="cmd-addclass" data-value="cccc">cccc</button>
			</div>
			<div>Add Class Name</div>
			<div>
				<button title="Decrease" class="cmd-addclass" data-value="ddd">ddd</button>
				<button title="Increase" class="cmd-addclass" data-value="eee">eee</button>
				<button title="Not Set" class="cmd-addclass" data-value="fff">fff</button>
			</div>
			</div>
	`) */

	$('.cmd-addclass-box .cmd-addclass').on("click",function(){
		$isbox.find(".is-container").addClass($(this).data("value"))
		$(this).siblings().each(function(){
			$isbox.find(".is-container").removeClass($(this).data("value"))
		})
	})


	$("#DnnMediaBox .close,#DnnMediaBox #MediaClose").on("click",function(){
		$("#DnnMediaBox").removeAttr("type").fadeOut();
		$("#MediaAddImages").off();
	})

	//$('#selSnippetCat option[value="120"]').after('<option value="800">Short Code</option>')





	// $(".d-wrapper").parents(".dnnSortable").removeClass("dnnSortable")

	if ($("#personaBar-iframe").length) {
		$("body").addClass("dnn-9")
	} else if ($("#ControlBar_ControlPanel").length) {
		$("body").addClass("dnn-8")
	} else {
		$("body").addClass("dnn-editor")
	}


	function unique1(arr){
	  var hash=[];
	  for (var i = 0; i < arr.length; i++) {
		 if(hash.indexOf(arr[i])==-1){
			var n = arr[i].split('data-class="')[1] ? arr[i].split('data-class="')[1].split('"')[0]:false;			
			if(!contentFont[n]){
				hash.push(arr[i]);
			}
		 }
	  }
	  return hash;
	}


	function saveHTML(e) {
		if (e.data('contentbox')) {



				
				e.find("link").each(function () {
					if (this.href.indexOf("googleapis") != -1 || $(this).data("name") == "contentstyle") { 
						$(this).remove();
					}
				});
			var sMainCss = '';
			var sSectionCss = e.data('contentbox').sectionCss();

			
			//var sSectionCss = '';
			googleFont.filter(d => d);

			if (e.data("index") == 0) {
				
				sMainCss = e.data('contentbox').mainCss();
				if (googleFont.length) {
					sMainCss += '<link href="https://fonts.googleapis.com/css?family=' + googleFont.join("|") + '" type="text/css" rel="stylesheet" id="contentbox-google" />';
				}
				for (var key in contentFont) {
					sMainCss += contentFont[key];
				}
			}
		
					
			if(sSectionCss){
				sSectionCss = unique1(sSectionCss.split("<link"));
				sSectionCss = sSectionCss.join("<link")
				
			}

		var sHTML = e.data('contentbox').html();

			var filter = $("<div>");
			filter.append(sHTML);
			filter.find("button.row-add,div.row-handle,button.row-up,button.row-down,button.row-duplicate,button.row-remove,.d-placeholder,.edit-mode-js").remove();

			filter.find('[data-module="shortcode"]').each(function(){
			
				$(this).attr('data-html','');
			})
			
			sHTML = filter.html();


			$.post(SaveContentServiceUrl + e.data("mid"), {
					Content: sHTML,
					MainCss: sMainCss,
					SectionCss: sSectionCss
				},
				function (data) {
					lastIndex++;

					if (lastIndex == lastlength) {
						setTimeout(function () {
							$('#ContentBuilderSaveHTML').removeClass('loading').addClass('success').delay(1000).queue(function () {
								$(this).removeClass('success').dequeue();
							});
						}, 2000)
					}
				}
			);
		}

	}

	$("#ContentBuilderSaveHTML").removeClass("hide").on("click", function () {
		googleFont = HasGoogleFont(googleFont);
		contentFont = HascontentFont(contentFont);

		$(this).addClass("loading").attr("disabled", "disabled");
		lastIndex = lastlength = 0;
		$(".d-wrapper").each(function () {
			if ($(this).data('contentbox')) {
				lastlength++;
			}
		})
		$(".d-wrapper").each(function (index) {
			saveHTML($(this));
		});

	});

	function saveImages(id) {
		$("body").saveimages({
			handler: CatImageServiceUrl + id,
			onComplete: function () {
				$(".d-wrapper").each(function (index) {
					//   saveHTML($(this));
				});
			}
		});
		$("body").data('saveimages').save();
	}

	jQuery(".is-wrapper").data("contentbox").settings.onRender();
	jQuery(".is-wrapper").data("contentbox").settings.onChange();
	
	window.onbeforeunload = function () {
		googleFont = HasGoogleFont(googleFont);
		contentFont = HascontentFont(contentFont);

		$(".d-wrapper").each(function (index) {
			//  saveHTML($(this));
		});
	}

	var snippetlistsrc = setInterval(function () {
		if ($("#divFb .is-modal.snippets iframe").length) {
			$("#divFb .is-modal.snippets iframe").attr("src", SkinPath + "Resource/vendor/content-builder/assets/minimalist-blocks/snippetlist.html");
			clearTimeout(snippetlistsrc);
		}
	}, 500)
});



function applyMediaImage(url){ 
	
	$("#MediaAddImages").off();
	if(url){
	var type =jQuery("#DnnMediaBox").attr("type");
		if(type=="image"){
			jQuery(".elm-active").attr("src",url);
			jQuery(".is-wrapper").data("contentbox").settings.onRender();
			jQuery(".is-wrapper").data("contentbox").settings.onChange();

		}if(type=="link"){
			jQuery('.is-modal.imagelink .input-link').val(url);	
		}else if(type=="cover"){

			var $activeBox = jQuery(".current-cover-bg");
			if($activeBox.find(".is-overlay").length == 0) {
				$activeBox.prepend('<div class="is-overlay"></div>')
			}
			var $overlay = $activeBox.find(".is-overlay");
			if($overlay.find(".is-overlay-bg").length == 0) {
				$overlay.prepend('<div class="is-overlay-bg" style="transform:scale(1.05)" data-bottom-top="transform:translateY(-120px) scale(1.05);" data-top-bottom="transform:translateY(120px) scale(1.05)"></div>');
				$overlay.find(".is-overlay-bg").prepend('<div class="is-overlay-color" style="opacity:0.1"></div>')
			}
			$activeBox.find(".is-overlay-bg").css("background-image",`url(${url})`).css("background-color","")
			$activeBox.removeClass("current-cover-bg");
			jQuery(".is-wrapper").data("contentbox").settings.onRender();
			jQuery(".is-wrapper").data("contentbox").settings.onChange();
		}else if(type=="modules"){
			$("#modulesImageVal").val(url).change();
		}
	}
}
function getMediaUrl(options){
	 
	return $(".is-wrapper").data("mediaurl");
}


