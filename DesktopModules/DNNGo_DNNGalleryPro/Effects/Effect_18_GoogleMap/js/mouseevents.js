(function($) {
	$.fn.mouseevents = function(options) {
		var defaults = {
			mousedown: function() {},
			mouseup: function() {},
			mousemove: function() {}
		}
		var o = $.extend(defaults, options);
		var e = $(this);
		e[0].addEventListener('touchstart', touch, false);
		e[0].addEventListener('touchmove', touch, false);
		e[0].addEventListener('touchend', touch, false);
		e[0].addEventListener('mousedown', touch, false);
		e[0].addEventListener('mouseup', touch, false);
		e[0].addEventListener('mousemove', touch, false);
		e[0].addEventListener('mouseleave', touch, false);
		var isdown = false,
		startX = 0,
		startY = 0,
		endX = 0,
		endY = 0;
		function touch(event) {
			var event = event || window.event;
			switch (event.type) {
			case "touchstart":
			case "mousedown":
				mouseevent = event.type == "mousedown" ? event: event.touches[0];
				isdown = true;
				o.mousedown();
				break;
			case "touchend":
			case "mouseup":
			case "mouseleave":
				if (isdown) {
					mouseevent = event.type != "touchend" ? event: event.changedTouches[0];
					isdown = false;
					o.mouseup();
				}
				break;
			case "touchmove":
			case "mousemove":
				if (isdown) {
					event.preventDefault();
					mouseevent = event.type == "mousemove" ? event: event.touches[0];
					o.mousemove();
				}
				break;
			}
		}
	}
})(jQuery);