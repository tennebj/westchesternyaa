(function($) {
	$.fn.history_ajax = function(options) {
		var defaults = {}
		var o = $.extend(defaults, options);
		var m = $(this),
			l = m.find(".history-main"),
			once = 0,
			lazyload = 0,
			container, ScrollMaxPage = 0,
			stops = true,
			amount = false,
			direction = "dr-left",
			htyYear = "0",
			wtop = $(window).scrollTop(),
			yearlength = m.find(".history-year").length;
		var LoadAjaxItems = function() {
				if (m.find(".history-item").length != 0) {
					m.data("loaddisplay", o.LoadDisplay)
				}
				m.addClass("ic-loading");
				m.GalleryPostService({
					callback: function(items, pages, IsEnd) {
						//console.log(items);
						var box = $("<div>");
						jQuery.each(items, function(i, item) {
							item["htyYear"] = new Date(item.StartTimeUS).format("yyyy") ;
							item["Time"] = new Date(item.StartTimeUS).format(o.dateformat) ;
							if (htyYear == item.htyYear) {
								item.displayYear = false;
								direction = direction == "dr-left" ? "dr-right" : "dr-left"
							} else {
								item.displayYear = true;
								direction = "dr-left"
							}
							htyYear = item.htyYear;
							item["direction"] = direction;
							box.append(jQuery("#AjaxSliders" + o.id).tmpl(item))
						});
						box.find(".history-year span").on("click", function() {
							var e = $(this);
							$("body,html").stop().animate({
								scrollTop: e.offset().top
							}, 500)
						}) ;
						l.append(box);
						box.find(".history-layers").slick({
							prevArrow: "<button type=\"button\" class=\"slick-prev\"><span class=\"fa fa-angle-left\"></span></button>",
							nextArrow: "<button type=\"button\" class=\"slick-next\"><span class=\"fa fa-angle-right\"></span></button>"
						});
						box.find(".hty-animat").each(function() {
							if ($(this).offset().top <= $(window).scrollTop() + $(window).height() * 0.9) {
								$(this).addClass("active")
							} else {
								var itm = $(this);
								$(window).scroll(function() {
									if(!itm.hasClass("active") && itm.offset().top <= $(window).scrollTop() + $(window).height() * 0.9){
										itm.addClass("active")
									}
								})
							}
						}) ;
						m.removeClass("ic-loading");
						if (IsEnd && once == "0") {
							once = 1;
							m.find(".history-more").hide();
							lazyload = 1;
						}
						ScrollMaxPage++;
						stops = true
					}
				})
			}
		LoadAjaxItems();
		m.find(".history-more").on("click", function() {
			if (stops) {
				stops = false;
				LoadAjaxItems()
			}
		}); 
		if (o.disrightarrow) {
			m.find(".history-next,.history-prev").on("click", function() {
				var aw = $(this),
					page = aw.data("page");
				if (page == "next") {
					wtop = $(window).scrollTop() + m.find(".history-year").height()
				} else {
					wtop = $(window).scrollTop()
				}
				yearlength = m.find(".history-year").length - 1;
				m.find(".history-year").each(function(i) {
					if ($(this).offset().top > wtop) {
						if (page == "next" || i == 0) {
							var e = $(this)
						} else {
							var e = m.find(".history-year").eq(i - 1)
						}
						$("body,html").stop().animate({
							scrollTop: e.offset().top - 10
						}, 500);
						return false
					} else {
						if (yearlength == i) {
							if (page == "next") {
								if (m.find(".history-more").css("display") != "none") {
									$("body,html").stop().animate({
										scrollTop: m.find(".history-more").offset().top - 10
									}, 500, function() {
										m.find(".history-more").click()
									})
								}
							} else {
								var e = $(this);
								$("body,html").stop().animate({
									scrollTop: e.offset().top - 10
								}, 500)
							}
							return false
						}
					}
				})
			}) ;
			
			$(window).scroll(function() {
				if (m.find(".history-year").length != 0) {
					if (m.find(".history-year").eq(0).offset().top + 10 >= $(window).scrollTop()) {
						m.find(".history-prev").addClass("not")
					} else {
						m.find(".history-prev").removeClass("not")
					}
				}
				if (m.find(".history-more").css("display") == "none" && m.find(".history-year:last").offset().top - 11 < $(window).scrollTop()) {
					m.find(".history-next").addClass("not")
				} else {
					m.find(".history-next").removeClass("not")
				}
			})
			
    if(o.loadeddata=="On Scroll"){  
      $(window).scroll(function() {
        if ($(window).scrollTop() + $(window).height() > m.find(".history-more").offset().top && lazyload==0) {
          if(ScrollMaxPage < o.ScrollMaxPage){
			if (stops && lazyload == 0) {
				stops = false;
				LoadAjaxItems();
			}
          }
        }
      });
   }	
			
	}
	}
})(jQuery);