﻿(function ($) {
	'use strict';

	$.ajaxForm = {
		init: function (selector, options) {
			$(selector).ajaxForm(options);
		}
	};

	$.fn.ajaxForm = function (options) {
		$(this).each(function (i, elem) {
			
			$(elem).wrapInner(function () {
			    return '<form action="' + $(this).data("action") + '" class="validationEngineContainer" />';
			});
			var form = $( $(elem).find("form"));
			console.log('form', form);
			var message = form.find('div.message');


			form.validationEngine();


			var settings = $.extend({
				'url': form.attr('action'),
			}, options);

			var url = settings.url.replace('/post?', '/post-json?').concat('&c=?');

			form.attr('novalidate', 'true');

			form.submit(function () {
              
				var msg;
				function successCallback(resp) {
					if (resp.result === 'success') {
						message.removeClass('error').addClass('valid');
						$("#form_ext_" + resp.ModuleId + " form").get(0).reset();
					} else {
					    message.removeClass('valid').addClass('error');
					}

					message.html(resp.message).show("slow");

					if (settings.Success) {
						settings.Success(resp);
					}
				}

				if (!form.validationEngine('validate')) {
					return false;
				}


				var data = {};
				var dataArray = form.serializeArray();
				$.each(dataArray, function (index, item) {
					data[item.name] = item.value;
				});

				$.ajax({
					url: url,
					data: data,
				    type:'POST',
					success: successCallback,
					dataType: 'jsonp',
					jsonp: 'jsoncallback',
					error: function (resp, text) {
					    message.html('ajax form submit error: ' + text).show("slow");
						console.log('ajax form submit error: ' + text);
						if (settings.Error) {
							settings.Error(resp);
						}
					}
				});

				// Translate and display submit message
				var submitMsg = 'Submitting...';
				message.html(submitMsg).show("slow");

				return false;
			});
		});
		return this;
	};






})(jQuery);



$(function ($) {


    function Success(resp) {
        console.log("success", resp);
        if (resp.result === 'success') {
            // Do stuff

        }
    }

    function Error(resp) {
        console.log("error", resp);
    }


    setTimeout(function () {
        $(".external-forms").ajaxForm({ Success: Success, Error: Error });
    }, "500");
    
});