﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="View_Article_Unsubscribe.ascx.cs" Inherits="DNNGo.Modules.xBlog.View_Article_Unsubscribe" %>

<asp:Panel ID="panShowUnsubscribe" runat="server" CssClass="">
<h1> <%=ViewResourceText("Title_Unsubscribe", "Unsubscribe")%> </h1>
<p><asp:Literal ID="liShowUnsubscribe" runat="server"></asp:Literal></p>
<p class="Unsubscribe_button">
    <asp:Button runat="server" ID="cmdUnsubscribe" OnClick="cmdUnsubscribe_Click" CssClass="btn" Text="Unsubscribe" resourcekey="cmdUnsubscribe"  />
    <asp:HyperLink runat="server" ID="cmdCancel" CssClass="btn" Text="Cancel" resourcekey="cmdCancel"></asp:HyperLink>
</p>




</asp:Panel>

<asp:Panel ID="panHideUnsubscribe" runat="server"  CssClass="">
<h1><%=ViewResourceText("Title_UnsubscribeOK", "Unsubscribe successful!")%></h1>
<p><asp:Literal ID="liHideUnsubscribe" runat="server"></asp:Literal></p>
<p class="submit_button">
    <asp:HyperLink runat="server" ID="cmdCancel1" CssClass="" Text="Return to blog" resourcekey="cmdCancel1"></asp:HyperLink>
</p>
</asp:Panel>