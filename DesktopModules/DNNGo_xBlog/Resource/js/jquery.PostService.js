
(function ($) {
    $.PostService = { version: '1.0.0' };

    $.fn.PostService = function (pp_settings) {
        pp_settings = jQuery.extend({
            ModulePath: '',
            ModuleId: 0,
            TabId: 0,
            PortalId: 0,
            PageIndex: 0,
            FirstScreen: 0,
            LoadDisplay: 0,
            Sequence: -1,
            Order:1,
            Feature: -1,
            Thumbnail_Width: 200,
            Thumbnail_Height: 200,
            Thumbnail_Mode: 'W',
            CategoryID: 0,
            Categorys: '',
            Tags:'',
            AjaxType: 'ajaxarticles',
            callback: function (Items, Pages, isEnd) { }
        }, pp_settings);

        var cov = function (v1, v2) {
            return v2 && v2 != undefined ? v2 : v1;
        };
        return this.each(function () {
            var $this = $(this);

            var Return_Items = new Array();

            var pageindex, pagesize, isEnd, moduleid, tabid, portalid, categoryid,categorys, isEnd, sequence, feature, tags;

            pageindex = cov(pp_settings.PageIndex, $this.data('pageindex'));
            firstscreen = cov(pp_settings.FirstScreen, $this.data('firstscreen'));
            loaddisplay = cov(pp_settings.LoadDisplay, $this.data('loaddisplay'));
            sequence = cov(pp_settings.Sequence, $this.data('sequence'));
            order = cov(pp_settings.Order, $this.data('order'));
            feature = cov(pp_settings.Feature, $this.data('feature'));
            moduleid = cov(pp_settings.ModuleId, $this.data('moduleid'));
            tabid = cov(pp_settings.TabId, $this.data('tabid'));
            portalid = cov(pp_settings.PortalId, $this.data('portalid'));
            modulepath = cov(pp_settings.ModulePath, $this.data('modulepath'));
            categoryid = cov(pp_settings.CategoryID, $this.data('categoryid'));
            categorys = cov(pp_settings.Categorys, $this.data('categorys'));
            tags = cov(pp_settings.Tags, $this.data('tags'));
            thumbnail_width = cov(pp_settings.Thumbnail_Width, $this.data('thumbnail_width'));
            thumbnail_height = cov(pp_settings.Thumbnail_Height, $this.data('thumbnail_height'));
            thumbnail_mode = cov(pp_settings.Thumbnail_Mode, $this.data('thumbnail_mode'));
            isEnd = cov(false, $this.data('isend'));
           
            jQuery.ajax({
                url: modulepath + "Resource_Service.aspx?Token=" + pp_settings.AjaxType + "&ModuleId=" + moduleid + "&TabId=" + tabid + "&PortalId=" + portalid ,
                type: "GET",
                data: { PageIndex: pageindex + 1, FirstScreen: firstscreen, LoadDisplay: loaddisplay, Sequence: sequence, Order: order, Feature: feature, CategoryID: categoryid, Categorys: categorys, Tags: tags, Thumbnail_Width: thumbnail_width, Thumbnail_Height: thumbnail_height, Thumbnail_Mode: thumbnail_mode },
                dataType: 'jsonp',
                jsonp: 'jsoncallback',
                success: function (data) {
                    var Pages = 0;
                    if (!isEnd || pageindex == 0) {
                        jQuery.each(data, function (i, item) {
                            Pages = item.Pages;
                            Return_Items.push(item);
                        });
                        Return_Items = Return_Items.reverse();
                    }
                    if (Pages > pageindex) {
                        $this.data("pageindex", $this.data("pageindex") + 1);
                    }
                    if (Pages <= (pageindex + 1)) {
                        isEnd = true;
                        $this.data('isend', true);
                    } else {
                        isEnd = false;
                        $this.data('isend', false);
                    }

                    pp_settings.callback(Return_Items, Pages, isEnd);
                }
            });


            //jQuery.get(modulepath + "Resource_Service.aspx?Token=" + pp_settings.AjaxType + "&ModuleId=" + moduleid + "&TabId=" + tabid + "&PortalId=" + portalid + "&jsoncallback=?", { PageIndex: pageindex + 1, FirstScreen: firstscreen, LoadDisplay: loaddisplay, Sequence: sequence, Feature: feature, CategoryID: categoryid, Tags: tags, Thumbnail_Width: thumbnail_width, Thumbnail_Height: thumbnail_height, Thumbnail_Mode: thumbnail_mode }, function (data) {
            //    alert(data);
                
            //});
        });
    }

})(jQuery);
