﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Manager_Option_Email.ascx.cs" Inherits="DNNGo.Modules.xBlog.Manager_Option_Email" %>

 
      <!-- start: PAGE HEADER -->
      <div class="row">
        <div class="col-sm-12"> 
          <!-- start: PAGE TITLE & BREADCRUMB -->
          
          <div class="page-header">
            <h1><i class="fa fa-envelope"></i> <%=ViewResourceText("Header_Title", "Email Settings")%></h1>
          </div>
          <!-- end: PAGE TITLE & BREADCRUMB --> 
        </div>
      </div>
      <!-- end: PAGE HEADER -->
            <!-- start: PAGE CONTENT -->
      
      <div class="row">
        <div class="col-sm-12">
    
            
          <div class="panel panel-default">
            <div class="panel-heading"> <i class="fa fa-external-link-square"></i> <%=ViewResourceText("Title_BaseSettings", "Basic Settings")%>
              <div class="panel-tools"> <a href="#" class="btn btn-xs btn-link panel-collapse collapses"> </a> </div>
            </div>
            <div class="panel-body">
                <div class="form-horizontal">
                  <div class="form-group">
                    <%=ViewControlTitle("lblSubscribeEmail", "Subscribe new article", "cbSubscribeEmail", ":", "col-sm-3 control-label")%>
                    <div class="col-sm-5">
                        <div class="checkbox-inline"><asp:CheckBox ID="cbSubscribeEmail" runat="server" CssClass="auto"/></div>
                    </div>
                  </div>

                 <div class="form-group">
                    <%=ViewControlTitle("lblSubscribeConfirm", "Enable user email confirmation", "cbSubscribeConfirm", ":", "col-sm-3 control-label")%>
                    <div class="col-sm-8">
                        <div class="checkbox-inline">
                            <asp:CheckBox runat="server" ID="cbSubscribeConfirm"  CssClass="auto"/>
                  
                         </div>
                        <%=ViewHelp("lblSubscribeConfirm", "Users need to click the confirmation link in the confirmation email to subscribe successfully.")%>
                    </div>
                  </div> 
                  <div class="form-group">
                    <%=ViewControlTitle("lblSenderEmail", "Sender E-mail", "SenderEmail", ":", "col-sm-3 control-label")%>
                    <div class="col-sm-5">
                      <asp:TextBox runat="server" ID="txtSenderEmail" Width="300px" CssClass="form-control  validate[required,custom[email]]"></asp:TextBox>
                    </div>
                  </div>
                </div>
            </div>
          </div>
             


           <div class="panel panel-default">
            <div class="panel-heading"> <i class="fa fa-external-link-square"></i> <%=ViewResourceText("Title_TemplateSettings", "Email Content Template")%>
              <div class="panel-tools"> <a href="#" class="btn btn-xs btn-link panel-collapse collapses"> </a> </div>
            </div>
            <div class="panel-body">
                <div class="form-horizontal">
                  <div class="form-group">
                    <%=ViewControlTitle("lblSubscriptionSubject", "Subscription Subject", "txtSubscriptionSubject", ":", "col-sm-3 control-label")%>
                    <div class="col-sm-8">
                      <asp:TextBox runat="server" ID="txtSubscriptionSubject" Width="500px" CssClass="form-control  validate[required,maxSize[200]]"></asp:TextBox>
                    </div>
                  </div>
                  <div class="form-group">
                    <%=ViewControlTitle("lblSubscriptionTemplate", "Subscription Template", "txtSubscriptionTemplate", ":", "col-sm-3 control-label")%>
                    <div class="col-sm-8">
                      <asp:TextBox  id="txtSubscriptionTemplate" CssClass="form-control" runat="server" height="300" width="100%" TextMode="MultiLine"></asp:TextBox>
                    </div>
                  </div>
  

                  <div class="form-group">
                    <%=ViewControlTitle("lblEmailTemplateTitle", "Template Token","", ":", "col-sm-3 control-label")%>
                    <div class="col-sm-8">
                      <%=ViewResourceText("lblEmailTemplate_SubscriptionTitle", "[Title]:display the article title.")%><br/>
                      <%=ViewResourceText("lblEmailTemplate_SubscriptionSummary", "[Summary]:display the article summary.")%><br/>
                      <%=ViewResourceText("lblEmailTemplate_SubscriptionContentText", "[ContentText]:display the article details.")%><br/>
                      <%=ViewResourceText("lblEmailTemplate_SubscriptionPublishTime", "[PublishTime]:display the article date posted.")%><br/>
                      <%=ViewResourceText("lblEmailTemplate_SubscriptionAuthor", "[Author]:display the article author.")%><br/>
                      <%=ViewResourceText("lblEmailTemplate_SubscriptionArticleUrl", "[ArticleUrl]:display the URL for the article details.")%><br/>
                      <%=ViewResourceText("lblEmailTemplate_SubscriptionThumbnailUrl", "[ThumbnailUrl]:display the URL for the article thumbnail.")%><br/>
                      <%=ViewResourceText("lblEmailTemplate_SubscriptionFeaturedImageUrl", "[FeaturedImageUrl]:display the URL for the article featured image.")%><br/>
                      <%=ViewResourceText("lblEmailTemplate_SubscriptionEmail", "[Email]:display the email address for the subscriber.")%><br/>
                      <%=ViewResourceText("lblEmailTemplate_SubscriptionName", "[Name]:display the user fullname for the subscriber.")%><br/>
                      <%=ViewResourceText("lblEmailTemplate_SubscriptionAttachmentList", "[AttachmentList]:display the article attachments.")%><br/>
                      <%=ViewResourceText("lblEmailTemplate_SubscriptionAuthorUrl", "[AuthorUrl]:display the URL for the author.")%><br/>
                      <%=ViewResourceText("lblEmailTemplate_SubscriptionWebSiteUrl", "[WebSiteUrl]:display the site URL.")%><br/>
                      <%=ViewResourceText("lblEmailTemplate_SubscriptionWebSite", "[WebSite]:display the site name.")%><br/>
                      <%=ViewResourceText("lblEmailTemplate_SubscriptionUnsubscribe", "[Unsubscribe]:display the Unsubscribe URL.")%>
                    </div>
                  </div>
                </div>
            </div>
          </div>



            <div class="panel panel-default">
            <div class="panel-heading"> <i class="fa fa-external-link-square"></i> <%=ViewResourceText("Title_SubscriptionListTemplateSettings", "Email  Subscription List Template")%>
              <div class="panel-tools"> <a href="#" class="btn btn-xs btn-link panel-collapse collapses"> </a> </div>
            </div>
            <div class="panel-body">
                <div class="form-horizontal">
                  
                  <div class="form-group">
                    <%=ViewControlTitle("lblSubscriptionListSubject", "Subscription List Subject", "txtSubscriptionListSubject", ":", "col-sm-3 control-label")%>
                    <div class="col-sm-8">
                      <asp:TextBox runat="server" ID="txtSubscriptionListSubject" Width="500px" CssClass="form-control  validate[required,maxSize[200]]"></asp:TextBox>
                    </div>
                  </div>
                  <div class="form-group">
                    <%=ViewControlTitle("lblSubscriptionListTemplate", "Subscription List Template", "txtSubscriptionListTemplate", ":", "col-sm-3 control-label")%>
                    <div class="col-sm-8">
                      <asp:TextBox  id="txtSubscriptionListTemplate" CssClass="form-control" runat="server" height="300" width="100%" TextMode="MultiLine"></asp:TextBox>
                    </div>
                  </div>
                   <div class="form-group">
                    <%=ViewControlTitle("lblSubscriptionListTemplateItems", "Subscription List Template Items", "txtSubscriptionListTemplateItems", ":", "col-sm-3 control-label")%>
                    <div class="col-sm-8">
                      <asp:TextBox  id="txtSubscriptionListTemplateItems" CssClass="form-control" runat="server" height="300" width="100%" TextMode="MultiLine"></asp:TextBox>
                    </div>
                  </div>

  

                  <div class="form-group">
                    <%=ViewControlTitle("lblEmailTemplateTitle", "Template Token","", ":", "col-sm-3 control-label")%>
                    <div class="col-sm-8">
                      <%=ViewResourceText("lblEmailTemplate_SubscriptionTitle", "[Title]:display the article title.")%><br/>
                      <%=ViewResourceText("lblEmailTemplate_SubscriptionSummary", "[Summary]:display the article summary.")%><br/>
                      	<%=ViewResourceText("lblEmailTemplate_SubscriptionContentText", "[ContentText]:display the article details.")%><br/>
                       <%=ViewResourceText("lblEmailTemplate_SubscriptionPublishTime", "[PublishTime]:display the article date posted.")%><br/>
                       <%=ViewResourceText("lblEmailTemplate_SubscriptionAuthor", "[Author]:display the article author.")%><br/>
                       <%=ViewResourceText("lblEmailTemplate_SubscriptionArticleUrl", "[ArticleUrl]:display the URL for the article details.")%><br/>
                       <%=ViewResourceText("lblEmailTemplate_SubscriptionThumbnailUrl", "[ThumbnailUrl]:display the URL for the article thumbnail.")%><br/>
                      <%=ViewResourceText("lblEmailTemplate_SubscriptionEmail", "[Email]:display the email address for the subscriber.")%><br/>
                      <%=ViewResourceText("lblEmailTemplate_SubscriptionName", "[Name]:display the user fullname for the subscriber.")%><br/>
                      <%=ViewResourceText("lblEmailTemplate_SubscriptionAuthorUrl", "[AuthorUrl]:display the URL for the author.")%><br/>
                      <%=ViewResourceText("lblEmailTemplate_SubscriptionWebSiteUrl", "[WebSiteUrl]:display the site URL.")%><br/>
                       <%=ViewResourceText("lblEmailTemplate_SubscriptionWebSite", "[WebSite]:display the site name.")%>
                    </div>
                  </div>
                </div>
            </div>
          </div>










          




        </div>
      </div>
      <!-- end: PAGE CONTENT-->
      
      <div class="row">
        <div class="col-sm-2"> </div>
        <div class="col-sm-10">
          <asp:Button CssClass="btn btn-primary" lang="Submit" ID="cmdUpdate" resourcekey="cmdUpdate"
        runat="server" Text="Update" OnClick="cmdUpdate_Click"></asp:Button>&nbsp;
        <asp:Button CssClass="btn btn-default" ID="cmdCancel" resourcekey="cmdCancel" runat="server"
            Text="Cancel" CausesValidation="False" OnClick="cmdCancel_Click"  OnClientClick="CancelValidation();"></asp:Button>&nbsp;
        
         </div>
      </div>


<script type="text/javascript">

    jQuery(function ($) {
        CKEDITOR.replace('<%=txtSubscriptionTemplate.ClientID %>', { height: '400px' });
        CKEDITOR.replace('<%=txtSubscriptionListTemplate.ClientID %>', { height: '400px' });
        CKEDITOR.replace('<%=txtSubscriptionListTemplateItems.ClientID %>', { height: '400px' });
   
    });
</script>