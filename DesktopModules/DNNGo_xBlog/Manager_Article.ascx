﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Manager_Article.ascx.cs"
    Inherits="DNNGo.Modules.xBlog.Manager_Article" %>

<%--<%@ Register TagPrefix="dnn" TagName="TextEditor" Src="~/controls/TextEditor.ascx"%>--%>
<link rel="stylesheet" href="<%=ModulePath %>Resource/plugins/jQuery-Tags-Input/jquery.tagsinput.css?cdv=<%=CrmVersion %>">
<script src="<%=ModulePath %>Resource/plugins/jQuery-Tags-Input/jquery.tagsinput.js?cdv=<%=CrmVersion %>"></script>

<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">
        <!-- start: PAGE TITLE & BREADCRUMB -->
        <div class="page-header">
            <h1>
                <i class="fa fa-plus"></i> <%=ViewResourceText("Header_Title", "Add New Post")%>
                <%--<small>overview &amp; stats </small>--%></h1>
        </div>
        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<!-- end: PAGE HEADER -->
<!-- start: PAGE CONTENT -->
<div class="row">
    <div class="col-sm-8">
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label for="form-field-1">
                        <%=ViewTitle("lblTitle", "Title", "txtTitle")%>:</label>
                        <asp:TextBox ID="txtTitle" placeholder="Title" runat="server" CssClass="form-control validate[required,maxSize[500]]"></asp:TextBox>
                </div>
                <div class="form-group">
                    <label for="form-field-2">
                        <%=ViewTitle("lblSummary", "Summary", "txtSummary")%>:</label>
                    <asp:TextBox ID="txtSummary" placeholder="Summary" CssClass="form-control limited validate[maxSize[2000]]"
                        runat="server" Rows="3" TextMode="MultiLine"></asp:TextBox>
                </div>
                <div class="form-group">
                    <label for="form-field-3">
                        <%=ViewTitle("lblContentText", "ContentText", "txtContentText")%>:
                    </label>
                    <asp:HyperLink ID="hlAddMedia" resourcekey="hlAddMedia" runat="server" Visible="false" CssClass="btn btn-light-grey" Text="<i class='fa fa-picture-o'></i> Add Media"  data-toggle="modal" NavigateUrl="#AdditionalPicture_Modal"></asp:HyperLink>
                    <asp:TextBox ID="txtContentText" CssClass="ckeditor_ form-control" runat="server"
                        Height="300" Width="100%" TextMode="MultiLine"></asp:TextBox>
                  <%--  <dnn:TextEditor id="txtContentTelerik" runat="server" height="400" width="100%"/>--%>
                      
                    
                </div>
            </div>
        </div>


        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i> <%=ViewResourceText("Title_Languages", "Language Links")%>
                <div class="panel-tools">
                    <a href="#" class="btn btn-xs btn-link panel-collapse collapses"></a>
                </div>
            </div>
            <div class="panel-body">
                  <div class="form-horizontal">
                <asp:Repeater ID="RepeaterLanguageLinks" runat="server" >
                    <ItemTemplate>
                        <div class="form-group">
                            <div class="col-sm-3"><a class="btn btn-primary btn-sm"><i class=""></i><%#Eval("NativeName")%></a></div>
                            <div class="col-sm-7"><input type="text" id="LanguageLink<%#Eval("LanguageId")%>" name="LanguageLink<%#Eval("LanguageId")%>" style="width:100%" placeholder="article link" value="<%#Eval("ArticleLink")%>" /> </div>
                            <div class="col-sm-2">
                                <div class="checkbox">
										<label>
											<input type="checkbox" id="LanguageEnable<%#Eval("LanguageId")%>" name="LanguageEnable<%#Eval("LanguageId")%>"  <%#Eval("Checked")%>   value="checked" class="grey">
											<%=ViewResourceText("Title_Enable", "Enable")%>
										</label>
									</div>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
                        </div>
            </div>
        </div>




               



        <!-- start: TEXT AREA PANEL -->
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i> <%=ViewResourceText("Title_SEOSettings", "SEO Settings")%>
                <div class="panel-tools">
                    <a href="#" class="btn btn-xs btn-link panel-collapse collapses"></a>
                </div>
            </div>
            <div class="panel-body">
            
                 <div class="form-group">
                    <%=ViewTitle("lblFriendlyUrl", "Friendly Url Name", "txtFriendlyUrl")%>:
                    <asp:TextBox ID="txtFriendlyUrl" CssClass="form-control validate[maxSize[500]]" runat="server"></asp:TextBox>
                </div>
                <div class="form-group">
                    <%=ViewTitle("lblSearchTitle", "SEO Title", "txtSearchTitle")%>:
                    <asp:TextBox ID="txtSearchTitle" CssClass="form-control validate[maxSize[500]]" runat="server"></asp:TextBox>
                </div>
                <div class="form-group">
                    <%=ViewTitle("lblSearchKeywords", "SEO Keywords", "txtSearchKeywords")%>:
                    <asp:TextBox ID="txtSearchKeywords" CssClass="form-control limited validate[maxSize[500]]"
                        runat="server" Rows="2" TextMode="MultiLine"></asp:TextBox>
                </div>
                <div class="form-group">
                    <%=ViewTitle("lblSearchDescription", "SEO Description", "txtSearchDescription")%>:
                    <asp:TextBox ID="txtSearchDescription" CssClass="autosize form-control validate[maxSize[500]]"
                        runat="server" Rows="4" TextMode="MultiLine"></asp:TextBox>
                </div>
                
            </div>
        </div>

        <div runat="server" id="divOptions" visible="false">
            <asp:Repeater ID="RepeaterGroup" runat="server" OnItemDataBound="RepeaterGroup_ItemDataBound">
                <ItemTemplate>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-external-link-square"></i>
                            <%#Eval("key")%>
                            <div class="panel-tools">
                                <a href="#" class="btn btn-xs btn-link panel-collapse collapses"></a>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal">
                                <asp:Repeater ID="RepeaterOptions" runat="server" OnItemDataBound="RepeaterOptions_ItemDataBound">
                                    <ItemTemplate>
                                        <div class="form-group">
                                            <asp:Literal ID="liTitle" runat="server"></asp:Literal>
                                            <div class="col-sm-9">
                                                <asp:PlaceHolder ID="ThemePH" runat="server"></asp:PlaceHolder>
                                                <asp:Literal ID="liHelp" runat="server"></asp:Literal>
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </div>
                        </div>
                        <!-- end: TEXT AREA PANEL -->
                    </div>
                </ItemTemplate>
            </asp:Repeater>
        </div>

    </div>
    <div class="col-sm-4">
        <!-- start: SELECT BOX PANEL -->
        <!--Publish-->
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i> <%=ViewResourceText("Title_Publish", "Publish")%>
                <div class="panel-tools">
                    <a href="#" class="btn btn-xs btn-link panel-collapse collapses"></a>
                </div>
            </div>
            <div class="panel-body buttons-widget">
                <div class="row">
                    <div class="col-sm-6">
                        <asp:Button CssClass="btn btn-light-grey" ID="cmdSaveDraft" resourcekey="cmdSaveDraft"
                            runat="server" Text="Save Draft" CausesValidation="False" OnClick="cmdSaveDraft_Click"
                            OnClientClick="CancelValidation();"></asp:Button>
                    </div>
                    <div class="col-sm-6 text_right">
                        <asp:HyperLink ID="hlPreview" resourcekey="hlPreview" runat="server" CssClass="btn btn-light-grey" Text="<i class='fa clip-link'></i> Preview"
                            Visible="false" Target="_blank"></asp:HyperLink>
                    </div>
                </div>
                <ul class="Edit_List" id="accordion">
                    <li>
                        <p>
                            <i class="fa clip-grid-5"></i>&nbsp;&nbsp;<%=ViewResourceText("Title_Status", "Status")%>: <b>
                                <asp:Label ID="liArticleStatus" runat="server" Text="Draft"></asp:Label></b>&nbsp;&nbsp;<a
                                    href="#Status" data-toggle="collapse" data-parent="#accordion"><i class="fa fa-pencil"></i>[Edit]</a></p>
                        <div class="collapse" id="Status">
                            <div class="row form-group">
                                <div class="col-sm-6">
                                    <asp:DropDownList ID="ddlArticleStatus" runat="server" CssClass="form-control form-trumpet">
                                    </asp:DropDownList>
                                    <asp:DropDownList ID="ddlStatusByUser" runat="server" CssClass="form-control form-trumpet">
                                    </asp:DropDownList>
                                </div>
                                <div class="col-sm-6 text_right">
                                    <a id="link_ArticleStatus" class="btn btn-default btn-ms2" href="#Status" data-toggle="collapse"
                                        data-parent="#accordion"><%=ViewResourceText("Title_OK", "OK")%> </a>&nbsp;<a href="#Status" data-toggle="collapse" data-parent="#accordion"><%=ViewResourceText("Title_Cancel", "Cancel")%></a>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li  id="divFeature" runat="server">
                        <p>
                            <i class="fa clip-clipboard"></i>&nbsp;&nbsp;<%=ViewResourceText("Title_Feature", "Feature")%>: <b>
                                <asp:Label ID="liFeature" runat="server" Text="No"></asp:Label></b>&nbsp;&nbsp;<a
                                    href="#Feature" data-toggle="collapse" data-parent="#accordion"><i class="fa fa-pencil"></i>[Edit]</a></p>
                        <div class="collapse" id="Feature">
                            <div class="row form-group">
                                <div class="col-sm-6">
                                    <asp:DropDownList ID="ddlFeature" runat="server" CssClass="form-control form-trumpet">
                                    </asp:DropDownList>
                                </div>
                                <div class="col-sm-6 text_right">
                                    <a id="link_Feature" class="btn btn-default btn-ms2" href="#Feature" data-toggle="collapse"
                                        data-parent="#accordion"><%=ViewResourceText("Title_OK", "OK")%> </a>&nbsp;<a href="#Feature" data-toggle="collapse" data-parent="#accordion"><%=ViewResourceText("Title_Cancel", "Cancel")%></a>
                                </div>
                            </div>
                        </div>
                    </li>
                      <li  id="divTopStatus" runat="server">
                        <p>
                            <i class="fa clip-thumbs-up"></i>&nbsp;&nbsp;<%=ViewResourceText("Title_TopStatus", "Top Status")%>: <b>
                                <asp:Label ID="liTopStatus" runat="server" Text="Normal"></asp:Label></b>&nbsp;&nbsp;<a
                                    href="#TopStatus" data-toggle="collapse" data-parent="#accordion"><i class="fa fa-pencil"></i>[Edit]</a></p>
                        <div class="collapse" id="TopStatus">
                            <div class="row form-group">
                                <div class="col-sm-6">
                                    <asp:DropDownList ID="ddlTopStatus" runat="server" CssClass="form-control form-trumpet">
                                    </asp:DropDownList>
                                </div>
                                <div class="col-sm-6 text_right">
                                    <a id="link_TopStatus" class="btn btn-default btn-ms2" href="#TopStatus" data-toggle="collapse"
                                        data-parent="#accordion"><%=ViewResourceText("Title_OK", "OK")%> </a>&nbsp;<a href="#TopStatus" data-toggle="collapse" data-parent="#accordion"><%=ViewResourceText("Title_Cancel", "Cancel")%></a>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <p>
                            <i class="fa clip-calendar-3"></i>&nbsp;&nbsp;<%=ViewResourceText("Title_Publish", "Publish")%>: <b>
                                <asp:Label ID="liPublishDateTime" runat="server" Text="Immediately"></asp:Label></b>&nbsp;<a
                                    href="#Publish" data-toggle="collapse" data-parent="#accordion"><i class="fa fa-pencil"></i>[Edit]</a></p>
                        <div class="panel-collapse collapse" id="Publish">
                            <div class="row form-group">
                                <div class="col-md-6 input-group">
                                    <asp:TextBox ID="txtPublishDate" runat="server" data-date-format="mm/dd/yyyy" data-date-viewmode="years"
                                        CssClass="form-control date-picker"></asp:TextBox>
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                </div>
                                <div class="col-md-5 input-group input-append bootstrap-timepicker">
                                    <asp:TextBox ID="txtPublishTime" runat="server" CssClass="form-control time-picker"></asp:TextBox>
                                    <span class="input-group-addon add-on"><i class="fa fa-clock-o"></i></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <a id="link_PublishDateTime" class="btn btn-default btn-ms2" href="#Publish" data-toggle="collapse"
                                    data-parent="#accordion"><%=ViewResourceText("Title_OK", "OK")%> </a>&nbsp;<a href="#Publish" data-toggle="collapse" data-parent="#accordion"><%=ViewResourceText("Title_Cancel", "Cancel")%></a>
                            </div>
                        </div>
                    </li>
                    <li>
                        <p>
                            <i class="clip-stopwatch"></i>&nbsp;&nbsp;<%=ViewResourceText("Title_Disable", "Disable")%>: <b>
                                <asp:Label ID="liDisableDateTime" runat="server" Text="Ten years later"></asp:Label></b>&nbsp;<a
                                    href="#DisableDateTime" data-toggle="collapse" data-parent="#accordion"><i class="fa fa-pencil"></i>[Edit]</a></p>
                        <div class="panel-collapse collapse" id="DisableDateTime">
                            <div class="row form-group">
                                <div class="col-md-6 input-group">
                                    <asp:TextBox ID="txtDisableDate" runat="server" data-date-format="mm/dd/yyyy" data-date-viewmode="years"
                                        CssClass="form-control date-picker"></asp:TextBox>
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                </div>
                                <div class="col-md-5 input-group input-append bootstrap-timepicker">
                                    <asp:TextBox ID="txtDisableTime" runat="server" CssClass="form-control time-picker"></asp:TextBox>
                                    <span class="input-group-addon add-on"><i class="fa fa-clock-o"></i></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <a id="link_DisableDateTime" class="btn btn-default btn-ms2" href="#DisableDateTime"
                                    data-toggle="collapse" data-parent="#accordion"><%=ViewResourceText("Title_OK", "OK")%> </a>&nbsp;<a href="#DisableDateTime"
                                        data-toggle="collapse" data-parent="#accordion"><%=ViewResourceText("Title_Cancel", "Cancel")%></a>
                            </div>
                        </div>
                    </li>
                </ul>
                <div class="row">
                	<br/>
                    <div class="col-sm-5">
                        <asp:Button CssClass="btn btn-link" ID="cmdDelete" resourcekey="cmdMovetoTrash" runat="server"
                            Text="Move to Trash" CausesValidation="False" OnClick="cmdDelete_Click" OnClientClick="CancelValidation();">
                        </asp:Button>
                    </div>
                    <div class="col-sm-7 text_right">
                        <asp:Button CssClass="btn btn-primary btn-sm" lang="Submit" ID="cmdPublish" resourcekey="cmdPublish"
                            runat="server" Text="Publish" OnClick="cmdPublish_Click"></asp:Button>&nbsp;
                        <asp:Button CssClass="btn btn-primary btn-sm" ID="cmdCancel" resourcekey="cmdCancel"
                            runat="server" Text="Cancel" CausesValidation="False" OnClick="cmdCancel_Click"
                            OnClientClick="CancelValidation();"></asp:Button>&nbsp;
                    </div>
                </div>
            </div>
        </div>



         <!--Article Author-->
        <div class="panel panel-default" id="divArticleAuthor" runat="server">
            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i><%=ViewResourceText("Title_ArticleAuthor", "Article Author")%>
                <div class="panel-tools">
                    <a href="#" class="btn btn-xs btn-link panel-collapse collapses"></a>
                </div>
            </div>
            <div class="panel-body buttons-widget">
                <asp:HiddenField runat="server" ID="hfArticleAuthor" />
               <div class="col-md-12 lead">
                    <asp:Image ID="imgArticleAuthor" runat="server" Width="80" />
                    <asp:Label ID="lbArticleAuthor" runat="server"></asp:Label>
               </div>
               <div class="col-md-7">
                <asp:HyperLink runat="server" ID="hlArticleAuthor" resourcekey="hlArticleAuthor"
                    data-toggle="modal" NavigateUrl="#ArticleAuthor_Modal" Text="<i class='fa clip-user-plus'></i> Set Article Author"
                    ToolTip="Set Article Author" Target="_blank" CssClass="btn btn-light-grey"></asp:HyperLink></div>
                <div id="ArticleAuthor_Modal" class="modal fade" tabindex="-1" data-width="820"
                    data-height="400" style="display: none;">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                            &times;
                        </button>
                        <h4 class="modal-title">
                            <i class='fa clip-user-plus'></i> Set Article Author</h4>
                    </div>
                    <div class="modal-body">
                        <iframe id="ArticleAuthor_Iframe" width="100%" height="100%" style="border-width: 0px;">
                        </iframe>
                    </div>
                </div>
                <%--<a href="#">Set featured image </a> --%>
            </div>
        </div>









        <!--Categories-->
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i> <%=ViewResourceText("Title_Categories", "Categories")%>
                <div class="panel-tools">
                    <a href="#" class="btn btn-xs btn-link panel-collapse collapses"></a>
                </div>
            </div>
            <div class="panel-body buttons-widget">
                <div class="tabbable">
                    <ul id="myTab4" class="nav nav-tabs tab-bricky">
                        <li class="active"><a href="#panel_tab3_example1" data-toggle="tab"><%=ViewResourceText("Title_AllCategories", "All Categories")%> </a>
                        </li>
                        <li><a href="#panel_tab3_example2" data-toggle="tab"><%=ViewResourceText("Title_MostUsed", "Most Used")%> </a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane in active" id="panel_tab3_example1">
                            <div class="checkbox">
                                <asp:Literal runat="server" ID="liCategory"></asp:Literal></div>
                            <asp:HyperLink runat="server" ID="hlAddCategory" resourcekey="hlAddCategory" Text="Add new category"></asp:HyperLink>
                        </div>
                        <div class="tab-pane" id="panel_tab3_example2">
                            <div class="checkbox">
                                <asp:CheckBoxList ID="cblCategory" runat="server" CssClass="MostUsed_Category" DataTextField="Name" DataValueField="ID"></asp:CheckBoxList>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


            <!--Socaial Gloups-->
        <div class="panel panel-default" id="divSocaialGloups" runat="server" visible="false">
            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i><%=ViewResourceText("Title_Share", "Share")%>
                <div class="panel-tools">
                    <a href="#" class="btn btn-xs btn-link panel-collapse collapses"></a>
                </div>
            </div>
            <div class="panel-body buttons-widget ">
                <div class="form-horizontal">
                    <div class="form-group">
                       <%=ViewControlTitle("lblJournalPost", "Post to journal", "cbJournalPost", ":", "col-sm-5 control-label")%>
                        <div class="col-sm-7">
                             <div class="checkbox-inline">
                                <asp:CheckBox ID="cbJournalPost" runat="server" CssClass="auto"/>
                            </div>
                        </div>
                  </div>

                  <div class="form-group">
                       <%=ViewControlTitle("lblJournalView", "View", "ddlJournalView", ":", "col-sm-5 control-label")%>
                        <div class="col-sm-7">
                                <asp:DropDownList ID="ddlJournalView" runat="server" CssClass="form-control"></asp:DropDownList>
                        </div>
                  </div>
                   <div class="form-group">
                       <%=ViewControlTitle("lblJournalGroups", "Groups", "ddlJournalGroups", ":", "col-sm-5 control-label")%>
                        <div class="col-sm-7">
                            <asp:DropDownList ID="ddlJournalGroups" runat="server" CssClass="form-control"></asp:DropDownList>
                        </div>
                  </div>


                </div>
                   
                  
            </div>
        </div>

        
        <div class="panel panel-default"  >
            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i>
                <%=ViewResourceText("Title_Permissions", "Permissions")%>
                <div class="panel-tools">
                    <a href="#" class="btn btn-xs btn-link panel-collapse collapses"></a>
                </div>
            </div>
            <div class="panel-body buttons-widget form-horizontal">
                  <div class="form-group">
                       <%=ViewControlTitle("lblPermissionsAllUsers", "All Users", "cbPermissionsAllUsers", ":", "col-sm-3 control-label")%>
                        <div class="col-sm-9">
                             <div class="checkbox-inline">
                                <asp:CheckBox ID="cbPermissionsAllUsers" runat="server" CssClass="auto"/>
                            </div>
                        </div>
                  </div>

                  <div class="form-group">
                       <%=ViewControlTitle("lblPermissionsRoles", "Permission Roles", "cblPermissionsRoles", ":", "col-sm-3 control-label")%>
                        <div class="col-sm-9">
                            <div class="checkbox-inline">
                                <asp:CheckBoxList ID="cblPermissionsRoles" runat="server" CssClass="auto"></asp:CheckBoxList>
                            </div>
                        </div>
                  </div>
            </div>
        </div>


        <!--Tags-->
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i><%=ViewResourceText("Title_Tags", "Tags")%>
                <div class="panel-tools">
                    <a href="#" class="btn btn-xs btn-link panel-collapse collapses"></a>
                </div>
            </div>
            <div class="panel-body buttons-widget">
                <div  id="TagDiv">
                    <asp:TextBox ID="txtTags" runat="server"></asp:TextBox>
                </div>
                <p>
                    <i><%=ViewResourceText("Title_Tags_Help", "Separate tags with commas")%></i>
                </p>
            </div>
        </div>
        <!--Featured Image-->
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i><%=ViewResourceText("Title_FeaturedImage", "Featured Image")%>
                <div class="panel-tools">
                    <a href="#" class="btn btn-xs btn-link panel-collapse collapses"></a>
                </div>
            </div>
            <div class="panel-body buttons-widget">
                <asp:HiddenField runat="server" ID="hfAdditionalPicture" />
                <asp:HyperLink runat="server" ID="hlAdditionalPicture" resourcekey="hlAdditionalPicture"
                    data-toggle="modal" NavigateUrl="#AdditionalPicture_Modal" Text="<i class='fa clip-pictures'></i> Set featured image"
                    ToolTip="Set featured image" Target="_blank" CssClass="btn btn-light-grey"></asp:HyperLink>
                <asp:HyperLink runat="server" ID="hlRemovePicture" onclick="RemovePicture();" Text="<i class='fa fa-trash-o'></i> Delete"
                    NavigateUrl="javascript:;" resourcekey="hlRemovePicture" CssClass="btn btn-xs btn-bricky"></asp:HyperLink>
                <div class="add_summary form_field">
                    <asp:Image runat="server" ID="imgAdditionalPicture" />
                </div>
                <div id="AdditionalPicture_Modal" class="modal fade" tabindex="-1" data-width="820"
                    data-height="400" style="display: none;">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                            &times;
                        </button>
                        <h4 class="modal-title">
                            <i class='fa fa-folder-open'></i>Set featured image</h4>
                    </div>
                    <div class="modal-body">
                        <iframe id="AdditionalPicture_Iframe" width="100%" height="100%" style="border-width: 0px;">
                        </iframe>
                    </div>
                </div>
                <%--<a href="#">Set featured image </a> --%>
            </div>
        </div>
        <!--Gallery Image-->
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i> <%=ViewResourceText("Title_GalleryImage", "Gallery Image")%>
                <div class="panel-tools">
                    <a href="#" class="btn btn-xs btn-link panel-collapse collapses"></a>
                </div>
            </div>
            <div class="panel-body buttons-widget">
                <asp:HyperLink runat="server" ID="hlAddAlbums" resourcekey="hlAddAlbums"
                    data-toggle="modal" NavigateUrl="#AddAlbums_Modal" Text="<i class='fa clip-images'></i> Set Gallery image"
                    ToolTip="Set Gallery image" CssClass="btn btn-light-grey"></asp:HyperLink>
                <asp:HiddenField runat="server" ID="hfAddAlbums" />
                 <div class="form-group">
                    <table id="ul_albums" class="ul_albums table table-striped table-bordered table-hover" id="sample-table-2">
                          <tbody>
                            <asp:Literal ID="liAddAlbums" runat="server"></asp:Literal>
                         </tbody>
                    </table>
 
                      <table id="Table_GalleryList" class="ul_albums table table-striped table-bordered table-hover">
                        <tbody>
                          <tr>
            	            <th>Images</th>
            	            <th>Title & Description</th>
                          </tr>
                        </tbody>
                      </table>


               </div>
                <div id="AddAlbums_Modal" class="modal fade" tabindex="-1" data-width="820"
                    data-height="400" style="display: none;">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                            &times;
                        </button>
                        <h4 class="modal-title">
                            <i class='fa fa-folder-open'></i>Set gallery image</h4>
                    </div>
                    <div class="modal-body">
                        <iframe id="AddAlbums_Iframe" width="100%" height="100%" style="border-width: 0px;">
                        </iframe>
                    </div>
                </div>
            </div>
        </div>
        <!--Attachments list-->
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i> <%=ViewResourceText("Title_AttachmentsList", "Attachments List")%>
                <div class="panel-tools">
                    <a href="#" class="btn btn-xs btn-link panel-collapse collapses"></a>
                </div>
            </div>
            <div class="panel-body buttons-widget">
                 <asp:HyperLink runat="server" ID="hlAddAttachments" resourcekey="hlAddAttachments"
                    data-toggle="modal" NavigateUrl="#AddAttachments_Modal" Text="<i class='fa clip-attachment'></i> Set Attachment list"
                    ToolTip="Add Attachments" CssClass="btn btn-light-grey"></asp:HyperLink>
                <asp:HiddenField runat="server" ID="hfAddAttachments" />
                <div class="form-group">
                    <table id="ul_Attachments" class="ul_Attachments table table-striped table-bordered table-hover" id="sample-table-2">
                          <tbody>
                            <asp:Literal ID="liAddAttachments" runat="server"></asp:Literal>
                         </tbody>
                    </table>
                      <table id="Table_Attachments" class="ul_albums table table-striped table-bordered table-hover">
                        <tbody>
                          <tr>
            	            <th>Images</th>
            	            <th>Title & Description</th>
                          </tr>
                        </tbody>
                      </table>

               </div>

            </div>
            <div id="AddAttachments_Modal" class="modal fade" tabindex="-1" data-width="820"
                    data-height="400" style="display: none;">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                            &times;
                        </button>
                        <h4 class="modal-title">
                            <i class='fa fa-folder-open'></i>Set Attachments</h4>
                    </div>
                    <div class="modal-body">
                        <iframe id="AddAttachments_Iframe" width="100%" height="100%" style="border-width: 0px;">
                        </iframe>
                    </div>
             </div>
        </div>
        <!-- end: SELECT BOX PANEL -->
    </div>
    <!-- end: PAGE CONTENT-->
</div>
<script type="text/javascript">


    /*移除图片*/
    function RemovePicture() {
        jQuery('#<%=hfAdditionalPicture.ClientID %>').val('0');
        jQuery('#<%=imgAdditionalPicture.ClientID %>').hide();
        jQuery('#<%=imgAdditionalPicture.ClientID %>').attr("src", "")
        jQuery('#<%=hlRemovePicture.ClientID %>').hide();
        jQuery('#<%=hlAdditionalPicture.ClientID %>').show();
    }
    /*显示图片*/
    function ShowPicture(PictureUrl, PictureID) {
        jQuery('#<%=hfAdditionalPicture.ClientID %>').val(PictureID);
        jQuery('#<%=imgAdditionalPicture.ClientID %>').attr("src", PictureUrl)
        jQuery('#<%=imgAdditionalPicture.ClientID %>').show();
        jQuery('#<%=hlRemovePicture.ClientID %>').show();
        jQuery('#<%=hlAdditionalPicture.ClientID %>').hide();

    }


    function ReturnUser(UserName, UserID,UserPic) {
        jQuery('#<%=lbArticleAuthor.ClientID %>').text(UserName);
        jQuery('#<%=hfArticleAuthor.ClientID %>').val(UserID);
        jQuery('#<%=imgArticleAuthor.ClientID %>').attr("src", UserPic);
    } 

    function insert_files(json_items, insert_type)
    {
        $.each(json_items,function (index, json_item) {
            if (insert_type) {
                if (!$("#Table_GalleryList tr[data-id='" + json_item.ID + "']").is("tr"))
                {
                    $("#Table_GalleryList").append($("#scriptFiles").tmpl(json_item));
                }
            } else {
                if (!$("#Table_Attachments tr[data-id='" + json_item.ID + "']").is("tr")) {
                    $("#Table_Attachments").append($("#scriptFiles").tmpl(json_item));
                }
            }
        });
        bind_delete_files_gallerylist();
        bind_delete_files_attachments();

    }
    function bind_delete_files_gallerylist() {
        $("#Table_GalleryList tr a.delete").click(function () {
            var id = $(this).attr("data-id");
            $("#Table_GalleryList tr[data-id='" + id + "']").hide("fast", function () {
                $(this).remove();
            });
        });
    }

    function bind_delete_files_attachments() {
        $("#Table_Attachments tr a.delete").click(function () {
            var id = $(this).attr("data-id");
            $("#Table_Attachments tr[data-id='" + id + "']").hide("fast", function () {
                $(this).remove();
            });
        });
    }

    function load_files(insert_type)
    {
        <%if (ArticleID > 0){%>
        $.getJSON("<%=ModulePath%>Resource_Service.aspx?Token=ajaxafiles&ArticleID=<%=ArticleID%>&insert_type=" + insert_type, Module, function (json) {
            insert_files(json, insert_type);
        });
        <%}%>
    }





    jQuery(function ($) {
        $("#<%=hlAdditionalPicture.ClientID %>").click(function () { $("#AdditionalPicture_Iframe").attr("src", $(this).attr("data-href")); });
        $("#<%=hlAddMedia.ClientID %>").click(function () { $("#AdditionalPicture_Iframe").attr("src", $(this).attr("data-href")); });
        $("#<%=hlAddAlbums.ClientID %>").click(function () { $("#AddAlbums_Iframe").attr("src", $(this).attr("data-href")); });
        $("#<%=hlAddAttachments.ClientID %>").click(function () { $("#AddAttachments_Iframe").attr("src", $(this).attr("data-href")); });
        $("#<%=hlArticleAuthor.ClientID %>").click(function () { $("#ArticleAuthor_Iframe").attr("src", $(this).attr("data-href")); });

        //加载默认的相册和附件列表
        load_files(true);
        load_files(false);

      

      


        $("#link_ArticleStatus").click(function () { $('#<%=liArticleStatus.ClientID %>').text($("#<%=ddlArticleStatus.ClientID %>,#<%=ddlStatusByUser.ClientID %>").find("option:selected").text()); });
        $("#link_Feature").click(function () { $('#<%=liFeature.ClientID %>').text($("#<%=ddlFeature.ClientID %>").find("option:selected").text()); });
        $("#link_TopStatus").click(function () { $('#<%=liTopStatus.ClientID %>').text($("#<%=ddlTopStatus.ClientID %>").find("option:selected").text()); });
        $("#link_PublishDateTime").click(function () { $('#<%=liPublishDateTime.ClientID %>').text($("#<%=txtPublishDate.ClientID %>").val() + "  " + $("#<%=txtPublishTime.ClientID %>").val()); });
        $("#link_DisableDateTime").click(function () { $('#<%=liDisableDateTime.ClientID %>').text($("#<%=txtDisableDate.ClientID %>").val() + "  " + $("#<%=txtDisableTime.ClientID %>").val()); });

        $('#<%=txtTags.ClientID %>').tagsInput({ width: 'auto', height: '100px' });

         CKEDITOR.replace('<%=txtContentText.ClientID %>', {height:'400px'});

    });
</script>
