﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Manager_Option_Captcha.ascx.cs" Inherits="DNNGo.Modules.xBlog.Manager_Option_Captcha" %>

 
      <!-- start: PAGE HEADER -->
      <div class="row">
        <div class="col-sm-12"> 
          <!-- start: PAGE TITLE & BREADCRUMB -->
          
          <div class="page-header">
            <h1><i class="fa clip-key-2"></i> <%=ViewResourceText("Header_Title", "Google reCAPTCHA Settings")%></h1>
          </div>
          <!-- end: PAGE TITLE & BREADCRUMB --> 
        </div>
      </div>
      <!-- end: PAGE HEADER -->
            <!-- start: PAGE CONTENT -->
      
      <div class="row">
        <div class="col-sm-12">
    
             


         <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i>
                <%=ViewTitle("lblCaptchaSettings", "Google reCAPTCHA v3")%>
                <div class="panel-tools">
                    <a href="#" class="btn btn-xs btn-link panel-collapse collapses"></a>
                </div>
            </div>
            <div class="panel-body">
                <div class="form-horizontal">
                    <div class="form-group">
                        <%=ViewControlTitle("lblCaptchaEnable", "reCAPTCHA Enable", "cbCaptchaEnable", ":", "col-sm-3 control-label")%>
                        <div class="col-sm-8">
                            <div class="checkbox-inline">
                               <asp:CheckBox ID="cbCaptchaEnable" runat="server" CssClass="auto" />
                                 <%=ViewHelp("lblCaptchaEnable","To use reCAPTCHA ( v3 ), you need to <a href='http://www.google.com/recaptcha/admin' target='_blank'>sign up for an API key pair</a> for your site. The key pair consists of a <b>site key</b> and <b>secret</b>.") %>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <%=ViewControlTitle("lblCaptchaSiteKey", "Site key", "txtCaptchaSiteKey", ":", "col-sm-3 control-label")%>
                        <div class="col-sm-8">
                            <div class="control-inline">
                               <asp:TextBox runat="server" ID="txtCaptchaSiteKey" Width="350px"  CssClass="form-control"></asp:TextBox>
                                <%=ViewHelp("lblCaptchaSiteKey","Use this in the code your site serves to users.") %>
                            </div>
                        </div>
                    </div>

                   <div class="form-group">
                        <%=ViewControlTitle("lblCaptchaSecretKey", "Secret key", "txtCaptchaSecretKey", ":", "col-sm-3 control-label")%>
                        <div class="col-sm-8">
                            <div class="control-inline">
                               <asp:TextBox runat="server" ID="txtCaptchaSecretKey" Width="350px"  CssClass="form-control"></asp:TextBox>
                                <%=ViewHelp("lblCaptchaSecretKey","Use this for communication between your site and Google. Be sure to keep it a secret.") %>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <%=ViewControlTitle("lblCaptchaSocial", "Social", "txtLimitSocial", ":", "col-sm-3 control-label")%>
                        <div class="col-sm-8">
                            <div class="control-inline">
                               <asp:TextBox runat="server" ID="txtLimitSocial" Width="50px"  CssClass="form-control  validate[required,custom[number]]"></asp:TextBox>
                                <%=ViewHelp("lblCaptchaSocial","0.1 - 1, The smaller the numerical value, the looser the restriction. Limit unanswered friend requests from abusive users") %>
                            </div>
                        </div>
                    </div>
                     
                  
                </div>
            </div>
        </div>

             
             








          




        </div>
      </div>
      <!-- end: PAGE CONTENT-->
      
      <div class="row">
        <div class="col-sm-2"> </div>
        <div class="col-sm-10">
          <asp:Button CssClass="btn btn-primary" lang="Submit" ID="cmdUpdate" resourcekey="cmdUpdate"
        runat="server" Text="Update" OnClick="cmdUpdate_Click"></asp:Button>&nbsp;
        <asp:Button CssClass="btn btn-default" ID="cmdCancel" resourcekey="cmdCancel" runat="server"
            Text="Cancel" CausesValidation="False" OnClick="cmdCancel_Click"  OnClientClick="CancelValidation();"></asp:Button>&nbsp;
        
         </div>
      </div>


 