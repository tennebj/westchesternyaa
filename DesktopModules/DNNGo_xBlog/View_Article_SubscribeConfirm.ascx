﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="View_Article_SubscribeConfirm.ascx.cs" Inherits="DNNGo.Modules.xBlog.View_Article_SubscribeConfirm" %>

<asp:Panel ID="panShowSubscribe" runat="server" CssClass="" Visible ="false">
<h1> <%=ViewResourceText("Title_SubscribeConfirm1", "Sorry!")%> </h1>
<p><asp:Literal ID="liSubscribeConfirm" runat="server"></asp:Literal></p>
<p class="subscribe_confirm_button"><asp:HyperLink runat="server" ID="cmdCancel" Text="Click here" resourcekey="cmdClickHere"></asp:HyperLink> to return to the site.</p>
</asp:Panel>

<asp:Panel ID="panShowSubscribeOK" runat="server" CssClass="" Visible ="false">
<h1> <%=ViewResourceText("Title_SubscribeConfirm2", "Subscription successful!")%> </h1>
<p>
    <asp:Literal ID="liSubscribeConfirmOK" runat="server"></asp:Literal>
</p>
<p class="subscribe_confirm_button"><asp:HyperLink runat="server" ID="cmdCancel2" Text="Click here" resourcekey="cmdClickHere2"></asp:HyperLink> to return to the site.</p>
</asp:Panel>
