﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="View_Subscription.ascx.cs" Inherits="DNNGo.Modules.xBlog.View_Subscription" %>
<asp:Panel ID="pNotsubscribed" runat="server" Visible="false">
    <asp:HiddenField ID="hfGoogleRecaptchaResponse" runat="server" />
    <div  class="validationEngineContainer form_div_<%=ModuleId %>">
    <div class="name_field"><%=ViewResourceText("Name_Title", "Name")%></div>
            <div class="xblog_Subscription_Nane">
           <asp:TextBox runat="server" ID="txtUserName" CssClass="input_text"></asp:TextBox>
          </div>
          <div class="email_field"><%=ViewResourceText("Email_Title", "Email*")%></div>
          <div class="xblog_Subscription_Email">
           <asp:TextBox runat="server" ID="txtUserEmail" CssClass="input_text validate[required,custom[email]]"></asp:TextBox>
          </div>

            <%if (ViewSettingT<Boolean>("xBlog_Recaptcha_v3_Enable", false)) {%>
           <div>
               <div class="g-recaptcha-msg msg-hide" id="g-recaptcha-msg-<%=ModuleId %>"  style="display: none;"> <%=ViewResourceText("GRecaptchaMsg", "Please verify you are not a robot!") %> </div>
           </div>
           <%}%>

           <asp:Panel ID="pConfirmDIV" CssClass="confirm-div" runat="server" Visible="false">
               <asp:Literal ID="liConfirmCheckBox" runat="server"></asp:Literal>
               <label for="cb_confirm_<%=ModuleId %>"><asp:Literal ID="liConfirmText" runat="server"></asp:Literal></label>
           </asp:Panel>
          <p  class="xblog_Subscription_btn">
               <asp:Button runat="server" ID="butNoLoginSubscription" Text="Subscription" resourcekey="butNoLoginSubscription"  lang="Submit"  onclick="butSubscription_Click" />
          </p>
   </div>
</asp:Panel>
<asp:Panel ID="pSubscribedNo" runat="server" Visible="false">
    <p><strong>You're Almost Done - Activate Your Subscription!</strong> </p>   
    <p>An email that contains a confirm link has just been sent to you.</p>
    <p>In order to activate your subscription, please check your email and click on the link in the email received. You will not receive your subscription until you click that link to activate it.</p>
    <p>If you don't see that email in your inbox shortly, fill out the form again to have another copy of it sent to you.</p>
</asp:Panel>

<asp:Panel ID="pSubscribed" runat="server" Visible="false">
    <%=ViewResourceText("Subscribed_Title", "The email you are using is already subscribed.  ")%>   
    <asp:Button runat="server" ID="butUnsubscribe" Text="Unsubscribe" resourcekey="butUnsubscribe"  onclick="butUnsubscribe_Click" />
</asp:Panel>
<asp:Literal ID="liContent" runat="server"></asp:Literal>
<asp:Label ID="lblMessage" runat="server"></asp:Label>


 <script type="text/javascript">
     jQuery(function (q) {
         q(".form_div_<%=ModuleId %>").validationEngine({
             promptPosition: "topRight"
         });

         q(".form_div_<%=ModuleId %> input[lang='Submit']").click(function () {
             if (!$('.form_div_<%=ModuleId %>').validationEngine('validate')) {
                 return false;
             }
         });




     });
     function CancelValidation() {
         jQuery('#Form').validationEngine('detach');
     }

    <%if (ViewSettingT<Boolean>("xBlog_Recaptcha_v3_Enable", false)) {%>

            function setgrecaptcha<%=ModuleId %>() {
                grecaptcha.execute('<%=ViewSettingT<String>("xBlog_Recaptcha_v3_SiteKey", "")%>', { action: 'page<%=TabId%>/module<%=ModuleId%>' }).then(function (token) {
                    $("#<%=hfGoogleRecaptchaResponse.ClientID%>").val(token);
                    //console.log("token-val:", $("#<%=hfGoogleRecaptchaResponse.ClientID%>").val());
                });
            }

            grecaptcha.ready(function () {
                setgrecaptcha<%=ModuleId %>();
                ref = setInterval(function () {
                    setgrecaptcha<%=ModuleId %>();
                    //console.log("setInterval-setgrecaptcha", new Date());
                }, 1000 * 60 * 2);
            });
     <%}%>

</script>