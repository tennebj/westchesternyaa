﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Manager_RssFeed.ascx.cs" Inherits="DNNGo.Modules.xBlog.Manager_RssFeed" %>
<!-- start: PAGE HEADER -->
      <div class="row">
        <div class="col-sm-12"> 
          <!-- start: PAGE TITLE & BREADCRUMB -->
          
          <div class="page-header">
            <h1><i class="fa fa-rss"></i> <%=ViewResourceText("Header_Title", "RssFeed Source")%> <asp:HyperLink ID="hlAddNewLink" runat="server" CssClass="btn btn-xs btn-bricky" Text="<i class='fa fa-plus'></i> Add New "  resourcekey="hlAddNew"></asp:HyperLink></h1>
          </div>
          <!-- end: PAGE TITLE & BREADCRUMB --> 
        </div>
      </div>
      <!-- end: PAGE HEADER --> 
       <!-- start: PAGE CONTENT -->
      
      <div class="row">
        <div class="col-sm-11">
          
          <div class="panel panel-default">
            <div class="panel-heading"> <i class="fa fa-external-link-square"></i> <%=ViewResourceText("Title_Schedule", "RSS Feed Schedule")%>
              <div class="panel-tools"> <a href="#" class="btn btn-xs btn-link panel-collapse collapses"> </a> </div>
            </div>
            <div class="panel-body">
              <div class="row">
                <div class="form-horizontal">
                     <div class="form-group">
                        <%=ViewControlTitle("lblScheduleEnabled", "Schedule Enabled", "cbScheduleEnabled", ":", "col-sm-3 control-label")%>
                        <div class="col-sm-6">
                            <asp:CheckBox ID="cbScheduleEnabled" runat="server" />
                        </div>
                      </div>
                     <div class="form-group">
                        <%=ViewControlTitle("lblTimeLapse", "Time Lapse", "txtTimeLapse", ":", "col-sm-3 control-label")%>
                        <div class="col-sm-6">
                            <asp:TextBox runat="server" ID="txtTimeLapse" Width="50px" CssClass="input_text validate[required,custom[integer]]"></asp:TextBox>
                            <%=ViewHelp("lblTimeLapse", "Hour")%>
                        </div>
                      </div>
                     <div class="form-group">
                        <%=ViewControlTitle("lblRetryFrequency", "Retry Frequency", "txtRetryFrequency", ":", "col-sm-3 control-label")%>
                        <div class="col-sm-6">
                            <asp:TextBox runat="server" ID="txtRetryFrequency" Width="50px" CssClass="input_text validate[required,custom[integer]]"></asp:TextBox>
                            <%=ViewHelp("lblRetryFrequency", "Minutes")%>
                        </div>
                      </div>

                     <div class="form-group">
                        <%=ViewControlTitle("lblRetainScheduleHistory", "Retain Schedule History", "txtRetainScheduleHistory", ":", "col-sm-3 control-label")%>
                        <div class="col-sm-6">
                            <asp:TextBox runat="server" ID="txtRetainScheduleHistory" Width="50px" CssClass="input_text validate[required,custom[integer]]"></asp:TextBox>
                            <%=ViewHelp("lblRetainScheduleHistory", "Items")%>
                        </div>
                      </div>
                </div>
              </div>
            </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading"> <i class="fa fa-external-link-square"></i> <%=ViewResourceText("Title_ArticleStatus", "Article Status")%>
              <div class="panel-tools"> <a href="#" class="btn btn-xs btn-link panel-collapse collapses"> </a> </div>
            </div>
            <div class="panel-body">
              <div class="row">
                <div class="form-horizontal">
                     <div class="form-group">
                        <%=ViewControlTitle("lblPublish", "Publish", "cbPublish", ":", "col-sm-3 control-label")%>
                        <div class="col-sm-6">
                            <asp:CheckBox ID="cbPublish" runat="server" />
                        </div>
                      </div>
                 
                </div>
              </div>
            </div>
          </div>

      <div class="row">
        <div class="col-sm-2"> </div>
        <div class="col-sm-10">
            
         <asp:Button CssClass="btn btn-primary" lang="Submit" ID="cmdUpdate" resourcekey="cmdUpdate"
        runat="server" Text="Update" OnClick="cmdUpdate_Click"></asp:Button>&nbsp;
        <asp:Button CssClass="btn btn-default" ID="cmdCancel" resourcekey="cmdCancel" runat="server"
            Text="Cancel" CausesValidation="False" OnClick="cmdCancel_Click"  OnClientClick="CancelValidation();"></asp:Button>&nbsp;

         </div>
      </div>

          
          

        </div>
      </div>
      
      <!-- end: PAGE CONTENT--> 

<div class="row">
    <div class="col-sm-12">
         <div class="form-group">
            <div class="row">
              <div class="col-sm-6">
                <asp:HyperLink runat="server" ID="hlAddNew" CssClass="btn btn-xs btn-bricky" Text="<i class='fa fa-plus'></i> Add New" resourcekey="hlAddNew"></asp:HyperLink>  
              </div>
            </div>
          </div>
        <div class="form-group">
            <asp:GridView ID="gvArticleList" runat="server" AutoGenerateColumns="False" OnRowDataBound="gvArticleList_RowDataBound"
                Width="100%" CellPadding="0" cellspacing="0" border="0" CssClass="table table-striped table-bordered table-hover"  GridLines="none" >
                <RowStyle CssClass="td_row" />
                <Columns>
                    <asp:BoundField DataField="ID" HeaderText="ID" HeaderStyle-Width="50" /> 
                    <asp:BoundField DataField="Title" HeaderText="Title" /> 
                    <asp:BoundField DataField="categories" HeaderText="Categorie"  HeaderStyle-Width="90" /> 
                    <asp:BoundField DataField="Author" HeaderText="Author" HeaderStyle-CssClass="hidden-xs" ItemStyle-CssClass="hidden-xs" />  
                    <asp:BoundField DataField="GetNum" HeaderText="Article Num" HeaderStyle-CssClass="hidden-xs" ItemStyle-CssClass="hidden-xs" /> 
                    <asp:BoundField DataField="ErrorNum" HeaderText="Error" HeaderStyle-CssClass="hidden-xs" ItemStyle-CssClass="hidden-xs" /> 
                    <asp:BoundField DataField="GetLastTime" HeaderText="Last Time" HeaderStyle-CssClass="hidden-xs" ItemStyle-CssClass="hidden-xs" /> 
                    <asp:BoundField DataField="Activation" HeaderText="Status" /> 
                    <asp:TemplateField HeaderText="Action" HeaderStyle-Width="80">
                        <ItemTemplate>
                             <div class="visible-md visible-lg hidden-sm hidden-xs">
                                        <asp:HyperLink ID="hlEdit" runat="server" CssClass="btn btn-xs btn-teal tooltips" data-original-title="Edit" data-placement="top" Text="<i class='fa fa-edit'></i>"></asp:HyperLink>
                                        <asp:LinkButton ID="btnRemove" runat="server" CssClass="btn btn-xs btn-bricky tooltips" data-original-title="Remove" data-placement="top" Text="<i class='fa fa-times fa fa-white'></i>" OnClick="imbDelete_Command"></asp:LinkButton>
                                         
                                     </div>
                                    <div class="visible-xs visible-sm hidden-md hidden-lg">
                                      <div class="btn-group"> <a href="#" data-toggle="dropdown" class="btn btn-primary dropdown-toggle btn-sm"> <i class="fa fa-cog"></i> <span class="caret"></span> </a>
                                        <ul class="dropdown-menu pull-right" role="menu">
                                          <li role="presentation">  <asp:HyperLink ID="hlMobileEdit" runat="server" tabindex="-1" role="menuitem" Text="<i class='fa fa-edit'></i> Edit"></asp:HyperLink></li>
                                          <li role="presentation"> <asp:LinkButton ID="btnMobileRemove" runat="server" tabindex="-1" role="menuitem" data-placement="top" Text="<i class='fa fa-times'></i> Remove"  OnClick="imbDelete_Command"></asp:LinkButton></li>
                                        </ul>
                                      </div>
                                    </div>

 
                        </ItemTemplate>
                        <HeaderStyle></HeaderStyle>
                    </asp:TemplateField>
                </Columns>
                <PagerSettings Visible="False" />
                <FooterStyle  />
                <PagerStyle  />
                <SelectedRowStyle  />
                <HeaderStyle  />
                <AlternatingRowStyle CssClass="alternating_row"  />
            </asp:GridView>
            <ul id="paginator-ArticleList" class="pagination-purple"></ul>
            <script type="text/javascript">
                $(document).ready(function () {
                    $('#paginator-ArticleList').bootstrapPaginator({
                        bootstrapMajorVersion: 3,
                        currentPage: <%=PageIndex %>,
                        totalPages: <%=RecordPages %>,
                        numberOfPages:7,
                        useBootstrapTooltip:true,
                        onPageClicked: function (e, originalEvent, type, page) {
                            window.location.href='<%=CurrentUrl %>&PageIndex='+ page;
                        }
                    });
                });
            </script>

        </div>
    </div>
</div>

 

