﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Manager_Options.ascx.cs" Inherits="DNNGo.Modules.xBlog.Manager_Options" %>
<%@ Register TagPrefix="dnn" Namespace="DotNetNuke.UI.WebControls" Assembly="DotNetNuke" %>
<%@ Register TagPrefix="dnn" Namespace="DotNetNuke.Security.Permissions.Controls" Assembly="DotNetNuke" %>
<!-- start: PAGE HEADER -->
      <div class="row">
        <div class="col-sm-12"> 
          <!-- start: PAGE TITLE & BREADCRUMB -->
          
          <div class="page-header">
            <h1><i class="fa fa-wrench"></i> <%=ViewResourceText("Header_Title", "General Settings")%></h1>
          </div>
          <!-- end: PAGE TITLE & BREADCRUMB --> 
        </div>
      </div>
      <!-- end: PAGE HEADER --> 


      <!-- start: PAGE CONTENT -->
      
      <div class="row">
        <div class="col-sm-12">
           <div class="panel panel-default">
            <div class="panel-body">
              <div class="row">



            <div class="form-horizontal">
              <div class="form-group">
                <%=ViewControlTitle("lblPageSize", "Articles per page", "txtPageSize",":", "col-sm-3 control-label")%>
                <div class="col-sm-8">
                    <asp:TextBox runat="server" ID="txtPageSize" Width="100px" CssClass="form-control form_default validate[required,custom[integer]]"></asp:TextBox>
                </div>
              </div>
     

          <%--   <div class="form-group" >
                <%=ViewControlTitle("lblEditor", "Editor", "ddlEditor", ":", "col-sm-3 control-label")%>
                <div class="col-sm-9">
                  <div class="control-inline"> <asp:DropDownList ID="ddlEditor" runat="server" CssClass="form-control form_default input_text validate[required]"></asp:DropDownList> </div>
                </div>
              </div> --%>

              <%--  <div class="form-group">
                <%=ViewControlTitle("lblMetaWeblog", "Enable MetaWeblog", "cbMetaWeblog", ":", "col-sm-3 control-label")%>
                <div class="col-sm-9">
                 <div class="checkbox-inline"><asp:CheckBox runat="server" ID="cbMetaWeblog"  CssClass="auto"/>      </div>
                   <%=ViewHelp("lblMetaWeblog", "")%><asp:Literal ID="liMetaWeblog" runat="server"></asp:Literal>
           
                </div>
              </div> --%>
                <div class="form-group">
                <%=ViewControlTitle("lblEnablePublishTime", "Enable Publish Time ", "cbEnablePublishTime", ":", "col-sm-3 control-label")%>
                <div class="col-sm-8">
                  <div class="checkbox-inline"> <asp:CheckBox ID="cbEnablePublishTime" runat="server" CssClass="auto"  ></asp:CheckBox> </div>
                </div>
              </div> 
              <div class="form-group">
                <%=ViewControlTitle("lblListSort", "Article list sorting", "rblListSort", ":", "col-sm-3 control-label")%>
                <div class="col-sm-8">
                    <div class="radio-inline">
                          <asp:RadioButtonList ID="rblListSort" runat="server" CssClass="auto"></asp:RadioButtonList>
                    </div>
                </div>
              </div> 
                 <div class="form-group">
                <%=ViewControlTitle("lblEnablePingService", "Enable Ping Service", "cbEnablePingService", ":", "col-sm-3 control-label")%>
                <div class="col-sm-8">
                <div class="checkbox-inline">
                    <asp:CheckBox runat="server" ID="cbEnablePingService"  CssClass="auto"/>
                 
                 </div>
                </div>
              </div> 

               <div class="form-group">
                <%=ViewControlTitle("lblPingService", "Ping Service", "txtPingService", ":", "col-sm-3 control-label")%>
                <div class="col-sm-8">
                  <asp:TextBox runat="server" ID="txtPingService" Width="95%" Height="120" TextMode="MultiLine"
                    Rows="8" CssClass=":max_length;2000"></asp:TextBox>
                    <%=ViewHelp("lblPingService", "When you publish a new post, xBlog automatically notifies the following site update services. For more about this, see <a target='_blank' href='http://codex.wordpress.org/Update_Services'>Update Services</a> on the Codex. Separate multiple service <abbr title='Universal Resource Locator'>URL</abbr>s with line breaks.")%>
                </div>
              </div> 

            


              
               <div class="form-group">
                <%=ViewControlTitle("lblSearchable", "Searchable", "cbSearchable", ":", "col-sm-3 control-label")%>
                <div class="col-sm-8">
                   <div class="checkbox-inline"> <asp:CheckBox ID="cbSearchable" runat="server" CssClass="auto" /></div>
                </div>
              </div>
               <div class="form-group">
                <%=ViewControlTitle("lblSearchableCount", "Searchable Count", "txtSearchableCount", ":", "col-sm-3 control-label")%>
                <div class="col-sm-8">
                    <asp:TextBox runat="server" ID="txtSearchableCount" Width="100px" CssClass="form-control form_default validate[required,custom[integer]]"></asp:TextBox>
                </div>
              </div>
              <div class="form-group">
                <%=ViewControlTitle("lblSearchText", "Search Text", "cbSearchSummary", ":", "col-sm-3 control-label")%>
                <div class="col-sm-8">
                   <div class="checkbox-inline"> 
                    <asp:CheckBox ID="cbSearchTitle" runat="server" CssClass="auto" resourcekey="cbSearchTitle" Text="Title"  Checked="true" Enabled="false" /> 
                    </div>
                      <div class="checkbox-inline"> 
                    <asp:CheckBox ID="cbSearchSummary" runat="server" CssClass="auto" resourcekey="cbSearchSummary" Text="Summary" /> 
                    </div>
                     <div class="checkbox-inline"> 
                    <asp:CheckBox ID="cbSearchContentText" runat="server" CssClass="auto" resourcekey="cbSearchContentText" Text="Content Text" /> 
                    </div>
                    <div class="checkbox-inline"> 
                    <asp:CheckBox ID="cbSearchTags" runat="server" CssClass="auto" resourcekey="cbSearchTags" Text="Tags" /> 
                    </div>
                </div>
              </div>
         

            </div>
          </div>
          </div>
          </div>
                     
            
            
            
            
            
            
        <div class="panel panel-default">
            <div class="panel-heading"> <i class="fa fa-external-link-square"></i> <%=ViewResourceText("Title_ArticleDetailSettings", "Article Detail Settings")%>
              <div class="panel-tools"> <a href="#" class="btn btn-xs btn-link panel-collapse collapses"> </a> </div>
            </div>
            <div class="panel-body">
              <div class="row">
                <div class="form-horizontal">

                     <div class="form-group">
                        <%=ViewControlTitle("lblDetailPage", "Detail of page", "rblDetailPage", ":", "col-sm-3 control-label")%>
                        <div class="col-sm-9">
                            <div class="radio-inline">
                               <asp:RadioButtonList ID="rblDetailPage" runat="server" CssClass="auto"></asp:RadioButtonList>
                            </div>
                           <%=ViewHelp("lblDetailPage", "You need to install DNNGo.xBlog.ArticleDetails module, and set it relate to the main module.", "rblDetailPage")%>
                        </div>
                      </div>

                     <div class="form-group">
                        <%=ViewControlTitle("lblDetailPath", "Article Detail Module Path", "DetailPath", ":", "col-sm-3 control-label")%>
                        <div class="col-sm-9">
                           <asp:HyperLink ID="hlDetailPath" runat="server" Target="_blank" CssClass="btn btn-link"></asp:HyperLink>
                        </div>
                      </div>
                  
                </div>
              </div>
            </div>
          </div>





          
          <div class="panel panel-default">
            <div class="panel-heading"> <i class="fa fa-external-link-square"></i> <%=ViewResourceText("Title_AuthorSettings", "Author Settings")%>
              <div class="panel-tools"> <a href="#" class="btn btn-xs btn-link panel-collapse collapses"> </a> </div>
            </div>
            <div class="panel-body">
              <div class="row">
                <div class="form-horizontal">
                     <div class="form-group">
                        <%=ViewControlTitle("lblAuthorRoles", "Author Roles", "cblAuthorRoles", ":", "col-sm-3 control-label")%>
                        <div class="col-sm-9">
                          
                             <%=ViewHelp("lblAuthorRoles", "Author permisson needs to give this role to module edit permission, please check setting image <a target='_blank' href='http://www.dnngo.net/images/AuthorRoles.jpg'>[open]</a> .")%>
                        </div>
                      </div>
                     <div class="form-group">
                        <%=ViewControlTitle("lblArticleCheck", "Review new article", "cbArticleCheck", ":", "col-sm-3 control-label")%>
                        <div class="col-sm-9">
                            <div class="checkbox-inline"><asp:CheckBox ID="cbArticleCheck" runat="server"   CssClass="auto"/></div>
                           <%=ViewHelp("lblArticleCheck", "It needs the administrator to review new article posts.", "cbArticleCheck")%>
                        </div>
                      </div>
                      <div class="form-group">
                        <%=ViewControlTitle("lblReviewNotification", "Review reminder", "cbReviewNotification", ":", "col-sm-3 control-label")%>
                        <div class="col-sm-9">
                            <div class="checkbox-inline"><asp:CheckBox ID="cbReviewNotification" runat="server"   CssClass="auto"/></div>
                           <%=ViewHelp("lblReviewNotification", "Send review reminder email of the new article to module administrator.", "cbReviewNotification")%>
                        </div>
                      </div>
                     <div class="form-group">
                        <%=ViewControlTitle("lblManagerModule", "Management Module Roles", "cbArticleCheck", ":", "col-sm-3 control-label")%>
                        <div class="col-sm-9">
                            <div class="checkbox-inline">
                                <asp:CheckBoxList ID="cblManagerModule" runat="server"  CssClass="auto"></asp:CheckBoxList>
                                </div>
                            <%=ViewHelp("lblManagerModule", "")%>
                          
                        </div>
                      </div>
                </div>
              </div>
            </div>
          </div>


          <div class="panel panel-default">
            <div class="panel-heading"> <i class="fa fa-external-link-square"></i> <%=ViewResourceText("Title_AvatarsSettings", "Avatars Settings")%>
              <div class="panel-tools"> <a href="#" class="btn btn-xs btn-link panel-collapse collapses"> </a> </div>
            </div>
            <div class="panel-body">
              <div class="row">
                <div class="form-horizontal">

                     <div class="form-group">
                        <%=ViewControlTitle("lblAvatarType", "Avatar Type", "rblAvatarType", ":", "col-sm-3 control-label")%>
                        <div class="col-sm-9">
                            <div class="radio-inline"> <asp:RadioButtonList ID="rblAvatarType" runat="server"  CssClass="auto"></asp:RadioButtonList></div>
                        </div>
                      </div>


                     <div class="form-group">
                        <div class="col-sm-3"></div>
                        <div class="col-sm-9">
                            <%=ViewResourceText("lblAvatarsSetting", "An avatar is an image that follows you from weblog to weblog appearing beside your name when you comment on avatar enabled sites. Here you can enable the display of avatars for people who comment on your site.")%>
                        </div>
                      </div>
                     <div class="form-group">
                        <%=ViewControlTitle("lblAvatarRating", "Avatar Maximum Rating", "rblAvatarRating", ":", "col-sm-3 control-label")%>
                        <div class="col-sm-9">
                            <div class="radio-inline"> <asp:RadioButtonList ID="rblAvatarRating" runat="server"  CssClass="auto"></asp:RadioButtonList></div>
                        </div>
                      </div>
                     <div class="form-group">
                        <%=ViewControlTitle("lblDefaultAvatar", "Default Avatar", "rblDefaultAvatar", ":", "col-sm-3 control-label")%>
                        <div class="col-sm-9">
                             <div class="radio-inline"> <asp:RadioButtonList ID="rblDefaultAvatar" runat="server"  CssClass="auto"></asp:RadioButtonList></div>
                             <%=ViewHelp("lblDefaultAvatar", "For users without a custom avatar of their own, you can either display a generic logo or a generated one based on their e-mail address.")%>
                        </div>
                      </div>

                     <div class="form-group">
                        <%=ViewControlTitle("lblAvatarSize", "Avatar Size", "txtAvatarSize", ":", "col-sm-3 control-label")%>
                        <div class="col-sm-9">
                            <asp:TextBox runat="server" ID="txtAvatarSize" Width="100px" CssClass="form-control validate[required,custom[integer]]"></asp:TextBox>
                            <%=ViewHelp("lblAvatarSize", "px")%>
                        </div>
                      </div>
                

                </div>
              </div>
            </div>
          </div>


           <div class="panel panel-default">
            <div class="panel-heading"> <i class="fa fa-external-link-square"></i> <%=ViewResourceText("Title_RssFeedsSettings", "Rss Feeds Settings")%>
              <div class="panel-tools"> <a href="#" class="btn btn-xs btn-link panel-collapse collapses"> </a> </div>
            </div>
            <div class="panel-body">
              <div class="row">
                <div class="form-horizontal">
                      
      
                      <div class="form-group">
                        <%=ViewControlTitle("lblFeedSize", "Synidication feeds display the latest", "txtFeedSize", ":", "col-sm-3 control-label")%>
                        <div class="col-sm-9">
                            <asp:TextBox runat="server" ID="txtFeedSize" Width="100px" CssClass="form-control validate[required,custom[integer]]"></asp:TextBox>
                            <%=ViewHelp("lblFeedSize", "items")%>
                        </div>
                      </div>


                      <div class="form-group">
                        <%=ViewControlTitle("lblFeedExcerpt", "Display Excerpt", "rblFeedExcerpt", ":", "col-sm-3 control-label")%>
                        <div class="col-sm-9">
                          <div class="radio-inline"><asp:RadioButtonList ID="rblFeedExcerpt" runat="server" RepeatDirection="Vertical" CssClass="auto"></asp:RadioButtonList></div>
                        </div>
                      </div> 

                     <div class="form-group">
                        <%=ViewControlTitle("lblFeedFillExcerpt", "Fill Excerpt", "cbFeedFillExcerpt", ":", "col-sm-3 control-label")%>
                        <div class="col-sm-9">
                          <div class="checkbox-inline">  <asp:CheckBox ID="cbFeedFillExcerpt" runat="server"  CssClass="auto"/></div>
                            <%=ViewHelp("lblFeedFillExcerpt", "When selecting to disaply aricle summary but with summary being not filled, it will use article details to fill in.")%>
                        </div>
                      </div>


                      <div class="form-group">
                        <%=ViewControlTitle("lblFeedDisplayReadMore", "Display Read more", "cbFeedDisplayReadMore", ":", "col-sm-3 control-label")%>
                        <div class="col-sm-9">
                            <div class="checkbox-inline"> <asp:CheckBox ID="cbFeedDisplayReadMore" runat="server"  CssClass="auto"/></div>
                            <%=ViewHelp("lblFeedDisplayReadMore", "")%>
                        </div>
                      </div>
                      <div class="form-group">
                        <%=ViewControlTitle("lblFeedDisplayPicture", "Display Picture", "cbFeedDisplayPicture", ":", "col-sm-3 control-label")%>
                        <div class="col-sm-9">
                          <div class="checkbox-inline">  <asp:CheckBox ID="cbFeedDisplayPicture" runat="server"  CssClass="auto"/></div>
                            <%=ViewHelp("lblFeedDisplayPicture", "")%>
                        </div>
                      </div>
                     <div class="form-group">
                        <%=ViewControlTitle("lblFeedPictureLink", "Picture Link", "rblFeedPictureLink", ":", "col-sm-3 control-label")%>
                        <div class="col-sm-9">
                          <div class="radio-inline">  
                              <asp:RadioButtonList ID="rblFeedPictureLink" runat="server" CssClass="auto"></asp:RadioButtonList>
                          </div>
                            <%=ViewHelp("lblFeedPictureLink", "")%>
                        </div>
                      </div>


                     <div class="form-group">
                        <%=ViewControlTitle("lblRssStylesheet", "Rss Stylesheet", "txtRssStylesheet", ":", "col-sm-3 control-label")%>
                        <div class="col-sm-8">
                            <asp:TextBox runat="server" ID="txtRssStylesheet" Width="450px" CssClass="form-control validate[max[512]]"></asp:TextBox>
                             <asp:Literal ID="liRssStylesheet" runat="server"></asp:Literal>
            
                        </div>
                      </div>
                      <div class="form-group">
                        <%=ViewControlTitle("lblRssStylexsl", "Rss Style xsl", "txtRssStylexsl", ":", "col-sm-3 control-label")%>
                        <div class="col-sm-8">
                            <asp:TextBox runat="server" ID="txtRssStylexsl" Width="450px" CssClass="form-control validate[max[512]]"></asp:TextBox>
                            <asp:Literal ID="liRssStylexsl" runat="server"></asp:Literal>
                 
                        </div>
                     </div>
                </div>
              </div>
            </div>
          </div>



          <div class="panel panel-default">
            <div class="panel-heading"> <i class="fa fa-external-link-square"></i> <%=ViewResourceText("Title_SitemapSettings", "Sitemap Settings")%>
              <div class="panel-tools"> <a href="#" class="btn btn-xs btn-link panel-collapse collapses"> </a> </div>
            </div>
            <div class="panel-body">
              <div class="row">
                <div class="form-horizontal">
                      
                     <div class="form-group">
                        <%=ViewControlTitle("lblSitemapSize", "Synidication sitemap display the latest", "txtSitemapSize", ":", "col-sm-3 control-label")%>
                        <div class="col-sm-8">
                            <asp:TextBox runat="server" ID="txtSitemapSize" Width="50px" CssClass="form-control form_default validate[required,custom[integer],max[5000]]"></asp:TextBox>
                        </div>
                      </div>
                     <div class="form-group">
                        <%=ViewControlTitle("lblSitemapPriority", "Sitemap Priority", "txtSitemapPriority", ":", "col-sm-3 control-label")%>
                        <div class="col-sm-8">
                             <asp:TextBox runat="server" ID="txtSitemapPriority" Width="50px" CssClass="form-control form_default validate[required,custom[number],max[1]]"></asp:TextBox>
                        </div>
                      </div>
                     <div class="form-group">
                        <%=ViewControlTitle("lblSitemapChangefreq", "Sitemap Changefreq", "ddlSitemapChangefreq", ":", "col-sm-3 control-label")%>
                        <div class="col-sm-8">
                            <asp:DropDownList runat="server" ID="ddlSitemapChangefreq" CssClass="form-control form_default"></asp:DropDownList>
                        </div>
                      </div>
                    <div class="form-group">
                        <%=ViewControlTitle("lblSitemapUrl", "SiteMap Url", "liSitemapUrl", ":", "col-sm-3 control-label")%>
                        <div class="col-sm-8">
                            <asp:Literal ID="liSitemapUrl" runat="server"></asp:Literal>
                        </div>
                      </div>
                </div>
              </div>
            </div>
          </div>

       <div class="row">
        <div class="col-sm-3"> </div>
        <div class="col-sm-9">
            
         <asp:Button CssClass="btn btn-primary" lang="Submit" ID="cmdUpdate" resourcekey="cmdUpdate"
        runat="server" Text="Update" OnClick="cmdUpdate_Click"></asp:Button>&nbsp;
        <asp:Button CssClass="btn btn-default" ID="cmdCancel" resourcekey="cmdCancel" runat="server"
            Text="Cancel" CausesValidation="False" OnClick="cmdCancel_Click"  OnClientClick="CancelValidation();"></asp:Button>&nbsp;

         </div>
      </div>

          

        </div>
      </div>
      
      <!-- end: PAGE CONTENT--> 


    