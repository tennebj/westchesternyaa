﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Resource_Newsletter.ascx.cs"
    Inherits="DNNGo.Modules.xBlog.Resource_Newsletter1" %>

<!-- start: PAGE CONTENT -->
<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i> 
                <%=ViewResourceText("Title_SendSettings", "Send Settings")%>
                <div class="panel-tools">
                    <a href="#" class="btn btn-xs btn-link panel-collapse collapses"></a>
                </div>
            </div>
            <div class="panel-body">
                <div class="form-horizontal">
                    <div class="form-group">
                        <%=ViewControlTitle("lblRoles", "Roles", "hlRoles", ":", "col-sm-3 control-label")%>
                        <div class="col-sm-5">
                            <asp:DropDownList ID="ddlRoles" CssClass="form-control form_default" runat="server" datavaluefield="RoleID" datatextfield="RoleName"></asp:DropDownList>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i>
                <%=ViewResourceText("Title_Preview", "Preview")%>
                <div class="panel-tools">
                    <a href="#" class="btn btn-xs btn-link panel-collapse collapses"></a>
                </div>
            </div>
            <div class="panel-body">
                <div class="form-horizontal">
                    <div class="form-group">
                        <%=ViewControlTitle("lblTitle", "Title", "lbShowTitle", ":", "col-sm-3 control-label")%>
                        <div class="col-sm-5">
                            <asp:Literal ID="lbShowTitle" runat="server"></asp:Literal>
               
                        </div>
                    </div>
                     <div class="form-group">
                        <%=ViewControlTitle("lblContent", "Content", "lbShowContent", ":", "col-sm-3 control-label")%>
                        <div class="col-sm-5">
                            <asp:Literal ID="lbShowContent" runat="server"></asp:Literal>
                        </div>
                    </div>
                </div>
            </div>
        </div>






    </div>
</div>

      <div class="row">
        <div class="col-sm-2"> </div>
        <div class="col-sm-10">
          <asp:Button CssClass="btn btn-primary" lang="Submit" ID="cmdSend" resourcekey="cmdSend"
        runat="server" Text="Send" OnClick="cmdSend_Click"></asp:Button>&nbsp;
        <asp:Button CssClass="btn btn-default" ID="cmdCancel" resourcekey="cmdCancel" runat="server"
            Text="Cancel" CausesValidation="False" OnClick="cmdCancel_Click"  OnClientClick=" window.parent.$('#Newsletter_Modal').modal('hide');return false;"></asp:Button>&nbsp;
        
         </div>
      </div>


