﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Manager_Option_AddThis.ascx.cs" Inherits="DNNGo.Modules.xBlog.Manager_Option_AddThis" %>
      <!-- start: PAGE HEADER -->
      <div class="row">
        <div class="col-sm-12"> 
          <!-- start: PAGE TITLE & BREADCRUMB -->
          
          <div class="page-header">
            <h1><i class="fa fa-share-square-o"></i> <%=ViewResourceText("Header_Title", "AddThis Settings")%></h1>
          </div>
          <!-- end: PAGE TITLE & BREADCRUMB --> 
        </div>
      </div>
      <!-- end: PAGE HEADER -->
            <!-- start: PAGE CONTENT -->
      
      <div class="row">
        <div class="col-sm-12">
    
            
          <div class="panel panel-default">
            <div class="panel-heading"> <i class="fa fa-external-link-square"></i> <%=ViewResourceText("Title_BaseSettings", "Basic Settings")%>
              <div class="panel-tools"> <a href="#" class="btn btn-xs btn-link panel-collapse collapses"> </a> </div>
            </div>
            <div class="panel-body">
                <div class="form-horizontal">
                  <div class="form-group hide">
                    <%=ViewControlTitle("lblSharingToolEnable", "Sharing tool enable", "cbSharingToolEnable",":", "col-sm-3 control-label")%>
                    <div class="col-sm-5">
                        <div class="control-inline"><asp:CheckBox runat="server" ID="cbSharingToolEnable" /></div>
                    </div>
                  </div>
                  <div class="form-group">
                    <%=ViewControlTitle("lblSharingTool", "Choose the sharing tool to display the post", "rblSharingTool",":", "col-sm-3 control-label")%>
                    <div class="col-sm-5">
                      <asp:RadioButtonList runat="server" ID="rblSharingTool" RepeatDirection="Vertical"></asp:RadioButtonList>
                    </div>
                  </div>
                  <div class="form-group"  id="CustomSharingTool">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-5">
                      <asp:TextBox runat="server" ID="txtCustomSharingTool" Width="95%" Height="120" TextMode="MultiLine" Rows="8" CssClass=":max_length;2000"></asp:TextBox>
                      <%=ViewHelp("lblCustomSharingTool", "This text box allows you to enter any AddThis markup that you wish. To see examples of what you can do, visit <a href='https://www.addthis.com/get/sharing'>AddThis.com Sharing Tools</a> and select any sharing tool. You can also check out our <a href='http://support.addthis.com/customer/portal/articles/381263-addthis-client-api#rendering-decoration'>Client API</a>. For any help you may need, please visit <a href='http://support.addthis.com'>AddThis Support</a>")%>
                    </div>
                  </div>
                  
                </div>
            </div>
          </div>


           <div class="panel panel-default">
            <div class="panel-heading"> <i class="fa fa-external-link-square"></i> <%=ViewResourceText("Title_Analytics", "Analytics reports")%>
              <div class="panel-tools"> <a href="#" class="btn btn-xs btn-link panel-collapse collapses"> </a> </div>
            </div>
            <div class="panel-body">
                <div class="form-horizontal">
                  <div class="form-group">
                    <div class="col-sm-1"></div>
                    <div class="col-sm-9">
                       <%=ViewHelp("lblAnalyticsReports", "<a href='https://www.addthis.com/register?profile=wpp' target='_blank'>Register</a> for free in-depth analytics reports and better understand your site's social traffic.")%>
                    </div>
                  </div>
                  <div class="form-group">
                    <%=ViewControlTitle("lblAddThisProfileID", "AddThis profile ID", "txtAddThisProfileID",":", "col-sm-3 control-label")%>
                    <div class="col-sm-5">
                      <asp:TextBox runat="server" ID="txtAddThisProfileID" Width="160px" CssClass="form-control"></asp:TextBox>
                    </div>
                  </div>
                
                </div>
            </div>
          </div>
        </div>
      </div>
      <!-- end: PAGE CONTENT-->
      
      <div class="row">
        <div class="col-sm-2"> </div>
        <div class="col-sm-10">
         <asp:Button CssClass="btn btn-primary" lang="Submit" ID="cmdUpdate" resourcekey="cmdUpdate"
        runat="server" Text="Update" OnClick="cmdUpdate_Click"></asp:Button>&nbsp;
        <asp:Button CssClass="btn btn-default" ID="cmdCancel" resourcekey="cmdCancel" runat="server"
            Text="Cancel" CausesValidation="False" OnClick="cmdCancel_Click"  OnClientClick="CancelValidation();"></asp:Button>&nbsp;
        
         </div>
      </div>