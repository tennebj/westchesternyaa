﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Manager_DashBoardOptionEffect.ascx.cs" Inherits="DNNGo.Modules.xBlog.Setting_DashBoardOptionEffect" %>
 <link rel="stylesheet" href="<%=ModulePath %>Resource/plugins/jQuery-Tags-Input/jquery.tagsinput.css">
<script src="<%=ModulePath %>Resource/plugins/jQuery-Tags-Input/jquery.tagsinput.js"></script>
 <div class="form-group">
    <%=ViewControlTitle("lblDisplaySize", "Display number", "txtDisplaySize", ":", "col-sm-3 control-label")%>
   <div class="col-sm-6"> <asp:TextBox runat="server" ID="txtDisplaySize" CssClass="form-control form_default validate[required,custom[integer],max[1000],min[0]]"></asp:TextBox>
   </div>
</div>
 

<!--Album start-->
 <div class="form-group" >
    <%=ViewControlTitle("lblChooseCategories", "Choose Categories", "lboxChooseCategories", ":", "col-sm-3 control-label")%>
    <div class="col-sm-6"><asp:ListBox ID="lboxChooseCategories" runat="server" CssClass="form-control" Rows="10" DataTextField="Name" DataValueField="ID" SelectionMode="Multiple"></asp:ListBox></div>
</div>
 <div class="form-group" >
    <%=ViewControlTitle("lblSequence", "Order By", "rblSequence", ":", "col-sm-3 control-label")%>
    <div class="col-sm-6">
        <div class="radio-inline auto">
          
        <asp:RadioButtonList ID="rblSequence" runat="server" CssClass="" ></asp:RadioButtonList>
            </div>
    </div>
</div>
 <div class="form-group" >
    <%=ViewControlTitle("lblOrder", "Order", "rblOrder", ":", "col-sm-3 control-label")%>
    <div class="col-sm-6 ">
        <div class="radio-inline auto">
            <asp:RadioButtonList ID="rblOrder" runat="server" CssClass="" ></asp:RadioButtonList>

        </div>
    </div>
</div>
 
<div class="form-group"  >
    <%=ViewControlTitle("lblFeature", "Feature", "rblFeature", ":", "col-sm-3 control-label")%>
    <div class="col-sm-6 ">
        <div class="radio-inline auto">
            <asp:RadioButtonList ID="rblFeature" runat="server" CssClass=""></asp:RadioButtonList>
        </div>
    </div>
</div>
<div class="form-group"  >
    <%=ViewControlTitle("lblHideOldArticles", "Hide old articles", "cbHideOldArticles", ":", "col-sm-3 control-label")%>
    <div class="col-sm-6 ">
        <div class="radio-inline auto">
            <%--<asp:CheckBox ID="cbHideOldArticles" runat="server"  CssClass="auto"/>--%>
            <asp:RadioButtonList ID="rblHideOldArticles" runat="server" CssClass="" ></asp:RadioButtonList>
        </div>
    </div>
</div>

<div class="form-group"  >
    <%=ViewControlTitle("lblChooseTags", "Tags", "txtChooseTags", ":", "col-sm-3 control-label")%>
    <div class="col-sm-6 control-inline">
        <asp:TextBox ID="txtChooseTags" runat="server"></asp:TextBox>
    </div>
</div>
<!--Album start-->
<script type="text/javascript">
    jQuery(function ($) {

        $('#<%=txtChooseTags.ClientID %>').tagsInput({ width: 'auto', height: '100px' });
    });


</script>