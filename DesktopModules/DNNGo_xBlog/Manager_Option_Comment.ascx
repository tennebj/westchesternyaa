﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Manager_Option_Comment.ascx.cs" Inherits="DNNGo.Modules.xBlog.Manager_Option_Comment" %>
      <!-- start: PAGE HEADER -->
      <div class="row">
        <div class="col-sm-12"> 
          <!-- start: PAGE TITLE & BREADCRUMB -->
          
          <div class="page-header">
            <h1><i class="fa clip-bubbles-2"></i> <%=ViewResourceText("Header_Title", "Comment Settings")%></h1>
          </div>
          <!-- end: PAGE TITLE & BREADCRUMB --> 
        </div>
      </div>
      <!-- end: PAGE HEADER -->
            <!-- start: PAGE CONTENT -->
      
      <div class="row">
        <div class="col-sm-12">
          <div class="panel panel-default">
            <div class="panel-heading"> <i class="fa fa-external-link-square"></i> <%=ViewResourceText("Title_BaseSettings", "Basic Settings")%>
              <div class="panel-tools"> <a href="#" class="btn btn-xs btn-link panel-collapse collapses"> </a> </div>
            </div>
            <div class="panel-body">
                <div class="form-horizontal">
                  <div class="form-group">
                    <%=ViewControlTitle("lblEnabledComment", "Enable Comment", "rblEnabledComment", ":", "col-sm-3 control-label")%>
                    <div class="col-sm-5">
                      <div class="control-inline"><asp:RadioButtonList ID="rblEnabledComment" runat="server" RepeatDirection="Horizontal"></asp:RadioButtonList></div>
                    </div>
                  </div>
                  <div class="form-group">
                    <%=ViewControlTitle("lblLoginComment", "Require login to comment", "rblLoginComment", ":", "col-sm-3 control-label")%>
                    <div class="col-sm-5">
                      <div class="control-inline"><asp:RadioButtonList ID="rblLoginComment" runat="server" RepeatDirection="Horizontal"></asp:RadioButtonList></div>
                    </div>
                  </div>
                  <div class="form-group">
                    <%=ViewControlTitle("lblCommentNotification", "Enable Comment Notification", "rblCommentNotification", ":", "col-sm-3 control-label")%>
                    <div class="col-sm-5">
                     <div class="control-inline"> <asp:RadioButtonList ID="rblCommentNotification" runat="server" RepeatDirection="Horizontal"></asp:RadioButtonList></div>
                    </div>
                  </div>
                  <div class="form-group">
                    <%=ViewControlTitle("lblNumberComments", "Number of Comments per page", "txtNumberComments", ":", "col-sm-3 control-label")%>
                    <div class="col-sm-5">
                        <asp:TextBox runat="server" ID="txtNumberComments" Width="50px" CssClass="form-control form_default :required :number"></asp:TextBox>
                        <%=ViewHelp("lblNumberComments", "items")%>
                    </div>
                  </div>
                  <div class="form-group">
                    <%=ViewControlTitle("lblCommentInitialstate", "Comment Initial State", "ddlCommentInitialstate", ":", "col-sm-3 control-label")%>
                    <div class="col-sm-5">
                      <asp:DropDownList CssClass="form-control form_default"  ID="ddlCommentInitialstate" runat="server"></asp:DropDownList>
                    </div>
                  </div>
                 
                </div>
            </div>
          </div>


          <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i>
                <%=ViewTitle("lblDisqusSettings", "Disqus Settings")%>
                <div class="panel-tools">
                    <a href="#" class="btn btn-xs btn-link panel-collapse collapses"></a>
                </div>
            </div>
            <div class="panel-body">
                <div class="form-horizontal">

                 <div class="form-group">
                    <%=ViewControlTitle("lblCommentService", "Comment Service", "rblCommentService", ":", "col-sm-3 control-label")%>
                    <div class="col-sm-5">
                      <div class="control-inline"><asp:RadioButtonList ID="rblCommentService" runat="server" RepeatDirection="Horizontal"></asp:RadioButtonList></div>
                   
                    </div>
                  </div>


                  <div class="form-group">
                        <%=ViewControlTitle("lblDisqusShortname", "Disqus Shortname", "txtDisqusShortname", ":", "col-sm-3 control-label")%>
                        <div class="col-sm-5">
                            <div class="control-inline">
                                <asp:TextBox runat="server" ID="txtDisqusShortname" CssClass="form-control input_text validate[required]"></asp:TextBox>
                                <%=ViewHelp("lblDisqusShortname", "To turn on the server, you need to register a relevant account at disqus.com and set Disqus Short name. All feature setings are from disqus.com.")%>
                             </div>
                        </div>
                    </div>


                     
                     
                    
                </div>
            </div>
        </div>

          <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i>
                <%=ViewTitle("lblEncryptSettings", "Hidden fields encrypt Settings")%>
                <div class="panel-tools">
                    <a href="#" class="btn btn-xs btn-link panel-collapse collapses"></a>
                </div>
            </div>
            <div class="panel-body">
                <div class="form-horizontal">
                    <div class="form-group">
                        <%=ViewControlTitle("lblHiddenfieldsEnable", "Hidden fields", "cbHiddenfieldsEnable", ":", "col-sm-3 control-label")%>
                        <div class="col-sm-5">
                            <div class="control-inline">
                                <asp:CheckBox ID="cbHiddenfieldsEnable" runat="server" />
                             </div>
                        </div>
                    </div>



                   <div class="form-group">
                        <%=ViewControlTitle("lblEncryptionKey", "Encryption key", "txtEncryptionKey", ":", "col-sm-3 control-label")%>
                        <div class="col-sm-5">
                            <div class="control-inline">
                                <asp:TextBox runat="server" ID="txtEncryptionKey" Width="150px" CssClass="input_text validate[required,maxSize[20],minSize[8]]"></asp:TextBox>
                             </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <%=ViewControlTitle("lblVerifyStringLength", "Verify String Length", "txtVerifyStringLength", ":", "col-sm-3 control-label")%>
                        <div class="col-sm-5">
                            <div class="control-inline">
                                <asp:TextBox runat="server" ID="txtVerifyStringLength" Width="80px" CssClass="input_text validate[required,max[30],min[5]]"></asp:TextBox>
                             </div>
                        </div>
                    </div>


                    <div class="form-group" style="display:none">
                        <%=ViewControlTitle("lblVerifyIntervalTime", "Verify Interval Time", "txtVerifyIntervalTime", ":", "col-sm-3 control-label")%>
                        <div class="col-sm-5">
                            <div class="control-inline">
                                <asp:TextBox runat="server" ID="txtVerifyIntervalTime" Width="80px" CssClass="input_text validate[required,max[60],min[1]]"></asp:TextBox>
                                <%=ViewResourceText("lblVerifyIntervalTime", "Minute","Help")%>
                                
                            </div>
                        </div>
                    </div>
                     
                    
                </div>
            </div>
        </div>

           <div class="panel panel-default">
            <div class="panel-heading"> <i class="fa fa-external-link-square"></i> <%=ViewResourceText("Title_Antispam", "Anti-spam services")%>
              <div class="panel-tools"> <a href="#" class="btn btn-xs btn-link panel-collapse collapses"> </a> </div>
            </div>
            <div class="panel-body">
                <div class="form-horizontal">

                 <div class="form-group">
                    <%=ViewControlTitle("lblEnabledAnti-spamServices", "Enable Anti-spam services", "rblEnabledAnti_spamServices", ":", "col-sm-3 control-label")%>
                    <div class="col-sm-5">
                      <div class="control-inline"> <asp:RadioButtonList ID="rblEnabledAnti_spamServices" runat="server" RepeatDirection="Horizontal"></asp:RadioButtonList></div>
                    </div>
                  </div>

                <asp:Panel runat="server" ID="FilterServicesPanel">
                  <div class="form-group">
                    <%=ViewControlTitle("lblApiKey", "Api Key", "txtApiKey", ":", "col-sm-3 control-label")%>
                    <div class="col-sm-5">
                      <asp:TextBox runat="server" ID="txtApiKey" Width="250px" CssClass="form-control form_default"></asp:TextBox>
                       <asp:Panel runat="server" ID="palAkismetFilterApiKey" Visible="false">
                         <span class="help-block"><%=ViewHelp("lblAkismetFilterApiKey", "For many people, <a target='_blank' href='http://akismet.com/'>Akismet</a> will greatly reduce or even completely eliminate the comment and trackback spam you get on your site. If one does happen to get through, simply mark it as \"spam\" on the moderation screen and Akismet will learn from the mistakes. If you don't have an API key yet, you can get one at <a target=\"_blank\" href=\"http://akismet.com/get/\">Akismet.com</a>.")%></span>
                       </asp:Panel>
                    </div>
                  </div>
                  <div class="form-group">
                    <%=ViewControlTitle("lblSiteURL", "Site URL", "txtSiteURL", ":", "col-sm-3 control-label")%>
                    <div class="col-sm-5">
                        <asp:TextBox runat="server" ID="txtSiteURL" Width="250px" CssClass="form-control form_default"></asp:TextBox>
                        <%=ViewHelp("lblSiteURL", "Your blog URL")%>
                    </div>
                  </div>
                  </asp:Panel>
                  <div class="form-group">
                    
                    <div class="col-sm-12">
                      <asp:GridView ID="gvFilterServices" runat="server" AutoGenerateColumns="False" OnRowDataBound="gvFilterServices_RowDataBound"
                                Width="100%" CellPadding="6" CssClass="table table-striped table-bordered table-hover"  GridLines="none" >
                                <RowStyle />
                                <Columns>
                                 <asp:TemplateField HeaderText="Activated"  HeaderStyle-Width="5">
                                        <ItemTemplate>
                                        <asp:CheckBox ID="cbEnabled" runat="server" Enabled="false" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="FilterName" HeaderText="Filter Name"  /> 
                                    <asp:BoundField DataField="CheckCount" HeaderText="Check"  HeaderStyle-CssClass="hidden-xs" ItemStyle-CssClass="hidden-xs" /> 
                                    <asp:BoundField DataField="SpamCount" HeaderText="Spam"  HeaderStyle-CssClass="hidden-xs" ItemStyle-CssClass="hidden-xs" /> 
                                    <asp:BoundField DataField="ErrorCount" HeaderText="Error"  HeaderStyle-CssClass="hidden-xs" ItemStyle-CssClass="hidden-xs" /> 
                    
                                    <asp:TemplateField HeaderText="Action"  HeaderStyle-Width="80">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imbEnabled" runat="server" ToolTip="Enabled" OnCommand="imbEnabled_Command"  />
                                             &nbsp;
                                            <asp:ImageButton ID="imbRefresh" runat="server" ToolTip="Refresh" OnCommand="imbRefresh_Command" ImageUrl="~/images/action_refresh.gif" />
                                              
                                        </ItemTemplate>
                                        <HeaderStyle ></HeaderStyle>
                                    </asp:TemplateField>
                                </Columns>
                                <PagerSettings Visible="False" />
                                <FooterStyle  />
                                <PagerStyle  />
                                <SelectedRowStyle  />
                                <HeaderStyle  />
                  </asp:GridView>
                    </div>
                  </div>
                  
                </div>
            </div>
          </div>


        </div>
      </div>
      <!-- end: PAGE CONTENT-->
      
      <div class="row">
        <div class="col-sm-2"> </div>
        <div class="col-sm-10">
        <asp:Button CssClass="btn btn-primary" lang="Submit" ID="cmdUpdate" resourcekey="cmdUpdate"
        runat="server" Text="Update" OnClick="cmdUpdate_Click"></asp:Button>&nbsp;
        <asp:Button CssClass="btn btn-default" ID="cmdCancel" resourcekey="cmdCancel" runat="server"
            Text="Cancel" CausesValidation="False" OnClick="cmdCancel_Click"  OnClientClick="CancelValidation();"></asp:Button>&nbsp;
        
         </div>
      </div>