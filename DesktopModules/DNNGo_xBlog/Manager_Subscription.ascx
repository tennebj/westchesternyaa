﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Manager_Subscription.ascx.cs" Inherits="DNNGo.Modules.xBlog.Manager_Subscription" %>
 <!-- start: PAGE HEADER -->
      <div class="row">
        <div class="col-sm-12"> 
          <!-- start: PAGE TITLE & BREADCRUMB -->
          
          <div class="page-header">
            <h1><i class="fa fa-plus"></i> <%=ViewResourceText("Header_Title", "Add New Role & Email")%></h1>
          </div>
          <!-- end: PAGE TITLE & BREADCRUMB --> 
        </div>
      </div>
      <!-- end: PAGE HEADER -->
            <!-- start: PAGE CONTENT -->
      
      <div class="row">
        <div class="col-sm-12">
    
            
          <div class="panel panel-default">
            <div class="panel-heading"> <i class="fa fa-external-link-square"></i> <%=ViewResourceText("Title_GeneralSettings", " General Settings")%>
              <div class="panel-tools"> <a href="#" class="btn btn-xs btn-link panel-collapse collapses"> </a> </div>
            </div>
            <div class="panel-body">
                <div class="form-horizontal">
                  <div class="form-group" id="divRole" runat="server" visible="false">
                    <%=ViewControlTitle("lblRole", "Role", "ddlRoleList", ":", "col-sm-3 control-label")%>
                    <div class="col-sm-5">
                      <asp:DropDownList ID="ddlRoleList" runat="server"  CssClass="form-control validate[required]"></asp:DropDownList>
                    </div>
                  </div>
                  <div class="form-group" id="divEmail" runat="server" visible="false">
                    <%=ViewControlTitle("lblUserEmail", "Email", "txtUserEmail", ":", "col-sm-3 control-label")%>
                    <div class="col-sm-5">
                      <asp:TextBox ID="txtUserEmail" runat="server"  CssClass="form-control validate[required,custom[email]]" Width="300"></asp:TextBox>
                    </div>
                  </div>
                  <div class="form-group">
                    <%=ViewControlTitle("lblBeginDate", "BeginDate", "txtBeginDate", ":", "col-sm-3 control-label")%>
                    <div class="col-sm-5">
                      <asp:TextBox ID="txtBeginDate" runat="server" CssClass="input_text :date_au" Width="100px"></asp:TextBox>
                    </div>
                  </div>
                  <div class="form-group">
                    <%=ViewControlTitle("lblEndDate", "EndDate", "txtEndDate", ":", "col-sm-3 control-label")%>
                    <div class="col-sm-5">
                       <asp:TextBox ID="txtEndDate" runat="server"  CssClass="input_text :date_au" Width="100px"></asp:TextBox>
                    </div>
                  </div>
                </div>
            </div>
          </div>
        </div>
      </div>
      <!-- end: PAGE CONTENT-->
      
      <div class="row">
        <div class="col-sm-2"> </div>
        <div class="col-sm-10">
        <asp:Button CssClass="btn btn-primary" lang="Submit" ID="cmdUpdate" resourcekey="cmdUpdate"
        runat="server" Text="Update" OnClick="cmdUpdate_Click"></asp:Button>&nbsp;
        <asp:Button CssClass="btn btn-default" ID="cmdCancel" resourcekey="cmdCancel" runat="server"
            Text="Cancel" CausesValidation="False" OnClick="cmdCancel_Click"  OnClientClick="CancelValidation();"></asp:Button>&nbsp;
        <asp:Button CssClass="btn btn-default" ID="cmdDelete" resourcekey="cmdDelete" runat="server"
         Text="Delete" Visible="false" CausesValidation="False" OnClick="cmdDelete_Click" OnClientClick="CancelValidation();"></asp:Button>&nbsp;
         </div>
      </div>


  <script type="text/javascript">
      jQuery(function () {
          var dates = jQuery("#<%=txtBeginDate.ClientID %>, #<%=txtEndDate.ClientID %>").datepicker({
              changeMonth: true, changeYear: true,
              onSelect: function (selectedDate) {
                  var option = this.id == "<%=txtBeginDate.ClientID %>" ? "minDate" : "maxDate",
					instance = jQuery(this).data("datepicker"),
					date = jQuery.datepicker.parseDate(
						instance.settings.dateFormat ||
						jQuery.datepicker._defaults.dateFormat,
						selectedDate, instance.settings);
                  dates.not(this).datepicker("option", option, date);
              }
          });
      });

</script>