(function($) {
	$.fn.verticalScroll = function(options) {
		var defaults = {
			autoPlay_speed: 40,
			step: 1,
			showitems: 3,
			rolltems: 3,
			itemsheight: "auto",
			navigation: true,
			pagination: false,
			infinite: true,
			speed: 800,
			autoPlay: 3000,
			easing:"linear",
			direction: "up",
			stopOnHover:true,
			content: {
				height: false,
				rollheight: false
			}
		}
		var o = $.extend(defaults, options);
		
		
		var e = $(this).children(".box-content"),
		box = e.children("ul.main"),
		items = box.children("li.item"),
		l = items.length,
		i = 0,
		auto,
		auto2,
		on = 0,
		pages=0;
		step=o.step;
		
		if(o.showitems > l){
			o.showitems = l
		}
		if (o.itemsheight != "auto") {
			items.height(o.itemsheight)
		}
		if (o.showitems < l) {
			h = items.eq(o.showitems).position().top;
		} else {
			h = box.outerHeight();
		}
		items.clone().addClass("copy").appendTo(box);
		items = box.children("li.item");
		e.height(h);
		o.showitems = 0 < o.showitems < l ? o.showitems: l;
		o.rolltems = 0 < o.rolltems < l ? o.rolltems: l;
		if (o.content.height) {
			e.height(o.content.height);
		}
		if (o.infinite) {
			function autoPlay() {
			
				if (o.direction == "up" ) {
					i = i + o.step;
					e.scrollTop(i)
					 if (e.scrollTop() >= box.children(".copy").eq(0).position().top) {
						i = e.scrollTop() - box.children(".copy").eq(0).position().top;
						e.scrollTop(i)
					}
				}else{
					i = i - o.step;
					e.scrollTop(i)
					 if (i <= 0) {
						i = box.children(".copy").eq(0).position().top;
						e.scrollTop(i)
					}
				}
			}
			auto = setInterval(autoPlay, o.autoPlay_speed);
			if(o.stopOnHover){
				e.on("mouseover",
				function() {
					clearInterval(auto)
				})
				 e.on("mouseout",
				function() {
					auto = setInterval(autoPlay, o.autoPlay_speed)
				})
			}
		}
		
		function start(type, number) {
			if (on == 1) return false;
			on = 1;
			if (o.content.rollheight) {
				if (type == "prev") {
					if (i - o.content.rollheight < 0) {
						e.scrollTop(box.children(".copy").eq(0).position().top + i)
						 i = box.children(".copy").eq(0).position().top + i - o.content.rollheight
					} else {
						i = i - o.content.rollheight
					}
				} else if (type == "page") {
					i = number * o.content.rollheight;
				} else {
					i = i + o.content.rollheight;
				}
				if (o.pagination) {
					dot.children("a").eq(number ? number: parseInt(i / o.content.rollheight)).addClass("active").siblings().removeClass("active");
				}
				e.stop().animate({
					scrollTop: i
				},
				o.speed,o.easing,
				function() {
					if (i >= box.children("li.copy").eq(0).position().top) {
						i = e.scrollTop() - box.children("li.copy").eq(0).position().top;
						e.scrollTop(i);
					}
					on = 0
				})
			} else {
				if (type == "prev") {
					if (pages - o.rolltems < 0) {
						pages = l + pages;
						e.scrollTop(items.eq(pages).position().top)
					}
					pages = pages - o.rolltems;
				} else if (type == "page") {
					pages = number * o.rolltems;
				} else {
					pages = pages + o.rolltems;
				}
				if (pages + o.showitems >= l * 2) {
					bh = box.outerHeight()
				} else {
					bh = items.eq(pages + o.showitems).position().top
				}
				if (o.content.height) {
					h = o.content.height;
				} else {
					h = bh - items.eq(pages).position().top;
				}
				i = items.eq(pages).position().top;
				if (o.pagination) {
					if (pages >= l) {
						np = l - pages;
					} else {
						np = pages;
					}
					dot.children("a").eq(number ? number: parseInt(np / o.rolltems)).addClass("active").siblings().removeClass("active");
				}
				e.stop().animate({
					scrollTop: i,
					height: h
				},
				o.speed,o.easing,
				function() {
					if (pages >= l) {
						i = e.scrollTop() - box.children("li.copy").eq(0).position().top;
						pages = pages - l;
						e.scrollTop(i);
					}
					on = 0
				})
			}
		}
		if (o.navigation) {
			if (e.siblings(".navigation").length == 0) {
				e.after("<div class=\"navigation\"><a href=\"javascript:\" class=\"prev\"></a><a href=\"javascript:\" class=\"next\"></a></div>");
			}
			var prev = e.siblings(".navigation").children(".prev"),
			next = e.siblings(".navigation").children(".next");
			
		if(o.infinite){
			
			prev.mousedown(function(){

				clearInterval(auto);
				o.direction = "up";
				o.step=3;
				auto = setInterval(autoPlay, o.autoPlay_speed/10);	

			}).mouseup(function(){

				clearInterval(auto);
				o.step=step;
				auto = setInterval(autoPlay, o.autoPlay_speed);	

			})
			
			next.mousedown(function(){

				clearInterval(auto);
				o.direction = "down";
				o.step=3;
				auto = setInterval(autoPlay, o.autoPlay_speed/10);	

			}).mouseup(function(){

				clearInterval(auto);
				o.step=step;
				auto = setInterval(autoPlay, o.autoPlay_speed);		

			})
		}else{
			prev.on("click",
			function() {
				clearInterval(auto2)
				 start("prev");
				if (o.autoPlay && o.autoPlay != 0) { 
					auto2 = setInterval(autoPlay2, ap)
				}
			})
			 next.on("click",
			function() {
				clearInterval(auto2)
				 start("next");
				if (o.autoPlay && o.autoPlay != 0) { 
					auto2 = setInterval(autoPlay2, ap)
				}
			})
		}	
			
		}
		if (o.pagination) {
			if (e.siblings(".pagination").length == 0) {
				e.after("<div class=\"paginations\"></div>");
			}
			var dot = e.siblings(".paginations")
			 if (o.content.rollheight) {
				for (i = 1; i <= Math.ceil(box.children("li.copy").eq(0).position().top / o.content.rollheight); i++) {
					dot.append("<a href='javascript:;'><span>" + i + "</span></a>")
				};
			} else {
				for (i = 1; i <= Math.ceil(l / o.rolltems); i++) {
					dot.append("<a href='javascript:;'><span>" + i + "</span></a>")
				};
			}
			dot.children("a").eq(0).addClass("active");
			dot.children("a").on("click",
			function() {
				if (!$(this).hasClass("active")) {
					clearInterval(auto2)
					 start("page", $(this).index())
					 if (o.autoPlay && o.autoPlay != 0) {
						 auto2 = setInterval(autoPlay2, ap)
					 }
				};
			})
		}
		if (o.autoPlay && o.autoPlay != 0) {
			ap = o.autoPlay == true ? 3000: o.autoPlay;
			function autoPlay2() {
				if (o.direction == "up") {
					start("prev");
				} else {
					start("next");
				}
			}
			auto2 = setInterval(autoPlay2, ap);
			
			if(o.stopOnHover){
			e.on("mouseover",
			function() {
				clearInterval(auto2)
			})
			 e.on("mouseout",
			function() {
				auto2 = setInterval(autoPlay2, ap)
			})
			}
		}
		$(window).resize(function() {
			if (on == 1) return false;
			h = items.eq(pages + o.showitems).position().top - items.eq(pages).position().top;
			if (!o.content.height) {
				e.css("height", h);
				e.scrollTop(items.eq(pages).position().top)
			}
		})
	}
})(jQuery);