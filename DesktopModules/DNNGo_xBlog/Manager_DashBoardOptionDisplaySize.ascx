﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Manager_DashBoardOptionDisplaySize.ascx.cs" Inherits="DNNGo.Modules.xBlog.Setting_DashBoardOptionCategory" %>
 <div class="form-group">
    <%=ViewControlTitle("lblDisplaySize", "Display number", "txtDisplaySize", ":", "col-sm-3 control-label")%>
   <div class="col-sm-6"> <asp:TextBox runat="server" ID="txtDisplaySize" Width="100px" CssClass="form-control form_default validate[required,custom[integer],max[1000],min[0]]"></asp:TextBox>
     </div>
</div>


