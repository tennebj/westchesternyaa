﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="View_DashBoard.ascx.cs" Inherits="DNNGo.Modules.xBlog.View_DashBoard" %>
<%@ Register TagPrefix="dnn" TagName="Label" Src="~/controls/LabelControl.ascx" %>

<asp:Panel ID="plLicense" runat="server">
<div class="validationEngineContainer form_div_<%=ModuleId %>">
<asp:PlaceHolder ID="phScript" runat="server"></asp:PlaceHolder>
 <asp:PlaceHolder  ID="phContainer" runat="server"></asp:PlaceHolder>
 </div>



 <script type="text/javascript">
     jQuery(function (q) {
         q(".form_div_<%=ModuleId %>").validationEngine({
             promptPosition: "topRight"
         });

         q(".form_div_<%=ModuleId %> input[lang='Submit']").click(function () {
             if (!$('.form_div_<%=ModuleId %>').validationEngine('validate')) {
                 return false;
             }
         });

     });
     function CancelValidation() {
         jQuery('#Form').validationEngine('detach');
     }
</script>


</asp:Panel>
<asp:Panel ID="pnlTrial" runat="server">
    <center>
        <asp:Literal ID="litTrial" runat="server"></asp:Literal>
        <br />
        <asp:Label ID="lblMessages" runat="server" CssClass="SubHead" resourcekey="lblMessages"
            Visible="false" ForeColor="Red"></asp:Label>
    </center>
</asp:Panel>