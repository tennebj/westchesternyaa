﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Manager_Option_Custom.ascx.cs" Inherits="DNNGo.Modules.xBlog.Manager_Option_Custom" %>
      <!-- start: PAGE HEADER -->
      <div class="row">
        <div class="col-sm-12"> 
          <!-- start: PAGE TITLE & BREADCRUMB -->
          
          <div class="page-header">
            <h1><i class="fa clip-search"></i> <%=ViewResourceText("Header_Title", "Custom Settings")%></h1>
          </div>
          <!-- end: PAGE TITLE & BREADCRUMB --> 
        </div>
      </div>
      <!-- end: PAGE HEADER -->
            <!-- start: PAGE CONTENT -->
      
      <div class="row">
        <div class="col-sm-12">
    
            
          <div class="panel panel-default">
            <div class="panel-heading"> <i class="fa fa-external-link-square"></i> <%=ViewResourceText("Title_BaseSettings", "Basic Settings")%>
              <div class="panel-tools"> <a href="#" class="btn btn-xs btn-link panel-collapse collapses"> </a> </div>
            </div>
            <div class="panel-body">
                <div class="form-horizontal">
                 <div class="form-group">
                    <%=ViewControlTitle("lblOpenCustom", "Enable Customization", "cbOpenCustom", ":", "col-sm-3 control-label")%>
                    <div class="col-sm-9">
                      <div class="control-inline">
                            <asp:CheckBox runat="server" ID="cbOpenCustom" />
                    </div>
                    </div>
                  </div>


                  <div class="form-group">
                    <%=ViewControlTitle("lblNoIndex", "Meta NoIndex", "cbNoIndex", ":", "col-sm-3 control-label")%>
                    <div class="col-sm-9">
                      <div class="control-inline">
                            <asp:CheckBox runat="server" ID="cbNoIndex" />
                            <%=ViewHelp("lblNoIndex", "The noindex value of an HTML robots meta tag requests that automated Internet bots avoid indexing a web page.Using a list of articles with parameters.")%>
                    </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <%=ViewControlTitle("lblRemoveMetaRobots", "Remove MetaRobots", "cbRemoveMetaRobots", ":", "col-sm-3 control-label")%>
                    <div class="col-sm-9">
                      <div class="control-inline">
                             <asp:CheckBox runat="server" ID="cbRemoveMetaRobots" />
                             <%=ViewHelp("lblRemoveMetaRobots", "")%>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
          </div>


          <div class="panel panel-default">
            <div class="panel-heading"> <i class="fa fa-external-link-square"></i> <%=ViewResourceText("Title_BaseSettings", "Custom Meta")%>
              <div class="panel-tools"> <a href="#" class="btn btn-xs btn-link panel-collapse collapses"> </a> </div>
            </div>
            <div class="panel-body">
                <div class="form-horizontal">
                 <div class="form-group">
                    <%=ViewControlTitle("lblCustomMetaArticle", "Custom Meta Article", "txtCustomMetaArticle", ":", "col-sm-3 control-label")%>
                    <div class="col-sm-9">
                       <asp:TextBox runat="server" ID="txtCustomMetaArticle" Width="95%" TextMode="MultiLine" Rows="3" CssClass="form-control validate[maxSize[1500]]"></asp:TextBox>
                    </div>
                  </div>
                   <div class="form-group">
                    <%=ViewControlTitle("lblCustomMetaArticle", "Custom Meta ArticleList", "txtCustomMetaArticleList", ":", "col-sm-3 control-label")%>
                    <div class="col-sm-9">
                       <asp:TextBox runat="server" ID="txtCustomMetaArticleList" Width="95%" TextMode="MultiLine" Rows="3" CssClass="form-control validate[maxSize[1500]]"></asp:TextBox>
                    </div>
                  </div>
                   <div class="form-group">
                    <%=ViewControlTitle("lblCustomMetaCategories", "Custom Meta Categories", "txtCustomMetaCategories", ":", "col-sm-3 control-label")%>
                    <div class="col-sm-9">
                       <asp:TextBox runat="server" ID="txtCustomMetaCategories" Width="95%" TextMode="MultiLine" Rows="3" CssClass="form-control validate[maxSize[1500]]"></asp:TextBox>
                    </div>
                  </div>
                   <div class="form-group">
                    <%=ViewControlTitle("lblCustomMetaTag", "Custom Meta Tag", "txtCustomMetaTag", ":", "col-sm-3 control-label")%>
                    <div class="col-sm-9">
                       <asp:TextBox runat="server" ID="txtCustomMetaTag" Width="95%" TextMode="MultiLine" Rows="3" CssClass="form-control validate[maxSize[1500]]"></asp:TextBox>
                    </div>
                  </div>
                   <div class="form-group">
                    <%=ViewControlTitle("lblCustomMetaAuthor", "Custom Meta Author", "txtCustomMetaAuthor", ":", "col-sm-3 control-label")%>
                    <div class="col-sm-9">
                       <asp:TextBox runat="server" ID="txtCustomMetaAuthor" Width="95%" TextMode="MultiLine" Rows="3" CssClass="form-control validate[maxSize[1500]]"></asp:TextBox>
                    </div>
                  </div>

                  
                </div>
            </div>
          </div>



        </div>
      </div>
      <!-- end: PAGE CONTENT-->
      
      <div class="row">
        <div class="col-sm-2"> </div>
        <div class="col-sm-10">
        <asp:Button CssClass="btn btn-primary" lang="Submit" ID="cmdUpdate" resourcekey="cmdUpdate"
        runat="server" Text="Update" OnClick="cmdUpdate_Click"></asp:Button>&nbsp;
        <asp:Button CssClass="btn btn-default" ID="cmdCancel" resourcekey="cmdCancel" runat="server"
            Text="Cancel" CausesValidation="False" OnClick="cmdCancel_Click"  OnClientClick="CancelValidation();"></asp:Button>&nbsp;
        
         </div>
      </div>