﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Manager_DashBoardOptionCategories.ascx.cs" Inherits="DNNGo.Modules.xBlog.Manager_DashBoardOptionCategories" %>

<!--Album start-->
 <div class="form-group" >
    <%=ViewControlTitle("lblChooseCategories", "Choose Categories", "lboxChooseCategories", ":", "col-sm-3 control-label")%>
    <div class="col-sm-6"><asp:ListBox ID="lboxChooseCategories" runat="server" CssClass="form-control" Rows="10" DataTextField="Name" DataValueField="ID" SelectionMode="Multiple"></asp:ListBox></div>
</div>
 