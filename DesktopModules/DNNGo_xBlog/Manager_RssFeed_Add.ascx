﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Manager_RssFeed_Add.ascx.cs" Inherits="DNNGo.Modules.xBlog.Manager_RssFeed_Add" %>
<%@ Register TagPrefix="dnn" Namespace="DotNetNuke.UI.WebControls" Assembly="DotNetNuke" %>
<%@ Register TagPrefix="asp" Namespace="DNNGo.Modules.xBlog.WebControls" Assembly="DNNGo.Modules.xBlog" %>
      <!-- start: PAGE HEADER -->
      <div class="row">
        <div class="col-sm-12"> 
          <!-- start: PAGE TITLE & BREADCRUMB -->
          
          <div class="page-header">
            <h1><i class="fa fa-rss"></i> <%=ViewResourceText("Header_Title", "Add New rssfeed")%></h1>
          </div>
          <!-- end: PAGE TITLE & BREADCRUMB --> 
        </div>
      </div>
      <!-- end: PAGE HEADER -->
            <!-- start: PAGE CONTENT -->
      
      <div class="row">
        <div class="col-sm-12">
    
            
          <div class="panel panel-default">
            <div class="panel-heading"> <i class="fa fa-external-link-square"></i> <%=ViewResourceText("Title_BaseSettings", "Basic Settings")%>
              <div class="panel-tools"> <a href="#" class="btn btn-xs btn-link panel-collapse collapses"> </a> </div>
            </div>
            <div class="panel-body">
                <div class="form-horizontal">
                  <div class="form-group">
                    <%=ViewControlTitle("lblTitle", "RSS Feed Title", "txtTitle", ":", "col-sm-3 control-label")%>
                    <div class="col-sm-5">
                      <asp:TextBox runat="server" ID="txtTitle" CssClass="form-control validate[required]"></asp:TextBox>
                    </div>
                  </div>
                  <div class="form-group">
                    <%=ViewControlTitle("lblUrl", "RSS Feed Url", "txtUrl", ":", "col-sm-3 control-label")%>
                    <div class="col-sm-5">
                      <asp:TextBox runat="server" ID="txtUrl" CssClass="form-control validate[required,custom[url]]"></asp:TextBox>
                    </div>
                  </div>
                  <div class="form-group">
                    <%=ViewControlTitle("lblActivation", "Activation", "cbActivation", ":", "col-sm-3 control-label")%>
                    <div class="col-sm-5">
                       <div class="checkbox-table"><asp:CheckBox ID="cbActivation" runat="server" CssClass="flat-grey"/></div>
                    </div>
                  </div>
                  <div class="form-group">
                    <%=ViewControlTitle("lblCategories", "Categories", "txtCategories", ":", "col-sm-3 control-label")%>
                    <div class="col-sm-5">
                        <asp:CustDropDownList ID="ddlCategories"  runat="server" CssClass="form-control">  </asp:CustDropDownList>
                    </div>
                  </div>
                 
                </div>
            </div>
          </div>

 
        </div>
      </div>
      <!-- end: PAGE CONTENT-->
      
      <div class="row">
        <div class="col-sm-2"> </div>
        <div class="col-sm-10">
        <asp:Button CssClass="btn btn-primary" lang="Submit" ID="cmdUpdate" resourcekey="cmdUpdate"
        runat="server" Text="Update" OnClick="cmdUpdate_Click"></asp:Button>&nbsp;
        <asp:Button CssClass="btn btn-default" ID="cmdCancel" resourcekey="cmdCancel" runat="server"
            Text="Cancel" CausesValidation="False" OnClick="cmdCancel_Click"  OnClientClick="CancelValidation();"></asp:Button>&nbsp;
        
         </div>
      </div>