﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Manager_Option_Push.ascx.cs" Inherits="DNNGo.Modules.xBlog.Manager_Option_Push" %>
<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">
        <!-- start: PAGE TITLE & BREADCRUMB -->
        <div class="page-header">
            <h1><i class="fa clip-loop"></i> <%=ViewResourceText("Header_Title", "Push Interface Settings")%></h1>
        </div>
        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<!-- end: PAGE HEADER -->
<!-- start: PAGE CONTENT -->
<div class="row">
    <div class="col-sm-12">


        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i> <%=ViewResourceText("Title_BaseSettings", "Basic Settings")%>
                <div class="panel-tools">
                    <a href="#" class="btn btn-xs btn-link panel-collapse collapses"></a>
                </div>
            </div>
            <div class="panel-body">
                <div class="form-horizontal">
                    <div class="form-group">
                        <%=ViewControlTitle("lblPushEnable", "Push Enable", "cbjQueryEnable", ":", "col-sm-3 control-label")%>
                        <div class="col-sm-5">
                           <div class="control-inline"> <asp:CheckBox ID="cbPushEnable" runat="server" /></div>
                        </div>
                    </div>

                     

                      

                    <div class="form-group">
                        <%=ViewControlTitle("lblArticleReviewAuthor", "Author Article Review", "cbArticleReviewAuthor", ":", "col-sm-3 control-label")%>
                        <div class="col-sm-5">
                           <div class="control-inline"> <asp:CheckBox ID="cbArticleReviewAuthor" runat="server" /></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <%=ViewControlTitle("lblArticleReviewAdmin", "Admin Article Review", "cbArticleReviewAdmin", ":", "col-sm-3 control-label")%>
                        <div class="col-sm-5">
                           <div class="control-inline"> <asp:CheckBox ID="cbArticleReviewAdmin" runat="server" /></div>
                        </div>
                    </div>
                       <div class="form-group">
                        <%=ViewControlTitle("lblAlreadyExists", "Already exists", "ddlAlreadyExists", ":", "col-sm-3 control-label")%>
                        <div class="col-sm-5">
                            <asp:DropDownList ID="ddlAlreadyExists" Width="150" runat="server" CssClass="form-control"></asp:DropDownList>
                        </div>
                    </div>
                    <div class="form-group">
                        <%=ViewControlTitle("lblAppVerify", "App Verify", "txtAppVerify", ":", "col-sm-3 control-label")%>
                        <div class="col-sm-5">
                           <asp:TextBox runat="server" ID="txtAppVerify" Width="300" CssClass="form-control validate[required]"></asp:TextBox>
                            <%=ViewHelp("lblAppVerify", "Url Query String: AppVerify=XXXXXXXXXXXXX ")%>
                        </div>
                    </div>

                     <div class="form-group">
                        <%=ViewControlTitle("lblQueryString", "Query String", "txtQueryString", ":", "col-sm-3 control-label")%>
                        <div class="col-sm-5">
                            <asp:TextBox runat="server" ID="txtQueryString" Width="300" TextMode="MultiLine" Rows="5" CssClass="form-control input_text validate[maxSize[1024]]"></asp:TextBox>
                            <%=ViewHelp("lblQueryString", "Default Query String: <br />Name=value <br />Name2=value2")%>
                        </div>
                    </div>

                </div>
            </div>
        </div>


        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i> <%=ViewResourceText("Title_PushInformation", "Push Information")%>
                <div class="panel-tools">
                    <a href="#" class="btn btn-xs btn-link panel-collapse collapses"></a>
                </div>
            </div>
            <div class="panel-body">
                <div class="form-horizontal">
                    <div class="form-group">
                        <%=ViewControlTitle("lblPushUrl", "Push Url", "hlPushUrl", ":", "col-sm-3 control-label")%>
                        <div class="col-sm-5">
                           <asp:HyperLink ID="hlPushUrl" runat="server" Target="_blank"></asp:HyperLink>
                        </div>
                    </div>

                     


                </div>
            </div>
        </div>

    </div>
</div>
<!-- end: PAGE CONTENT-->
<div class="row">
    <div class="col-sm-2">
    </div>
    <div class="col-sm-10">
        <asp:Button CssClass="btn btn-primary" lang="Submit" ID="cmdUpdate" resourcekey="cmdUpdate"
            runat="server" Text="Update" OnClick="cmdUpdate_Click"></asp:Button>&nbsp;
        <asp:Button CssClass="btn btn-default" ID="cmdCancel" resourcekey="cmdCancel" runat="server"
            Text="Cancel" CausesValidation="False" OnClick="cmdCancel_Click" OnClientClick="CancelValidation();">
        </asp:Button>&nbsp;
    </div>
</div>
