﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Manager_Option_SEO.ascx.cs" Inherits="DNNGo.Modules.xBlog.Manager_Option_SEO" %>
      <!-- start: PAGE HEADER -->
      <div class="row">
        <div class="col-sm-12"> 
          <!-- start: PAGE TITLE & BREADCRUMB -->
          
          <div class="page-header">
            <h1><i class="fa clip-search"></i> <%=ViewResourceText("Header_Title", "SEO Settings")%></h1>
          </div>
          <!-- end: PAGE TITLE & BREADCRUMB --> 
        </div>
      </div>
      <!-- end: PAGE HEADER -->
            <!-- start: PAGE CONTENT -->
      
      <div class="row">
        <div class="col-sm-12">
    
            
          <div class="panel panel-default">
            <div class="panel-heading"> <i class="fa fa-external-link-square"></i> <%=ViewResourceText("Title_BaseSettings", "Basic Settings")%>
              <div class="panel-tools"> <a href="#" class="btn btn-xs btn-link panel-collapse collapses"> </a> </div>
            </div>
            <div class="panel-body">
                <div class="form-horizontal">
                  <div class="form-group">
                    <%=ViewControlTitle("lblSeoTitle", "Page Title", "rblSeoTitle", ":", "col-sm-3 control-label")%>
                    <div class="col-sm-9">
                      <div class="control-inline"><asp:RadioButtonList ID="rblSeoTitle" runat="server" RepeatDirection="Horizontal" CssClass=""></asp:RadioButtonList></div>
                    </div>
                  </div>
                  <div class="form-group">
                    <%=ViewControlTitle("lblFriendlyUrl", "Friendly Url", "rblFriendlyUrl", ":", "col-sm-3 control-label")%>
                    <div class="col-sm-9">
                      <div class="control-inline"><asp:RadioButtonList ID="rblFriendlyUrl" runat="server" RepeatDirection="Horizontal" CssClass=""></asp:RadioButtonList></div>
                    </div>
                  </div>
                  <div class="form-group">
                    <%=ViewControlTitle("lblUrlParameterName", "Url Parameter Name", "txtUrlParameterName", ":", "col-sm-3 control-label")%>
                    <div class="col-sm-9">
                        <asp:TextBox runat="server" ID="txtUrlParameterName" Width="150px" CssClass="form-control form_default validate[required,minSize[1],maxSize[10],custom[onlyLetterNumber]]"></asp:TextBox>
                    </div>
                  </div>
                 
                </div>
            </div>
          </div>


          



        </div>
      </div>
      <!-- end: PAGE CONTENT-->
      
      <div class="row">
        <div class="col-sm-2"> </div>
        <div class="col-sm-10">
        <asp:Button CssClass="btn btn-primary" lang="Submit" ID="cmdUpdate" resourcekey="cmdUpdate"
        runat="server" Text="Update" OnClick="cmdUpdate_Click"></asp:Button>&nbsp;
        <asp:Button CssClass="btn btn-default" ID="cmdCancel" resourcekey="cmdCancel" runat="server"
            Text="Cancel" CausesValidation="False" OnClick="cmdCancel_Click"  OnClientClick="CancelValidation();"></asp:Button>&nbsp;
        
         </div>
      </div>