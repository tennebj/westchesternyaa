﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Manager_Event_AdditionalDate.ascx.cs" Inherits="DNNGo.Modules.xEvent.Manager_Event_AdditionalDate" %>


<table class="table table-striped table-bordered table-hover" cellspacing="0" cellpadding="0" border="0" style="width:100%;border-collapse:collapse;">
  <tbody class="AdditionalDate_Table_Tbody">
    <tr>
      <th width="50px">Action</th>
      <th>Date</th>
     
    </tr>
    <asp:Literal ID="liList" runat="server"></asp:Literal>
  </tbody>
</table>

<div class="row">
      <div class="col-sm-12">
                <div class="form-group">
                     <div id="message_success" class="alert alert-success" style=" display:none;"><button data-dismiss="alert" class="close">×</button><div class="message_content"></div><div></div></div>
                    
                </div>
           <div class="form-group">

               <div id="message_warning" class="alert alert-warning" style=" display:none;"><button data-dismiss="alert" class="close">×</button><div class="message_content"></div><div></div></div>
           </div>

      </div>
</div>


<table  cellspacing="0" cellpadding="0" border="0" style="width:100%;border-collapse:collapse;" align="left">
  <tr>
    <td width="30%"><div class="panel-collapse" id="Start">
          <div class="input-group">
            <input name="Event$txtStartDate" type="text" id="Event_txtStartDate" class="form-control date-picker-c validate[optional]" data-date-format="mm/dd/yyyy" data-date-viewmode="years" />
            <span class="input-group-addon"><i class="fa fa-calendar"></i></span> </div>
        </div>
     </td>
      <td width="1%"> </td>
      <td>
          <a id="butAddDate" class="btn btn-primary btn-sm" ><i class="fa fa-plus"></i> Add Date</a>
     </td>
  </tr>
    <tr>
        <td colspan="3">
           
        </td>
    </tr>
</table>




<script id="scriptTableTR" type="text/x-jquery-tmpl">
    <tr data-ticks="${Ticks}">
      <td align="center"><a class="btn btn-xs btn-bricky tooltips delete" data-ticks="${Ticks}"><i class="fa fa-times fa fa-white"></i></a></td>
      <td>${DateTime}</td>
    </tr>
</script>

<script type="text/javascript">




    jQuery(function ($) {

        $('.date-picker-c').datepicker({
            autoclose: true
        }).on('changeDate', function (ev) {
            var date = new Date(ev.date);
            $('.date-picker-c').data("ticks", date.format("yyyy-MM-dd"));
        });


        var ShowMessages = function (alert,content)
        {
            $("#message_" + alert).find(".message_content").html(content);
            $("#message_" + alert).stop().fadeIn("fast", function () {
                $(this).fadeOut(10000);
            });
        }

        var CreateDelete = function () {
            $("a.delete").bind('click', function () {
                var ticks = $(this).data("ticks");
                $.getJSON("<%=ModulePath %>Resource_Service.aspx?Token=AjaxAdditionalDate&Actions=DELETE&EventID=<%=EventID%>&Ticks=" + ticks, Module, function (item) {
                    if (item.result == "success") {
                        $(".AdditionalDate_Table_Tbody tr[data-ticks='" + ticks + "']").empty();
                        //ShowMessages("success", "删除日期成功!");
                    } else {
                        //ShowMessages("warning", "删除日期失败:" + item.fail);
                    }
                });

            });
        };

        CreateDelete();


    $("#butAddDate").click(function () {
            var ticks = $('.date-picker-c').data("ticks");
            if (ticks !== undefined && ticks != "") {
           
            if(!$("#td[data-ticks='"+ticks +"']").is("td"))
            {
                $.getJSON("<%=ModulePath %>Resource_Service.aspx?Token=AjaxAdditionalDate&Actions=ADD&EventID=<%=EventID%>&Ticks=" + ticks, Module, function (item) {
                    if(item.result =="success")
                    {
                        $("#scriptTableTR").tmpl(item).appendTo(".AdditionalDate_Table_Tbody");
                        CreateDelete();
                        //ShowMessages("success", "添加日期成功!");
                    }else
                    {
                        //ShowMessages("warning", "添加日期失败:" + item.fail);
                    }
                    $('.date-picker-c').val("");
                  
                });
            }else
            {
                //ShowMessages("warning", "已经存在了，不要添加了");
             
            }
        }
    });



       
});
</script>