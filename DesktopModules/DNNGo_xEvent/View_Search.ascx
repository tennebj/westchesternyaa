﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="View_Search.ascx.cs" Inherits="DNNGo.Modules.xEvent.View_Search" %>


<asp:Panel ID="plLicense" runat="server">
<div class="validationEngineContainer form_div_<%=ModuleId %>">
<asp:Literal ID="liContent" runat="server"></asp:Literal>
<asp:Button runat="server" ID="SearchButton" OnClick="SearchButton_Click" Visible="false" OnClientClick="CancelValidation();"  />
</div>



 <script type="text/javascript">
     jQuery(function (q) {
         q(".form_div_<%=ModuleId %>").validationEngine({
             promptPosition: "topRight"
         });

         q(".form_div_<%=ModuleId %> input[lang='Submit']").click(function () {
             if (!$('.form_div_<%=ModuleId %>').validationEngine('validate')) {
                 return false;
             }
         });

     });
     function CancelValidation() {
         jQuery('#Form').validationEngine('detach');
     }
</script>


</asp:Panel>
<asp:Panel ID="pnlTrial" runat="server">
    <center>
        <asp:Literal ID="litTrial" runat="server"></asp:Literal>
        <br />
        <asp:Label ID="lblMessages" runat="server" CssClass="SubHead" resourcekey="lblMessages"
            Visible="false" ForeColor="Red"></asp:Label>
    </center>
</asp:Panel>


