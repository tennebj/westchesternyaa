function ImgLoad(callback,e){
	var imgdefereds=[];
	e.find('img').each(function(){
		var dfd=jQuery.Deferred();
			$(this).bind('load',function(){
				dfd.resolve();
			}).bind('error',function(){
			})
			if(this.complete) setTimeout(function(){
				dfd.resolve();
			},1000);
			imgdefereds.push(dfd);
	})
	jQuery.when.apply(null,imgdefereds).done(function(){
		callback();
	});
}

var FC = $.fullCalendar;
var View = FC.View;
CustomMonth = View.extend({
	setHeight: function(height, isAuto) {},
	renderEvents: function(events) {
		$(".fcp-cminfo").remove();
		this.el.children(".fcp-CustomMonth").remove();
		this.el.append("<div class=\"fcp-CustomMonth\"></div>")
		main = this.el.parents(".DashBoard_calendar"),
		viewBox = main.children(".calendar-view");
		main.append("<div class=\"fcp-cminfo\"><div class=\"fc-close-tooltip\"></div><div class=\"fcp-main\"></div></div>");
		tool = main.children(".fcp-cminfo"),
		toolClose = tool.children(".fc-close-tooltip"),
		toolMain = tool.children(".fcp-main"),
		CustomMonthTemplate = main.children(".fcp-Template-CustomMonth");
		tool.prependTo("body");

		var acbox = this.el.children(".fcp-CustomMonth");
		var currentDayStart = this.start.clone();
		
		function is_leap(year) {
			return (year % 100 == 0 ? res = (year % 400 == 0 ? 1: 0) : res = (year % 4 == 0 ? 1: 0));
		}
		var nstr = currentDayStart;
		var ynow =  parseInt(nstr.format("YYYY"));
		var mnow = parseInt(nstr.format("M"))-1;
		var dnow =  parseInt(nstr.format("D"));
		var n1str = new Date(ynow, mnow, 1);
		var firstday = n1str.getDay();
		var m_days = new Array(31, 28 + is_leap(ynow), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
		var tr_str = Math.ceil((m_days[mnow] + firstday) / 7);
		var upt= mnow==0? 11 : mnow-1;
		
		 
		 
		var table = $("<table>")
		 table.append("<tr><th class=\"fcp-day-header fcp-widget-header fcp-Sun\">Sun</th><th class=\"fcp-day-header fcp-widget-header fcp-Mon\">Mon</th><th class=\"fcp-day-header fcp-widget-header fcp-Tue\">Tue</th><th class=\"fcp-day-header fcp-widget-header fcp-Wed\">Wed</th><th class=\"fcp-day-header fcp-widget-header fcp-Thu\">Thu</th><th class=\"fcp-day-header fcp-widget-header fcp-Fri\">Fri</th><th class=\"fcp-day-header fcp-widget-header fcp-Sat\">Sat</th></tr>");
		for (i = 0; i < tr_str; i++) {
			var tr = $("<tr></tr>");
			for (k = 0; k < 7; k++) {
				idx = i * 7 + k;
				date_str = idx - firstday + 1; 
				var mnow_c=mnow+1,
					ynow_c=ynow;
				if(date_str <= 0){
					date_str = m_days[upt]  + date_str;
					other =" fc-other-month";
					mnow_c--;
					if(mnow_c==0){
						mnow_c=12
						ynow_c--;
					}
				}else if(date_str > m_days[mnow]){
					date_str = date_str-m_days[mnow] 
					other =" fc-other-month";
					mnow_c++;
					if(mnow_c==13){
						mnow_c=1
						ynow_c++;
					}
				}else{
					date_str = idx - firstday + 1;
					other ="";
				}
				tr.append("<td class=\"fcp-day fcp-widget-content"+other+"\" date=\"" + ynow_c + "-" + parseInt(mnow_c) + "-" + parseInt(date_str) + "\">" + date_str + "</td>")
			}
			table.append(tr);
		}
		acbox.html(table)
		for (var key = 0; key < events.length; key++) {
			var eventStart = moment(events[key].start).format("YYYY-M-D");
			var tr = table.find("td[date='" + eventStart + "']");
			if (tr.length != 0) {
				tr.addClass("ev-day");
				if (tr.data("events")) {
					tr.data("events", tr.data("events") + "," + key)
					tr.data("number", parseInt(tr.data("number"))+1)
				} else {
					tr.data("events", key)
					tr.data("number", 1)
				}
			}
		}
		main.find(".ev-day").on("click",
		function() {
			var eventlist = String($(this).data("events")).split(",");
			toolMain.mCustomScrollbar("destroy");	
			toolMain.html(" ");
			for (var l = 0; l < eventlist.length; l++) {
				if (l >= 1) {
					toolMain.append("<div class=\"fcp-divider\"></div>")
				}
				toolMain.append(CustomMonthTemplate.tmpl(events[eventlist[l]]));
			}
			toolMain.css({
				"max-width": $(window).width()-20,
				"max-height": $(window).height() / 1.5
			});
			var w = tool.outerWidth(),
			h = tool.outerHeight(),
			ww = $(window).width(),
			wt = $(window).scrollTop(),
			X = $(this).offset().left,
			Y = $(this).offset().top;
			if (X > ww / 2) {
				var left = X  - w;
				if (left-10 < 0) {
					left = 0
				}
			} else {
				var left = X ;
				if (left + w+ 20 > ww) {
					left = ww - w -20
				}
			}
			var top = Y - h / 2 < wt ? wt: Y - h / 2;
			tool.hide().stop().fadeIn().css({
				"top": top,
				"left": left
			});
			toolMain.mCustomScrollbar({
				autoHideScrollbar: true,
				theme: "light-thin"
			});
			
			ImgLoad(function() {
				toolMain.mCustomScrollbar("update");
			},toolMain)

			
			toolClose.on("click",
			function() {
				tool.hide();
			})
			 main.find(".fc-toolbar button").on("click",
			function() {
				tool.hide();
			})
		})
		main.find(".ev-day").each(function(){
			$(this).append("<span class=\"number\">"+$(this).data("number")+"</span>")
			
		})
		
		
		
		var today=new Date().format("yyyy-M-d");
		table.find("td[date='" + today + "']").addClass("today")
		
		 main.removeClass("fc-loading");
	},
	destroyEvents: function() {},
	renderSelection: function(range) {},
	destroySelection: function() {
	//	this.el.children(".fcp-CustomMonth").remove();
	//	$(".fcp-cminfo").remove();
	}
});
FC.views.CustomMonth = CustomMonth;


var selectDate=function(main,view){
			var y =view.start.format("YYYY"),
				m =view.start.format("MM"),
				md=["January","February","March","April","May","June","July","August","September","October","November","December"];
				if(main.find(".fc-toolbar .fc-center .quickdate").length==0){
					var quickdate =$("<div class=\"quickdate\">"),
						mlist=$("<select class=\"month\"></select>"),
						ylist=$("<select class=\"year\"></select>"),
						monthcopy=$("<div class=\"monthcopy\"></div>");
			
						for(var i=1;i<=12;i++){
							if(i==parseInt(m)){
								mlist.append("<option value=\""+i+"\" selected=\"selected\">"+md[i-1]+"</option>")
								monthcopy.html(md[i-1]);
							}else{
								mlist.append("<option value=\""+i+"\" >"+md[i-1]+"</option>")
							}
						}
						for(var i=0;i<15;i++){
							if(i==9){
								ylist.append("<option value=\""+parseInt(y-9+i)+"\" selected=\"selected\">"+parseInt(y-9+i)+"</option>")
							}else{
								ylist.append("<option value=\""+parseInt(y-9+i)+"\">"+parseInt(y-9+i)+"</option>")
							}
						}
						quickdate.append(mlist)
						quickdate.append(ylist)
						quickdate.append(monthcopy)
					main.find(".fc-toolbar .fc-center").append(quickdate);
					
					var quickclick=main.find(".fc-toolbar .fc-center h2");
					quickdate.find("select").eq(0).width(monthcopy.outerWidth())
					quickdate.find("select").on("change",function(){
						quickdate.find("select").eq(0).val();
						quickdate.find("select").eq(1).val();
						main.fullCalendar( 'gotoDate', quickdate.find("select").eq(1).val()+"-"+("0"+quickdate.find("select").eq(0).val()).slice(-2)+"-01" )
					})
				}else{
					var quickdate=main.find(".quickdate"),monthcopy=quickdate.find(".monthcopy");
						quickdate.find("select").eq(0).val(parseInt(m));
						quickdate.find("select").eq(1).val(y);
					    monthcopy.html(md[parseInt(m)-1]);
					quickdate.append(monthcopy)
					quickdate.find("select").eq(0).width(monthcopy.outerWidth())
				}
}
