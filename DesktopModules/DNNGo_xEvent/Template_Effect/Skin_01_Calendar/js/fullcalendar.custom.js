function ImgLoad(callback,e){
	var imgdefereds=[];
	e.find('img').each(function(){
		var dfd=jQuery.Deferred();
			$(this).bind('load',function(){
				dfd.resolve();
			}).bind('error',function(){
			})
			if(this.complete) setTimeout(function(){
				dfd.resolve();
			},1000);
			imgdefereds.push(dfd);
	})
	jQuery.when.apply(null,imgdefereds).done(function(){
		callback();
	});
}


var FC = $.fullCalendar;
var View = FC.View; 
(function($) {
	$.fn.mouseevents = function(options) {
		var defaults = {
			mousedown: function() {},
			mouseup: function() {},
			mousemove: function() {}
		}
		var o = $.extend(defaults, options);
		var e = $(this);
		e[0].addEventListener('touchstart', touch, false);
		e[0].addEventListener('touchmove', touch, false);
		e[0].addEventListener('touchend', touch, false);
		e[0].addEventListener('mousedown', touch, false);
		e[0].addEventListener('mouseup', touch, false);
		e[0].addEventListener('mousemove', touch, false);
		e[0].addEventListener('mouseleave', touch, false);
		var isdown = false,
		startX = 0,
		startY = 0,
		endX = 0,
		endY = 0;
		function touch(event) {
			var event = event || window.event;
			switch (event.type) {
			case "touchstart":
			case "mousedown":
				mouseevent = event.type == "mousedown" ? event: event.touches[0];
				isdown = true;
				o.mousedown();
				break;
			case "touchend":
			case "mouseup":
			case "mouseleave":
				if (isdown) {
					mouseevent = event.type != "touchend" ? event: event.changedTouches[0];
					isdown = false;
					o.mouseup();
				}
				break;
			case "touchmove":
			case "mousemove":
				if (isdown) {
					event.preventDefault();
					mouseevent = event.type == "mousemove" ? event: event.touches[0];
					o.mousemove();
				}
				break;
			}
		}
	}
})(jQuery);
timeLine = View.extend({
	initialize: function() {},
	render: function() {
		this.el.children(".fcp_timeline").remove();
		this.el.children(".fcp-timelinemore").remove();
		this.el.append("<div class=\"fcp_timeline\"></div>");
		main = this.el.parents(".calendar"),
		viewBox = main.children(".calendar-view"),
		timelineTemplate = main.children(".fcp-Template-timeline"),
		ExtensionsTemplate = main.children(".fcp-Template-Extensions");
	},
	setHeight: function(height, isAuto) {},
	renderEvents: function(events) {
		var currentDayStart = this.start.clone();
		var periodEnd = this.end.clone();
		var dateSame = currentDayStart.format("DD") - 1;
		var tlbox = this.el.children(".fcp_timeline");
		var today = new Date(),
		ty = "",
		id = this.el.parents(".calendar").data("id");
		timelineDefault = eval("timelineDefault" + id);
		var appendbox=$("<div>")
		
		for (var i=0;i<events.length;i++) {
			var e = events[i];
			//if (!e.ExtensionsTemplate) {
			//	ExtensionsBox(e)
			//}
			var eventStart = e.start.clone();
			var eventEnd = this.calendar.getEventEnd(e);
			if ((currentDayStart <= eventStart || currentDayStart == eventStart) && periodEnd > eventStart) {
				if (eventStart.format("DD") != dateSame) {
					if (today.format("dd") == eventStart.format("DD")) {
						ty = "today"
					} else {
						ty = " "
					}
					appendbox.append("<h3 class=\"fcp-line-header " + ty + " \"><span>" + moment(eventStart).locale(jsViewLanguage.Language).format('MMM DD, YYYY') + "</span></h3>")
				}
				dateSame = eventStart.format("DD");
				appendbox.append(timelineTemplate.tmpl(e));
				length++;
			}
		}
		
		
		var appendbox_ch= appendbox.children();
		appendbox = appendbox.children(".fcp-timeBox");
		appendbox.each(function(index){
				$(this).data("index",$(this).index());
		});
		
		if(appendbox.length > timelineDefault.Display ){
			var more_s=0,more_e=timelineDefault.Display,ScrollMaxPage=0,stop=true;
				tlbox.append(appendbox_ch.slice(more_s,appendbox.eq(more_e-1).data("index")+1));
			var acmore=$("<div class=\"fcp-timelinemore\"><span><i class=\"fa fa-refresh\"></i> "+jsViewLanguage.More+"</span></div>");
			
			var LoadAjaxItems = function(){
				more_s = more_e;
				more_e = more_e + timelineDefault.LoadDisplay > appendbox.length?appendbox.length:more_e + timelineDefault.LoadDisplay;
				
				var atm=$("<div>").hide();
				tlbox.append(atm.append(appendbox_ch.slice(appendbox.eq(more_s-1).data("index")+1,appendbox.eq(more_e-1).data("index")+1)));
				atm.slideDown(300,function(){stop=true});
				if(more_e >= appendbox.length){
					stop=false;
					acmore.fadeOut(function(){acmore.remove()});
				}
				ScrollMaxPage++;
				acmore.find(".fa").fadeOut();
			}
			
			
			acmore.on("click",function(){
				if(stop){
					stop=false;
					acmore.find(".fa").css("display","inline-block");
					setTimeout(LoadAjaxItems,500);
				}		
			})
			if(timelineDefault.loadeddata=="On Scroll"){
				$(window).scroll(function() {
					if ($(window).scrollTop() + $(window).height() > acmore.offset().top ) {
						if(ScrollMaxPage < timelineDefault.ScrollMaxPage && stop){ 
							stop=false;
							acmore.find(".fa").css("display","inline-block");
							setTimeout(LoadAjaxItems,500);
						}
					}
				});
			}
			
			
			this.el.append(acmore);
		}else{
			tlbox.append(appendbox_ch)
		}
		
		if (appendbox.length == 0) {
			tlbox.append('<div class="not">'+jsViewLanguage.NoEvents+'</div>')
		};
	},
	destroyEvents: function() {
		this.el.children(".fcp_timeline").remove();
		this.el.children(".fcp-timelinemore").remove();
	}
});
FC.views.timeLine = timeLine;
GMap = View.extend({
	initialize: function() {},
	render: function() {
		this.el.children(".fcp_Gmap").remove();
		this.el.append("<div class=\"fcp_Gmap\"><div class=\"fcp-gmapinfo\"><div class=\"fc-close-tooltip\"></div><span class=\"fcp-gmapclose\"></span><span class=\"fcp-gmapup\"></span><span class=\"fcp-gmapdown\"></span><div class=\"fcp-main\"></div></div><div class=\"GmapBox\"></div><div class=\"gmap_full\">"+jsViewLanguage.Fullscreen+"</div><div class=\"gmap_full gmap_full2\">"+jsViewLanguage.Fullscreen+"</div> </div>")
		 main = this.el.parents(".calendar"),
		viewBox = main.children(".calendar-view"),
		GmapInfoTemplate = main.children(".fcp-Template-GmapInfo"),
		GmapTitleInfoTemplate = main.children(".fcp-Template-GmapTitleInfo"),
		ExtensionsTemplate = main.children(".fcp-Template-Extensions");
	},
	setHeight: function(height, isAuto) {},
	renderEvents: function(events) {
		id = this.el.parents(".calendar").data("id");
		GmapDefault = eval("GmapDefault" + id)
	var currentDayStart = this.start.clone(),
		periodEnd = this.end.clone(),
		fcpGmap=this.el.children('.fcp_Gmap'),
		gmapBox = fcpGmap.children('.GmapBox'),
		gmapinfo = fcpGmap.children(".fcp-gmapinfo"),
		gmapinfoClose = gmapinfo.children(".fc-close-tooltip"),
		gmapinfoMain = gmapinfo.children(".fcp-main"),
		gmapinfoup = gmapinfo.children(".fcp-gmapup"),
		gmapinfodown = gmapinfo.children(".fcp-gmapdown"),
		gmapinfoclose = gmapinfo.children(".fcp-gmapclose"),
		full = fcpGmap.children(".gmap_full"),
		address = [],
		soindex = 0,
		map;
		
		for (var i=0;i<events.length;i++) {
				var e = events[i];
				var eventStart = e.start.clone();
				var eventEnd = this.calendar.getEventEnd(e);
				if ((currentDayStart <= eventStart || currentDayStart == eventStart) && periodEnd > eventStart && events[i].LocationXY && events[i].LocationXY!=",") {
					var par =true;
					if(address.length!=0){
						for(var x=0;x<address.length;x++){
							if( events[i].LocationXY == address[x].LocationXY ){
								address[x].options.eventsDate.push(i);
								par=false;
							}					
						}
					}
					
					if(par || address.length==0){
						
						events[i].Address.Location=events[i].Location
						
						var datas={
								latLng:[events[i].Address.Latitude,events[i].Address.Longitude],
								LocationXY:events[i].Address.Latitude+","+events[i].Address.Longitude,
								data:"<h5>"+events[i].Address.Title+"</h5>"+events[i].Address.MarkerText,
								options: {
									index:address.length,
									eventsDate:[i],
									icon:events[i].Address.MarkerIcon,
									Address:events[i].Address,
									Location:events[i].Location
								}
							}
							address.push(datas)
					}
					
			}
		}
		
		var Ev_NewMap = function(id){
			if (address.length != 0) {
				gmapBox.gmap3({
					marker: {
						values: address,
						options: {
							draggable: false
						},
						events: {
							mouseover: function(marker, event, context) {
								var map = $(this).gmap3("get"),
								infowindow = $(this).gmap3({
									get: {
										name: "infowindow"
									}
								});
								if (infowindow) {
									infowindow.open(map, marker);
									infowindow.setContent(context.data);
								} else {
									$(this).gmap3({
										infowindow: {
											anchor: marker,
											options: {
												content: context.data
											}
										}
									});
								}
							},
							mouseout: function() {
								var infowindow = $(this).gmap3({
									get: {
										name: "infowindow"
									}
								});
								if (infowindow) {
									infowindow.close();
								}
							},
							click: function(marker,context) {
								gmapinfoMain.mCustomScrollbar("destroy");	
								gmapinfoMain.html(GmapTitleInfoTemplate.tmpl(marker.Address));
							 

								for(i=0;i<marker.eventsDate.length;i++){
									if(i>0){
									gmapinfoMain.append("<div class=\"line\"></div>");
									}
									gmapinfoMain.append(GmapInfoTemplate.tmpl(events[marker.eventsDate[i]]));
								}
								gmapinfo.fadeIn();
								gmapinfoMain.css({
									"max-height": gmapBox.height() * 0.8
								});
								
								gmapinfoMain.mCustomScrollbar({
									autoHideScrollbar: true,
									theme: "light-thin"
								});
								
								ImgLoad(function() {
									gmapinfoMain.mCustomScrollbar("update");
								},gmapinfoMain)
								
								soindex=marker.index;
								map.setZoom(marker.Address.Zoom);
								map.setCenter(markers[soindex].getPosition());
								$.each(markers,
								function(i, marker) {
									marker.setAnimation(null);
								});
								marker.setAnimation(google.maps.Animation.BOUNCE);
							}
						}
					},
					map: {
						options: {
							zoom: GmapDefault.gmapZoom
						},
						callback: function() {
							markers = this.gmap3({
								get: {
									name: "marker",
									all: true
								}
							});
							map = gmapBox.gmap3("get");
						}
					}
				})
				}
			if (address.length > 1) {
				 gmapinfo.mouseevents({
					mousedown: function() {
						startX = mouseevent.clientX;
						startY = mouseevent.clientY;
						objX = gmapinfo.position().left;
						objY = gmapinfo.position().top;
						gmapinfo.css("cursor", "move")
					},
					mouseup: function() {
						gmapinfo.css("cursor", "default")
					},
					mousemove: function() {
						x = mouseevent.clientX - startX;
						y = mouseevent.clientY - startY
						 gmapinfo.css({
							left: objX + x,
							top: objY + y
						})
					}
				})
				 var gmapMagnify = function(index) {
						gmapinfoMain.mCustomScrollbar("destroy");			 
						gmapinfoMain.html(GmapTitleInfoTemplate.tmpl(address[index].options.Address));
						 
						for(i=0;i<address[index].options.eventsDate.length;i++){
							if(i>0){
								gmapinfoMain.append("<div class=\"line\"></div>");
							}
							gmapinfoMain.append(GmapInfoTemplate.tmpl(events[address[index].options.eventsDate[i]]));
						}
						
						map.setCenter(markers[soindex].getPosition());
						map.setZoom(address[index].options.Address.Zoom);
						gmapinfoMain.css({
							"max-height": gmapBox.height() * 0.8
						});
						
						gmapinfoMain.mCustomScrollbar({
							autoHideScrollbar: true,
							theme: "light-thin"
						});
					
						ImgLoad(function() {
							gmapinfoMain.mCustomScrollbar("update");
						},gmapinfoMain)
						$.each(markers,
						function(i, marker) {
							marker.setAnimation(null);
						});
						markers[soindex].setAnimation(google.maps.Animation.BOUNCE);
				}
	
				gmapinfoup.show();
				gmapinfodown.show();
				gmapinfoup.on('click',
				function() {
					soindex = soindex + 1 >= address.length ? 0: soindex + 1;
					gmapMagnify(soindex);
				})
				 gmapinfodown.on('click',
				function() {
					soindex = soindex - 1 < 0 ? address.length - 1 : soindex - 1;
					gmapMagnify(soindex);
				})
				gmapinfoclose.on("click",
				function() {
					gmapinfo.fadeOut();
					markers[soindex].setAnimation(null);
				})
			}else{
				gmapinfoup.hide()
				gmapinfoup.hide();
				gmapinfodown.hide();
				gmapBox.gmap3({
					map: {
						options: {
							zoom: 2
						}
					}
				});
				gmapinfoclose.on("click",
				function() {
					gmapinfo.fadeOut();
					markers[soindex].setAnimation(null);
				})
			}
			full.on("click",function(){
			if($(this).hasClass("active")){
				fcpGmap.removeClass("active");
				fcpGmap.children(".gmap_full").html(jsViewLanguage.Fullscreen).removeClass("active");
				gmapBox.gmap3({trigger:"resize"});
				
			}else{
				fcpGmap.addClass("active");
				fcpGmap.children(".gmap_full").html(jsViewLanguage.ExitFullscreen).addClass("active");
				gmapBox.gmap3({trigger:"resize"});
			}
			})
		
		}
		
	//	console.log(typeof mapcallback)
		if (typeof($(document).gmap3) == "function") { 
			Ev_NewMap();
		}else{
			
			if (typeof mapcallback == 'function') { 
			var Ev_MapList = mapcallback;
				mapcallback = function(){
					Ev_MapList();
					Ev_NewMap();
				}
			}else{
				mapcallback = function(){
					Ev_NewMap();
				}
			}
		}

	},
	destroyEvents: function() {
		this.el.children(".fcp_Gmap").remove();
	}
});
FC.views.GMap = GMap;
EAccordion = View.extend({
	initialize: function() {},
	render: function() {
		this.el.children(".fcp-accordionBox").remove();
		this.el.children(".fcp-accordionmore").remove();
		this.el.append("<div class=\"fcp-accordionBox\"></div>")
		main = this.el.parents(".calendar"),
		viewBox = main.children(".calendar-view"),
		accordionTemplate = main.children(".fcp-Template-accordion"),
		accordionContentTemplate = main.children(".fcp-Template-accordion-content");
		ExtensionsTemplate = main.children(".fcp-Template-Extensions");
	},
	setHeight: function(height, isAuto) {},
	renderEvents: function(events) {
		var currentDayStart = this.start.clone();
		var periodEnd = this.end.clone();
		var dateSame = currentDayStart.format("DD") - 1;
		var acbox = this.el.children(".fcp-accordionBox");
		id = this.el.parents(".calendar").data("id");
		accordionDefault = eval("accordionDefault" + id);
		var today = new Date();

		var appendbox=$("<div>");

		for (var i=0;i< events.length;i++) {
			var e = events[i];
			var eventStart = e.start.clone();
			var eventEnd = this.calendar.getEventEnd(e);
			if ((currentDayStart <= eventStart || currentDayStart == eventStart) && periodEnd > eventStart) {
			//if((eventStart.isAfter(currentDayStart) || currentDayStart == eventStart ) && eventStart.isBefore(periodEnd)){
				dateSame = eventStart.format("DD");
				e["day"] = e.start.format("DD");
				e["month"] = e.start.format("MMM");
				//if (e.end) {
				//	e["endday"] = e.end.format("DD");
				//	e["endmonth"] = e.end.format("MMM");
				//}
				var cCont = accordionTemplate.tmpl(e);
				var aa = i;
				if (today.format("dd") == eventStart.format("DD")) {
					cCont.addClass("today")
				}
				cCont.children(".fcp-title").data("index", i);
				cCont.children(".fcp-title").on("click",function(movies) {
					var thisBox = $(this);
					if (thisBox.siblings(".fcp-content").html().length == 0) {
						var e = events[$(this).data("index")];
						//if (!e.ExtensionsTemplate) {
						//	ExtensionsBox(e);
						//}
						thisBox.siblings(".fcp-content").html(accordionContentTemplate.tmpl(e));
						
						thisBox.siblings(".fcp-content").slideToggle(200,
						function() {
							if (e.Location) {
							var ev_gmap_Acc=function(){
								
								thisBox.siblings(".fcp-content").children('.fcp-gmap-' + e.ID).gmap3({
									marker: {
										values: [{
											latLng:[e.Address.Latitude,e.Address.Longitude],
											data:"<h5>"+e.Address.Title+"</h5>"+e.Address.MarkerText,
											options: {
												icon: e.Address.MarkerIcon
											}
										}],
										options: {
											draggable: false
										},
										events: {
											click: function(marker, event, context) {
												var map = $(this).gmap3("get"),
												infowindow = $(this).gmap3({
													get: {
														name: "infowindow"
													}
												});
												if (infowindow) {
													infowindow.open(map, marker);
													infowindow.setContent(context.data);
												} else {
													$(this).gmap3({
														infowindow: {
															anchor: marker,
															options: {
																content: context.data
															}
														}
													});
												}
											}
										}
									},
									map: {
										options: {
											zoom: accordionDefault.gmapZoom,
											scrollwheel: accordionDefault.gmapscrollwheel,
										}
									}
								})
								}
								
								if (typeof($(document).gmap3) == "function" ) { 
									ev_gmap_Acc();
								}else{
									if (typeof mapcallback == 'function') { 
									var Ev_MapList_Acc = mapcallback;
										mapcallback = function(){
											Ev_MapList_Acc();
											ev_gmap_Acc();
										}
									}else{
										mapcallback = function(){
											ev_gmap_Acc();
										}
									}
								}
								
							}
						});
					} else {
						thisBox.siblings(".fcp-content").slideToggle(200);
					}
				})
				appendbox.append(cCont);
			}
		}
		appendbox = appendbox.children();
		if(appendbox.length > accordionDefault.Display ){
			var more_s=0,more_e=accordionDefault.Display,ScrollMaxPage=0,stop=true;;
			acbox.append(appendbox.slice(more_s,more_e));
			var acmore=$("<div class=\"fcp-accordionmore\"><span><i class=\"fa fa-refresh\"></i>"+jsViewLanguage.More+"</span></div>");
				acmore.find(".fa").hide();
				

				var LoadAjaxItems=function(){
					more_s=more_e;
					more_e=more_e + accordionDefault.LoadDisplay;
					var atm=$("<div>").hide();
					acbox.append( atm.append(appendbox.slice(more_s,more_e)));
					atm.slideDown(300,function(){stop=true});
					if(more_e >= appendbox.length){
						stop=false;
						acmore.remove();
					}
					ScrollMaxPage++;
					acmore.find(".fa").fadeOut();
				}


			acmore.on("click",function(){
				if(stop){
					stop=false;
					acmore.find(".fa").css("display","inline-block");
					setTimeout(LoadAjaxItems,500);
				}		
			})
			
			if(accordionDefault.loadeddata=="On Scroll"){
				$(window).scroll(function() {
					if ($(window).scrollTop() + $(window).height() > acmore.offset().top ) {
						if(ScrollMaxPage < accordionDefault.ScrollMaxPage && stop){ 
							stop=false;
							acmore.find(".fa").css("display","inline-block");
							setTimeout(LoadAjaxItems,500);
						}
					}
				});
			}
			
			
			this.el.append(acmore);
		}else{
			acbox.append(appendbox)
		}
		
		if (this.el.find(".fcp-accordion").length == 0) {
			acbox.append('<div class="not">'+jsViewLanguage.NoEvents+'</div>')
		};
	},
	destroyEvents: function() {
		this.el.children(".fcp-accordionBox").remove();
		this.el.children(".fcp-accordionmore").remove();
	}
});
FC.views.EAccordion = EAccordion;
var MobileMonth = View.extend({

    render: function() {
		this.el.append("<div class=\"fcp-MobileMonth\"></div>")
		main = this.el.parents(".calendar"),
		viewBox = main.children(".calendar-view"),
		tool = main.children(".fcp-tooltip")
		if(!tool.length){
			tool = $("body").children(".fcp-tooltip");
		}

		toolClose = tool.children(".fc-close-tooltip"),
		toolMain = tool.children(".fcp-main"),
		tooltipTemplate = main.children(".fcp-Template-tooltip"),
		ExtensionsTemplate = main.children(".fcp-Template-Extensions");
		feb = main.find(".fc-EventMobileMonth-button"),
		fmb = main.find(".fc-month-button");
		feb.show();
		fmb.hide();
		
    },
    renderEvents: function(events) {
		var noDebug = true;
		noDebug || console.log(events);
		var eventsCopy = events.slice().reverse(); 
		var periodEnd = this.end.clone();
		noDebug || console.log('Period start: ' + this.start.format("YYYY MM DD HH:mm:ss Z") + ', and end: ' + this.end.format("YYYY MM DD HH:mm:ss Z"));

		
		var currentDayStart = this.start.clone();
		while (currentDayStart.isBefore(periodEnd)) {
			var didAddDayHeader = false;
			var currentDayEnd = currentDayStart.clone().add(1, 'days');

			noDebug || console.log('=== this day start: ' + currentDayStart.format("YYYY MM DD HH:mm:ss Z") + ', and end: ' + currentDayEnd.format("YYYY MM DD HH:mm:ss Z"));
			var list =$("<div class=\"fcp-event-list\"></div>");
				list.append('<div class="fcp-event-date">'+currentDayStart.format("DD")+'</div>')
			for (var i = eventsCopy.length - 1; i >= 0; --i) {
				var e = eventsCopy[i];
				var eventStart = e.start.clone();
				var eventEnd = this.calendar.getEventEnd(e);
			 if (currentDayStart.isAfter(eventEnd) || (currentDayStart.isSame(eventEnd) && !eventStart.isSame(eventEnd)) || periodEnd.isBefore(eventStart)) {
					eventsCopy.splice(i, 1);
					noDebug || console.log("--- Removed the above event");
				} else if (currentDayEnd.isAfter(eventStart)) {
						if(e["url"]){
							var segEl =$('<a href="'+e["url"]+'" title="'+e.title+'"><div class="fcp-event-title" style="background-color:'+e.color+'" >' + e.title + '</div></a>')
							list.append(segEl);
						}else{
							var segEl =$('<div class="fcp-event-title" style="background-color:'+e.color+'">' + e.title + '</div>')
							list.append(segEl);
							(function(_this, myEvent, mySegEl) {
								segEl.on('click', function(ev) {
						



									eventClickTooltip(myEvent, ev, segEl ,main,viewBox ,tool,toolClose,toolMain,tooltipTemplate)

									//return _this.trigger('eventClick', myEvent, ev,mySegEl);

								});
							})(this, e, segEl);
						}
				}
			}
			
			this.el.children(".fcp-MobileMonth").append(list)
			
			currentDayStart.add(1, 'days');
		}
	},
    destroyEvents: function() {
	 this.el.children(".fcp-MobileMonth").remove();
    }
});
FC.views.MobileMonth = MobileMonth; // register our class with the view system


var eventClickTooltip = function(calEvent, jsEvent, view, main, viewBox, tool, toolClose, toolMain, tooltipTemplate) {
	var e = calEvent;
//	if (!e.ExtensionsTemplate) {
//		ExtensionsBox(e);
//	}
	if(toolMain.find(".mCSB_container").length){
		toolMain.mCustomScrollbar("disable");
	}
	toolMain.html(tooltipTemplate.tmpl(e));
	toolMain.css({
		"max-width": $(window).width()-20,
		"max-height": $(window).height() / 1.5
	});
	var w = tool.outerWidth(),
	h = tool.outerHeight(),
	ww = $(window).width(),
	wt = $(window).scrollTop(),
	X = jsEvent.pageX,
	Y = jsEvent.pageY;
	if (X > ww / 2) {
		var left = X  - w;
		if (left-10 < 0) {
			left = 0
		}
	} else {
		var left = X ;
		if (left + w+ 20 > ww) {
			left = ww - w -20
		}
	}
	var top = Y - h / 2 < wt ? wt: Y - h / 2;
	tool.hide().stop().fadeIn().css({
		"top": top,
		"left": left
	});
	toolMain.mCustomScrollbar({
		autoHideScrollbar: true,
		theme: "light-thin"
	});
	ImgLoad(function() { 
		toolMain.mCustomScrollbar("update");
	},toolMain)

}
var moviesResize = function(view, em, main, viewBox, tool, toolClose, toolMain) {
	
	main.find(".fc-once").removeClass("fc-once");
	tool.hide();
	var feb = main.find(".fc-EventMobileMonth-button"),
	fmb = main.find(".fc-month-button");
	if ($(window).width() < 768 && view.name == "month") {
		feb.show();
		fmb.hide();
		viewBox.fullCalendar('changeView','EventMobileMonth');
		
	}
	if ($(window).width() >= 768 && view.name == "EventMobileMonth") {
		feb.hide();
		fmb.show();
		viewBox.fullCalendar('changeView', 'month');
	}
}