﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Manager_Event_Address.ascx.cs" Inherits="DNNGo.Modules.xEvent.Manager_Event_Address" %>

<script type="text/javascript" src='https://maps.google.com/maps/api/js?sensor=true&libraries=places&key=<%=ViewSetting("General.APIKey","") %>&cdv=<%=CrmVersion %>'></script>
<script src="<%=ModulePath %>Resource/plugins/location/locationpicker.jquery.js?cdv=<%=CrmVersion %>"></script>
<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">
        <!-- start: PAGE TITLE & BREADCRUMB -->

        <div class="page-header">
            <h1><i class="fa clip-earth"></i><%=ViewResourceText("Header_Title", "Add New Address")%></h1>
        </div>
        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<!-- end: PAGE HEADER -->
<!-- start: PAGE CONTENT -->

<div class="row">
    <div class="col-sm-12">


        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i><%=ViewResourceText("Title_BaseSettings", "Basic Settings")%>
                <div class="panel-tools"><a href="#" class="btn btn-xs btn-link panel-collapse collapses"></a></div>
            </div>
            <div class="panel-body">
                <div class="form-horizontal">

                     <div class="form-group">
                        <%=ViewControlTitle("lblActivation", "Activate", "cbActivation", ":", "col-sm-3 control-label")%>
                        <div class="col-sm-5">
                            <div class="checkbox-table">
                                <asp:CheckBox ID="cbActivation" runat="server" CssClass="flat-grey auto" /></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <%=ViewControlTitle("lblTitle", "Address Title", "txtTitle", ":", "col-sm-3 control-label")%>
                        <div class="col-sm-5">
                            <asp:TextBox runat="server" ID="txtTitle" placeholder="Address Title" CssClass="form-control validate[required]"></asp:TextBox>
                        </div>
                    </div>

                    <div class="form-group">
                        <%=ViewControlTitle("lblPicture", "Picture", "txtPicture", ":", "col-sm-3 control-label")%>
                        <div class="col-sm-5">
                            <asp:PlaceHolder ID="phPicture" runat="server"></asp:PlaceHolder>
                        </div>
                    </div>

                    <div class="form-group">
                        <%=ViewControlTitle("lblDescription", "Description", "txtDescription", ":", "col-sm-3 control-label")%>
                        <div class="col-sm-5">
                            <asp:TextBox runat="server" ID="txtDescription" placeholder="Description" CssClass="form-control validate[maxSize[500]]" TextMode="MultiLine" Rows="4"></asp:TextBox>
                        </div>
                    </div>

                    
                  
                    <div class="form-group">
                        <%=ViewControlTitle("lblCountry", "Country/State/City", "txtTitle", ":", "col-sm-3 control-label")%>
                        <div class="col-sm-2">
                            <asp:DropDownList ID="ddlCountry" runat="server" CssClass="form-control search-select validate[groupRequired[Country]]"></asp:DropDownList>
                        </div>
                        <div class="col-sm-2">
                            <asp:TextBox runat="server" ID="txtState" placeholder="State" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-sm-2">
                            <asp:TextBox runat="server" ID="txtCity" placeholder="City" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <%=ViewControlTitle("lblStreet", "Street", "txtStreet", ":", "col-sm-3 control-label")%>
                        <div class="col-sm-6">
                            <asp:TextBox runat="server" ID="txtStreet" placeholder="Street" CssClass="form-control validate[required]"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <%=ViewControlTitle("lblPostalCode", "PostalCode", "txtPostalCode", ":", "col-sm-3 control-label")%>
                        <div class="col-sm-2">
                            <asp:TextBox runat="server" ID="txtPostalCode" placeholder="Postal Code" CssClass="form-control" Width="200"></asp:TextBox>
                        </div>
                    </div>

                    <div class="form-group">
                        <%=ViewControlTitle("lblPhone", "Phone", "txtPhone", ":", "col-sm-3 control-label")%>
                        <div class="col-sm-5">
                            <asp:TextBox runat="server" ID="txtPhone" placeholder="Phone" CssClass="form-control validate[custom[phone]]"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <%=ViewControlTitle("lblFax", "Fax", "txtFax", ":", "col-sm-3 control-label")%>
                        <div class="col-sm-5">
                            <asp:TextBox runat="server" ID="txtFax" placeholder="Fax" CssClass="form-control validate[custom[phone]]"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <%=ViewControlTitle("lblEmail", "Email", "txtEmail", ":", "col-sm-3 control-label")%>
                        <div class="col-sm-5">
                            <asp:TextBox runat="server" ID="txtEmail" placeholder="Email" CssClass="form-control validate[custom[email]]"></asp:TextBox>
                        </div>
                    </div>


                    <div class="form-group">
                        <%=ViewControlTitle("lblWebSite", "WebSite", "txtWebSite", ":", "col-sm-3 control-label")%>
                        <div class="col-sm-5">
                            <asp:TextBox runat="server" ID="txtWebSite" placeholder="http://" CssClass="form-control validate[custom[url]]"></asp:TextBox>
                        </div>
                    </div>



                   

                </div>
            </div>
        </div>





        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i><%=ViewResourceText("Title_Maps", "Map Settings")%>
                <div class="panel-tools"><a href="#" class="btn btn-xs btn-link panel-collapse collapses"></a></div>
            </div>
            <div class="panel-body">
                <div class="form-horizontal">
                    <div class="form-group">
                        <%=ViewControlTitle("lblCoordinates", "Longitude/latitude/zoom", "txtCoordinatesX", ":", "col-sm-3 control-label")%>
                        <div class="col-sm-2">
                            <asp:TextBox runat="server" ID="txtCoordinatesX" placeholder="Longitude" CssClass="form-control validate[required]"></asp:TextBox>
                        </div>
                        <div class="col-sm-2">
                            <asp:TextBox runat="server" ID="txtCoordinatesY" placeholder="Latitude" CssClass="form-control validate[required]"></asp:TextBox>
                        </div>
                        <div class="col-sm-2">
                            <asp:TextBox runat="server" ID="txtZoom" placeholder="Zoom" CssClass="form-control validate[required]"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <%=ViewControlTitle("lblMapAddress", "Goole Map Address", "txtMapAddress", ":", "col-sm-3 control-label")%>
                        <div class="col-sm-6">
                            <span class="input-icon input-icon-right">
									<asp:TextBox runat="server" ID="txtMapAddress" placeholder="Goole Map Address" CssClass="form-control validate[required]"></asp:TextBox>
									<i class="fa fa-search"></i> 
                            </span>
                             
                            
                             <%=ViewHelp("lblMapAddress","You can either type in addresses or coordinates through the input box, or generate addresses and coordinates by drag and drop.") %>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-2"></div>
                        <div class="col-sm-8">
                           
                            <div id="google_maps" class="google_maps" style="height: 300px;"></div>
                            
                            <script type="text/javascript">
                                $('#google_maps').locationpicker({
                                    location: { latitude: $('#<%=txtCoordinatesY.ClientID %>').val(), longitude: $('#<%=txtCoordinatesX.ClientID %>').val() },
                                     radius: 200,
                                     inputBinding: {
                                         latitudeInput: $('#<%=txtCoordinatesY.ClientID %>'),
                                         longitudeInput: $('#<%=txtCoordinatesX.ClientID %>'),
                                         locationNameInput: $('#<%=txtMapAddress.ClientID %>')
                                     },
                                     enableAutocomplete: true,
                                     onchanged: function (currentLocation, radius, isMarkerDropped) {
                                         $('#<%=txtMapAddress.ClientID %>').val($(this).locationpicker('map').location.formattedAddress);
                                     }
                                 });
                            </script>
                        </div>
                    </div>

                


                   <div class="form-group">
                        <%=ViewControlTitle("lblMarkerIcon", "Marker Icon", "phMarkerIcon", ":", "col-sm-3 control-label")%>
                        <div class="col-sm-5">
                            <asp:PlaceHolder ID="phMarkerIcon" runat="server"></asp:PlaceHolder>
                        </div>
                    </div>



                    <div class="form-group">
                        <%=ViewControlTitle("lblMarkerText", "Marker Text", "txtMarkerText", ":", "col-sm-3 control-label")%>
                        <div class="col-sm-5">
                            <asp:TextBox runat="server" ID="txtMarkerText" placeholder="Marker Text" CssClass="form-control  validate[maxSize[500]]" TextMode="MultiLine" Rows="3"></asp:TextBox>
                        </div>
                    </div>





                </div>
            </div>
        </div>


    </div>

</div>
<!-- end: PAGE CONTENT-->

<div class="row">
    <div class="col-sm-2"></div>
    <div class="col-sm-10">
        <asp:Button CssClass="btn btn-primary" lang="Submit" ID="cmdUpdate" resourcekey="cmdUpdate"
            runat="server" Text="Update" OnClick="cmdUpdate_Click"></asp:Button>&nbsp;
        <asp:Button CssClass="btn btn-default" ID="cmdCancel" resourcekey="cmdCancel" runat="server"
            Text="Cancel" CausesValidation="False" OnClick="cmdCancel_Click" OnClientClick="CancelValidation();"></asp:Button>&nbsp;
        
    </div>
</div>
