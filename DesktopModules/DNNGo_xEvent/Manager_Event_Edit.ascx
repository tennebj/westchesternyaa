﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Manager_Event_Edit.ascx.cs"
    Inherits="DNNGo.Modules.xEvent.Manager_Event" %>

<link rel="stylesheet" href="<%=ModulePath %>Resource/plugins/jQuery-Tags-Input/jquery.tagsinput.css?cdv=<%=CrmVersion %>">
<script src="<%=ModulePath %>Resource/plugins/jQuery-Tags-Input/jquery.tagsinput.js?cdv=<%=CrmVersion %>"></script>


<link rel="stylesheet" href="<%=ModulePath %>Resource/plugins/jquery-labelauty/css/style.css?cdv=<%=CrmVersion %>">
<script type="text/javascript" src="<%=ModulePath %>Resource/plugins/jquery-labelauty/js/jquery.labelauty.js?cdv=<%=CrmVersion %>"></script>


<script type="text/javascript" src='https://maps.google.com/maps/api/js?sensor=true&libraries=places&key=<%=ViewSetting("General.APIKey","") %>&cdv=<%=CrmVersion %>'></script>
<script src="<%=ModulePath %>Resource/plugins/gmaps/gmaps.js?cdv=<%=CrmVersion %>"></script>
 
<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">
        <!-- start: PAGE TITLE & BREADCRUMB -->
        <div class="page-header">
            <h1>
                <i class="fa fa-plus"></i> <%=ViewResourceText("Header_Title", "Add New Event")%>
                <%--<small>overview &amp; stats </small>--%></h1>
        </div>
        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<!-- end: PAGE HEADER -->
<!-- start: PAGE CONTENT -->
<div class="row">
    <div class="col-sm-8">
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label for="form-field-1">
                        <%=ViewTitle("lblTitle", "Title", "txtTitle")%>:</label>
                        <asp:TextBox ID="txtTitle" placeholder="Title" runat="server" CssClass="form-control validate[required,maxSize[500]]"></asp:TextBox>
                </div>
                <div class="form-group">
                    <label for="form-field-2">
                        <%=ViewTitle("lblSummary", "Summary", "txtSummary")%>:</label>
                    <asp:TextBox ID="txtSummary" placeholder="Summary" CssClass="form-control limited validate[maxSize[2000]]"
                        runat="server" Rows="3" TextMode="MultiLine"></asp:TextBox>
                </div>
                <div class="form-group">
                    <label for="form-field-3">
                        <%=ViewTitle("lblContentText", "ContentText", "txtContentText")%>:
                    </label>
                   <!-- <asp:HyperLink ID="hlAddMedia" resourcekey="hlAddMedia" runat="server" CssClass="btn btn-light-grey" Text="<i class='fa fa-picture-o'></i> Add Media"  data-toggle="modal" NavigateUrl="#Picture_Modal"></asp:HyperLink>-->
                    <asp:TextBox ID="txtContentText" CssClass="ckeditor_ form-control" runat="server"
                        Height="300" Width="100%" TextMode="MultiLine"></asp:TextBox>
                  <%--  <dnn:TextEditor id="txtContentTelerik" runat="server" height="400" width="100%"/>--%>
                </div>
            </div>
        </div>



        <!-- start: TEXT AREA PANEL -->
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i> <%=ViewResourceText("Title_LocationSettings", "Location Settings")%>
                <div class="panel-tools">
                    <a href="#" class="btn btn-xs btn-link panel-collapse collapses"></a>
                </div>
            </div>
            <div class="panel-body">
                 <div class="form-group">
                    <%=ViewTitle("lblLocation", "Address", "txtLocation")%>:
                    <asp:DropDownList runat="server" ID="ddlAddress" CssClass=" search-select-map search-select" Width="100%"></asp:DropDownList>
                </div>
                <div class="form-group">
                    
                    <div id="google_maps" class="google_maps" style="height: 200px; position: relative; overflow: hidden; transform: translateZ(0px); background-color: rgb(229, 227, 223);"></div>
                     <script type="text/javascript">
                         jQuery(function ($) {

                             function LoadMaps(AddressID)
                             {
                                 if (AddressID > 0)
                                 {
                                     $.getJSON("<%=ModulePath %>Resource_Service.aspx?Token=AjaxAddresses&AddressID=" + AddressID, Module, function (json) {
                                         if (json.server == "true") {
                                             map = new GMaps({
                                                 el: '#google_maps',
                                                 lat: json.Latitude,
                                                 lng: json.Longitude
                                             });
                                             map.addMarker({
                                                 lat: json.Latitude,
                                                 lng: json.Longitude,
                                                 title: json.Title,
                                                 infoWindow: {
                                                     content: json.Title + ":" + json.Street + " " + json.City + " " + json.State + " " + json.Country + "<br /> " + json.MarkerText
                                                 }
                                             });
                                         }
                                         else
                                         {
                                             $("#google_maps").html("<div class=\"alert alert-warning\"><button data-dismiss=\"alert\" class=\"close\">×</button><%=ViewResourceText("NoAddress", "")%><div>");
                                         }
                                     });  
                                 } else
                                 {
                                     $("#google_maps").html("<div class=\"alert alert-warning\"><button data-dismiss=\"alert\" class=\"close\">×</button><%=ViewResourceText("NoAddress", "")%><div>");
                                 }
                                 
                             }

                            
                            
                    

                         $(".search-select-map").select2({
                             allowClear: true
                         }).on("change", function (e) {
                             LoadMaps(e.val);
                         });

                         LoadMaps($("#<%=ddlAddress.ClientID %> option:selected").val());
                      });
                        
                    </script>
                </div>
                
            </div>
        </div>

         <div runat="server" id="divOptions" visible="false">
            <asp:Repeater ID="RepeaterGroup" runat="server" OnItemDataBound="RepeaterGroup_ItemDataBound">
                <ItemTemplate>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-external-link-square"></i>
                            <%#Eval("key")%>
                            <div class="panel-tools">
                                <a href="#" class="btn btn-xs btn-link panel-collapse collapses"></a>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal">
                                <asp:Repeater ID="RepeaterOptions" runat="server" OnItemDataBound="RepeaterOptions_ItemDataBound">
                                    <ItemTemplate>
                                        <div class="form-group">
                                            <asp:Literal ID="liTitle" runat="server"></asp:Literal>
                                            <div class="col-sm-9">
                                                <asp:PlaceHolder ID="ThemePH" runat="server"></asp:PlaceHolder>
                                                <asp:Literal ID="liHelp" runat="server"></asp:Literal>
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </div>
                        </div>
                        <!-- end: TEXT AREA PANEL -->
                    </div>
                </ItemTemplate>
            </asp:Repeater>
        </div>


        <!-- start: TEXT AREA PANEL -->
        <div class="panel panel-default" id="div_group_seo" runat="server">
            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i> <%=ViewResourceText("Title_SEOSettings", "SEO Settings")%>
                <div class="panel-tools">
                    <a href="#" class="btn btn-xs btn-link panel-collapse collapses"></a>
                </div>
            </div>
            <div class="panel-body">
                 <div class="form-group">
                    <%=ViewTitle("lblFriendlyUrl", "Friendly Urls", "txtFriendlyUrl")%>:
                    <asp:TextBox ID="txtFriendlyUrl" CssClass="form-control validate[maxSize[500]]" runat="server"></asp:TextBox>
                </div>
                <div class="form-group">
                    <%=ViewTitle("lblSearchTitle", "SEO Title", "txtSearchTitle")%>:
                    <asp:TextBox ID="txtSearchTitle" CssClass="form-control validate[maxSize[500]]" runat="server"></asp:TextBox>
                </div>
                <div class="form-group">
                    <%=ViewTitle("lblSearchKeywords", "SEO Keywords", "txtSearchKeywords")%>:
                    <asp:TextBox ID="txtSearchKeywords" CssClass="form-control limited validate[maxSize[500]]"
                        runat="server" Rows="2" TextMode="MultiLine"></asp:TextBox>
                </div>
                <div>
                    <%=ViewTitle("lblSearchDescription", "SEO Description", "txtSearchDescription")%>:
                    <asp:TextBox ID="txtSearchDescription" CssClass="autosize form-control validate[maxSize[500]]"
                        runat="server" Rows="4" TextMode="MultiLine"></asp:TextBox>
                </div>
            </div>
        </div>

       

    </div>
    <div class="col-sm-4">
        <!-- start: SELECT BOX PANEL -->
        <!--Start-->
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i> <%=ViewResourceText("Title_Publish", "Publish")%>
                <div class="panel-tools">
                    <a href="#" class="btn btn-xs btn-link panel-collapse collapses"></a>
                </div>
            </div>
            <div class="panel-body buttons-widget">
                <div class="row">
                    <div class="col-sm-6">
                        <asp:Button CssClass="btn btn-light-grey" ID="cmdSaveDraft" resourcekey="cmdSaveDraft"
                            runat="server" Text="Save Draft" CausesValidation="False" OnClick="cmdSaveDraft_Click"
                            OnClientClick="CancelValidation();"></asp:Button>
                    </div>
                    <div class="col-sm-6 text_right">
                        <asp:HyperLink ID="hlPreview" resourcekey="hlPreview" runat="server" CssClass="btn btn-light-grey" Text="<i class='fa clip-link'></i> Preview"
                            Visible="false" Target="_blank"></asp:HyperLink>
                    </div>
                </div>
                <ul class="Edit_List" id="accordion">
                    <li>
                        <p>
                            <i class="fa clip-grid-5"></i>&nbsp;&nbsp;<%=ViewResourceText("Title_Status", "Status")%>: <b>
                            </p>
                        <div class="panel-collapse" id="Status">
                            <div class="row form-group">
                                <div class="col-sm-6">
                                    <asp:RadioButtonList ID="rblEventStatus" runat="server" CssClass="form-control auto"></asp:RadioButtonList>
                                    <asp:DropDownList ID="ddlEventStatus" runat="server" CssClass="form-control">
                                    </asp:DropDownList>
                                    <asp:DropDownList ID="ddlStatusByUser" runat="server" CssClass="form-control">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </li>
                  
                    <li  id="divFeature" runat="server">
                        <div class="panel-collapse" id="Feature">
                            <div class="row form-group">
                                <div class="col-sm-8">
                                    <div class="make-switch" data-on="primary" data-off="info">
                                        <asp:CheckBox ID="cbFeature" runat="server"   />
                                    </div>
                                    <label for="<%=cbFeature.ClientID %>">&nbsp;&nbsp;<i class="fa clip-clipboard"></i>&nbsp;<%=ViewResourceText("Title_Feature", "Feature")%></label>
                                </div>
                            </div>
                        </div>
                    </li>
                      <li  id="divTopStatus" runat="server">
                        <div class="panel-collapse" id="TopStatus">
                            <div class="row form-group">
                                <div class="col-sm-8">
                                    <div class="make-switch" data-on="primary" data-off="info">
                                        <asp:CheckBox ID="cbTopStatus" runat="server" />
                                    </div>
                                    <label for="<%=cbTopStatus.ClientID %>">&nbsp;&nbsp;<i class="fa clip-thumbs-up"></i>&nbsp;<%=ViewResourceText("Title_TopStatus", "Top Status")%></label>
                                </div>
                            </div>
                        </div>
                    </li>


                </ul>
                <div class="row">
                	<br/>
                    <div class="col-sm-5">
                        <asp:Button CssClass="btn btn-link" ID="cmdDelete" resourcekey="cmdMovetoTrash" runat="server"
                            Text="Move to Trash" CausesValidation="False" OnClick="cmdDelete_Click" OnClientClick="CancelValidation();">
                        </asp:Button>
                    </div>
                    <div class="col-sm-7 text_right">
                        <asp:Button CssClass="btn btn-primary btn-sm" lang="Submit" ID="cmdUpdate" resourcekey="cmdUpdate"
                            runat="server" Text="Update" OnClick="cmdUpdate_Click"></asp:Button>&nbsp;
                        <asp:Button CssClass="btn btn-primary btn-sm" ID="cmdCancel" resourcekey="cmdCancel"
                            runat="server" Text="Cancel" CausesValidation="False" OnClick="cmdCancel_Click"
                            OnClientClick="CancelValidation();"></asp:Button>&nbsp;
                    </div>
                </div>
            </div>
        </div>



            <!--Time and date-->
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i><%=ViewResourceText("Title_DateTime", "Time & Date")%>
                <div class="panel-tools">
                    <a href="#" class="btn btn-xs btn-link panel-collapse collapses"></a>
                </div>
            </div>
            <div class="panel-body buttons-widget">
                 <ul class="Edit_List" id="ul_DateTime">
                    <li  id="divAllDayEvent" runat="server">
                        <div class="panel-collapse" id="AllDayEvent">
                            <div class="row form-group">
                                <div class="col-sm-12">
                                    <div class="make-switch" data-on="primary" data-off="info">
                                        <asp:CheckBox ID="cbAllDayEvent" runat="server" />
                                    </div>
                                    <label for="<%=cbAllDayEvent.ClientID %>">&nbsp;&nbsp;<i class="fa clip-thumbs-up"></i>&nbsp;<%=ViewResourceText("Title_AllDayEvent", "All Day Event")%></label>
                                </div>
                            </div>
                        </div>
                    </li>


                    <li>
                        <p>
                            <i class="fa clip-calendar-3"></i>&nbsp;<%=ViewResourceText("Title_Start", "Start")%>: <b></p>
                        <div class="panel-collapse" id="Start">
                            <div class="row form-group">
                                <div class="col-md-6 input-group">
                                    <asp:TextBox ID="txtStartDate" runat="server" data-date-format="mm/dd/yyyy" data-date-viewmode="years"
                                        CssClass="form-control date-picker"></asp:TextBox>
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                </div>
                                <div id="divStartTime" class="col-md-5 input-group input-append bootstrap-timepicker">
                                    <asp:TextBox ID="txtStartTime" runat="server" CssClass="form-control time-picker"></asp:TextBox>
                                    <span class="input-group-addon add-on"><i class="fa fa-clock-o"></i></span>
                                </div>
                            </div>

                        </div>
                    </li>
                    <li>
                        <p>
                            <i class="clip-stopwatch"></i>&nbsp;&nbsp;<%=ViewResourceText("Title_Disable", "Disable")%>: <b>
                         </p>
                        <div class="panel-collapse" id="DisableDateTime">
                            <div class="row form-group">
                                <div class="col-md-6 input-group">
                                    <asp:TextBox ID="txtDisableDate" runat="server" data-date-format="mm/dd/yyyy" data-date-viewmode="years"
                                        CssClass="form-control date-picker"></asp:TextBox>
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                </div>
                                <div id="divDisableTime" class="col-md-5 input-group input-append bootstrap-timepicker">
                                    <asp:TextBox ID="txtDisableTime" runat="server" CssClass="form-control time-picker"></asp:TextBox>
                                    <span class="input-group-addon add-on"><i class="fa fa-clock-o"></i></span>
                                </div>
                            </div>
                           
                        </div>
                    </li>
                    <li  id="divRepeat" runat="server">
                        <p>
                            <i class="fa fa-repeat"></i>&nbsp;&nbsp;<%=ViewResourceText("Title_Repeat", "Repeat")%>: <b>
                         </p>
                        <div class="panel-collapse" id="Repeat">
                            <div class="row form-group">
                                <div class="col-sm-6">
                                     <asp:DropDownList ID="ddlRepeat" runat="server" CssClass="form-control"></asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </li>


                      <li  id="divMoreOptions" runat="server">
                        <div class="panel-collapse" id="MoreOptions">
                            <div class="row form-group">
                                <div class="col-sm-4">
                                  <p>  <i class="fa fa-repeat"></i>&nbsp;&nbsp; <%=ViewTitle("lblRepeatEvery", "Repeat every", "txtRepeatEveryInterval")%>:</p>
                                </div>
                                <div class="col-sm-2">
                                     <asp:TextBox ID="txtRepeatEveryInterval" runat="server" CssClass="form-control validate[required,custom[integer],min[1],max[99]]"></asp:TextBox>
                                </div>
                                  <div class="col-sm-4">
                                    <asp:DropDownList ID="ddlRepeatEvery" runat="server" CssClass="form-control"></asp:DropDownList>
                                </div>
                            </div>

                             <div id="divFollowMonths" runat="server">
                                 <div class="row form-group">
                                    <div class="col-sm-8"><%=ViewTitle("lblFollowMonths", "On the following months", "")%>:</div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-sm-12">
                                            <asp:Literal ID="liFollowMonths" runat="server"></asp:Literal>
                                    </div>
                                </div>
                             </div>


                             <div id="divFollowWeekOfYear" runat="server"> 
                                <div class="row form-group">
                                    <div class="col-sm-8"><%=ViewTitle("lblFollowWeekOfYear", "On the following week of year", "")%>:</div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-sm-12">
                                        <asp:Literal ID="liFollowWeekOfYear" runat="server"></asp:Literal>
                                    </div>
                                </div>
                            </div>

                             <div id="divFollowDaysOfMonth" runat="server"> 

                                <div class="row form-group">
                                    <div class="col-sm-8"><%=ViewTitle("lblFollowDaysOfMonth", "On the following days of the month", "")%>:</div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-sm-12">
                                        <asp:Literal ID="liFollowDaysOfMonth" runat="server"></asp:Literal>
                                    </div>
                                </div>
                            </div>

                            <div id="divRepeatEveryWeeks" runat="server"> 
                                <div class="row form-group">
                                    <div class="col-sm-8"><%=ViewTitle("lblFollowDaysOfWeeks", "On the following days of the week", "")%>:</div>
                                </div>
                                 <div class="row form-group"  id="divFollowWeekOfMonths" runat="server">
                                    <div class="col-sm-12">
                                        <asp:Literal ID="liFollowWeekOfMonth" runat="server"></asp:Literal>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-sm-12">
                                        <asp:Literal ID="liFollowDaysOfWeeks" runat="server"></asp:Literal>
                                    </div>
                                </div>
                               
                           </div>

                        </div>
                    </li>

                    
                   <li  id="divEvery" runat="server">
                        <div class="panel-collapse" id="Every">
                            <div class="row form-group">
                                <div class="col-sm-5">
                                     <%=ViewTitle("lblEndRepeat", "End repeat", "ddlEndRepeat")%>:
                                </div>
                                <div class="col-sm-6">
                                    <asp:DropDownList ID="ddlEndRepeat" runat="server" CssClass="form-control"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="row form-group"  id="divEndRepeatDate" runat="server">
                                <div class="col-sm-5">
                                     <%=ViewTitle("lblEndRepeatDate", "End Repeat Date", "txtEndRepeatDate")%>:
                                </div>
                                <div class="col-sm-6 input-group">
                                    <asp:TextBox ID="txtEndRepeatDate" runat="server" data-date-format="mm/dd/yyyy" data-date-viewmode="years"
                                        CssClass="form-control date-picker"></asp:TextBox>
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                </div>
                            </div>
                            <div class="row form-group" id="divEndRepeatCount" runat="server">
                                <div class="col-sm-5">
                                     <%=ViewTitle("lblEndRepeatCount", "End RepeatCount", "txtEndRepeatCount")%>:
                                </div>
                                <div class="col-sm-6">
                                     <asp:TextBox ID="txtEndRepeatCount" runat="server" CssClass="form-control validate[required,custom[integer],min[1],max[999]]" Text="1"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </li>
                   <li  id="divRuleDate" runat="server" visible="false">
                        <div class="panel-collapse" id="RuleDate">
                            <div class="row form-group">
                                <div class="col-sm-12">
                                    <div class="btn-group btn-group-reset">
                                         <%--<asp:HyperLink ID="hlAdditionalDate" runat="server" CssClass="btn btn-success tooltips" data-original-title="Set additional dates" data-placement="top" Text=" Set additional dates <i class='fa clip-calendar'></i> "  data-toggle="modal" NavigateUrl="#RuleDate_Modal"></asp:HyperLink>
										 <asp:HyperLink ID="hlExceptionDate" runat="server" CssClass="btn btn-bricky tooltips" data-original-title="Set excluded dates" data-placement="top" Text=" Set excluded dates <i class='fa clip-calendar'></i> "  data-toggle="modal" NavigateUrl="#RuleDate_Modal"></asp:HyperLink>	--%>
                                        <asp:HyperLink ID="hlAdditionalDate" runat="server" CssClass="btn btn-success "   Text=" Set additional dates <i class='fa clip-calendar'></i> "  data-toggle="modal" NavigateUrl="#RuleDate_Modal"></asp:HyperLink>
										 <asp:HyperLink ID="hlExceptionDate" runat="server" CssClass="btn btn-bricky "  Text=" Set excluded dates <i class='fa clip-calendar'></i> "  data-toggle="modal" NavigateUrl="#RuleDate_Modal"></asp:HyperLink>	
										</div>
                                        <script type="text/javascript">jQuery(function ($) { $("#<%=hlAdditionalDate.ClientID%>").click(function () { $("#RuleDate_Iframe").attr("src", $(this).attr("data-href")); $("#RuleDate_EventTitle").text($(this).text()); }); });    </script>
                                        <script type="text/javascript">jQuery(function ($) { $("#<%=hlExceptionDate.ClientID%>").click(function () { $("#RuleDate_Iframe").attr("src", $(this).attr("data-href")); $("#RuleDate_EventTitle").text($(this).text()); }); });    </script>
                                   
                                        <div id="RuleDate_Modal" class="modal fade" tabindex="-1" data-width="820" data-height="450" style="display: none;">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                                    &times;
                                                </button>
                                                <h4 class="modal-title">
                                                    <i class='fa clip-calendar'></i> Rule - <name id="RuleDate_EventTitle"></name> </h4>
                                            </div>
                                            <div class="modal-body">
                                                <iframe id="RuleDate_Iframe" width="100%" height="100%" style="border-width: 0px;">
                                                </iframe>
                                            </div>
                                         </div>
     
                                     </div>
                               
                            </div>
                             
                        </div>
                    </li>

                      
                    </ul>
            </div>
        </div>



         
        <!--Color-->
        <div class="panel panel-default" id="div_group_color" runat="server">
            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i><%=ViewResourceText("Title_Color", "Color")%>
                <div class="panel-tools">
                    <a href="#" class="btn btn-xs btn-link panel-collapse collapses"></a>
                </div>
            </div>
            <div class="panel-body buttons-widget">
               <div id="div_color" class="form-horizontal">
                     <div class="form-group">
                        <div class="col-sm-10">
                            <div class="input-group">
                               <asp:Literal ID="labColorPicker" runat="server"></asp:Literal>
                                <asp:TextBox ID="txtColorPicker" runat="server" CssClass="form-control color-picker-auto input_text validate[required]" data-color-format="rgb"></asp:TextBox>
                             </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <div id="divColorPicker">
                                <asp:Literal ID="liColors" runat="server"></asp:Literal>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>






        <!--Categories-->
        <div class="panel panel-default" id="div_group_categories" runat="server">
            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i> <%=ViewResourceText("Title_Categories", "Categories")%>
                <div class="panel-tools">
                    <a href="#" class="btn btn-xs btn-link panel-collapse collapses"></a>
                </div>
            </div>
            <div class="panel-body buttons-widget">
                <div class="tabbable">
                    <ul id="myTab4" class="nav nav-tabs tab-bricky">
                        <li class="active"><a href="#panel_tab3_example1" data-toggle="tab"><%=ViewResourceText("Title_AllCategories", "All Categories")%> </a>
                        </li>
                        <li><a href="#panel_tab3_example2" data-toggle="tab"><%=ViewResourceText("Title_MostUsed", "Most Used")%> </a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane in active" id="panel_tab3_example1">
                            <div class="checkbox">
                                <asp:Literal runat="server" ID="liCategory"></asp:Literal></div>
                            <asp:HyperLink runat="server" ID="hlAddCategory" resourcekey="hlAddCategory" Text="Add new category"></asp:HyperLink>
                        </div>
                        <div class="tab-pane" id="panel_tab3_example2">
                            <div class="checkbox">
                                <asp:CheckBoxList ID="cblCategory" runat="server" CssClass="MostUsed_Category" DataTextField="Name" DataValueField="ID"></asp:CheckBoxList>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        
         <!--Socaial Gloups-->
        <div class="panel panel-default" id="divSocaialGloups" runat="server" visible="false">
            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i><%=ViewResourceText("Title_Share", "Share")%>
                <div class="panel-tools">
                    <a href="#" class="btn btn-xs btn-link panel-collapse collapses"></a>
                </div>
            </div>
            <div class="panel-body buttons-widget ">
                <div class="form-horizontal">
                    <div class="form-group">
                       <%=ViewControlTitle("lblJournalPost", "Post to journal", "cbJournalPost", ":", "col-sm-5 control-label")%>
                        <div class="col-sm-7">
                             <div class="checkbox-inline">
                                <asp:CheckBox ID="cbJournalPost" runat="server" CssClass="auto"/>
                            </div>
                        </div>
                  </div>

                  <div class="form-group">
                       <%=ViewControlTitle("lblJournalView", "View", "ddlJournalView", ":", "col-sm-5 control-label")%>
                        <div class="col-sm-7">
                                <asp:DropDownList ID="ddlJournalView" runat="server" CssClass="form-control"></asp:DropDownList>
                        </div>
                  </div>
                   <div class="form-group">
                       <%=ViewControlTitle("lblJournalGroups", "Groups", "ddlJournalGroups", ":", "col-sm-5 control-label")%>
                        <div class="col-sm-7">
                            <asp:DropDownList ID="ddlJournalGroups" runat="server" CssClass="form-control"></asp:DropDownList>
                        </div>
                  </div>


                </div>
                   
                  
            </div>
        </div>


        
        <%--        
        <!--Custom models-->
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i><%=ViewResourceText("Title_CustomModels", "Custom models")%>
                <div class="panel-tools">
                    <a href="#" class="btn btn-xs btn-link panel-collapse collapses"></a>
                </div>
            </div>
            <div class="panel-body buttons-widget">
               <div id="div_CustomModels" class="form-horizontal">
                    <div class="form-group">
                        <div class="col-sm-5"><%=ViewResourceText("Title_Name", "Name")%></div>
                        <div class="col-sm-5"><%=ViewResourceText("Title_Value", "Value")%></div>
                    </div>
      
               
               </div>
            </div>
        </div>
        --%>
        
        <!--Tags-->
        <div class="panel panel-default" id="div_group_tags" runat="server">
            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i><%=ViewResourceText("Title_Tags", "Tags")%>
                <div class="panel-tools">
                    <a href="#" class="btn btn-xs btn-link panel-collapse collapses"></a>
                </div>
            </div>
            <div class="panel-body buttons-widget">
                <div  id="TagDiv">
                    <asp:TextBox ID="txtTags" runat="server"></asp:TextBox>
                </div>
                <p>
                    <i><%=ViewResourceText("Title_Tags_Help", "Separate tags with commas")%></i>
                </p>
            </div>
        </div>
        <!--Featured Image-->
        <div class="panel panel-default" id="div_group_featuredimage" runat="server">
            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i><%=ViewResourceText("Title_FeaturedImage", "Featured Image")%>
                <div class="panel-tools">
                    <a href="#" class="btn btn-xs btn-link panel-collapse collapses"></a>
                </div>
            </div>
            <div class="panel-body buttons-widget">
                <asp:HiddenField runat="server" ID="hfPicture" />
                <asp:HyperLink runat="server" ID="hlPicture" resourcekey="hlPicture"
                    data-toggle="modal" NavigateUrl="#Picture_Modal" Text="<i class='fa clip-pictures'></i> Set featured image"
                    ToolTip="Set featured image" Target="_blank" CssClass="btn btn-light-grey"></asp:HyperLink>
                <asp:HyperLink runat="server" ID="hlRemovePicture" onclick="RemovePicture();" Text="<i class='fa fa-trash-o'></i> Delete"
                    NavigateUrl="javascript:;" resourcekey="hlRemovePicture" CssClass="btn btn-xs btn-bricky"></asp:HyperLink>
                <div class="add_summary form_field">
                    <asp:Image runat="server" ID="imgPicture" />
                </div>
                <div id="Picture_Modal" class="modal fade" tabindex="-1" data-width="820"
                    data-height="400" style="display: none;">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                            &times;
                        </button>
                        <h4 class="modal-title">
                            <i class='fa fa-folder-open'></i>Set featured image</h4>
                    </div>
                    <div class="modal-body">
                        <iframe id="Picture_Iframe" width="100%" height="100%" style="border-width: 0px;">
                        </iframe>
                    </div>
                </div>
                <%--<a href="#">Set featured image </a> --%>
            </div>
        </div>
        <!--Gallery Image-->
        <div class="panel panel-default" id="div_group_galleryimage" runat="server">
            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i> <%=ViewResourceText("Title_GalleryImage", "Gallery Image")%>
                <div class="panel-tools">
                    <a href="#" class="btn btn-xs btn-link panel-collapse collapses"></a>
                </div>
            </div>
            <div class="panel-body buttons-widget">
                <asp:HyperLink runat="server" ID="hlAddAlbums" resourcekey="hlAddAlbums"
                    data-toggle="modal" NavigateUrl="#AddAlbums_Modal" Text="<i class='fa clip-images'></i> Set Gallery image"
                    ToolTip="Set Gallery image" CssClass="btn btn-light-grey"></asp:HyperLink>
                <asp:HiddenField runat="server" ID="hfAddAlbums" />
                 <div class="form-group">
                    <table id="ul_albums" class="ul_albums table table-striped table-bordered table-hover" id="sample-table-2">
                          <tbody>
                            <asp:Literal ID="liAddAlbums" runat="server"></asp:Literal>
                         </tbody>
                    </table>
               </div>
                <div id="AddAlbums_Modal" class="modal fade" tabindex="-1" data-width="820"
                    data-height="400" style="display: none;">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                            &times;
                        </button>
                        <h4 class="modal-title">
                            <i class='fa fa-folder-open'></i>Set gallery image</h4>
                    </div>
                    <div class="modal-body">
                        <iframe id="AddAlbums_Iframe" width="100%" height="100%" style="border-width: 0px;">
                        </iframe>
                    </div>
                </div>
            </div>
        </div>
        <!--Attachments list-->
        <div class="panel panel-default" id="div_group_attachments" runat="server">
            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i> <%=ViewResourceText("Title_AttachmentsList", "Attachment List")%>
                <div class="panel-tools">
                    <a href="#" class="btn btn-xs btn-link panel-collapse collapses"></a>
                </div>
            </div>
            <div class="panel-body buttons-widget">
                 <asp:HyperLink runat="server" ID="hlAddAttachments" resourcekey="hlAddAttachments"
                    data-toggle="modal" NavigateUrl="#AddAttachments_Modal" Text="<i class='fa clip-attachment'></i> Set Attachment list"
                    ToolTip="Add Attachments" CssClass="btn btn-light-grey"></asp:HyperLink>
                <asp:HiddenField runat="server" ID="hfAddAttachments" />
                <div class="form-group">
                    <table id="ul_Attachments" class="ul_Attachments table table-striped table-bordered table-hover" id="sample-table-2">
                          <tbody>
                            <asp:Literal ID="liAddAttachments" runat="server"></asp:Literal>
                         </tbody>
                    </table>
               </div>

            </div>
            <div id="AddAttachments_Modal" class="modal fade" tabindex="-1" data-width="820"
                    data-height="400" style="display: none;">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                            &times;
                        </button>
                        <h4 class="modal-title">
                            <i class='fa fa-folder-open'></i>Set Attachments</h4>
                    </div>
                    <div class="modal-body">
                        <iframe id="AddAttachments_Iframe" width="100%" height="100%" style="border-width: 0px;">
                        </iframe>
                    </div>
             </div>
        </div>
         <!--Permissions-->
        <div class="panel panel-default"  id="div_group_permissions" runat="server">
            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i>
                <%=ViewResourceText("Title_Permissions", "Permissions")%>
                <div class="panel-tools">
                    <a href="#" class="btn btn-xs btn-link panel-collapse collapses"></a>
                </div>
            </div>
            <div class="panel-body buttons-widget form-horizontal">
                  <div class="form-group">
                       <%=ViewControlTitle("lblPermissionsAllUsers", "All Users", "cbPermissionsAllUsers", ":", "col-sm-3 control-label")%>
                        <div class="col-sm-9">
                             <div class="checkbox-inline">
                                <asp:CheckBox ID="cbPermissionsAllUsers" runat="server" CssClass="auto"/>
                            </div>
                        </div>
                  </div>

                  <div class="form-group">
                       <%=ViewControlTitle("lblPermissionsRoles", "Permission Roles", "cblPermissionsRoles", ":", "col-sm-3 control-label")%>
                        <div class="col-sm-9">
                            <div class="checkbox-inline">
                                <asp:CheckBoxList ID="cblPermissionsRoles" runat="server" CssClass="auto"></asp:CheckBoxList>
                            </div>
                        </div>
                  </div>
            </div>
        </div>

        <!--Event Author-->
        <div class="panel panel-default" id="divEventAuthor" runat="server">
            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i><%=ViewResourceText("Title_EventAuthor", "Event Author")%>
                <div class="panel-tools">
                    <a href="#" class="btn btn-xs btn-link panel-collapse collapses"></a>
                </div>
            </div>
            <div class="panel-body buttons-widget">
                <asp:HiddenField runat="server" ID="hfEventAuthor" />
               <div class="col-md-12 lead">
                    <asp:Image ID="imgEventAuthor" runat="server" Width="80" />
                    <asp:Label ID="lbEventAuthor" runat="server"></asp:Label>
               </div>
               <div class="col-md-7">
                <asp:HyperLink runat="server" ID="hlEventAuthor" resourcekey="hlEventAuthor"
                    data-toggle="modal" NavigateUrl="#EventAuthor_Modal" Text="<i class='fa clip-user-plus'></i> Set Event Author"
                    ToolTip="Set Event Author" Target="_blank" CssClass="btn btn-light-grey"></asp:HyperLink></div>
                <div id="EventAuthor_Modal" class="modal fade" tabindex="-1" data-width="820"
                    data-height="400" style="display: none;">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                            &times;
                        </button>
                        <h4 class="modal-title">
                            <i class='fa clip-user-plus'></i> Set Event Author</h4>
                    </div>
                    <div class="modal-body">
                        <iframe id="EventAuthor_Iframe" width="100%" height="100%" style="border-width: 0px;">
                        </iframe>
                    </div>
                </div>
                <%--<a href="#">Set featured image </a> --%>
            </div>
        </div>
        <!-- end: SELECT BOX PANEL -->
    </div>
    <!-- end: PAGE CONTENT-->
</div>

<%--
<!-- start: Custom models-->
<script id="scriptCustomModels" type="text/x-jquery-tmpl">
    <div class="form-group" data-name="${Name}">
        <div class="col-sm-5"><input type="text" class="form-control" name="Model$Name$<%=ModuleId %>" value="${Name}"/> </div>
        <div class="col-sm-5"><input type="text" class="form-control" name="Model$Value$<%=ModuleId %>" value="${Value}"/></div>
        <div class="col-sm-2">
            {{if Name == "" }}
                <a class="btn btn-xs btn-bricky _plus"><i class="fa fa-plus"></i></a>
                <a class="btn btn-xs btn-bricky _close" style="display:none"><i class="fa fa-close"></i></a>
             {{else}}
                <a class="btn btn-xs btn-bricky _close" data-name="${Name}"><i class="fa fa-close"></i></a>
             {{/if}}
        </div>
    </div>
</script> 
<script type="text/javascript">
    jQuery(function ($) {
        var strJSON = "{Name:'',Value:''}";
        function bindrows(item) {
            $("div#div_CustomModels").append($("#scriptCustomModels").tmpl(item)).find("a._close").one("click", function () { $(this).parent().parent().remove(); });
            $("div#div_CustomModels .form-group:last-child").find("a._plus").one("click", function () {
                $(this).hide().parent().find("a._close").show();
                bindrows(eval("(" + strJSON + ")"));
            });
        }
        $.getJSON("<%=ModulePath %>Resource_Service.aspx?Token=CustomModels&EventID=<%=EventID %>", function (data) {
            jQuery.each(data, function (i, item) {
                bindrows(item);
            });
            //bindrows(eval("(" + strJSON + ")"));
        });
    });
</script>
<!-- end: Custom models-->
--%>


<script type="text/javascript">


    /*移除图片*/
    function RemovePicture() {
        jQuery('#<%=hfPicture.ClientID %>').val('0');
        jQuery('#<%=imgPicture.ClientID %>').hide();
        jQuery('#<%=imgPicture.ClientID %>').attr("src", "")
        jQuery('#<%=hlRemovePicture.ClientID %>').hide();
        jQuery('#<%=hlPicture.ClientID %>').show();
    }
    /*显示图片*/
    function ShowPicture(PictureUrl, PictureID) {
        jQuery('#<%=hfPicture.ClientID %>').val(PictureID);
        jQuery('#<%=imgPicture.ClientID %>').attr("src", PictureUrl)
        jQuery('#<%=imgPicture.ClientID %>').show();
        jQuery('#<%=hlRemovePicture.ClientID %>').show();
        jQuery('#<%=hlPicture.ClientID %>').hide();

    }


    function ReturnUser(UserName, UserID,UserPic) {
        jQuery('#<%=lbEventAuthor.ClientID %>').text(UserName);
        jQuery('#<%=hfEventAuthor.ClientID %>').val(UserID);
        jQuery('#<%=imgEventAuthor.ClientID %>').attr("src", UserPic);
    } 


    function ShowAlbums(PictureUrls, PictureIDs, PictureNames) {
        var IDs = jQuery('#<%=hfAddAlbums.ClientID %>').val();
        for (var i = 0; i < PictureIDs.length; i++) {
            var id = PictureIDs[i];
            var Urls = PictureUrls[i];
            var Names = PictureNames[i];

            if (IDs !== "" && IDs.indexOf(id + ",") >= 0) {
                continue;
            }
            $("#ul_albums").append("<tr data-value='" + id + "'><td>" + Names + "<br/><img src='" + Urls + "' style='max-width:120px; margin-right:15px;'/></td><td class='center'><a data-value='" + id + "' class='btn btn-xs btn-bricky tooltips' href='javascript:;' data-placement='top' data-original-title='Delete'><i class='fa fa-times fa fa-white'></i></a></td></tr>");
            //$("#ul_albums").append("<li data-value='" + id + "'><img  src='" + Urls + "'/> <a data-value='" + id + "' href='javascript:;' class='btn btn-xs btn-bricky'><i class='fa fa-trash-o'></i> Remove</a> </li>");
            IDs += (id + ",");
        }
        jQuery('#<%=hfAddAlbums.ClientID %>').val(IDs);

        $("#ul_albums tr a").click(function () {
            var id = $(this).attr("data-value");
            $("#ul_albums tr[data-value='" + id + "']").hide("fast", function () {
                $(this).remove();
            });
            jQuery('#<%=hfAddAlbums.ClientID %>').val(jQuery('#<%=hfAddAlbums.ClientID %>').val().replace(id + ",", ""));
        });
    }

    function ShowAttachments(PictureUrls, PictureIDs, PictureNames) {
        var IDs = jQuery('#<%=hfAddAttachments.ClientID %>').val();
        for (var i = 0; i < PictureIDs.length; i++) {
            var id = PictureIDs[i];
            var Urls = PictureUrls[i];
            var Names = PictureNames[i];

            if (IDs !== "" && IDs.indexOf(id + ",") >= 0) {
                continue;
            }

            $("#ul_Attachments").append("<tr data-value='" + id + "'><td>" + Names + "<br/><img src='" + Urls + "' style='max-width:120px; margin-right:15px;'/></td><td class='center'><a data-value='" + id + "' class='btn btn-xs btn-bricky tooltips' href='javascript:;' data-placement='top' data-original-title='Delete'><i class='fa fa-times fa fa-white'></i></a></td></tr>");
            IDs += (id + ",");
        }
        jQuery('#<%=hfAddAttachments.ClientID %>').val(IDs);

        $("#ul_Attachments tr a").click(function () {
            var id = $(this).attr("data-value");
            $("#ul_Attachments tr[data-value='" + id + "']").hide("fast", function () {
                $(this).remove();
            });
            jQuery('#<%=hfAddAttachments.ClientID %>').val(jQuery('#<%=hfAddAttachments.ClientID %>').val().replace(id + ",", ""));
        });
    }





    jQuery(function ($) {
        $("#<%=hlPicture.ClientID %>").click(function () { $("#Picture_Iframe").attr("src", $(this).attr("data-href")); });
        $("#<%=hlAddMedia.ClientID %>").click(function () { $("#Picture_Iframe").attr("src", $(this).attr("data-href")); });
        $("#<%=hlAddAlbums.ClientID %>").click(function () { $("#AddAlbums_Iframe").attr("src", $(this).attr("data-href")); });
        $("#<%=hlAddAttachments.ClientID %>").click(function () { $("#AddAttachments_Iframe").attr("src", $(this).attr("data-href")); });
        $("#<%=hlEventAuthor.ClientID %>").click(function () { $("#EventAuthor_Iframe").attr("src", $(this).attr("data-href")); });

        $("#ul_albums tr a").click(function () {
            var id = $(this).attr("data-value");
            $("#ul_albums tr[data-value='" + id + "']").hide("fast", function () {
                $(this).remove();
            });
            jQuery('#<%=hfAddAlbums.ClientID %>').val(jQuery('#<%=hfAddAlbums.ClientID %>').val().replace(id + ",", ""));
        });


        $("#ul_Attachments tr a").click(function () {
            var id = $(this).attr("data-value");
            $("#ul_Attachments tr[data-value='" + id + "']").hide("fast", function () {
                $(this).remove();
            });
            jQuery('#<%=hfAddAttachments.ClientID %>').val(jQuery('#<%=hfAddAttachments.ClientID %>').val().replace(id + ",", ""));
        });

        $("div#divColorPicker a").click(function () {
            var bg_color = $(this).getHexBackgroundColor()
            $("#<%=txtColorPicker.ClientID %>").val(bg_color);
            $("#ColorPicker_label").css("background-color", bg_color);
        });

        $('#<%=txtTags.ClientID %>').tagsInput({ width: 'auto', height: '100px' });

        CKEDITOR.replace('<%=txtContentText.ClientID %>', { height: '400px' });


        $('#<%=ddlRepeat.ClientID %>').change(function () {
            var repeattype = $(this).val();
            if (repeattype == 0) {
                $("#<%=divEvery.ClientID %>,#<%=divMoreOptions.ClientID %>").hide();
            }
            else if (repeattype == 90) {
                $("#<%=divEvery.ClientID %>,#<%=divMoreOptions.ClientID %>").show();
            } else {
                $("#<%=divMoreOptions.ClientID %>").hide();
                $("#<%=divEvery.ClientID %>").show();
            }
        });

        $('#<%=ddlRepeatEvery.ClientID %>').change(function () {
            var repeatevery = $(this).val();
            if (repeatevery == 0) {
                $("#<%=divFollowDaysOfMonth.ClientID %>,#<%=divFollowWeekOfYear.ClientID %>,#<%=divRepeatEveryWeeks.ClientID %>").show();
                 $("#<%=divFollowWeekOfMonths.ClientID %>").hide();
            }
            else if (repeatevery == 1) {
                $("#<%=divFollowDaysOfMonth.ClientID %>,#<%=divRepeatEveryWeeks.ClientID %>,#<%=divFollowWeekOfMonths.ClientID %>").show();
                $("#<%=divFollowWeekOfYear.ClientID %>").hide();

            } else if (repeatevery == 2) {
                $("#<%=divFollowWeekOfYear.ClientID %>,#<%=divFollowWeekOfMonths.ClientID %>,#<%=divFollowDaysOfMonth.ClientID %>").hide();
                $("#<%=divRepeatEveryWeeks.ClientID %>,#<%=divFollowMonths.ClientID %>").show();
            }
        });
        

        $('#<%=ddlEndRepeat.ClientID %>').change(function () {
            var endrepeat = $(this).val();
            if (endrepeat == 1) {
                $("#<%=divEndRepeatCount.ClientID %>").hide();
                $("#<%=divEndRepeatDate.ClientID %>").show();
            }
            else if (endrepeat == 2) {
                $("#<%=divEndRepeatDate.ClientID %>").hide();
                $("#<%=divEndRepeatCount.ClientID %>").show();
            } else {
                $("#<%=divEndRepeatDate.ClientID %>,#<%=divEndRepeatCount.ClientID %>").hide();
            }
        });

        $('#<%=cbAllDayEvent.ClientID %>').change(function () { $('#divStartTime,#divDisableTime').toggle($(this).attr("checked") != "checked"); });
        $('#divStartTime,#divDisableTime').toggle($('#<%=cbAllDayEvent.ClientID %>').attr("checked") != "checked");

        $('.follow_checkboxs input').labelauty();
 
    });
</script>
