﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Manager_Event_Repeats.ascx.cs" Inherits="DNNGo.Modules.xEvent.Manager_Event_Repeats" %>
<div class="container">
  <div class="row">
        <div class="col-sm-8">
        </div>
        <div class="col-sm-3 text_right">
        	<div class="control-inline"><asp:Label ID="lblRecordCount" runat="server"></asp:Label></div>
        </div>
      </div>
     <div class="row"> 
     	<div class="col-sm-12">
		<!-- start-->
			<div class="form-group">
             <asp:GridView ID="gvEventList" runat="server" AutoGenerateColumns="False" OnRowDataBound="gvEventList_RowDataBound" OnRowCreated="gvEventList_RowCreated" OnSorting="gvEventList_Sorting" AllowSorting="true"
                        Width="98%" CellPadding="0" cellspacing="0" border="0" CssClass="table table-striped table-bordered table-hover"  GridLines="none" >
                        <Columns>
                            <asp:BoundField DataField="StartDateTime" HeaderText="Start DateTime" />
                            <asp:BoundField DataField="EndDateTime" HeaderText="End DateTime"  />
                            <asp:BoundField DataField="LastUser" HeaderText="Created User"   HeaderStyle-Width="100"/> 
                            <asp:BoundField DataField="LastTime" HeaderText="Created Date"  HeaderStyle-Width="100"/> 
                            <asp:TemplateField HeaderText="Registers" HeaderStyle-Width="50">
                                <ItemTemplate>
                                    <asp:HyperLink ID="hlRegisters" runat="server" CssClass="btn btn-xs btn-default tooltips" data-original-title="View registers" data-placement="top" Text="<i class='fa clip-puzzle-3'></i>" ></asp:HyperLink>
                                 </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <PagerSettings Visible="False" />
                    </asp:GridView>
                    <ul id="paginator-EventList" class="pagination-purple"></ul>

      <!-- end--> 
     </div>
     </div> 
 
         </div>
    </div>
 
           
            <script type="text/javascript">
                $(document).ready(function () {
                     
                    $('#paginator-EventList').bootstrapPaginator({
                        bootstrapMajorVersion: 3,
                        currentPage: <%=PageIndex %>,
                        totalPages: <%=RecordPages %>,
                        numberOfPages:7,
                        useBootstrapTooltip:true,
                        onPageClicked: function (e, originalEvent, type, page) {
                            window.location.href='<%=CurrentUrl %>&PageIndex='+ page;
                        }
                    });
                });

             
                function ReturnUser(UserName,UserID,UserPic) {
                    window.parent.ReturnUser(UserName, UserID,UserPic);
                    window.parent.$('#EventAuthor_Modal').modal('hide');
                }

                

 
            </script>
       