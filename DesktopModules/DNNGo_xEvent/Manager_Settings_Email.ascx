﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Manager_Settings_Email.ascx.cs" Inherits="DNNGo.Modules.xEvent.Manager_Settings_Email" %>

 
      <!-- start: PAGE HEADER -->
      <div class="row">
        <div class="col-sm-12"> 
          <!-- start: PAGE TITLE & BREADCRUMB -->
          
          <div class="page-header">
            <h1><i class="fa fa-envelope"></i> <%=ViewResourceText("Header_Title", "Email Settings")%></h1>
              <%--language switch: <asp:Literal ID="liLanguageLinks" runat="server"></asp:Literal>--%>
          </div>
          <!-- end: PAGE TITLE & BREADCRUMB --> 
        </div>
      </div>
      <!-- end: PAGE HEADER -->
            <!-- start: PAGE CONTENT -->
      
      <div class="row">
        <div class="col-sm-12">
    
            
          <div class="panel panel-default">
            <div class="panel-heading"> <i class="fa fa-external-link-square"></i> <%=ViewResourceText("Title_BaseSettings", "Basic Settings")%>
              <div class="panel-tools"> <a href="#" class="btn btn-xs btn-link panel-collapse collapses"> </a> </div>
            </div>
            <div class="panel-body">
                <div class="form-horizontal">

                  <div class="form-group">
                    <%=ViewControlTitle("lblTemplateName", "Template Name", "lbShowTemplateName", ":", "col-sm-3 control-label")%>
                    <div class="col-sm-5">
                       <label class="control-label"><asp:Literal ID="lbShowTemplateName" runat="server"></asp:Literal> / <asp:Literal ID="lbShowTemplateAlias" runat="server"></asp:Literal></label>
                    </div>
                  </div>
                      <div class="form-group">
                    <%=ViewControlTitle("lblLanguage", "Language", "labShowLanguage", ":", "col-sm-3 control-label")%>
                    <div class="col-sm-5">
                       
                        <label class="control-label"><asp:Literal ID="lbShowLanguage" runat="server"></asp:Literal></label>
                    </div>
                  </div>

                  <div class="form-group">
                    <%=ViewControlTitle("lblEnable", "Enable", "cbEnable", ":", "col-sm-3 control-label")%>
                    <div class="col-sm-5">
                        <div class="checkbox-inline"><asp:CheckBox ID="cbEnable" runat="server"  CssClass="auto"/></div>
                    </div>
                  </div>

                  <div class="form-group" id="divMailTime" runat="server" visible="false">
                    <%=ViewControlTitle("lblMailTime", "Time Before Starting", "txtMailTime", ":", "col-sm-3 control-label")%>
                    <div class="col-sm-8">
                      <asp:TextBox runat="server" ID="txtMailTime" Width="300px" CssClass="form-control  validate[required,maxSize[200]]"></asp:TextBox>
                         <%=ViewHelp("lblMailTime", "Hours")%>
                         <%=ViewHelp("lblMailTime2", "Users will receive the reminder email at a set time.")%>
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <%=ViewControlTitle("lblMailTo", "Mail To", "txtMailTo", ":", "col-sm-3 control-label")%>
                    <div class="col-sm-8">
                      <asp:TextBox runat="server" ID="txtMailTo" Width="300px" CssClass="form-control  validate[required,maxSize[200]]"></asp:TextBox>
                    </div>
                  </div>
                   <div class="form-group">
                    <%=ViewControlTitle("lblMailCC", "Mail CC", "txtMailCC", ":", "col-sm-3 control-label")%>
                    <div class="col-sm-8">
                      <asp:TextBox runat="server" ID="txtMailCC" Width="300px" CssClass="form-control  validate[maxSize[200]]"></asp:TextBox>
                    </div>
                  </div>


                </div>
            </div>
          </div>


           <div class="panel panel-default">
            <div class="panel-heading"> <i class="fa fa-external-link-square"></i> <%=ViewResourceText("Title_TemplateSettings", "Email Content Template")%>
              <div class="panel-tools"> <a href="#" class="btn btn-xs btn-link panel-collapse collapses"> </a> </div>
            </div>
            <div class="panel-body">
                <div class="form-horizontal">
      

                  <div class="form-group">
                    <%=ViewControlTitle("lblMailSubject", "Mail Subject", "txtMailSubject", ":", "col-sm-3 control-label")%>
                    <div class="col-sm-8">
                      <asp:TextBox runat="server" ID="txtMailSubject" Width="500px" CssClass="form-control  validate[required,maxSize[200]]"></asp:TextBox>
                    </div>
                  </div>

                  <div class="form-group">
                    <%=ViewControlTitle("lblMailBody", "Mail Body", "txtMailBody", ":", "col-sm-3 control-label")%>
                    <div class="col-sm-8">
                      <asp:TextBox  id="txtMailBody" CssClass="form-control" runat="server" height="300" width="100%" TextMode="MultiLine"></asp:TextBox>
                    </div>
                  </div>
  

                  <div class="form-group">
                    <%=ViewControlTitle("lblTemplateTitle", "Template Token","", ":", "col-sm-3 control-label")%>
                    <div class="col-sm-8">
                        <%=ViewResourceText("Template_USER_NAME", "[USER_NAME]: User name of the one who registered the activity, can be only obtained after logging in.")%><br/>
                        <%=ViewResourceText("Template_USER_DISPLAYNAME", "[USER_DISPLAYNAME]: Display name of the one who registered the activity, can be only obtained after logging in.")%><br/>
                        <%=ViewResourceText("Template_REGISTER_MAIL", "[REGISTER_MAIL]: Email address filled in when registering the activity.")%><br/>
                        <%=ViewResourceText("Template_REGISTER_NAME", "[REGISTER_NAME]: Name filled in when registering the activity.")%><br/>
                        <%=ViewResourceText("Template_REGISTER_PHONE", "[REGISTER_PHONE]: Phone number filled in when registering the activity.")%><br/>
                        <%=ViewResourceText("Template_REGISTER_COMMENTS", "[REGISTER_COMMENTS]: Comments filled in when registering the activity.")%><br/>
                        <%=ViewResourceText("Template_REGISTER_TIME", "[REGISTER_TIME]: Time filled in when registering the activity.")%><br/>
             

                        <%=ViewResourceText("Template_AUTHOR_MAIL", "[AUTHOR_MAIL]: Email address of the author created in event activity.")%><br/>
                        <%=ViewResourceText("Template_AUTHOR_USERNAME", "[AUTHOR_USERNAME]: Username of the author created in event activity.")%><br/>
                        <%=ViewResourceText("Template_AUTHOR_DISPLAYNAME", "[AUTHOR_DISPLAYNAME]: Display name of the author created in the event activity.")%><br/>

                        <%=ViewResourceText("Template_TITLE", "[TITLE]: Title of the event.")%><br/>
                        <%=ViewResourceText("Template_SUMMARY", "[SUMMARY]: Summary of the event.")%><br/>
                        <%=ViewResourceText("Template_CONTENTTEXT", "[CONTENTTEXT]: Details of the event.")%><br/>
                        <%=ViewResourceText("Template_TAGS", "[TAGS]: Tags of the event.")%><br/>
                        <%=ViewResourceText("Template_COLOR", "[COLOR]: Color of the event title.")%><br/>
                        <%=ViewResourceText("Template_VIEWCOUNT", "[VIEWCOUNT]: View counts of the event.")%><br/>
                        <%=ViewResourceText("Template_CREATETIME", "[CREATETIME]: Created time of the event.")%><br/>
                        <%=ViewResourceText("Template_CREATEDATE", "[CREATEDATE]: Created date of the event.")%><br/>
                        <%=ViewResourceText("Template_PICTURE", "[PICTURE]: Image URL of the event.")%><br/>
                        <%=ViewResourceText("Template_EVENTURL", "[EVENTURL]: Page URL to view the event details.")%><br/>
                        <%=ViewResourceText("Template_EDITEVENTURL", "[EDITEVENTURL]: Page URL to edit the event details.")%><br/>
                        <%=ViewResourceText("Template_CATEGORYS", "[CATEGORIES]: Categories of the event, separated by a comma.")%><br/>


                        <%=ViewResourceText("Template_STARTTIME", "[STARTTIME]: Start time of the event&activity.")%><br/>
                        <%=ViewResourceText("Template_STARTDATE", "[STARTDATE]: Start date of the event&activity.")%><br/>
                        <%=ViewResourceText("Template_ENDTIME", "[ENDTIME]: End time of the event&activity.")%><br/>
                        <%=ViewResourceText("Template_ENDDATE", "[ENDDATE]: End date of the event&activity.")%><br/>
                        <%=ViewResourceText("Template_MAILSETTING_TIME", "[MAILSETTING_TIME]: Set how many hours ahead of the starting event&activity time to send mails.")%><br/>
                        <%=ViewResourceText("Template_DATETIME_NOW", "[DATETIME_NOW]: Template will obtain the current system time.")%><br/>
                        <%=ViewResourceText("Template_TIME_ZONE", "[TIME_ZONE]: Template will obtain the current time zone.")%><br/>
                 
                 
                        <%=ViewResourceText("Template_ADDRESS", "[ADDRESS]: Complete address of the event&activity.")%><br/>
                      
                       









 














                        
                     <%--   <%=ViewResourceText("Template_", "[]: ")%><br/>--%>
               
                    </div>
                  </div>
                </div>
            </div>
          </div>



        </div>
      </div>
      <!-- end: PAGE CONTENT-->
      
      <div class="row">
        <div class="col-sm-2"> </div>
        <div class="col-sm-10">
          <asp:Button CssClass="btn btn-primary" lang="Submit" ID="cmdUpdate" resourcekey="cmdUpdate"
        runat="server" Text="Update" OnClick="cmdUpdate_Click"></asp:Button>&nbsp;
        <asp:Button CssClass="btn btn-default" ID="cmdCancel" resourcekey="cmdCancel" runat="server"
            Text="Cancel" CausesValidation="False" OnClick="cmdCancel_Click"  OnClientClick="CancelValidation();"></asp:Button>&nbsp;
        
         </div>
      </div>


<script type="text/javascript">

    jQuery(function ($) {
        CKEDITOR.replace('<%=txtMailBody.ClientID %>', { height: '400px' });

   
    });
</script>