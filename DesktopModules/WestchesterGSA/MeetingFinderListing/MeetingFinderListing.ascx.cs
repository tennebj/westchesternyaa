﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Net;
using System.Data;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Collections;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Services.FileSystem;
using DotNetNuke.Security.Roles;
using DotNetNuke.Abstractions;

public partial class DesktopModules_WestchesterGSA_MeetingFinderListing : DotNetNuke.Entities.Modules.PortalModuleBase, DotNetNuke.Entities.Modules.IActionable
{
    private bool _isLoggedIn;
    private string _adminPageName;
    private string _currentCategory = "";
    private List<string> _alreadyAddedCategories = new List<string>();
    private int _currentIndex = 0;
    private List<string> _finalCategories = new List<string>();
  
    const string SETTING_AdminPage = "MeetingFinder.AdminPage";
    const string SETTING_PlaceholderText = "MeetingFinder.PlaceholderText";



    protected void Page_Load(object sender, EventArgs e)
    {
        //lblPlaceholderText.Text = ModuleContext.Settings.GetValueOrDefault(SETTING_PlaceholderText, @"");
        //_isLoggedIn = (HttpContext.Current.User.Identity.IsAuthenticated && SecurityManager.UserName.Length > 0); ;
        //_adminPageName = ModuleContext.Settings.GetValueOrDefault(SETTING_AdminPage, @"Documents Admin");
        //bool isAdmin = UserInfo.IsSuperUser || UserInfo.IsInRole("Admin");
        //List<string> classes = new List<string>();
        //if (!isAdmin) classes = UserInfo.Roles.ToList();
        //else
        //{
        //    var roles = (new RoleController()).GetRoles();
        //    foreach (RoleInfo ri in roles)
        //    {
        //        classes.Add(ri.RoleName);
        //    }
        //}
        //for(int i = 0; i < classes.Count; i++)
        //{
        //    classes[i] = classes[i].Replace("_", ".");
        //}
        //var roleGroups = RoleController.GetRoleGroups(this.PortalId);
        //foreach(RoleGroupInfo rgi in roleGroups)
        //{
        //    classes.Add(rgi.RoleGroupName);
        //}

        gvMeetingListing.Font.Size = FontUnit.Small;

        if (!this.IsPostBack)
            LoadData();
    }

    private void LoadData()
    {
        // Get day of week
        string today = DateTime.Now.ToString("dddd");
     ddlbDay.SelectedValue = today;
    SqlDataSource_MeetingList.SelectParameters["DayOfWeek"].DefaultValue = today;
        SqlDataSource_MeetingList.SelectParameters["City"].DefaultValue = ddlbLocation.SelectedItem.Text;
        SqlDataSource_MeetingList.SelectParameters["Time"].DefaultValue = ddlbTime.SelectedItem.Text;
        SqlDataSource_MeetingList.SelectParameters["Type"].DefaultValue = ddlbType.SelectedItem.Value;

        gvMeetingListing.DataBind();
    }



    public ModuleActionCollection ModuleActions
    {
        get
        {
            return new ModuleActionCollection();
        }
    }

    protected string FormatMeetingUrl(string meetingID)
    {
        return DotNetNuke.Common.Globals.NavigateURL(PortalSettings.ActiveTab.TabID, "Meeting-Details", string.Format("mid={0}", meetingID));
    }


    protected void ddlbDay_SelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList ddlbDay = (DropDownList)sender;
        SqlDataSource_MeetingList.SelectParameters["DayOfWeek"].DefaultValue = ddlbDay.Text;
        SqlDataSource_MeetingList.SelectParameters["Time"].DefaultValue = ddlbTime.SelectedItem.Text;
        SqlDataSource_MeetingList.SelectParameters["City"].DefaultValue = ddlbLocation.SelectedItem.Text;
        SqlDataSource_MeetingList.SelectParameters["Type"].DefaultValue = ddlbType.SelectedItem.Value;
        gvMeetingListing.DataBind();

        if (ddlbDay.Text.Equals("Any Day"))
        {
            ListItem removeItem = ddlbTime.Items.FindByValue("Upcoming");
            ddlbTime.Items.Remove(removeItem);
        }

        if (!ddlbDay.Text.Equals("Any Day"))
        {
            ListItem removeItem = ddlbTime.Items.FindByValue("Upcoming");
            if (removeItem == null)
            {
                ddlbTime.Items.Insert(1, new ListItem("Upcoming", "Upcoming"));
            }

        }


    }


    protected void ddlbLocation_SelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList ddlbLocation = (DropDownList)sender;
        SqlDataSource_MeetingList.SelectParameters["City"].DefaultValue = ddlbLocation.Text;
        SqlDataSource_MeetingList.SelectParameters["Time"].DefaultValue = ddlbTime.SelectedItem.Text;
        SqlDataSource_MeetingList.SelectParameters["DayOfWeek"].DefaultValue = ddlbDay.SelectedItem.Text;
        SqlDataSource_MeetingList.SelectParameters["Type"].DefaultValue = ddlbType.SelectedItem.Value;
    }

    protected void ddlbTime_SelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList ddlbTime = (DropDownList)sender;
        SqlDataSource_MeetingList.SelectParameters["Time"].DefaultValue = ddlbTime.Text;
        SqlDataSource_MeetingList.SelectParameters["City"].DefaultValue = ddlbLocation.SelectedItem.Text;
        SqlDataSource_MeetingList.SelectParameters["DayOfWeek"].DefaultValue = ddlbDay.SelectedItem.Text;
        SqlDataSource_MeetingList.SelectParameters["Type"].DefaultValue = ddlbType.SelectedItem.Value;
        gvMeetingListing.DataBind();
    }

    protected void ddlbType_SelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList ddlbType = (DropDownList)sender;
        SqlDataSource_MeetingList.SelectParameters["Type"].DefaultValue = ddlbType.SelectedItem.Value;
        SqlDataSource_MeetingList.SelectParameters["Time"].DefaultValue = ddlbTime.SelectedItem.Text;
        SqlDataSource_MeetingList.SelectParameters["City"].DefaultValue = ddlbLocation.SelectedItem.Text;
        SqlDataSource_MeetingList.SelectParameters["DayOfWeek"].DefaultValue = ddlbDay.SelectedItem.Text;
        gvMeetingListing.DataBind();
    }


}