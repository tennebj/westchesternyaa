﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MeetingFinderListing.ascx.cs" Inherits="DesktopModules_WestchesterGSA_MeetingFinderListing" %>

<%@ Register TagPrefix="dnn" Namespace="DotNetNuke.Web.Client.ClientResourceManagement" Assembly="DotNetNuke.Web.Client" %>

<dnn:DnnCssInclude ID="BootstrapCSS" runat="server" FilePath="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" />

<dnn:DnnJsInclude ID="jQuery" runat="server" FilePath="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js" />

<dnn:DnnJsInclude ID="BootstrapJS" runat="server" FilePath="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" />

<style>
    .hiddencol{
        display: none;
    }
    .lbl-padding{
        padding-top: 5px;
        display:block;
        padding-right: 10%;
    }
</style>


<div class="doc-listing">

<div class="doc-listing-gridview">
        <asp:Label CssClass="lbl-padding" ID="lblPlaceholderText" runat="server" Text=""> </asp:Label>

    <div class="row controls hidden-print mb-3">

<%--        <table>
        <tr>
            <td>--%>
   <%-- <div class='input-group'>
         <input type='text' class='form-control' placeholder=""  />
        <div class='input-group-field'>
   <asp:ImageButton ID="LinkButton1" runat="server" Text="Action" class="btn btn-default dropdown-toggle"
data-toggle="dro  pdown" aria-haspopup="true" aria-expanded="false">Action <span class="caret"></span></asp:ImageButton>
        </div>
        </div>--%>


        <%--    </td>

                       <td>       --%>

            <div class="col-sm-6 col-md-2 col-md-pull-2 control-region">
                <div class='input-group-sm'>
           
                    </div>

           </div>

       <div class="col-sm-6 col-md-2 col-md-pull-2 control-region">
                <div class='input-group-sm'>
                    <asp:DropDownList ID="ddlbLocation" runat="server"  CssClass="form-control" OnSelectedIndexChanged="ddlbLocation_SelectedIndexChanged" AutoPostBack="true" DataSourceID="sqlLocation"
                         DataTextField="City" DataValueField ="City" AppendDataBoundItems="true">
                                        <asp:ListItem Text="Everywhere" Value="Everywhere" Selected="True"/>
                                      
                        </asp:DropDownList>
                    </div>

           </div>
          <%--  </td>

            <td>       --%>

        
       <div class="col-sm-6 col-md-2 col-md-pull-2 control-region">
                <div class='input-group-sm'>
                    <asp:DropDownList ID="ddlbDay" runat="server"  CssClass="form-control"  OnSelectedIndexChanged="ddlbDay_SelectedIndexChanged" AutoPostBack="true">
                                <asp:ListItem Text="Any Day" Value="Any Day" Selected="True"/>
                                    <asp:ListItem Text="Sunday" Value="Sunday"></asp:ListItem>
                                     <asp:ListItem Text="Monday" Value="Monday" Selected="True"></asp:ListItem>
                                     <asp:ListItem Text="Tuesday" Value="Tuesday"></asp:ListItem>
                                     <asp:ListItem Text="Wednesday" Value="Wednesday"></asp:ListItem>
                                     <asp:ListItem Text="Thursday" Value="Thursday"></asp:ListItem>
                                     <asp:ListItem Text="Friday" Value="Friday"></asp:ListItem>
                                     <asp:ListItem Text="Saturday" Value="Saturday"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
           </div>
         <%--   </td>

                    <td>      --%> 

        
       <div class="col-sm-6 col-md-2 col-md-pull-2 control-region">
                <div class='input-group-sm'>
                    <asp:DropDownList ID="ddlbTime" runat="server"  CssClass="form-control"  OnSelectedIndexChanged="ddlbTime_SelectedIndexChanged" AutoPostBack="true">
                                <asp:ListItem Text="Any Time" Value="Any Time" Selected="True"/>
                                    <asp:ListItem Text="Upcoming" Value="Upcoming"></asp:ListItem>
                                     <asp:ListItem Text="Morning" Value="Morning" ></asp:ListItem>
                                     <asp:ListItem Text="Midday" Value="Midday"></asp:ListItem>
                                     <asp:ListItem Text="Evening" Value="Evening"></asp:ListItem>
                                     <asp:ListItem Text="Night" Value="Night"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
           </div>
 
                 <div class="col-sm-6 col-md-2 col-md-pull-2 control-region">
                <div class='input-group-sm'>
                    <asp:DropDownList ID="ddlbType" runat="server"  CssClass="form-control"  OnSelectedIndexChanged="ddlbType_SelectedIndexChanged" AutoPostBack="true" DataSourceID="sqlType"
                         DataTextField="MeetingType" DataValueField ="MeetingCode" AppendDataBoundItems="true">
                                        <asp:ListItem Text="Any Type" Value="ANY" Selected="True"/>
                        </asp:DropDownList>
                    </div>      
                 </div>


        </div>

          <div class='input-group-sm col-xs-12'>
<asp:GridView ID="gvMeetingListing" runat="server" AutoGenerateColumns="false" GridLines="None"  DataSourceID="SqlDataSource_MeetingList" CssClass= "table table-striped" Sty>
    <Columns>
           <asp:boundfield DataField="Day" HeaderText="Day" SortExpression="DayofWeek" />
                 <asp:boundfield DataField="Time" HeaderText="Time" SortExpression="Time" />
         <%--    <asp:boundfield DataField="Meeting" HeaderText="Meeting" SortExpression="Meeting"  />--%>

		         <asp:HyperLinkField runat="server" DataTextField = "Meeting"   DataNavigateUrlFields="ID" DataNavigateUrlFormatString="/Meeting-Details/{0}"/>

				<asp:boundfield DataField="Location" HeaderText="Location" SortExpression="Location" />
				<asp:boundfield DataField="Address" HeaderText="Address" SortExpression="Address" />
				<asp:boundfield DataField="Region" HeaderText="Region" SortExpression="Region" />
    </Columns>
</asp:GridView>
              </div>
          <asp:SqlDataSource ID="SqlDataSource_MeetingList" runat="server" ConnectionString="<%$ ConnectionStrings:SiteSqlServer %>"
                    SelectCommand="WGSA_sp_MeetingList" SelectCommandType="StoredProcedure">
               <SelectParameters>
                     <asp:Parameter Name="DayofWeek" Type="string" />
                        <asp:Parameter Name="City" Type="string" />
                      <asp:Parameter Name="Time" Type="string" />
                      <asp:Parameter Name="Type" Type="string"/>
                            </SelectParameters>        
          </asp:SqlDataSource>

    <asp:SqlDataSource ID="sqlLocation" runat="server" ConnectionString="<%$ ConnectionStrings:SiteSqlServer %>"   SelectCommand="WGSA_sp_MeetingListLocations" SelectCommandType="StoredProcedure">
          </asp:SqlDataSource>

    <asp:SqlDataSource ID="sqlType" runat="server" ConnectionString="<%$ ConnectionStrings:SiteSqlServer %>"   SelectCommand="WGSA_sp_MeetingType" SelectCommandType="StoredProcedure">
          </asp:SqlDataSource>
</div>
</div>
