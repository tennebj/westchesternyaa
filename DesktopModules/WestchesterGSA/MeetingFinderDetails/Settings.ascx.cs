﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Collections;

public partial class DesktopModules_IATSE_Document_Listing_Settings : ModuleSettingsBase
{
    const string SETTING_AdminPage = "Documents.AdminPage";
    const string SETTING_PlaceholderText = "Documents.PlaceholderText";

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    public override void LoadSettings()
    {
        txtAdminPageName.Text = ModuleContext.Settings.GetValueOrDefault(SETTING_AdminPage, @"Documents Admin");
        TextBox1.Text = ModuleContext.Settings.GetValueOrDefault(SETTING_PlaceholderText, @"");
    }

    public override void UpdateSettings()
    {
        var controller = new ModuleController();
        controller.UpdateModuleSetting(ModuleContext.ModuleId, SETTING_AdminPage, txtAdminPageName.Text);
        controller.UpdateModuleSetting(ModuleContext.ModuleId, SETTING_PlaceholderText, TextBox1.Text);
    }
}