﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Net;
using System.Data;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Collections;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Services.FileSystem;
using DotNetNuke.Security.Roles;
using DotNetNuke.Abstractions;
using DotNetNuke.Entities.Tabs;
using System.Data.SqlClient;

public partial class DesktopModules_WestchesterGSA_MeetingFinderDetails : DotNetNuke.Entities.Modules.PortalModuleBase, DotNetNuke.Entities.Modules.IActionable
{
    private bool _isLoggedIn;
    private string _adminPageName;
    private string _currentCategory = "";
    private List<string> _alreadyAddedCategories = new List<string>();
    private int _currentIndex = 0;
    private List<string> _finalCategories = new List<string>();
  
    const string SETTING_AdminPage = "MeetingFinder.AdminPage";
    const string SETTING_PlaceholderText = "MeetingFinder.PlaceholderText";

    protected string strMapURL = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        //lblPlaceholderText.Text = ModuleContext.Settings.GetValueOrDefault(SETTING_PlaceholderText, @"");
        //_isLoggedIn = (HttpContext.Current.User.Identity.IsAuthenticated && SecurityManager.UserName.Length > 0); ;
        //_adminPageName = ModuleContext.Settings.GetValueOrDefault(SETTING_AdminPage, @"Documents Admin");
        //bool isAdmin = UserInfo.IsSuperUser || UserInfo.IsInRole("Admin");
        //List<string> classes = new List<string>();
        //if (!isAdmin) classes = UserInfo.Roles.ToList();
        //else
        //{
        //    var roles = (new RoleController()).GetRoles();
        //    foreach (RoleInfo ri in roles)
        //    {
        //        classes.Add(ri.RoleName);
        //    }
        //}
        //for(int i = 0; i < classes.Count; i++)
        //{
        //    classes[i] = classes[i].Replace("_", ".");
        //}
        //var roleGroups = RoleController.GetRoleGroups(this.PortalId);
        //foreach(RoleGroupInfo rgi in roleGroups)
        //{
        //    classes.Add(rgi.RoleGroupName);
        //}
      LoadData();
    }

    private void LoadData()
    {
        lblDay.Text = HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority + HttpContext.Current.Request.RawUrl;
        // Get Meeting ID
        string fullURL = HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority + HttpContext.Current.Request.RawUrl;
        string[] urlParms = fullURL.Split('/');
        int c = urlParms.Count();

        if (urlParms.Count() != 5) Response.Redirect("/Find-a-Meeting");

        
            int meetingID = Convert.ToInt32(urlParms[c - 1]);


            String _connect = System.Configuration.ConfigurationManager.ConnectionStrings["SiteSqlServer"].ConnectionString;
            SqlConnection connection = new SqlConnection(_connect);

            SqlCommand command = connection.CreateCommand();
            command.CommandType = CommandType.StoredProcedure;
            command.CommandText = "WGSA_sp_MeetingDetail";
            command.Parameters.Add("@ID", SqlDbType.Int).Value = meetingID;

            SqlDataAdapter da = new SqlDataAdapter(command);
            DataSet ds = new DataSet();

            da.Fill(ds);
            if (ds.Tables[0].Rows.Count > 0)
            {
                lblDay.Text = ds.Tables[0].Rows[0]["Day"].ToString();
                lblStart.Text = ds.Tables[0].Rows[0]["StartTime"].ToString();
                lblEnd.Text = ds.Tables[0].Rows[0]["EndTime"].ToString();
                 lblMeetingName.Text = ds.Tables[0].Rows[0]["Meeting"].ToString();
                lblCity.Text = ds.Tables[0].Rows[0]["City"].ToString();
                lblNotes.Text = ds.Tables[0].Rows[0]["Notes"].ToString();
                lblLocation.Text = ds.Tables[0].Rows[0]["Location"].ToString();
                lblMeetingCount.Text = ds.Tables[0].Rows[0]["MeetingCount"].ToString();
            lblFullAddress.Text = ds.Tables[0].Rows[0]["FullAddress"].ToString();
            lblAddress2.Text = ds.Tables[0].Rows[0]["Address2"].ToString();
            lblMeetingID.Text = ds.Tables[0].Rows[0]["Meeting_ID"].ToString();

            strMapURL = "https://www.google.com/maps/embed/v1/place?&key=" + System.Configuration.ConfigurationManager.AppSettings["GoogleMapsAPI"] + "&q=" + HttpUtility.UrlEncode(ds.Tables[0].Rows[0]["FullAddress"].ToString());
     

            // Process check boxes display
            // If there is only 1 type then call SP only once
            if (!ds.Tables[0].Rows[0]["Type"].ToString().Contains('|'))
                {
                SqlCommand typeCommand = connection.CreateCommand();
                typeCommand.CommandType = CommandType.StoredProcedure;
                typeCommand.CommandText = "WGSA_sp_GetTypeDescription";
                typeCommand.Parameters.Add("@Code", SqlDbType.VarChar).Value = ds.Tables[0].Rows[0]["Type"].ToString();

                SqlDataAdapter daType = new SqlDataAdapter(typeCommand);
                DataSet dsType = new DataSet();

                daType.Fill(dsType);

                lblTypes.Text = "<li style=\"list-style:none;\"><svg class=\"icon\" width=\"1em\" height=\"1em\" viewBox=\"0 0 16 16\" fill=\"currentColor\" xmlns=\"http://www.w3.org/2000/svg\"><path fill-rule=\"evenodd\" d=\"M10.97 4.97a.75.75 0 0 1 1.071 1.05l-3.992 4.99a.75.75 0 0 1-1.08.02L4.324 8.384a.75.75 0 1 1 1.06-1.06l2.094 2.093 3.473-4.425a.236.236 0 0 1 .02-.022z\"></path></svg>" + dsType.Tables[0].Rows[0]["MeetingType"].ToString() + "</li>";

                //   lblTypes.Text = "<li><i class=\"glyphicon glyphicon-ok\"></i> "  "</li>";
            }
            else
            {
                string[] sTypes = ds.Tables[0].Rows[0]["Type"].ToString().Split('|');
                lblTypes.Text = "";
                foreach (string s in sTypes)
                {
                    SqlCommand typeCommand = connection.CreateCommand();
                    typeCommand.CommandType = CommandType.StoredProcedure;
                    typeCommand.CommandText = "WGSA_sp_GetTypeDescription";
                    typeCommand.Parameters.Add("@Code", SqlDbType.VarChar).Value = s;

                    SqlDataAdapter daType = new SqlDataAdapter(typeCommand);
                    DataSet dsType = new DataSet();

                    daType.Fill(dsType);

                    lblTypes.Text = lblTypes.Text + "<li style=\"list-style:none;\"><svg class=\"icon\" width=\"1em\" height=\"1em\" viewBox=\"0 0 16 16\" fill=\"currentColor\" xmlns=\"http://www.w3.org/2000/svg\"><path fill-rule=\"evenodd\" d=\"M10.97 4.97a.75.75 0 0 1 1.071 1.05l-3.992 4.99a.75.75 0 0 1-1.08.02L4.324 8.384a.75.75 0 1 1 1.06-1.06l2.094 2.093 3.473-4.425a.236.236 0 0 1 .02-.022z\"></path></svg>" + dsType.Tables[0].Rows[0]["MeetingType"].ToString() + "</li>";


                }

            }
        }

       

    }

    protected string GetMapURL()
    {
        return strMapURL;
    }

    private string ConvertStringArrayToString(string[] vs)
    {
        throw new NotImplementedException();
    }

    protected void btnBackToMeeings_Click(object sender, EventArgs e)
    {
        Response.Redirect("/Find-A-Meeting");
    }

    public ModuleActionCollection ModuleActions
    {
        get
        {
            return new ModuleActionCollection();
        }
    }

    
}