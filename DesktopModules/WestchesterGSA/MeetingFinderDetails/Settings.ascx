﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Settings.ascx.cs" Inherits="DesktopModules_IATSE_Document_Listing_Settings" %>
<div id="Div2" class="dnnForm dnnClear">
    <fieldset>
        <div class="form-group">
            <asp:Label ID="lblOne" runat="server" Text="Admin Page Name - this is the name of the page with Document Upload Control."></asp:Label>            
            <asp:TextBox ID="txtAdminPageName" runat="server" CssClass="form-control" style="width:100%;"/>
        </div>
    </fieldset>
    <fieldset>
        <div class="form-group">
            <asp:Label ID="Label1" runat="server" Text="Please enter placeholder text here."></asp:Label>            
            <asp:TextBox ID="TextBox1" runat="server" CssClass="form-control" style="width:100%;"/>
        </div>
    </fieldset>
</div>