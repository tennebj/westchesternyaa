﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MeetingFinderDetails.ascx.cs" Inherits="DesktopModules_WestchesterGSA_MeetingFinderDetails" %>
<%@ Register TagPrefix="dnn" Namespace="DotNetNuke.Web.Client.ClientResourceManagement" Assembly="DotNetNuke.Web.Client" %>

<dnn:DnnCssInclude ID="BootstrapCSS" runat="server" FilePath="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" />

<dnn:DnnJsInclude ID="jQuery" runat="server" FilePath="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js" />

<dnn:DnnJsInclude ID="BootstrapJS" runat="server" FilePath="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" />

<style>
    .hiddencol{
        display: none;
    }
    .lbl-padding{
        padding-top: 5px;
        display:block;
        padding-right: 10%;
    }
</style>



	<div id="meeting" class="container">
		<div class="row">
			<div class="col-md-10 col-md-offset-1 main">
					
				<div class="page-header">
			       <h1><asp:Label Text="" runat="server" ID="lblCity"/></h1>
			<a href="/Find-A-Meeting"><i class="glyphicon glyphicon-chevron-right"></i> Back to Meetings</a>

				</div>

				<div class="row">
					
					<div class="col-md-4">

												<div class="panel panel-default">
							<%--<asp:Button id="LinkButton1" runat="server" Text="Get Directions" OnClick="btnBackToMeeings_Click" />	--%>
						<%--							<a class="panel-heading tsml-directions" href="#" data-latitude="41.125012" data-longitude="-73.711418" data-location="St Stephen's Church">
								<h3 class="panel-title">
									Get Directions									<span class="panel-title-buttons">
										<svg width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
											<path fill-rule="evenodd" d="M9.896 2.396a.5.5 0 0 0 0 .708l2.647 2.646-2.647 2.646a.5.5 0 1 0 .708.708l3-3a.5.5 0 0 0 0-.708l-3-3a.5.5 0 0 0-.708 0z"></path>
											<path fill-rule="evenodd" d="M13.25 5.75a.5.5 0 0 0-.5-.5h-6.5a2.5 2.5 0 0 0-2.5 2.5v5.5a.5.5 0 0 0 1 0v-5.5a1.5 1.5 0 0 1 1.5-1.5h6.5a.5.5 0 0 0 .5-.5z"></path>
										</svg>
									</span>
								</h3>
							</a>--%>
						</div>
						
						<div class="panel panel-default">
							<ul class="list-group">
								<li class="list-group-item meeting-info">
									<p class="meeting-time" style="font-weight:bold" >Meeting Information</p>
									<p class="meeting-time" style="font-weight:bold" ><asp:Label Text="" runat="server" ID="lblDay"/>, <time><asp:Label Text="" runat="server" ID="lblStart"/></time> to <asp:Label Text="" runat="server" ID="lblEnd"/></p>
									<ul class="meeting-types">
																				<asp:Label Text="" runat="server" ID="lblTypes"/>
																				</ul>
																					<p class="meeting-type-description"><asp:Label Text="" runat="server" ID="lblNotes"/></p>
			
								<a href="" class="list-group-item list-group-item-location">
										<h4 class="list-group-item-heading" style="font-weight:bold"><asp:Label Text="" runat="server" ID="lblLocation"/></h4><p class="location-other-meetings"> <asp:Label Text="" runat="server" ID="lblMeetingCount"/>&nbsp;other meetings at this location</p>
									<p class="location-address"><asp:Label Text="" runat="server" ID="lblFullAddress"/></p><section class="location-notes"><p></p></section><p class="location-region"><asp:Label Text="" runat="server" ID="lblAddress2"/></p></a>		<li class="list-group-item list-group-item-group">
										<h4 class="list-group-item-heading"  style="font-weight:bold"><asp:Label Text="" runat="server" ID="lblMeetingName"/> #<asp:Label Text="" runat="server" ID="lblMeetingID"/></h4>
																					<section class="group-notes"><p></p>
</section>
																			</li>
																<li class="list-group-item list-group-item-updated">
									Updated									July 15, 2020								</li>
							</ul>
						</div>

						
					</div>
					<div class="col-md-8">
																		<div id="map"><iframe id="iMap" src="<%=GetMapURL()%>" width="600" height="800"  style="border:0;"  aria-hidden="false" tabindex="0"></iframe></div>
											</div>
				</div>
			</div>
		</div>

					<div class="widgets meeting-widgets meeting-widgets-bottom" role="complementary">
							</div>
		
	</div>



<%--          <asp:SqlDataSource ID="SqlDataSource_MeetingList" runat="server" ConnectionString="<%$ ConnectionStrings:SiteSqlServer %>"
                    SelectCommand="sp_MeetingList" SelectCommandType="StoredProcedure">
               <SelectParameters>
                     <asp:Parameter Name="DayofWeek" Type="string" />
                            </SelectParameters>        
          </asp:SqlDataSource>--%>
