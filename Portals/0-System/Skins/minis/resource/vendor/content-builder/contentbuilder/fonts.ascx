<%@ Control language="vb" AutoEventWireup="false" Explicit="True" Inherits="DotNetNuke.UI.Skins.Skin" %>

<div id="divFontList">

<div data-provider="" data-font-family="" style="font-size:12px;padding:10px 7px;box-sizing:border-box;">None</div>
<div data-provider="" data-font-family="Arial, sans-serif"><img src="<%= SkinPath %>fonts/arial.png"></div>
<div data-provider="" data-font-family="courier"><img src="<%= SkinPath %>fonts/courier.png"></div>
<div data-provider="" data-font-family="Georgia, serif"><img src="<%= SkinPath %>fonts/georgia.png"></div>
<!--<div data-provider="" data-font-family="Helvetica Neue, Helvetica, Arial, sans-serif"><img src="<%= SkinPath %>fonts/helvetica_neue.png"></div>-->
<div data-provider="" data-font-family="monospace"><img src="<%= SkinPath %>fonts/monospace.png"></div>
<div data-provider="" data-font-family="sans-serif"><img src="<%= SkinPath %>fonts/sans_serif.png"></div>
<div data-provider="" data-font-family="serif"><img src="<%= SkinPath %>fonts/serif.png"></div>
<div data-provider="google" data-font-family="Abel, sans-serif"><img src="<%= SkinPath %>fonts/abel.png"></div>
<div data-provider="google" data-font-family="Abril Fatface"><img src="<%= SkinPath %>fonts/abril_fatface.png"></div>
<div data-provider="google" data-font-family="Advent Pro, sans-serif" data-font-style="300"><img src="<%= SkinPath %>fonts/advent_pro.png"></div>
<div data-provider="google" data-font-family="Aladin, cursive"><img src="<%= SkinPath %>fonts/aladin.png"></div>
<div data-provider="google" data-font-family="Alegreya Sans SC" data-font-style="300,700"><img src="<%= SkinPath %>fonts/alegreya_sans_sc.png"></div>
<div data-provider="google" data-font-family="Allerta Stencil, sans-serif"><img src="<%= SkinPath %>fonts/allerta_stencil.png"></div>
<div data-provider="google" data-font-family="Allura, cursive"><img src="<%= SkinPath %>fonts/allura.png"></div>
<div data-provider="google" data-font-family="Almendra Display, cursive"><img src="<%= SkinPath %>fonts/almendra_display.png"></div>
<div data-provider="google" data-font-family="Amatic SC" data-font-style="400,700"><img src="<%= SkinPath %>fonts/amatic_sc.png"></div>
<div data-provider="google" data-font-family="Architects Daughter, cursive"><img src="<%= SkinPath %>fonts/architects_daughter.png"></div>
<div data-provider="google" data-font-family="Assistant" data-font-style="300,700"><img src="<%= SkinPath %>fonts/assistant.png"></div>
<div data-provider="google" data-font-family="Aubrey, cursive"><img src="<%= SkinPath %>fonts/aubrey.png"></div>
<div data-provider="google" data-font-family="Anton, sans-serif"><img src="<%= SkinPath %>fonts/anton.png"></div>
<div data-provider="google" data-font-family="Archivo Narrow, sans-serif"><img src="<%= SkinPath %>fonts/archivo_narrow.jpg"></div>
<div data-provider="google" data-font-family="Bad Script, cursive"><img src="<%= SkinPath %>fonts/bad_script.jpg"></div>
<div data-provider="google" data-font-family="BenchNine, sans-serif"><img src="<%= SkinPath %>fonts/benchNine.jpg"></div>
<div data-provider="google" data-font-family="Bevan, cursive"><img src="<%= SkinPath %>fonts/bevan.png"></div>
<div data-provider="google" data-font-family="Bigelow Rules, cursive"><img src="<%= SkinPath %>fonts/bigelow_rules.png"></div>
<div data-provider="google" data-font-family="Bilbo, cursive"><img src="<%= SkinPath %>fonts/bilbo.jpg"></div>
<div data-provider="google" data-font-family="Bonbon, cursive"><img src="<%= SkinPath %>fonts/bonbon.jpg"></div>
<div data-provider="google" data-font-family="Bowlby One SC, cursive"><img src="<%= SkinPath %>fonts/bowlby_one_sc.jpg"></div>
<div data-provider="google" data-font-family="Cabin Condensed, sans-serif"><img src="<%= SkinPath %>fonts/cabin_condensed.jpg"></div>
<div data-provider="google" data-font-family="Carrois Gothic SC, sans-serif"><img src="<%= SkinPath %>fonts/carrois_gothic_sc.jpg"></div>
<div data-provider="google" data-font-family="Chewy, cursive"><img src="<%= SkinPath %>fonts/chewy.png"></div>
<div data-provider="google" data-font-family="Cinzel, serif"><img src="<%= SkinPath %>fonts/cinzel.jpg"></div>
<div data-provider="google" data-font-family="Comfortaa, cursive" data-font-style="300"><img src="<%= SkinPath %>fonts/comfortaa.jpg"></div>
<div data-provider="google" data-font-family="Concert One, cursive"><img src="<%= SkinPath %>fonts/concert_one.jpg"></div>
<div data-provider="google" data-font-family="Cousine" data-font-style="400,700"><img src="<%= SkinPath %>fonts/cousine.png"></div>
<div data-provider="google" data-font-family="Crafty Girls, cursive"><img src="<%= SkinPath %>fonts/crafty_girls.png"></div>
<div data-provider="google" data-font-family="Cutive Mono"><img src="<%= SkinPath %>fonts/cutive_mono.png"></div>
<div data-provider="google" data-font-family="Devonshire, cursive"><img src="<%= SkinPath %>fonts/devonshire.jpg"></div>
<div data-provider="google" data-font-family="Didact Gothic, sans-serif"><img src="<%= SkinPath %>fonts/didact_gothic.jpg"></div>
<div data-provider="google" data-font-family="Diplomata SC, cursive"><img src="<%= SkinPath %>fonts/diplomata_sc.jpg"></div>
<div data-provider="google" data-font-family="Dosis, sans-serif" data-font-style="200"><img src="<%= SkinPath %>fonts/dosis.jpg"></div>
<div data-provider="google" data-font-family="Elsie" data-font-style="400,900"><img src="<%= SkinPath %>fonts/elsie.png"></div>
<div data-provider="google" data-font-family="Encode Sans" data-font-style="300,700"><img src="<%= SkinPath %>fonts/encode_sans.png"></div>
<div data-provider="google" data-font-family="Exo, sans-serif" data-font-style="100"><img src="<%= SkinPath %>fonts/exo.jpg"></div>
<div data-provider="google" data-font-family="Felipa, cursive"><img src="<%= SkinPath %>fonts/felipa.jpg"></div>
<div data-provider="google" data-font-family="Fjalla One, sans-serif"><img src="<%= SkinPath %>fonts/fjalla_one.png"></div>
<div data-provider="google" data-font-family="Forum, cursive"><img src="<%= SkinPath %>fonts/forum.png"></div>
<div data-provider="google" data-font-family="Frank Ruhl Libre" data-font-style="300,700"><img src="<%= SkinPath %>fonts/frank_ruhl_libre.png"></div>
<div data-provider="google" data-font-family="Fredericka the Great, cursive"><img src="<%= SkinPath %>fonts/fredericka_the_great.jpg"></div>
<div data-provider="google" data-font-family="Gilda Display, serif"><img src="<%= SkinPath %>fonts/gilda_display.jpg"></div>
<div data-provider="google" data-font-family="Give You Glory, cursive"><img src="<%= SkinPath %>fonts/give_you_glory.jpg"></div>
<div data-provider="google" data-font-family="Gruppo, cursive"><img src="<%= SkinPath %>fonts/gruppo.png"></div>
<div data-provider="google" data-font-family="Handlee, cursive"><img src="<%= SkinPath %>fonts/handlee.jpg"></div>
<div data-provider="google" data-font-family="Happy Monkey, cursive"><img src="<%= SkinPath %>fonts/happy_monkey.jpg"></div>
<div data-provider="google" data-font-family="Hind" data-font-style="300,700"><img src="<%= SkinPath %>fonts/hind.png"></div>
<div data-provider="google" data-font-family="Iceland, cursive"><img src="<%= SkinPath %>fonts/iceland.png"></div>
<div data-provider="google" data-font-family="Inconsolata" data-font-style="400,700"><img src="<%= SkinPath %>fonts/inconsolata.png"></div>
<div data-provider="google" data-font-family="Josefin Sans, sans-serif" data-font-style="300,700"><img src="<%= SkinPath %>fonts/josefin_sans.jpg"></div>
<div data-provider="google" data-font-family="Julee"><img src="<%= SkinPath %>fonts/julee.png"></div>
<div data-provider="google" data-font-family="Julius Sans One, sans-serif"><img src="<%= SkinPath %>fonts/julius_sans_one.jpg"></div>
<div data-provider="google" data-font-family="Junge, serif"><img src="<%= SkinPath %>fonts/junge.jpg"></div>
<div data-provider="google" data-font-family="Just Me Again Down Here, cursive"><img src="<%= SkinPath %>fonts/just_me_again_down_here.png"></div>
<div data-provider="google" data-font-family="Kaushan Script, cursive"><img src="<%= SkinPath %>fonts/kaushan_script.png"></div>
<div data-provider="google" data-font-family="Kite One, sans-serif"><img src="<%= SkinPath %>fonts/kite_one.jpg"></div>
<div data-provider="google" data-font-family="Lato" data-font-style="300,700"><img src="<%= SkinPath %>fonts/lato.png"></div>
<div data-provider="google" data-font-family="Lekton" data-font-style="400,700"><img src="<%= SkinPath %>fonts/lekton.png"></div>
<div data-provider="google" data-font-family="Life Savers, cursive"><img src="<%= SkinPath %>fonts/life_savers.jpg"></div>
<div data-provider="google" data-font-family="Lobster"><img src="<%= SkinPath %>fonts/lobster.png"></div>
<div data-provider="google" data-font-family="Lobster Two, cursive"><img src="<%= SkinPath %>fonts/lobster_two.jpg"></div>
<div data-provider="google" data-font-family="Londrina Shadow, cursive"><img src="<%= SkinPath %>fonts/londrina_shadow.jpg"></div>
<div data-provider="google" data-font-family="Lora" data-font-style="400,700"><img src="<%= SkinPath %>fonts/lora.png"></div>
<div data-provider="google" data-font-family="Lovers Quarrel, cursive"><img src="<%= SkinPath %>fonts/lovers_quarrel.jpg"></div>
<div data-provider="google" data-font-family="Macondo, cursive"><img src="<%= SkinPath %>fonts/macondo.png"></div>
<div data-provider="google" data-font-family="Marcellus SC, serif"><img src="<%= SkinPath %>fonts/marcellus_sc.jpg"></div>
<div data-provider="google" data-font-family="Martel" data-font-style="300,700"><img src="<%= SkinPath %>fonts/martel.png"></div>
<div data-provider="google" data-font-family="Maven Pro, sans-serif"><img src="<%= SkinPath %>fonts/maven_pro.png"></div>
<div data-provider="google" data-font-family="Merriweather, serif" data-font-style="300,700"><img src="<%= SkinPath %>fonts/merriweather.png"></div>
<div data-provider="google" data-font-family="Merriweather Sans" data-font-style="300,700"><img src="<%= SkinPath %>fonts/merriweather_sans.png"></div>
<div data-provider="google" data-font-family="Monoton, cursive"><img src="<%= SkinPath %>fonts/monoton.png"></div>
<div data-provider="google" data-font-family="Montez, cursive"><img src="<%= SkinPath %>fonts/montez.png"></div>
<div data-provider="google" data-font-family="Montserrat" data-font-style="300,400,700"><img src="<%= SkinPath %>fonts/montserrat.png"></div>
<div data-provider="google" data-font-family="Montserrat Subrayada, sans-serif"><img src="<%= SkinPath %>fonts/montserrat_subrayada.jpg"></div>
<div data-provider="google" data-font-family="Muli" data-font-style="300,700"><img src="<%= SkinPath %>fonts/muli.png"></div>
<div data-provider="google" data-font-family="Neuton" data-font-style="200,700"><img src="<%= SkinPath %>fonts/neuton.png"></div>
<div data-provider="google" data-font-family="Nixie One, cursive"><img src="<%= SkinPath %>fonts/nixie_one.png"></div>
<div data-provider="google" data-font-family="Nothing You Could Do, cursive"><img src="<%= SkinPath %>fonts/nothing_you_could_do.jpg"></div>
<div data-provider="google" data-font-family="Open Sans, sans-serif" data-font-style="300,400,600,800"><img src="<%= SkinPath %>fonts/open_sans.jpg"></div>
<div data-provider="google" data-font-family="Oranienbaum, serif"><img src="<%= SkinPath %>fonts/oranienbaum.jpg"></div>
<div data-provider="google" data-font-family="Oswald, sans-serif" data-font-style="300,400,700"><img src="<%= SkinPath %>fonts/oswald.png"></div>
<div data-provider="google" data-font-family="Oxygen" data-font-style="300,700"><img src="<%= SkinPath %>fonts/oxygen.png"></div>
<div data-provider="google" data-font-family="Passion One, cursive"><img src="<%= SkinPath %>fonts/passion_one.jpg"></div>
<div data-provider="google" data-font-family="Pathway Gothic One"><img src="<%= SkinPath %>fonts/pathway_gothic_one.png"></div>
<div data-provider="google" data-font-family="Petit Formal Script, cursive"><img src="<%= SkinPath %>fonts/petit_formal_script.png"></div>
<div data-provider="google" data-font-family="Philosopher, sans-serif"><img src="<%= SkinPath %>fonts/philosopher.jpg"></div>
<div data-provider="google" data-font-family="Playfair Display" data-font-style="400,400i,700"><img src="<%= SkinPath %>fonts/playfair_display.png"></div>
<div data-provider="google" data-font-family="Poiret One, cursive"><img src="<%= SkinPath %>fonts/poiret_one.jpg"></div>
<div data-provider="google" data-font-family="Pompiere, cursive"><img src="<%= SkinPath %>fonts/pompiere.png"></div>
<div data-provider="google" data-font-family="Poppins" data-font-style="400,600"><img src="<%= SkinPath %>fonts/poppins.png"></div>
<div data-provider="google" data-font-family="PT Serif" data-font-style="400,700"><img src="<%= SkinPath %>fonts/pt_serif.png"></div>
<div data-provider="google" data-font-family="Quattrocento Sans, sans-serif"><img src="<%= SkinPath %>fonts/quattrocento_sans.jpg"></div>
<div data-provider="google" data-font-family="Quattrocento, serif"><img src="<%= SkinPath %>fonts/quattrocento.jpg"></div>
<div data-provider="google" data-font-family="Quicksand, sans-serif"><img src="<%= SkinPath %>fonts/quicksand.jpg"></div>
<div data-provider="google" data-font-family="Qwigley, cursive"><img src="<%= SkinPath %>fonts/qwigley.jpg"></div>
<div data-provider="google" data-font-family="Raleway, sans-serif" data-font-style="100"><img src="<%= SkinPath %>fonts/raleway.png"></div>
<div data-provider="google" data-font-family="Raleway Dots, sans-serif"><img src="<%= SkinPath %>fonts/raleway_dots.png"></div>
<div data-provider="google" data-font-family="Redressed, cursive"><img src="<%= SkinPath %>fonts/redressed.jpg"></div>
<div data-provider="google" data-font-family="Ribeye Marrow, cursive"><img src="<%= SkinPath %>fonts/ribeye_marrow.png"></div>
<div data-provider="google" data-font-family="Righteous, cursive"><img src="<%= SkinPath %>fonts/righteous.png"></div>
<div data-provider="google" data-font-family="Roboto, sans-serif" data-font-style="300"><img src="<%= SkinPath %>fonts/roboto.jpg"></div>
<div data-provider="google" data-font-family="Roboto Mono" data-font-style="300,700"><img src="<%= SkinPath %>fonts/roboto_mono.png"></div>
<div data-provider="google" data-font-family="Rochester"><img src="<%= SkinPath %>fonts/rochester.png"></div>
<div data-provider="google" data-font-family="Rouge Script, cursive"><img src="<%= SkinPath %>fonts/rouge_script.png"></div>
<div data-provider="google" data-font-family="Sacramento, cursive"><img src="<%= SkinPath %>fonts/sacramento.jpg"></div>
<div data-provider="google" data-font-family="Sanchez, serif"><img src="<%= SkinPath %>fonts/sanchez.jpg"></div>
<div data-provider="google" data-font-family="Satisfy, cursive"><img src="<%= SkinPath %>fonts/satisfy.jpg"></div>
<div data-provider="google" data-font-family="Seaweed Script, cursive"><img src="<%= SkinPath %>fonts/seaweed_script.jpg"></div>
<div data-provider="google" data-font-family="Seymour One, sans-serif"><img src="<%= SkinPath %>fonts/seymour_one.jpg"></div>
<div data-provider="google" data-font-family="Shadows Into Light Two, cursive"><img src="<%= SkinPath %>fonts/shadows_into_light_two.jpg"></div>
<div data-provider="google" data-font-family="Six Caps"><img src="<%= SkinPath %>fonts/six_caps.png"></div>
<div data-provider="google" data-font-family="Snowburst One, cursive"><img src="<%= SkinPath %>fonts/snowburst_one.jpg"></div>
<div data-provider="google" data-font-family="Source Code Pro" data-font-style="300,700"><img src="<%= SkinPath %>fonts/source_code_pro.png"></div>
<div data-provider="google" data-font-family="Source Sans Pro, sans-serif" data-font-style="200"><img src="<%= SkinPath %>fonts/source_sans_pro.jpg"></div>
<div data-provider="google" data-font-family="Special Elite, cursive"><img src="<%= SkinPath %>fonts/special_elite.jpg"></div>
<div data-provider="google" data-font-family="Squada One, cursive"><img src="<%= SkinPath %>fonts/squada_one.jpg"></div>
<div data-provider="google" data-font-family="Stint Ultra Expanded, cursive"><img src="<%= SkinPath %>fonts/stint_ultra_expanded.jpg"></div>
<div data-provider="google" data-font-family="Syncopate, sans-serif"><img src="<%= SkinPath %>fonts/syncopate.png"></div>
<div data-provider="google" data-font-family="Tangerine, cursive"><img src="<%= SkinPath %>fonts/tangerine.png"></div>
<div data-provider="google" data-font-family="Tenor Sans, sans-serif"><img src="<%= SkinPath %>fonts/tenor_sans.png"></div>
<div data-provider="google" data-font-family="Ubuntu Mono" data-font-style="400,700"><img src="<%= SkinPath %>fonts/ubuntu_mono.png"></div>
<div data-provider="google" data-font-family="UnifrakturMaguntia, cursive"><img src="<%= SkinPath %>fonts/unifrakturmaguntia.png"></div>
<div data-provider="google" data-font-family="Vast Shadow, cursive"><img src="<%= SkinPath %>fonts/vast_shadow.png"></div>
<div data-provider="google" data-font-family="Viga, sans-serif"><img src="<%= SkinPath %>fonts/viga.jpg"></div>
<div data-provider="google" data-font-family="Voltaire, sans-serif"><img src="<%= SkinPath %>fonts/voltaire.jpg"></div>
<div data-provider="google" data-font-family="Wire One, sans-serif"><img src="<%= SkinPath %>fonts/wire_one.png"></div>

</div>

<script src="<%= SkinPath %>jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">
    jQuery(document).ready(function ($) {

        if (parent._cb.settings.emailMode) {
            $('[data-provider=google]').remove();
        }

        $("#divFontList div").click(function () {
            window.parent.focus();
            if ($(this).hasClass('on')) {//toggle
                $('.cell-fontfamily-options', window.parent.document).attr('data-font-family', '');
                $('.cell-fontfamily-options', window.parent.document).attr('data-font-style', '');
                $('.cell-fontfamily-options', window.parent.document).attr('data-provider', '');
            } else {
                $('.cell-fontfamily-options', window.parent.document).attr('data-font-family', $(this).data('font-family'));
                var s = '';
                if ($(this).data('font-style')) {
                    s = $(this).data('font-style');
                }
                $('.cell-fontfamily-options', window.parent.document).attr('data-font-style', s);
                $('.cell-fontfamily-options', window.parent.document).attr('data-provider', $(this).data('provider'));
            }
            parent.applyFont();
        });

    });

</script>


