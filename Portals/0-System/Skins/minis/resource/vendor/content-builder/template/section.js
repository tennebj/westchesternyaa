﻿//Layout 
data_basic.designs.push({
    'thumbnail': 'layout/col-12.jpg',
    'category': '999',
    'html': `<div class="is-section is-box">
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container is-builder ui-sortable">
                <div class="row">
                    <div class="col-md-12">
                        <div class="is-builder ui-sortable"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>`
}, {
    'thumbnail': 'layout/col-3-9.jpg',
    'category': '999',
    'html': `<div class="is-section is-box">
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container is-builder ui-sortable">
                <div class="row">
                    <div class="col-md-3">
                        <div class="is-builder ui-sortable"></div>
                    </div>
                    <div class="col-md-9">
                        <div class="is-builder ui-sortable"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>`
}, {
    'thumbnail': 'layout/col-4-8.jpg',
    'category': '999',
    'html': `<div class="is-section is-box">
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container is-builder ui-sortable">
                <div class="row">
                    <div class="col-md-4">
                        <div class="is-builder ui-sortable"></div>
                    </div>
                    <div class="col-md-8">
                        <div class="is-builder ui-sortable"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>`
}, {
    'thumbnail': 'layout/col-5-7.jpg',
    'category': '999',
    'html': `<div class="is-section is-box">
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container is-builder ui-sortable">
                <div class="row">
                    <div class="col-md-5">
                        <div class="is-builder ui-sortable"></div>
                    </div>
                    <div class="col-md-7">
                        <div class="is-builder ui-sortable"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>`
}, {
    'thumbnail': 'layout/col-6-6.jpg',
    'category': '999',
    'html': `<div class="is-section is-box">
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container is-builder ui-sortable">
                <div class="row">
                    <div class="col-md-6">
                        <div class="is-builder ui-sortable"></div>
                    </div>
                    <div class="col-md-6">
                        <div class="is-builder ui-sortable"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>`
}, {
    'thumbnail': 'layout/col-7-5.jpg',
    'category': '999',
    'html': `<div class="is-section is-box">
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container is-builder ui-sortable">
                <div class="row">
                    <div class="col-md-7">
                        <div class="is-builder ui-sortable"></div>
                    </div>
                    <div class="col-md-5">
                        <div class="is-builder ui-sortable"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>`
}, {
    'thumbnail': 'layout/col-8-4.jpg',
    'category': '999',
    'html': `<div class="is-section is-box">
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container is-builder ui-sortable">
                <div class="row">
                    <div class="col-md-8">
                        <div class="is-builder ui-sortable"></div>
                    </div>
                    <div class="col-md-4">
                        <div class="is-builder ui-sortable"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>`
}, {
    'thumbnail': 'layout/col-9-3.jpg',
    'category': '999',
    'html': `<div class="is-section is-box">
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container is-builder ui-sortable">
                <div class="row">
                    <div class="col-md-9">
                        <div class="is-builder ui-sortable"></div>
                    </div>
                    <div class="col-md-3">
                        <div class="is-builder ui-sortable"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>`
}, {
    'thumbnail': 'layout/col-4-4-4.jpg',
    'category': '999',
    'html': `<div class="is-section is-box">
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container is-builder ui-sortable">
                <div class="row">
                    <div class="col-md-4">
                        <div class="is-builder ui-sortable"></div>
                    </div>
                    <div class="col-md-4">
                        <div class="is-builder ui-sortable"></div>
                    </div>
                    <div class="col-md-4">
                        <div class="is-builder ui-sortable"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>`
}, {
    'thumbnail': 'layout/col-3-6-3.jpg',
    'category': '999',
    'html': `<div class="is-section is-box">
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container is-builder ui-sortable">
                <div class="row">
                    <div class="col-md-3">
                        <div class="is-builder ui-sortable"></div>
                    </div>
                    <div class="col-md-6">
                        <div class="is-builder ui-sortable"></div>
                    </div>
                    <div class="col-md-3">
                        <div class="is-builder ui-sortable"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>`
}, {
    'thumbnail': 'layout/col-3-3-3-3.jpg',
    'category': '999',
    'html': `<div class="is-section is-box">
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container is-builder ui-sortable">
                <div class="row">
                    <div class="col-md-3">
                        <div class="is-builder ui-sortable"></div>
                    </div>
                    <div class="col-md-3">
                        <div class="is-builder ui-sortable"></div>
                    </div>
                    <div class="col-md-3">
                        <div class="is-builder ui-sortable"></div>
                    </div>
                    <div class="col-md-3">
                        <div class="is-builder ui-sortable"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>`
});


/* 
 ${getSnippetCode("carousel01")}
 <style class="build-css" data-class="button-01">${csstemplate["button-01"]}</style>
*/

/*Home01*/
data_basic.designs.push({
    'thumbnail': 'minis-section/home01/banner.jpg',
    'category': '800',
    'html': `
<div class="is-section is-box is-light-text is-section-100 is-section-md-50">
    <div class="is-overlay">
        <div class="is-overlay-bg is-scale-animated" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/home01/homepage01-banner.jpg&quot;);">
        </div>

    </div>
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container">
                <div class="row justify-content-end">
                    <div class="col-md-12" style="max-width: 650px;">
                        <h1 class="size-60" style="letter-spacing: 3px;">Achieve Everything You Can Imagine</h1>
                        <div class="spacer height-40"></div>
                        <p class="size-18">Nam ut auctor lectus, sit amet sagittis felis. Vivamus nisi neque, blandit non elementum a, feugiat ac nisl accumsan.&nbsp;</p>
                        <div class="spacer height-20"></div>
                        <a href="#" title="Learn more" class="button-01 border-radius-3 mt-5">LEARN MORE</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style class="build-css" data-class="button-01">${csstemplate["button-01"]}</style>
`
}, {
    'thumbnail': 'minis-section/home01/section-01.jpg',
    'category': '800',
    'html': `
<div class="is-section is-box is-bg-light is-dark-text">
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container mb-40 mb-lg-80">
                <div class="row">
                    <div class="col-md-12">
                        <div class="title-02 mt-10">
                            <small class="color-accent">OUR FEATURES</small>
                            <h3>We Specialize In Quality Design</h3>
                        </div>
                        <div class="text-center m-auto" style="max-width: 700px;">
                            <p class="size-18" style="color: rgb(102, 102, 102);">Nam pharetra dui vitae nulla vestibulum, at porta turpis faucibus. Aenean rutrum vel risus ut mollis. Ut convallis nisl id ante aliquet.</p>
                        </div>
                        <div class="spacer height-40 mb-5"></div>
                    </div>
                </div>
                ${getSnippetCode("iconbox-02")}
            </div>
        </div>
    </div>
</div>
<style class="build-css" data-class="title-02">${csstemplate["title-02"]}</style>
`
}, {
    'thumbnail': 'minis-section/home01/section-02.jpg',
    'category': '800',
    'html': `
<div class="is-section">
    <div class="is-boxes">
        <div class="is-box-6 is-light-text is-box">
            <div class="is-overlay">
                <div class="is-overlay-content"></div>
                <div class="is-overlay-bg" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/home01/homepage01-img01.jpg&quot;);"></div>
            </div>
            <div class="is-boxes"></div>
        </div>
        <div class="is-box-6 is-box" style="background-color: rgb(245, 247, 252);">
            <div class="is-boxes">
                <div class="is-box-centered">
                    <div class="is-container container-fluid" style="max-width: 640px;">
                        <div class="row">
                            <div class="col-md-12 pl-xl-70">
                                <div class="spacer height-20 d-none d-lg-block"></div>
                                <div class="title-03">
                                    <small class="color-accent">ABOUT US</small>
                                    <h3>We Develop Products That Are Ahead Of The Curve</h3>
                                </div>
                                <p style="font-size: 22px; color: rgb(51, 51, 51); font-weight: 600; padding-bottom: 5px;">Present you a great website with stylish elements.</p>
                                <p  style="color: rgb(102, 102, 102);">We are an experienced team who are passionate about delivering the most recent trend. Always using the most popular tools &amp; technology on the market, so that you can keep up with the trend.</p>
                                <div class="spacer height-20"></div>
                                <div> <a href="#" title="Learn more" class="button-01 border-radius-3 mt-10 mb-5 mb-lg-10">LEARN MORE</a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style class="build-css" data-class="title-03">${csstemplate["title-03"]}</style>
<style class="build-css" data-class="button-01">${csstemplate["button-01"]}</style>
`
}, {
    'thumbnail': 'minis-section/home01/section-03.jpg',
    'category': '800',
    'html': `<div class="is-section is-box">
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="title-02 mt-10">
                            <small class="color-accent">RECENT PROJECTS</small>
                            <h3>Our Great And Stunning Works</h3>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="pb-5"></div>
                        ${getSnippetCode("carousel01")}
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="spacer height-20"></div>
                    </div>
                </div>
                <div class="row text-center m-auto" style="max-width: 700px;">
                    <div class="col-md-12">
                        <p>Nam pharetra dui vitae nulla vestibulum, at porta turpis faucibus. Aenean rutrum vel risus ut mollis. Ut convallis nisl id ante aliquet.</p>
                        <div class="spacer height-20"></div>
                        <div>
                            <a href="#" title="Learn more" class="button-01 border-radius-3 mt-15 mb-20">LEARN MORE</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style class="build-css" data-class="title-02">${csstemplate["title-02"]}</style>
<style class="build-css" data-class="button-01">${csstemplate["button-01"]}</style>`
}, {
    'thumbnail': 'minis-section/home01/section-04.jpg',
    'category': '800',
    'html': `<div class="is-section is-box is-bg-light is-dark-text is-section-auto">
    <div class="is-overlay">
        <div class="is-overlay-content"></div>
        <div class="is-overlay-bg" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/home01/homepage01-bg01.jpg&quot;);"></div>
    </div>
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container container-fluid" style="max-width: 1270px;">
                <div class="row">
                    <div class="col-md-8">
                        <div class="spacer height-100"></div>
                        <h5 class="color-accent">WE ARE PROFESSIONAL</h5>
                        <div class="spacer height-20"></div>
                        <h1 style="font-weight: 600;">Provide Website Solutions With Powerful Technology.</h1>
                        <div class="spacer height-40"></div>
                        <div><a href="#" title="learn more" class="button-01 mt-10">CONTACT US</a></div>
                        <div class="spacer height-100 mb-5"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style class="build-css" data-class="button-01">${csstemplate["button-01"]}</style>`
}, {
    'thumbnail': 'minis-section/home01/section-05.jpg',
    'category': '800',
    'html': `<div class="is-section is-box is-bg-light is-dark-text">
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container">
                <div class="row text-center align-items-center">
                    <div class="col-md-12">
                        <div class="title-02 mt-10">
                            <small style="color-accent">INFO YOU INTEREST</small>
                            <h3>FAQs Updated In Real-time</h3>
                        </div>
                        <div class="text-center m-auto" style="max-width: 700px;">
                            <p class="size-18" style="color: rgb(102, 102, 102);">Nam pharetra dui vitae nulla vestibulum, at porta turpis faucibus. Aenean rutrum vel risus ut mollis. Ut convallis nisl id ante aliquet.</p>
                        </div>
                        <div class="spacer height-40 mb-5"></div>
                    </div>
                </div>
                <div class="row align-items-center">
                    <div class="col-md-5 mb-30 mb-md-0"><img class="img-Lazy" src="/Portals/_default/ContentBuilder/minis-page/home01/homepage01-img02.jpg" alt=""></div>
                    <div class="col-md-7 pl-lg-40">
                        ${getSnippetCode("accordion01")}
                    </div>

                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="spacer height-20"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style class="build-css" data-class="title-02">${csstemplate["title-02"]}</style>
`
}, {
    'thumbnail': 'minis-section/home01/footer.jpg',
    'category': '800',
    'html': `<div class="is-section is-box" style="background-color: rgb(39, 39, 39);">
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container mb-md-50">
                <div class="row size-16" style="color: rgb(135, 135, 135);">
                    <div class="col-md-4">
                        <div style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/home01/homepage01-footer-map.png&quot;); background-position: center center; background-repeat: no-repeat; background-size: auto;"><img src="/Portals/_default/ContentBuilder/minis-page/logo-white.png" style="margin-bottom: 39px;margin-top: 5px;">
                            <p class="mb-25">Aliquam efficitur, nisl tempor imperdiet aliquam, neque sem mattis est, quis dapibus nisl ipsum in libero ultricies.</p>
                        </div>
                        <div class="edit-box mb-5">
                            <a class="social-03 facebook-f" href="#" title=""><i class="sico fab-facebook-f"></i></a><a class="social-03 linkedin-in" href="#" title=""><i class="sico fab-linkedin-in"></i></a><a class="social-03 twitter" href="#" title=""><i class="sico fab-twitter"></i></a>
                        </div>
                        <div class="spacer height-20"></div>
                        <p>@ 2019 by DNNGo Corp.</p>
                    </div>

                    <div class="col-md-4">
                    <h5 style="color: rgb(255, 255, 255);" class="pt-10 mb-15">Contact Us</h5>
                    <div class="spacer height-20">
                    </div>
                    <p class="mb-0">Free legal advice</p>
                    <p class="size-24 mb-15 color-accent" style=" font-style: italic;">+1 845-359-0545</p>
                    <p class="mb-0">Email address:</p>
                    <p class="mb-25">service.simple@gmail.com</p>
                    <p class="mb-0">Address:</p>
                    <p class="mb-0">Yanta W Rd, Yanta District, Xian Shi, Shaanxi</p>
                    <br>
                    </div>
                    <div class="col-md-4">
                        <h5 style="color: rgb(255, 255, 255);" class="pt-10 mb-15">Friendly Links</h5>
                        <div class="spacer height-20"></div>
                        <div class="row home01-footer-linklist">
                            <div class="col-sm-6">
                                <p><a href="#" title="What we do">What we do</a></p>
                                <p><a href="#" title="Our service">Our service</a></p>
                                <p><a href="#" title="Creative design">Creative design</a></p>
                                <p><a href="#" title="Industry experts">Industry experts</a></p>
                                <p><a href="#" title="Feature">Feature</a></p>
                                <p><a href="#" title="Testimonials">Testimonials</a></p>
                            </div>
                            <div class="col-sm-6">
                                <p><a href="#" title="Our team">Our team</a></p>
                                <p><a href="#" title="Our pricing">Our pricing</a></p>
                                <p><a href="#" title="FAQ">FAQ</a></p>
                                <p><a href="#" title="The latest news">The latest news</a></p>
                                <p><a href="#" title="Amazing support">Amazing support</a></p>
                                <p><a href="#" title="Professional skills">Professional skills</a></p>
                            </div>
                            
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12" data-noedit="">
                        <div class="spacer height-40"></div>
                    </div>
                </div>
                <div class="pt-30 height-20" style="border-top: 1px solid rgba(255, 255, 255, 0.1);"></div>
                ${getSnippetCode("clientlist01")}
            </div>
        </div>
    </div>
</div>
<style class="build-css" data-class="home01-footer-linklist">
    .home01-footer-linklist a,
    .home01-footer-linklist a:link {
        color: currentColor;
    }

    .home01-footer-linklist a:hover {
        color: #FFF
    }

    .home01-footer-linklist p {
        margin-bottom: 10px;
    }
</style>
<style class="build-css" data-class="clientlist01">
        .clientlist01 {
            text-align: center;
        }

        .clientlist01 a img {
            opacity: .5;
            margin: 15px 0;
            transition: opacity ease-in 300ms;
        }

        .clientlist01 a:hover img {
            opacity: 1;
        }
    </style><style class="build-css" data-class="social-03">${csstemplate["social-03"]}</style>`
});

/*Home02*/

data_basic.designs.push({
    'thumbnail': 'minis-section/home02/banner.jpg',
    'category': '801',
    'html': `
    <div class="is-section is-section-100 is-section-md-50 is-dark-text is-box is-align-center">
	<div class="is-overlay">
		<div class="is-overlay-color"></div>
		<div class="is-overlay-content"></div>
		<div class="is-overlay-bg" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/home02/HomePage2-banner.jpg&quot;);"></div>
	</div>
	<div class="is-boxes">
		<div class="is-box-centered">
			<div class="is-container container-fluid" style="max-width: 640px;">
				<div class="row">
					<div class="col-md-12">
						<h1 class="size-60">Imagination Is The Source Of Creation</h1>
						<div class="spacer height-40"></div>
						<p class="size-16">Vestibulum sit amet tellus placerat, aliquam risus et, tempus felis. Suspendisse mollis nisl eu elit dapibus, vitae posuere dolor laoreet.</p>
						<div class="spacer height-40 mt-15"></div>
						<a class="button-01" href="#" title="">LEARN MORE</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<style class="build-css" data-class="button-01">${csstemplate["button-01"]}</style>
`
}, {
    'thumbnail': 'minis-section/home02/section-01.jpg',
    'category': '801',
    'html': `
<div class="is-section is-box homepage02-section01">
	<div class="is-boxes">
		<div class="is-box-centered">
			<div class="is-container layout-container">
				<div class="row">
					<div class="col-md-12">
						<div class="spacer height-20"></div>
					</div>
				</div>
				<div class="row" style="margin-bottom: -20px;">
					<div class="col-lg-4 col-md-6">
						<div class="iconbox-01">
							<div class="icon">
								<i class="sico lnr-rocket"></i>
							</div>
							<div class="content edit-box">
								<h6>High Efficiency</h6>
								<div class="line"></div>
								<p>Morbi vehicula pellentesque neque, tincidunt ultricies massa placerat pretium tortor mi.</p>
								<a href="#" style="font-weight: bold;">MORE +</a>
							</div>
						</div>
						<div class="spacer height-40 d-none d-lg-block"></div>
					</div>
					<div class="col-lg-4 col-md-6">
						<div class="iconbox-01">
							<div class="icon">
								<i class="sico lnr-database"></i>
							</div>
							<div class="content edit-box">
								<h6>All-inclusive Specialties</h6>
								<div class="line"></div>
								<p>Pellentesque sed maximus orci. Suspendisse tincidunt consequat ipsum id sagittis. </p>
								<a href="#" style="font-weight: bold;">MORE +</a>
							</div>
						</div>
						<div class="spacer height-40 d-none d-lg-block"></div>
					</div>

					<div class="col-lg-4 col-md-6">
						<div class="iconbox-01">
							<div class="icon">
								<i class="sico lnr-bullhorn"></i>
							</div>
							<div class="content edit-box">
								<h6>Instant Notifications</h6>
								<div class="line"></div>
								<p>Curabitur suscipit lorem sit amet urna congue, sit amet dignissim diam feugiat liquam.</p>
								<a href="#" style="font-weight: bold;">MORE +</a>
							</div>
						</div>
						<div class="spacer height-40 d-none d-lg-block"></div>
					</div>

					<div class="col-lg-4 col-md-6">
						<div class="iconbox-01">
							<div class="icon">
								<i class="sico lnr-laptop-phone"></i>
							</div>
							<div class="content edit-box">
								<h6>Fully Compatible</h6>
								<div class="line"></div>
								<p>Aenean sit amet dolor pellentesque, vestibulum quam a, imperdiet dui. Vestibulum quis euismod.</p>
								<a href="#" style="font-weight: bold;">MORE +</a>
							</div>
						</div>
					</div>
					<div class="col-lg-4 col-md-6">
						<div class="iconbox-01">
							<div class="icon">
								<i class="sico lnr-magic-wand"></i>
							</div>
							<div class="content edit-box">
								<h6>Creative Design</h6>
								<div class="line"></div>
								<p>Sed sagittis eros id urna euismod, sit amet tempor odio hendrerit. Sed ullamcorper quam.</p>
								<a href="#" style="font-weight: bold;">MORE +</a>
							</div>
						</div>
					</div>
					<div class="col-lg-4 col-md-6">
						<div class="iconbox-01">
							<div class="icon">
								<i class="sico lnr-clipboard-check"></i>
							</div>
							<div class="content edit-box">
								<h6>Unlimited Options</h6>
								<div class="line"></div>
								<p>Nulla sodales risus eget tortor posuere, sed varius nulla volutpat. Sed sed nunc vitae.</p>
								<a href="#" style="font-weight: bold;">MORE +</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<style class="build-css" data-class="iconbox-01">${csstemplate["iconbox-01"]}</style>
`
}, {
    'thumbnail': 'minis-section/home02/section-02.jpg',
    'category': '801',
    'html': `
<div class="is-section is-box is-bg-grey homepage02-section02">
	<div class="is-overlay">
		<div class="is-overlay-bg" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/home02/HomePage2-about-bg.jpg&quot;);">
		</div>
	</div>
	<div class="is-boxes">
		<div class="is-box-centered">
			<div class="is-container layout-container">
				<div class="row">
					<div class="col-md-12">
						<div class="spacer height-20"></div>
					</div>
				</div>
				<div class="row align-items-center">
					<div class="col-md-6">
						<div class="col-md-12 text-center">
							<div style="width: 100%; position: relative;" class=""><img class="shadow-lg img-fluid img-Lazy" alt=""  src="/Portals/_default/ContentBuilder/minis-page/home02/HomePage2-img01.jpg"></div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="spacer height-40 d-md-none"></div>
						<div class="title-01">
							<h2>What <span class="color-accent">We Do</span></h2>
						</div>
						<p class="pt-10 pb-5" style="font-size: 22px; line-height:34px; color: rgb(51, 51, 51); font-weight: 600;">Our experienced developers can turn your brief ideas into powerful features like a magic.</p>
						<p style="color: rgb(102, 102, 102);">Quisque faucibus eu lectus in eleifend. Nullam id aliquam erat, sit amet consectetur nunc. In dapibus sit amet diam in posuere. In eget imperdiet turpis. Etiam imperdiet scelerisque nibh porta fringilla. </p>
						<div class="spacer height-20 mb-15"></div>
						<a class="button-01" href="#">LEARN MORE</a>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="spacer height-20"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<style class="build-css" data-class="title-01">${csstemplate["title-01"]}</style>
<style class="build-css" data-class="button-01">${csstemplate["button-01"]}</style>
`
}, {
    'thumbnail': 'minis-section/home02/section-03.jpg',
    'category': '801',
    'html': `
<div class="is-section is-box homepage02-section03">
	<div class="is-boxes">
		<div class="is-box-centered">
			<div class="is-container layout-container">
				<div class="row">
					<div class="col-md-12">
						<div class="spacer mt-5 mt-md-20"></div>
					</div>
				</div>
				<div class="row align-items-center">
					<div class="col-md-6">
						<div class="title-01">
							<h2>How Do <span class="color-accent">We Work</span></h2>
						</div>
						<p class="size-18" style="color: rgb(102, 102, 102);">Curabitur at magna sem. Etiam fringilla, eros vel efficitur
							congue, nisi felis luctus nulla, sit amet tincidunt nulla.</p>
						<div class="spacer height-20 mb-15"></div>
                        ${getSnippetCode("infobox-01")}
					</div>
					<div class="col-md-6">
						<div style="position: relative; left: 16%;" class=""><img class="img-Lazy shadow-lg" style="max-width: 100%; " alt="" src="/Portals/_default/ContentBuilder/minis-page/home02/Minis-HomePage2_03.png" ></div>
						<div style="width: 50%; position: absolute; right: -10%; opacity: 1; top: 50%;transform: translate3d(0px, -50%, 0);" class=""><img class="img-Lazy shadow-lg"  alt="" src="/Portals/_default/ContentBuilder/minis-page/home02/Minis-HomePage2_06.png" ></div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="spacer height-20"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<style class="build-css" data-class="title-01">${csstemplate["title-01"]}</style>
`
}, {
    'thumbnail': 'minis-section/home02/section-04.jpg',
    'category': '801',
    'html': `
<div class="is-section is-box homepage02-section04 layout-no-mt layout-no-mb layout-no-plr" style="border-top: 1px solid #f1f1f1">
	<div class="is-boxes">
		<div class="is-box-centered">
			<div class="is-container layout-container">
				<div class="row pt-5 pb-4">
					<div class="col-md-12 text-center">
						<div class="spacer mt-lg-80 mt-40 height-20"></div>
						<div class="title-01">
							<h2>Trusted Technical <span class="color-accent">Team</span></h2>
						</div>
						<p class="text-center m-auto size-18" style="max-width: 780px;line-height: 30px;color: rgb(102, 102, 102);">Vestibulum sit amet tellus placerat, aliquam risus et, tempus felis. Suspendisse mollis nisl eu elit dapibus, vitae posuere dolor laoreet. </p>
						<div class="spacer height-60"></div>
					</div>
				</div>
			</div>
			<div class="is-container is-container-fluid layout-container">
                ${getSnippetCode("ourteam-01-list")}
			</div>
		</div>
	</div>
</div>
<style class="build-css" data-class="title-01">${csstemplate["title-01"]}</style>
`
}, {
    'thumbnail': 'minis-section/home02/section-05.jpg',
    'category': '801',
    'html': `
<div class="is-section is-box is-light-text homepage02-section05" style="background-color: rgb(70, 73, 86);">
	<div class="is-boxes">
		<div class="is-box-centered">
			<div class="is-container layout-container mt-70 mb-70">
            ${getSnippetCode("counter-01")}
			</div>
		</div>
	</div>
</div>
<style class="build-css" data-class="homepage02-section05">
    .homepage02-section05 .is-boxes {
        min-height: auto;
    }
</style>
`
}, {
    'thumbnail': 'minis-section/home02/section-06.jpg',
    'category': '801',
    'html': `
<div class="is-section is-box homepage02-section06">
	<div class="is-boxes">
		<div class="is-box-centered">
			<div class="is-container layout-container">

				<div class="row align-items-center">
					<div class="col-md-5">
						<div class="title-01">
							<h2>Frequently Asked <span class="color-accent">Questions</span></h2>
						</div>
						<div class="spacer height-20 mb-10"></div>
						<p class="size-18" style="color: rgb(102, 102, 102);">Vestibulum sit amet tellus placerat, aliquam risus et, tempus felis. Suspendisse mollis nisl eu elit dapibus, vitae posuere dolor laoreet.</p>
						<div class="spacer height-40"></div>
						<a class="button-01" href="#">LEARN MORE</a>
						<div class="spacer height-40 d-md-none"></div>
					</div>
                    <div class="col-md-7">
                    ${getSnippetCode("accordion03")}
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="spacer height-20"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<style class="build-css" data-class="button-01">${csstemplate["button-01"]}</style>
<style class="build-css" data-class="title-01">${csstemplate["title-01"]}</style>
`
}, {
    'thumbnail': 'minis-section/home02/section-07.jpg',
    'category': '801',
    'html': `
<div class="is-section is-box homepage02-section07">
	<div class="is-boxes" style="background-color: rgb(249, 249, 249);">
		<div class="is-box-centered">
			<div class="is-container layout-container">
				<div class="row">
					<div class="col-md-12">
						<div class="spacer mb-5"></div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="text-center">
							<div class="title-01">
								<h2>What Our <span class="color-accent">Clients Say</span></h2>
							</div>
							<p class="m-auto size-18" style="max-width: 750px;color: rgb(102, 102, 102);">Nunc et urna tincidunt elit scelerisque sollicitudin. Sed eget maximus purus. Mauris laoreet sapien magna, ut auctor mauris lobortis.</p>
						</div>
						<div class="spacer height-60"></div>
					</div>
				</div>
                ${getSnippetCode("blockquotes-02")}
			</div>
		</div>
	</div>
</div>
<style class="build-css" data-class="title-01">${csstemplate["title-01"]}</style>
`
}, {
    'thumbnail': 'minis-section/home02/section-08.jpg',
    'category': '801',
    'html': `
<div class="is-section is-box homepage02-section08">
	<div class="is-overlay">
		<div class="is-overlay-bg" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/home02/homepage02-section08-bg.jpg&quot;);">
		</div>
	</div>
	<div class="is-boxes">
		<div class="is-box-centered">
			<div class="is-container layout-container" style="max-width: 834px;">
				<div class="row">
					<div class="col-md-12">
						<div class="spacer height-20"></div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="edit-box text-center">
							<p class="size-18" style="font-weight: 600; color: rgb(66, 66, 66);">Subscribe to the latest news</p>
							<div class="spacer height-20"></div>
							<h1 style=" font-weight: 600;">The Latest Update Are Right Here Waiting For You</h1>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="spacer height-20 mb-15"></div>
					</div>
				</div>
				<div class="row">
                    <div class="col-md-12 m-auto" style="max-width: 500px;">
                     ${getSnippetCode("ajaxform-02")}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
`
}, {
    'thumbnail': 'minis-section/home02/footer.jpg',
    'category': '801',
    'html': `
<div class="is-section is-box homepage02-footer">
	<div class="is-overlay">
		<div class="is-overlay-bg" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/home02/footer-bg.jpg&quot;);"></div>
	</div>
	<div class="is-boxes">
		<div class="is-box-centered">
			<div class="is-container layout-container">

				<div class="row">
					<div class="col-md-4">
						<h5 style="margin-bottom: 35px;">Contact Us</h5>
						<p class="size-18 mb-5" style="color: rgb(66, 66, 66); font-weight: 600;">Call us:</p>
						<p class="color-accent mb-15" style="font-size: 22px; font-weight: 600;"><a href="tel:08453597777">(845) 359-7777</a></p>
						<p class="size-18 mb-3" style="font-weight: 600; color: rgb(66, 66, 66);">Email us:</p>
						<p>service.simple@gmail.com</p>
						<p class="size-18 mb-3" style="color: rgb(66, 66, 66); font-weight: 600;">Address:</p>
						<p>252 NY-303, Orangeburg, NY 10965</p>
					</div>
					<div class="col-md-4 mt-30 mt-md-0">
						<h5 style="margin-bottom: 35px;">Friendly Links</h5>
						<div class="row">
							<div class="col-md-6 footer-top-links">
								<p><a href="#">What we do</a></p>
								<p><a href="#">Our service</a></p>
								<p><a href="#">Creative design</a></p>
								<p><a href="#">Industry experts</a></p>
								<p><a href="#">Feature</a></p>
								<p><a href="#">Website SEO</a></p>
							</div>
							<div class="col-md-6 footer-top-links">
								<p><a href="#">Our team</a></p>
								<p><a href="#">Our pricing</a></p>
								<p><a href="#">FAQ</a></p>
								<p><a href="#">The latest news</a></p>
								<p><a href="#">Amazing support</a></p>
								<p><a href="#">UI design</a></p>
							</div>
						</div>
					</div>
					<div class="col-md-4 mt-30 mt-md-0">
						<img style="margin-top:-4px;" class="mb-35 img-fluid img-Lazy" src="/Portals/_default/ContentBuilder/minis-page/home02/homepage02-logo-black.png"
						 alt="logo">
						<p style="line-height: 30px;">Nulla sit amet consectetur elit. Donec interdum interdum dolor, sed varius neque rhoncus eu. Morbi metus tortor ultricies eget.</p>
						<div class="edit-box" style="margin-left: -8px;">
							<a class="social-05" href="#"><i class="sico fab-twitter"></i></a>
							<a class="social-05" href="#"><i class="sico fab-pinterest-p"></i></a>
							<a class="social-05" href="#"><i class="sico fab-linkedin-in"></i></a>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-12">
						<div class="spacer height-20"></div>
					</div>
				</div>
				<div class="row" style="border-top: 1px solid rgb(228, 228, 228); padding-top: 32px; margin-top: 32px;">
					<div class="col-md-6">
						<p>@ 2019 by DNNGo Corp. All Rights Resevered</p>
					</div>
					<div class="col-md-6 footer-bottom-links">
						<p class="text-md-right"><a href="#">Privacy Policy</a><span class="pr-10 pl-10">|</span><a href="#">Terms And Conditions</a></p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="spacer height-40"></div>
					</div>
				</div>

			</div>
		</div>
	</div>
</div> 
<style class="build-css" data-class="social-05">${csstemplate["social-05"]}</style>
<style class="build-css" data-class="homepage02-footer"> .homepage02-footer .footer-top-links p {
    margin-bottom: 12px;
}
.homepage02-footer .footer-top-links a,
.homepage02-footer .footer-bottom-links a {
    color: inherit;
    text-decoration: none;
}

.homepage02-footer .footer-top-links a:hover,
.homepage02-footer .footer-bottom-links a:hover {
    color:var(--accent-color);
}</style>
`
})

/*Home03*/

data_basic.designs.push({
    'thumbnail': 'minis-section/home03/banner.jpg',
    'category': '802',
    'html': `<div class="is-section is-box is-bg-grey is-light-text is-section-100 is-section-lg-50">
	<div class="is-overlay">
		<div class="is-overlay-bg" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/home03/homepage03-banner.jpg&quot;);">
		</div>
	</div>
	<div class="is-boxes">
		<div class="is-box-centered">
			<div class="is-container layout-container m-auto mb-md-0">
				<div class="row">
					<div class="col-md-12">
						<div class="spacer mb-40 mb-md-60 mb-xl-80 pt-xl-60 height-200"></div>
					</div>
				</div>
				<div class="row align-items-center">
					<div class="col-md-7 text-center text-md-left">
						<h2 style="font-weight:600;">Not Only Beautifully Designed, But Also Super Easy To Use</h2>
						<div class="spacer height-40"></div>
						<div class="mb-50 mb-md-0"><a href="#" title="Learn more" class="button-01">LEARN MORE</a></div>
					</div>
					<div class="col-md-2"></div>
					<div class="col-md-3 text-center"><a class="play-button is-lightbox" href="https://www.youtube.com/embed/AkcUoA2f-jU" data-ilightbox="youtube" title=""><span><i class="sico fas-play color-accent size-18"></i></span></a>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="spacer height-120 mb-md-40 mb-xl-70"></div>
					</div>
                </div>
                ${getSnippetCode("iconbox-03")}
				<div class="row">
					<div class="col-md-12">
						<div class="spacer height-80 d-lg-none"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<style class="build-css" data-class="play-button">${csstemplate["play-button"]}</style>
<style class="build-css" data-class="button-01">${csstemplate["button-01"]}</style>
`
}, {
    'thumbnail': 'minis-section/home03/section-01.jpg',
    'category': '802',
    'html': `
<div class="is-section is-box">
	<div class="is-boxes">
		<div class="is-box-centered">
            <div class="is-container layout-container">
                <div class="row">
					<div class="col-md-12">
                        <div class="spacer height-20"></div>
					</div>
				</div>
				<div class="row align-items-center">
					<div class="col-md-6">
						<div class="title-04" style="max-width:550px;">
							<small class="color-accent">WHAT WE DO</small>
							<h3>Our Experienced Developers Can Turn Your Brief Ideas Into Powerful Features Like A Magic.</h3>
						</div>
						<p style="font-weight: 600;">Fusce at elementum arcu, sit amet imperdiet enim. Duis ultrices convallis nunc, sed ultrices libero iaculis posuere. Nulla et ligula eget ipsum viverra faucibus. Nulla lectus dolor, condimentum ac luctus a, aliquam et velit. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
						<div class="spacer height-20 mb-5"></div>
						<div class="mb-50 mb-md-0"> <a href="#" title="Learn more" class="button-01">LEARN MORE</a></div>
					</div>
					<div class="col-md-1">
					</div>
					<div class="col-md-5">
						<div class="home03-section02-right">
							<div class="pic" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/home03/homepage03-bg02.png&quot;); background-position: right center; background-repeat: no-repeat; background-size: auto;">
								<img alt="" class="img-Lazy" src="/Portals/_default/ContentBuilder/minis-page/home03/homepage03-img01.jpg"></div>
							<div class="content">
								<div class="number-box">
									<div class="custom-module loading" data-effect="Number" id="module-{id}" data-moduleid="{id}" data-settings="%7B%22number%22%3A%2230%22%7D"></div>+
								</div>
								<p>Years Of Experience On Designing</p>
								<i class="icon-bg sico lnr-pencil-ruler2"></i>
							</div>
						</div>
				</div>
				</div>
			</div>
		</div>
	</div>
</div>
<style class="build-css" data-class="title-04">${csstemplate["title-04"]}</style>
<style class="build-css" data-class="button-01">${csstemplate["button-01"]}</style>
<style class="build-css" data-class="home03-section02-right">
.home03-section02-right {
    position: relative;
    z-index: 2;
    margin-bottom: 20px;
}

.home03-section02-right .bg {
    position: absolute;
    right: 0;
    top: 10%;
    z-index: -1;
}

.home03-section02-right .content {
    position: absolute;
    background-color:var(--accent-color);
    bottom: -35px;
    left: -60px;
    width: 260px;
    padding: 58px 30px 38px;
    text-align: center;
    color: #fff;
    font-size: 22px;
    z-index: 2;
}

.home03-section02-right .number-box {
    font-size: 50px;
    font-weight: bold;
    line-height: 70px;
}

.home03-section02-right .custom-module {
    display: inline-block;
}

.home03-section02-right .icon-bg {
    position: absolute;
    bottom: 0;
    left: 0;
    color: #000;
    font-size: 210px;
    opacity: .05;
    z-index: -1;
}

@media only screen and (max-width: 767px) {
    .home03-section02-right .content {

        bottom: -15px;
        left: -15px;
    }
}
</style>

`
}, {
    'thumbnail': 'minis-section/home03/section-02.jpg',
    'category': '802',
    'html': `
<div class="is-section is-shadow-1 is-bg-grey is-section-auto" data-element="">
	<div class="is-boxes">
		<div class="is-box-6 is-light-text is-box">
			<div class="is-overlay">
				<div class="is-overlay-bg" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/home03/homepage03-img02.jpg&quot;);"></div>
			</div>

			<div class="is-boxes"></div>
		</div>
		<div class="is-box-6 is-box current-cover-bg">
			<div class="is-overlay">
				<div class="is-overlay-bg" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/home03/homepage03-bg03.jpg&quot;);"></div>
			</div>
			<div class="is-boxes">
				<div class="is-box-centered">
					<div class="is-container container-fluid" style="max-width: 600px;">
						<div class="row">
							<div class="col-md-12">
								<div class="spacer height-120 mb-10"></div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="title-04 mb-0 pl-lg-40">
									<small class="color-accent">OUR SERVICES</small>
									<h1 style="font-weight:600;">We Promise That The Service We Provide For You Is <span class="color-accent">All-round</span></h1>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="spacer height-120 mb-10"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<style class="build-css" data-class="title-04">${csstemplate["title-04"]}</style>
`
}, {
    'thumbnail': 'minis-section/home03/section-03.jpg',
    'category': '802',
    'html': `
<div class="is-section is-box">
	<div class="is-boxes">
		<div class="is-box-centered">
			<div class="is-container layout-container mb-20 mb-md-60">
				<div class="row">
					<div class="col-md-12">
						<div class="title-05 mt-10" style="max-width: 700px;">
							<small class="color-accent">OUR LATEST PROJECTS</small>
							<h3 style="font-weight:600;">These Are Only Some Of The Great Works We Delivered To Our Clients</h3>
						</div>
					</div>
                </div>
                ${getSnippetCode("img-box01")}
			</div>
		</div>
	</div>
</div>
<style class="build-css" data-class="title-05">${csstemplate["title-05"]}</style>
`
}, {
    'thumbnail': 'minis-section/home03/section-04.jpg',
    'category': '802',
    'html': `
<div class="is-section is-box is-light-text" style="background-color: rgb(63, 71, 97);">
	<div class="is-boxes is-section-md-auto">
		<div class="is-box-centered">
			<div class="is-container layout-container mt-md-60 mb-md-60">
				<div class="row">
					<div class="col-md-12">
						<div class="promo01">
							<div class="promo-icon"><i class="sico lnr-rocket" default=""></i></div>
							<div class="promo-content">
								<h3 style="font-weight:600;">Free Lifetime Upgrade</h3>
								<p class="mb-10" style="font-weight: 600;">We promise free lifetime upgrade, one-time purchase, no worry lifetime.</p>
							</div>

							<div class="promo-button">
								<a href="#" title="CONTACT US" class="button-01">CONTACT US</a>
							</div>
						</div>

						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<style class="build-css" data-class="button-01">${csstemplate["button-01"]}</style>
<style class="build-css" data-class="promo01">${csstemplate["promo01"]}</style>
`
}, {
    'thumbnail': 'minis-section/home03/section-05.jpg',
    'category': '802',
    'html': `
<div class="is-section is-box is-bg-light is-dark-text current-cover-bg">
	<div class="is-overlay">

	</div>
	<div class="is-boxes">
		<div class="is-box-centered">
			<div class="is-container layout-container mb-40 mb-md-80">
				<div class="row justify-content-center" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/home03/homepage03-bg04.png&quot;); background-position: left center; background-repeat: no-repeat; background-size: contain;">
					<div class="col-lg-8">
						<div class="title-05 mt-10">
							<small>TESTIMONIALS</small>
                        </div>
                        ${getSnippetCode("testimonials01")}
					</div>
				</div>

			</div>
		</div>
	</div>
</div>
<style class="build-css" data-class="title-05">${csstemplate["title-05"]}</style>
`
}, {
    'thumbnail': 'minis-section/home03/section-06.jpg',
    'category': '802',
    'html': `
<div class="is-section is-box is-bg-grey is-light-text is-section-auto">
	<div class="is-overlay">
		<div class="is-overlay-bg" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/home03/homepage03-bg05.jpg&quot;);">
		</div>
	</div>
	<div class="is-boxes">
		<div class="is-box-centered">
			<div class="is-container layout-container">
				<div class="row">
					<div class="col-md-6"></div>
					<div class="col-md-6">
						<div class="spacer mb-15 height-20"></div>
						<h2 style="font-weight:600;">Are You Ready To Start</h2>
						<p class="size-18 mt-35" style="font-weight: 600;">Integer dignissim ultricies est vitae maximus. Curabitur eget
							enim sem. Morbi atdiam. Sed sit amet sagittis enim.</p>
						<div class="spacer height-20 mb-10"></div>
						<p><a class="button-01" href="#" title="START NOW">START NOW</a></p>
						<div class="spacer height-20"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<style class="build-css" data-class="button-01">${csstemplate["button-01"]}</style>
`
}, {
    'thumbnail': 'minis-section/home03/footer.jpg',
    'category': '802',
    'html': `
<div class="is-section is-box">
	<div class="is-boxes">
		<div class="is-box-centered">
			<div class="is-container layout-container mt-60 mt-md-30 pt-md-10 mb-0">
				<div class="row">
					<div class="col-md-6 col-lg-4">
						<div class="spacer height-20 d-none d-md-block"></div>
						<h5 class="mb-25">Subscription</h5>
						<p style="font-weight: 600;">Suspendisse ut tristique turpis. Duis libero ligula, suscipit at urna elementum.</p>
                        <div class="spacer height-20"></div>
                        ${getSnippetCode("ajaxform-04")}
						<div class="spacer height-40"></div>
					</div>
					<div class="col-md-6 col-lg-2 home03-footer-linklist">
						<div class="spacer height-20 d-none d-md-block"></div>
						<h5 class="mb-25">About Us</h5>
						<p><a href="#" title="">What we do</a></p>
						<p><a href="#" title="">Our features</a></p>
						<p><a href="#" title="">Our team</a></p>
						<p><a href="#" title="">Reasonable price</a></p>
						<div class="spacer height-40"></div>
					</div>
					<div class="col-md-6 col-lg-2 home03-footer-linklist">
						<div class="spacer height-20 d-none d-md-block"></div>
						<h5 class="mb-25">Friendly links</h5>
						<p><a href="#" title="">Industry experts</a></p>
						<p><a href="#" title="">FAQs</a></p>
						<p><a href="#" title="">Contact us</a></p>
						<p><a href="#" title="">Recent posts</a></p>
						<div class="spacer height-40"></div>
					</div>
					<div class="col-md-6 col-lg-4">
						<div class="spacer height-20 d-none d-md-block"></div>
						<h5 class="mb-25">Follow Us</h5>
						<p style="font-weight: 600;">Donec gravida lectus quis mi suscipit molestie. Praesent commodo tristique nisi.</p>
						<div class="edit-box pt-5">
							<a class="social-09" href="#"><i class="sico fab-twitter"></i></a>
							<a class="social-09" href="#"><i class="sico fab-pinterest-p"></i></a>
							<a class="social-09" href="#"><i class="sico fab-linkedin-in"></i></a>
						</div>
						<div class="spacer height-40"></div>
					</div>


				</div>
				<div class="row align-items-center col-no-padding pt-20 pb-20" style="border-top-width: 1px; border-top-style: solid;border-top-color: #e2e2e2;">
					<div class="col-md-6 text-center text-md-left"><img class="img-Lazy" alt="" src="/Portals/_default/ContentBuilder/minis-page/home03/homepage03-logo-black.png"></div>
					<div class="col-md-6 text-center text-md-right">
						<p class="mt-15 mt-md-0 edit-box mb-0">@ 2019 by DNNGo Corp. All Rights Resevered</p>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>
<style class="build-css" data-class="social-09">${csstemplate["social-09"]}</style>
<style class="build-css" data-class="home03-footer-linklist">
.home03-footer-linklist a,
.home03-footer-linklist a:link {
    color: #787878;
    font-weight: 600
}
.home03-footer-linklist a:hover {
    color:var(--accent-color);
}
.home03-footer-linklist p {
    margin: 0 0 11px;
}
</style>
    `
})



/*Home04*/

data_basic.designs.push({
        'thumbnail': 'minis-section/home04/banner.jpg',
        'category': '803',
        'html': `
    <div class="is-section is-section-100 is-section-md-50 is-light-text is-box is-bg-grey homepage04-banner">
      <div class="is-overlay">
        <div class="is-overlay-color"></div>
        <div class="is-overlay-content"></div>
        <div class="is-overlay-bg" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/home04/homepage04-banner.jpg&quot;);"></div>
      </div>
      <div class="is-boxes">
        <div class="is-box-centered">
          <div class="is-container container-fluid" style="max-width: 900px;">
            <div class="row">
              <div class="col-md-12">
                <div class="edit-box text-center">
                  <div class="title-08 mb-40"> <small>CREATIVE DESIGN</small>
                    <h1 style="font-weight: bold;">MAKE YOUR SITE STAND OUT WITH A STYLISH STYLE</h1>
                  </div>
                  <div><a href="#" title="learn more" class="button-01">CONTACT US</a></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <style class="build-css" data-class="title-08">${csstemplate["title-08"]}</style>
    <style class="build-css" data-class="button-01">${csstemplate["button-01"]}</style>
    `
    }, {
        'thumbnail': 'minis-section/home04/section-01.jpg',
        'category': '803',
        'html': `
    <div class="is-section is-box homepage04-section01">
      <div class="is-boxes">
        <div class="is-box-centered">
          <div class="is-container layout-container">
          ${getSnippetCode("infobox-02")}
          </div>
        </div>
      </div>
    </div>
    <style class="build-css" data-class="homepage04-section01">
    .homepage04-section01.is-box .is-boxes{
        padding-bottom: 25px;
    }
    .homepage04-section01.is-box .is-container{
        margin-top: -120px;
        margin-bottom: 0;
    }
    @media (max-width: 991px){
        .homepage04-section01.is-box .is-container{
            margin-top: 60px;
            margin-bottom: 0;
      }
    }
</style>
    `
    }, {
        'thumbnail': 'minis-section/home04/section-02.jpg',
        'category': '803',
        'html': `
    <div class="is-section is-box homepage04-section02">
      <div class="is-boxes">
        <div class="is-box-centered">
          <div class="is-container layout-container">
            <div class="row">
              <div class="col-md-12">
                <div class="edit-box text-center mt-10 mb-5">
                  <div class="title-08 m-auto" style="max-width: 650px;"> <small class="color-accent">ABOUT US</small>
                    <h3>Skilled People Come Up With Creative Ideas When Together</h3>
                  </div>
                </div>
                <div class="spacer height-60"></div>
              </div>
            </div>
            <div class="row d-flex align-items-center">
              <div class="col-md-6">
                <div class="pic"> <img class="img-fluid shadow-md img-Lazy" alt="Images" src="/Portals/_default/ContentBuilder/minis-page/home04/homepage04-section02-01.jpg"> </div>
              </div>
              <div class="col-md-6">
                <div class="spacer height-40 d-md-none"></div>
                <div class="edit-box margin-left-1024-reset" style="margin-left: 40px;">
                  <h4 style="font-weight: 700; margin-bottom: 31px;"><i class="sico lnr-pencil-ruler2 color-accent" style="margin-right: 20px;font-size: 30px;vertical-align: sub;"></i>What We Do</h4>
                  <p>Morbi feugiat, nibh nec laoreet mollis, augue mi accumsan velit, ac scelerisque turpis purus ut odio. Donec ut massa est. Sed congue lorem id lorem maximus ultricies.</p>
                  <a class="button-02" href="#" style="margin-top: 16px;"><i class="sico lnr-plus"></i></a> </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="spacer height-60"></div>
              </div>
            </div>
            <div class="row d-flex align-items-center">
              <div class="col-md-6 order-md-2">
                <div class="pic"> <img class="img-fluid shadow-md img-Lazy" alt="Images" src="/Portals/_default/ContentBuilder/minis-page/home04/homepage04-section02-02.jpg"> </div>
              </div>
              <div class="col-md-6 order-md-1">
                <div class="spacer height-40 d-md-none"></div>
                <div class="edit-box margin-right-1024-reset text-md-right" style="margin-right: 40px;">
                  <h4 style="font-weight: 700; margin-bottom: 31px;">How We Work<i class="sico lnr-cog color-accent" style="margin-left: 20px;font-size: 30px;vertical-align: sub;"></i></h4>
                  <p>Cras egestas enim turpis, non elementum lorem varius nec. Quisque sapien mi, porttitor nec dui at, tristique egestas arcu. Morbi tempus leo sit amet consequat pellentesque.</p>
                  <a class="button-02" href="#" style="margin-top: 16px;"><i class="sico lnr-plus"></i></a> </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="spacer height-20"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <style class="build-css" data-class="title-08">${csstemplate["title-08"]}</style>
    <style class="build-css" data-class="button-02">${csstemplate["button-02"]}</style>
    <style class="build-css" data-class="homepage04-section02">
        .homepage04-section02.is-box .is-boxes{
            margin-top: -25px;
        }
    </style>
    `
    }, {
        'thumbnail': 'minis-section/home04/section-03.jpg',
        'category': '803',
        'html': `
    <div class="is-section is-box is-light-text homepage04-section03">
      <div class="is-overlay">
        <div class="is-overlay-bg" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/home04/homepage04-section03-bg.jpg&quot;);"> </div>
      </div>
      <div class="is-boxes">
        <div class="is-box-centered">
          <div class="is-container layout-container mt-70 mb-70">
            <div class="row">
              <div class="col-md-12">
                <div class="spacer height-20"></div>
              </div>
            </div>
            ${getSnippetCode("counter-02")}
            <div class="row">
              <div class="col-md-12">
                <div class="spacer height-20"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>`
    }, {
        'thumbnail': 'minis-section/home04/section-04.jpg',
        'category': '803',
        'html': `
    <div class="is-section is-box homepage04-section04">
      <div class="is-boxes">
        <div class="is-box-centered">
          <div class="is-container layout-container">
            <div class="row">
              <div class="col-md-12">
                <div class="spacer height-20"></div>
              </div>
            </div>
            <div class="row d-flex align-items-center">
              <div class="col-md-6">
                <div class="pic text-center"> <img class="img-fluid shadow-md img-Lazy" alt="Images" src="/Portals/_default/ContentBuilder/minis-page/home04/homepage04-section04-img.jpg"> <a class="play-button is-lightbox" data-ilightbox="youtube" href="https://www.youtube.com/embed/AkcUoA2f-jU" data-type="iframe" data-options="width: '1280',height: '720',skin:'dark'" title=""><span><i class="sico fas-play color-accent size-18"></i></span></a> </div>
              </div>
              <div class="col-md-6">
                <div class="edit-box">
                  <div class="spacer height-40 d-md-none"></div>
                  <div class="title-08"> <small class="color-accent">OUR STRENGTH</small>
                    <h3>Use Superb Skills To Design You A Beautiful And Stylish Site</h3>
                  </div>
                  ${getSnippetCode("progressbar01")}                 
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="spacer height-20"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <style class="build-css" data-class="title-08">${csstemplate["title-08"]}</style>
    <style class="build-css" data-class="play-button">${csstemplate["play-button"]}</style>
    <style class="build-css" data-class="homepage04-section04">
    .homepage04-section04.is-box .pic {
        position: relative;
    }
    .homepage04-section04.is-box .pic .play-button{
        position: absolute;
        top: 50%;
        left: 50%;
        -webkit-transform: translate3d(-50%,-50%,0);
        transform: translate3d(-50%,-50%,0);
    }
</style>
    `
    }, {
        'thumbnail': 'minis-section/home04/section-05.jpg',
        'category': '803',
        'html': `
    <div class="is-section is-box is-dark-text homepage04-section05" style="background-color: rgb(240, 243, 255);">
      <div class="is-boxes">
        <div class="is-box-centered">
          <div class="is-container layout-container">
            <div class="row">
              <div class="col-md-12">
                <div class="spacer mb-10"></div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="edit-box text-center">
                  <div class="title-08 mb-30"> <small class="color-accent">WHAT WE DO</small>
                    <h3 class="m-auto">Would Like To know Our Team? </h3>
                  </div>
                  <p class="m-auto" style="color:#666666;max-width: 750px;">Pellentesque feugiat feugiat mattis. In gravida tortor ut nibh suscipit commodo. Vestibulum ac luctus felis. Nulla nec neque maximus, hendrerit enim eget.</p>
                  <div class="spacer height-60"></div>
                </div>
              </div>
            </div>
            ${getSnippetCode("infobox-03")}
            <div class="row">
              <div class="col-md-12">
                <div class="spacer mt-10"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <style class="build-css" data-class="title-08">${csstemplate["title-08"]}</style>
    `
    }, {
        'thumbnail': 'minis-section/home04/section-06.jpg',
        'category': '803',
        'html': `
    <div class="is-section is-box homepage04-section06">
      <div class="is-boxes">
        <div class="is-box-centered">
          <div class="is-container layout-container">
            <div class="row">
              <div class="col-md-12">
                <div class="spacer height-20"></div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-7">
                <h3>Our Memorable Stories</h3>
                <p class="mb-20 size-18">Duis dapibus bibendum elit in malesuada. Proin varius eu nisl aliquam tincidunt.</p>
                ${getSnippetCode("carousel02")}
              </div>
              <div class="col-md-5">
                <div class="homepage04-section06-form">
                  <h4>Email Us</h4>
                  <p>Phasellus in sapien lectus. Phasellus commodo facilisis tincidunt pretium.</p>
                  ${getSnippetCode("ajaxform-06","{id2}")}
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="spacer height-20"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <style class="build-css" data-class="homepage04-section06">
    .homepage04-section06 .homepage04-section06-form{
        float: right;
        padding: 50px 40px;
        max-width: 426px;
        -webkit-box-shadow: 0px 0px 25px rgba(0,0,0,0.1);
        box-shadow: 0px 0px 25px rgba(0,0,0,0.1);
        }
        @media (max-width: 767px) {
            .homepage04-section06 .homepage04-section06-form{
                float: none;
                max-width: none;
                margin-top: 60px;
            }
        }
    </style>
    `
    }, {
        'thumbnail': 'minis-section/home04/section-07.jpg',
        'category': '803',
        'html': `
    <div class="is-section is-box homepage04-section07 is-light-text is-align-center">
      <div class="is-overlay">
        <div class="is-overlay-bg" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/home04/homepage04-section07-bg.jpg&quot;);"> </div>
      </div>
      <div class="is-boxes">
        <div class="is-box-centered">
          <div class="is-container layout-container" style="max-width: 904px;">
            <div class="col-md-12">
              <div class="spacer height-60"></div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <h1 style="font-weight: 300;"><strong>Special Design With Fashion Trends</strong>, Our Design Follows The Times.</h1>
                <div class="spacer height-20"></div>
                <a href="#" title="CONTACT US" class="button-01 mt-10 ml-15">CONTACT US</a> <a href="#" title="BUT IT NOW" class="button-01 mt-10 ml-15">BUT IT NOW</a> </div>
            </div>
            <div class="col-md-12">
              <div class="spacer height-60 mt-10"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <style class="build-css" data-class="homepage04-section07">
    .homepage04-section07 .button-01 + .button-01{
        color: #fff;
        border: 2px solid #fff;
        background: transparent;
        line-height: 46px;
      }
    </style>
    `
    }, {
        'thumbnail': 'minis-section/home04/footer.jpg',
        'category': '803',
        'html': `
        <div class="is-section is-box homepage04-footer">
        <div class="is-boxes">
            <div class="is-box-centered">
            <div class="is-container layout-container m-auto">
                <div class="row pt-60 mt-10 pt-md-80">
                <div class="col-md-4">
                    <div class="footer-top-links">
                    <p><a href="#">Home</a></p>
                    <p><a href="#">About Us</a></p>
                    <p><a href="#">Our Service</a></p>
                    <p><a href="#">Terrific Team</a></p>
                    </div>
                </div>
                <div class="col-md-4">
                    <p class="size-18 mb-2" style="color: rgb(66, 66, 66); font-weight: 600;">Call us:</p>
                    <p class="color-accent mb-10" style="font-size: 22px; font-style: italic;"><a href="tel:08453597777">(845) 359-7777</a></p>
                    <p class="size-18 mb-1" style="color: rgb(66, 66, 66); font-weight: 600;">Address:</p>
                    <p>252 NY-303, Orangeburg, NY 10965</p>
                </div>
                <div class="col-md-4">
                    <p class="mb-0" style="color: rgb(66, 66, 66); font-weight: 600; font-size: 20px;" data-keep-font-size="">Get In Touch</p>
                    <p>Sed facilisis justo eu tellus vulputate euismod.</p>
                    <div class="edit-box" style="margin-left: -8px;"> ${getSnippetCode("social-05")} </div>
                </div>
                </div>
                <div class="row align-items-center pt-20 pb-20 mt-60" style="border-top-width: 1px; border-top-style: solid;border-top-color: #e2e2e2;">
                <div class="col-md-6 text-center text-md-left"><img class="img-Lazy" alt="" src="/Portals/_default/ContentBuilder/minis-page/home03/homepage03-logo-black.png"></div>
                <div class="col-md-6 text-center text-md-right">
                    <p class="mt-15 mt-md-0 mb-0">@ 2019 by DNNGo Corp. All Rights Resevered</p>
                </div>
                </div>
            </div>
            </div>
        </div>
        </div>
        <style class="build-css" data-class="social-05">${csstemplate["social-05"]}</style>
        <style class="build-css" data-class="homepage04-footer">
          .homepage04-footer .footer-top-links{
              color: #333333;
          }
          .homepage04-footer .footer-top-links p{
              margin-bottom: 12px;
              font-size: 18px;
          }
          .homepage04-footer .footer-top-links a,
          .homepage04-footer .footer-bottom-links a{
              color: inherit;
              text-decoration: none;
          }
          .homepage04-footer .footer-top-links a:hover,
          .homepage04-footer .footer-bottom-links a:hover{
              color: var(--accent-color);
          }
        </style>
        `
    }

)


/*Home05*/

data_basic.designs.push({
    'thumbnail': 'minis-section/home05/banner.jpg',
    'category': '804',
    'html': `
    <div class="is-section is-section-100 is-section-lg-50 is-shadow-1">
        <div class="is-boxes">
            <div class="is-box is-bg-light is-dark-text is-box-6">
                <div class="is-overlay">
                    <div class="is-overlay-content"></div>
                    <div class="is-overlay-bg"
                        style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/home05/homepage05-banner-bg.png&quot;);">
                    </div>
                </div>
                <div class="is-boxes">
                    <div class="is-box-centered">
                        <div class="is-container container-fluid" style="max-width: 540px;" dragwithouthandle="">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="spacer height-20"></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <span class="size-24 color-accent mb-15" style="font-weight: 700;">Keep up to date</span>
                                    <h1 class="size-60" style="font-weight: 800;"> Creative Thinking Inspires Ideas</h1>
                                    <div class="spacer height-20 mb-10"></div>
                                    <p class="size-18">Nam ac justo id felis tincidunt elementum. Sed a erat sit amet risus venenatis lacinia. Interdum et malesuada fames ac ante ipsum primis in faucibus.</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="spacer height-20  d-none d-lg-block">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="is-box is-box-6">
                <div class="is-overlay">
                    <div class="is-overlay-bg"
                        style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/home05/homepage05-banner.jpg&quot;);">
                    </div>
                </div>
                <div class="is-boxes">
                    <div class="is-box-centered">
                    </div>
                </div>
            </div>
        </div>
    </div>
    `
}, {
    'thumbnail': 'minis-section/home05/section-01.jpg',
    'category': '804',
    'html': `
    <div class="is-section">
        <div class="is-container layout-container m-auto" dragwithouthandle="">
            <div class="row home05-clientlist" style="margin-top: -50px;">
                <div class="col-md-1/5"><a href="#" title="" class=""><span class="Lazy-loading" style="width:170px"></span><img alt="" class="img-Lazy" src="/Portals/_default/ContentBuilder/minis-page/home05/homepage05-client01.png"></a>
                </div>
                <div class="col-md-1/5"><a href="#" title="" class=""><span class="Lazy-loading" style="width:170px"></span><img alt="" class="img-Lazy" src="/Portals/_default/ContentBuilder/minis-page/home05/homepage05-client02.png"></a>
                </div>
                <div class="col-md-1/5"><a href="#" title="" class=""><span class="Lazy-loading" style="width:170px"></span><img alt="" class="img-Lazy" src="/Portals/_default/ContentBuilder/minis-page/home05/homepage05-client03.png"></a>
                </div>
                <div class="col-md-1/5"><a href="#" title="" class=""><span class="Lazy-loading" style="width:170px"></span><img alt="" class="img-Lazy" src="/Portals/_default/ContentBuilder/minis-page/home05/homepage05-client04.png"></a>
                </div>
                <div class="col-md-1/5"><a href="#" title="" class=""><span class="Lazy-loading" style="width:170px"></span><img alt="" class="img-Lazy" src="/Portals/_default/ContentBuilder/minis-page/home05/homepage05-client05.png"></a>
                </div>
            </div>
        </div>
    </div>
    <style class="build-css" data-class="home05-clientlist">
        .home05-clientlist {
            box-shadow: 0 0 35px rgba(0, 0, 0, .2);
            border-radius: 3px;
            padding: 30px 50px;
            text-align: center;
            margin: 0px 0 15px;
            background-color: #FFF;
        }

        .home05-clientlist img {
            opacity: .5;
            -webkit-transition: all ease-in 300ms;
            transition: all ease-in 300ms;
            padding: 15px 0;
        }

        .home05-clientlist img:hover {
            opacity: 1;
        }
    </style>
    `
}, {
    'thumbnail': 'minis-section/home05/section-02.jpg',
    'category': '804',
    'html': `
    <div class="is-section is-box">
        <div class="is-boxes">
            <div class="is-box-centered">
                <div class="is-container layout-container mt-md-80">
                    <div class="row align-items-center">
                        <div class="col-md-12">
                            <div class="title-07 text-center mt-15">
                                <small>Efficient</small>
                                <h2>Professional Service</h2>
                            </div>
                            <p class="text-center m-auto" style="max-width: 730px;  line-height:1.7; font-size: 18px;">Quisque rutrum diam sit amet justo convallis vestibulum. Cras nec egestas odio. Ut at felis congue sapien accumsan tincidunt. </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="spacer height-60 d-none d-md-block">
                            </div>
                        </div>
                    </div>
                    <div class="row align-items-center">
                        <div class="col-md-6 pl-lg-60">
                            <div class="spacer height-20"></div>
                            <h4 class="mb-25"><span class="color-accent">01.</span> Website Design</h4>
                            <p class="size-18" style="line-height:1.7;">Vivamus sed rhoncus felis. Suspendisse in tincidunt lorem. Proin at velit sed arcu mollis accumsan. Vestibulum commodo dui non elit elementum malesuada metus commodo.</p>
                            <div class="spacer height-20"></div>
                        </div>
                        <div class="col-md-6 order-md-first">
                            <img alt="" style="position: absolute; left: -10%; top: 69%;" class="img-Lazy" src="/Portals/_default/ContentBuilder/minis-page/home05/homepage05-bg01.png"><img alt="" class="shadow-lg border-radius-3 img-Lazy" style="position: relative; transform: translateX(0px); opacity: 1;"
                                src="/Portals/_default/ContentBuilder/minis-page/home05/homepage05-img01.jpg">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="spacer height-80"></div>
                        </div>
                    </div>
                    <div class="row align-items-center">
                        <div class="col-md-6 pl-lg-60">
                            <div class="spacer height-20"></div>
                            <h4 class="mb-25"><span class="color-accent">02.</span> SEO Optimization</h4>
                            <p class="size-18" style="line-height:1.7;">Phasellus interdum sem vitae massa luctus placerat. Etiam at sapien ante. Nunc scelerisque nulla non ligula porta elementum urna placerat.</p>
                            <div class="spacer height-20"></div>
                        </div>
                        <div class="col-md-6 text-right mb-50" style="position: relative;"><img alt="" class="shadow-lg border-radius-3 img-Lazy" style="width: 83%; transform: translateX(0px); opacity: 1;" src="/Portals/_default/ContentBuilder/minis-page/home05/homepage05-img02.jpg"><img alt="" class="shadow-lg border-radius-3 img-Lazy" style="position: absolute; right: 38%; top: 47.5%; width: 54%; transform: translateX(0px); opacity: 1;" src="/Portals/_default/ContentBuilder/minis-page/home05/homepage05-img03.jpg"></div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="spacer height-80"></div>
                        </div>
                    </div>
                    <div class="row align-items-center">
                        <div class="col-md-6 pl-lg-60">
                            <div class="spacer height-20"></div>
                            <h4 class="mb-25"><span class="color-accent">03.</span> APP Design</h4>
                            <p class="size-18" style="line-height:1.7;">Mauris  sit amet eros mollis, ornare  quam non, eleifend orci. Etiam suscipit arcu justo. Phasellus congue nisl sapien, nec facilisis massa tempor at. </p>
                            <div class="spacer height-20"></div>
                        </div>
                        <div class="col-md-6 order-md-first" style="position: relative;">
                            <img alt="" class="shadow-lg border-radius-3 img-Lazy" style="width: 72%; transform: translateX(0px); opacity: 1;" src="/Portals/_default/ContentBuilder/minis-page/home05/homepage05-img04.jpg"><img alt="" class="shadow-lg border-radius-3 img-Lazy" style="position: absolute; left: 40.5%; top: 20%; width: 48%; transform: translateX(0px); opacity: 1;" src="/Portals/_default/ContentBuilder/minis-page/home05/homepage05-img05.jpg">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="spacer height-20"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style class="build-css" data-class="title-07">${csstemplate["title-07"]}</style>
    `
}, {
    'thumbnail': 'minis-section/home05/section-03.jpg',
    'category': '804',
    'html': `
    <div class="is-section is-box is-light-text">
        <div class="is-overlay">
            <div class="is-overlay-bg"
                style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/home05/homepage05-bg02.jpg&quot;);">
            </div>
        </div>
        <div class="is-boxes">
            <div class="is-box-centered">
                <div class="is-container layout-container pt-md-10 mb-md-80 pb-md-10">
                    <div class="row">
                        <div class="col-md-5">
                            <div class="title-07">
                                <small class="color-accent">Features</small>
                                <h3>Reasons Why You Should Choose Us</h3>
                            </div>
                        </div>
                        <div class="col-md-7" style="font-size: 18px;">
                            <p class="mb-25" style="font-size:20px; line-height:1.6; font-weight:600;">Our experienced developers can turn your brief ideas into powerful  features like a magic.</p>
                            <p class="size-18" style="line-height:1.7;">Donec vel augue eget felis vestibulum tempus. Suspendisse id metus ac mauris tempor sollicitudin. Curabitur luctus tincidunt elit, sit amet consequat metus rhoncus nec. Sed sit amet dolor ultrices.</p>
                        </div>
                    </div>
                    <div class="row mt-60 pt-80" style="border-top: 1px solid rgba(255, 255, 255, 0.5);">
                        <div class="col-md-1/5">
                            <div class="iconbox-04">
                                <div class="icon"><i class="sico lnr-hammer-wrench color-accent"></i></div>
                                <h6>Top-notch<br>Service</h6>
                            </div>
                        </div>
                        <div class="col-md-1/5">
                            <div class="iconbox-04">
                                <div class="icon"><i class="sico lnr-thumbs-up color-accent"></i></div>
                                <h6>Excellent<br>Technology</h6>
                            </div>
                        </div>
                        <div class="col-md-1/5">
                            <div class="iconbox-04">
                                <div class="icon"><i class="sico lnr-shield-check color-accent"></i></div>
                                <h6>2-factor<br>Authentication
                                </h6>
                            </div>
                        </div>
                        <div class="col-md-1/5">
                            <div class="iconbox-04">
                                <div class="icon"><i class="sico lnr-clipboard-check color-accent"></i></div>
                                <h6>Unlimited<br>Options</h6>
                            </div>
                        </div>
                        <div class="col-md-1/5">
                            <div class="iconbox-04">
                                <div class="icon"><i class="sico lnr-pencil-ruler2 color-accent"></i></div>
                                <h6>Sophisticated<br>Design</h6>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <style class="build-css" data-class="title-07">${csstemplate["title-07"]}</style>
    <style class="build-css" data-class="iconbox-04">${csstemplate["iconbox-04"]}</style>
    `
}, {
    'thumbnail': 'minis-section/home05/section-04.jpg',
    'category': '804',
    'html': `
    <div class="is-section is-box">
        <div class="is-boxes">
            <div class="is-box-centered">
                <div class="is-container layout-container">
                    <div class="row align-items-center">
                        <div class="col-md-12">
                            <div class="spacer height-60 d-none d-xl-block">
                            </div>
                        </div>
                    </div>
                    <div class="row align-items-center">
                        <div class="col-md-5">
                            <div class="spacer height-60 d-none d-lg-block"></div>
                            <div class="title-07 mt-10 mt-md-0">
                                <small class="color-accent">About us</small>
                                <h2>We Are Always Your Solid Backing!</h2>
                            </div>
                            <p style="line-height:1.7;" class="size-18 mb-20">Vestibulum a metus enim. Pellentesque orci dui, malesuada sit amet volutpat eget, rutrum id neque. Integer aliquam eget nisl id volutpat.</p>
                            <div class="spacer height-20"></div>
                            <p><a href="https://www.youtube.com/embed/AkcUoA2f-jU" title="How we work" class="button-03 is-lightbox" data-ilightbox="youtube"><i class="sico fas-play"></i>How we work</a></p>
                            <div class="spacer height-40"></div>
                        </div>
                        <div class="col-md-1"></div>
                        <div class="col-md-6 p" style="position: relative;"><img alt="" style="position: absolute; left: 24%; top: -19%;" class="img-Lazy"
                                src="/Portals/_default/ContentBuilder/minis-page/home05/homepage05-bg03.png"><img alt=""
                                style="position: absolute;left: 22%;top: 70%;" class="img-Lazy" src="/Portals/_default/ContentBuilder/minis-page/home05/homepage05-bg04.png"><img alt=""  class="shadow-lg img-Lazy" style="position: relative; transform: translateX(0px); opacity: 1;" src="/Portals/_default/ContentBuilder/minis-page/home05/homepage05-img06.jpg">
                        </div>
                    </div>
                    <div class="row align-items-center">
                        <div class="col-md-12">
                            <div class="spacer height-60 mb-15 d-none d-xl-block">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style class="build-css" data-class="button-03">${csstemplate["button-03"]}</style>
    `
}, {
    'thumbnail': 'minis-section/home05/section-05.jpg',
    'category': '804',
    'html': `
    <div class="is-section is-box is-bg-grey is-light-text is-align-center">
        <div class="is-overlay">
            <div class="is-overlay-bg"
                style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/home05/homepage05-bg05.jpg&quot;);">
            </div>
        </div>
        <div class="is-boxes">
            <div class="is-box-centered">
                <div class="is-container layout-container" style="max-width: 680px;">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="spacer height-20 mb-15">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <p style="font-size: 40px; line-height: 1; font-weight: 300;" class="mb-15">Looking For The Best Solution For A </p>
                            <p class="size-48 mb-0" style="font-weight: 700; line-height: 1.1;">Professional Website?</dipv>
                            <div class="spacer mb-10 height-40"></div>
                            <div><a href="#" title="contact us" class="button-01">CONTACT US</a></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="spacer height-20 mb-15">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style class="build-css" data-class="button-01">${csstemplate["button-01"]}</style>
    `
}, {
    'thumbnail': 'minis-section/home05/section-06.jpg',
    'category': '804',
    'html': `
    <div class="is-section is-box">
        <div class="is-boxes">
            <div class="is-box-centered">
                <div class="is-container layout-container pt-10 mb-40 mb-md-80">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="title-07 text-center">
                                <small class="color-accent">Transparent</small>
                                <h2>Affordable Pricing Plans</h2>
                            </div>
                            <p class="text-center m-auto" style="max-width: 730px; line-height:1.7; font-size: 18px;">Aliquam tincidunt, dolor a semper ultrices, lorem dui ornare nisi, sed congue velit nulla in eros semper elementum quam.</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="spacer height-60"></div>
                        </div>
                    </div>
                    ${getSnippetCode("price-05")}                    
                </div>
            </div>
        </div>
    </div>
    <style class="build-css" data-class="title-07">${csstemplate["title-07"]}</style>
    `
}, {
    'thumbnail': 'minis-section/home05/section-07.jpg',
    'category': '804',
    'html': `
    <div class="is-section is-box is-dark-text">
        <div class="is-overlay">
            <div class="is-overlay-bg"
                style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/home05/homepage05-bg06.jpg&quot;);">
            </div>
        </div>
        <div class="is-boxes">
            <div class="is-box-centered">
                <div class="is-container layout-container pt-md-10">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="title-07 text-center">
                                <small class="color-accent">Contact</small>
                                <h2>Get In Touch With Us</h2>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="spacer height-20"></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 m-auto" style="max-width: 770px;">                            
                            ${getSnippetCode("ajaxform-01")}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style class="build-css" data-class="title-07">${csstemplate["title-07"]}</style>
    `
}, {
    'thumbnail': 'minis-section/home05/section-08.jpg',
    'category': '804',
    'html': `
    <div class="is-section is-box">
        <div class="is-boxes">
            <div class="is-box-centered">
                <div class="is-container layout-container mt-md-40 mt-lg-80 pt-10 mb-40 mb-md-40 mb-lg-80">
                    <div class="row align-items-center">
                        <div class="col-md-4 pr-lg-40">
                            <div class="title-07">
                                <small class="color-accent">Testimonial</small>
                                <h3>What Are People Saying About Us</h3>
                            </div>
                            <p class="pt-5 size-18">Quisque cursus tortor lorem, vel finibus dolor varius non.</p>
                        </div>
                        <div class="col-md-8">
                        ${getSnippetCode("testimonials02")}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style class="build-css" data-class="title-07">${csstemplate["title-07"]}</style>
    `
}, {
    'thumbnail': 'minis-section/home05/footer.jpg',
    'category': '804',
    'html': `
    <div class="is-section is-box is-light-text">
        <div class="is-overlay">
            <div class="is-overlay-bg"
                style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/home05/homepage05-footer-bg.jpg&quot;);">
            </div>
        </div>
        <div class="is-boxes">
            <div class="is-box-centered">
                <div class="is-container layout-container pt-10 mb-30" dragwithouthandle="">
                    <div class="row text-center m-auto align-items-center" style="max-width: 770px;">
                        <div class="col-md-12">
                            <h2 class="mb-35 size-32">Are You Ready Now?</h2>
                            <p style="margin-bottom: 20px; color: rgb(147, 148, 154);" class="size-18">Donec vel nunc sit amet erat consectetur egestas. Maecenas ut nibh nulla. Donec eu rutrum enim, accumsan ultricies velit. Cras et eros vel tellus suscipit imperdiet.</p>
                            <p style="font-size: 20px; color: rgb(206, 206, 209);" class="mb-30">Phone: (845) 359-7777&nbsp; &nbsp; Email: service.simple@gmail.com</p>
                            <div><a href="#" title="contact us" class="button-01">CONTACT NOW</a></div>
                        </div>
                    </div>
                    <div class="row align-items-center">
                        <div class="col-md-12">
                            <div class="spacer height-80"></div>
                        </div>
                    </div>
                    <div class="row align-items-center pt-20 col-no-padding"
                        style="border-top: 1px solid rgba(255, 255, 255, 0.1);">
                        <div class="col-md text-center text-md-left">
                            <img src="/Portals/_default/ContentBuilder/minis-page/logo-white.png" alt="" class="img-Lazy mb-15 mb-md-0">
                        </div>
                        <div class="col-md">
                            <p style="color: rgb(168, 169, 174);" class="text-center mt-0 mb-15 mb-md-0"> @ 2019 by DNNGo Corp. All Rights Resevered</p>
                        </div>
                        <div class="col-md">
                            <div class="edit-box text-center text-md-right">
                                <a class="social-02" href="#"><i class="sico fab-twitter"></i></a>
                                <a class="social-02" href="#"><i class="sico fab-pinterest-p"></i></a>
                                <a class="social-02" href="#"><i class="sico fab-linkedin-in"></i></a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <style class="build-css" data-class="button-01">${csstemplate["button-01"]}</style>
    <style class="build-css" data-class="social-02">${csstemplate["social-02"]}</style>
    `
})

/*Home06*/

data_basic.designs.push({
    'thumbnail': 'minis-section/home06/banner.jpg',
    'category': '805',
    'html': `
    <div class="is-section is-light-text is-box homepage06-banner is-section-75 is-section-md-50">
        <div class="is-overlay">
            <div class="is-overlay-color"></div>
            <div class="is-overlay-content"></div>
            <div class="is-overlay-bg" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/home06/homepage06-banner.jpg&quot;);"></div>
        </div>
        <div class="is-boxes">
            <div class="is-box-centered">
                <div class="is-container container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="display" style="max-width: 585px;">
                                <div class="title-09">
                                    <small style="margin-bottom: 20px;">WELCOME MINIS</small>
                                    <h1 style="font-weight: 600;">Efficient Customer Service Will Melt Your Heart</h1>
                                </div>
                                <a class="button-04" href="#" style="margin-top: -5px;">LEARN MORE<i class="sico lnr-chevron-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style class="build-css" data-class="button-04">${csstemplate["button-04"]}</style>
    <style class="build-css" data-class="title-09">${csstemplate["title-09"]}</style>
    <style class="build-css" data-class="homepage06-banner">
        .homepage06-banner .title-09 small::before {
            width: 50px;
        }
    </style>
    `
}, {
    'thumbnail': 'minis-section/home06/section-01.jpg',
    'category': '805',
    'html': `
        <div class="is-section is-box homepage06-section01">
            <div class="is-boxes">
                <div class="is-box-centered">
                    <div class="is-container layout-container is-container-fluid m-auto pl-0 pr-0">
                    ${getSnippetCode("infobox-04")}
                    </div>
                </div>
            </div>
        </div>   
    `
}, {
    'thumbnail': 'minis-section/home06/section-02.jpg',
    'category': '805',
    'html': `
    <div class="is-section is-box homepage06-section02">
        <div class="is-boxes">
            <div class="is-box-centered">
                <div class="is-container layout-container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="spacer height-20"></div>
                        </div>
                    </div>
                    <div class="row align-items-center">
                        <div class="col-md-6">
                            <div class="display m-auto" style="max-width: 450px;">
                                <div class="title-09">
                                    <small>ABOUT US</small>
                                    <h3>Offering Unique Solutions For <span class="color-accent">You Business</span></h3>
                                </div>
                                <p>Suspendisse potenti. Nam porttitor gravida quam ac placerat. Nam faucibus, orci sed ultrices feugiat, turpis nisl sollicitudin erat, eget convallis metus neque eget sem. </p>
                                <div class="row" style="margin-top: 43px;">
                                    <div class="col-auto">
                                        <div class="">
                                            <img alt="" class="img-Lazy border-radius-circle" src="/Portals/_default/ContentBuilder/minis-page/home06/homepage06-section02-01.png">
                                        </div>
                                    </div>
                                    <div class="col d-flex justify-content-center flex-column edit-box"
                                        style="font-weight: 600;">
                                        <p class="size-18 mb-5" style="color: rgb(66, 66, 66);">Elizabeth Green</p>
                                        <span class="size-16 color-accent">Designer</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 text-center">
                            <div class="mt-60 mt-md-0 text-center">
                                <img alt="" class="img-Lazy" src="/Portals/_default/ContentBuilder/minis-page/home06/homepage06-section02-02.png">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="spacer height-20"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style class="build-css" data-class="title-09">${csstemplate["title-09"]}</style>
    `
}, {
    'thumbnail': 'minis-section/home06/section-03.jpg',
    'category': '805',
    'html': `
    <div class="is-section is-section-auto homepage06-section03">
        <div class="is-boxes">
            <div class="is-box-6 is-box-img is-box">
                <div class="is-overlay">
                    <div class="is-overlay-content"></div>
                    <div class="is-overlay-bg" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/home06/homepage06-section03-01.jpg&quot;);">
                    </div>
                </div>
                <div class="is-boxes"></div>
            </div>
            <div class="is-box-6 is-box" style="background-color: rgb(241, 243, 252);">
                <div class="is-boxes">
                    <div class="is-box-centered">
                        <div class="is-container container-fluid" style="max-width: 500px;">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="spacer height-20 mt-5 d-none d-md-block">
                                    </div>
                                    <div class="display edit-box">
                                        <div class="title-09">
                                            <small>HOW WE WORK</small>
                                            <h3>Get To know The Secrets Of <span class="color-accent">Our Success</span></h3>
                                        </div>
                                        <p>Nam faucibus, orci sed ultrices feugiat turpis nisl sollicitudin erat, eget convallis metus neque eget sem.</p>
                                        <div class="mt-40"></div>                                                                            
                                        ${getSnippetCode("counter-03")}
                                        <div class="mb-30"></div>    
                                        <a class="button-05 is-lightbox" data-ilightbox="youtube" href="https://www.youtube.com/embed/AkcUoA2f-jU" data-type="iframe" data-options="width: '1280',height: '720',skin:'dark'" title=""><span><i class="sico fas-play"></i></span>WATCH VIDEO</a>
                                    </div>
                                    <div class="spacer height-20 mb-10 d-none d-md-block">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style class="build-css" data-class="title-09">${csstemplate["title-09"]}</style>
    <style class="build-css" data-class="button-05">${csstemplate["button-05"]}</style>
    `
}, {
    'thumbnail': 'minis-section/home06/section-04.jpg',
    'category': '805',
    'html': `<div class="is-section is-box">
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container mb-40 mb-md-0 mb-xl-80 mt-md-50 mt-lg-40 mt-xl-80 pt-xl-20" dragwithouthandle="">
                <div class="row">
                    <div class="col-md-12">
                        <div class="spacer height-60 mb-10 d-none d-lg-block">
                        </div>
                        <div class="pt-10 d-block d-lg-none">
                        </div>
                    </div>
                </div>
                <div class="row align-items-center">
                    <div class="col-md-4 pr-lg-80">
                        <div class="title-09" style="margin-bottom: 40px;">
                            <small>ABOUT US</small>
                            <h3>Our Professional Team Will Always
                                <span class="color-accent">Be Your Solid</span>
                            </h3>
                        </div>
                        <p>Vivamus ac euismod nisi. Donec ultricies tellus sed purus tincidunt, in efficitur nunc venenatis. Donec laoreet quam ut vestibulum pharetra. Vestibulum euismod ipsum id sapien tempus.</p>
                    </div>
                    <div class="col-md-5">
                        <img class="img-Lazy" src="/Portals/_default/ContentBuilder/minis-page/home06/homepage06-section04-bg.png" alt="Images" style="max-width: 120%; position: absolute;  top: -19%;right:4%">
                        <img class="border-radius-circle img-Lazy" style="position: relative;" src="/Portals/_default/ContentBuilder/minis-page/about-us/aboutus-pic01.jpg" alt="Images">
                    </div>
                    <div class="col-md-3">
                        <div class="spacer height-40 d-md-none">
                        </div>
                        <div class="iconbox-05">
                            <div class="icon color-accent">
                                <i class="sico lnr-magic-wand">
                                    <svg>
                                        <use xlink:href="#lnr-magic-wand">
                                        </use>
                                    </svg>
                                </i>
                            </div>
                            <h6 class="size-18">All Options Are Created With Innovative Ideas</h6>
                        </div>
                        <div class="iconbox-05">
                            <div class="icon color-accent">
                                <i class="sico lnr-desktop">
                                    <svg>
                                        <use xlink:href="#lnr-desktop">
                                        </use>
                                    </svg>
                                </i>
                            </div>
                            <h6 class="size-18">Responsive Design Will Work Well On Any Devices</h6>
                        </div>
                        <div class="iconbox-05">
                            <div class="icon color-accent">
                                <i class="sico lnr-microscope">
                                    <svg>
                                        <use xlink:href="#lnr-microscope">
                                        </use>
                                    </svg>
                                </i>
                            </div>
                            <h6 class="size-18">Retina Ready Design Guarantee A Wonderful Visual Feast</h6>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="spacer mb-5 d-none d-md-block height-80">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    `
}, {
    'thumbnail': 'minis-section/home06/section-05.jpg',
    'category': '805',
    'html': `
    <div class="is-section is-box is-light-text homepage06-section05">
        <div class="is-overlay">
            <div class="is-overlay-bg" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/home06/homepage06-section05-bg.jpg&quot;);">
            </div>
            <img alt="" src="/Portals/_default/ContentBuilder/minis-page/home06/homepage06-section05-mask.png" style="position: absolute;bottom: 0;">
        </div>
        <div class="is-boxes">
            <div class="is-box-centered">
                <div class="is-container layout-container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="spacer height-20 mt-5">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="title-09">
                                <small>OUR TECHNOLOGY</small>
                                <h3>Greatly Boost Your Business With Advanced Skills</h3>
                            </div>
                            ${getSnippetCode("progressbar02")}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="spacer height-160">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style class="build-css" data-class="title-09">${csstemplate["title-09"]}</style>
    `
}, {
    'thumbnail': 'minis-section/home06/section-06.jpg',
    'category': '805',
    'html': `
    <div class="is-section is-box homepage06-section06">
        <div class="is-boxes">
            <div class="is-box-centered">
                <div class="is-container layout-container">
                    <div class="row align-items-center">
                        <div class="col-md-6">
                            <div class="title-09" style="max-width: 400px;">
                                <small>GET IN TOUCH</small>
                                <h3>Ready To Get Started? <span class="color-accent">Contact Us!</span></h3>
                            </div>
                            <p style="max-width: 500px;">Feel free to contact us via following ways, we will try our best to answer all questions within 24 hours</p>
                            <div class="spacer height-20 mb-5">
                            </div>
                            ${getSnippetCode("iconbox-06")}
                        </div>

                        <div class="col-md-6">
                            <h4>Leave A Message</h4>
                            ${getSnippetCode("ajaxform-07")}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style class="build-css" data-class="title-09">${csstemplate["title-09"]}</style>
    `
}, {
    'thumbnail': 'minis-section/home06/section-07.jpg',
    'category': '805',
    'html': `
    <div class="is-section is-box homepage06-section07 is-align-center" style="border-top: 1px solid #e2e2e2">
        <div class="is-boxes">
            <div class="is-box-centered">
                <div class="is-container layout-container is-container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="spacer mt-10"></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="title-14">
                                <small>LONG-TERM PARTNER</small>
                                <h2>Thousands Of Clients Love Us
                                </h2>
                            </div>
                            <div class="client-box">
                                <div class="pic"><img class="img-Lazy" src="/Portals/_default/ContentBuilder/minis-page/home06/homepage06-section07-map.png" alt="map">
                                </div>
                                <div class="client-list">
                                    <div class="item"><a href="#"><img class="img-Lazy" src="/Portals/_default/ContentBuilder/minis-page/home06/homepage06-section07-01.png" alt="logo"></a>
                                    </div>
                                    <div class="item"><a href="#"><img class="img-Lazy" src="/Portals/_default/ContentBuilder/minis-page/home06/homepage06-section07-02.png" alt="logo"></a>
                                    </div>
                                    <div class="item"><a href="#"><img class="img-Lazy" src="/Portals/_default/ContentBuilder/minis-page/home06/homepage06-section07-03.png" alt="logo"></a>
                                    </div>
                                    <div class="item"><a href="#"><img class="img-Lazy" src="/Portals/_default/ContentBuilder/minis-page/home06/homepage06-section07-04.png" alt="logo"></a>
                                    </div>
                                    <div class="item"><a href="#"><img class="img-Lazy" src="/Portals/_default/ContentBuilder/minis-page/home06/homepage06-section07-05.png" alt="logo"></a>
                                    </div>
                                    <div class="item"><a href="#"><img class="img-Lazy" src="/Portals/_default/ContentBuilder/minis-page/home06/homepage06-section07-06.png" alt="logo"></a>
                                    </div>
                                    <div class="item"><a href="#"><img class="img-Lazy" src="/Portals/_default/ContentBuilder/minis-page/home06/homepage06-section07-07.png" alt="logo"></a>
                                    </div>
                                    <div class="item"><a href="#"><img class="img-Lazy" src="/Portals/_default/ContentBuilder/minis-page/home06/homepage06-section07-08.png" alt="logo"></a>
                                    </div>
                                    <div class="item"><a href="#"><img class="img-Lazy" src="/Portals/_default/ContentBuilder/minis-page/home06/homepage06-section07-09.png" alt="logo"></a>
                                    </div>
                                    <div class="item"><a href="#"><img class="img-Lazy" src="/Portals/_default/ContentBuilder/minis-page/home06/homepage06-section07-10.png" alt="logo"></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style class="build-css" data-class="title-14">${csstemplate["title-14"]}</style>
    <style class="build-css" data-class="homepage06-section07">
        .homepage06-section07 {
            position: relative;
            text-align: center;
        }
        .homepage06-section07 .client-box {
            position: relative;
            display: inline-block;
        }
        .homepage06-section07 .client-box .client-list {
            position: absolute;
            display: block;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
        }
        .homepage06-section07 .client-box .client-list .item {
            position: absolute;
            display: none;
        }
        .homepage06-section07 .client-box .client-list .item:nth-child(1) {
            left: 21.15942%;
            top: 12.850812%;
        }
        .homepage06-section07 .client-box .client-list .item:nth-child(2) {
            left: 60.724638%;
            top: 15.361891%;
        }
        .homepage06-section07 .client-box .client-list .item:nth-child(3) {
            left: 82.753623%;
            top: 26.144756%;
        }
        .homepage06-section07 .client-box .client-list .item:nth-child(4) {
            left: 37.173913%;
            top: 30.576071%;
        }
        .homepage06-section07 .client-box .client-list .item:nth-child(5) {
            left: 7.608696%;
            top: 34.121123%;
        }
        .homepage06-section07 .client-box .client-list .item:nth-child(6) {
            left: 70.144928%;
            top: 41.802068%;
        }
        .homepage06-section07 .client-box .client-list .item:nth-child(7) {
            left: 56.956522%;
            top: 52.584934%;
        }
        .homepage06-section07 .client-box .client-list .item:nth-child(8) {
            left: 41.376812%;
            top: 61.447563%;
        }
        .homepage06-section07 .client-box .client-list .item:nth-child(9) {
            left: 27.028986%;
            top: 71.935007%;
        }
        .homepage06-section07 .client-box .client-list .item:nth-child(10) {
            left: 77.608696%;
            top: 73.707533%;
        }
        .homepage06-section07 .client-box .client-list .item:nth-child(-n+10) {
            display: inline-block;
        }
        @media (min-width: 768px) and (max-width: 1199px) {
            .homepage06-section07 .client-box .client-list .item img {
                max-width: calc(50% + (100 - 50) * ((100vw - 768px) / (1199 - 768)))
            }
        }
        @media only screen and (max-width: 767px) {
            .homepage06-section07 .client-box .client-list {
                display: inline-flex;
                flex-wrap: wrap;
                justify-content: center;
                align-items: center;
            }
            .homepage06-section07 .client-box .client-list .item {
                position: relative;
                left: 0 !important;
                top: 0 !important;
                width: calc(100% / 5);
            }
            .homepage06-section07 .client-box .client-list .item img {
                max-width: 62px;
            }
        }
    </style>
    `
}, {
    'thumbnail': 'minis-section/home06/footer.jpg',
    'category': '805',
    'html': `
    <div class="is-section is-box is-light-text homepage06-footer">
        <div class="is-overlay">
            <div class="is-overlay-bg" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/home06/homepage06-footer-bg.jpg&quot;);"></div>
            <img alt=""  src="/Portals/_default/ContentBuilder/minis-page/home06/homepage06-footer-mask.png" style="position: relative;vertical-align: top;">
        </div>
        <div class="is-boxes">
            <div class="is-box-centered">
                <div class="is-container layout-container mb-30">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="spacer height-60 mt-10">
                            </div>
                        </div>
                    </div>
                    <div class="row text-center m-auto align-items-center" style="max-width: 770px;">
                        <div class="col-md-12">
                            <h2 class="mb-35">Subscribe To Receive Updates</h2>
                            <p class="size-18">Maecenas est tellus, faucibus sit amet euismod in, tincidunt in urna. Pellentesque nec faucibus lectus, ac maximus risus ultrices lacus.</p>
                        </div>
                        <div class="col-md-12" style="max-width: 430px; margin: 26px auto 4px;">
                            ${getSnippetCode("ajaxform-04")}
                        </div>
                    </div>
                    <div class="row align-items-center">
                        <div class="col-md-12">
                            <div class="spacer height-80"></div>
                        </div>
                    </div>
                    <div class="row align-items-center pt-20 pb-70" style="border-top: 1px solid rgba(255, 255, 255, 0.1);">
                        <div class="col-md text-center text-md-left"><img src="/Portals/_default/ContentBuilder/minis-page/logo-white.png" alt="" class="mb-15 mb-md-0 img-Lazy">
                        </div>
                        <div class="col-md">
                            <p style="opacity: 0.5;" class="text-center mb-15 mb-md-0">@ 2019 by DNNGo Corp. All Rights Resevered</p>
                        </div>
                        <div class="col-md">
                            <div class="edit-box text-center text-md-right">
                                <a class="social-02" href="#"><i class="sico fab-twitter"></i></a>
                                <a class="social-02" href="#"><i class="sico fab-pinterest-p"></i></a>
                                <a class="social-02" href="#"><i class="sico fab-linkedin-in"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style class="build-css" data-class="title-08">${csstemplate["title-08"]}</style>
    <style class="build-css" data-class="social-02">${csstemplate["social-02"]}</style>
    `
})


/*Home07*/

data_basic.designs.push({
        'thumbnail': 'minis-section/home07/banner.jpg',
        'category': '806',
        'html': `
    <div class="is-section is-section-100 is-section-lg-50 is-light-text is-box is-bg-grey edge-y-4">
        <div class="is-overlay">
            <div class="is-overlay-content"></div>
            <div class="is-overlay-bg" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/home07/homepage07-banner.jpg&quot;);"></div>
        </div>
        <div class="is-boxes">
            <div class="is-box-centered edge-y-4">
                <div class="is-container container-fluid is-content-left edge-x-3 is-container-fluid" dragwithouthandle="">
                    <div class="row">
                        <div class="col-md-6" style="max-width: 620px;">
                            <div class="spacer height-40"></div>
                            <h1 class="size-60" style="font-weight: 600;">Your business deserves a wonderful <span class="color-accent">business</span> theme</h1>
                            <div class="spacer height-40 mb-5"></div>
                            <div><a href="#" title="GET STARTED" class="button-01 mb-10 mr-20">GET STARTED</a><a href="#" title="WATCH VIDEO" class="button-06 mb-10">WATCH VIDEO <i class="sico lnr-arrow-right"></i></a></div>
                            <div class="spacer height-40"></div>
                        </div>
                        <div class="col-md-6">
                            <div class="homepage07-banner-img">
                                <div class="spacer pt-80 pb-80 d-block d-md-none ">
                                </div>
                                <div class="img-01"><img class="img-Lazy" alt="" src="/Portals/_default/ContentBuilder/minis-page/home07/homepage07-banner-img02.png" style="margin-top: 12%; margin-left: 20%;"></div>
                                <div class="img-02"><img class="img-Lazy" src="/Portals/_default/ContentBuilder/minis-page/home07/homepage07-banner-img01.png" alt="" style="margin-top: 19%; margin-left: -6%;">
                                </div>
                            </div>                            
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="spacer height-20"></div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
    <style class="build-css" data-class="button-01">${csstemplate["button-01"]}</style>
    <style class="build-css" data-class="button-06">${csstemplate["button-06"]}</style>
    <style class="build-css"  data-class="homepage07-banner-img">
        .homepage07-banner-img .img-01,
        .homepage07-banner-img .img-02 {
            position: absolute;
            top: 50%;
            transform: translateY(-50%);
            width: 114%;
        }
    </style>
    `
    }, {
        'thumbnail': 'minis-section/home07/section-01.jpg',
        'category': '806',
        'html': `
    <div class="is-section is-box bg-accent is-light-text">
        <div class="is-boxes">
            <div class="is-box-centered">
                <div class="is-container layout-container m-auto home07-section01 pt-65 pb-45" dragwithouthandle="">
                    <div class="row align-items-center">
                        <div class="col-md-4">
                            <span style="font-size:18px;" data-keep-font-size="">We are coming</span>
                            <h3>Please stay tuned</h3>
                        </div>
                        <div class="col-md-8">${getSnippetCode("countdown-01")}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style class="build-css"  data-class="home07-section01">
        .home07-section01::before {
            content: "";
            height: 57px;
            border-left: 1px solid rgba(255, 255, 255, .9);
            position: absolute;
            top: 0;
        }
        @media only screen and (max-width: 767px) {
            .home07-section01 {
                text-align: center
            }

            .home07-section01::before {
                left: 50%
            }
        }
    </style>
    `
    }, {
        'thumbnail': 'minis-section/home07/section-02.jpg',
        'category': '806',
        'html': `
    <div class="is-section is-box is-bg-light is-dark-text">
        <div class="is-boxes">
            <div class="is-box-centered">
                <div class="is-container layout-container" dragwithouthandle="">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="spacer height-20"></div>
                        </div>
                    </div>
                    <div class="row align-items-center">
                        <div class="col-md-6"><img class="img-Lazy" src="/Portals/_default/ContentBuilder/minis-page/home07/homepage07-img01.jpg" style="transform: translateX(0px); opacity: 1;" alt=""></div>
                        <div class="col-md-6">
                            <h3 style="line-height: 44px;" data-keep-line-height="">We Are a Professional IT Company <span class="color-accent">For Your Business</span></h3>
                            <p class="pt-15">Morbi feugiat, nibh nec laoreet mollis, augue mi accu msan velit, ac scelerisque turpis purus ut odio. Suspendisse ut tristique turpis.
                            </p>
                            <div class="spacer height-20 mb-5">
                            </div>
                            ${getSnippetCode("iconbox-08")}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="spacer height-20 d-none d-md-block">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    `
    }, {
        'thumbnail': 'minis-section/home07/section-03.jpg',
        'category': '806',
        'html': `
    <div class="is-section is-box is-bg-grey is-section-auto">
        <div class="is-overlay">
            <div class="is-overlay-bg" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/home07/homepage07-bg01.jpg&quot;);"></div>
        </div>
        <div class="is-boxes">
            <div class="is-box-centered">
                <div class="is-container layout-container pt-10 mb-40 mb-md-80 pb-15" dragwithouthandle="">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="title-07 text-center">
                                <small>About Our Company</small>
                                <h3>What We Specialize In</h3>
                            </div>
                            <p class="m-auto text-center" style="max-width: 750px; color:#666;">Morbi feugiat, nibh nec laoreet molis augue mi accumsan velitacscele. risque turpis purus ut odio. Donec ut massa est. Sed congue lorem id lorem maximus ultricies.</p>
                            <div class="spacer height-60"></div>
                        </div>
                    </div>
                    ${getSnippetCode("iconbox-07")}
                </div>
            </div>
        </div>
    </div>
    <style class="build-css" data-class="title-07">${csstemplate["title-07"]}</style>
    `
    }, {
        'thumbnail': 'minis-section/home07/section-04.jpg',
        'category': '806',
        'html': `
    <div class="is-section is-box">
        <div class="is-boxes">
            <div class="is-box-centered">
                <div class="is-container layout-container" dragwithouthandle="">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="d-block d-md-none mb-5">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="spacer height-20"></div>
                        </div>
                    </div>
                    <div class="row align-items-center">
                        <div class="col-md-6 pr-md-80">
                            <h3 style="line-height: 44px;">This Amazing Theme Makes For <span class="color-accent">The Business Growth.</span></h3>
                            <div class="spacer height-20 mb-10">
                            </div>
                            <div class="blockquotes-01 edit-box mr-30">
                                <blockquote>In maximus risus sed lacus feugiat tempus. In sit amet est porttitor tortor vehicula lobortis. Phasellus at venenatis urna. Duis suscipit, tellus eu volutpat tincidunt, velit diam hendrerit mauris, sed vestibulum.&nbsp;
                                </blockquote>
                                <div class="spacer height-20 mb-10">
                                </div>
                                <div class="pic"><img class="img-Lazy" src="/Portals/_default/ContentBuilder/minis-page/home07/homepage07-img02.jpg" alt=""></div>
                            </div>
                            
                            <div class="spacer pb-80 d-block d-md-none">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="home07-section03-img" style="margin: 26% auto 10%;max-width: 79vw;">
                                <img class="img-Lazy" src="/Portals/_default/ContentBuilder/minis-page/home07/homepage07-img04.jpg" alt="" style="transform: translateX(-34px); "><img class="img-Lazy" src="/Portals/_default/ContentBuilder/minis-page/home07/homepage07-img03.jpg" style="position: absolute;top: -37%;left: -13%;width: 52%;" alt=""><img class="img-Lazy" src="/Portals/_default/ContentBuilder/minis-page/home07/homepage07-img05.jpg" style="position: absolute; bottom: -16%; right: 9%; width: 39%; transform: translateX(50px); " alt="">
                            </div>   
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="spacer height-20"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>  
    <style class="build-css" data-class="blockquotes-01">
    .blockquotes-01 {
        position: relative;
        z-index: 1;
    }
    .blockquotes-01::after {
        content: '';
        background: url("data:image/svg+xml;utf8,<svg _tmplitem='2' width='200' height='200' xmlns='http://www.w3.org/2000/svg'><path _tmplitem='2' d='M56.856,58.019 c-2.686,2.145-5.552,3.754-8.594,4.826c-3.046,1.073-6.538,1.609-10.474,1.609c-8.417,0-15.308-2.547-20.679-7.642 c-5.371-5.094-8.057-11.663-8.057-19.706c0-8.219,2.773-15.148,8.325-20.779c5.547-5.63,12.53-8.445,20.947-8.445 10.024,0,18.215,4.021,24.573,12.065c6.353,8.043,9.534,18.68,9.534,31.905c0,16.623-7.297,33.782-21.887,51.478 c-14.594,17.695-26.184,26.543-34.778,26.543c-1.435,0-2.644-0.489-3.625-1.475c-0.986-0.979-1.477-2.187-1.477-3.619 c0-1.784,3.042-4.733,9.131-8.848C41.456,100.741,53.81,81.437,56.856,58.019z M148.701,58.019 c-2.686,2.145-5.551,3.754-8.594,4.826c-3.047,1.073-6.537,1.609-10.473,1.609c-8.594,0-15.623-2.589-21.082-7.775 c-5.463-5.182-8.191-11.705-8.191-19.572c0-8.219,2.773-15.148,8.326-20.779c5.547-5.63,12.529-8.445,20.947-8.445 c10.023,0,18.303,4.068,24.84,12.199c6.535,8.136,9.803,18.726,9.803,31.771c0,16.623-7.385,33.782-22.156,51.478 c-14.77,17.695-26.451,26.543-35.045,26.543c-1.436,0-2.645-0.489-3.627-1.475c-0.984-0.979-1.477-2.187-1.477-3.619 c0-1.784,3.043-4.733,9.131-8.848C133.125,100.741,145.654,81.437,148.701,58.019z' stroke-width='15' fill='%23f6f6f6'  stroke='%23f6f6f6'></path></svg>");
        background-size: 203px;
        background-repeat: no-repeat;
        position: absolute;
        right: -36px;
        bottom: 35px;
        width: 180px;
        height: 153px;
        z-index: -1;
    }

    .blockquotes-01 blockquote {
        padding: 0;
        margin: 0;
        border: none;
        font-size: 18px;
        color: #444444;
        font-weight: 600;
        line-height: 1.6
    }
</style>
<style class="build-css" data-class="home07-section03-img">
    .home07-section03-img {

        text-align: right;
        max-width: 79vw;
        position: relative;
        margin: 21% auto 10%;
    }
    .home07-section03-img img {
        border: 8px solid #ffffff;
        box-shadow: 0 0 30px rgba(0, 0, 0, 0.3);
    }
    </style>   
    `
    }, {
        'thumbnail': 'minis-section/home07/section-05.jpg',
        'category': '806',
        'html': `
    <div class="is-section is-box is-bg-grey is-light-text">
        <div class="is-overlay">
            <div class="is-overlay-bg"
                style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/home07/homepage07-bg02.jpg&quot;);">
            </div>
        </div>
        <div class="is-boxes">
            <div class="is-box-centered">
                <div class="is-container layout-container pt-10 mb-15 mb-md-50" dragwithouthandle="">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="title-07 text-center">
                                <small>Work Process</small>
                                <h3>How Do We Work On Project
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <p class="text-center m-auto" style="max-width: 750px; color: rgb(227, 227, 227);">Morbi feugiat, nibh nec laoreet molis augue mi accumsan velitacscele. risque turpis purus ut odio. Donec ut massa est. Sed congue lorem id lorem maximus ultricies.</p>
                            <div class="spacer height-60 mb-15">
                            </div>
                        </div>
                    </div>
                    ${getSnippetCode("infobox-05")}
                </div>
            </div>
        </div>
    </div>
    <style class="build-css" data-class="title-07">${csstemplate["title-07"]}</style>
    `
    }, {
        'thumbnail': 'minis-section/home07/section-06.jpg',
        'category': '806',
        'html': `
    <div class="is-section is-box">
        <div class="is-boxes">
            <div class="is-box-centered">
                <div class="is-container layout-container mb-35 mb-md-75 pb-lg-10" dragwithouthandle="">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="spacer height-20 d-none d-md-block">
                            </div>
                        </div>
                    </div>
                    <div class="row align-items-center">
                        <div class="col-md-5">
                            <h3 style="line-height: 44px;" data-keep-line-height="">Please Meet Our Passionate <span class="color-accent">Team Members</span></h3>
                            <div class="spacer height-20 mb-10">
                            </div>
                            <p class="size-18">Pellentesque feugiat feugiat mattis. In gravida tortor ut nibh suscipit commodo. Vestibulum ac luctus felis. Nulla nec neque maximus, hendrerit. </p>
                            <div class="spacer height-20 mb-5">
                            </div>
                            <div><a href="#" title="learn more" class="button-01 mt-10">CONTACT US</a></div>
                            <div class="spacer height-40"></div>
                        </div>
                        <div class="col-md-7 pl-lg-40">
                        ${getSnippetCode("infobox-06")}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style class="build-css" data-class="button-01">${csstemplate["button-01"]}</style>
    `
    }, {
        'thumbnail': 'minis-section/home07/footer.jpg',
        'category': '806',
        'html': `
        <div class="is-section is-box is-align-center current-cover-bg is-light-text" style="background: url(&quot;/Portals/_default/ContentBuilder/minis-page/home07/homepage07-footer-bg.jpg&quot;) repeat-x left top; background-size: contain">
        <div class="is-boxes">
            <div class="is-box-centered">
                <div class="is-container layout-container mb-40" dragwithouthandle="">
                    <div class="row">
                        <div class="col-md-12"><img class="img-Lazy" alt=""  src="/Portals/_default/ContentBuilder/minis-page/home07/homepage07-footer-logo.png">
                            <div class="spacer height-20"></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="spacer height-20 mb-10">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <p class="m-auto" style="font-size:18px; max-width: 670px; color: rgb(192, 202, 209);">Proin quis neque faucibus, sodales nisl sit amet, finibus ipsum. Integer id sagittis mauris. Pellentesque habitant morbi tristique senectus et netus.</p>
                            <div class="spacer height-40"></div>
                            <div class="edit-box">
                                <a class="social-04" href="#"><i class="sico fab-facebook-f"></i></a><a class="social-04"
                                    href="#"><i class="sico fab-twitter"></i></a>
                                <a class="social-04" href="#"><i class="sico fab-instagram"></i></a><a class="social-04"
                                    href="#"><i class="sico fab-pinterest-p"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div style="background-color: rgba(0,0,0,.12)">
                    <div class="is-container layout-container mt-0 mb-0 pt-30 pb-30" dragwithouthandle="">
                        <div class="row m-auto" style="max-width: 830px;">
                            <div class="col-md">
                                <p class="mb-0" style="color: rgb(133, 160, 175);">Phone: <a href="tel: (845) 359-7777" style=" color: currentColor; ">(845) 359-7777</a></p>
                            </div>
                            <div class="col-md">
                                <p class="mb-0" style="color: rgb(133, 160, 175);">Email: dnnskindev@gmail.com</p>
                            </div>
                            <div class="col-md">
                                <p class="mb-0" style="color: rgb(133, 160, 175);">Skype: dnngo-linda</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="is-container layout-container mt-30 mb-30" dragwithouthandle="">
                    <div class="row">
                        <div class="col-md-12">
                            <p class="home07-footer-color mt-o mb-0">@ 2019 by DNNGo Corp &nbsp; | &nbsp; <a href="#" title="Privacy Statement">Privacy Statement</a> &nbsp; | &nbsp; <a href="#" title="Privacy Statement">Terms of Use</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style class="build-css" data-class="social-04">${csstemplate["social-04"]}</style>
    <style class="build-css" data-class="home07-footer-color">
        p.home07-footer-color,
        .home07-footer-color,
        .home07-footer-color a:link,
        .home07-footer-color a:focus,
        .home07-footer-color a:active {
            color: #74968e;
        }
        .home07-footer-color a:hover {
            color: var(--accent-color);
        }
    </style>    
    `
    }

)

/*Home08*/

data_basic.designs.push({
    'thumbnail': 'minis-section/home08/banner.jpg',
    'category': '807',
    'html': `
    <div class="is-section is-section-100 is-section-md-50 is-light-text is-box is-align-center">
        <div class="is-overlay">
            <div class="is-overlay-content"></div>
            <div class="is-overlay-bg"
                style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/home08/homepage08-banner.jpg&quot;);">
            </div>
        </div>
        <div class="is-boxes">
            <div class="is-box-centered">
                <div class="is-container container-fluid is-container-fluid" dragwithouthandle="">
                    <div class="row">
                        <div class="col-md-12">
                            <h1 class="home08-banner-title01" style="font-family: Poppins;">CREATIVE</h1>
                            <h3 class="home08-banner-title02">PHOTOGRAPHY&nbsp;</h3>                     
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <link href="https://fonts.googleapis.com/css?family=Poppins" type="text/css" rel="stylesheet" />
    <style class="build-css" data-class="home08-banner-title01">
    .home08-banner-title01 {
        font-size: 180px;
        font-weight: 900;
        letter-spacing: 0.3166em;
    }
    .home08-banner-title02 {
        letter-spacing: 1.4em;
        font-weight: 900;
    }

    @media(max-width:1199px) {
        .home08-banner-title01 {
            font-size: 120px;
        }

    }
    @media(max-width:991px) {
        .home08-banner-title01 {
            font-size: 100px;
        }

        .home08-banner-title02 {
            letter-spacing: 1em;

        }
    }
    @media(max-width:767px) {
        .home08-banner-title01 {
            font-size: 46px;
        }

        .home08-banner-title02 {
            letter-spacing: .2em;
            font-weight: 400;
            font-size: 22px;
        }
    }
    </style>
    `
}, {
    'thumbnail': 'minis-section/home08/section-01.jpg',
    'category': '807',
    'html': `
    <div class="is-section is-box">
        <div class="is-boxes">
            <div class="is-box-centered">
                <div class="is-container layout-container" dragwithouthandle="">
                    <div class="row align-items-center">
                        <div class="col-md-6 pr-lg-60"><img class="img-Lazy" alt="" src="/Portals/_default/ContentBuilder/minis-page/home08/homepage08-bg01.png" style="position: absolute;top: 8%;left: 6%;z-index: 0;max-width: 78%;"><img alt="" src="/Portals/_default/ContentBuilder/minis-page/home08/homepage08-img01.jpg" style="position: relative;z-index: 2;max-width: 73%;" class="shadow-sm img-Lazy"><img alt="" src="/Portals/_default/ContentBuilder/minis-page/home08/homepage08-img02.jpg" style="float: right;position: relative;margin-top: -50%;z-index: 1;max-width: 61%;" class="shadow-sm img-Lazy">
                        </div>
                        <div class="col-md-6">
                            <div class="spacer mb-40  d-block d-md-none">
                            </div>
                            <div class="title-11">
                                <h3 style="max-width:500px;">We Provide Website Solutions&nbsp; With Powerful Technology</h3>
                            </div>
                            <div class="home08-section-info">
                                <span class="number color-accent">25</span>
                                <p class="color-accent">years of creative experience in digital marketing is definitely worth of your rust in us.</p>
                            </div>
                            <p>Praesent ornare, orci quis euismod semper, nisi justo tempor eros, eget blandit nunc mauris sit amet risus. Maecenas elit lorem, blandit non massa sed, tristique cursus lacus.&nbsp;</p>
                            <div class="pt-15"><a href="#" title="learn more" class="button-01 mt-10">LEARN MORE</a></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="spacer height-20"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style class="build-css" data-class="title-11">${csstemplate["title-11"]}</style>
    <style class="build-css" data-class="button-01">${csstemplate["button-01"]}</style>
    <style class="build-css" data-class="home08-section-info">
    .home08-section-info {
        font-size: 18px;
        font-weight: 600;
        color: var(--accent-color);
        margin-bottom: 34px;
        line-height: 1.7;
    }

    .home08-section-info .number {
        font-size: 64px;
        line-height: 1.3;
        font-weight: 700;
        margin-right: 15px;
        float: left;
        margin-top: -8px;
    }
    </style>
    `
}, {
    'thumbnail': 'minis-section/home08/section-02.jpg',
    'category': '807',
    'html': `
    <div class="is-section is-box">
        <div class="is-overlay">
            <div class="is-overlay-bg" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/home08/homepage08-bg02.jpg&quot;);"></div>
        </div>
        <div class="is-boxes">
            <div class="is-box-centered">
                <div class="is-container layout-container mt-80 pt-5 mb-35" dragwithouthandle="">
                ${getSnippetCode("counter-09")}
                </div>
            </div>
        </div>
    </div>
    `
}, {
    'thumbnail': 'minis-section/home08/section-03.jpg',
    'category': '807',
    'html': `
    <div class="is-section is-box is-align-center">
        <div class="is-boxes">
            <div class="is-box-centered">
                <div class="is-container layout-container pt-10" dragwithouthandle="">
                    <div class="row">
                        <div class="col-md-12 pb-50 mb-20" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/home08/homepage08-bg03.png&quot;); background-position: center bottom; background-repeat: no-repeat; background-size: contain;">
                            <div style="max-width: 770px;" class="m-auto">
                                <div class="title-11 text-center">
                                    <h3>We Can Turn Your Sketchy Ideas Into Great Features Like A Magic</h3>
                                </div>
                                <div style="position: relative;"><img alt="" src="/Portals/_default/ContentBuilder/minis-page/home08/homepage08-img03.jpg" class="shadow-xl img-Lazy" style="border-radius: 5px;"><a class="play-button2 is-lightbox bg-accent" href="https://www.youtube.com/embed/AkcUoA2f-jU" data-ilightbox="youtube" title=""><span class="icon"></span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style class="build-css" data-class="title-11">${csstemplate["title-11"]}</style>
    <style class="build-css" data-class="play-button2">${csstemplate["play-button2"]}</style>
    `
}, {
    'thumbnail': 'minis-section/home08/section-04.jpg',
    'category': '807',
    'html': `
    <div class="is-section is-box is-bg-grey is-light-text">
        <div class="is-overlay">
            <div class="is-overlay-bg" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/home08/homepage08-bg04.jpg&quot;);"></div>
        </div>
        <div class="is-boxes">
            <div class="is-box-centered">
                <div class="is-container layout-container pt-10 mb-40 mb-md-80" dragwithouthandle="">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="title-11 text-center">
                                <h3 class="m-auto" style="max-width: 570px;">Highly Powerful And Useful Website Functionality</h3>
                            </div>
                        </div>
                    </div>                  
                    ${getSnippetCode("iconbox-09")}
                </div>
            </div>
        </div>
    </div>
    <style class="build-css" data-class="title-11">${csstemplate["title-11"]}</style>
    `
}, {
    'thumbnail': 'minis-section/home08/section-05.jpg',
    'category': '807',
    'html': `
    <div class="is-section is-box">
        <div class="is-boxes">
            <div class="is-box-centered">
                <div class="is-container layout-container mb-30 mb-lg-70" dragwithouthandle="">
                    <div class="row align-items-start">
                        <div class="col-md-12"></div>
                    </div>
                    <div class="row">
                        <div class="col-md-5 pr-lg-40">
                            <div class="sidebar-sticky edit-box">
                                <div class="title-11 mt-10" style="max-width: 320px;">
                                    <h3>Would Like To know Our Team? </h3>
                                </div>
                                <p>Vestibulum quis efficitur lectus. Sed leo ipsum, mattis at faucibus gravida, fringilla at odio. Praesent dictum, lorem ut ultrices hendrerit quam.</p>
                                <div class="spacer height-20 mb-10"></div>
                                <div><a class="button-01" href="#" title="">LEARN MORE</a></div>
                                <p class="spacer height-60 mb-0"></p>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <div class="spacer height-20 mb-15 mb-md-0"></div>                         
                            ${getSnippetCode("ourteam-02")}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style class="build-css" data-class="title-11">${csstemplate["title-11"]}</style>
    <style class="build-css" data-class="button-01">${csstemplate["button-01"]}</style>
    `
}, {
    'thumbnail': 'minis-section/home08/section-06.jpg',
    'category': '807',
    'html': `
    <div class="is-section is-box is-bg-grey">
        <div class="is-overlay">
            <div class="is-overlay-bg" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/home08/homepage08-bg05.jpg&quot;);"></div>
        </div>
        <div class="is-boxes">
            <div class="is-box-centered">
                <div class="is-container layout-container" dragwithouthandle="">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="title-11">
                                <h3>Please Feel Free To Contact Us, We Will Respond To You ASAP.</h3>
                            </div>
                            ${getSnippetCode("ajaxform-08")}
                        </div>
                        <div class="col-md-6"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style class="build-css" data-class="title-11">${csstemplate["title-11"]}</style>
    `
}, {
    'thumbnail': 'minis-section/home08/section-07.jpg',
    'category': '807',
    'html': `
    <div class="is-section is-box">
        <div class="is-boxes">
            <div class="is-box-centered">
                <div class="is-container layout-container pt-10" dragwithouthandle="">
                    <div class="row align-items-center">
                        <div class="col-md-12 m-auto" style="max-width: 650px;">
                            <div class="title-11 text-center">
                                <h3>FAQs Here May Help You Out If You Are Troubled By Any Issues</h3>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="spacer height-20"></div>
                        </div>
                    </div>
                    <div class="row align-items-center">
                        <div class="col-md-4"><img alt="" src="/Portals/_default/ContentBuilder/minis-page/home08/homepage08-img04.jpg" class="shadow-sm img-Lazy">
                        <div class="spacer height-40 d-blcok d-md-none"></div>
                        </div>
                        <div class="col-md-8">
                            ${getSnippetCode("faq-01")}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style class="build-css" data-class="title-11">${csstemplate["title-11"]}</style>
    `
}, {
    'thumbnail': 'minis-section/home08/footer.jpg',
    'category': '807',
    'html': `
    <div class="is-section is-box is-align-center is-light-text">
        <div class="is-overlay">
            <div class="is-overlay-bg" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/home08/homepage08-footer-bg.jpg&quot;);"></div>
        </div>
        <div class="is-boxes">
            <div class="is-box-centered">
                <div class="is-container layout-container mb-40" dragwithouthandle="">
                    <div class="row">
                        <div class="col-md-12"><img class="img-Lazy" alt="" src="/Portals/_default/ContentBuilder/minis-page/home08/homepage08-footer-logo.png">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="spacer height-40 mt-10"></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <p class="m-auto" style="font-size:18px; max-width: 770px; color: rgb(135, 168, 162);">Aliquam nec varius lectus. Sed vitae ipsum non odio lobortis tempor. Donec in accumsan augue. Donec lectus eros, facilisis eget dui ac, vulputate aliquam risus. </p>
                            <div class="spacer height-40 mb-10">
                            </div>
                            <div>
                                <a class="social-06" href="#"><i class="sico fab-pinterest-p"></i></a>
                                <a class="social-06" href="#"><i class="sico fab-linkedin-in"></i></a>
                                <a class="social-06" href="#"><i class="sico fab-twitter"></i></a><a class="social-06" href="#"><i class="sico fab-dribbble"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div style="background-color: rgba(255,255,255,.08)">
                    <div class="is-container layout-container mt-0 mb-0 pt-35" dragwithouthandle="">
                        <div class="row homepage08-footer-line">
                            <div class="col-md pb-35">
                                <div class="icon">
                                    <i class="sico lnr-envelope-open size-32"></i>
                                </div>
                                <p class="mt-5 mb-0">
                                    service.simple@gmail.com
                                </p>
                            </div>
                            <div class="col-md pb-35">
                                <div class="icon">
                                    <i class="sico lnr-phone-wave size-32"></i>
                                </div>
                                <p class="mt-5 mb-0">(845) 359-7777
                                </p>
                            </div>
                            <div class="col-md  pb-35">
                                <div class="icon">
                                    <i class="sico lnr-road-sign size-32"></i>
                                </div>
                                <p class="mt-5 mb-0">252 NY-303, Orangeburg, NY 10962</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="is-container layout-container mt-40 mb-40" dragwithouthandle="">
                    <div class="row">
                        <div class="col-md-12">
                            <p class="mt-0 mb-0" style="color: rgb(135, 168, 162);">@ 2019 by DNNGo Corp. All Rights Resevered</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style class="build-css" data-class="social-06">${csstemplate["social-06"]}</style>
    <style class="build-css" data-class="homepage08-footer-line">
    @media all and (min-width: 768px) {
    .homepage08-footer-line > div::before{
        content: "";
        border-left: 1px dashed rgba(255,255,255,.2);
        height: 70px;
        right: 0;
    top:-2px;
        position: absolute;
    }
    .homepage08-footer-line > div:last-child::before{
        content: none;

    }
    }
    </style>
    `
})


/*Home09*/

data_basic.designs.push({
    'thumbnail': 'minis-section/home09/banner.jpg',
    'category': '808',
    'html': `
    <div class="is-section is-section-75 is-section-md-50 is-light-text is-box homepage09-banner">
        <div class="is-overlay">
            <div class="is-overlay-content"></div>
            <div class="is-overlay-bg" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/home09/homepage09-banner.jpg&quot;);"></div>
        </div>
        <div class="is-boxes">
            <div class="is-box-centered">
                <div class="is-container container-fluid is-content-800">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="edit-box text-center">
                                <div class="title-18 mb-0">
                                    <small class="size-24" style="margin-bottom: 5px;font-weight: 600;">With Professional Skills</small>
                                    <h1 style="font-weight: 700;">Develop Business Solutions That Fitted To Your Needs</h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style class="build-css" data-class="title-18">${csstemplate["title-18"]}</style>
    `
}, {
    'thumbnail': 'minis-section/home09/section-01.jpg',
    'category': '808',
    'html': `
    <div class="is-section is-box homepage09-section01">
        <div class="is-boxes">
            <div class="is-box-centered">
                <div class="is-container layout-container">
                    <div class="row text-center">
                        <div class="col-md-6 col-lg-4 column-01">
                            <div class="infobox-07">
                                <div class="pic">
                                    <span><a href="/Portals/_default/ContentBuilder/minis-page/home09/homepage09-section01-01.jpg" title="Professional Design" class="is-lightbox" data-ilightbox="image"><img class="img-Lazy" alt="Professional Design" src="/Portals/_default/ContentBuilder/minis-page/home09/homepage09-section01-01.jpg"></a></span>
                                </div>
                                <div class="content edit-box">
                                    <h5 class="size-24">Professional Design</h5>
                                    <p>Maecenas cursus efficitur nisl quis vehicula.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-4 column-02">
                            <div class="infobox-07 mt-30 mt-md-0">
                                <div class="pic">
                                    <img class="img-Lazy" alt="SEO Basic Optimization" src="/Portals/_default/ContentBuilder/minis-page/home09/homepage09-section01-02.jpg">
                                </div>
                                <div class="content edit-box">
                                    <h5 class="size-24">SEO Basic Optimization</h5>
                                    <p>Maecenas cursus efficitur nisl quis vehicula.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-4 column-03">
                            <div class="infobox-07 mt-30 mt-lg-0">
                                <div class="pic">
                                    <img class="img-Lazy" alt="Incomparable service" src="/Portals/_default/ContentBuilder/minis-page/home09/homepage09-section01-03.jpg">
                                </div>
                                <div class="content edit-box">
                                    <h5 class="size-24">Incomparable service</h5>
                                    <p>Maecenas cursus efficitur nisl quis vehicula.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style class="build-css" data-class="infobox-07">${csstemplate["infobox-07"]}</style>
    <style class="build-css" data-class="homepage09-section01">
    .homepage09-section01.is-box .is-container {
        margin-top: -138px;
        margin-bottom: 0;
    }

    .homepage09-section01.is-box .is-container .column-01 {
        margin-top: 50px;
    }

    .homepage09-section01.is-box .is-container .column-03 {
        margin-top: 100px;
    }

    @media (max-width: 991px) {
        .homepage09-section01.is-box .is-container {
            margin-top: 60px;
            margin-bottom: 0;
        }

        .homepage09-section01.is-box .is-container .column-01,
        .homepage09-section01.is-box .is-container .column-03 {
            margin-top: 0;
        }

    }
    </style>
    `
}, {
    'thumbnail': 'minis-section/home09/section-02.jpg',
    'category': '808',
    'html': `
    <div class="is-section is-box homepage09-section02">
        <div class="is-boxes">
            <div class="is-box-centered">
                <div class="is-container layout-container" style="max-width: 998px;">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <div class="text-center pt-0 pt-md-5">
                                <div class="title-18" style="max-width: 670px;margin-left: auto;margin-right: auto">
                                    <h3>We Provide Website <span>Solutions With</span> Powerful Technology</h3>
                                </div>
                                <p>Sed nibh justo, posuere in ullamcorper eget, finibus ut turpis. Etiam odio nulla, venenatis vel pulvinar interdum, varius quis turpis. Cras nunc dui, finibus sit amet interdum non, luctus sit amet mauris. In feugiat odio a gravida pharetra. Vestibulum nisl massa, iaculis eu nunc eget, hendrerit fringilla dui. Sed ac leo consequat, tincidunt ex sed, aliquam leo.</p>
                                <div class="spacer height-20 mb-10"></div>
                            </div>
                        </div>
                    </div>
                    ${getSnippetCode("infobox-08")}
                    <div class="row">
                        <div class="col-md-12">
                            <div class="text-center">
                                <a href="#" title="VIEW MORE" class="button-07 mb-0"><span>VIEW MORE</span><i class="sico lnr-binoculars"></i></a>
                            </div>
                            <div class="spacer height-20"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style class="build-css" data-class="title-18">${csstemplate["title-18"]}</style>
    <style class="build-css" data-class="button-07">${csstemplate["button-07"]}</style>
    `
}, {
    'thumbnail': 'minis-section/home09/section-03.jpg',
    'category': '808',
    'html': `
    <div class="is-section is-section-auto homepage09-section03">
        <div class="is-boxes">
            <div class="is-box-6 is-light-text is-box">
                <div class="is-overlay">
                    <div class="is-overlay-content"></div>
                    <div class="is-overlay-bg" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/home09/homepage09-section03-01.jpg&quot;);"></div>
                </div>
                <div class="is-boxes">
                    <div class="is-box-centered">
                        <div class="is-container container-fluid" style="max-width: 560px;">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="display">
                                        <div class="title-18">
                                            <small>With Professional Skills</small>
                                            <h2>We Can Turn Your Sketchy Ideas Into Great Features Like A Magic</h2>
                                        </div>
                                        <a class="button-05 is-lightbox" data-ilightbox="youtube" href="https://www.youtube.com/embed/AkcUoA2f-jU" data-type="iframe" data-options="width: '1280',height: '720',skin:'dark'" title="HOW WE WORKS"><span><i class="sico fas-play"></i></span>HOW WE WORKS</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="is-box-6 is-light-text is-box">
                <div class="is-boxes">
                    <div class="is-box-centered">
                        <div class="is-container layout-container is-container-fluid m-auto pl-0 pr-0">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="display-box text-center">
                                        <div class="item-01">
                                        ${getSnippetCode("infobox-09")}   
                                        </div>
                                        <div class="item-02">
                                            <div class=""><img class="img-Lazy" alt="image" src="/Portals/_default/ContentBuilder/minis-page/home09/homepage09-section03-03.jpg"></div>
                                        </div>
                                        <div class="item-03">
                                            <div class=""><img class="img-Lazy" alt="image" src="/Portals/_default/ContentBuilder/minis-page/home09/homepage09-section03-04.jpg"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style class="build-css" data-class="title-18">${csstemplate["title-18"]}</style>
    <style class="build-css" data-class="button-05">${csstemplate["button-05"]}</style>
    <style class="build-css" data-class="homepage09-section03">
    .homepage09-section03 .display-box {
        display: flex;
        flex-wrap: wrap;
    }
    .homepage09-section03 .display-box .item-01 {
        width: 100%;
    }

    .homepage09-section03 .display-box .item-02,
    .homepage09-section03 .display-box .item-03 {
        width: 50%;
    }

    @media (max-width: 767px) {
        .homepage09-section03 .display-box .item-02,
        .homepage09-section03 .display-box .item-03 {
            width: 100%;
        }
    }
    </style>
    `
}, {
    'thumbnail': 'minis-section/home09/section-04.jpg',
    'category': '808',
    'html': `
    <div class="is-section is-box homepage09-section04">
        <div class="is-boxes">
            <div class="is-box-centered">
                <div class="is-container layout-container">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="spacer height-20"></div>
                        </div>
                    </div>
                    <div class="row text-center text-lg-left">
                        <div class="col-lg-6">
                            <div class=""><img class="img-Lazy" alt="image" src="/Portals/_default/ContentBuilder/minis-page/home09/homepage09-section04-01.jpg"></div>
                        </div>
                        <div class="col-lg-6">
                            <div class="display pl-lg-30 pt-40 mt-2">
                                <div class="title-18 d-inline-block" style="max-width: 420px;">
                                    <h3>Skilled People Always Come Up With <span>Creative Ideas</span> When Together</h3>
                                </div>
                                <div class="display-box text-left edit-box">
                                <div class="custom-module loading" id="module-{id}" data-moduleid="{id}" data-style="progressbar01" data-effect="progressbar" data-settings="%7B%22items%22%3A%7B%22item0%22%3A%7B%22title%22%3A%22Web%20Design%22%2C%22progress%22%3A%2292%22%7D%2C%22item1%22%3A%7B%22title%22%3A%22PhotoShop%22%2C%22progress%22%3A%2288%22%2C%22color%22%3A%22rgb(32%2C163%2C252)%22%7D%2C%22item2%22%3A%7B%22title%22%3A%22HTML5%20%26%20CSS3%22%2C%22progress%22%3A%2286%22%2C%22color%22%3A%22rgb(60%2C179%2C60)%22%7D%2C%22item3%22%3A%7B%22title%22%3A%22UI%20Design%22%2C%22progress%22%3A%2290%22%2C%22color%22%3A%22rgb(255%2C110%2C47)%22%7D%2C%22item4%22%3A%7B%22title%22%3A%22JavaScript%22%2C%22progress%22%3A%2296%22%2C%22color%22%3A%22%2300ac8a%22%7D%7D%7D"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="spacer height-20 mt-lg-5 mb-lg-3"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style class="build-css" data-class="title-18">${csstemplate["title-18"]}</style>
    <style class="build-css" data-class="homepage09-section04">
    .homepage09-section04 .display-box {
        width: 130%;
        padding: 53px 50px 60px;
        background: #fff;
        -webkit-transform: translate3d(-22.5%, 8px, 0);
        transform: translate3d(-22.5%, 8px, 0);
        -webkit-box-shadow: 0px 0px 25px rgba(0, 0, 0, 0.1);
        box-shadow: 0px 0px 25px rgba(0, 0, 0, 0.1);
    }

    .homepage09-section04 .display-box .progressbar01 .loadlist .progress {
        height: 15px;
    }

    @media (max-width: 991px) {
        .homepage09-section04 .display-box {
            width: 100%;
            -webkit-transform: translate3d(0, 0, 0);
            transform: translate3d(0, 0, 0);
        }
    }
    @media (max-width: 575px) {
        .homepage09-section04 .display-box {
            padding: 53px 15px 60px;
        }
    }
    </style>
    `
}, {
    'thumbnail': 'minis-section/home09/section-05.jpg',
    'category': '808',
    'html': `
    <div class="is-section is-box homepage09-section05 current-cover-bg is-light-text">
        <div class="is-overlay">
            <div class="is-overlay-bg" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/home09/homepage09-section05-bg.jpg&quot;);"></div>
        </div>
        <div class="is-boxes">
            <div class="is-box-centered">
                <div class="is-container layout-container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="edit-box text-center pt-md-5 pb-15 mt-md-2 m-auto" style="max-width: 570px;">
                                <h3>Praise From People Is What Driving Us Work Harder</h3>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                        ${getSnippetCode("testimonials03")}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style class="build-css" data-class="homepage09-section05">
    .homepage09-section05 .swiper-testimonials03 {
        margin-bottom: -10px;
    }
    </style>
    `
}, {
    'thumbnail': 'minis-section/home09/section-06.jpg',
    'category': '808',
    'html': `
    <div class="is-section is-box homepage09-section06">
        <div class="is-boxes">
            <div class="is-box-centered">
                <div class="is-container layout-container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="spacer height-20 d-none d-lg-block"></div>
                        </div>
                    </div>
                    <div class="row align-items-center">
                        <div class="col-lg-5">
                            <div class="title-18" style="max-width: 350px;margin-bottom: 42px;">
                                <h3>Would Like To Know <span>Our Team</span>? </h3>
                            </div>
                            <p>Cras posuere, elit eu semper mollis, erat purus cursus mauris, quis efficitur augue enim quis arcu. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere.</p>
                            <div class="iconbox-06 pt-15">
                                <div class="icon">
                                    <i class="sico lnr-envelope-open"></i>
                                </div>
                                <div class="content">
                                    <p>Email Us</p>
                                    <h6>service.simple@gmail.com</h6>
                                </div>
                            </div>
                            <div class="spacer height-20"></div>
                            <a href="#" title="VIEW MORE" class="button-07"><span>VIEW MORE</span><i class="sico lnr-binoculars"></i></a>
                        </div>
                        <div class="col-lg-7">
                            <div class="infobox-list pt-40 pt-lg-0 pl-lg-30">
                            ${getSnippetCode("infobox-10")}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="spacer height-20"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style class="build-css" data-class="title-18">${csstemplate["title-18"]}</style>
    <style class="build-css" data-class="button-07">${csstemplate["button-07"]}</style>
    <style class="build-css" data-class="iconbox-06">${csstemplate["iconbox-06"]}</style>

    <style data-class="homepage09-section06">
    .homepage09-section06 .infobox-list {
        padding-bottom: 50px;
        margin-bottom: -30px;
    }

    .homepage09-section06 .infobox-list [class*='col-']:nth-child(even) {
        transform: translateY(50px);
    }

    @media (max-width: 767px) {
        .homepage09-section06 .infobox-list {
            padding-bottom: 0;
        }

        .homepage09-section06 .infobox-list [class*='col-']:nth-child(even) {
            transform: translateY(0);
        }
    }
    </style>
    `
}, {
    'thumbnail': 'minis-section/home09/section-07.jpg',
    'category': '808',
    'html': `
    <div class="is-section is-box homepage09-section07">
        <div class="is-boxes">
            <div class="is-box-centered">
                <div class="is-container layout-container is-container-fluid m-auto pl-0 pr-0">
                    <div class="row col-no-padding text-center">
                        <div class="col-lg-1/5 col-sm-6">
                            <div class="">
                                <img class="img-Lazy" alt="image" src="/Portals/_default/ContentBuilder/minis-page/home09/homepage09-section07-01.jpg">
                            </div>
                        </div>
                        <div class="col-lg-1/5 col-sm-6">
                            <div class="">
                                <img class="img-Lazy" alt="image" src="/Portals/_default/ContentBuilder/minis-page/home09/homepage09-section07-02.jpg">
                            </div>
                        </div>
                        <div class="col-lg-1/5 col-sm-6">
                            <div class="">
                                <img class="img-Lazy" alt="image" src="/Portals/_default/ContentBuilder/minis-page/home09/homepage09-section07-03.jpg">
                            </div>
                        </div>
                        <div class="col-lg-1/5 col-sm-6">
                            <div class="">
                                <img class="img-Lazy" alt="image" src="/Portals/_default/ContentBuilder/minis-page/home09/homepage09-section07-04.jpg">
                            </div>
                        </div>
                        <div class="col-lg-1/5 col-sm-6">
                            <div class="">
                                <img class="img-Lazy" alt="image" src="/Portals/_default/ContentBuilder/minis-page/home09/homepage09-section07-05.jpg">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    `
}, {
    'thumbnail': 'minis-section/home09/footer.jpg',
    'category': '808',
    'html': `
    <div class="is-section is-box homepage09-footer">
        <div class="is-overlay">
            <div class="is-overlay-bg" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/home09/homepage09-footer-bg.jpg&quot;);"></div>
        </div>
        <div class="is-boxes">
            <div class="is-box-centered">
                <div class="is-container layout-container" style="max-width: 800px;">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="text-center" style="color: #7d7f8b;">
                                <div class="">
                                    <img class="img-Lazy" alt="image" src="/Portals/_default/ContentBuilder/minis-page/logo-big-white.png">
                                </div>
                                <p style="margin-top: 41px; margin-bottom: 59px;">Proin quis neque faucibus, sodales nisl sit amet, finibus ipsum. Integer id sagittis mauris. Pellentesque habitant morbi tristique senectus et netus.</p>
                                <h5 style="color: #fff">Subscribe To Newsletter</h5>
                                <div style="margin: 26px auto 47px; max-width: 430px;">
                                ${getSnippetCode("ajaxform-09")}
                                </div>
                                <p class="social-list edit-box">
                                    <a class="social-07" href="#"><i class="color-accent sico fab-pinterest-p"></i>Pinterest</a>
                                    <a class="social-07" href="#"><i class="color-accent sico fab-linkedin-in"></i>Linkedin</a>
                                    <a class="social-07" href="#"><i class="color-accent sico fab-twitter"></i>Twitter</a>
                                    <a class="social-07" href="#"><i class="color-accent sico fab-dribbble"></i>Dribbble</a>
                                </p>
                                <p style="margin-bottom: -9px;">@ 2019 by DNNGo Corp.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style class="build-css" data-class="social-07">${csstemplate["social-07"]}</style>
    <style class="build-css" data-class="homepage09-footer">
        .homepage09-footer .social-list {
            margin-bottom: 35px;
            font-size: 0;
        }

        .homepage09-footer .social-list .social-07 {
            font-size: 16px;
            margin-left: 30px;
            margin-right: 30px;
        }
    </style>
    `
})


/*Home10*/

data_basic.designs.push({
    'thumbnail': 'minis-section/home10/banner.jpg',
    'category': '809',
    'html': `
        <div class="is-section is-section-100 is-section-md-50 is-light-text is-box homepage10-banner">
            <div class="is-overlay">
                <div class="is-overlay-content"></div>
                <div class="is-overlay-bg" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/home10/homepage10-banner.jpg&quot;);"></div>
            </div>

            <div class="is-boxes">
                <div class="is-box-centered">
                    <div class="is-container container-fluid is-container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="display">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-md-6 edit-box">
                                                <h3 style="color: rgb(86, 253, 220); font-weight: normal; margin-bottom: 6px;">Professional IT Team</h3>
                                                <h1 style="margin-bottom: 40px; font-weight:700;">Help Your Business Grow</h1>
                                                <p style="margin-bottom: 60px; max-width: 410px; font-size:18px; line-height:30px;" data-keep-font-size="" data-keep-line-height="">Etiam augue augue, aliquam vel sodales ac, blandit et eros. In conse quat neque in elit porta molestie. Donec at mi nulla.</p>
                                                <a class="play-button is-lightbox" data-ilightbox="youtube" href="https://www.youtube.com/embed/AkcUoA2f-jU" data-type="iframe" data-options="width: '1280',height: '720',skin:'dark'" title=""><span><i class="sico fas-play color-accent size-18"></i></span></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="elements">
                                        <div class="item-01">
                                            <img class="img-Lazy" alt="image" src="/Portals/_default/ContentBuilder/minis-page/home10/homepage10-banner-01.png">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <style class="build-css" data-class="play-button">${csstemplate["play-button"]}</style>
        <style class="build-css" data-class="homepage10-banner">
            .homepage10-banner{
                position: relative;
            }
            .homepage10-banner .elements .item-01{
                position: absolute; 
                top: 42%; 
                left: 60%;
                transform: translate3d(-25%,-36%,0);
                width: 100vw;
            }

            
            @media (max-width: 1199px){
                .homepage10-banner .elements .item-01{
                    left: 50%; 
                    transform: translate3d(-8%,-36%,0);
                    max-width: calc( 50% + ( 100 - 50 ) * ((100vw - 768px) / (1199 - 768)));
                }  
            }
            @media (max-width: 767px){
                .homepage10-banner .elements .item-01{
                    top: 100%;
                    transform: translate3d(-8%,-78%,0);
                } 
            }
        </style>
    `
}, {
    'thumbnail': 'minis-section/home10/section-01.jpg',
    'category': '809',
    'html': `
        <div class="is-section is-box homepage10-section01">
            <div class="is-boxes">
                <div class="is-box-centered">
                    <div class="is-container layout-container" dragwithouthandle="">

                        <div class="row">
                            <div class="col-md-12">
                                <div class="spacer pt-md-10 pb-10"></div>
                            </div>
                        </div>
                        <div class="row align-items-center">
                            <div class="col-md-6">
                                <div class="display-content">
                                    <div class="title-09" style="max-width: 500px;">
                                        <small>ABOUT US</small>
                                        <h2>Creative ideas have made <span class="color-accent">the world better.</span></h2>
                                    </div>

                                    <div class="blockquotes-03">
                                        <div class="content edit-box">

                                            <p>In maximus risus sed lacus feugiat tempus. In sit amet est porttitor tortor vehicula lobortis. Phasellus at venenatis urna. Duis suscipit, tellus eu volutpat tincidunt, velit diam hendr erit mauris, sed vestibulum.</p>
                                        </div>
                                        <footer>
                                            <div class="pic border-accent">
                                                <span class="Lazy-loading" style="width:83px"></span><img alt="Ross Williams" class="img-Lazy" src="/Portals/_default/ContentBuilder/minis-page/home10/homepage10-section01-blockquotes.png">
                                            </div>
                                            <div class="info">
                                                <strong>David Smith</strong>
                                                <cite>CEO</cite>
                                            </div>
                                        </footer>
                                    </div>


                                </div>
                            </div>
                            <div class="col-md-6">
                                ${getSnippetCode("carousel03")}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="spacer height-20"></div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <style class="build-css" data-class="title-09">${csstemplate["title-09"]}</style>
        <style class="build-css" data-class="blockquotes-03">${csstemplate["blockquotes-03"]}</style>

    `
}, {
    'thumbnail': 'minis-section/home10/section-02.jpg',
    'category': '809',
    'html': `
        <div class="is-section is-box homepage10-section02" style="background-color: rgb(238, 240, 244);">
            <div class="is-overlay d-none d-md-block">
                <div class="is-overlay-bg" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/home10/homepage10-section02-bg.jpg&quot;);"></div>
            </div>
            <div class="is-boxes">
                <div class="is-box-centered">
                    <div class="is-container layout-container" style="max-width: 1620px;" dragwithouthandle="">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="spacer pt-10"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-lg-6">
                                <div></div>
                            </div>
                            <div class="col-md-8 col-lg-6">
                                <div class="display-content">
                                    <div class="title-13">
                                        <h3>How Do <span class="color-accent">We Work</span></h3>
                                        <div class="line color-accent"></div>
                                    </div>
                                    <div class="row pt-10 iconbox-list" style="margin-bottom:-52px;">
                                        <div class="col-sm-6">
                                            <div class="iconbox-10">
                                                <div class="icon color-accent">
                                                    <i class="sico lnr-clipboard-check"></i>
                                                </div>
                                                <div class="title">
                                                    <h6>Analyze requirements</h6>
                                                </div>
                                                <div class="edit-box">
                                                    <p>Curabitur at magna sem. Etiam fringilla, eros vel efficitur congue, nisi felis luctus nulla, sit amet </p>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-6">
                                            <div class="iconbox-10">
                                                <div class="icon color-accent">
                                                    <i class="sico lnr-magic-wand"></i>
                                                </div>
                                                <div class="title">
                                                    <h6>Work on web design</h6>
                                                </div>
                                                <div class="edit-box">
                                                    <p>Pellentesque sed maximus orci. Suspendisse tincidunt consequat ipsum id sagittis.</p>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-6">
                                            <div class="iconbox-10">
                                                <div class="icon color-accent">
                                                    <i class="sico lnr-laptop-phone"></i>
                                                </div>
                                                <div class="title">
                                                    <h6>Develop product</h6>
                                                </div>
                                                <div class="edit-box">
                                                    <p>Morbi vehicula pellentesque neque, tincidunt ultricies massa placerat pretium tortor mi.</p>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-6">
                                            <div class="iconbox-10">
                                                <div class="icon color-accent">
                                                    <i class="sico lnr-rocket"></i>
                                                </div>
                                                <div class="title">
                                                    <h6>Make test</h6>
                                                </div>
                                                <div class="edit-box">
                                                    <p>Aenean sit amet dolor pellentesque, vestibulum quam a, imperdiet dui. Vestibulum quis euismod.</p>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                </div>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="spacer pb-10"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <style class="build-css" data-class="title-13">${csstemplate["title-13"]}</style>
        <style class="build-css" data-class="iconbox-10">${csstemplate["iconbox-10"]}</style>
    `
}, {
    'thumbnail': 'minis-section/home10/section-03.jpg',
    'category': '809',
    'html': `
        <div class="is-section is-box homepage10-section03">
            <div class="is-boxes">
                <div class="is-box-centered">
                    <div class="is-container layout-container" dragwithouthandle="">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="spacer pb-10"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">

                                <div class="display-content">
                                    <div class="title-13 text-center" style="padding-bottom: 16px;">
                                        <h3>Our <span class="color-accent">Projects</span></h3>
                                        <div class="line color-accent"></div>
                                    </div>
                                    <p class="size-18 text-center m-auto" style="max-width: 700px;">Aenean sit amet dolor pellentesque, vestibulum quam a, imperdiet dui. Vestibulum quis euismod, vitae posuere dolor laoreet. </p>
                                </div>
                                <div class="spacer height-60"></div>
                                ${getSnippetCode("portfolio-masonry-style03")}

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="spacer pb-10"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <style class="build-css" data-class="title-13">${csstemplate["title-13"]}</style>

    `
}, {
    'thumbnail': 'minis-section/home10/section-04.jpg',
    'category': '809',
    'html': `

        <div class="is-section is-box homepage10-section04 is-light-text">
            <div class="is-overlay">
                <div class="is-overlay-bg" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/home10/homepage10-section04-bg.jpg&quot;);"></div>
            </div>
            <div class="is-boxes">
                <div class="is-box-centered">
                    <div class="is-container layout-container is-container-fluid m-auto pl-0 pr-0" dragwithouthandle="">
                        <div class="row ml-0 mr-0">

                            <div class="col-lg-6" style="background-color: rgba(50, 142, 124, 0.78);">
                                <div class="spacer height-140 mt-5 d-none d-lg-block"></div>
                                <div class="spacer height-60 mt-10 d-lg-none"></div>
                                <div class="display-box edit-box m-auto" style="max-width: 570px;">
                                    <div class="title-13" style="max-width: 480px;">
                                        <h3>Ready To Get Started? Start from Minis!</h3>
                                        <div class="line"></div>
                                    </div>
                                    <p style="max-width: 520px;">Praesent eget ex vel libero pellentesque faucibus id vel odio. Morbi porttitor justo felis, gravida porttitor.</p>
                                    <div class="infobox-11" style="padding-top: 19px; padding-bottom: 27px;">
                                        <div class="index">
                                            <span>01</span>
                                        </div>
                                        <div class="content">
                                            <h5>Install theme on your site</h5>
                                            <p>Quisque augue nisi, gravida id finibus facilisis malesuada.</p>
                                        </div>
                                    </div>
                                    <div class="infobox-11 pb-0">
                                        <div class="index">
                                            <span>02</span>
                                        </div>
                                        <div class="content">
                                            <h5>Import the demo templates</h5>
                                            <p>Quisque augue nisi, gravida id finibus facilisis malesuada.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="spacer height-40"></div>
                                <div class="spacer height-40"></div>
                            </div>
                            <div class="col-lg-6" style="background-color: rgba(44, 80, 129, 0.78);">
                                <div class="spacer height-140 mt-5 d-none d-lg-block"></div>
                                <div class="spacer height-60 mt-10 d-lg-none"></div>
                                <div class="display-box edit-box m-auto" style="max-width: 570px;">
                                    <div class="title-13" style="max-width: 520px">
                                        <h3>We Provide Unique Solutions For Your Business</h3>
                                        <div class="line"></div>
                                    </div>
                                    
                                    ${getSnippetCode("progressbar02")}

                                </div>
                                <div class="spacer height-140 mb-10 d-none d-lg-block"></div>
                                <div class="spacer height-40 d-lg-none"></div>
                                <div class="spacer height-40 d-lg-none"></div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <style class="build-css" data-class="title-13">${csstemplate["title-13"]}</style>
        <style class="build-css" data-class="infobox-11">${csstemplate["infobox-11"]}</style>
        <style class="build-css" data-class="homepage10-section04">
            .homepage10-section04 .progressbar02 .loadlist .progress {
                height: 15px;
            }
        </style>
    `
}, {
    'thumbnail': 'minis-section/home10/section-05.jpg',
    'category': '809',
    'html': `
        <div class="is-section is-box homepage10-section05">
            <div class="is-overlay">
                <div class="is-overlay-bg" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/home10/homepage10-section05-bg.jpg&quot;);"></div>
            </div>
            <div class="is-boxes">
                <div class="is-box-centered">

                    <div class="is-container layout-container" dragwithouthandle="">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="spacer mb-5"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="title-13 text-center mt-5">
                                    <h3>Testimonials</h3>
                                    <div class="line color-accent"></div>
                                </div>
                                <p class="m-auto size-18 text-center" style="max-width: 700px; color: rgb(102, 102, 102); line-height:30px;">Sed sagittis eros id urna euismod, sit amet tempor odio hendrerit. Sed ullamcorper quam, vitae posuere dolor laoreet. </p>
                                <div class="spacer height-60 mb-4"></div>
                            </div>
                        </div>
                        ${getSnippetCode("blockquotes-02")}

                    </div>

                </div>
            </div>
        </div>

        <style class="build-css" data-class="title-13">${csstemplate["title-13"]}</style>
    `
}, {
    'thumbnail': 'minis-section/home10/section-06.jpg',
    'category': '809',
    'html': `
        <div class="is-section is-box homepage10-section06">
            <div class="is-overlay"></div>
            <div class="is-boxes">
                <div class="is-box-centered">

                    <div class="is-container layout-container mb-lg-80 pb-lg-10" dragwithouthandle="">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="spacer mb-5"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="title-13 text-center mt-5">
                                    <h3>Get In <span class="color-accent">Touch</span></h3>
                                    <div class="line color-accent"></div>
                                </div>
                                <p class="m-auto size-18 text-center" style="max-width: 750px;color: rgb(102, 102, 102);">Nulla sodales risus eget tortor posuere, sed varius nulla volutpat. Sed sed nunc vitae, elit dapibus, vitae posuere dolor laoreet. </p>
                                <div class="spacer height-40 mb-15"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                ${getSnippetCode("ajaxform-06")}
                            </div>

                            <div class="col-md-6">
                                <div class="infobox-list">
                                    <div class="infobox-12 edit-box" style="color: rgb(26, 188, 156);">
                                        <div class="icon">
                                            <i class="sico lnr-telephone2"></i>
                                        </div>
                                        <h6>Phone Number</h6>
                                        <p class="color-accent size-18" style="font-weight: 600;">(845) 359-7777</p>
                                    </div>
                                    <div class="infobox-12 edit-box" style="color: rgb(255, 110, 47);">
                                        <div class="icon">
                                            <i class="sico lnr-envelope"></i>
                                        </div>
                                        <h6>Email Address</h6>
                                        <p>dnnskindev@gmail.com</p>
                                    </div>
                                    <div class="infobox-12 edit-box" style="color: rgb(33, 120, 224);">
                                        <div class="icon">
                                            <i class="sico lnr-map-marker"></i>
                                        </div>
                                        <h6>Location</h6>
                                        <p>252 NY-303, Orangeburg, NY 10965</p>
                                    </div>
                                </div>
                            </div>


                        </div>
                       
                    </div>

                </div>
            </div>
        </div>

        <style class="build-css" data-class="title-13">${csstemplate["title-13"]}</style>
        <style class="build-css" data-class="infobox-12">${csstemplate["infobox-12"]}</style>
        <style class="build-css" data-class="homepage10-section06">
            .homepage10-section06 .infobox-list {
                max-width: 520px;
                margin: 0 auto;
            }

            .homepage10-section06 .infobox-list .infobox-12 {
                max-width: 420px;
                margin-bottom: 20px;
            }

            .homepage10-section06 .infobox-list .infobox-12:last-child {
                margin-bottom: 0;
            }

            .homepage10-section06 .infobox-list .infobox-12:nth-child(even) {
                margin-left: auto;
            }

            @media only screen and (max-width: 767px) {
                .homepage10-section06 .ajaxform .message {
                    right: 0;
                    top: auto;
                    bottom: 30px;
                    left: 110px;
                }
            }
        </style>
    `
}, {
    'thumbnail': 'minis-section/home10/footer.jpg',
    'category': '809',
    'html': `
            <div class="is-section is-box is-light-text homepage10-footer">
                <div class="is-overlay">
                    <div class="is-overlay-bg" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/home10/homepage10-footer-bg.jpg&quot;);"></div>
                </div>
                <div class="is-boxes">
                    <div class="is-box-centered">
                        <div class="is-container layout-container is-container-fluid m-auto">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="spacer height-100 mt-15 d-none d-lg-block"></div>
                                    <div class="spacer height-20 mt-15 d-lg-none"></div>
                                    <div class="spacer height-40 d-lg-none"></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 text-center">
                                   <h3 style="font-weight: 400;line-height: 36px;">We Provide Perfect</h3>
                                    <h1 style="font-weight: 700;">Business Solution for You</h1>
                                    <div class="spacer height-80"></div>
                                    <div class="image-box">
                                        <div class="item">
                                            <img alt="Iamge" class="img-Lazy shadow-md" src="/Portals/_default/ContentBuilder/minis-page/home10/homepage10-footer-03.jpg">
                                        </div>
                                        <div class="item-01">
                                            <img alt="Iamge" class="img-Lazy shadow-md" src="/Portals/_default/ContentBuilder/minis-page/home10/homepage10-footer-01.jpg">
                                        </div>
                                        <div class="item-02">
                                            <img alt="Iamge" class="img-Lazy shadow-md" src="/Portals/_default/ContentBuilder/minis-page/home10/homepage10-footer-02.jpg">
                                        </div>
                                        <div class="item-04">
                                            <img alt="Iamge" class="img-Lazy shadow-md" src="/Portals/_default/ContentBuilder/minis-page/home10/homepage10-footer-04.jpg">
                                        </div>
                                        <div class="item-05">
                                            <img alt="Iamge" class="img-Lazy shadow-md" src="/Portals/_default/ContentBuilder/minis-page/home10/homepage10-footer-05.jpg">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="spacer height-120 d-none d-lg-block"></div>
                                    <div class="spacer height-40 d-lg-none"></div>
                                    <div class="spacer height-40 d-lg-none"></div>
                                </div>
                            </div>
                        </div>
                        <div style="background-color: rgba(0,0,0,.5)">
                            <div class="is-container layout-container is-container-fluid m-auto homepage10-footer-color">
                                <div class="row m-auto" style="max-width: 1270px;">
                                    <div class="col-md-6 text-center text-md-left">
                                        <p style="margin:0">@ 2019 by DNNGo Corp</p>
                                    </div>
                                    <div class="col-md-6 text-center text-md-right">
                                    <span><a href="#" title="Privacy Statement">Privacy Statement</a> &nbsp; | &nbsp; <a href="#" title="Privacy Statement">Terms of Use</a></span>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>


            <style class="build-css" data-class="homepage10-footer">
                .homepage10-footer .image-box {
                    position: relative;
                    text-align: center;
                    overflow: hidden;
                }

                .homepage10-footer .image-box .item {
                    display: inline-block;
                    position: relative;
                    z-index: 3;
                    pointer-events: none;
                }

                .homepage10-footer .image-box [class*="item-"] {
                    position: absolute;
                    top: 100%;
                    left: 50%;
                    transform: translate3d(-50%, -100%, 0);
                    pointer-events: none;
                }

                .homepage10-footer .image-box .item-01 {
                    left: 23%;
                    z-index: 1;
                }

                .homepage10-footer .image-box .item-02 {
                    left: 36%;
                    z-index: 2;
                }

                .homepage10-footer .image-box .item-04 {
                    left: 64%;
                    z-index: 2;
                }

                .homepage10-footer .image-box .item-05 {
                    left: 77%;
                    z-index: 1;
                }

                .homepage10-footer .image-box [class*="item"] img {
                    pointer-events: all;
                }

                .homepage10-footer .homepage10-footer-color {
                    padding-top: 41px;
                    padding-bottom: 41px;
                }

                .homepage10-footer .homepage10-footer-color * {
                    color: #9eb4af;
                }

                .homepage10-footer .homepage10-footer-color a:hover {
                    color: var(--accent-color);
                }

                .homepage10-footer .image-box .item {
                    width: 32%;
                }

                .homepage10-footer .image-box .item-01,
                .homepage10-footer .image-box .item-05 {
                    width: 21%;
                }

                .homepage10-footer .image-box .item-02,
                .homepage10-footer .image-box .item-04  {
                    width: 26%; 
                }
               
            </style>
    `
})




/*Home11*/

data_basic.designs.push({
    'thumbnail': 'minis-section/home11/banner.jpg',
    'category': '810',
    'html': `
        <div class="is-section is-section-100 is-section-md-auto is-box is-dark-text is-align-left">
            <div class="is-overlay">
                <div class="is-overlay-content"></div>
                <div class="is-overlay-bg" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/home11/homepage11-banner.jpg&quot;);"></div>
            </div>
            <div class="is-boxes">
                <div class="is-box-centered">
                    <div class="is-container container-fluid" style="max-width: 1379px;" dragwithouthandle="">
                        <div class="row">
                            <div class="col-8 col-md-6">
                                <p class="size-18 mb-20" style="color: rgb(85, 85, 85);">Professional IT Company</p>
                                <h1 class="size-48" style="border-bottom: 1px solid rgb(188, 188, 188); font-weight: 700; padding-bottom: 26px; margin-bottom: 43px;"><span class="color-accent size-40" style="line-height: 1.46;">We Provide Perfect</span><br>Business Solution for You</h1>
                                <p style="color: rgb(102, 102, 102);">Etiam augue augue, aliquam vel sodales ac, blandit et eros. In conse quat neque in elit porta molestie. Donec at mi nulla. Mauris cursus vel libero ac iaculis. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod.</p>
                                <div class="spacer height-20 mb-10"></div>
                                <p>
                                    <a href="#" title="Read More" class="button-08">Read More <i class="sico lnr-arrow-right"></i></a>
                                    <a href="#" title="Buy It Now" class="button-08">Buy It Now<i class="sico lnr-arrow-right"></i></a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <style class="build-css" data-class="button-08">${csstemplate["button-08"]}</style>
    `
}, {
    'thumbnail': 'minis-section/home11/section-01.jpg',
    'category': '810',
    'html': `
        <div class="is-section is-box">
            <div class="is-boxes">
                <div class="is-box-centered">
                    <div class="is-container layout-container mb-50 mb-lg-80 pb-lg-10">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="title-15">
                                    <h3>Development <b>History</b></h3>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="pic"><img alt="The Establishment of company" class="mb-30 img-Lazy" src="/Portals/_default/ContentBuilder/minis-page/home11/homepage11-img01.jpg">
                                </div>
                                <h6>The Establishment of company</h6>
                                <p><i class="color-accent">July 11, 2011</i></p>
                                <p>Morbi feugiat, nibh nec laoreet molis augue mi accumsan velitacscele. risque turpis purus ut odio. Donec ut massa est. </p>
                                <p class="pt-10"><a href="#" title="Buy It Now" class="button-08">Read More<i class="sico lnr-arrow-right"></i></a></p>
                            </div>
                            <div class="col-md-4">
                                <div class="pic"><img alt="Break the DVG technology" class="mb-30 img-Lazy" src="/Portals/_default/ContentBuilder/minis-page/home11/homepage11-img02.jpg"></div>
                                <h6>Break the DVG technology</h6>
                                <p><i class="color-accent">March 20, 2013</i></p>
                                <p>Feugiat, nibh nec laoreet molis augue mi accumsan velitacs cele. risque turpis purus ut odio. Donec ut massa est ultricies. </p>
                                <p class="pt-10"><a href="#" title="Buy It Now" class="button-08">Read More<i class="sico lnr-arrow-right"></i></a></p>
                            </div>
                            <div class="col-md-4">
                                <div class="pic"><img alt="One of the best DNN vendors" class="mb-30 img-Lazy" src="/Portals/_default/ContentBuilder/minis-page/home11/homepage11-img03.jpg"></div>
                                <h6>One of the best DNN vendors</h6>
                                <p><i class="color-accent">January, 2016</i></p>
                                <p>Dhnec laoreet molis augue mi accum san velitac scele. ris que turpis purus ut odio. Donec ut massa est. Sed congue lorem id lorem maximus ultricies. </p>
                                <p class="pt-10"><a href="#" title="Buy It Now" class="button-08">Read More<i class="sico lnr-arrow-right"></i></a></p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <style class="build-css" data-class="title-15">${csstemplate["title-15"]}</style>
        <style class="build-css" data-class="button-08">${csstemplate["button-08"]}</style>
    `
}, {
    'thumbnail': 'minis-section/home11/section-02.jpg',
    'category': '810',
    'html': `
        <div class="is-section is-box is-light-text is-align-center">
            <div class="is-overlay">
                <div class="is-overlay-bg" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/home11/homepage11-bg01.jpg&quot;);"></div>
            </div>
            <div class="is-boxes">
                <div class="is-box-centered">
                    <div class="is-container layout-container mb-0">
                        <div class="row">
                            <div class="col-md-12">
                                <h1 style="font-weight: 600;">Break The Inherent Thinking</h1>
                                <h5 class="size-24">Constantly Looking For New Ideas And Methods.</h5>
                                <div class="spacer height-100 d-none d-lg-block"></div>
                                <div class="mb-20 mt-40 mb-lg-0 mt-lg-0"><a class="play-button2 is-lightbox bg-accent m-auto" href="https://www.youtube.com/embed/AkcUoA2f-jU" data-ilightbox="youtube" title="" style="position: relative; top: 0px; left: 0px;"><span class="icon"></span></a></div>
                                <div class="spacer height-20 pt-80 pb-80 mb-50"></div>
                                <div class="spacer height-20 pt-0 pb-40 pb-md-0 pt-lg-80 pb-lg-80"></div>
                            </div>
                        </div>



                    </div>
                </div>
            </div>
        </div>
        <style class="build-css" data-class="play-button2">${csstemplate["play-button2"]}</style>
    `
}, {
    'thumbnail': 'minis-section/home11/section-03.jpg',
    'category': '810',
    'html': `
        <div class="is-section is-box section-no-padding">
            <div class="is-boxes no-min-height">
                <div class="is-box-centered">
                    <div class="is-container layout-container">
                        <div class="row" style="margin: -200px 0px 0px;">
                            <div class="col-md-6">
                                <div class="img-box06">
                                    <span class=""><img class="shadow-md img-Lazy" src="/Portals/_default/ContentBuilder/minis-page/home11/homepage11-img04.jpg" alt="Amazing book mockup design"></span>
                                    <div class="title">
                                        <p> Amazing book mockup design</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="img-box06">
                                    <img class="shadow-md img-Lazy" src="/Portals/_default/ContentBuilder/minis-page/home11/homepage11-img05.jpg" alt="Beauty natural logo design">
                                    <div class="title">
                                        <p>Beauty natural logo design</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <style class="build-css" data-class="img-box06">${csstemplate["img-box06"]}</style>
    `
}, {
    'thumbnail': 'minis-section/home11/section-04.jpg',
    'category': '810',
    'html': `
        <div class="is-section is-box">
            <div class="is-boxes">
                <div class="is-box-centered">
                    <div class="is-container layout-container mt-35 mt-lg-75 mb-60">
                        <div class="row">
                            <div class="col-md-12" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/home03/homepage03-bg04.png&quot;); background-position: center center; background-repeat: no-repeat; background-size: auto;">
                                <div class="title-15">
                                    <h3>Our <b>Clients</b></h3>
                                </div>
                                ${getSnippetCode("testimonials05")}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <style class="build-css" data-class="title-15">${csstemplate["title-15"]}</style>
    `
}, {
    'thumbnail': 'minis-section/home11/section-05.jpg',
    'category': '810',
    'html': `
        <div class="is-section is-box section-no-padding">
            <div class="is-boxes no-min-height">
                <div class="is-box-centered">
                    <div class="is-container layout-container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row home11-clientlist">
                                    <div class="col-1/5">
                                        <span><a href="#" title="" class=""><img alt="client" class="img-Lazy" src="/Portals/_default/ContentBuilder/minis-page/home11/homepage11-client01.jpg"></a></span>
                                    </div>
                                    <div class="col-1/5">
                                        <span><a href="#" title="" class=""><img alt="client" class="img-Lazy" src="/Portals/_default/ContentBuilder/minis-page/home11/homepage11-client02.jpg"></a></span>
                                    </div>
                                    <div class="col-1/5">
                                        <span><a href="#" title="" class=""><img alt="client" class="img-Lazy" src="/Portals/_default/ContentBuilder/minis-page/home11/homepage11-client03.jpg"></a></span>
                                    </div>
                                    <div class="col-1/5">
                                        <span><a href="#" title="" class=""><img alt="client" class="img-Lazy" src="/Portals/_default/ContentBuilder/minis-page/home11/homepage11-client04.jpg"></a></span>
                                    </div>
                                    <div class="col-1/5">
                                        <span><a href="#" title="" class=""><img alt="client" class="img-Lazy" src="/Portals/_default/ContentBuilder/minis-page/home11/homepage11-client05.jpg"></a></span>
                                    </div>
                                </div>
                                <div class="spacer height-80 mb-10"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <style class="build-css" data-class="home11-clientlist">
            .home11-clientlist {
                text-align: center;
            }

            .home11-clientlist img {
                box-shadow: 0 0 10px rgba(0, 0, 0, .3);
                opacity: 0.3;
                transition: all .3s cubic-bezier(0.22, 0.61, 0.36, 1);
                margin-bottom: 30px;
            }

            .home11-clientlist a:hover img {
                opacity: 0.5;
            }
        </style>
    `
}, {
    'thumbnail': 'minis-section/home11/section-06.jpg',
    'category': '810',
    'html': `
        <div class="is-section is-box is-bg-grey is-align-center">
            <div class="is-overlay">
                <div class="is-overlay-bg" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/home11/homepage11-bg02.jpg&quot;);"></div>
            </div>
            <div class="is-boxes">
                <div class="is-box-centered">
                    <div class="is-container layout-container" style="max-width: 760px;">
                        <div class="row">
                            <div class="col-md-12">
                                <h6 class="pb-20 color-accent" style="font-weight: 400;">WE ARE PROFESSIONAL</h6>
                                <h2 style="font-weight: 600;">Provide Website Solutions With Powerful Technology.</h2>
                                <div class="spacer height-20 mb-5"></div>
                                <p style="color: rgb(102, 102, 102);">Morbi feugiat, nibh nec laoreet molis augue mi accumsan velitacscele. Risque turpis purus. ut odio. Donec ut massa est. Sed congue lorem id lorem maximus ultricies.</p>
                                <div class="spacer height-20 mb-15"></div>
                                <p class="mb-5">
                                    <a href="#" title="learn more" class="button-08">Buy It Now<i class="sico lnr-arrow-right"></i></a>
                                    <a href="#" title="Buy It Now" class="button-08">Contact Us<i class="sico lnr-arrow-right"></i></a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <style class="build-css" data-class="button-08">${csstemplate["button-08"]}</style>
    `
}, {
    'thumbnail': 'minis-section/home11/section-07.jpg',
    'category': '810',
    'html': `
        <div class="is-section is-box">
            <div class="is-boxes">
                <div class="is-box-centered">
                    <div class="is-container layout-container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="title-15 mb-25 mt-5">
                                    <h3>Our <b>Works</b></h3>
                                </div>
                                <p style="text-align: center; font-size:18px;">Morbi feugiat, nibh nec laoreet molis augue mi accumsan velitacscele.&nbsp;</p>
                                <div class="spacer height-40 mb-10"></div>
                            </div>
                        </div> 
                        <div class="row col-no-padding">
                            <div class="col-md-4">
                                <div class="img-box07">
                                    <span><a href="#" title="" class=""><img alt="Image" class="img-Lazy" src="/Portals/_default/ContentBuilder/minis-page/home11/homepage11-img11.jpg"></a></span>
                                    <div class="title">
                                        <h6>Ceramic bottle design</h6>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="img-box07">
                                    <span><a href="#" title="" class=""><img alt="Image" class="img-Lazy" src="/Portals/_default/ContentBuilder/minis-page/home11/homepage11-img12.jpg"></a></span>
                                    <div class="title">
                                        <h6>Ceramic bottle design</h6>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="img-box07">
                                    <span><a href="#" title="" class=""><img class="img-Lazy" alt="Image" src="/Portals/_default/ContentBuilder/minis-page/home11/homepage11-img13.jpg"></a></span>
                                    <div class="title">
                                        <h6>Ceramic bottle design</h6>
                                    </div>
                                </div>
                            </div>
                        </div> 
                        <div class="row col-no-padding">
                            <div class="col-md-4">
                                <div class="img-box07">
                                    <span><a href="#" title="" class=""><img class="img-Lazy" alt="Image" src="/Portals/_default/ContentBuilder/minis-page/home11/homepage11-img14.jpg"></a></span>
                                    <div class="title">
                                        <h6>Ceramic bottle design</h6>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="img-box07">
                                    <span><a href="#" title="" class=""><img class="img-Lazy" alt="Image" src="/Portals/_default/ContentBuilder/minis-page/home11/homepage11-img15.jpg"></a></span>
                                    <div class="title">
                                        <h6>Ceramic bottle design</h6>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="img-box07">
                                    <span><a href="#" title="" class=""><img class="img-Lazy" alt="Image" src="/Portals/_default/ContentBuilder/minis-page/home11/homepage11-img16.jpg"></a></span>
                                    <div class="title">
                                        <h6>Ceramic bottle design</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <style class="build-css" data-class="title-15">${csstemplate["title-15"]}</style>
        <style class="build-css" data-class="img-box07">${csstemplate["img-box07"]}</style>
    `
}, {
    'thumbnail': 'minis-section/home11/footer.jpg',
    'category': '810',
    'html': `
        <div class="is-section is-box is-bg-grey">
            <div class="is-overlay">
                <div class="is-overlay-bg" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/home11/homepage11-footer-bg.jpg&quot;);"></div>
            </div>
            <div class="is-boxes">
                <div class="is-box-centered">
                    <div class="is-container layout-container pt-10" dragwithouthandle="">
                        <div class="row">
                            <div class="col-md-6 text-center text-md-left"><img alt="Logo" class="img-Lazy" src="/Portals/_default/ContentBuilder/minis-page/home11/homepage11-logo.png">
                                <p class="mt-10">@ 2019 by DNNGo Corp</p>
                            </div>
                            <div class="col-md-6 text-center text-md-right pt-5 pt-md-15">
                                <div class="edit-box">
                                    <a class="social-08" href="#"><i class="sico fab-facebook-f size-24"></i></a>
                                    <a class="social-08" href="#"><i class="sico fab-twitter size-24"></i></a>
                                    <a class="social-08" href="#"><i class="sico fab-linkedin-in size-24"></i></a>
                                    <a class="social-08" href="#"><i class="sico fab-pinterest-p size-24"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12" data-noedit="">
                                <div class="line pt-30 mb-40" style="border-bottom:1px solid #e3d9d6"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-8">
                                <h5 class="pb-15">Links</h5>
                                <div class="row">

                                    <div class="col-md-4">
                                        <div class="home11-footer-links">
                                            <p><a href="#">What we do</a></p>
                                            <p><a href="#">Our service</a></p>
                                            <p><a href="#">Creative design</a></p>
                                            <p><a href="#">Industry experts</a></p>
                                            <p><a href="#">Feature</a></p>
                                            <p><a href="#">Website SEO</a></p>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="home11-footer-links">
                                            <p><a href="#">About Us</a></p>
                                            <p><a href="#">Contact Us</a></p>
                                            <p><a href="#">Term &amp; Conditions</a></p>
                                            <p><a href="#">Task Management</a></p>
                                            <p><a href="#">FAQs</a></p>
                                            <p><a href="#">Project Management</a></p>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="home11-footer-links">
                                            <p><a href="#">Blogs</a></p>
                                            <p><a href="#">Portfolios</a></p>
                                            <p><a href="#">Online Documentation</a></p>
                                            <p><a href="#">Elements</a></p>
                                            <p><a href="#">Live Demo</a></p>
                                            <p><a href="#">Help Center</a></p>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="col-md-4">
                                <h5 class="pb-15">Contact Info</h5>
                                <b style="font-size: 18px; color: rgb(66, 66, 66); font-weight:600;">Phone Number:</b>
                                <p style="font-size: 22px;" class="color-accent">(845) 359-7777</p>
                                <b style="font-size: 18px; color: rgb(66, 66, 66); font-weight:600;">Email:</b>
                                <p>dnnskindev@gmail.com / admin@dnngo.net</p>
                                <b style="font-size: 18px; color: rgb(66, 66, 66); font-weight:600;">Address:</b>
                                <p>252 NY-303, Orangeburg, NY 10965, Yanta W Rd</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <style class="build-css" data-class="social-08">${csstemplate["social-08"]}</style>
        <style class="build-css" data-class="home11-footer-links">
            .home11-footer-links p {
                margin-bottom: 10px;
            }

            .home11-footer-links p a,
            .home11-footer-links p a:link,
            .home11-footer-links p a:active,
            .home11-footer-links p a:visited {
                color: #666666;
            }

            .home11-footer-links p a:hover {
                color: var(--accent-color);
                text-decoration: underline;
            }
        </style>
    `
})


/*Home12*/

data_basic.designs.push({
    'thumbnail': 'minis-section/home12/banner.jpg',
    'category': '811',
    'html': `
    <div class="is-section is-box is-section-100 is-light-text">
        <div class="is-overlay">
            <div class="is-overlay-bg" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/home12/homepage12-banner.jpg&quot;);"></div>
        </div>
        <div class="is-boxes">
            <div class="is-box-centered">
                <div class="is-container layout-container" dragwithouthandle="">
                    <div class="row align-items-center">
                        <div class="col-md-6 text-center text-md-right">
                            <h1 style="font-size: 70px; font-weight: 700; letter-spacing: 5px;">BEST SELLER</h1>
                            <h2 class="mb-35" style="letter-spacing: 1px;  font-weight: 600;">Limited Time Promotion</h2>
                            <p class="text-center text-md-right"><a class="button-01 border-radius-0" href="#" title="">LEARN MORE</a></p>
                        </div>
                        <div class="col-md-6 text-center">
                            <div class="home12-banner-info">
                                <p class="number" style="font-family: &quot;Julius Sans One&quot;, sans-serif;">70</p>
                                <p class="title">SAVE UP TO</p>
                                <p class="unit" style="font-family: &quot;Poiret One&quot;, cursive; font-weight: 600;">%<br>OFF</p>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div >
    <link href="https://fonts.googleapis.com/css?family=Poiret%20One" type="text/css" rel="stylesheet" />
    <style class="build-css" data-class="button-01">${csstemplate["button-01"]}</style>
    <style class="build-css" data-class="home12-banner-info">
    .home12-banner-info {
        position: relative;
        line-height: 1;
        display: inline-block;
    }

    .home12-banner-info .number {
        font-size: 240px;
        color: #ffc229;
        font-weight: 100;
    }

    .home12-banner-info .title {
        font-size: 26px;
        color: #ffffff;
        position: absolute;
        font-weight: bold;
        top: 50%;
        left: -28%;
    }
    .home12-banner-info .unit {
        font-size: 50px;
        text-align: center;
        color: #ffc229;
        position: absolute;
        left: 90%;
        bottom: 33px;
        line-height: 1.2;
    }
    @media (min-width: 1200px) {
        .home12-banner-info {
            margin-top: -180px;
        }
    }
    @media (max-width:991px) {
        .home12-banner-info .number {
            font-size: 200px;
        }

        .home12-banner-info .unit {
            font-size: 30px;
        }
    }
    @media (max-width:767px) {
        .home12-banner-info .title {
            font-size: 18px;
            left: -12%;
        }
    }
    </style>
    `
}, {
    'thumbnail': 'minis-section/home12/section-01.jpg',
    'category': '811',
    'html': `
    <div class="is-section is-box ">
        <div class="is-boxes">
            <div class="is-box-centered">
                <div class="is-container layout-container mb-25 mb-md-65" dragwithouthandle="">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="title-14 text-left mt-md-20">
                                <small class="color-accent">About us</small>
                                <h3>We Provide Professional Furniture Website</h3>
                            </div>
                            <p>Praesent ornare, orci quis euismod semper, nisi justo tempor eros, eget blandit nunc mauris sit amet risus. Maecenas elit lortristique cursus. </p>
                            <p class="text-left"><a class="button-01 border-radius-0 mt-30" href="#" title="">LEARN MORE</a></p>
                            <div class="spacer height-60"></div>
                        </div>
                        <div class="col-lg-8">
                            <div class="row ml-xl-15">
                                <div class="col-md-6">
                                    <div class="iconbox-13 edit-box active">
                                        <div class="icon"><span class="Lazy-loading" style="width:112px"></span><img alt="" class="img-Lazy" src="/Portals/_default/ContentBuilder/minis-page/home12/homepage12-icon01.png"></div>
                                        <div class="title">
                                            <h6>Soft Texture</h6>
                                        </div>
                                        <p>Morbi porttitor tellus in enim convalli sacort a faucibus. Vestibu lum sitae tellu placerali, uarisuset, tempus felis laoreet.</p>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="iconbox-13 edit-box">
                                        <div class="icon"><span class="Lazy-loading" style="width:112px"></span><img alt="" class="img-Lazy" src="/Portals/_default/ContentBuilder/minis-page/home12/homepage12-icon02.png"></div>
                                        <div class="title">
                                            <h6>Delicate Touch</h6>
                                        </div>
                                        <p>Vestibulum sit amet tellus placerat, aliquam risus et, tempus felis. Suspe Fandisse mollis nisleuelit ipsum id sagittis.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row ml-xl-15">
                                <div class="col-md-6">
                                    <div class="iconbox-13 edit-box">
                                        <div class="icon"><span class="Lazy-loading" style="width:112px"></span><img alt="" class="img-Lazy" src="/Portals/_default/ContentBuilder/minis-page/home12/homepage12-icon03.png"></div>
                                        <div class="title">
                                            <h6>Unique Design</h6>
                                        </div>
                                        <p>Morbi porttitor tellus in enim convalli sacort a faucibus. Vestibu lum sitae tellu placerali, uarisuset, tempus felis laoreet.</p>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="iconbox-13 edit-box">

                                        <div class="icon"><span class="Lazy-loading" style="width:112px"></span><img alt="" class="img-Lazy" src="/Portals/_default/ContentBuilder/minis-page/home12/homepage12-icon04.png"></div>
                                        <div class="title">
                                            <h6>Good Quality</h6>
                                        </div>
                                        <p>Vestibulum sit amet tellus placerat, aliquam risus et, tempus felis. Suspe Fandisse mollis nisleuelit ipsum id sagittis.</p>

                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style class="build-css" data-class="title-14">${csstemplate["title-14"]}</style>
    <style class="build-css" data-class="button-01">${csstemplate["button-01"]}</style>
    <style class="build-css" data-class="iconbox-13">${csstemplate["iconbox-13"]}</style>
    `
}, {
    'thumbnail': 'minis-section/home12/section-02.jpg',
    'category': '811',
    'html': `
    <div class="is-section is-box is-bg-grey is-align-center is-light-text">
        <div class="is-overlay">
            <div class="is-overlay-bg" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/home12/homepage12-bg01.jpg&quot;);"></div>
        </div>
        <div class="is-boxes">
            <div class="is-box-centered">
                <div class="is-container layout-container" dragwithouthandle="">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="spacer height-20"></div>
                            <h4 style="color: #fef500; font-weight: 400; font-size:28px;">Modern concept</h4>
                            <h1 class="size-50 mt-15" style="font-weight: 600;">Create Cozy Aura With Simple Furniture</h1>
                            <div class="spacer height-120"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    `
}, {
    'thumbnail': 'minis-section/home12/section-03.jpg',
    'category': '811',
    'html': `
    <div class="is-section is-box">
        <div class="is-boxes">
            <div class="is-box-centered">
                <div class="is-container layout-container mb-20 mb-md-55" dragwithouthandle="">
                    <div class="row justify-content-center">
                        <div class="col-md-12">
                            <div class="title-05 mt-10" style="max-width: 700px;">
                                <small class="color-accent">HOT PRODUCTS</small>
                                <h3 style="font-weight:600;">Theses Are Some Of The Most Popular Furniture Products By Customers</h3>
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-md-6 col-lg-8">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="homepage12-imgbox01">
                                        <span class="Lazy-loading" style="width:370px"></span><img class="img-Lazy" alt="" src="/Portals/_default/ContentBuilder/minis-page/home12/homepage12-img01.jpg">
                                        <div class="edit-box">
                                            <h6 class="subtitle">Latest product</h6>
                                            <h4 class="title">Sofa <span class="color-accent">Chair</span></h4>
                                            <p class="link"><a href="#" title="Read More">Read More <i class="sico lnr-arrow-right"></i></a></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="homepage12-imgbox02">
                                        <span class="Lazy-loading" style="width:370px"></span><img alt="" class="img-Lazy" src="/Portals/_default/ContentBuilder/minis-page/home12/homepage12-img02.jpg">
                                        <div class="edit-box">
                                            <h6 class="subtitle">The most popular</h6>
                                            <h4 class="title">Recreational Chair</h4>
                                            <p class="link"><a href="#" title="Read More">Read More <i class="sico lnr-arrow-right"></i></a></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="homepage12-imgbox03">
                                        <span class="Lazy-loading" style="width:769px"></span><img alt="" class="img-Lazy" src="/Portals/_default/ContentBuilder/minis-page/home12/homepage12-img04.jpg">
                                        <div class="edit-box">
                                            <h6 class="subtitle">HOT STYLE DOUBLE SOFA</h6>
                                            <h4 class="title">SAVE</h4>
                                            <p class="link"><a href="#" title="Read More">Buy Now <i class="sico lnr-arrow-right"></i></a> <i class="number">50%</i></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-4">
                            <div class="homepage12-imgbox04">
                                <span class="Lazy-loading" style="width:370px"></span><img alt="" class="mb-30 img-Lazy" src="/Portals/_default/ContentBuilder/minis-page/home12/homepage12-img03.jpg">
                                <div class="edit-box">
                                    <h2 class="title">Single Sofa</h2>
                                    <h5 class="subtitle">FLASH SALE</h5>
                                    <p class="link"><a href="#" title="Read More">Buy Now <i class="sico lnr-arrow-right"></i></a></p>
                                </div>
                            </div>
                        </div>
                    </div>

                    
                </div>
            </div>
        </div>
    </div>
    <style class="build-css" data-class="title-05">${csstemplate["title-05"]}</style>
    <style  class="build-css" data-class="homepage12-imgbox01">
    .homepage12-imgbox01,
    .homepage12-imgbox02,
    .homepage12-imgbox03,
    .homepage12-imgbox04 {
        position: relative;
        margin: 0 0 30px;
    }
    .homepage12-imgbox01 .subtitle,
    .homepage12-imgbox02 .subtitle {
        position: absolute;
        top: 15%;
        left: 54%;
        font-weight: 400;
        font-size: 18px;
        margin: 0;
        color: #555555;
        white-space: nowrap
    }
    .homepage12-imgbox01 .title,
    .homepage12-imgbox02 .title {
        position: absolute;
        top: 24%;
        left: 54%;
    }
    .homepage12-imgbox01 .link {
        position: absolute;
        top: 63%;
        left: 66%;
        white-space: nowrap;
    }
   
    .homepage12-imgbox01 .link a i,
    .homepage12-imgbox02 .link a i,
    .homepage12-imgbox03 .link a i,
    .homepage12-imgbox04 .link a i {
        vertical-align: middle;
        font-size: 1.3em;
        margin-bottom: -2px;
        display: inline-block;
    }
    .homepage12-imgbox01 .link a:hover,
    .homepage12-imgbox02 .link a:hover,
    .homepage12-imgbox03 .link a:hover,
    .homepage12-imgbox04 .link a:hover {
        text-decoration: underline;
    }

    .homepage12-imgbox02 .link {
        position: absolute;
        top: 63%;
        left: 54%;
    }
    .homepage12-imgbox03 .subtitle {
        position: absolute;
        top: 15%;
        left: 62%;
        font-weight: 400;
        font-size: 18px;
        margin: 0;
        color: #555555;
    }
    .homepage12-imgbox03 .title {
        position: absolute;
        top: 24%;
        left: 62%;
        font-size: 52px;
    }
    .homepage12-imgbox03 .link {
        position: absolute;
        top: 55%;
        left: 62%;
        line-height: 1;
    }
    .homepage12-imgbox03 .link a{
        display: inline-block;
    }
    .homepage12-imgbox03 .number {
        color: #df0b0b;
        font-size: 70px;
        font-weight: 700;
        vertical-align: sub;
        display: inline-block;
        margin-left: 10px;
    }
    .homepage12-imgbox04 .title {
        position: absolute;
        top: 32%;
        left: 0;
        font-size: 40px;
        color: #ffffff;
        width: 100%;
        text-align: center;
    }
    .homepage12-imgbox04 .subtitle {
        position: absolute;
        top: 43%;
        left: 0;
        font-size: 26px;
        color: #fbf200;
        width: 100%;
        text-align: center;
    }
    .homepage12-imgbox04 .link {
        position: absolute;
        top: 52%;
        left: 0;
        width: 100%;
        text-align: center;
    }
    .homepage12-imgbox04 .link a,
    .homepage12-imgbox04 .link a:link,
    .homepage12-imgbox04 .link a:visited {
        color: #fff;
    }
    @media only screen and (min-width:992px) and (max-width:1024px) {
        .homepage12-imgbox01 .subtitle,
        .homepage12-imgbox02 .subtitle,
        .homepage12-imgbox03 .subtitle {
            font-size: 16px;
        }

        .homepage12-imgbox01 .title,
        .homepage12-imgbox02 .title {
            font-size: 22px;
        }

        .homepage12-imgbox01 .link {
            left: 60%;
        }

        .homepage12-imgbox03 .title {
            font-size: 44px;
        }

        .homepage12-imgbox03 .number {
            font-size: 50px;
        }

    }
    @media only screen and (min-width:768px) and (max-width:991px) {
        .homepage12-imgbox03 {
            width: calc(100vw - 38px);
        }
    }
    @media only screen and (max-width:767px) {
        .homepage12-imgbox03 .subtitle {
            font-size: 12px;
        }

        .homepage12-imgbox03 .title {
            font-size: 30px;
            top: 40%;
        }

        .homepage12-imgbox03 .link {
            top: 80%;
            font-size: 13px;
        }

        .homepage12-imgbox03 .number {
            font-size: 20px;
            vertical-align: middle;
            margin: 0;
        }

    }
    </style>
    `
}, {
    'thumbnail': 'minis-section/home12/section-04.jpg',
    'category': '811',
    'html': `
    <div class="is-section is-box" style="background-color: rgb(246, 245, 248);">
        <div class="is-boxes">
            <div class="is-box-centered">
                <div class="is-container layout-container m-auto" dragwithouthandle="">
                    <div class="row align-items-center">
                        <div class="col-md-7 pt-80 pb-md-80 pr-xl-80">
                            <div class="spacer height-20"></div>
                            <div class="title-16">
                                <small>New Product Seckilling</small>
                                <h1 class="size-50 mb-25">Simple <span class="color-accent">Bedroom LaCmp</span></h1>
                            </div>
                            <p>Morbi porttitor tellus inenim convalli sacort a faucibus. Vestibu lumsitae telluplcer aliuar suset, tempus felis laoreet. Fusce fringilla mi mollis enim auctor consequat. </p>
                            <p><a class="button-01 border-radius-0 mt-30" href="#" title="">REMIND ME</a></p>
                            <div class="spacer height-20 mb-15"></div>
                            ${getSnippetCode("countdown-02")}

                            <div class="spacer height-20"></div>
                        </div>
                        <div class="col-md-5">
                            <div class="homepage12-productbox">
                                <span class="Lazy-loading" style="width:856px"></span><img alt="" class="img-Lazy" src="/Portals/_default/ContentBuilder/minis-page/home12/homepage12-img13.png">
                                <p class="price">$29</p>
                            </div>                    
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style class="build-css" data-class="title-16">${csstemplate["title-16"]}</style>
    <style class="build-css" data-class="button-01">${csstemplate["button-01"]}</style>
    <style class="build-css" data-class="homepage12-productbox">
    .homepage12-productbox {
        position: relative;
    }
    .homepage12-productbox img {
        max-width: 50vw;
    }
    .homepage12-productbox .price {
        width: 2.37em;
        height: 2.37em;
        line-height: 2.37em;
        text-align: center;
        background-color: var(--accent-color);
        border-radius: 50%;
        font-size: 70px;
        color: #ffffff;
        font-weight: 700;
        position: absolute;
        top: 24%;
        left: 121%;
    }

    @media only screen and (max-width:1650px) {
        .homepage12-productbox .price {
            left: auto;
            right: -10%;
            font-size: 56px;
        }
    }

    @media only screen and (max-width:1200px) {
        .homepage12-productbox .price {
            top: 15%;
            left: auto;
            right: 0%;
            font-size: 38px;
        }
    }

    @media only screen and (max-width:767px) {
        .homepage12-productbox img {
            max-width: 100%;
        }

        .homepage12-productbox .price {
            top: 24%;
            left: auto;
            right: 10%;
            font-size: 30px;
        }
    }
    </style>   
    `
}, {
    'thumbnail': 'minis-section/home12/section-05.jpg',
    'category': '811',
    'html': `
    <div class="is-section is-box">
        <div class="is-boxes">
            <div class="is-box-centered">
                <div class="is-container layout-container mb-30 mb-md-70" dragwithouthandle="">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="title-05 mt-10" style="max-width: 700px;">
                                <small class="color-accent">FEATURED PRODUCTS</small>
                                <h3 style="font-weight:600;">Have A Look At The Best-sell Collections Which Are Fashionable and Simple</h3>
                            </div>
                        </div>
                    </div>                   
                    ${getSnippetCode("img-box08")}
                </div>
            </div>
        </div>
    </div>
    <style class="build-css" data-class="title-05">${csstemplate["title-05"]}</style>
    `
}, {
    'thumbnail': 'minis-section/home12/section-06.jpg',
    'category': '811',
    'html': `
    <div class="is-section section-no-padding is-box is-bg-grey is-section-auto">
        <div class="is-boxes">
            <div class="is-box-centered">
                <div class="is-container no-space layout-container is-container-fluid" dragwithouthandle="">
                    <div class="row">
                        <div class="col-md-12">
                        <div class="custom-module loading" data-effect="gmap" id="module-{id}" data-moduleid="{id}" data-settings="%7B%22address%22%3A%22740%20Prince%20Avenue%20Athens%2C%20Georgia%2030601%2C%20740%20Prince%20Ave%2C%20Athens%2C%20GA%2030606%2C%20740%22%2C%22markerscolor%22%3A%22%23303030%22%2C%22markersbg%22%3A%22%23ffffff%22%2C%22markersicon%22%3A%22%5BSkinPath%5Dresource%2Fthumbnails%2Fmap%2Fmapicon06.png%22%2C%22zoom%22%3A%2216%22%2C%22linktype%22%3A%22roadmap%22%2C%22xxl%22%3A%22700%22%2C%22xl%22%3A%22665%22%2C%22l%22%3A%22665%22%2C%22m%22%3A%22665%22%2C%22s%22%3A%22400%22%2C%22xs%22%3A%22360%22%2C%22type%22%3A%22roadmap%22%2C%22position%22%3A%22right%22%7D"></div>
                            <div class="homepage12-map-info">
                                <h4 class="mb-20">Contact <span class="color-accent">Information</span></h4>
                                <dl>
                                    <dt>Address:</dt>
                                    <dd>252 NY-3v03, Orangeburg, NY 10965, Yanta W Rd</dd>
                                    <dt>Email:</dt>
                                    <dd>dnnskindev@gmail.com<br>
                                        admin@dnngo.net</dd>
                                    <dt>Telephone Number:</dt>
                                    <dd> (+123)-456-789-143</dd>
                                </dl>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style class="homepage12-map-info" data-class="title-05">
    .homepage12-map-info {
        position: absolute;
        top: 15%;
        right: 19.5%;
        box-shadow: 0 0 15px rgba(0, 0, 0, .1);
        background-color: #FFF;
        padding: 41px 36px;
        max-width: 370px;
    }
    .homepage12-map-info dt {
        font-size: 18px;
        color: #424242;
    }
    .homepage12-map-info dd {
        border-bottom: 1px solid #ebebeb;
        margin-bottom: 18px;
        padding-bottom: 16px;
    }
    .homepage12-map-info dd:last-child {
        margin-bottom: 0;
        padding-bottom: 0;
        border-bottom: 0;
    }
    @media only screen and (max-width:1400px) {
        .homepage12-map-info {
            right: 8%;
        }
    }
    @media only screen and (max-width:991px) {
        .homepage12-map-info {
            right: 3%;
        }
    }

    @media only screen and (max-width:767px) {
        .homepage12-map-info {
            position: static;
            max-width: initial;
            padding: 25px;
        }
    }
    </style>
    `
}, {
    'thumbnail': 'minis-section/home12/footer.jpg',
    'category': '811',
    'html': `
    <div class="is-section is-box">
        <div class="is-boxes">
            <div class="is-box-centered">
                <div class="is-container layout-container mb-20" dragwithouthandle="">
                ${getSnippetCode("iconbox-14")}
                    <div class="row">
                        <div class="col-md-12">
                            <div class="spacer height-40 mb-30" style="border-top: 1px solid rgb(222, 222, 222);"></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-lg-8">
                            <div class="row">
                                <div class="col-md-4">
                                    <h6 class="title-17">About Us</h6>
                                    <div class="home12-footer-links">
                                        <p><a href="#">What we do</a></p>
                                        <p><a href="#">Our features</a></p>
                                        <p><a href="#">Our team</a></p>
                                        <p><a href="#">Reasonable price</a></p>
                                    </div>
                                    <div class="spacer height-20"></div>
                                </div>
                                <div class="col-md-4">
                                    <h5 class="title-17">Our Service</h5>
                                    <div class="home12-footer-links">
                                        <p><a href="#">DNN website design</a></p>
                                        <p><a href="#">SEO optimization</a></p>
                                        <p><a href="#">UI design</a></p>
                                        <p><a href="#">APP design</a></p>
                                    </div>
                                    <div class="spacer height-20"></div>
                                </div>
                                <div class="col-md-4">
                                    <h5 class="title-17">Friendly links</h5>
                                    <div class="home12-footer-links">
                                        <p><a href="#">Industry experts</a></p>
                                        <p><a href="#">FAQs</a></p>
                                        <p><a href="#">Contact us</a></p>
                                        <p><a href="#">Recent posts</a></p>
                                    </div>
                                    <div class="spacer height-20"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-4">
                            <h5 class="title-17">Subscription</h5>
                            <p>Suspendisse ut tristique turpis. Duis libero ligula, suscipit at urna elementum.</p>
                            <div class="spacer height-20"></div>
                            ${getSnippetCode("ajaxform-02")}
                            <div class="spacer height-20"></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="spacer height-20 mb-20" style="border-bottom: 1px solid rgb(222, 222, 222);"></div>
                        </div>
                    </div>
                    <div class="row align-items-center">
                        <div class="col-md-6">
                            <p class="mb-0">@ 2019 by DNNGo Corp. All Rights Resevered</p>
                        </div>
                        <div class="col-md-6 text-right">
                            <div class="homepage12-iconlink">
                                <a href="#" class=""><img alt="" class="img-Lazy" src="/Portals/_default/ContentBuilder/minis-page/home12/homepage12-icon05.png"></a>
                                <a href="#" class=""><img alt="" class="img-Lazy" src="/Portals/_default/ContentBuilder/minis-page/home12/homepage12-icon06.png"></a>
                                <a href="#" class=""><img alt="" class="img-Lazy" src="/Portals/_default/ContentBuilder/minis-page/home12/homepage12-icon07.png"></a>
                                <a href="#" class=""><img alt="" class="img-Lazy" src="/Portals/_default/ContentBuilder/minis-page/home12/homepage12-icon08.png"></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style class="build-css" data-class="title-17">${csstemplate["title-17"]}</style>
    <style class="build-css" data-class="home12-footer-links">
    .home12-footer-links p {
        margin-bottom: 10px;
    }
    .home12-footer-links p a,
    .home12-footer-links p a:link,
    .home12-footer-links p a:active,
    .home12-footer-links p a:visited {
        color: #666666;
    }
    .home12-footer-links p a:hover {
        color: var(--accent-color);
        text-decoration: underline;
    }
    </style>   
    <style class="build-css" data-class="homepage12-iconlink">
    .homepage12-iconlink a {
        margin-left: 12px;
    }
</style>   
    `
})


/*Home13*/

data_basic.designs.push({
    'thumbnail': 'minis-section/home13/banner.jpg',
    'category': '812',
    'html': `<div class="is-section is-box is-section-100 is-section-md-auto homepage13-banner is-align-center is-light-text">
    <div class="is-overlay">
        <div class="is-overlay-bg" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/home13/homepage13-banner.jpg&quot;);"></div>
    </div>
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container mb-0">
                <div class="row align-items-end justify-content-center">
                    <div class="col-md-12">
                        <div class="spacer height-40 d-none d-md-block"></div>
                        <h1 style="font-weight: 700;" class="mb-20">Awesome App Template</h1>
                        <h3 style="color: rgb(189, 188, 255); font-weight: 300;">Visitors Linger In Its <b style=" font-style: italic; " class="custom-module loading" data-effect="texttabs" id="module-{id}" data-module-desc="Text Slider" data-moduleid="{id}" data-settings="%7B%22moduleid%22%3A%22sgDwM2%22%2C%22items%22%3A%7B%22item0%22%3A%7B%22title%22%3A%22Unique%22%7D%2C%22item1%22%3A%7B%22title%22%3A%22Modern%22%7D%2C%22item2%22%3A%7B%22title%22%3A%22Stylish%22%7D%7D%7D"></b> Design And Features</h3>
                        <div class="spacer height-20"></div>
                        <div class="spacer height-20"></div>

                        <p><a class="button-09 mb-25 mr-25" href="#" title="">BUY IT NOW</a> <a class="button-10 mb-25" href="#" title=""><i class="sico fas-play"></i>WATCH VIDEO</a> </p>
                        <div class="spacer height-80 mb-10 d-none d-md-block"></div>
                    </div>
                </div>
                <div class="row align-items-end row align-items-end justify-content-center col-no-padding">
                    <div class="col-12"><img style="margin-bottom: -10%; position: relative;z-index:1" class="img-Lazy" src="/Portals/_default/ContentBuilder/minis-page/home13/homepage13-banner-img.png" alt="banner"></div>

                </div>
            </div>

        </div>
    </div>
</div>
<style class="build-css" data-class="button-09">${csstemplate["button-09"]}</style>
<style class="build-css" data-class="button-10">${csstemplate["button-10"]}</style>

    <style class="build-css" data-class="homepage13-banner">
        .homepage13-banner .is-overlay::after {
            content: "";
            width: 100%;
            padding-bottom: 10%;
            position: absolute;
            bottom: -1px;
            left: 0;
            pointer-events: none;
            background: url('data:image/svg+xml;utf8,<svg width="1500" height="148" xmlns="http://www.w3.org/2000/svg"><path fill="%23FFFFFF" d="M0,29.2c0,0,70.3,15,185.2-26.2c0,0,59.4-15.5,91.5,25 c0,0,77.9,117,203.2,51.5c0,0,65.6-47.4,101.2-62.2c0,0,72.4-39.4,150.7,6c0,0,59.9,38.8,77.2,52.5c0,0,103.5,107.8,230.2,56.2 c0,0,23.6-6.7,62.2-31.5c0,0,52.6-37.6,96-52.5c0,0,43.5-27.5,121.5-4.5c0,0,80.6,38.1,147.7-0.8c0,0,10.1-4.8,33-20.2v126.2H0V29.2z"></path></svg>');
            background-position: center top;
            background-size: cover;
            pointer-events: none;
        }
    </style>
    `
}, {
    'thumbnail': 'minis-section/home13/section-01.jpg',
    'category': '812',
    'html': `<div class="is-section is-box is-align-center">
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="spacer height-20 d-none d-md-block"></div>
                        <h3 class="title-18 m-auto" style="max-width: 550px;">We Provide Website <span>Solutions With</span> Powerful Technology</h3>
                        <div class="spacer height-40 mb-15"></div>

                        <p class="m-auto" style="max-width: 840px;">Sed nibh justo, posuere in ullamcorper eget, finibus ut turpis. Etiam odio nulla, venenatis vel pulvinar interdum, varius quis turpis. Cras nunc dui, finibus sit amet interdum non, luctus sit amet mauris.</p>
                        <div class="spacer height-60 mb-5"></div>
                    </div>
                </div>
                ${getSnippetCode("iconbox-15")}
                <div class="row">
                    <div class="col-md-12"><a class="button-01" href="#" title="">CONTACT US</a></div>
                </div>
            </div>
        </div>
    </div>
</div>
<style class="build-css" data-class="title-18">${csstemplate["title-18"]}</style>
<style class="build-css" data-class="button-01">${csstemplate["button-01"]}</style>
    `
}, {
    'thumbnail': 'minis-section/home13/section-02.jpg',
    'category': '812',
    'html': `
    <div class="is-section is-box section-no-padding is-light-text homepage13-section02">
    <div class="is-overlay">
        <div class="is-overlay-bg" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/home13/homepage13-bg01.jpg&quot;);"></div>
    </div>
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container is-container-fluid no-space">
                <div class="row align-items-center justify-content-center">
                    <div class="col-md-12 col-lg-8 col-xl-4 pl-30 pl-30 pl-md-80">
                        <div class="spacer mt-80 height-80"></div>
                        <h1 style="font-weight: 700;">Pretty Interactive Mobile App Designs</h1>
                        <div class="spacer height-20 mb-10"></div>
                        <p style="font-size: 18px;" data-keep-font-size="">Integer at lobortis leo, a ornare magna. Praesent laoreet ipsum non congue semper. Nullam orci nisl, viverra ut tellus vitae, accumsan rutrum miadmin haveds yourd danser.</p>
                        <div class="spacer height-20"></div>
                        <p><a href="#" title="" class="mr-15 mb-15 d-inline-block" style="
"><img class="img-Lazy" src="/Portals/_default/ContentBuilder/minis-page/home13/homepage13-btn01.png" alt="Button"></a> <a href="#" title="" class="mr-15 mb-15 d-inline-block"><img class="img-Lazy" src="/Portals/_default/ContentBuilder/minis-page/home13/homepage13-btn02.png" alt="Button"></a></p>
                    </div>
                    <div class="col-md-12 col-xl-8">
                        <div class="spacer d-none d-xl-block height-80"></div>
                        <div class="">
                            <img class="img-Lazy" src="/Portals/_default/ContentBuilder/minis-page/home13/homepage13-img01.png" alt="Image"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style class="build-css" data-class="homepage13-section02">
.homepage13-section02 .is-overlay::after{
    content: "";
    width: 100%;
    background: url('data:image/svg+xml;utf8,<svg width="2000" height="265" xmlns="http://www.w3.org/2000/svg"><defs><linearGradient id="grad1" x1="0" y1="0" x2="100%25" y2="0"><stop offset="0" style="stop-color:%23e3f2f4;" ></stop><stop offset="30%25" style="stop-color:%23e3f2f4;" ></stop><stop offset="100%25" style="stop-color:%23e7eaf6;" ></stop></linearGradient><linearGradient id="grad2" x1="0" y1="0" x2="100%25" y2="0"><stop offset="0" style="stop-color:%232fb2bd;" ></stop><stop offset="50%25" style="stop-color:%23309ec2;" ></stop><stop offset="100%25" style="stop-color:%230e37d2;" ></stop></linearGradient></defs><path fill="%23ffffff" d="M2000,0c0,10,0,160.5,0,170.5c-49.5-3-536-31.5-536-31.5l-85.875-11.875L1294,122l-92.125,8.25L1118,145  l-556,2l-46.5-17.75L400,91L0,67.417C0,67.417,0,18.67,0,0C666.7,0,1333.29,0,2000,0z" ></path><path fill="%23e3f2f4" d="M379,66c6.88,8.12,21,24,21,24L200,79.208L0,77c0,0,0-14.67,0-22c10.84,0.49,19.47,3.19,31,3  c11.67,1.67,30.33,1.67,42,0c79.24-1.76,132.07-29.93,201-42c6.5-0.375,12.792-0.5,19,0c29.38,2.62,48.48,15.52,66,30  C365.72,52.61,372.39,59.28,379,66z" ></path><path  fill="url(%23grad1)" d="M878,18c111.57,10.43,161.48,82.52,240,126c-98,32.5-217.458,47.209-271.844,50.625  C840,195.563,839.458,195.063,835,195c-109.41,2.07-202.62-12.04-273-49c65.16-3.96,102.23-42.89,146-71  c44.86-28.81,88.47-55.6,159-57C870.67,18,874.33,18,878,18z" ></path><path fill="%23e7eaf6" d="M2000,29c0,51,0,102,0,153c-39.689,11.311-80.54,21.46-130,23c-3,0-15.875,2-36,1  c-25.125,1.334-25.125,1.334-63-3c-115.46-8.54-213.37-34.63-307-65c59.061-35.89,117.08-86.48,206-92  c71.57-4.45,114.9,31.02,182,35c9.188,0.669,9.875,0.583,20,0C1928.04,77.04,1966.29,55.29,2000,29z" ></path><path  fill="url(%23grad2)" d="M2000,182c0,4,0,8,0,12c-78.04,26.66-171.25,22.42-264,13c-78.65-7.99-165.939-28.92-233-52  c-72.46-24.93-165.939-34.48-255-28c-45.67,3.32-85.66,10.02-125,19c-38.38,8.76-75.11,20.43-115,29  c-125.53,26.96-272.45,32.1-386-2c-34.88-10.48-66.82-28.51-103-41C378.71,83.55,181.12,71.56,0,90c0-4.33,0-8.67,0-13  c36.67-5.66,76.21-8.46,117-10c29.25-2.125,55-1,77-1c9.104,0.083,146.93,11.74,206,24c59.1,13.57,112.97,32.36,162,56  c70.38,36.96,163.59,51.07,273,49c126.417-8.666,194.833-26.167,283-51c54.85-11.48,110.82-21.85,176-23c12,0,24,0,36,0  c49.59,0.75,93.1,7.57,134,17c93.63,30.37,191.54,56.46,307,65c29.416,3,68.083,3.904,99,2  C1919.46,203.46,1960.311,193.311,2000,182z" ></path></svg>') ;
    background-size: cover;
    padding-bottom: 11.5%;
    background-position: center top;
    top: -1px;
    left: 0;
    position: absolute;
    pointer-events: none;
}
</style>
`
}, {
    'thumbnail': 'minis-section/home13/section-03.jpg',
    'category': '812',
    'html': `
    <div class="is-section is-box">
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container mb-0">
                <div class="row">
                    <div class="col-md-12">
                        <div class="spacer height-20 d-none d-lg-block"></div>
                        <div class="mb-5 d-lg-none "></div>
                    </div>
                </div>
                <div class="row align-items-center" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/home13/homepage13-bg02.png&quot;); background-position: right bottom; background-repeat: no-repeat; background-size: auto;">
                    <div class="col-md-6 pr-lg-70 left-full-column">
                        <div class="full-column-inner text-right"><img class="img-Lazy" src="/Portals/_default/ContentBuilder/minis-page/home13/homepage13-img02.png" alt="Image"></div>
                    </div>
                    <div class="col-md-6">
                        <div class="title-03">
                            <h3 style="max-width: 383px;">The Best Mobile Apps For Your Businesses</h3>
                        </div>
                        <p>Mauris blandit tempor magna, at efficitur lacus tristique aliquet. dolor vel egestas facilisis, lectus dolor cursus tellus.</p>
                        <div class="spacer height-20 mb-10"></div>
                        ${getSnippetCode("counter-05")}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style class="build-css" data-class="title-03">${csstemplate["title-03"]}</style>
`
}, {
    'thumbnail': 'minis-section/home13/section-04.jpg',
    'category': '812',
    'html': `
    <div class="is-section is-box">
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container mt-20 mt-md-45">
                <div class="row align-items-center">
                    <div class="col-md-6">
                        <div class="title-03 pr-lg-40">
                            <h3>Creative Design Makes Your APP Have A Brand New Look</h3>
                        </div>
                        <p>Mauris blandit tempor magna, at efficitur lacus tristique aliquet. dolor vel egests facilisis, lectus dolor cursus tellus.</p>
                        <div class="spacer height-20"></div>
                        <div class="spacer height-20"></div>
                        <ul class="infolist-01">
                            <li class="edit-box">
                                <div class="number"><span>01</span></div>
                                <h6 class="title">Creative Production</h6>
                                <p>Sed eget turpis purus. Donec at amet consequat pellentesque.</p>
                            </li>
                            <li class="edit-box">
                                <div class="number"><span>02</span></div>
                                <h6 class="title">Working Plan</h6>
                                <p>Vestibulum ac luctus felis. Nulla nec neque maximus hendrerit.</p>
                            </li>
                            <li class="edit-box">
                                <div class="number"><span>03</span></div>
                                <h6 class="title">SEO Analyzation</h6>
                                <p>Cras egestas enim turpis, non elementum lorem varius nec.</p>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-6 right-full-column">
                        <div class="full-column-inner pl-lg-40 mb-5"><img class="img-Lazy" src="/Portals/_default/ContentBuilder/minis-page/home13/homepage13-img03.png" alt="Image"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style class="build-css" data-class="title-03">${csstemplate["title-03"]}</style>
<style class="build-css" data-class="infolist-01">${csstemplate["infolist-01"]}</style>`
}, {
    'thumbnail': 'minis-section/home13/section-05.jpg',
    'category': '812',
    'html': `<div class="is-section is-box section-no-padding is-light-text homepage13-section05">
    <div class="is-overlay">
        <div class="is-overlay-bg" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/home13/homepage13-bg03.jpg&quot;);"></div>
    </div>
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container is-container-fluid no-space">
                <div class="row align-items-center justify-content-center">

                    <div class="col-lg-8 col-xl-4 pl-30">
                        <div class="spacer mb-45 height-140"></div>
                        <h1><b>Rich Mobile Applications In-Store For You</b></h1>
                        <div class="spacer height-20"></div>
                        <p class="size-18">Integer at lobortis leo, a ornare magna. Praesent laoreet ipsum non congue semper. Nullam orci nisl, viverra ut tellus vitae, accumsan.</p>
                        <div class="spacer height-20"></div>

                        <div class="row">
                            <div class="col-sm-4">
                                <ul class="list-01">
                                    <li><i class="sico lnr-check"></i>Agriculture</li>
                                    <li><i class="sico lnr-check"></i>APP</li>
                                    <li><i class="sico lnr-check"></i>Business</li>
                                </ul>
                            </div>
                            <div class="col-sm-4">
                                <ul class="list-01">
                                    <li><i class="sico lnr-check"></i>Car Agency</li>
                                    <li><i class="sico lnr-check"></i>Construction</li>
                                    <li><i class="sico lnr-check"></i>Creative</li>
                                </ul>
                            </div>
                            <div class="col-sm-4">
                                <ul class="list-01">
                                    <li><i class="sico lnr-check"></i>Education</li>
                                    <li><i class="sico lnr-check"></i>Law Agency</li>
                                    <li><i class="sico lnr-check"></i>Real Estate</li>
                                </ul>
                            </div>
                        </div>
                        <div class="spacer d-none d-xl-block height-280 mt-20"></div>
                    </div>
                    <div class="col-xl-2"></div>


                    <div class="col-xl-6 order-xl-first pr-lg-30">
                        <div class="spacer d-none d-lg-block height-60"></div><img src="/Portals/_default/ContentBuilder/minis-page/home13/homepage13-img04.png" alt="Image">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style class="build-css" data-class="list-01">
.list-01{
    margin: 0;
    padding: 0;
    list-style: none;
}
.list-01 li{
    margin: 0 0 14px;
}
.list-01 li i{
    vertical-align: sub;
    font-size: 1.2em;
    margin-right: 15px;
}
</style>
<style class="build-css" data-class="homepage13-section05">
.homepage13-section05{
    overflow:hidden;
}
.homepage13-section05 .left-image{
    margin-bottom:-22%;
}
.homepage13-section05::after{
    content: "";
    width: 100%;
    padding-bottom:7%;
    position: absolute;
    bottom: -1px;
    left:0;
    pointer-events: none;
    background: url('data:image/svg+xml;utf8,<svg width="1500" height="98" xmlns="http://www.w3.org/2000/svg"><path fill="%23c9cbcf" d="M0,114.42h1499.813V92.673c0,0-269.217,33.744-440.195-47.246  c0,0-131.233-68.241-364.454-5.25c0,0-177.728,62.243-339.708,0c0,0-211.474-71.239-355.456-23.996V114.42z" ></path><path fill="%23dddee1" d="M0,111.128h1499.813V69.883c0,0-270.716,44.993-440.195-27.748  c0,0-131.233-68.241-364.454-5.25c0,0-177.728,62.243-339.708,0c0,0-207.091-70.559-355.456-16.498V111.128z" ></path><path fill="%23ffffff" d="M0,111.878l1504.312-0.75V80.38c0,0-269.217,42.745-440.195-38.245  c0,0-131.233-68.241-364.454-5.25c0,0-177.728,62.243-339.708,0c0,0-211.474-63.742-355.456-16.498L0,111.878z" ></path></svg>');
    background-position: center top;
    background-size: cover;
}
@media only screen and (min-width: 992px) and (max-width: 1399px) {
    .homepage13-section05 .left-image{
        margin-bottom:0;
    }
}

</style>
`
}, {
    'thumbnail': 'minis-section/home13/section-06.jpg',
    'category': '812',
    'html': `<div class="is-section is-box is-align-center">
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container mb-30 mb-md-20 mb-lg-60">
                <div class="row">
                    <div class="col-md-12">
                        <h3 class="title-18 m-auto" style="max-width: 550px;">We Provide Affordable <span>Pricing Plan</span> For Digital Marketing</h3>

                        <div class="spacer height-60"></div>
                        <p class="m-auto" style="max-width: 840px;">Sed nibh justo, posuere in ullamcorper eget, finibus ut turpis. Etiam odio nulla, venenatis vel pulvinar interdum, varius quis turpis. Cras nunc dui, finibus sit amet interdum non, luctus sit amet mauris.</p>
                        <div class="spacer height-60"></div>
                    </div>
                </div>
                ${getSnippetCode("price-03")}
            </div>
        </div>
    </div>
</div>
<style class="build-css" data-class="title-18">${csstemplate["title-03"]}</style>
    `
}, {
    'thumbnail': 'minis-section/home13/footer.jpg',
    'category': '812',
    'html': `<div class="is-section is-box is-light-text is-align-center homepage13-footer-bg">
    <div class="is-overlay">
        <div class="is-overlay-bg" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/home13/homepage13-footer-bg.jpg&quot;);"></div>
    </div>
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container mb-40" dragwithouthandle="">
                <div class="row justify-content-center">
                    <div class="col-md-12">
                        <div class="spacer height-60 mb-0 mb-md-15"></div>
                        <h2>Subscribe To Receive Updates</h2>
                        <div class="spacer height-20 mb-5"></div>
                        <p class="size-18 m-auto" style="max-width: 770px;">Maecenas est tellus, faucibus sit amet euismod in, tincidunt in urna. Pellentesque nec faucibus lectus, ac maximus risus ultrices lacus.</p>
                        <div class="spacer height-40"></div>
                    </div>
                </div>

                <div class="row justify-content-center">
                    <div class="col-md-12" style="max-width: 430px;">
                        ${getSnippetCode("ajaxform-04")}
                        <div class="spacer height-60 mb-15"></div>
                    </div>
                </div>

                <div class="row justify-content-center homepage13-footer-info">
                    <div class="col-md-3">
                        <h5 style="font-size:24px;">Phone</h5>
                        <p style="font-size:18px;">(845) 359-7777</p>
                    </div>
                    <div class="col-md-3">
                        <h5 style="font-size:24px;">Tax</h5>
                        <p style="font-size:18px;">(845) 359-7776</p>
                    </div>
                    <div class="col-md-3">
                        <h5 style="font-size:24px;">Email</h5>
                        <p style="font-size:18px;">dnnskindev@gmail.com</p>
                    </div>
                    <div class="col-md-3">
                        <h5 style="font-size:24px;">Skype</h5>
                        <p style="font-size:18px;">dnngo-linda</p>
                    </div>
                </div>
                <div class="row align-items-center pt-20 col-no-padding" style="border-top: 1px solid rgba(255, 255, 255, 0.1);">
                    <div class="col-md text-center text-md-left"><img alt="" class="mb-15 mb-md-0 img-Lazy" src="/Portals/_default/ContentBuilder/minis-page/logo-white.png" alt="Logo"></div>
                    <div class="col-md">
                        <p style="color: rgb(168, 169, 174);" class="text-center mb-md-0">@ 2019 by DNNGo Corp. All Rights Resevered</p>
                    </div>
                    <div class="col-md">
                        <div class="edit-box text-center text-md-right">
                            <a class="social-02" href="#"><i class="sico fab-twitter"></i></a>
                            <a class="social-02" href="#"><i class="sico fab-pinterest-p"></i></a>
                            <a class="social-02" href="#"><i class="sico fab-linkedin-in"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style class="build-css" data-class="social-02">${csstemplate["social-02"]}</style>

<style class="build-css" data-class="homepage13-footer-info">
.homepage13-footer-info {
    margin-bottom: 40px;
}
.homepage13-footer-info p{
 margin: 0; 
 color: #c8cddf;
}
.homepage13-footer-info >div{
   border-right: 1px solid rgba(255,255,255,.3);
   margin-bottom: 40px;
}
.homepage13-footer-info >div:last-child{
    border: none;
}
.homepage13-footer-bg:after{
content: "";
    width: 100%;
        background: url('data:image/svg+xml;utf8,<svg width="2000" height="145" xmlns="http://www.w3.org/2000/svg"><path fill="%23ffffff" d="M2000.25,0.25c0,11.67,0,23.33,0,35c-33.811,10.53-68.92,19.75-106,27l-472,18  c-73.787-12.581-136.579-35.382-205.46-52.25C1117.5,0.25,962.667,0.25,812.25,28.25c-10.07-0.26-312,62-312,62l-419,5  c-28.77-9.23-55.1-20.9-81-33c0-20.67,0-41.33,0-62C666.95,0.25,1333.54,0.25,2000.25,0.25z"></path><path fill="%232AA98E" d="M1825.25,40.25c24.41,5.92,47.28,13.39,69,22c-82.51,17.49-167.23,32.77-261,39c-13.67,0-27.33,0-41,0 c-62.42-1.25-116.62-10.71-170-21c19.07-7.93,40.83-13.17,61-20C1576.58,32.27,1716.1,13.52,1825.25,40.25z"></path><path fill="%23dcf8f3" d="M1825.25,40.25c-109.15-26.73-248.67-7.98-342,20c41.32-25.33,101.01-44.08,163-48 C1708.05,8.34,1774.49,20.54,1825.25,40.25z"></path><path fill="%23dcf8f3" d="M812.25,28.25c-93.7,22.63-184.17,48.49-275,74c-12.65-3.68-25.05-7.62-37-12 c63.93-33.41,134.37-60.644,228-64.344C772,24.333,802.18,27.99,812.25,28.25z"></path><path fill="%232AA98E" d="M500.25,90.25c11.95,4.38,24.35,8.32,37,12c-56.439,18.561-117.27,32.73-191,34c-14.33,0-28.67,0-43,0 c-4,0-9.25-0.191-22-1c-75.84-4.16-140.7-19.3-200-40c62.16-14.5,128.09-25.24,202-28c13.623-0.811,19-1,19-1c7.33,0,14.67,0,22,0 c8.156,0.391,13.595,0.638,20,1C402.12,69.04,452.67,78.17,500.25,90.25z"></path></svg>');
    background-size: cover;
    padding-bottom: 7%;
    background-position: center center;
    top: -1px;
    left: 0;
    z-index: 20;
    position: absolute;
    pointer-events: none;
}
</style>
    `
})

/*Home14*/

data_basic.designs.push({
        'thumbnail': 'minis-section/home14/banner.jpg',
        'category': '813',
        'html': `
    <div class="is-section is-box is-bg-grey is-light-text is-section-auto">
        <div class="is-overlay">
            <div class="is-overlay-bg" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/home14/homepage14-banner.jpg&quot;);"></div>
        </div>
        <div class="is-boxes">
            <div class="is-box-centered">
                <div class="is-container layout-container">
                    <div class="row align-items-center">
                        <div class="col-md-6">
                            <h2 class="mb-20 pt-5" style="font-weight:600; max-width:450px;">Creativity Is The Process Of <i class="color-accent">Having Original Ideas</i> That Have Value.</h2>
                            <p style="font-size: 18px !important; font-weight:600;" data-keep-font-size="">Our team will create you something amazing and stunning.</p>
                            <div class="spacer height-20 mb-5"></div><a href="#" title="learn more" class="button-12">LEARN MORE</a>
                            <div class="spacer height-20 mb-20 mb-md-0"></div>
                        </div>
                        <div class="col-md-6 right-full-column pl-lg-70">
                            <div class="spacer d-none d-lg-block height-60"></div>
                            <div class="full-column-inner full-sm-none">
                                <img class="img-Lazy" src="/Portals/_default/ContentBuilder/minis-page/home14/homepage14-img01.png" alt="Image">
                            </div>
                            <div class="spacer height-40"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style class="build-css" data-class="button-12">${csstemplate["button-12"]}</style>

    `
    }, {
        'thumbnail': 'minis-section/home14/section-01.jpg',
        'category': '813',
        'html': `
    <div class="is-section is-box is-light-text" style="background-color: rgb(21, 23, 55);">
        <div class="is-boxes">
            <div class="is-box-centered">
                <div class="is-container layout-container">
                    <div class="row align-items-center">
                        <div class="col-md-6 left-full-column pr-lg-70">
                            <div class="spacer height-20"></div>
                            <div class="full-column-inner full-sm-none text-right"><img class="img-Lazy" src="/Portals/_default/ContentBuilder/minis-page/home14/homepage14-img02.png" alt="Image"></div>
                            <div class="spacer height-40 d-lg-none"></div>
                            <div class="spacer height-20 d-lg-block d-none"></div>
                        </div>
                        <div class="col-md-6">
                            <div class="spacer height-20 mb-10 d-md-block d-lg-none"></div>
                            <div class="title-20">
                                <h2 class="title">What We Do</h2>
                            </div>
                            <p style="color: rgb(213, 214, 223);">Aenean non sapien aliquam ipsum vulputate consectetur. Nunc varius, sem a iaculis euismod, est dui tempus est.</p>
                            <div class="spacer height-20"></div>
                            <p style="color: rgb(213, 214, 223);">In porta libero et malesuada aliquet. Morbi sit amet velit cursus, iaculis lacus eu, sodales velit. Curabitur fermentum molestie interdum. Vestibulum non interdum tellus. Nam commodo tortor vitae semper elementum. </p>
                            <div class="spacer height-20"></div>
                            ${getSnippetCode("counter-07")}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style class="build-css" data-class="title-20">${csstemplate["title-20"]}</style>
    `
    }, {
        'thumbnail': 'minis-section/home14/section-02.jpg',
        'category': '813',
        'html': `
    <div class="is-section is-box is-light-text is-align-center">
        <div class="is-overlay">
            <div class="is-overlay-bg" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/home14/homepage14-bg01.jpg&quot;);"></div>
        </div>
        <div class="is-boxes">
            <div class="is-box-centered">
                <div class="is-container layout-container mb-25 mb-lg-80">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="title-20 mt-10 mt-md-25">
                                <h2 class="title"> Many Features For Your Needs</h2>
                            </div>
                            <p class="m-auto" style="max-width: 730px; color: rgb(213, 214, 223);">Pellentesque posuere a nisl in dictum. Praesent maximus arcu nulla. Proin sollicitudin facilisis felis non sagittis. Phasellus porttitor ante et iaculis blandit. </p>
                            <div class="spacer height-40 mb-10"></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="iconbox-16 edit-box">
                                <div class="icon"><img alt="Image" class="img-Lazy" src="/Portals/_default/ContentBuilder/minis-page/home14/homepage14-icon01.png"></div>
                                <h6 class="title">Trendy Design</h6>
                                <p>Aliquam convallis lorem in dui lobortis porttitor tincidunt fringilla leo.</p>
                            </div>
                            <div class="iconbox-16 edit-box">
                                <div class="icon"><img alt="Image" class="img-Lazy" src="/Portals/_default/ContentBuilder/minis-page/home14/homepage14-icon02.png"></div>
                                <h6 class="title">Incredible Support</h6>
                                <p>Mauris pretium, massa sit amet sollicitudin laoreet, ante tortor condimentum.</p>
                            </div>
                        </div>
                        <div class="col-md-4 pl-50 pr-50">
                            ${getSnippetCode("carousel05")}
                            <div class="spacer height-20 mb-lg-10"></div>
                        </div>
                        <div class="col-md-4">
                            <div class="iconbox-16 edit-box">
                                <div class="icon"><img alt="Image" class="img-Lazy" src="/Portals/_default/ContentBuilder/minis-page/home14/homepage14-icon03.png"></div>
                                <h6 class="title">Fully Customizable</h6>
                                <p>Donec fringilla sagittis neque vitae finibus. Aliquam nec convallis mauris. </p>
                            </div>
                            <div class="iconbox-16 edit-box">
                                <div class="icon"><img alt="Image" class="img-Lazy" src="/Portals/_default/ContentBuilder/minis-page/home14/homepage14-icon04.png"></div>
                                <h6 class="title">Innovative Ideas</h6>
                                <p>Morbi pharetra metus in tellus consequat sagittis. Quisque lobortis luctus lorem.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>  
    <style class="build-css" data-class="title-20">${csstemplate["title-20"]}</style>
    <style class="build-css" data-class="iconbox-16">${csstemplate["iconbox-16"]}</style>
    `
    }, {
        'thumbnail': 'minis-section/home14/section-03.jpg',
        'category': '813',
        'html': `
    <div class="is-section is-box is-light-text is-align-center" style="background-color: rgb(21, 23, 55);">
        <div class="is-boxes">
            <div class="is-box-centered">
                <div class="is-container layout-container">
                    <div class="row justify-content-center">
                        <div class="col-lg-10">
                            <div class="title-20 mt-10 mt-md-25">
                                <h2 class="title">How We Work</h2>
                            </div>
                            <p class="m-auto" style="max-width: 730px; color: rgb(213, 214, 223);">Pellentesque posuere a nisl in dictum. Praesent maximus arcu nulla. Proin sollicitudin facilisis felis non sagittis. Phasellus porttitor ante et iaculis blandit. </p>
                            <div class="spacer height-40 mb-10"></div>
                            ${getSnippetCode("step01")}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style class="build-css" data-class="title-20">${csstemplate["title-20"]}</style>
    `
    }, {
        'thumbnail': 'minis-section/home14/section-04.jpg',
        'category': '813',
        'html': `
    <div class="is-section is-section-auto">
        <div class="is-boxes">
            <div class="is-box-6 is-box is-light-text">
                <div class="is-overlay">
                    <div class="is-overlay-content"></div>
                    <div class="is-overlay-bg" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/home14/homepage14-bg02.jpg&quot;);"></div>
                </div>
                <div class="is-boxes">
                    <div class="is-box-centered">
                        <div class="is-container container-fluid mt-lg-0 mb-lg-0" style="max-width: 580px;">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="spacer height-20"></div>
                                    <div class="title-20">
                                        <h2 class="title">Our Stunning Works</h2>
                                    </div>
                                    <p style="color: rgb(213, 214, 223);">Pellentesque gravida sapien ac nunc lobortis egestas. Ut pharetra sed neque ac semper. Morbi eleifend hendrerit libero, vitae placerat enim luctus malesuada. Nulla lacinia sed nisi et condimentum.</p>
                                    <div class="spacer height-20 mb-5"></div>
                                    <div><a href="#" title="learn more" class="button-12">LEARN MORE</a></div>
                                    <div class="spacer d-none d-lg-block height-20"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="is-box-6 is-box">
                <div class="is-boxes">
                    <div class="is-box-centered">
                        <div class="is-container is-container-fluid no-space">
                            <div class="row col-no-padding">
                                <div class="col-md-6">
                                    <div class="img-box09">
                                        <div class="pic">
                                            <span><a href="#" title="">
                                                    <img alt="Image" class="img-Lazy" src="/Portals/_default/ContentBuilder/minis-page/home14/homepage14-img09.jpg">
                                                </a></span>
                                        </div>

                                        <h6 class="title">Illustration Design</h6>

                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="img-box09">
                                        <div class="pic">
                                            <span><a href="#" title="">
                                                    <img alt="Image" class="img-Lazy" src="/Portals/_default/ContentBuilder/minis-page/home14/homepage14-img10.jpg">
                                                </a></span>
                                        </div>

                                        <h6 class="title">Illustration Design</h6>

                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="img-box09">
                                        <div class="pic">
                                            <span><a href="#" title="">
                                                    <img alt="Image" class="img-Lazy" src="/Portals/_default/ContentBuilder/minis-page/home14/homepage14-img11.jpg">
                                                </a></span>
                                        </div>
                                        <h6 class="title">Illustration Design</h6>

                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="img-box09">
                                        <div class="pic">
                                            <span><a href="#" title="">
                                                    <img alt="Image" class="img-Lazy" src="/Portals/_default/ContentBuilder/minis-page/home14/homepage14-img12.jpg">
                                                </a></span>
                                        </div>
                                        <h6 class="title">Illustration Design</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style class="build-css" data-class="title-20">${csstemplate["title-20"]}</style>
    <style class="build-css" data-class="button-12">${csstemplate["button-12"]}</style>
    <style class="build-css" data-class="img-box09">${csstemplate["img-box09"]}</style>
    `
    }, {
        'thumbnail': 'minis-section/home14/section-05.jpg',
        'category': '813',
        'html': `
    <div class="is-section is-box is-light-text is-align-center" style="background-color: rgb(21, 23, 55);">
        <div class="is-boxes">
            <div class="is-box-centered">
                <div class="is-container layout-container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="title-20 mt-10 mt-md-25">
                                <h2 class="title"> Affordable Pricing Plans</h2>
                            </div>
                            <p class="m-auto" style="max-width: 730px; color: rgb(213, 214, 223);">Pellentesque posuere a nisl in dictum. Praesent maximus arcu nulla. Proin sollicitudin facilisis felis non sagittis. Phasellus porttitor ante et iaculis blandit. </p>
                            <div class="spacer height-40 mb-10"></div>
                        </div>
                    </div>
                    ${getSnippetCode("price-04")}
                </div>
            </div>
        </div>
    </div>
    <style class="build-css" data-class="title-20">${csstemplate["title-20"]}</style>
    `
    }, {
        'thumbnail': 'minis-section/home14/section-06.jpg',
        'category': '813',
        'html': `
    <div class="is-section is-box is-align-center is-light-text">
        <div class="is-overlay">
            <div class="is-overlay-bg" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/home14/homepage14-bg03.jpg&quot;);"></div>
        </div>
        <div class="is-boxes">
            <div class="is-box-centered">
                <div class="is-container layout-container">
                    <div class="row">
                        <div class="col-md-12">
                            <h2 class="m-auto mt-10" style="max-width: 450px; font-weight:600;">We Are Coming, Please Stay Tuned</h2>
                            <div class="spacer height-40"></div>
                            ${getSnippetCode("countdown-03")}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="spacer height-40"></div>
                            <p class="size-18 pb-10">Will Update You Once It Is Live</p>
                            <div style="max-width: 535px;" class="m-auto">${getSnippetCode("ajaxform-10")}</div>
                            <div class="spacer height-20"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    `
    }, {
        'thumbnail': 'minis-section/home14/section-07.jpg',
        'category': '813',
        'html': `
    <div class="is-section is-box is-light-text is-align-center" style="background-color: rgb(21, 23, 55);">
        <div class="is-boxes">
            <div class="is-box-centered">
                <div class="is-container layout-container pb-10 mb-35 mb-lg-80">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="title-20 mt-10 mt-md-25">
                                <h2 class="title">What Our Clients Say</h2>
                            </div>
                            <p class="m-auto" style="max-width: 730px; color: rgb(213, 214, 223);">Pellentesque posuere a nisl in dictum. Praesent maximus arcu nulla. Proin sollicitudin facilisis felis non sagittis. Phasellus porttitor ante et iaculis blandit.</p>
                            <div class="spacer height-60"></div>                           
                            ${getSnippetCode("testimonials06")}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12"><div class="spacer height-20"></div></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style class="build-css" data-class="title-20">${csstemplate["title-20"]}</style>
    `
    }, {
        'thumbnail': 'minis-section/home14/section-08.jpg',
        'category': '813',
        'html': `
    <div class="is-section  is-section-auto">
        <div class="is-boxes">
            <div class="is-box-6 is-light-text is-box is-align-center">
                <div class="is-overlay">
                    <div class="is-overlay-content"></div>
                    <div class="is-overlay-bg" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/home14/homepage14-bg04.jpg&quot;);"></div>
                </div>
                <div class="is-boxes">
                    <div class="is-box-centered">
                        <div class="is-container is-content-500 container-fluid mt-70 mb-55">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="icon"><i class="sico lnr-telephone2 size-42"></i></div>
                                    <h3>Get In Touch With Us</h3>
                                    <p>Morbi imperdiet erat in tellus vehicula</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="is-box-6 is-box is-light-text is-align-center">
                <div class="is-overlay">
                    <div class="is-overlay-bg" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/home14/homepage14-bg05.jpg&quot;);"></div>
                </div>
                <div class="is-boxes">
                    <div class="is-box-centered">
                        <div class="is-container container-fluid mt-70 mb-55" style="max-width: 520px;">
                            <div class="row">
                                <div class="col-md-12">
                                    <h4 style="font-weight: 600;">Check Our FAQs, You Are Likely To Find Your Answers Here.</h4>
                                    <div class="mt-15"><a href="#" class="button-11"><i class="sico lnr-chevron-right"></i></a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style class="build-css" data-class="button-11">${csstemplate["button-11"]}</style>
    `
    }, {
        'thumbnail': 'minis-section/home14/footer.jpg',
        'category': '813',
        'html': `
    <div class="is-section is-box is-light-text is-align-center" style="background-color: rgb(29, 30, 52);">
        <div class="is-boxes">
            <div class="is-box-centered">
                <div class="is-container layout-container" dragwithouthandle="">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="spacer height-20"></div>
                            <div>
                                <img class="img-Lazy" alt="Logo" src="/Portals/_default/ContentBuilder/minis-page/home14/homepage14-footer-logo.png">
                            </div>
                            <div class="spacer height-20 mb-10"></div>
                            <p class="m-auto" style="max-width: 745px; color: rgb(213, 214, 223);">Quisque aliquet efficitur risus, convallis gravida purus. Fusce posuere turpis ut hendrerit rhoncus. Integer viverra pretium rutrum felis et est tempus elementum.</p>
                            <div class="spacer height-40 mb-15"></div>
                            <div class="homepage14-footer-clientlist">
                                <a href="#"><img class="img-Lazy" alt="Image" src="/Portals/_default/ContentBuilder/minis-page/home14/homepage14-footer-client01.jpg"></a>
                                <a href="#"><img class="img-Lazy" alt="Image" src="/Portals/_default/ContentBuilder/minis-page/home14/homepage14-footer-client02.jpg"></a>
                                <a href="#"><img class="img-Lazy" alt="Image" src="/Portals/_default/ContentBuilder/minis-page/home14/homepage14-footer-client03.jpg"></a>
                                <a href="#"><img class="img-Lazy" alt="Image" src="/Portals/_default/ContentBuilder/minis-page/home14/homepage14-footer-client04.jpg"></a>
                                <a href="#"><img class="img-Lazy" alt="Image" src="/Portals/_default/ContentBuilder/minis-page/home14/homepage14-footer-client05.jpg"></a>
                            </div>
                            <div class="spacer height-20 mb-5"></div>
                            <p>@ 2019 by DNNGo Corp. All Rights Resevered</p>
                            <div class="spacer height-20 mb-10"></div>
                            <div class="edit-box">
                                <a class="social-06" href="#"><i class="sico fab-pinterest-p"></i></a>
                                <a class="social-06" href="#"><i class="sico fab-linkedin-in"></i></a>
                                <a class="social-06" href="#"><i class="sico fab-twitter"></i></a>
                                <a class="social-06" href="#"><i class="sico fab-dribbble"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style class="build-css" data-class="social-06">${csstemplate["social-06"]}</style>
    <style class="build-css" data-class="homepage14-footer-clientlist">
    .homepage14-footer-clientlist a {
        display: inline-block;
        margin: 0px 13px 20px;
    }
    </style>
    `
    }

)


/*app 01*/

data_basic.designs.push({
        'thumbnail': 'minis-section/app01/banner.jpg',
        'category': '814',
        'html': `
    <div class="is-section is-box app01-banner">
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container is-container-fluid mt-0 mb-0 position-static">
                <div class="row">
                    <div class="banner-bg">
                        <div class="bg-iamge">
                            <img src="/Portals/_default/ContentBuilder/minis-page/app01/app01-banner.png" alt="Image" style="max-width: 72%; max-height: calc( 100% - 95px );">
                        </div>
                        <div class="bg-el">
                            <div style="top: 24.636175%; left: 9.35%">
                                <img src="/Portals/_default/ContentBuilder/minis-page/app01/app01-left01.png" alt="Image">
                            </div>
                            <div style="top: 25.675676%; left: 29.25%">
                                <img src="/Portals/_default/ContentBuilder/minis-page/app01/app01-left02.png" alt="Image">
                            </div>
                            <div style="top: 55.168399%; left: 10.5%">
                                <img src="/Portals/_default/ContentBuilder/minis-page/app01/app01-left03.png" alt="Image">
                            </div>
                            <div style="top: 81.600832%; left: 31.15%">
                                <img src="/Portals/_default/ContentBuilder/minis-page/app01/app01-left04.png" alt="Image">
                            </div>

                            <div style="top: 0px; right: 20.7%">
                                <img src="/Portals/_default/ContentBuilder/minis-page/app01/app01-right01.png" alt="Image">
                            </div>
                            <div style="top: -3.638254%; right: -0.6%">
                                <img src="/Portals/_default/ContentBuilder/minis-page/app01/app01-right02.png" alt="Image">
                            </div>
                            <div style="top: 15.592516%; right: 18.95%">
                                <img src="/Portals/_default/ContentBuilder/minis-page/app01/app01-right03.png" alt="Image">
                            </div>
                            <div style="top: 26.715177%; right: 13.05%">
                                <img src="/Portals/_default/ContentBuilder/minis-page/app01/app01-right04.png" alt="Image">
                            </div>
                            <div style="top: 45.114345%; right: 6.65%">
                                <img src="/Portals/_default/ContentBuilder/minis-page/app01/app01-right05.png" alt="Image">
                            </div>
                            <div style="top: 50.31185%; right: 9.65%">
                                <img src="/Portals/_default/ContentBuilder/minis-page/app01/app01-right06.png" alt="Image">
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12">
                        <div class="spacer mt-5 d-none d-lg-block height-120"></div>
                        <div class="spacer height-20 mt-15 d-none d-md-block d-lg-none"></div>
                        <div class="spacer height-40 d-lg-none"></div>
                    </div>
                    <div class="col-md-12">
                        <div class="row m-auto align-items-center align-items-lg-start" style="max-width: 1380px;">
                            <div class="col-md-6">
                                <div class="spacer d-none d-lg-block height-140"></div>
                                <h1 style="max-width: 500px; line-height: 1.20833; font-weight: 600;">Booking At Anytime, Convenient And Efficient</h1>
                                <div class="spacer mb-10 height-20"></div>
                                <p style="max-width: 460px;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus a neque elit. Donec commodo, ipsum at viverra sodales, dolor leo lacinia lorem, sit amet molestie nibh ante volutpat tellus. </p>
                                <div class="spacer mt-20 mb-5 height-20"></div>
                                <div class="mb-10 mb-xl-0" style="font-size: 0px;">
                                    <a href="#"><img class="shadow-md img-Lazy" src="/Portals/_default/ContentBuilder/minis-page/app01/app01-banner-btn01.png" style="border-radius: 50px; margin-right: 30px; margin-bottom: 10px;" alt="Image"></a>
                                    <a href="#"><img class="shadow-md img-Lazy" src="/Portals/_default/ContentBuilder/minis-page/app01/app01-banner-btn02.png" style="border-radius: 50px; margin-bottom: 10px;" alt="Image"></a>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div  class="images-box">
                                    <img src="/Portals/_default/ContentBuilder/minis-page/app01/app01-img01.png" class="img-Lazy" alt="Image">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<style class="build-css" data-class="app01-banner">
    .app01-banner .banner-bg {
        position: absolute;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        text-align: right;
    }

    .app01-banner .banner-bg .bg-iamge {
        height: 100%;
    }

    .app01-banner .banner-bg .bg-el {
        position: absolute;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
    }

    .app01-banner .banner-bg .bg-el>div {
        position: absolute;
    }

    @media only screen and (min-width: 1600px) {
        .app01-banner .images-box {
            margin-right: -47px;
        }
    }

    @media only screen and (max-width: 991px) {
        .app01-banner .banner-bg .bg-el {
            display: none;
        }

        .app01-banner .banner-bg .bg-iamge {
            display: inline-block;
            width: 80%;
        }
    }

    @media only screen and (max-width: 767px) {
        .app01-banner .banner-bg .bg-iamge {
            display: none;
        }
    }
</style>
    `
    }, {
        'thumbnail': 'minis-section/app01/section-01.jpg',
        'category': '814',
        'html': `<div class="is-section is-box app01-section01">
        <div class="is-boxes">
            <div class="is-box-centered">
                <div class="bg-iamge">
                    <img alt="" src="/Portals/_default/ContentBuilder/minis-page/app01/app01-section01-bg.png">
                </div>
                <div class="is-container layout-container mt-55">
                ${getSnippetCode("counter-06")}
                </div>
            </div>
        </div>
    </div>
    <style class="build-css" data-class="app01-section01">
    .app01-section01 .bg-iamge {
        position: absolute;
        left: 0;
        bottom: 0;
    }

    @media only screen and (max-width: 768px) {
        .app01-section01 .bg-iamge {
            display: none;
        }

        .app01-section01 .counter-06 p {
            width: 100%;
        }
    }
    </style>
    `
    }, {
        'thumbnail': 'minis-section/app01/section-02.jpg',
        'category': '814',
        'html': `
        <div class="is-section is-box app01-section02">
        <div class="is-boxes">
            <div class="is-box-centered">
                <div class="is-container layout-container mt-0">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="text-center m-auto" style="max-width: 800px;">
                                <div class="title-19">
                                    <h3 class="mt-0"><span style="font-weight: 600;">Awesome App </span><span class="color-accent">Features</span></h3>
                                </div>
                                <p>Nulla consequat commodo augue sit amet finibus. In justo sapien, elementum nec imperdiet a, blandit sed mauris. Donec fermentum mauris non quam. </p>
                                <div class="spacer mt-20 mb-15 height-20"></div>
                            </div>
                        </div>
                    </div>
                    ${getSnippetCode("iconbox-11")}
                </div>
            </div>
        </div>
    </div>
    <style class="build-css" data-class="title-19">${csstemplate["title-19"]}</style>
    `
    }, {
        'thumbnail': 'minis-section/app01/section-03.jpg',
        'category': '814',
        'html': `
        <div class="is-section is-box app01-section03">
            <div class="is-boxes">
                <div class="is-box-centered">
                    <div class="is-container layout-container mt-0 mb-0">
                        <div class="row align-items-center" style="margin-top: -40px;">
                            <div class="col-md-6">
                                <div class="section-bg">
                                    <div class="bg-iamge">
                                        <img class="img-Lazy" src="/Portals/_default/ContentBuilder/minis-page/app01/app01-section03-01.png" alt="Image">
                                    </div>
                                    <div class="bg-el">
                                        <img src="/Portals/_default/ContentBuilder/minis-page/app01/app01-section03-02.png" alt="Image">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 pl-lg-50">
                                <div class="title-19" style="max-width: 430px">
                                    <h3><span class="color-accent">All The Medical </span><span style="font-weight: 600;">Info You Care Is Gathered In This Wonderful APP</span></h3>
                                </div>
                                <p>Ut imperdiet posuere odio vitae mattis. Etiam viverra laoreet felis vitae tempor. Fusce varius tortor in nulla placerat facilisis. Nunc sodales at metus eu venenatis. </p>
                                <div class="spacer mb-5 height-20"></div>
                                <a href="#" title="LEARN MORE" class="button-13">LEARN MORE</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <style class="build-css" data-class="title-19">${csstemplate["title-19"]}</style>
        <style class="build-css" data-class="button-13">${csstemplate["button-13"]}</style>
        <style class="build-css" data-class="app01-section03">
    .app01-section03 .section-bg,
    .app01-section03 .section-bg .bg-iamge {
        position: relative;
        text-align: center;
        z-index: 1;
    }

    .app01-section03 .section-bg .bg-el {
        position: absolute;
        top: 0;
        right: 0;
        bottom: 0;
        left: -80px;

        display: flex;
        justify-content: center;
        align-items: center;
        z-index: 0;
    }

    @media only screen and (max-width: 768px) {
        .app01-section03 .section-bg .bg-el {
            left: 0px;
        }
    }
</style>
        `
    }, {
        'thumbnail': 'minis-section/app01/section-04.jpg',
        'category': '814',
        'html': `
        <div class="is-section is-box app01-section04">
        <div class="is-boxes">
            <div class="is-box-centered">
                <div class="is-container layout-container mt-30">
                    <div class="row align-items-center">
    
                        <div class="col-md-6 order-md-1 order-12">
                            <div style="max-width: 470px">
                                <div class="title-19">
                                    <h3><span class="color-accent">Easy To Manage </span><span style="font-weight: 600;">All Your Data By This App</span></h3>
                                </div>
                                <p>Nam et ante ac massa suscipit vulputate non ac mi. Aliquam viverra, elit eget pretium elementum.</p>
                                <div class="spacer mb-5 height-20"></div>
                                ${getSnippetCode("accordion02")}
                            </div>
                        </div>
    
                        <div class="col-md-6 order-md-12 order-1">
                            <div class="section-bg">
                                <div class="bg-iamge">
                                    <img class="img-Lazy" src="/Portals/_default/ContentBuilder/minis-page/app01/app01-section04-01.png" alt="Image">
                                </div>
                                <div class="bg-el">
                                    <img src="/Portals/_default/ContentBuilder/minis-page/app01/app01-section04-02.png" alt="Image">
                                </div>
                            </div>
                        </div>
    
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style class="build-css" data-class="title-19">${csstemplate["title-19"]}</style>
    <style class="build-css" data-class="app01-section04">
    .app01-section04 .section-bg,
    .app01-section04 .section-bg .bg-iamge {
        position: relative;
        text-align: center;
        z-index: 1;
    }
    .app01-section04 .section-bg .bg-el {
        position: absolute;
        top: 0;
        right: -107px;
        bottom: 0;
        left: -24px;

        display: flex;
        justify-content: center;
        align-items: center;
        z-index: 0;
    }
    @media only screen and (max-width: 768px) {
        .app01-section04 .section-bg .bg-el {
            left: 0px;
            right: 0px;
        }
    }
    </style>
    `
    }, {
        'thumbnail': 'minis-section/app01/section-05.jpg',
        'category': '814',
        'html': `
        <div class="is-section is-box is-light-text app01-section05 ">
        <div class="is-overlay">
            <div class="is-overlay-bg" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/app01/app01-section05-bg.jpg&quot;);"></div>
        </div>
        <div class="is-boxes">
            <div class="is-box-centered">
                <div class="is-container layout-container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="title-19 text-center" style="margin-top:13px;">
                                <h3><span style="font-weight: 600;">How Does This</span> App Work?</h3>
                            </div>
                            <div class="spacer mb-5 height-20"></div>
                            ${getSnippetCode("carousel04")}
                            <div class="spacer height-20"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style class="build-css" data-class="title-19">${csstemplate["title-19"]}</style>
    `
    }, {
        'thumbnail': 'minis-section/app01/section-06.jpg',
        'category': '814',
        'html': `
        <div class="is-section is-box app01-section06">
        <div class="is-boxes">
            <div class="is-box-centered">
                <div class="bg-iamge">
                    <img src="/Portals/_default/ContentBuilder/minis-page/app01/app01-section06-bg.png" alt="Image">
                </div>
                <div class="is-container layout-container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="text-center m-auto" style="max-width: 770px;">
                                <div class="title-19" style="margin-top: 12px;">
                                    <h3><span class="color-accent">Pricing Plans </span><span style="font-weight: 600;">And Options</span></h3>
                                </div>
                                <p>Nulla consequat commodo augue sit amet finibus. In justo sapien, elementum nec imperdiet a, blandit sed mauris. Donec fermentum mauris non quam. </p>
                            </div>
                            <div class="spacer height-80"></div>
                        </div>
                    </div>
                    ${getSnippetCode("price-01-2")}
                </div>
            </div>
        </div>
    </div>
    <style class="build-css" data-class="title-19">${csstemplate["title-19"]}</style>
    <style class="build-css" data-class="app01-section06">
    .app01-section06 .bg-iamge {
        position: absolute;
        left: 0;
        top: 35%;
    }

    @media only screen and (max-width: 767px) {
        .app01-section06 .bg-iamge {
            display: none;
        }
    }
</style>
    `
    }, {
        'thumbnail': 'minis-section/app01/section-07.jpg',
        'category': '814',
        'html': `<div class="is-section is-box app01-section07">
        <div class="is-boxes">
            <div class="is-box-centered">
                <div class="bg-iamge">
                    <img src="/Portals/_default/ContentBuilder/minis-page/app01/app01-section07-bg.png" alt="Image">
                </div>
                <div class="is-container layout-container mt-0">
                    <div class="row align-items-center" style="margin-top: -26px">
                        <div class="col-md-6">
                            <div class="images-box">
                                <img class="img-Lazy" src="/Portals/_default/ContentBuilder/minis-page/app01/app01-section07-01.png" alt="Image">
                            </div>
                        </div>
                        <div class="col-md-6 pl-lg-40">
                            <div class="title-19">
                                <h3 style="margin-bottom: 2px;"><span style="font-weight: 600;">Praise From People Is What </span><span class="color-accent">Driving Us Work Harder</span></h3>
                            </div>
                            <div class="spacer mb-5 height-20"></div>
                            ${getSnippetCode("testimonials07")}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style class="build-css" data-class="app01-section07">
    .app01-section07 .bg-iamge {
        position: absolute;
        right: 0;
        bottom: 27%;
    }

    @media only screen and (min-width: 1600px) {
        .app01-section07 .images-box {
            margin-right: -15px;
            margin-left: -115px;
        }
    }

    @media only screen and (max-width: 768px) {
        .app01-section07 .bg-iamge {
            display: none;
        }
    }
</style>
    `
    }, {
        'thumbnail': 'minis-section/app01/footer.jpg',
        'category': '814',
        'html': `<div class="is-section is-box app01-footer is-light-text">
        <div class="is-overlay">
            <div class="is-overlay-bg" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/app01/app01-footer-bg.jpg&quot;);"></div>
        </div>
        <div class="is-boxes">
            <div class="is-box-centered">
                <div class="is-container layout-container">
                    <div class="row justify-content-center">
                        <div class="col-md-12">
                            <div class="spacer height-100"></div>
                            <div class="spacer height-100 d-none d-xl-block"></div>
                            <div class="spacer height-20 mb-5"></div>
                        </div>
                    </div>
                    <div class="row justify-content-center text-center text-lg-left" style="align-items: center; padding-bottom: 55px;">
                        <div class="col-lg-6">
                            <h2 style="font-weight: 600; margin-bottom: 0px;">What Are You Waiting For?</h2>
                            <h4 style="font-weight: 600; margin-top: 0px;">Subscribe To Get The Latest News.</h4>
                        </div>
                        <div class="col-md-8 col-lg-5 offset-lg-1">
                            ${getSnippetCode("ajaxform-02")}
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-md-12">
                            <div style="border-bottom: 1px dashed rgba(255, 255, 255, 0.3); margin-bottom: 65px;"></div>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-12 col-sm-6 mb-30 mb-lg-0">
                            <img src="/Portals/_default/ContentBuilder/minis-page/logo-white.png" alt="logo" style="margin-bottom: 32px; margin-top: 5px;">
                            <p style="font-size: 20px; margin-bottom: 0px; line-height: 32px;"><i class="sico lnr-phone-wave color-accent" style="margin-right: 10px;position: relative; top: 3px;"></i>+1 845-359-0545</p>
                            <p style="font-size: 20px; margin-bottom: 16px; line-height: 32px;"><i class="sico lnr-envelope-open color-accent" style="margin-right: 10px;position: relative; top: 3px;"></i>service.simple@gmail.com</p>
                            <p style="margin-bottom: 18px;">@ 2019 by DNNGo Corp.</p>
                            <div class="edit-box">
                                <a href="#" title=""><i class="sico fab-facebook-f" style="font-size: 15px; margin-right: 15px;"></i></a>
                                <a href="#" title=""><i class="sico fab-linkedin-in" style="font-size: 15px; margin-right: 15px;"></i></a>
                                <a href="#" title=""><i class="sico fab-twitter" style="font-size: 15px; margin-right: 15px;"></i></a>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-lg-2 mb-30 mb-lg-0">
                            <h5 style="margin-bottom: 24px; margin-top: 16px;">About Us</h5>
                            <p style="margin-bottom: 12px;"><a href="#">What we do</a></p>
                            <p style="margin-bottom: 12px;"><a href="#">Our features</a></p>
                            <p style="margin-bottom: 12px;"><a href="#">Our team</a></p>
                            <p style="margin-bottom: 12px;"><a href="#">Reasonable price</a></p>
                        </div>
                        <div class="col-12 col-sm-6 col-lg-2 mb-30 mb-lg-0">
                            <h5 style="margin-bottom: 24px; margin-top: 16px;">Our Service</h5>
                            <p style="margin-bottom: 12px;"><a href="#">DNN website design</a></p>
                            <p style="margin-bottom: 12px;"><a href="#">SEO optimization</a></p>
                            <p style="margin-bottom: 12px;"><a href="#">UI design</a></p>
                            <p style="margin-bottom: 12px;"><a href="#">APP design</a></p>
                        </div>
                        <div class="col-12 col-sm-6 col-lg-2">
                            <h5 style="margin-bottom: 24px; margin-top: 16px;">Friendly links</h5>
                            <p style="margin-bottom: 12px;"><a href="#">Industry experts</a></p>
                            <p style="margin-bottom: 12px;"><a href="#">FAQs</a></p>
                            <p style="margin-bottom: 12px;"><a href="#">Contact us</a></p>
                            <p style="margin-bottom: 12px;"><a href="#">Recent posts</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style class="build-css" data-class="app01-footer">
    .app01-footer::after {
        content: "";
        width: 100%;
        background: url('data:image/svg+xml;utf8,<svg xmlns="http://www.w3.org/2000/svg" width="2000" height="206"> <defs> <linearGradient id="grad1" x1="0" y1="0" x2="100%25" y2="0"> <stop offset="0" style="stop-color:%23e9eff9;" ></stop> <stop offset="100%25" style="stop-color:%23e9f0f9;" ></stop> </linearGradient> <linearGradient id="grad2" x1="0" y1="0" x2="100%25" y2="0"> <stop offset="0" style="stop-color:%23e9f1fa;" ></stop> <stop offset="100%25" style="stop-color:%23e8f6ff;" ></stop> </linearGradient> <linearGradient id="grad3" x1="0" y1="0" x2="100%25" y2="0"> <stop offset="0" style="stop-color:%23e8f8fd;" ></stop> <stop offset="100%25" style="stop-color:%23e8f9f9;" ></stop> </linearGradient> </defs> <path fill="%23ffffff" d="M2001.012,0.351c0,71,0,142,0,213c-666.631-0.67-1336.081,1.33-2001-1c666.7,0,1333.29,0,2000,0c0-43.67,0-87.33,0-131 c0-25.33,0-50.67,0-76c-8.15-0.52-389.451,104.221-527,175c-14.6-2.74-27.01-7.66-41-11c-2.939-2.061-7.211-2.791-11-4 c-67.781-26.891-135.68-53.65-229-55c-14.498-1.135-35.625-0.625-58,1c-20.76,0.24-38.58,3.42-57,6c-43.75-22.4-348.67,55-354,55 c-89.3-7.041-378.1-88.11-452-85c-48.849-4.582-48.849-4.582-88.822-2.587C164.986,86.072,0.011,84.351,0.011,84.351v-84 C0.011,0.351,1422.541,0.351,2001.012,0.351z"></path> <path fill="url(%23grad3)" d="M2000,5c0,25.33,0,50.67,0,76c-0.33,0-0.67,0-1,0c-26.29-17.5-49.561-33.18-94-30 c-68.74,4.92-96.83,58.2-139,91c-44.15,34.34-99.439,54-175,54c-43.65,0-80.97-7.37-118-16c137.55-70.779,249.7-166.96,449-176 c2.953-0.359,4.245-0.459,5.458-0.563S1931.783,3.125,1933,3c10.67,0,21.33,0,32,0c1.604,0.313,4.854,0.354,4.854,0.354 S1991.85,4.48,2000,5z"></path> <path fill="url(%23grad2)" d="M1077,117c-73.39,8.26-131.02,39.35-191,63c-60.41,23.811-140.12,32.5-222,22 c-75.99-9.74-140.09-28.35-200-54c-59.97-25.68-118.98-52.67-193-61c73.9-3.11,136.82-17.18,213-18c6,0,12,0,18,0 c93.811,2.52,104.7,87.96,194,95c6.875,0.5,10.063,0.563,16,0c45.18-4.27,81.49-18.529,114-36c29.01-15.59,55.04-37.03,85-48 C972.72,57.4,1033.25,94.6,1077,117z"></path> <path fill="url(%23grad1)" d="M176,84c0,0.053,7.252,0.023,7.252,0.224c0,0.214-7.252,0.662-7.252,0.776 c-71.5,3.83-127.61,23.061-176,50C0,95.33,0,55.67,0,16C48.8,48.53,101.81,76.86,176,84z"></path> </svg>');
        background-size: cover;
        padding-bottom: 11%;
        background-position: center center;
        top: -1px;
        left: 0;
        z-index: 20;
        position: absolute;
        pointer-events: none;
    }

    .app01-footer i.lnr,
    .app01-footer i.fa {
        vertical-align: middle;
    }

    .app01-footer a,
    .app01-footer a i {
        color: #ffffff;

        text-decoration: none;
        transition: color ease 300ms;
        -webkit-transition: color ease 300ms;
    }

    .app01-footer a:hover,
    .app01-footer a:hover i {
        color: var(--accent-color);
    }
    </style>
    `
    }

)
/*SEO-01*/

data_basic.designs.push({
        'thumbnail': 'minis-section/seo01/banner.jpg',
        'category': '815',
        'html': `
    <div class="is-section is-box is-light-text seo01-banner is-align-center">
    <div class="is-overlay">
        <div class="is-overlay-bg" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/seo01/seo01-banner-bg.jpg&quot;);"></div>
    </div>
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container mb-0">
                <div class="row">
                    <div class="col-md-12">
                        <div class="spacer height-60"></div>
                        <div style="max-width: 800px; margin-left: auto; margin-right: auto; margin-bottom: 15px;">
                            <h2 style="font-weight: 600;">We Use Techniques To Get A Top Google Ranking For Your Business</h2>
                            <div class="spacer height-20 mb-15"></div>
                            <a href="#" title="LEARN MORE" class="button-01">GET START NOW</a>
                        </div>
                        <div class="spacer height-100"></div>
                        <div><img class="img-Lazy" src="/Portals/_default/ContentBuilder/minis-page/seo01/seo-banner-img.png" alt="Image"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style class="build-css" data-class="button-01">${csstemplate["button-01"]}</style>
<style class="build-css" data-class="seo01-banner">
    .seo01-banner::before {
        content: "";
        width: 100%;
        background: url('data:image/svg+xml;utf8,<svg xmlns="http://www.w3.org/2000/svg" width="2000" height="522"><defs><linearGradient id="grad1" x1="0" y1="0" x2="100%25" y2="0"><stop offset="0" style="stop-color:rgba(227,243,235,1);" ></stop><stop offset="100%25" style="stop-color:rgba(220,242,232,1);" ></stop></linearGradient><linearGradient id="grad2" x1="0" y1="0" x2="100%25" y2="0"><stop offset="0" style="stop-color:rgba(227,231,243,1);" ></stop><stop offset="100%25" style="stop-color:rgba(239,243,241,1);" ></stop></linearGradient></defs><path fill="%23ffffff" d="M1999.916,300.334c0,74,0,148,0,222c-666.699,0-1333.29,0-2000,0c0-70.67,0-141.33,0-212c7.7-0.7,692.5-275.5,692.5-275.5l307.5,116.5h272c31.85,17.69,45.23,47.79,71,70c23.51,20.26,52.98,32.32,91,38c73.961,11.05,156.15-10.56,225-27c10.717,0.567,205.5-27.5,205.5-27.5S1974.543,282.482,1999.916,300.334z"></path><path fill="url(%23grad1)" d="M2000,256c0,14.67,0,29.33,0,44c-53.42-37.58-114.5-67.5-201-72c-2.04-2.29-70.97-2.31-74,0c-23.34-0.68-45.59,5.08-66,4c55.4-11.94,103.08-31.58,168-34c10.33,0,20.67,0,31,0C1922.53,200.14,1969.859,219.47,2000,256z"></path><path fill="url(%23grad2)" d="M1214,138c24.86-1.19,42,5.34,58,13c-53.29,12.44-93.47,30.8-134,56c-22.92,14.25-45.57,31.61-81,34c-5,0-10,0-15,0C898.05,218.96,837.82,113.18,691,94c0,0-14.878-0.708-29,0c-59.34,3.19-106.83,26.52-151,49C379.64,209.84,273.501,300.357,82,311c0,0-67,6.167-82,0c0,0,0-145.67,0-218c45.58,36.42,112.51,51.49,191,55h44c102.25-2.07,176.09-38.05,250-70c74.47-32.19,149.337-66.564,249-71c7.992-0.356,27.932-0.141,32,0c77.939,2.704,121.189,41.77,171,72c50.98,30.94,106.62,52.44,186,58c0,0,18,1,22,1C1154.67,138,1207.33,138,1214,138z"></path></svg>');
        background-size: cover;
        padding-bottom: 27.45%;
        background-position: center center;
        bottom: -1px;
        left: 0;
        z-index: 1;
        position: absolute;
        pointer-events: none;
    }
    .seo01-banner .is-boxes {
        z-index: 2;
    }

    @media only screen and (max-width: 991px) {}

    @media only screen and (max-width: 767px) {}
</style>
    `
    }, {
        'thumbnail': 'minis-section/seo01/section-01.jpg',
        'category': '815',
        'html': `<div class="is-section is-box seo01-section01">
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="spacer height-20"></div>
                    </div>
                </div>
                <div class="row align-items-center">
                    <div class="col-lg-5 col-xl-6">
                        <div class="m-auto text-center" style="max-width: 500px">
                            <img class="img-Lazy" src="/Portals/_default/ContentBuilder/minis-page/seo01/seo01-section01-img01.png" alt="Image">
                        </div>
                    </div>
                    <div class="col-lg-7 col-xl-6">
                        <div class="pl-lg-30 pt-30 pt-lg-0">
                            <div class="title-21" style="max-width: 500px;margin-bottom: 24px;">
                                <h3>Fully Dedicated To Finding The Best Solutions</h3>
                            </div>
                            <div class="spacer height-20"></div>
                            ${getSnippetCode("tab01")}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="spacer height-0 mb-15"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style class="build-css" data-class="title-21">${csstemplate["title-21"]}</style>
<style class="build-css" data-class="seo01-section01">
.seo01-section01 .tab01  ul.tab-list{
    list-style: none;
    margin: 0;
    padding-top: 5px;
}
.seo01-section01 .tab01  ul.tab-list li{
    font-size: 16px;
    line-height: 36px;
}
.seo01-section01 .tab01  ul.tab-list li > i{
    margin-right: 12px;
    vertical-align: middle;
}
</style>
    `
    }, {
        'thumbnail': 'minis-section/seo01/section-02.jpg',
        'category': '815',
        'html': `<div class="is-section is-box seo01-section02" style="background-color: rgb(246, 247, 252);">
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="spacer height-60 mt-5"></div>
                        <div class="title-21 text-center" style="max-width: 570px; margin-bottom: 45px; margin-left: auto; margin-right: auto;">
                            <h3>Best SEO Solution For Your Website, What Are You Waiting For</h3>
                        </div>
                    </div>
                </div>
                <div class="row align-items-center">
                    <div class="col-lg-4 col-md-6 order-1">
                    ${getSnippetCode("infobox-13")}
                        
                    </div>
                    <div class="col-lg-4 col-md-12 order-3 order-lg-2">
                        <div class="text-center">
                            <img src="/Portals/_default/ContentBuilder/minis-page/seo01/seo01-section02-img01.png" alt="Image" class="img-Lazy">
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 order-2">
                    ${getSnippetCode("infobox-13-right")}   

                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="spacer height-80"></div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<style class="build-css" data-class="title-21">${csstemplate["title-21"]}</style>
<style class="build-css" data-class="seo01-section02">
.seo01-section02::before{
    content: "";
    width: 100%;
    background: url('data:image/svg+xml;utf8,<svg xmlns="http://www.w3.org/2000/svg" width="2000" height="125"><path fill="%23ffffff" d="M2000,0c0,0,0,18.496,0,27.166c-68.52,11.63-127.66,30.91-188,49c-60.04,18-120.15,39.6-195,41c-13,0-49.96-0.371-53-1c-97.73-8.621-179.1-35.811-263-59c-83.84-23.18-169.48-43.63-275-46c-1.39-2.28-99.32-2.32-104,0c-171.64,7.36-302.17,55.831-474,63c-3.04,0.629-103.625,2.125-106,1c-136.62-1.381-220.63-55.371-342-72c0-0.33,0-3.072,0-3.072L2000,0z"></path></svg>');
    background-size: cover;
    padding-bottom: 6%;
    background-position: center center;
    top: -1px;
    left: 0;
    z-index: 1;
    position: absolute;
    pointer-events: none;
}
.seo01-section02::after{
    content: "";
    width: 100%;
    background: url('data:image/svg+xml;utf8,<svg xmlns="http://www.w3.org/2000/svg" width="2000" height="140"><path fill="%23ffffff" d="M2000,106c0,11.33,0,34,0,34H0c0,0,0-3.754,0-4c35-1,17.75-0.5,35-1c93.66-6.5,166.43-30.98,238-58c70.79-26.73,138.94-60.94,228-68c6.33,0,19.063-0.313,19.063-0.313S535.33,9,543,9c93.71,0.39,161.15,36.97,233,62c72.79,25.359,147.03,47.65,244,52h43c171.15-4.51,264.7-86.64,433-94c7.01-0.66,22-1,22-1h13h24h12C1729.05,36.28,1843.46,92.21,2000,106z"></path></svg>');
    background-size: cover;
    padding-bottom: 7%;
    background-position: center center;
    bottom: -1px;
    left: 0;
    z-index: 1;
    position: absolute;
    pointer-events: none;
}

@media only screen and (max-width: 991px) {

}
@media only screen and (max-width: 767px) {

}
</style>
    `
    }, {
        'thumbnail': 'minis-section/seo01/section-03.jpg',
        'category': '815',
        'html': `
    <div class="is-section is-box seo01-section03">
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="spacer height-0 mt-15"></div>
                    </div>
                </div>
                <div class="row align-items-center">
                    <div class="col-md-6">
                        <div class="title-21 pb-10" style="max-width: 500px;">
                            <h3>Skilled People Always Come Up With Creative Ideas</h3>
                        </div>
                        ${getSnippetCode("progressbar02")}
                    </div>
                    <div class="col-md-6 right-full-column">
                        <div class="full-column-inner pl-md-40 pt-60 pt-md-0">
                            <img class="img-Lazy" src="/Portals/_default/ContentBuilder/minis-page/seo01/seo01-section03-img01.png" alt="Image">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="spacer height-0 mt-15"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style class="build-css" data-class="title-21">${csstemplate["title-21"]}</style>
    `
    }, {
        'thumbnail': 'minis-section/seo01/section-04.jpg',
        'category': '815',
        'html': `<div class="is-section is-box is-light-text seo01-section04">
    <div class="is-overlay">
        <div class="is-overlay-bg" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/seo01/seo01-section04-bg.jpg&quot;);"></div>
    </div>
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="spacer height-40 mt-5 mb-3"></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="title-21 text-center" style="max-width: 600px; margin-left: auto; margin-right: auto;">
                            <h3>Praise From People Is What Driving Us Work Harder</h3>
                        </div>
                        ${getSnippetCode("testimonials08")}
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="spacer height-40 mb-0 mb-lg-50"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style class="build-css" data-class="title-21">${csstemplate["title-21"]}</style>
<style class="build-css" data-class="seo01-section04">
.seo01-section04::before{
    content: "";
    width: 100%;
    background: url('data:image/svg+xml;utf8,<svg xmlns="http://www.w3.org/2000/svg" width="2000" height="144"><path fill="%23ffffff" d="M1998,0c1.7,0,1.891,0.5,2,2c-128.33,41.33-261.017,74.364-423,86c-23.414,1.682-73.255,2.395-115,0c-89.146-5.114-164.04-16.29-236-34c-71.54-17.61-136.359-42.294-222-47c-15.735-0.865-38.289-1.228-54,0c-107.756,8.42-188.17,29.2-267,61c-18.84,7.6-37.87,16.3-56,26c-54.11,28.96-120.23,41.83-199,47c-3.7,0.641-28.497,1.011-38,1c-8.004-0.009-28.3-0.359-32-1C215.88,133.46,103.56,96.109,0,50C0,33.33,0,16.67,0,0C661.75,0,1338.59,0,1998,0z"></path></svg>');
    background-size: cover;
    padding-bottom: 7%;
    background-position: center center;
    top: -1px;
    left: 0;
    z-index: 1;
    position: absolute;
    pointer-events: none;
}
.seo01-section04::after{
    content: "";
    width: 100%;
    background: url('data:image/svg+xml;utf8,<svg xmlns="http://www.w3.org/2000/svg" width="2000" height="112"><path fill="%23ffffff" d="M2000,83c0,9.67,0,19.33,0,29c-666.7,0-1333.29,0-2000,0c0-31.33,0-62.67,0-94c66.61,24.39,140.102,37.618,227,46c39.504,3.811,81.849,3.033,113,0c74.099-7.216,137.91-12.36,200-27C601,22.61,661.404,6.717,733,3c23.553-1.223,44.423-2.288,69,0c162.429,15.12,301.93,54.061,455,75c94.49,12.92,187.189,24.51,290,29c70,5,139.25,5,202,0C1836.18,102.52,1916.32,90.99,2000,83z"></path></svg>');
    background-size: cover;
    padding-bottom: 6%;
    background-position: center center;
    bottom: -1px;
    left: 0;
    z-index: 1;
    position: absolute;
    pointer-events: none;
}
</style>
`
    }, {
        'thumbnail': 'minis-section/seo01/section-05.jpg',
        'category': '815',
        'html': `<div class="is-section is-box seo01-section05">
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="text-center pt-5">
                            <div class="title-21" style="max-width: 570px; margin-left: auto; margin-right: auto;">
                                <h3>Our Pricing Plans Are Grounded On Reasonable Profit</h3>
                            </div>
                            <p style="max-width: 780px; margin-left: auto; margin-right: auto;">Nulla et arcu quis est volutpat pellentesque. Donec at luctus tellus, non venenatis ante. Sed id eros fringilla, iaculis arcu vitae, ornare diam. </p>
                            <div class="spacer height-20 mb-15"></div>
                        </div>
                    </div>
                </div>
                ${getSnippetCode("price-06")}
            </div>
        </div>
    </div>
</div>
<style class="build-css" data-class="title-21">${csstemplate["title-21"]}</style>
    `
    }, {
        'thumbnail': 'minis-section/seo01/section-06.jpg',
        'category': '815',
        'html': `<div class="is-section is-box is-light-text seo01-section06">
    <div class="is-overlay">
        <div class="is-overlay-bg" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/seo01/seo01-section06-bg.jpg&quot;);"></div>
    </div>
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="spacer height-80 mt-5"></div>
                        <div class="text-center" style="max-width: 770px; margin-left: auto; margin-right: auto;">
                            <div class="title-01 pb-5">
                                <h3>Avail Free SEO Quotes Now</h3>
                            </div>
                            <p>Quisque pulvinar eget sem nec suscipit. Maecenas velit massa, condimentum eu nisi eu, dapibus facilisis massa. Vestibulum at mattis mauris, et tristique lacus.</p>
                            <div class="spacer height-20 mb-15"></div>
                            <div style="margin-left: auto;margin-right: auto;max-width: 630px;}">${getSnippetCode("ajaxform-11")}</div>
                        </div>
                        <div class="spacer height-100"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style class="build-css" data-class="title-01">${csstemplate["title-01"]}</style>
<style class="build-css" data-class="seo01-section06">
.seo01-section06::before{
    content: "";
    width: 100%;
    background: url('data:image/svg+xml;utf8,<svg xmlns="http://www.w3.org/2000/svg" width="2000" height="110"><path fill="%23ffffff" d="M2000,0c0,8.33,0,16.67,0,25c-51.359,27.97-115.24,38.386-191,47c-18.105,2.059-46.214,2.656-74,0c-141.677-13.542-256.396-55.937-408.166-59.167C1276.204,11.887,1284.167,12.5,1240,15c-98.98,7.14-184.55,31.85-273,49c-88.49,17.16-209.527,35.374-280,42c-40.322,3.791-121.588,2.676-162,0c-69.927-4.63-190.47-19.32-278-36C159.16,53.26,75.1,34.57,0,3c0-1,0-2,0-3C666.7,0,1333.29,0,2000,0z"></path></svg>');
    background-size: cover;
    padding-bottom: 5.8%;
    background-position: center center;
    top: -1px;
    left: 0;
    z-index: 1;
    position: absolute;
    pointer-events: none;
}
.seo01-section06::after{
    content: "";
    width: 100%;
    background: url('data:image/svg+xml;utf8,<svg xmlns="http://www.w3.org/2000/svg" width="2000" height="197"><path fill="%23ffffff" d="M2000,2c0,65,0,195,0,195H0c0,0,0-2.67,0-4c2,0,4,0,6,0c37.625,3,45.985,1.829,81,0c134.71-10.5,247.01-35.529,359-66c110.3-30.01,218.13-69.12,353-72c3.67,0,22.33-1,23-1c7,0,76-1,93-1c8.75-0.125,39.33,1,44,1c0.67,0,54.65,2.35,60,3c0.33,0,19.67,1,23,1c126.43,5.57,240.82,23.18,365,31c9.676,0.86,48.33,2,44,2c-4.944,0,54.124,1.292,97,2c21.375-0.375,23.986-0.673,65-3C1770.5,80.333,1897.311,57.64,2000,2z"></path></svg>');
    background-size: cover;
    padding-bottom: 10.4%;
    background-position: center center;
    bottom: -1px;
    left: 0;
    z-index: 1;
    position: absolute;
    pointer-events: none;
}
</style>
    `
    }, {
        'thumbnail': 'minis-section/seo01/footer.jpg',
        'category': '815',
        'html': `
    <div class="is-section seo01-footer">
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container mt-65 mb-30">
                <div class="row size-16" style="color: rgb(135, 135, 135);">
                    <div class="col-md-4">
                    <img src="/Portals/_default/ContentBuilder/minis-page/logo-black.png" alt="Logo" style="margin-bottom: 33px;margin-top: 5px;" class="img-Lazy">
                        <p>Vivamus elit mauris, luctus et facilisis at, tincidunt vel metus consequat neque</p>
                        <div class="spacer height-20"></div>
                        <p style="font-size: 18px;">service.simple@gmail.com<br>Yanta W Rd, Yanta District, Xian Shi, Shaanxi</p>
                    </div>

                    <div class="col-md-4 pb-20 pb-md-0">
                        <h5 class="pt-10 mb-10">Contact Us</h5>
                        <div class="spacer height-20"></div>
                        <p class="size-26 color-accent mb-0" style="font-weight: 600; line-height: 1.35;">+1 845-359-0545</p>
                        <p class="mb-10">Free Consultation</p>
                        <div class="spacer height-20"></div>
                        <p style="font-weight: 600; color: rgb(51, 51, 51); margin-bottom: 12px;">Working Time</p>
                        <p class="mb-0">Monday-Friday: 8:00am-5:30pm</p>
                        <p class="mb-0">Saturday: 10:00am-2:00pm</p>
                    </div>
                    <div class="col-md-4">
                        <h5 class="pt-10 mb-10">Friendly Links</h5>
                        <div class="spacer height-20"></div>

                        <div class="row seo01-footer-linklist">
                            <div class="col-sm-6">
                                <p><a href="#" title="What we do">What we do</a></p>
                                <p><a href="#" title="Our service">Our service</a></p>
                                <p><a href="#" title="Creative design">Creative design</a></p>
                                <p><a href="#" title="Industry experts">Industry experts</a></p>
                                <p><a href="#" title="Feature">Feature</a></p>
                            </div>
                            <div class="col-sm-6">
                                <p><a href="#" title="Our team">Our team</a></p>
                                <p><a href="#" title="Our pricing">Our pricing</a></p>
                                <p><a href="#" title="FAQ">FAQ</a></p>
                                <p><a href="#" title="The latest news">The latest news</a></p>
                                <p><a href="#" title="Amazing support">Amazing support</a></p>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="spacer height-60 mb-10"></div>
                    </div>
                </div>
                <div class="row clientlist01 pt-40" style="border-top: 1px solid rgba(235, 235, 235, 1);">
                    <div class="col-md-2 col-4">
                        <span><a href="#" title=""><img src="/Portals/_default/ContentBuilder/minis-page/seo01/seo01-footer-logo01.png" class="img-Lazy"></a></span>
                    </div>
                    <div class="col-md-2 col-4">
                        <span><a href="#" title=""><img src="/Portals/_default/ContentBuilder/minis-page/seo01/seo01-footer-logo02.png" class="img-Lazy"></a></span>
                    </div>
                    <div class="col-md-2 col-4">
                        <span><a href="#" title=""><img src="/Portals/_default/ContentBuilder/minis-page/seo01/seo01-footer-logo03.png" class="img-Lazy"></a></span>
                    </div>
                    <div class="col-md-2 col-4">
                        <span><a href="#" title=""><img src="/Portals/_default/ContentBuilder/minis-page/seo01/seo01-footer-logo04.png" class="img-Lazy"></a></span>
                    </div>
                    <div class="col-md-2 col-4">
                        <span><a href="#" title=""><img src="/Portals/_default/ContentBuilder/minis-page/seo01/seo01-footer-logo05.png" class="img-Lazy"></a></span>
                    </div>
                    <div class="col-md-2 col-4">
                        <span><a href="#" title=""><img src="/Portals/_default/ContentBuilder/minis-page/seo01/seo01-footer-logo06.png" class="img-Lazy"></a></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style class="build-css" data-class="clientlist01">${csstemplate["clientlist01"]}</style>
<style class="build-css" data-class="seo01-footer">
    .seo01-footer .seo01-footer-linklist a,
    .seo01-footer .seo01-footer-linklist a:link {
        color: currentColor;
    }

    .seo01-footer .seo01-footer-linklist a:hover {
        color: var(--accent-color)
    }

    .seo01-footer .seo01-footer-linklist p {
        margin-bottom: 12px;
    }
</style>
    `
    }

)

/*Pages*/

data_basic.designs.push({
    'thumbnail': 'minis-section/page/section-01.jpg',
    'category': '816',
    'html': `<div class="is-section is-box is-bg-light is-dark-text">
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container">
                <div class="row">
                    <div class="col-md-12" data-noedit="">
                        <div class="spacer height-20"></div>
                    </div>
                </div>
                <div class="row align-items-center">
                    <div class="col-md-6">
                        <div><img src="/Portals/_default/ContentBuilder/minis-page/about-us/aboutus-pic02.jpg" class="shadow-md img-Lazy" style="position: relative;max-width: 64%;" alt="">
                        </div>
                        <div>
                            <img src="/Portals/_default/ContentBuilder/minis-page/about-us/aboutus-pic03.jpg" class="shadow-md img-Lazy" style="float: right;margin-top: -34%;margin-bottom: -21%;position: relative;max-width: 66%;">
                        </div>
                        <div>
                            <img src="/Portals/_default/ContentBuilder/minis-page/about-us/aboutus-pic04.jpg" class="shadow-md img-Lazy" style="position: relative;max-width: 53%;"></div>
                    </div>
                    <div class="col-md-6 pl-lg-45">
                    <div class="spacer height-40 d-md-none d-lg-block">
                    </div>
                        <div class="title-09 mr-lg-30" style="margin-bottom: 50px;">
                            <small>TERRIFIC TEAM</small>
                            <h3>
                                Skilled People Always Come Up <span class="color-accent">With Creative Ideas</span></h3>
                        </div>
                        ${getSnippetCode("progressbar01")}
                        <div class="spacer height-40 mb-10"></div>
                        <div><a href="#" title="learn more" class="button-01" style="margin-right: 12px;">LEARN MORE</a></div>
                        <div class="spacer height-40 d-none d-lg-block">
                        </div>

                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12" data-noedit="">
                        <div class="spacer height-20"></div>
                    </div>
                </div>
                <div class="row align-items-center">
                    <div class="col-md-12"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<style class="build-css" data-class="title-09">${csstemplate["title-09"]}</style>
<style class="build-css" data-class="button-01">${csstemplate["button-01"]}</style>
`
}, {
    'thumbnail': 'minis-section/page/section-02.jpg',
    'category': '816',
    'html': `<div class="is-section is-box is-light-text is-align-center">
    <div class="is-overlay">
        <div class="is-overlay-bg" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/about-us/aboutus-bg01.jpg&quot;);">
        </div>
    </div>
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="title-14 mt-10">
                            <small>RECENT PROJECTS</small>
                            <h2>Great And Stunning Works</h2>
                        </div>
                        <p class="m-auto" style="max-width: 970px;">Fusce sollicitudin suscipit turpis, sed ultricies ex sollicitudin non. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Aliquam suscipit ipsum tempor mi viverra.</p>
                        <div class="spacer height-40 mb-10"></div>
                        <div><a href="#" title="learn more" class="button-01" style="margin-right: 12px;">LEARN MORE</a></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="spacer height-100"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="is-section is-box">
    <div class="is-boxes no-min-height">
        <div class="is-box-centered">
            <div class="is-container layout-container m-auto">
                <div class="row aboutus-section04">
                    <div class="col-md-4">
                        <div class="img-box02">
                            <div class="pic"><img class="img-Lazy" src="/Portals/_default/ContentBuilder/minis-page/about-us/aboutus-pic05.jpg" alt="Image"></div>
                            <div class="cont edit-box">
                                <p>Duis egestas mollis ligula et finibus. Praesent condimentum diam sed purus lacinia, id vehicula libero.</p>
                                <div class="title bg-accent">
                                    <h6>Best Sell Magazine</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="img-box02">
                            <div class="pic"><img class="img-Lazy" src="/Portals/_default/ContentBuilder/minis-page/about-us/aboutus-pic06.jpg" alt="Image"></div>
                            <div class="cont edit-box">

                                <p>Quisque porta lacus lacus, nec sagittis est mollis nec. Integer non tristique elit. Suspendisse sed elit urna.</p>

                                <div class="title bg-accent">
                                    <h6>Creative Spaghetti</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="img-box02">
                            <div class="pic"><img class="img-Lazy" src="/Portals/_default/ContentBuilder/minis-page/about-us/aboutus-pic07.jpg" alt="Image"></div>
                            <div class="cont edit-box">
                                <p> Pellentesque nec imperdiet odio. Sed interdum massa sit amet tempus pretium, vestibulum cursus.</p>
                                <div class="title bg-accent">
                                    <h6>Bird Of Paradise</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<style class="build-css" data-class="title-14">${csstemplate["title-14"]}</style>
<style class="build-css" data-class="button-01">${csstemplate["button-01"]}</style>
<style class="build-css" data-class="img-box02">${csstemplate["img-box02"]}</style>
<style class="build-css" data-class="aboutus-section04">
    .aboutus-section04 {
        margin-top: -115px;
    }

    @media all and (max-width: 991px) {
        .aboutus-section04 {
            margin-top: -65px;
        }
    }

    @media all and (max-width: 767px) {
        .aboutus-section04 {
            margin-top: -35px;
        }
    }
</style>
    `
}, {
    'thumbnail': 'minis-section/page/section-03.jpg',
    'category': '816',
    'html': `<div class="is-section is-box is-bg-grey">
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container is-container-fluid m-auto no-space">
                ${getSnippetCode("img-box03")}
            </div>
        </div>
    </div>
</div>
    `
}, {
    'thumbnail': 'minis-section/page/section-04.jpg',
    'category': '816',
    'html': `<div class="is-section is-box is-bg-light is-dark-text section-no-padding">
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container">
            <div class="row">
            <div class="col-md-12">
                <div class="spacer mb-30 height-80">
                </div>
            </div>
        </div>
                <div class="clear"></div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="title-09">
                            <small>WHAT WE DO</small>
                            <h3>We Offer All kinds Of Services, <span class="color-accent">Covering Everything You Need</span></h3>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="iconbox-02 edit-box">
                            <div class="icon color-accent"><i class="sico lnr-window size-32"></i></div>
                            <h6>Professional Website Design</h6>
                            <p>Fusce hendrerit lacinia suscipit. Pellentesque habitant morbi tristique.</p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="iconbox-02 edit-box">
                            <div class="icon color-accent"><i class="sico lnr-signal size-32"></i></div>
                            <h6>SEO Basic Optimization</h6>
                            <p>Etiam eros dui, elementum eget ullamcorper eu, sollicitudin nec erat.</p>
                        </div>
                    </div>
                </div>
    
                <div class="row">
                    <div class="col-md-4">
                        <div class="iconbox-02 edit-box no-line">
                            <div class="icon color-accent"><i class="sico lnr-cog size-32"></i></div>
                            <h6>Logo Designing</h6>
                            <p>Etiam eros dui, elementum eget ullamcorper eu, sollicitudin nec erat.</p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="iconbox-02 edit-box">
                            <div class="icon color-accent"><i class="sico lnr-wallet size-32"></i></div>
                            <h6>Free Trial </h6>
                            <p>Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae.</p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="iconbox-02 edit-box">
                            <div class="icon color-accent"><i class="sico lnr-upload2 size-32"></i></div>
                            <h6>Free Updates </h6>
                            <p>Etiam eros dui, elementum eget ullamcorper eu, sollicitudin nec erat.</p>
                        </div>
                    </div>
    
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="spacer height-20 mb-md-20 mb-md-40"></div>
                    </div>
                </div>
    
            </div>
        </div>
    </div>
    <style class="build-css" data-class="title-09">${csstemplate["title-09"]}</style>
    <style class="build-css" data-class="iconbox-02">${csstemplate["iconbox-02"]}</style>
    `
}, {
    'thumbnail': 'minis-section/page/section-05.jpg',
    'category': '816',
    'html': `<div class="is-section is-shadow-1 is-section-auto">
    <div class="is-boxes">
        <div class="is-box-6 is-box is-light-text is-align-center">
            <div class="is-overlay">
                <div class="is-overlay-content"></div>
                <div class="is-overlay-bg" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/our-service/ourservice-bg01.jpg&quot;);"></div>
            </div>
            <div class="is-boxes">
                <div class="is-box-centered">
                    <div class="is-container container-fluid" style="max-width: 580px;">
                        <div class="row">
                            <div class="col-md-12">
                                <h2>How We Work?</h2>
                                <div class="spacer height-20"></div>
                                <p>Quisque quis consequat orci. Nam vel placerat justo. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; </p>
                                <div class="spacer height-40 mb-5"></div>
                                <div>
                                ${getSnippetCode("play-button-02")}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="is-box-6 is-box" style="background-color: rgb(244, 244, 244);">
            <div class="is-boxes">
                <div class="is-box-centered">
                    <div class="is-container container-fluid" style="max-width: 610px;">
                        <div class="row">
                            <div class="col-md-12 pl-md-50">
                                <div class="title-11 pr-lg-40">
                                    <h3>We Can Turn Your Brief Ideas <span class="color-accent">Into Powerful Features</span></h3>
                                </div>
                                <div class="content">
                                ${getSnippetCode("img-box04")}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style class="build-css" data-class="title-11">${csstemplate["title-11"]}</style>
`
}, {
    'thumbnail': 'minis-section/page/section-06.jpg',
    'category': '816',
    'html': `<div class="is-section is-box">
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container">
                <div class="row justify-content-center">
                    <div class="col-md-12">
                        <div class="title-14 mt-10 text-center">
                            <small>TRANSPARENT</small>
                            <h2>Reasonable Pricing Plans</h2>
                        </div>

                        <p class="m-auto text-center" style="max-width: 800px;">Nullam gravida magna ligula, egestas porta quam luctus sit amet. Phasellus sed vulputate eros. Suspendisse tempus orci eu lorem scelerisque.</p>
                        <div class="spacer height-60 mb-5"></div>
                    </div>
                </div>
                ${getSnippetCode("price-02")}
            </div>
        </div>
    </div>
</div>
<style class="build-css" data-class="title-14">${csstemplate["title-14"]}</style>
`
}, {
    'thumbnail': 'minis-section/page/section-07.jpg',
    'category': '816',
    'html': `<div class="is-section is-box is-bg-grey is-light-text">
    <div class="is-overlay">
        <div class="is-overlay-bg" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/our-service/ourservice-bg02.jpg&quot;);"></div>
    </div>
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container">
                <div class="row">
                    <div class="col-md-6"></div>
                    <div class="col-md-6 pr-40">
                        <p style="font-size:18px;" class="pb-10">What are you waiting for?</p>
                        <h3>Subscribe To Get <span class="color-accent">The Latest News Or Best Deals.</span></h3>
                        <div class="spacer height-40"></div>
                        ${getSnippetCode("ajaxform-05")}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    `
}, {
    'thumbnail': 'minis-section/page/section-08.jpg',
    'category': '816',
    'html': `<div class="is-section is-box">
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container mb-50 mb-lg-80 pb-md-10">
                <div class="row">
                    <div class="col-md-5">
                        <div class="sidebar-sticky edit-box">
                            <div class="spacer height-80 d-none d-md-block"></div>
                            <div class="title-09 pr-lg-70 mt-10">
                                <small>QUESTIONS</small>
                                <h3>Here Are The Answers You <span class="color-accent">Are Looking For</span></h3>
                            </div>
                            <p>Donec posuere est non nibh egestas ullamcorper. Cras placerat ullamcorper ipsum quis interdum. Fusce suscipit posuere interdum pretium vitae.</p>
                            <div class="spacer height-20 mb-10"></div>
                            <div><a class="button-01" href="#" title="">LEARN MORE</a></div>
                            <div class="spacer height-80"></div>
                        </div>
                    </div>
                    <div class="col-md-7 pl-lg-45">
                        <div class="spacer height-20"></div>
                        ${getSnippetCode("faq-02")}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style class="build-css" data-class="button-01">${csstemplate["button-01"]}</style>
<style class="build-css" data-class="title-09">${csstemplate["title-09"]}</style>
`
}, {
    'thumbnail': 'minis-section/page/section-09.jpg',
    'category': '816',
    'html': `<div class="is-section is-box">
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container mt-50 mt-lg-80 mb-50 mb-lg-80 pb-lg-10">
            <div class="row">
                    <div class="col-md-12">
                        <div class="spacer height-20 mb-lg-20">
                        </div>
                    </div>
                </div>
                <div class="row align-items-center">
                    <div class="col-lg-5">
                        <div class="title-09 pr-lg-80">
                            <small>TEAM ADVANTAGE</small>
                            <h3>Our Strengths Speak For <span class="color-accent">Themselves</span></h3>
                        </div>
                        <p>Mauris nec purus eget metus porttitor lobortis vitae id magna. Suspendisse lobortis feugiat lacinia. Fusce iaculis nisi vel vulputate tincidunt.</p>
                        <div class="spacer height-20 mb-5"></div>
                        <div class="iconbox-06">
                            <div class="icon edit-box">
                                <i class="sico lnr-phone-wave"></i>
                            </div>
                            <div class="content">
                                <p class="size-18">Want know more info?</p>
                                <h6 class="size-22">Contact <span class="color-accent">+1 845-359-0545</span></h6>
                            </div>
                        </div>
                        <div class="spacer height-40"></div>
                    </div>
                    <div class="col-lg-7 pl-lg-45">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="iconbox-11 edit-box">
                                    <div class="icon img-Lazy-warp"><img class="img-Lazy" alt="" src="/Portals/_default/ContentBuilder/minis-page/our-team/ourteam-icon01.png"></div>
                                    <div class="title">
                                        <h6>Extensive Experience</h6>
                                    </div>
                                    <p>Nam posuere erat tincidunt purus viverra, in semper nisi posuere leo risus.</p>
                                </div>
                                <div class="iconbox-11 edit-box">
                                    <div class="icon img-Lazy-warp"><img class="img-Lazy" alt=""  src="/Portals/_default/ContentBuilder/minis-page/our-team/ourteam-icon03.png"></div>
                                    <div class="title">
                                        <h6>Incomparable Service</h6>
                                    </div>
                                    <p>Nunc a fermentum tellus. Aenean purus dui, commodo sed sapien egestas.</p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="spacer height-40 mb-10 d-none d-md-block"></div>
                                <div class="iconbox-11 edit-box">
                                    <div class="icon img-Lazy-warp"><img class="img-Lazy" alt=""  src="/Portals/_default/ContentBuilder/minis-page/our-team/ourteam-icon02.png"></div>
                                    <div class="title">
                                        <h6>Excellent Technology</h6>
                                    </div>
                                    <p>Nullam nec odio efficitur, elementum metus iaculis, aliquam ipsum.</p>
                                </div>
                                <div class="iconbox-11 edit-box">
                                    <div class="icon img-Lazy-warp"><img class="img-Lazy" alt=""  src="/Portals/_default/ContentBuilder/minis-page/our-team/ourteam-icon04.png"></div>
                                    <div class="title">
                                        <h6>Innovative Ideas</h6>
                                    </div>
                                    <p>Proin id felis lorem. Fusce pellentesque ante est, et ornare leo posuere eu. </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style class="build-css" data-class="title-09">${csstemplate["title-09"]}</style>
    <style class="build-css" data-class="iconbox-06">${csstemplate["iconbox-06"]}</style>
    <style class="build-css" data-class="iconbox-11">${csstemplate["iconbox-11"]}</style>
    `
}, {
    'thumbnail': 'minis-section/page/section-10.jpg',
    'category': '816',
    'html': `<div class="is-section is-box is-bg-grey is-light-text is-align-center">
    <div class="is-overlay">
        <div class="is-overlay-bg" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/our-team/ourteam-bg01.jpg&quot;);"></div>
    </div>
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container">

                <div class="row">
                    <div class="col-md-12">
                        <div class="spacer height-40"></div>
                        <p class="pb-5"><i class="sico lnr-users2" style="font-size: 64px;"></i></p>
                        <p>

                        </p>
                        <h3 class="m-auto" style="max-width: 870px;">We Have A Professional, Mature, Hard-working Team, To Provide Quality Customer Service. </h3>
                        <div class="spacer height-40 mb-5"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    `
}, {
    'thumbnail': 'minis-section/page/section-11.jpg',
    'category': '816',
    'html': `<div class="is-section is-box">
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container mb-30 mb-lg-70">
                <div class="row">
                    <div class="col-md-12">
                        <div class="title-14 mt-10 text-center">
                            <small>PARTNER</small>
                            <h2>Meet Our Team</h2>
                        </div>
                        <p class="m-auto text-center" style="max-width: 770px;">Nullam gravida magna ligula, egestas porta quam luctus sit amet. Phasellus sed vulputate eros. Suspendisse tempus orci eu lorem scelerisque.</p>
                        <div class="spacer height-60"></div>

                        <div class="row">
                            <div class="col-lg-3 col-sm-6">
                                <div class="ourteam-01 mb-50">
                                    <div class="pic">
                                        <img class="img-Lazy" alt="Ross Williams" src="/Portals/_default/ContentBuilder/minis-page/our-team/ourteam-pic01.jpg">
                                    </div>
                                    <div class="content">
                                        <h6 class="title">Amy Walke</h6>
                                        <small>Nullam tempor elementum</small>
                                        <span class="icon">
                                            <a href="#" title="sico fab-facebook-f"><i class="sico fab-facebook-f"></i></a>
                                            <a href="#" title="sico fab-twitter"><i class="sico fab-twitter"></i></a>
                                            <a href="#" title="sico fab-linkedin-in"><i class="sico fab-linkedin-in"></i></a>
                                        </span>
                                    </div>
                                </div>
                            </div>


                            <div class="col-lg-3 col-sm-6">
                                <div class="ourteam-01 mb-50">
                                    <div class="pic">
                                        <img class="img-Lazy" alt="Ross Williams" src="/Portals/_default/ContentBuilder/minis-page/our-team/ourteam-pic02.jpg">
                                    </div>
                                    <div class="content">
                                        <h6 class="title">Ettle Halsey</h6>
                                        <small>Interdum et malesuada fames</small>
                                        <span class="icon">
                                            <a href="#" title="sico fab-facebook-f"><i class="sico fab-facebook-f"></i></a>
                                            <a href="#" title="sico fab-twitter"><i class="sico fab-twitter"></i></a>
                                            <a href="#" title="sico fab-linkedin-in"><i class="sico fab-linkedin-in"></i></a>
                                        </span>
                                    </div>
                                </div>
                            </div>


                            <div class="col-lg-3 col-sm-6">
                                <div class="ourteam-01 mb-50">
                                    <div class="pic">
                                        <img class="img-Lazy" alt="Ross Williams" src="/Portals/_default/ContentBuilder/minis-page/our-team/ourteam-pic03.jpg">
                                    </div>
                                    <div class="content">
                                        <h6 class="title">Jackie Brown</h6>
                                        <small>Vestibulum purus justo</small>
                                        <span class="icon">
                                            <a href="#" title="sico fab-facebook-f"><i class="sico fab-facebook-f"></i></a>
                                            <a href="#" title="sico fab-twitter"><i class="sico fab-twitter"></i></a>
                                            <a href="#" title="sico fab-linkedin-in"><i class="sico fab-linkedin-in"></i></a>
                                        </span>
                                    </div>
                                </div>
                            </div>


                            <div class="col-lg-3 col-sm-6">
                                <div class="ourteam-01 mb-50">
                                    <div class="pic">
                                        <img class="img-Lazy" alt="Ross Williams" src="/Portals/_default/ContentBuilder/minis-page/our-team/ourteam-pic04.jpg">
                                    </div>
                                    <div class="content">
                                        <h6 class="title">Willie Perry</h6>
                                        <small>Phasellus a placerat arcu</small>
                                        <span class="icon">
                                            <a href="#" title="sico fab-facebook-f"><i class="sico fab-facebook-f"></i></a>
                                            <a href="#" title="sico fab-twitter"><i class="sico fab-twitter"></i></a>
                                            <a href="#" title="sico fab-linkedin-in"><i class="sico fab-linkedin-in"></i></a>
                                        </span>
                                    </div>
                                </div>
                            </div>


                            <div class="col-lg-3 col-sm-6">
                                <div class="ourteam-01 mb-50">
                                    <div class="pic">
                                        <img class="img-Lazy" alt="Ross Williams" src="/Portals/_default/ContentBuilder/minis-page/our-team/ourteam-pic05.jpg">
                                    </div>
                                    <div class="content">
                                        <h6 class="title">Paul Hicks</h6>
                                        <small>Praesent nec suscipit arcu</small>
                                        <span class="icon">
                                            <a href="#" title="sico fab-facebook-f"><i class="sico fab-facebook-f"></i></a>
                                            <a href="#" title="sico fab-twitter"><i class="sico fab-twitter"></i></a>
                                            <a href="#" title="sico fab-linkedin-in"><i class="sico fab-linkedin-in"></i></a>
                                        </span>
                                    </div>
                                </div>
                            </div>


                            <div class="col-lg-3 col-sm-6">
                                <div class="ourteam-01 mb-50">
                                    <div class="pic">
                                        <img class="img-Lazy" alt="Ross Williams" src="/Portals/_default/ContentBuilder/minis-page/our-team/ourteam-pic06.jpg">
                                    </div>
                                    <div class="content">
                                        <h6 class="title">Greg Wilson</h6>
                                        <small>Interdum et malesuada fames</small>
                                        <span class="icon">
                                            <a href="#" title="sico fab-facebook-f"><i class="sico fab-facebook-f"></i></a>
                                            <a href="#" title="sico fab-twitter"><i class="sico fab-twitter"></i></a>
                                            <a href="#" title="sico fab-linkedin-in"><i class="sico fab-linkedin-in"></i></a>
                                        </span>
                                    </div>
                                </div>
                            </div>


                            <div class="col-lg-3 col-sm-6">
                                <div class="ourteam-01 mb-50">
                                    <div class="pic">
                                        <img class="img-Lazy" alt="Ross Williams" src="/Portals/_default/ContentBuilder/minis-page/our-team/ourteam-pic07.jpg">
                                    </div>
                                    <div class="content">
                                        <h6 class="title">James Shrut</h6>
                                        <small>Nullam tempor egestas porta</small>
                                        <span class="icon">
                                            <a href="#" title="sico fab-facebook-f"><i class="sico fab-facebook-f"></i></a>
                                            <a href="#" title="sico fab-twitter"><i class="sico fab-twitter"></i></a>
                                            <a href="#" title="sico fab-linkedin-in"><i class="sico fab-linkedin-in"></i></a>
                                        </span>
                                    </div>
                                </div>
                            </div>


                            <div class="col-lg-3 col-sm-6">
                                <div class="ourteam-01 mb-50">
                                    <div class="pic">
                                        <img class="img-Lazy" alt="Ross Williams" src="/Portals/_default/ContentBuilder/minis-page/our-team/ourteam-pic08.jpg">
                                    </div>
                                    <div class="content">
                                        <h6 class="title">Olivia Boyd</h6>
                                        <small>Maecenas sed sollicitudin</small>
                                        <span class="icon">
                                            <a href="#" title="sico fab-facebook-f"><i class="sico fab-facebook-f"></i></a>
                                            <a href="#" title="sico fab-twitter"><i class="sico fab-twitter"></i></a>
                                            <a href="#" title="sico fab-linkedin-in"><i class="sico fab-linkedin-in"></i></a>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style class="build-css" data-class="title-14">${csstemplate["title-14"]}</style>
<style class="build-css" data-class="ourteam-01">${csstemplate["ourteam-01"]}</style>
    `
}, {
    'thumbnail': 'minis-section/page/section-12.jpg',
    'category': '816',
    'html': `<div class="is-section is-box is-bg-grey">
    <div class="is-overlay">
        <div class="is-overlay-bg" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/our-team/ourteam-bg02.jpg&quot;);"></div>
    </div>
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="spacer height-20"></div>
                        <div class="title-09">
                            <small>PROFESSIONAL SKILL</small>
                            <h3>
                                Greatly Boost Your Business With <span class="color-accent">Advanced Skills</span></h3>
                        </div>
                        <div class="custom-module loading" id="module-{id}" data-moduleid="{id}" data-style="progressbar02" data-effect="progressbar" data-settings="%7B%22style%22%3A%22progressbar02%22%2C%22moduleid%22%3A%22HzsAsYz%22%2C%22items%22%3A%7B%22item0%22%3A%7B%22title%22%3A%22Web%20Design%22%2C%22progress%22%3A%2292%22%7D%2C%22item1%22%3A%7B%22title%22%3A%22PhotoShop%22%2C%22progress%22%3A%2288%22%2C%22color%22%3A%22%23ff6e2f%22%7D%2C%22item2%22%3A%7B%22title%22%3A%22HTML5%20%26%20CSS3%22%2C%22progress%22%3A%2286%22%2C%22color%22%3A%22%2320a3fd%22%7D%2C%22item3%22%3A%7B%22title%22%3A%22UI%22%2C%22progress%22%3A%2295%22%2C%22color%22%3A%22%233cb33d%22%7D%7D%7D"></div>
                        <div class="spacer height-20 mb-10"></div>
                    </div>
                    <div class="col-md-6"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<style class="build-css" data-class="title-09">${csstemplate["title-09"]}</style>
`
}, {
    'thumbnail': 'minis-section/page/section-13.jpg',
    'category': '816',
    'html': `<div class="is-section is-box">
    <div class="is-overlay">
        <div class="is-overlay-bg is-scale-animated" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/our-team/ourteam-bg03.jpg&quot;);"></div>
    </div>
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="title-14 mt-10 text-center">
                            <small>TRANSPARENT</small>
                            <h2>What Our Clients Say</h2>
                        </div>
                        ${getSnippetCode("testimonials04")}
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="spacer height-80 d-none d-lg-block"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style class="build-css" data-class="title-14">${csstemplate["title-14"]}</style>
`
}, {
    'thumbnail': 'minis-section/page/section-14.jpg',
    'category': '816',
    'html': `<div class="is-section is-box section-no-padding is-light-text is-align-center">
    <div class="is-container layout-container">
        <div class="row ourteam-contact align-items-center" style="background-color: rgb(27, 188, 154); min-width: 100%;">
            <div class="col-md-3" style="background-color: rgb(63, 70, 96);">
                <div class="title-11 text-left mb-0">
                    <h3>Get In Touch With Us</h3>
                </div>
            </div>
            <div class="col-md-3">
                <div class="icon"><i class="sico lnr-phone-wave"></i></div>
                <p class="mb-0">+1 845-359-0545</p>
                <p class="mb-0">+1 845-359-0532</p>
            </div>
            <div class="col-md-3">
                <div class="icon"><i class="sico lnr-envelope-open"></i></div>
                <p class="mb-0">service.simple@gmail.com</p>
                <p class="mb-0">other@gmail.com</p>
            </div>
            <div class="col-md-3">
                <div class="icon"><i class="sico lnr-map-marker"></i></div>
                <p class="mb-0">Yanta W Rd, Yanta District,</p>
                <p class="mb-0">Xian Shi, Shaanxi</p>
            </div>
        </div>
    </div>
</div>
<div class="is-section is-box section-no-padding is-dark-text">
    <div class="is-boxes no-min-height">
        <div class="is-box-centered">
            <div class="is-container no-space layout-container is-container-fluid">
                <div class="row col-no-padding">
                    <div class="col-md-12">
                        <div class="custom-module loading" data-effect="gmap" id="module-{id}" data-moduleid="{id}" data-settings="%7B%22address%22%3A%22740%20Prince%20Avenue%20Athens%2C%20Georgia%2030601%2C%20740%20Prince%20Ave%2C%20Athens%2C%20GA%2030606%2C%20740%22%2C%22markers%22%3A%22%3Ch5%20class%3D%5C%22title%5C%22%3ECompany%20Name%3C%2Fh5%3E%5Cn%3Cdl%3E%5Cn%20%20%20%20%3Cdt%3EAddress%3A%201000%20Palisades%20Center%20Dr%2C%20West%20Nyack%2C%20NY%2010994%3C%2Fdt%3E%5Cn%20%20%20%20%3Cdt%3ETel%3A%20%2B1%20845-727-3501%3C%2Fdt%3E%5Cn%20%20%20%20%3Cdt%3EEmail%3A%20%3Ca%20href%3D%5C%22mailto%3Asample%40gmail.com%5C%22%3Esample%40gmail.com%3C%2Fa%3E%3C%2Fdt%3E%5Cn%20%20%20%20%3Cdt%3EHours%3A%208%3A00am-5%3A30pm%20(Mon-Fri)%3C%2Fdt%3E%5Cn%3C%2Fdl%3E%22%2C%22markerscolor%22%3A%22%23303030%22%2C%22markersbg%22%3A%22%23ffffff%22%2C%22markersicon%22%3A%22%5BSkinPath%5Dresource%2Fthumbnails%2Fmap%2Fmapicon06.png%22%2C%22zoom%22%3A%2216%22%2C%22linktype%22%3A%22roadmap%22%2C%22xxl%22%3A%22700%22%2C%22xl%22%3A%22600%22%2C%22l%22%3A%22500%22%2C%22m%22%3A%22400%22%2C%22s%22%3A%22300%22%2C%22xs%22%3A%22260%22%2C%22type%22%3A%22roadmap%22%2C%22position%22%3A%22right%22%7D"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style class="build-css" data-class="title-11">${csstemplate["title-11"]}</style>
<style class="build-css" data-class="ourteam-contact">
    .ourteam-contact {
        box-shadow: 0 0 15px rgba(0, 0, 0, .2);
        position: absolute !important;
        -webkit-transform: translateY(-50%);
        transform: translateY(-50%);
        z-index: 100
    }

    .ourteam-contact>div:not(.is-row-tool) {
        padding: 10px 50px;
        position: relative;
    }

    .ourteam-contact>div:first-child {
        padding: 35px 50px;
    }

    .ourteam-contact .icon {
        font-size: 42px;
        line-height: 1;
        margin-bottom: 6px;
    }

    .ourteam-contact>div::after {
        content: "";
        position: absolute;
        top: 10px;
        bottom: 10px;
        border-left: 1px dashed rgba(255, 255, 255, .5);
        right: 0;
    }

    .ourteam-contact>div:first-child::after,
    .ourteam-contact>div:last-child::after {
        content: none;
    }

    @media all and (max-width: 991px) {
        .ourteam-contact {
            position: relative !important;
            -webkit-transform: none;
            transform: none;
            margin: 0px -20px;
        }

        .ourteam-contact>div:not(.is-row-tool) {
            padding: 8px;
        }
    }

    @media all and (max-width: 767px) {
        .ourteam-contact {
            position: relative !important;
            -webkit-transform: none;
            transform: none;
            margin: 0px -15px;
        }

        .ourteam-contact>div:not(.is-row-tool) {
            padding: 50px;
        }

        .ourteam-contact>div::after {
            bottom: 0px;
            top: auto;
            left: 20px;
            right: 20px;
            border: none;
            border-bottom: 1px dashed rgba(255, 255, 255, .5);
        }
    }
</style>
    `
}, {
    'thumbnail': 'minis-section/page/section-15.jpg',
    'category': '816',
    'html': `<div class="is-section is-box">
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container mb-0">
            <div class="row">
                    <div class="col-md-12" data-noedit="">
                        <div class="spacer height-20 d-none d-md-block">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-7 pr-lg-45">
                        <h2 class="mt-10 mt-md-0"><span class="color-accent" style="font-weight: 700;">Laura Nixon</span> <small class="size-18" style="font-weight: 600;">-&nbsp; Senior Programmer</small></h2>
                        <div class="spacer height-20"></div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas et nibh ut quam hendrerit auctor vel a tortor. Donec consequat mauris magna, vel laoreet nisi imperdiet ac. Vivamus dignissim luctus velit in faucibus. Aenean molestie sapien ut aliquam facilisis. Mauris vitae gravida leo. Suspendisse pulvinar tortor non tincidunt venenatis.</p>
                        <div class="spacer height-20"></div>
                        ${getSnippetCode("iconbox-12")}
                        <div style="border-bottom: 1px solid rgb(235, 235, 235);" class="mb-25"></div>
                        <div class="edit-box mb-5">
                            <a class="social-01 facebook-f" href="#" title=""><i class="sico fab-twitter"></i></a>
                            <a class="social-01 linkedin-in" href="#" title=""><i class="sico fab-pinterest-p"></i></a>
                            <a class="social-01 twitter" href="#" title=""><i class="sico fab-linkedin-in"></i></a>
                        </div>
                        <div class="spacer d-none d-lg-block height-120"></div>
                        <div class="spacer d-blockd-lg-none height-20"></div>
                    </div>
                    <div class="col-md-5 img-Lazy-warp"><img src="data:image/svg+xml,%3Csvg viewBox='0 0 470 569' width='470' height='569' xmlns='http://www.w3.org/2000/svg'/%3E" class="img-Lazy shadow-md" data-src="/Portals/_default/ContentBuilder/minis-page/our-team-single/ourteam-single-img01.jpg"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="is-section is-box is-bg-none">
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container mt-0">
                <div class="row">
                    <div class="col-lg-10 ourteamsingle-section01">
                    ${getSnippetCode("counter-04")}
                    <div class="spacer height-20"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style class="build-css" data-class="social-01">${csstemplate["social-01"]}</style>
<style class="build-css" data-class="ourteamsingle-section01">
    .ourteamsingle-section01 {
        margin-top: -80px;
    }
    @media only screen and (max-width:991px) {
        .ourteamsingle-section01 {
            margin: 0px;
        }

    }
</style>`
}, {
    'thumbnail': 'minis-section/page/section-16.jpg',
    'category': '816',
    'html': `<div class="is-section is-box" style="background-color: rgb(237, 249, 247);">
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container mb-50 mb-md-80 pb-md-10" dragwithouthandle="">
                <div class="row">
                    <div class="col-md-12">
                        <div class="title-14 mt-10 text-center">
                            <small>GREAT INGENUITY</small>
                            <h2>From Latest Lovely Work</h2>
                        </div>
                        <div class="spacer height-20 mb-10">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="img-box05">
                            <div class="pic color-accent">
                                <a href="#" title="">
                                    <img class="img-Lazy" alt="Image" src="/Portals/_default/ContentBuilder/minis-page/our-team-single/ourteam-single-img02.jpg">
                                </a>
                            </div>
                            <div class="content edit-box">
                                <h6>Best Sell Magazine</h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="img-box05">
                            <div class="pic color-accent">
                                <a href="#" title="">
                                    <img class="img-Lazy" alt="Image" src="/Portals/_default/ContentBuilder/minis-page/our-team-single/ourteam-single-img03.jpg">
                                </a>
                            </div>
                            <div class="content edit-box">
                                <h6>Bird Of Paradise</h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="img-box05">
                            <div class="pic color-accent">
                                <a href="#" title="">
                                    <img class="img-Lazy" alt="Image" src="/Portals/_default/ContentBuilder/minis-page/our-team-single/ourteam-single-img04.jpg">
                                </a>
                            </div>
                            <div class="content edit-box">
                                <h6>Guitar-shaped Accessory</h6>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="img-box05">
                            <div class="pic color-accent">
                                <a href="#" title="">
                                    <img class="img-Lazy" alt="Image" src="/Portals/_default/ContentBuilder/minis-page/our-team-single/ourteam-single-img05.jpg">
                                </a>
                            </div>
                            <div class="content edit-box">
                                <h6>Beautiful Sea Tower</h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="img-box05">
                            <div class="pic color-accent">
                                <a href="#" title="">
                                    <img class="img-Lazy" alt="Image" src="/Portals/_default/ContentBuilder/minis-page/our-team-single/ourteam-single-img06.jpg">
                                </a>
                            </div>
                            <div class="content edit-box">
                                <h6>Wonderful Combination</h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="img-box05">
                            <div class="pic color-accent">
                                <a href="#" title="">
                                    <img class="img-Lazy" alt="Image" src="/Portals/_default/ContentBuilder/minis-page/our-team-single/ourteam-single-img07.jpg">
                                </a>
                            </div>
                            <div class="content edit-box">
                                <h6>Creative Fish Tank</h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style class="build-css" data-class="title-14">${csstemplate["title-14"]}</style>
<style class="build-css" data-class="img-box05">${csstemplate["img-box05"]}</style>
`
}, {
    'thumbnail': 'minis-section/page/section-17.jpg',
    'category': '816',
    'html': `<div class="is-section is-box">
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container">
                <div class="row">
                    <div class="col-md-6 pr-lg-45">
                        <div class="spacer height-20"></div>
                        <div><img class="img-Lazy shadow-md" alt="Image" src="/Portals/_default/ContentBuilder/minis-page/our-team-single/ourteam-single-img08.jpg"/></div>
                        <div class="spacer height-20"></div>
                    </div>
                    <div class="col-md-6">
                        <div class="spacer height-40"></div>
                        <div class="title-11 mr-lg-80  ml-lg-30">
                            <h3>Skilled People Always Come Up With <span class="color-accent">Creative Ideas</span> When Together </h3>
                        </div>
                        <div class="spacer height-20 d-none d-md-block"></div>
                        <div class="ourteam-single-section">
                                <div class="custom-module loading" id="module-{id}" data-moduleid="{id}" data-style="progressbar01" data-effect="progressbar" data-settings="%7B%22style%22%3A%22progressbar01%22%2C%22items%22%3A%7B%22item0%22%3A%7B%22title%22%3A%22Web%20Design%22%2C%22progress%22%3A%2292%22%2C%22color%22%3A%22%231bbc9b%22%7D%2C%22item1%22%3A%7B%22title%22%3A%22PhotoShop%22%2C%22progress%22%3A%2288%22%2C%22color%22%3A%22%2321a2fd%22%7D%2C%22item2%22%3A%7B%22title%22%3A%22HTML5%20%26%20CSS3%22%2C%22progress%22%3A%2286%22%2C%22color%22%3A%22%233cb33d%22%7D%2C%22item3%22%3A%7B%22title%22%3A%22UI%20Design%22%2C%22progress%22%3A%2290%22%2C%22color%22%3A%22%23fe6e2f%22%7D%2C%22item4%22%3A%7B%22title%22%3A%22JavaScript%22%2C%22progress%22%3A%2296%22%2C%22color%22%3A%22%2300ac8a%22%7D%7D%2C%22moduleid%22%3A%22czCLMy%22%7D"></div>
                        </div>
                        <div class="spacer height-20"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style class="build-css" data-class="title-11">${csstemplate["title-11"]}</style>
<style class="build-css" data-class="ourteam-single-section">
    .ourteam-single-section {
        background-color: #FFF;
        box-shadow: 0 0 25px rgba(0, 0, 0, .15);
        padding: 55px 50px 60px;
        margin-left: -130px;
    }

    @media only screen and (max-width:767px) {
        .ourteam-single-section {
            margin: 0;
            padding: 30px;
        }
    }
</style>
 `
}, {
    'thumbnail': 'minis-section/page/section-18.jpg',
    'category': '816',
    'html': `<div class="is-section is-box is-bg-grey is-light-text is-align-center">
    <div class="is-overlay">
        <div class="is-overlay-bg" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/our-team-single/ourteam-single-bg01.jpg&quot;);"></div>
    </div>
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="spacer height-40"></div>
                        <h2 class="m-auto" style="max-width: 830px;">Looking For Fancy Products &amp; Services? We Are The Idea Team You Need</h2>
                        <div class="spacer height-20 mb-15"></div>
                        <div><a class="button-01" href="#" title="">CONTACT US NOW</a></div>
                        <div class="spacer height-60"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style class="build-css" data-class="button-01">${csstemplate["button-01"]}</style>

`
}, {
    'thumbnail': 'minis-section/page/section-19.jpg',
    'category': '816',
    'html': `<div class="is-section is-box">
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container pt-10" dragwithouthandle="">
                <div class="row">
                    <div class="col-md-6 pr-lg-80">
                        <div class="spacer height-40 d-none d-lg-block"></div>
                        <div class="title-09" style="margin-bottom: 50px;">
                            <small>LEAVE A MESSAGE</small>
                            <h3>Reach Us Out Anytime</h3>
                        </div>
                        <p>Facing any issue and can't find any solution? Don't worry, we are here to help. Your email address will not be published. </p>
                        <h6 class="color-accent mb-20 mt-35">Working Time</h6>
                        <div class="row align-items-center">
                            <div class="col-auto">
                                <i class="sico lnr-clock size-46"></i>
                            </div>
                            <div class="col edit-box pl-0">
                                <p class="m-auto">Monday-Friday: 8:00am-5:30pm</p>
                                <p class="m-auto">Saturday: 10:00am-2:00pm</p>
                            </div>
                        </div>

                        <div class="spacer height-40"></div>
                    </div>
                    <div class="col-md-6">
                        ${getSnippetCode("ajaxform-03")}
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="spacer height-120 d-none d-lg-block"></div>
                        <div class="mb-20 d-lg-none"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style class="build-css" data-class="title-09">${csstemplate["title-09"]}</style>
`
}, {
    'thumbnail': 'minis-section/page/section-20.jpg',
    'category': '816',
    'html': `<div class="is-section is-box">
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="title-14 mt-10 text-center">
                            <small>INFO YOU CARE</small>
                            <h2>FAQs Updated In Real-time</h2>
                        </div>
                        <div class="spacer height-20 mb-10"></div>
                        ${getSnippetCode("faq-03")}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><style class="build-css" data-class="title-14">${csstemplate["title-14"]}</style>`
})
data_basic.designs.push({
    'thumbnail': 'minis-section/page/section-21.jpg',
    'category': '816',
    'html': `<div class="is-section is-box is-align-center aboutUs02-section01 layout-no-mb layout-no-mt">
    <div class="is-overlay">
        <div class="is-overlay-bg" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/about-us02/aboutUs02-section01-bg.jpg&quot;);"></div>
    </div>
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container" dragwithouthandle="">
                <div class="row">
                    <div class="col-md-12">
                        <div class="spacer height-100 mb-20 mb-lg-0"></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="title-07 mt-10 mb-25">
                            <small class="color-accent size-18" style="margin-bottom: 7px; font-weight: 700;">CLEVERLY CONSTRUCTED</small>
                            <h3>Let Them Know Why You Are The Best.</h3>
                        </div>
                        <p class="size-18 mb-25" style="color: rgb(51, 51, 51); font-weight: 600;">We have been developing and releasing products to be ahead of the curve.</p>
                        <p class="m-auto" style="max-width: 800px;">Curabitur massa nunc, consectetur ut purus in, volutpat elementum libero. Mauris dictum ultrices mi sit amet molestie. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum elementum tincidunt commodo.</p>
                        <div class="mt-50" style="font-size:0">
                            <a href="#" title="BUY IT NOW" class="button-01 border-radius-3 ml-15 mr-15 mb-10">BUY IT NOW</a>
                            <a href="#" title="CONTACT US" class="button-01 border-radius-3 ml-15 mr-15 mb-10">CONTACT US</a>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="spacer height-100 mb-30 mb-lg-5"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style class="build-css" data-class="title-07">${csstemplate["title-07"]}</style>
<style class="build-css" data-class="button-01">${csstemplate["button-01"]}</style>
`
}, {
    'thumbnail': 'minis-section/page/section-22.jpg',
    'category': '816',
    'html': `<div class="is-section is-box aboutUs02-section02">
	<div class="is-boxes">
		<div class="is-box-centered">
			<div class="is-container layout-container" dragwithouthandle="">
				<div class="row">
					<div class="col-lg-12">
						<div class="spacer height-20"></div>
					</div>
				</div>
				<div class="row iconbox-23-list">
					<div class="col-lg-4">
						<div class="title-09 mt-lg-80">
							<small>CREATIVE DESIGN</small>
							<h3>Tell Us Your Concept, We Will Deliver You a 
								<span class="color-accent">Striking Website.</span>
							</h3>
						</div>
						<p>Integer venenatis lacus facilisis augue tincidunt, id fermentum nibh lacinia. Donec ultrices dictum mauris eu
							suscipit.</p>
						<div class="spacer height-20 d-lg-none"></div>
					</div>
					<div class="col-md-6 col-lg-4">
						<div class="iconbox-23 edit-box color-1 is-light-text">
							<div class="icon">
								<i class="sico lnr-cash-yen"><svg>
										<use xlink:href="#lnr-cash-yen"></use>
									</svg></i>
							</div>
							<div class="title">
								<h6>Free Update</h6>
							</div>
							<div class="content">
								<p>Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum ante ipsum
									primis in faucibus orci cubilia.</p>
							</div>
							<div class="btn">
								<a href="#" title="VIEW MORE" class="iconbox-btn">VIEW MORE</a>
							</div>
						</div>
					</div>
					<div class="col-md-6 col-lg-4">
						<div class="iconbox-23 edit-box is-light-text">
							<div class="icon">
								<i class="sico lnr-laptop-phone"><svg>
										<use xlink:href="#lnr-laptop-phone"></use>
									</svg></i>
							</div>
							<div class="title">
								<h6>Powerful Support</h6>
							</div>
							<div class="content">
								<p>Tincidunt placerat risus nam at risus in dui facilisis pellentesque. Pellentesque tempus elit augue, eget
									tincidunt justo finibus iaculis nec mauris.</p>
							</div>
							<div class="btn">
								<a href="#" title="VIEW MORE" class="iconbox-btn">VIEW MORE</a>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<div class="spacer height-20"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<style class="build-css" data-class="title-09">${csstemplate["title-09"]}</style>
<style class="build-css" data-class="iconbox-23">${csstemplate["iconbox-23"]}</style>
`
}, {
    'thumbnail': 'minis-section/page/section-23.jpg',
    'category': '816',
    'html': `<div class="is-section is-box is-light-text aboutUs02-section03">
    <div class="is-overlay">
        <div class="is-overlay-bg" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/about-us02/aboutUs02-section03-bg.jpg&quot;); background-position: 50% 60%;"></div>
    </div>
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container" dragwithouthandle="">
                <div class="row">
                    <div class="col-md-12">
                        <div class="spacer height-40 d-none d-lg-block" style="margin-bottom: -3px"></div>
                        <div class="spacer height-20 d-lg-none" style="margin-bottom: -3px"></div>
                    </div>
                </div>
                <div class="row align-items-center justify-content-between">
                    <div class="col-md-9 col-lg-7">
                        <div class="text-center text-md-left">
                            <h3 style="margin-bottom: 23px;">Looking for Fancy Products &amp; Services? We Are the Idea Team You Need.</h3>
                            <p class="size-20 mb-0">Orci varius natoque penatibus et magnis dis parturient montes</p>
                        </div>
                        <div class="spacer height-40 d-md-none"></div>
                    </div>
                    <div class="col-md-3">
                        <div class="text-center">
                            <a class="play-button is-lightbox" href="https://www.youtube.com/embed/AkcUoA2f-jU" data-ilightbox="youtube" title="">
                                <span>
                                    <i class="sico fas-play color-accent size-18"><svg><use xlink:href="#fas-play"></use></svg></i>
                                </span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="spacer height-40 d-none d-lg-block" style="margin-bottom: 2px"></div>
                        <div class="spacer height-20 d-lg-none" style="margin-bottom: 2px"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style class="build-css" data-class="play-button">${csstemplate["play-button"]}</style>
`
}, {
    'thumbnail': 'minis-section/page/section-24.jpg',
    'category': '816',
    'html': `<div class="is-section is-box aboutUs02-section04 layout-no-mb">
    <div class="is-overlay"></div>
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container" dragwithouthandle="">
                <div class="row">
                    <div class="col-md-12">
                        <div class="spacer height-20" style="margin-bottom: -8px;"></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="text-center m-auto" style="max-width: 800px;">
                            <div class="title-14">
                                <h3 style="margin-top:0;">Our Innovative Teams</h3>
                            </div>
                            <p>Nulla et arcu quis est volutpat pellentesque. Donec at luctus tellus, non venenatis ante. Sed id eros fringilla, iaculis arcu vitae, ornare diam.</p>
                        </div>
                        <div class="spacer height-60" style="margin-top: -3px;"></div>
                    </div>
                </div>
                <div class="row ourteam-03-list">
                    <div class="col-lg-6">
                        <div class="ourteam-03">
                            <div class="pic">
                                <img src="/Portals/_default/ContentBuilder/minis-page/about-us02/aboutUs02-section04-img01.jpg" alt="Michale Joe" class="img-Lazy">
                                <div class="share">
                                    <a href="#">
                                        <i class="sico fab-twitter"><svg><use xlink:href="#fab-twitter"></use></svg></i>
                                    </a>
                                    <a href="#">
                                        <i class="sico fab-pinterest-p"><svg><use xlink:href="#fab-pinterest-p"></use></svg></i>
                                    </a>
                                    <a href="#">
                                        <i class="sico fab-linkedin-in"><svg><use xlink:href="#fab-linkedin-in"></use></svg></i>
                                    </a>
                                </div>
                            </div>
                            <div class="content">
                                <div class="title">
                                    <h6>Michale Joe</h6>
                                </div>
                                <div class="info">
                                    <p>Integer non nisl commodo, tincidunt ligula eget, tincidunt odio. Interdum et fames ac ante ipsum primis in faucibus.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="ourteam-03">
                            <div class="pic">
                                <img src="/Portals/_default/ContentBuilder/minis-page/about-us02/aboutUs02-section04-img02.jpg" alt="Jack Simmons" class="img-Lazy">
                                <div class="share">
                                    <a href="#">
                                        <i class="sico fab-twitter"><svg><use xlink:href="#fab-twitter"></use></svg></i>
                                    </a>
                                    <a href="#">
                                        <i class="sico fab-pinterest-p"><svg><use xlink:href="#fab-pinterest-p"></use></svg></i>
                                    </a>
                                    <a href="#">
                                        <i class="sico fab-linkedin-in"><svg><use xlink:href="#fab-linkedin-in"></use></svg></i>
                                    </a>
                                </div>
                            </div>
                            <div class="content">
                                <div class="title">
                                    <h6>Jack Simmons</h6>
                                </div>
                                <div class="info">
                                    <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row ourteam-03-list">
                    <div class="col-lg-6">
                        <div class="ourteam-03">
                            <div class="pic">
                                <img src="/Portals/_default/ContentBuilder/minis-page/about-us02/aboutUs02-section04-img03.jpg" alt="Alyssa Hiyama" class="img-Lazy">
                                <div class="share">
                                    <a href="#">
                                        <i class="sico fab-twitter"><svg><use xlink:href="#fab-twitter"></use></svg></i>
                                    </a>
                                    <a href="#">
                                        <i class="sico fab-pinterest-p"><svg><use xlink:href="#fab-pinterest-p"></use></svg></i>
                                    </a>
                                    <a href="#">
                                        <i class="sico fab-linkedin-in"><svg><use xlink:href="#fab-linkedin-in"></use></svg></i>
                                    </a>
                                </div>
                            </div>
                            <div class="content">
                                <div class="title">
                                    <h6>Alyssa Hiyama</h6>
                                </div>
                                <div class="info">
                                    <p>Vivamus hendrerit sem vel libero facilisis, consectetur interdum est ultrices. Orci varius natoque penatibus.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="ourteam-03">
                            <div class="pic">
                                <img src="/Portals/_default/ContentBuilder/minis-page/about-us02/aboutUs02-section04-img04.jpg" alt="Renita Gillenwater" class="img-Lazy">
                                <div class="share">
                                    <a href="#">
                                        <i class="sico fab-twitter"><svg><use xlink:href="#fab-twitter"></use></svg></i>
                                    </a>
                                    <a href="#">
                                        <i class="sico fab-pinterest-p"><svg><use xlink:href="#fab-pinterest-p"></use></svg></i>
                                    </a>
                                    <a href="#">
                                        <i class="sico fab-linkedin-in"><svg><use xlink:href="#fab-linkedin-in"></use></svg></i>
                                    </a>
                                </div>
                            </div>
                            <div class="content">
                                <div class="title">
                                    <h6>Renita Gillenwater</h6>
                                </div>
                                <div class="info">
                                    <p>Etiam auctor est quis metus tristique pharetra. Maecenas fermentum est nec nulla viverra efficitur.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12" data-noedit="">
                        <div class="spacer height-120"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style class="build-css" data-class="title-14">${csstemplate["title-14"]}</style>
<style class="build-css" data-class="ourteam-03">${csstemplate["ourteam-03"]}</style>`
}, {
    'thumbnail': 'minis-section/page/section-25.jpg',
    'category': '816',
    'html': `<div class="is-section is-box aboutUs02-section05">
	<div class="is-overlay">
		<div class="is-overlay-bg" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/about-us02/aboutUs02-section05-bg.jpg&quot;);"></div>
	</div>
	<div class="is-boxes">
		<div class="is-box-centered">
			<div class="is-container layout-container" dragwithouthandle="">
				<div class="row">
					<div class="col-md-12">
						<div class="spacer height-20"></div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-5">
						<div class="aboutUs02-section05-form edit-box">
							<div class="title-14">
								<h3>Get In Touch</h3>
							</div>
							<p>Facing any issue and can't find any solution? Just write to us, we are here to help.</p>
							<div class="spacer height-20" style="margin-bottom:5px;"></div>
                             ${getSnippetCode("ajaxform-13")}
						</div>
					</div>
					<div class="col-lg-7 pl-md-45">
                    ${getSnippetCode("infobox-17")}
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="spacer height-20"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<style class="build-css" data-class="title-14">${csstemplate["title-14"]}</style>
<style class="build-css" data-class="aboutUs02-section05-form">
	.aboutUs02-section05-form {
		text-align: center;
		background: #fff;
		border-radius: 3px;
		padding: 47px 40px 60px 40px;
		height: 100%;
	}
</style>
`
}, {
    'thumbnail': 'minis-section/page/section-26.jpg',
    'category': '816',
    'html': `<div class="is-section is-box aboutUs02-section06">
	<div class="is-overlay">
		<div class="is-overlay-bg" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/about-us02/aboutUs02-section06-bg.png&quot;); background-position: 50% 50%; background-size: auto; background-repeat: no-repeat;"></div>
	</div>
	<div class="is-boxes">
		<div class="is-box-centered">
			<div class="is-container layout-container" dragwithouthandle="">
				<div class="row">
					<div class="col-md-12">
						<div class="spacer height-20" style="margin-bottom: -8px;"></div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="title-14">
							<h3 style="margin-top:0;">What Our Clients Say</h3>
						</div>
						<div class="spacer height-40"></div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="custom-module m-auto loading" style="max-width:800px;" id="module-{id}" data-moduleid="{id}" data-effect="testimonials" data-style="testimonials01" data-settings="%7B%22items%22%3A%7B%22item0%22%3A%7B%22title%22%3A%22Jennifer%20Freeman%22%2C%22job%22%3A%22Free-lancer%22%2C%22single%22%3A%22%2FPortals%2F_default%2FContentBuilder%2Fminis-page%2Fhome03%2Fhomepage03-testimonials01.jpg%22%2C%22singlewidth%22%3A%2292%22%2C%22singleheight%22%3A%2292%22%2C%22description%22%3A%22Easy%20module%20to%20work%20with%2C%20awesome%20addition%20to%20any%20website.%20And%20their%20always%20quick%20to%20respond%20to%20any%20support%20questions.%22%2C%22rating%22%3A%225%22%7D%2C%22item1%22%3A%7B%22title%22%3A%22Harry%20Brown%22%2C%22job%22%3A%22Free-lancer%22%2C%22single%22%3A%22%2FPortals%2F_default%2FContentBuilder%2Fminis-page%2Fhome03%2Fhomepage03-testimonials02.jpg%22%2C%22singlewidth%22%3A%2292%22%2C%22singleheight%22%3A%2292%22%2C%22description%22%3A%22Great%20package!%20The%20support%20is%20outstanding.%20Courteous%2C%20prompt%20and%20very%20helpful.%20We%20are%20very%20happy%20with%20everything%22%2C%22rating%22%3A%225%22%7D%2C%22item2%22%3A%7B%22title%22%3A%22Ross%20Williams%22%2C%22job%22%3A%22Free-lancer%22%2C%22single%22%3A%22%2FPortals%2F_default%2FContentBuilder%2Fminis-page%2Fhome03%2Fhomepage03-testimonials03.jpg%22%2C%22singlewidth%22%3A%2292%22%2C%22singleheight%22%3A%2292%22%2C%22description%22%3A%22They%20surprised%20us%20with%20their%20innovative%20products%20every%20time.%20We%20have%20been%20enjoying%20their%20great%20skins%2C%20modules%20and%20services%20all%20the%20time%22%2C%22rating%22%3A%225%22%7D%2C%22item3%22%3A%7B%22title%22%3A%22Marcy%20Carsey%22%2C%22job%22%3A%22Free-lancer%22%2C%22single%22%3A%22%2FPortals%2F_default%2FContentBuilder%2Fminis-page%2Fhome03%2Fhomepage03-testimonials04.jpg%22%2C%22singlewidth%22%3A%2292%22%2C%22singleheight%22%3A%2292%22%2C%22description%22%3A%22Whenever%20we%20reach%20out%20them%20to%20help%20us%20solving%20some%20issues%2C%20they%20have%20everything%20corrected%20very%20fast.%20Best%20customer%20service%20ever!%22%2C%22rating%22%3A%225%22%7D%2C%22item4%22%3A%7B%22title%22%3A%22David%20Smith%22%2C%22job%22%3A%22Free-lancer%22%2C%22single%22%3A%22%2FPortals%2F_default%2FContentBuilder%2Fminis-page%2Fhome03%2Fhomepage03-testimonials05.jpg%22%2C%22singlewidth%22%3A%2292%22%2C%22singleheight%22%3A%2292%22%2C%22description%22%3A%22They%20use%20advanced%20concept%20and%20fashionable%20elements%20to%20our%20project%2C%20delivering%20us%20a%20wonderful%20and%20imaginative%20website.%22%2C%22rating%22%3A%225%22%7D%7D%2C%22categorys%22%3A%22Electronics%2COffice%20Supply%2CPlant%2CSweets%22%2C%22lazy%22%3A%22checked%22%2C%22delay%22%3A%225000%22%2C%22autoplay%22%3A%22checked%22%7D"></div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="spacer height-40" style="margin-bottom: -10px;"></div>
					</div>
				</div>
				<div class="row clientlist01" style="padding-bottom: 5px;">
					<div class="col-md-2 col-4">
						<span>
							<a href="#" title="">
								<img src="/Portals/_default/ContentBuilder/minis-page/about-us02/aboutUs02-section06-client01.png" class="img-Lazy"
								 alt=""></a>
						</span>
					</div>
					<div class="col-md-2 col-4">
						<span>
							<a href="#" title="">
								<img src="/Portals/_default/ContentBuilder/minis-page/about-us02/aboutUs02-section06-client02.png" class="img-Lazy"
								 alt=""></a>
						</span>
					</div>
					<div class="col-md-2 col-4">
						<span>
							<a href="#" title="">
								<img src="/Portals/_default/ContentBuilder/minis-page/about-us02/aboutUs02-section06-client03.png" class="img-Lazy"
								 alt=""></a>
						</span>
					</div>
					<div class="col-md-2 col-4">
						<span>
							<a href="#" title="">
								<img src="/Portals/_default/ContentBuilder/minis-page/about-us02/aboutUs02-section06-client04.png" class="img-Lazy"
								 alt=""></a>
						</span>
					</div>
					<div class="col-md-2 col-4">
						<span>
							<a href="#" title="">
								<img src="/Portals/_default/ContentBuilder/minis-page/about-us02/aboutUs02-section06-client05.png" class="img-Lazy"
								 alt=""></a>
						</span>
					</div>
					<div class="col-md-2 col-4">
						<span>
							<a href="#" title="">
								<img src="/Portals/_default/ContentBuilder/minis-page/about-us02/aboutUs02-section06-client06.png" class="img-Lazy"
								 alt=""></a>
						</span>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<style class="build-css" data-class="title-14">${csstemplate["title-14"]}</style>
<style class="build-css" data-class="clientlist01">${csstemplate["clientlist01"]}</style>
`
}, {
    'thumbnail': 'minis-section/page/footer.jpg',
    'category': '816',
    'html': `<div class="is-section is-box aboutUs02-footerTop" style="background-color: rgb(30, 30, 30);">
	<div class="is-boxes">
		<div class="is-box-centered">
			<div class="is-container layout-container" dragwithouthandle="">
				<div class="row size-16" style="color: rgb(135, 135, 135);">
					<div class="col-md-4">
						<div style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/home01/homepage01-footer-map.png&quot;); background-position: center center; background-repeat: no-repeat; background-size: auto;">
							<img src="/Portals/_default/ContentBuilder/minis-page/logo-white.png" style="margin-bottom: 39px;margin-top: 5px;">
							<p class="mb-0">Aliquam efficitur, nisl tempor imperdiet aliquam, neque sem mattis est, quis dapibus nisl ipsum
								in libero ultricies felis, gravida porttitor.Integer eu velit at dui convallis lacinia consectetur.</p>
							<div class="spacer height-40" style="margin-bottom: -3px;"></div>
							<h6 class="mb-0 mt-0" style="color: #ddd;">Subscribe to get free trial</h6>
							<div class="spacer height-20" style="margin-bottom: 2px;"></div>
						</div>
						<div class="custom-module mr-lg-40 loading" data-style="ajaxform-04" data-effect="ajaxform" id="module-{id}" data-moduleid="{id}"	 data-settings="%7B%22style%22%3A%22ajaxform-04%22%2C%22message%22%3A%22Thank%20you%2C%20your%20message%20has%20been%20submitted%20to%20us.%22%7D"></div>
						<div class="spacer height-40 d-md-none"></div>
					</div>
					<div class="col-md-4">
						<h5 style="color: rgb(255, 255, 255);" class="pt-10 mb-15">Contact Us</h5>
						<div class="spacer height-20"></div>
						<p class="mb-0">Free legal advice</p>
						<p class="size-24 mb-15 color-accent" style=" font-style: italic;">+1 845-359-0545</p>
						<p class="mb-0">Email address:</p>
						<p class="mb-25">dnnskindev@gmail.com/
							<br class="d-lg-none">admin@dnngo.net</p>
						<p class="mb-0">Address:</p>
						<p class="mb-0">Yanta W Rd, Yanta District, Xian Shi, Shaanxi</p>
						<div class="spacer height-40 d-md-none"></div>
					</div>
					<div class="col-md-4">
						<h5 style="color: rgb(255, 255, 255);" class="pt-10 mb-15">Friendly Links</h5>
						<div class="spacer height-20"></div>
						<div class="row home01-footer-linklist">
							<div class="col-sm-6">
								<p>
									<a href="#" title="What we do">What we do</a>
								</p>
								<p>
									<a href="#" title="Our service">Our service</a>
								</p>
								<p>
									<a href="#" title="Creative design">Creative design</a>
								</p>
								<p>
									<a href="#" title="Industry experts">Industry experts</a>
								</p>
								<p>
									<a href="#" title="Feature">Feature</a>
								</p>
								<p>
									<a href="#" title="Testimonials">Testimonials</a>
								</p>
							</div>
							<div class="col-sm-6">
								<p>
									<a href="#" title="Our team">Our team</a>
								</p>
								<p>
									<a href="#" title="Our pricing">Our pricing</a>
								</p>
								<p>
									<a href="#" title="FAQ">FAQ</a>
								</p>
								<p>
									<a href="#" title="The latest news">The latest news</a>
								</p>
								<p>
									<a href="#" title="Amazing support">Amazing support</a>
								</p>
								<p>
									<a href="#" title="Professional skills">Professional skills</a>
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="is-section is-box is-light-text layout-no-mt layout-no-mb aboutUs02-footerBottom" style="background-color: rgb(18, 18, 18);">
	<div class="is-boxes">
		<div class="is-box-centered">
			<div class="is-container layout-container" dragwithouthandle="">
				<div class="row align-items-center">
					<div class="spacer height-20"></div>
				</div>
				<div class="row align-items-center">
					<div class="col-md-auto">
						<p class="aboutUs02-footer-color mt-0 mb-0 text-center text-md-left">@ 2019 by DNNGo Corp &nbsp; | &nbsp;
							<a href="#" title="Privacy Statement">Privacy Statement</a>&nbsp; | &nbsp;
							<a href="#" title="Privacy Statement">Terms of Use</a>
						</p>
					</div>
					<div class="col-md">
						<div class="edit-box aboutUs02-footer-color text-center text-md-right">
							<a class="social-08" href="#">
								<i class="sico fab-twitter size-18"><svg>
										<use xlink:href="#fab-twitter"></use>
									</svg></i>
							</a>
							<a class="social-08" href="#">
								<i class="sico fab-facebook-f size-18"><svg>
										<use xlink:href="#fab-facebook-f"></use>
									</svg></i>
							</a>
							<a class="social-08" href="#">
								<i class="sico fas-info size-18"><svg>
										<use xlink:href="#fas-info"></use>
									</svg></i>
							</a>
							<a class="social-08" href="#">
								<i class="sico fab-tumblr size-18"><svg>
										<use xlink:href="#fab-tumblr"></use>
									</svg></i>
							</a>
							<a class="social-08" href="#">
								<i class="sico fab-google-plus-g size-18"><svg>
										<use xlink:href="#fab-google-plus-g"></use>
									</svg></i>
							</a>
						</div>
					</div>
				</div>
				<div class="row align-items-center">
					<div class="spacer height-20"></div>
				</div>
			</div>
		</div>
    </div>
    <style class="build-css" data-class="social-08">${csstemplate["social-08"]}</style>
	<style class="build-css" data-class="aboutUs02-footerBottom">
		.aboutUs02-footerBottom .is-boxes {
			min-height: 50px;
		}
	</style>
	<style class="build-css" data-class="aboutUs02-footer-color">
		p.aboutUs02-footer-color,
		.aboutUs02-footer-color,
		.aboutUs02-footer-color a:link,
		.aboutUs02-footer-color a:focus,
		.aboutUs02-footer-color a:active {
			color: #838383;
		}

		.aboutUs02-footer-color a:hover {
			color: var(--accent-color);
		}
	</style>
	<style class="build-css" data-class="home01-footer-linklist">
		.home01-footer-linklist a,
		.home01-footer-linklist a:link {
			color: currentColor;
		}

		.home01-footer-linklist a:hover {
			color: #FFF
		}

		.home01-footer-linklist p {
			margin-bottom: 10px;
		}
	</style>
</div>
`
}, {
    'thumbnail': 'minis-section/page/section-27.jpg',
    'category': '816',
    'html': `<div class="is-section is-box">
	<div class="is-boxes">
		<div class="is-box-centered">
			<div class="is-container layout-container" dragwithouthandle="">
				<div class="row">
					<div class="col-md-12">
						<div class="title-08 ml-auto mr-auto mt-10 text-center" style="max-width: 650px;">
							<small class="color-accent">YOU WILL GET TO KNOW MORE ABOUT US FROM HERE</small>
							<h3>It’s All About Turning Great Ideas Into Wonderful Websites</h3>
						</div>
					</div>
				</div>        
                ${getSnippetCode("timeline01-html")}
			</div>
		</div>
	</div>
</div>
<style class="build-css" data-class="title-08">${csstemplate["title-08"]}</style>
`
}, {
    'thumbnail': 'minis-section/page/section-28.jpg',
    'category': '816',
    'html': `<div class="is-section is-box" style="background-color: rgb(245, 245, 245);">
	<div class="is-boxes">
		<div class="is-box-centered">
			<div class="is-container layout-container" dragwithouthandle="">
				<div class="row align-items-center">
					<div class="col-md-5">
						<div class="title-09">
							<small>WE ARE YOUR SOLID BACKING</small>
							<h3>What Makes Us So Prominent is Our Professional Attitude <span class="color-accent">and Pragmatic Spirit.</span>
							</h3>
						</div>
						<p>Curabitur ac malesuada tellus, eget mattis augue. Morbi hendrerit placerat dui sed mollisur euismod a. Phasellus aliquet tincidunt libero vel mollisn am mauris metus porttitor mollislaoreet vitae gravida eget massa.</p>
						<div class="spacer height-40"></div>
						<p>
							<a href="#" title="CLICK HERE" class="button-01 border-radius-3">CLICK HERE</a>
						</p>
					</div>
					<div class="col-md-7 right-full-column">
						<div class="full-column-inner mt-5 mb-5">
							<img class="img-Lazy" src="/Portals/_default/ContentBuilder/minis-page/about-us03/aboutUs03-img01.png" alt=""></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<style class="build-css" data-class="title-09">${csstemplate["title-09"]}</style>
<style class="build-css" data-class="button-01">${csstemplate["button-01"]}</style>
`
}, {
    'thumbnail': 'minis-section/page/section-29.jpg',
    'category': '816',
    'html': `<div class="is-section is-box is-light-text is-align-center">
	<div class="is-overlay">
		<div class="is-overlay-bg" style="background-image:url(&quot;/Portals/_default/ContentBuilder/minis-page/about-us03/aboutUs03-bg01.jpg&quot;)"></div>
	</div>
	<div class="is-boxes">
		<div class="is-box-centered">
			<div class="is-container layout-container" dragwithouthandle="">
				<div class="row">
					<div class="col-md-12">
						<a class="play-button is-lightbox" href="https://www.youtube.com/embed/AkcUoA2f-jU" data-ilightbox="youtube"
						 title="">
							<span style="font-size: 14px;" class="color-accent">
								<i class="sico fas-play size-18"><svg>
										<use xlink:href="#fas-play"></use>
									</svg></i>
							</span>
						</a>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12" data-noedit="">
						<div class="spacer height-40 mb-10"></div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<h2 class="m-auto" style="max-width: 570px; font-size: 36px;">Enjoy the Diversity Brought by the Wonderful Theme</h2>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12" data-noedit="">
						<div class="spacer height-100 mb-40 mb-lg-5"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<style class="build-css" data-class="play-button">${csstemplate["play-button"]}</style>
<div class="is-section is-box layout-no-mt layout-no-mb">
	<div class="is-boxes">
		<div class="is-box-centered">
			<div class="is-container layout-container" dragwithouthandle="">
				 ${getSnippetCode("iconbox-24")}
			</div>
		</div>
	</div>
</div>
`
}, {
    'thumbnail': 'minis-section/page/section-30.jpg',
    'category': '816',
    'html': `<div class="is-section is-box layout-no-mt">
	<div class="is-boxes">
		<div class="is-box-centered">
			<div class="is-container layout-container" dragwithouthandle="">
				<div class="row">
					<div class="col-md-12" data-noedit="">
						<div class="spacer height-60 mb-15"></div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6 left-full-column text-md-right text-center">
						<div class="full-column-inner mb-20 mb-md-0">
							<img class="img-Lazy" src="/Portals/_default/ContentBuilder/minis-page/about-us03/aboutUs03-img02.png" alt=""></div>
					</div>
					<div class="col-md-6 pl-lg-45 pt-5">
						<div class="title-09 mb-55">
							<small>OUR WORK SPEAKS FOR ITSELF</small>
							<h3>We Provide Highly Powerful and Useful <span class="color-accent">Website Functionality</span>
							</h3>
						</div>
						<div class="custom-module loading" id="module-{id}" data-moduleid="{id}" data-style="progressbar01" data-effect="progressbar" data-settings="%7B%22items%22%3A%7B%22item0%22%3A%7B%22title%22%3A%22JavaScript%22%2C%22progress%22%3A%2295%22%7D%2C%22item1%22%3A%7B%22title%22%3A%22JSON%22%2C%22progress%22%3A%2290%22%7D%2C%22item2%22%3A%7B%22title%22%3A%22C%23%22%2C%22progress%22%3A%2292%22%7D%2C%22item3%22%3A%7B%22title%22%3A%22Bootstrap%22%2C%22progress%22%3A%2285%22%7D%7D%7D"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<style class="build-css" data-class="title-09">${csstemplate["title-09"]}</style>
`
}, {
    'thumbnail': 'minis-section/page/section-31.jpg',
    'category': '816',
    'html': `<div class="is-section is-box is-align-center is-light-text layout-no-mb">
	<div class="is-overlay">
		<div class="is-overlay-bg" style="background-image:url(&quot;/Portals/_default/ContentBuilder/minis-page/about-us03/aboutUs03-bg02.jpg&quot;)"></div>
	</div>
	<div class="is-boxes">
		<div class="is-box-centered">
			<div class="is-container layout-container" dragwithouthandle="">
				<div class="row">
					<div class="col-md-12">
						<h2 style="font-size:36px;">We Love to Build Innovation Projects for Startups</h2>
						<div class="spacer height-20"></div>
						<p class="m-auto" style="max-width: 630px;">Nulla fringilla, dolor eget elementum tempus, orci libero dignissim neque, vitae viverra sapien libero non leo sed a neque risus lacinia</p>
						<div class="spacer height-60"></div>
						<a href="#" title="LEARN MORE" class="button-01 border-radius-3">GET STARTED NOW</a>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12" data-noedit="">
						<div class="spacer height-100 mb-15"></div>
					</div>
				</div>
				<div class="row aboutus03-client-line">
					<div class="col-md-3">
						<span>
							<a href="#" title="">
								<img src="/Portals/_default/ContentBuilder/minis-page/about-us03/aboutUs03-client01.png" class="img-Lazy" alt=""></a>
						</span>
					</div>
					<div class="col-md-3">
						<span>
							<a href="#" title="">
								<img src="/Portals/_default/ContentBuilder/minis-page/about-us03/aboutUs03-client02.png" class="img-Lazy" alt=""></a>
						</span>
					</div>
					<div class="col-md-3">
						<span>
							<a href="#" title="">
								<img src="/Portals/_default/ContentBuilder/minis-page/about-us03/aboutUs03-client03.png" class="img-Lazy" alt=""></a>
						</span>
					</div>
					<div class="col-md-3">
						<span>
							<a href="#" title="">
								<img src="/Portals/_default/ContentBuilder/minis-page/about-us03/aboutUs03-client04.png" class="img-Lazy" alt=""></a>
						</span>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<style class="build-css" data-class="button-01">${csstemplate["button-01"]}</style>
<style class="build-css" data-class="aboutus03-client-line">
		.aboutus03-client-line::before {
			content: "";
			border-bottom: 1px solid rgba(255, 255, 255, 0.3);
			position: absolute;
			left: -1000px;
			right: -1000px;
		}

		.aboutus03-client-line>div {
			border-right: 1px solid rgba(255, 255, 255, 0.3);
			text-align: center;
			padding: 28px;
		}

		.aboutus03-client-line>div:last-child {
			border-right: none;
		}

		.aboutus03-client-line a {
			opacity: 0.5;
			transition: opacity ease-in 300ms;
		}

		.aboutus03-client-line a:hover {
			opacity: 1;
		}
	</style>
`
}, {
    'thumbnail': 'minis-section/page/section-32.jpg',
    'category': '816',
    'html': `<div class="is-section is-box is-bg-light is-dark-text section-no-padding">
	<div class="is-overlay"></div>
	<div class="is-boxes">
		<div class="is-box-centered">
			<div class="is-container layout-container" dragwithouthandle="">
				<div class="row align-items-center">
					<div class="col-md-12">
						<div class="spacer height-120 mb-30 mb-md-40 mb-lg-0"></div>
					</div>
				</div>
				<div class="clear"></div>
				<div class="row align-items-center">
					<div class="col-md-5">
						<div class="spacer height-40 d-none d-md-block"></div>
						<div class="title-09">
							<small>WE ARE YOUR SOLID BACKING!</small>
							<h3>We Keep Up With The Most <span class="color-accent">Recent Trend.</span>
							</h3>
						</div>
						<p>Integer venenatis lacus facilisis augue tincidunt, id fermentum nibh lacinia. Donec ultrices dictum mauris eu suscipit donec lacus arcu dapibus nisl ut.</p>
						<div class="spacer height-40"></div>
						<p>
							<a href="#" title="LEARN MORE" class="button-01">LEARN MORE</a>
						</p>
						<div class="spacer height-40"></div>
					</div>
					<div class="col-md-7">
						<div class="row">
							<div class="col-md-6">
								<div class="iconbox-02 edit-box ">
									<div class="icon color-accent">
										<i class="sico lnr-hammer-wrench size-32"><svg>
												<use xlink:href="#lnr-hammer-wrench"></use>
											</svg></i>
									</div>
									<h6>Fully Customizable</h6>
									<p>Etiam eros dui, elementum eget ullamcorper eu, sollicitudin nec erat.</p>
								</div>
							</div>
							<div class="col-md-6">
								<div class="iconbox-02 edit-box">
									<div class="icon color-accent">
										<i class="sico lnr-woman size-32"><svg>
												<use xlink:href="#lnr-woman"></use>
											</svg></i>
									</div>
									<h6>Awesome Customer Service </h6>
									<p>Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae.</p>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="iconbox-02 edit-box ">
									<div class="icon color-accent">
										<i class="sico lnr-magic-wand size-32"><svg>
												<use xlink:href="#lnr-magic-wand"></use>
											</svg></i>
									</div>
									<h6>Creative Design</h6>
									<p>Etiam eros dui, elementum eget ullamcorper eu, sollicitudin nec erat.</p>
								</div>
							</div>
							<div class="col-md-6">
								<div class="iconbox-02 edit-box">
									<div class="icon color-accent">
										<i class="sico lnr-window size-32"><svg>
												<use xlink:href="#lnr-window"></use>
											</svg></i>
									</div>
									<h6>Humanized Interface </h6>
									<p>Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae.</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row align-items-center">
					<div class="col-md-12">
						<div class="spacer mb-lg-40 height-40"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<style class="build-css" data-class="title-09">${csstemplate["title-09"]}</style>
<style class="build-css" data-class="button-01">${csstemplate["button-01"]}</style>
<style class="build-css" data-class="iconbox-02">${csstemplate["iconbox-02"]}</style>
`
}, {
    'thumbnail': 'minis-section/page/section-33.jpg',
    'category': '816',
    'html': `<div class="is-section is-box">
	<div class="is-overlay">
		<div class="is-overlay-bg" style="background-image:url(&quot;/Portals/_default/ContentBuilder/minis-page/about-us04/aboutUs04-bg01.jpg&quot;)"></div>
	</div>
	<div class="is-boxes">
		<div class="is-box-centered">
			<div class="is-container layout-container" dragwithouthandle="">
				<div class="row">
					<div class="col-md-12" data-noedit="">
						<div class="spacer height-20"></div>
					</div>
				</div>
				<div class="row align-items-center">
					<div class="col-md-8 col-lg-6 aboutus04-section02-box">
						<div class="title-09">
							<small>PROFESSIONAL SKILLS</small>
							<h3>We Are An Experienced Team <span class="color-accent">Who Are Full Of Passion.</span>
							</h3>
						</div>
						<div class="custom-module loading" id="module-{id}" data-moduleid="{id}" data-style="progressbar02" data-effect="progressbar"  data-settings="%7B%22items%22%3A%7B%22item0%22%3A%7B%22title%22%3A%22Web%20Design%22%2C%22progress%22%3A%2292%22%2C%22color%22%3A%22%23527dd6%22%7D%2C%22item1%22%3A%7B%22title%22%3A%22PhotoShop%22%2C%22progress%22%3A%2288%22%2C%22color%22%3A%22%2320a3f0%22%7D%2C%22item2%22%3A%7B%22title%22%3A%22HTML5%20%26%20CSS3%22%2C%22progress%22%3A%2286%22%2C%22color%22%3A%22%235bc5db%22%7D%2C%22item3%22%3A%7B%22title%22%3A%22UI%22%2C%22progress%22%3A%2295%22%2C%22color%22%3A%22%231abc9c%22%7D%7D%7D"></div> 
					</div>
					<div class="col-md-4 col-lg-6 text-center">
						<a class="play-button is-lightbox" href="https://www.youtube.com/embed/AkcUoA2f-jU" data-ilightbox="youtube"
						 title="">
							<span>
								<i class="sico fas-play color-accent size-18"><svg>
										<use xlink:href="#fas-play"></use>
									</svg></i>
							</span>
						</a>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12" data-noedit="">
						<div class="spacer height-20"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<style class="build-css" data-class="title-09">${csstemplate["title-09"]}</style>
<style class="build-css" data-class="play-button">${csstemplate["play-button"]}</style> 
<style class="build-css" data-class="aboutus04-section02-box">
    .aboutus04-section02-box {
        background-color: rgba(255, 255, 255, 0.9);
        border-radius: 3px;
        padding: 60px 40px 70px;
    }
    @media only screen and (max-width: 767px) {
        .aboutus04-section02-box {
            margin: 30px 25px 35px;
            padding: 25px;
        }
    }
</style>
`
}, {
    'thumbnail': 'minis-section/page/section-34.jpg',
    'category': '816',
    'html': `<div class="is-section is-box layout-no-mb">
	<div class="is-boxes">
		<div class="is-box-centered">
			<div class="is-container layout-container" dragwithouthandle="">
				<div class="row align-items-center">
					<div class="col-md-5 pr-lg-45">
						<div class="spacer height-40 d-none d-md-block"></div>
						<div class="title-09 mt-10 mt-md-0">
							<small>OUR CLIENTS</small>
							<h3>Creative solutions to help <span class="color-accent">brand growing.</span>
							</h3>
						</div>
						<p>Integer venenatis lacus facilisis augue tincidunt, id fermentum nibh lacinia. Donec ultrices dictum mauris eu suscipit donec lacus arcu dapibus nisl ut.</p>
						<div class="spacer height-40"></div>
						<a href="#" title="CONTACT US" class="button-01">CONTACT US</a>
						<div class="spacer height-40"></div>
					</div>
					<div class="col-md-7">
                        ${getSnippetCode("clientlist05")}
					</div>
				</div>
				<div class="row">
					<div class="col-md-12" data-noedit="">
                        <div class="spacer height-100 mb-0 mb-md-5"></div>
                       
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<style class="build-css" data-class="title-09">${csstemplate["title-09"]}</style>
<style class="build-css" data-class="button-01">${csstemplate["button-01"]}</style>
`
}, {
    'thumbnail': 'minis-section/page/section-35.jpg',
    'category': '816',
    'html': `<div class="is-section is-box is-align-center is-dark-text">
	<div class="is-overlay">
		<div class="is-overlay-bg" style="background-image:url(&quot;/Portals/_default/ContentBuilder/minis-page/about-us04/aboutUs04-bg02.jpg&quot;)"></div>
	</div>
	<div class="is-boxes">
		<div class="is-box-centered">
			<div class="is-container layout-container" dragwithouthandle="">
				<div class="row">
					<div class="col-md-12">
						<i class="sico lnr-paper-plane size-42 color-accent"><svg>
								<use xlink:href="#lnr-paper-plane"></use>
							</svg></i>
						<h3>Start to Contact with Us</h3>
						<div class="spacer height-20"></div>
						<p class="size-18 m-auto" style="max-width: 620px;">Whether you just wanna contact us or you'd like to view more of our works, please check our social platforms.</p>
                        <div class="spacer height-20"></div>
                        ${getSnippetCode("social-11")}
					</div>
				</div>
			</div>
		</div>
	</div> 
</div>
`
}, {
    'thumbnail': 'minis-section/page/section-36.jpg',
    'category': '816',
    'html': `<div class="is-section is-box layout-no-mb layout-no-mt">
	<div class="is-boxes">
		<div class="is-box-centered">
			<div class="is-container layout-container" dragwithouthandle="">
				<div class="row align-items-start">
					<div class="col-md-12" data-noedit="">
						<div class="spacer height-100 mb-25 mb-lg-5"></div>
					</div>
				</div>
				<div class="row align-items-start">
					<div class="col-md-12">
						<div class="title-14 m-auto" style="max-width: 570px;">
							<h3>We Provide Our Customers with the Highest Quality Service</h3>
						</div>
						<div class="spacer height-60"></div>
					</div>
				</div>
				<div class="row align-items-start">
                    <div class="col-md-6 pr-md-40">
                    ${getSnippetCode("accordion06")}
 					</div>
					<div class="col-md-3">
						<div class="spacer height-20"></div>
						<div class="iconbox-25">
							<div class="icon">
								<i class="sico lnr-reading size-54"><svg>
										<use xlink:href="#lnr-reading"></use>
									</svg></i>
							</div>
							<h6 class="title">Getting Started</h6>
							<p>If you don't know where to start after purchasing the theme, then you can get some guides here to help you move forward more easily.</p>
							<p class="mb-0">
								<a title="START NOW" href="#" class="link"> START NOW</a>
							</p>
						</div>
						<div class="spacer height-20"></div>
					</div>
					<div class="col-md-3">
						<div class="spacer height-20"></div>
						<div class="iconbox-25 color-2">
							<div class="icon">
								<i class="sico lnr-library size-54"><svg>
										<use xlink:href="#lnr-library"></use>
									</svg></i>
							</div>
							<h6 class="title">Support Form</h6>
							<p>If you have anything to ask or would like to report an issue, please do so on our dedicated support forum.</p>
							<p class="mb-0">
								<a title="START NOW" href="#" class="link"> START NOW</a>
							</p>
						</div>
						<div class="spacer height-20"></div>
					</div>
				</div>
				<div class="row align-items-start">
					<div class="col-md-12" data-noedit="">
						<div class="spacer mt-10 height-80"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<style class="build-css" data-class="iconbox-25">${csstemplate["iconbox-25"]}</style>
<style class="build-css" data-class="title-14">${csstemplate["title-14"]}</style>
`
}, {
    'thumbnail': 'minis-section/page/section-37.jpg',
    'category': '816',
    'html': `<div class="is-section is-box layout-no-mb">
	<div class="is-overlay">
		<div class="is-overlay-bg" style="background-image:url(&quot;/Portals/_default/ContentBuilder/minis-page/about-us05/aboutUs05-bg01.jpg&quot;)"></div>
	</div>
	<div class="is-boxes">
		<div class="is-box-centered">
			<div class="is-container layout-container" dragwithouthandle="">
				<div class="row">
					<div class="col-md-12" data-noedit="">
						<div class="spacer height-20 d-none d-md-block"></div>
					</div>
				</div>
				<div class="row align-items-center">
					<div class="col-lg-5 pr-lg-50">
						<div class="title-19 mt-10 mt-md-0">
							<h3>
								<span class="color-accent" style="font-weight: 700;">We Are Professional </span>
								<span style="font-weight: 600;">DNN Theme Development Company</span>
							</h3>
						</div>
						<p>Fusce aliquet iaculis dolor, sed fermentum eros semper vitae. Vestibulum aliquet mollis tortor, sit amet interdum magna egestas non. Quisque eu aliquet ligula. Curabitur eget vestibulum quam. </p>
						<div class="spacer height-40"></div>
					</div>
					<div class="col-lg-7">
						<div class="row">
							<div class="col-md-6">
								<div class="iconbox-26 mt-lg-50">
									<div class="icon">
										<i class="sico lnr-cog"><svg>
												<use xlink:href="#lnr-cog"></use>
											</svg></i>
									</div>
									<h6 class="title">Quality assurance</h6>
									<p>Morbi erat lacus variusin congue quis vulputate in ante aenean.</p>
								</div>
								<div class="iconbox-26">
									<div class="icon">
										<i class="sico lnr-site-map"><svg>
												<use xlink:href="#lnr-site-map"></use>
											</svg></i>
									</div>
									<h6 class="title">Contract management</h6>
									<p>Suspendisse placerat ex pretium lectus pharetra convallis.</p>
								</div>
							</div>
							<div class="col-md-6">
								<div class="iconbox-26">
									<div class="icon">
										<i class="sico lnr-balance"><svg>
												<use xlink:href="#lnr-balance"></use>
											</svg></i>
									</div>
									<h6 class="title">Cost control</h6>
									<p>Nunc ex leo rhoncus at velit ac mattis euismod neque quisque.</p>
								</div>
								<div class="iconbox-26">
									<div class="icon">
										<i class="sico lnr-refund"><svg>
												<use xlink:href="#lnr-refund"></use>
											</svg></i>
									</div>
									<h6 class="title">Save time</h6>
									<p> Sed pretium dignissim erat, id sodales tellus feugiat at. </p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12" data-noedit="">
						<div class="spacer height-80 mb-10"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<style class="build-css" data-class="title-19">${csstemplate["title-19"]}</style>
<style class="build-css" data-class="iconbox-26">${csstemplate["iconbox-26"]}</style>
`
}, {
    'thumbnail': 'minis-section/page/section-38.jpg',
    'category': '816',
    'html': `<div class="is-section is-shadow-1 is-section-auto layout-no-mb layout-no-mt">
	<div class="is-boxes">
		<div class="is-light-text is-box is-align-center is-box-6">
			<div class="is-overlay">
				<div class="is-overlay-bg" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/about-us05/aboutUs05-bg02.jpg&quot;);"></div>
				<div class="is-overlay-color"></div>
				<div class="is-overlay-content"></div>
			</div>
			<div class="is-boxes">
				<div class="is-box-centered">
					<div class="is-container container-fluid" style="max-width: 620px;" dragwithouthandle="">
						<div class="row">
							<div class="col-md-12" data-noedit="">
								<div class="spacer height-40"></div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<i class="sico lnr-magic-wand size-60"><svg>
										<use xlink:href="#lnr-magic-wand"></use>
									</svg></i>
								<div class="spacer height-20"></div>
								<h2 style="font-weight: 600;">Break The Inherent Thinking </h2>
								<div class="spacer height-20"></div>
								<p>Praesent sodales in justo at mollis. Fusce ac tincidunt mi. Nunc quis eleifend lacus, a dapibus leo. In vestibulum arcu ut ex venenatis.</p>
								<div class="spacer height-20"></div>
								<a class="button-09" href="#" title="VIEW MORE">VIEW MORE</a>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12" data-noedit="">
								<div class="spacer height-40"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="is-light-text is-box is-box-6">
			<div class="is-overlay">
				<div class="is-overlay-bg" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/about-us05/aboutUs05-bg03.jpg&quot;);"></div>
				<div class="is-overlay-color"></div>
				<div class="is-overlay-content"></div>
			</div>
			<div class="is-boxes">
				<div class="is-box-centered">
					<div class="is-container container-fluid" style="max-width: 657px;" dragwithouthandle="">
						<div class="row">
							<div class="col-md-12" data-noedit="">
								<div class="spacer height-80"></div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12 text-center">
								<div class="title-19 border-accent">
									<h2>What We Provide</h2>
									<h5>Best DNN Products and Professional Services for You.</h5>
								</div>
								<p class="pt-10">Curabitur pretium sagittis dictum, vivamus a vulputate elit at aliquam massa. Sed vestibulum ante quis urna maximus rhoncus.</p>
								<div class="spacer height-20"></div>
                                ${getSnippetCode("counter-12")}
							</div>
						</div>
						<div class="row">
							<div class="col-md-12" data-noedit="">
								<div class="spacer height-60 d-none d-md-block"></div>
								<div class="mt-10 d-block d-md-none"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<style class="build-css" data-class="title-19">${csstemplate["title-19"]}</style>
<style class="build-css" data-class="button-09">${csstemplate["button-09"]}</style>
`
}, {
    'thumbnail': 'minis-section/page/section-39.jpg',
    'category': '816',
    'html': `<div class="is-section is-box is-align-center layout-no-mb">
	<div class="is-overlay">
		<div class="is-overlay-bg" style="background-image:url(&quot;/Portals/_default/ContentBuilder/minis-page/about-us05/aboutUs05-bg04.jpg&quot;)"></div>
	</div>
	<div class="is-boxes">
		<div class="is-box-centered">
			<div class="is-container layout-container" dragwithouthandle="">
				<div class="row">
					<div class="col-md-12">
						<div class="title-19">
							<h3>
								<span class="color-accent" style="font-weight: 700;">Our Outstanding </span>
								<span style="font-weight: 600;">Projects</span>
							</h3>
						</div>
						<p class="m-auto" style="max-width: 770px;">Nulla consequat commodo augue sit amet finibus. In justo sapien, elementum nec imperdiet a, blandit sed mauris. Donec fermentum mauris non quam. </p>
						<div class="spacer height-20 mb-10"></div>
						<div class="custom-module loading" id="module-{id}" data-moduleid="{id}" data-effect="carousel" data-style="carousel08" data-settings="%7B%22size%22%3A%22large%22%2C%22items%22%3A%7B%22item0%22%3A%7B%22title%22%3A%22Still%20Life%20Business%20Card%22%2C%22subtitle%22%3A%22Business%22%2C%22single%22%3A%22%2FPortals%2F_default%2FContentBuilder%2Fminis-page%2Fabout-us05%2FaboutUs05-img01.jpg%22%2C%22singlewidth%22%3A%22370%22%2C%22singleheight%22%3A%22352%22%2C%22linktype%22%3A%22linkurl%22%2C%22link%22%3A%22%23%22%7D%2C%22item1%22%3A%7B%22title%22%3A%22Coffee%20Owl%22%2C%22subtitle%22%3A%22Beverage%22%2C%22single%22%3A%22%2FPortals%2F_default%2FContentBuilder%2Fminis-page%2Fabout-us05%2FaboutUs05-img02.jpg%22%2C%22singlewidth%22%3A%22370%22%2C%22singleheight%22%3A%22352%22%2C%22linktype%22%3A%22linkurl%22%2C%22link%22%3A%22%23%22%7D%2C%22item2%22%3A%7B%22title%22%3A%22Wood%20Frame%20Mockup%22%2C%22subtitle%22%3A%22Product%22%2C%22single%22%3A%22%2FPortals%2F_default%2FContentBuilder%2Fminis-page%2Fabout-us05%2FaboutUs05-img03.jpg%22%2C%22singlewidth%22%3A%22370%22%2C%22singleheight%22%3A%22352%22%2C%22linktype%22%3A%22linkurl%22%2C%22link%22%3A%22%23%22%7D%7D%2C%22accopen%22%3A%22checked%22%2C%22accopenindex%22%3A%221%22%2C%22%22%3A%22linkurl%22%2C%22link%22%3A%22%23%22%2C%22displayxxl%22%3A%223%22%2C%22displayxl%22%3A%223%22%2C%22displayl%22%3A%223%22%2C%22displaym%22%3A%222%22%2C%22displays%22%3A%222%22%2C%22displayxs%22%3A%221%22%2C%22navigation%22%3A%22checked%22%2C%22lazy%22%3A%22checked%22%2C%22delay%22%3A%225000%22%2C%22pagination%22%3A%22checked%22%7D"></div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12" data-noedit="">
						<div class="spacer height-80"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<style class="build-css" data-class="title-19">${csstemplate["title-19"]}</style>

`
}, {
    'thumbnail': 'minis-section/page/section-40.jpg',
    'category': '816',
    'html': `<div class="is-section is-box is-bg-light is-light-text layout-no-mt layout-no-mb is-align-center">
	<div class="is-boxes">
		<div class="is-box-centered">
			<div class="is-container layout-container" dragwithouthandle="">
				<div class="row aboutUs05-section04">
					<div class="col-md-4" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/about-us05/aboutUs05-section04-bg01.png&quot;); background-repeat: no-repeat; background-size: auto; background-color: rgb(41, 204, 172);">
						<div class="icon">
							<i class="icon-svg icon-basic-headset size-42 animated"><svg version="1.1" xmlns="http://www.w3.org/2000/svg"
								 xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="64px" height="64px" viewBox="0 0 64 64"
								 enable-background="new 0 0 64 64" xml:space="preserve" id="icon-svg-t1dhy5basu">
									<path xmlns="http://www.w3.org/2000/svg" d="M11,48C5.477,48,1,43.523,1,38s4.477-10,10-10h2v20             H11z"
									 style="stroke-dasharray: 56, 58; stroke-dashoffset: 0;"></path>
									<path xmlns="http://www.w3.org/2000/svg" d="M53,28c5.523,0,10,4.477,10,10s-4.477,10-10,10h-2             V28H53z"
									 style="stroke-dasharray: 56, 58; stroke-dashoffset: 0;"></path>
									<path xmlns="http://www.w3.org/2000/svg" d="M13,31v-9c0,0,0-16,19-16s19,16,19,16v6" style="stroke-dasharray: 71, 73; stroke-dashoffset: 0;"></path>
									<path xmlns="http://www.w3.org/2000/svg" d="M51,48L51,53L36,59L28,59L28,55L36,55L36,58" style="stroke-dasharray: 45, 47; stroke-dashoffset: 0;"></path>
								</svg></i>
						</div>
						<p data-keep-background-color="" data-keep-background="">(+086)-2566-8799 </p>
					</div>
					<div class="col-md-4" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/about-us05/aboutUs05-section04-bg02.png&quot;); background-repeat: no-repeat; background-size: auto; background-color: rgb(22, 179, 144);">
						<div class="icon">
							<i class="icon-svg icon-basic-folder-multiple size-42 animated"><svg version="1.1" xmlns="http://www.w3.org/2000/svg"
								 xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="64px" height="64px" viewBox="0 0 64 64"
								 enable-background="new 0 0 64 64" xml:space="preserve" id="icon-svg-zm6737xf04">
									<path xmlns="http://www.w3.org/2000/svg" d="M56,22L56,54L1,54L1,15L19.629,15L26.726,22Z" style="stroke-dasharray: 184, 186; stroke-dashoffset: 0;"></path>
									<path xmlns="http://www.w3.org/2000/svg" d="M8,13L8,7L26.629,7L33.726,14L63,14L63,46L58,46" style="stroke-dasharray: 101, 103; stroke-dashoffset: 0;"></path>
								</svg></i>
						</div>
						<p>dnnskindev@gmail.com</p>
					</div>
					<div class="col-md-4" style="background-color: rgb(14, 155, 115); background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/about-us05/aboutUs05-section04-bg03.png&quot;); background-repeat: no-repeat; background-size: auto;">
						<div class="icon">
							<i class="icon-svg icon-basic-geolocalize-05 size-42 animated"><svg version="1.1" xmlns="http://www.w3.org/2000/svg"
								 xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="64px" height="64px" viewBox="0 0 64 64"
								 enable-background="new 0 0 64 64" xml:space="preserve" id="icon-svg-i0yoqxt3a09">
									<g xmlns="http://www.w3.org/2000/svg">
										<path d="M28,19.001A4,4 0,1,1 36,19.001A4,4 0,1,1 28,19.001" style="stroke-dasharray: 26, 28; stroke-dashoffset: 0;"></path>
										<path d="M45,21.023C44.968,13.276,39.181,7,32,7                 s-13,6.276-13,14.023C19,31.046,31.979,47,31.979,47S45.043,31.046,45,21.023z"
										 style="stroke-dasharray: 102, 104; stroke-dashoffset: 0;"></path>
									</g>
									<path xmlns="http://www.w3.org/2000/svg" d="M21,37L12,37L1,57L32,57L63,57L52,37L43,37" style="stroke-dasharray: 126, 128; stroke-dashoffset: 0;"></path>
								</svg></i>
						</div>
						<p>Jiangan District, Wuhan, Hubei</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<style data-class="aboutUs05-section04" class="build-css">
		.aboutUs05-section04 {
			margin-bottom: -80px;
			z-index: 10;
			position: relative;
		}

		.aboutUs05-section04>div {
			padding: 42px 20px;
		}

		.aboutUs05-section04 p:last-child {
			margin: 0;
		}

		@media all and (max-width: 768px) {
			.aboutUs05-section04 {
				margin: 0 -20px 0px;
			}
		}
	</style>
</div>
<div class="is-section is-box is-light-text is-align-center">
	<div class="is-overlay">
		<div class="is-overlay-bg" style="background-image:url(&quot;/Portals/_default/ContentBuilder/minis-page/about-us05/aboutUs05-bg05.jpg&quot;)"></div>
	</div>
	<div class="is-boxes">
		<div class="is-box-centered">
			<div class="is-container layout-container" style="max-width: 800px;" dragwithouthandle="">
				<div class="row">
					<div class="col-md-12">
						<div class="spacer height-80 mb-10 d-none d-md-block"></div>
						<div class="mb-10 d-block d-md-none"></div>
						<h3 class="m-auto" style="max-width: 620px;">
							<span class="color-accent">Want To Ask A Question </span>Or Just Say Hi? You Can Reach Us Via Email.</h3>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12" data-noedit="">
						<div class="spacer height-60"></div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
                        ${getSnippetCode("ajaxform-01")}
					</div>
				</div>
				<div class="row">
					<div class="col-md-12" data-noedit="">
						<div class="spacer height-20"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
`
}, {
    'thumbnail': 'minis-section/page/section-41.jpg',
    'category': '816',
    'html': `<div class="is-section is-box layout-no-mb is-align-center">
	<div class="is-overlay">
		<div class="is-overlay-bg" style="background-image:url(&quot;/Portals/_default/ContentBuilder/minis-page/about-us05/aboutUs05-bg06.jpg&quot;)"></div>
	</div>
	<div class="is-boxes">
		<div class="is-box-centered">
			<div class="is-container layout-container" dragwithouthandle="">
				<div class="row">
					<div class="col-md-12">
						<div class="title-19 text-center mt-10">
							<h3>
								<span class="color-accent">We Have </span>The Best Team </h3>
						</div>
						<p class="m-auto" style="max-width: 770px;">Nulla consequat commodo augue sit amet finibus. In justo sapien, elementum nec imperdiet a, blandit sed mauris. Donec fermentum mauris non quam. </p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="spacer height-60"></div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<img src="/Portals/_default/ContentBuilder/minis-page/about-us05/aboutUs05-img04.png" alt="" class="img-Lazy"></div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="is-section is-box is-align-center is-light-text layout-no-mt layout-no-mb" style="background-color: rgb(26, 188, 156);">
	<div class="is-boxes">
		<div class="is-box-centered">
			<div class="is-container layout-container" dragwithouthandle="">
				<div class="row">
					<div class="col-md-12" data-noedit="">
						<div class="spacer height-40"></div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<span class="size-32 mr-md-40 d-block d-md-inline-block">Want to learn more about our team</span>
						<a class="button-09 mt-20 mt-md-0 mb-10" href="#" title="VIEW MORE">VIEW MORE</a>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12" data-noedit="">
						<div class="spacer height-40"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<style class="build-css" data-class="title-19">${csstemplate["title-19"]}</style>
<style class="build-css" data-class="button-09">${csstemplate["button-09"]}</style>
`
}, {
    'thumbnail': 'minis-section/page/section-42.jpg',
    'category': '816',
    'html': `<div class="is-section is-box is-align-center layout-no-mb">
	<div class="is-boxes">
		<div class="is-box-centered">
			<div class="is-container layout-container" dragwithouthandle="">
				<div class="row">
					<div class="col-md-12">
						<div class="title-14 mt-10">
							<h3>Flexible Digital Services</h3>
						</div>
						<p class="m-auto" style="max-width: 700px;">Cras malesuada at enim et faucibus pellentesque egestas dui non ornare fringilla. Morbi suscipit porttitor augue, a dignissim odio pretium eu.</p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12" data-noedit="">
						<div class="spacer height-60 mb-5"></div>
					</div>
                </div>
                ${getSnippetCode("flipbox-03")}
				<div class="row">
					<div class="col-md-12">
						<div class="spacer height-80 mb-10"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<style class="build-css" data-class="title-14">${csstemplate["title-14"]}</style>
`
}, {
    'thumbnail': 'minis-section/page/section-43.jpg',
    'category': '816',
    'html': `<div class="is-section is-box" style="background-color: rgb(249, 249, 249);">
	<div class="is-boxes">
		<div class="is-box-centered">
			<div class="is-container layout-container" dragwithouthandle="">
				<div class="row">
					<div class="col-md-12" data-noedit="">
						<div class="spacer height-20"></div>
					</div>
				</div>
				<div class="row align-items-center">
					<div class="col-md-6 text-center pr-lg-40">
						<img class="img-Lazy" src="/Portals/_default/ContentBuilder/minis-page/our-service02/ourservice02-img01.jpg" alt="Images">
						<span>
							<a class="play-button is-lightbox" href="https://www.youtube.com/embed/AkcUoA2f-jU" data-ilightbox="youtube"
							 title="" style="position: absolute; top: 50%; left: 50%; margin-top: -36px; margin-left: -36px;">
								<span>
									<i class="sico fas-play color-accent size-18"><svg>
											<use xlink:href="#fas-play"></use>
										</svg></i>
								</span>
							</a>
						</span>
					</div>
					<div class="col-md-6">
						<div class="spacer height-40"></div>
						<div class="title-11 mb-20">
							<h3 style="max-width:500px;">Our <span class="color-accent">Skills &amp; Expertise</span>
							</h3>
						</div>
						<p>Fusce lacus ligula interdum non orci sit amet tincidunt volutpat magna. Praesent quis convallis nulla, integer nec sem sed.</p>
						<div class="spacer height-20"></div>
						<div class="custom-module loading" id="module-{id}" data-moduleid="{id}" data-style="progressbar01" data-effect="progressbar" data-settings="%7B%22items%22%3A%7B%22item0%22%3A%7B%22title%22%3A%22Web%20Design%22%2C%22progress%22%3A%2292%22%7D%2C%22item1%22%3A%7B%22title%22%3A%22PhotoShop%22%2C%22progress%22%3A%2286%22%7D%2C%22item2%22%3A%7B%22title%22%3A%22HTML5%20%26%20CSS3%22%2C%22progress%22%3A%2295%22%7D%2C%22item3%22%3A%7B%22title%22%3A%22JavaScript%22%2C%22progress%22%3A%2290%22%7D%7D%2C%22stripes%22%3A%22checked%22%7D"></div>
						<div class="spacer height-40 d-none d-md-block"></div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12" data-noedit="">
						<div class="spacer height-20"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<style class="build-css" data-class="play-button">${csstemplate["play-button"]}</style>
<style class="build-css" data-class="title-11">${csstemplate["title-11"]}</style>

`
}, {
    'thumbnail': 'minis-section/page/section-44.jpg',
    'category': '816',
    'html': `<div class="is-section is-box layout-no-mb layout-no-mt">
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container" dragwithouthandle="">
                <div class="row">
                    <div class="col-md-12" data-noedit="">
                        <div class="spacer height-100 mb-30 mb-md-10"></div>
                    </div>
                </div>
                <div class="row align-items-center">
                    <div class="col-md-6">
                        <div class="title-11">
                            <h3 style="max-width:500px;">Diverse <span class="color-accent">Features</span>
                            </h3>
                        </div>
                        <p>Phasellus semper at justo eget interdum. Quisque mollis, libero non feugiat hendrerit, nibh ante imperdiet velit.</p>
                        <div class="spacer height-20 mb-10"></div>
                        <div class="row">
                            <div class="col-md-6 edit-box">
                                <div class="iconbox-05">
                                    <div class="icon color-accent">
                                        <i class="icon-svg icon-basic-pencil-ruler animated"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="64px" height="64px" viewBox="0 0 64 64" enable-background="new 0 0 64 64" xml:space="preserve" id="icon-svg-fxltu0aavc">
                                                <path xmlns="http://www.w3.org/2000/svg" d="M1,63L1,2L62,63Z" style="stroke-dasharray: 209, 211; stroke-dashoffset: 0;"></path>
                                                <path xmlns="http://www.w3.org/2000/svg" d="M14,54L14,30L38,54Z" style="stroke-dasharray: 82, 84; stroke-dashoffset: 0;"></path>
                                                <path xmlns="http://www.w3.org/2000/svg" d="M8,22L2,22" style="stroke-dasharray: 6, 8; stroke-dashoffset: 0;"></path>
                                                <path xmlns="http://www.w3.org/2000/svg" d="M6,30L2,30" style="stroke-dasharray: 4, 6; stroke-dashoffset: 0;"></path>
                                                <path xmlns="http://www.w3.org/2000/svg" d="M8,38L2,38" style="stroke-dasharray: 6, 8; stroke-dashoffset: 0;"></path>
                                                <path xmlns="http://www.w3.org/2000/svg" d="M6,46L2,46" style="stroke-dasharray: 4, 6; stroke-dashoffset: 0;"></path>
                                                <path xmlns="http://www.w3.org/2000/svg" d="M8,54L2,54" style="stroke-dasharray: 6, 8; stroke-dashoffset: 0;"></path>
                                                <path xmlns="http://www.w3.org/2000/svg" d="M25,1L19,7L55,43L63,45L61,37Z" style="stroke-dasharray: 127, 129; stroke-dashoffset: 0;"></path>
                                                <path xmlns="http://www.w3.org/2000/svg" d="M25,13L31,7" style="stroke-dasharray: 9, 11; stroke-dashoffset: 0;"></path></svg></i>
                                    </div>
                                    <h6 class="mb-15">Designed with Care</h6>
                                    <p>Vivamus velit purus, euismod vitae elementum id, porta in nisl.</p>
                                </div>
                                <div class="iconbox-05">
                                    <div class="icon color-accent">
                                        <i class="icon-svg icon-software-layers2 animated"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="64px" height="64px" viewBox="0 0 64 64" enable-background="new 0 0 64 64" xml:space="preserve" id="icon-svg-hczz42x6u86">
                                                <path xmlns="http://www.w3.org/2000/svg" d="M18,26.066L2,33L32,45L62,33L46,26.067" style="stroke-dasharray: 100, 102; stroke-dashoffset: 0;"></path>
                                                <path xmlns="http://www.w3.org/2000/svg" d="M18,39.066L2,46L32,58L62,46L46,39.067" style="stroke-dasharray: 100, 102; stroke-dashoffset: 0;"></path>
                                                <path xmlns="http://www.w3.org/2000/svg" d="M32,32L62,20L32,7L2,20Z" style="stroke-dasharray: 131, 133; stroke-dashoffset: 0;"></path></svg></i>
                                    </div>
                                    <h6 class="mb-15">Cleverly Constructed</h6>
                                    <p>Vestibulum leo erat, elementum et ullamcorper accumsan.</p>
                                </div>
                            </div>
                            <div class="col-md-6 edit-box">
                                <div class="iconbox-05">
                                    <div class="icon color-accent">
                                        <i class="icon-svg icon-software-reflect-horizontal animated"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="64px" height="64px" viewBox="0 0 64 64" enable-background="new 0 0 64 64" xml:space="preserve" id="icon-svg-g154t3h7jw">
                                                <path xmlns="http://www.w3.org/2000/svg" d="M1,17L1,57L21,37Z" style="stroke-dasharray: 97, 99; stroke-dashoffset: 0;"></path>
                                                <g xmlns="http://www.w3.org/2000/svg">
                                                    <g>
                                                        <path d="M61.586,55.586L63,57L63,55" style="stroke-dasharray: 4, 6; stroke-dashoffset: 0;"></path>
                                                        <path stroke-dasharray="4,2" d="M63,53L63,20" style="stroke-dasharray: 33, 35; stroke-dashoffset: 0;"></path>
                                                        <path d="M63,19L63,17L61.586,18.414" style="stroke-dasharray: 4, 6; stroke-dashoffset: 0;"></path>
                                                        <path stroke-dasharray="3.736,1.868" d="M60.265,19.735L45.075,34.925" style="stroke-dasharray: 22, 24; stroke-dashoffset: 0;"></path>
                                                        <path d="M44.414,35.586L43,37L44.414,38.414" style="stroke-dasharray: 4, 6; stroke-dashoffset: 0;"></path>
                                                        <path stroke-dasharray="3.736,1.868" d="M45.735,39.735L60.925,54.925" style="stroke-dasharray: 22, 24; stroke-dashoffset: 0;"></path>
                                                    </g>
                                                </g>
                                                <g xmlns="http://www.w3.org/2000/svg">
                                                    <g>
                                                        <path d="M32,61L32,59" style="stroke-dasharray: 2, 4; stroke-dashoffset: 0;"></path>
                                                        <path stroke-dasharray="4,2" d="M32,57L32,16" style="stroke-dasharray: 41, 43; stroke-dashoffset: 0;"></path>
                                                        <path d="M32,15L32,13" style="stroke-dasharray: 2, 4; stroke-dashoffset: 0;"></path>
                                                    </g>
                                                </g>
                                                <path xmlns="http://www.w3.org/2000/svg" stroke-linejoin="bevel" d="M46,13L57,13L57,2" style="stroke-dasharray: 22, 24; stroke-dashoffset: 0;"></path>
                                                <path xmlns="http://www.w3.org/2000/svg" d="M57,13C51.378,5.132,42.408,1,32,1             C21.591,1,12.622,5.13,7,13" style="stroke-dasharray: 58, 60; stroke-dashoffset: 0;"></path></svg></i>
                                    </div>
                                    <h6 class="mb-15">Visual Builder</h6>
                                    <p>Mauris leo mauris, auctor in est sit amet, porttitor ullamcorper.</p>
                                </div>
                                <div class="iconbox-05">
                                    <div class="icon color-accent">
                                        <i class="icon-svg icon-basic-geolocalize-05 animated"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="64px" height="64px" viewBox="0 0 64 64" enable-background="new 0 0 64 64" xml:space="preserve" id="icon-svg-mfcheygw8as">
                                                <g xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M28,19.001A4,4 0,1,1 36,19.001A4,4 0,1,1 28,19.001" style="stroke-dasharray: 26, 28; stroke-dashoffset: 0;"></path>
                                                    <path d="M45,21.023C44.968,13.276,39.181,7,32,7                 s-13,6.276-13,14.023C19,31.046,31.979,47,31.979,47S45.043,31.046,45,21.023z" style="stroke-dasharray: 102, 104; stroke-dashoffset: 0;"></path>
                                                </g>
                                                <path xmlns="http://www.w3.org/2000/svg" d="M21,37L12,37L1,57L32,57L63,57L52,37L43,37" style="stroke-dasharray: 126, 128; stroke-dashoffset: 0;"></path></svg></i>
                                    </div>
                                    <h6 class="mb-15">Location Targeting</h6>
                                    <p>Nunc accumsan, nunc nec sagittis lacinia, massa felis.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 text-center pl-lg-40">
                        <img class="img-Lazy" src="/Portals/_default/ContentBuilder/minis-page/our-service02/ourservice02-img02.jpg" alt="Images">
                        <span>
                            <a class="play-button is-lightbox" href="https://www.youtube.com/embed/AkcUoA2f-jU" data-ilightbox="youtube" title="" style="position: absolute; top: 50%; left: 50%; margin-top: -36px; margin-left: -36px;">
                                <span>
                                    <i class="sico fas-play color-accent size-18"><svg><use xlink:href="#fas-play"></use></svg></i>
                                </span>
                            </a>
                        </span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12" data-noedit="">
                        <div class="spacer height-100 mb-40 mb-md-10"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style class="build-css" data-class="play-button">${csstemplate["play-button"]}</style>
<style class="build-css" data-class="title-11">${csstemplate["title-11"]}</style>
<style class="build-css" data-class="iconbox-05">${csstemplate["iconbox-05"]}</style>
`
}, {
    'thumbnail': 'minis-section/page/section-45.jpg',
    'category': '816',
    'html': `<div class="is-section is-box is-light-text">
	<div class="is-overlay">
		<div class="is-overlay-bg" style="background-image:url(&quot;/Portals/_default/ContentBuilder/minis-page/our-service02/ourservice02-bg01.jpg&quot;)"></div>
	</div>
	<div class="is-boxes">
		<div class="is-box-centered">
			<div class="is-container layout-container" dragwithouthandle="">
            ${getSnippetCode("counter-13")}
			</div>
		</div>
	</div>
</div>
`
}, {
    'thumbnail': 'minis-section/page/section-46.jpg',
    'category': '816',
    'html': `<div class="is-section is-box is-align-center layout-no-mb">
	<div class="is-boxes">
		<div class="is-box-centered">
			<div class="is-container layout-container" dragwithouthandle="">
				<div class="row">
					<div class="col-md-12">
						<div class="title-14 mt-10">
							<h3>
								<span class="color-accent">Pricing Plans</span> And Options</h3>
						</div>
						<p class="m-auto" style="max-width: 700px;">Suspendisse feugiat elementum felis a auctor. Duis sed quam sit amet nulla dictum pharetra. Maecenas pretium dolor purus, at mattis ligula rhoncus in. </p>
						<div class="spacer height-60 mb-10 mb-lg-0"></div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<div class="price-05">
							<div class="price-item">
								<div class="price-header">
									<div class="price-box">
										<div class="pricing"><sup>$</sup>
											<span>105</span>
										</div>
										<div class="unit">
											<span>Per Month</span>
										</div>
									</div>
								</div>
								<div class="price-content">
									<h4 class="price-title">Standard Plan</h4>
									<div class="price-features">
										<ul>
											<li>Full Time Support</li>
											<li>Unlimited Data Transfer</li>
											<li>50GB Space</li>
											<li style="text-decoration: line-through;">Multiple Header Styles</li>
											<li style="text-decoration: line-through;">Free Mockup</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="price-05">
							<div class="price-item">
								<div class="price-header">
									<div class="price-box">
										<div class="pricing"><sup>$</sup>
											<span>150</span>
										</div>
										<div class="unit">
											<span>Per Month</span>
										</div>
									</div>
								</div>
								<div class="price-content">
									<h4 class="price-title">Basic Plan</h4>
									<div class="price-features">
										<ul>
											<li>Full Time Support</li>
											<li>Unlimited Data Transfer</li>
											<li>100GB Space</li>
											<li>Multiple Header Styles</li>
											<li style="text-decoration: line-through;">Free Mockup</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="price-05">
							<div class="price-item">
								<div class="price-header">
									<div class="price-box">
										<div class="pricing"><sup>$</sup>
											<span>200</span>
										</div>
										<div class="unit">
											<span>Per Month</span>
										</div>
									</div>
								</div>
								<div class="price-content">
									<h4 class="price-title">Maximum Plan</h4>
									<div class="price-features">
										<ul>
											<li>Full Time Support</li>
											<li>Unlimited Data Transfer</li>
											<li>200GB Space</li>
											<li>Multiple Heade rStyles</li>
											<li>Free Mockup</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12" data-noedit="">
						<div class="spacer height-80"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<style class="build-css" data-class="title-14">${csstemplate["title-14"]}</style>
<style class="build-css" data-class="price-05">${csstemplate["price-05"]}</style>
`
}, {
    'thumbnail': 'minis-section/page/section-47.jpg',
    'category': '816',
    'html': `<div class="is-section is-box layout-no-mb is-align-center" style="background-color: rgb(249, 249, 249);">
	<div class="is-overlay">
		<div class="is-overlay-bg" style="background-image:url(&quot;/Portals/_default/ContentBuilder/minis-page/our-service02/ourservice02-bg02.png&quot;)"></div>
	</div>
	<div class="is-boxes">
		<div class="is-box-centered">
			<div class="is-container layout-container" dragwithouthandle="">
				<div class="row">
					<div class="col-md-12">
						<div class="title-08 ml-auto mr-auto text-center" style="max-width: 770px; margin-bottom:40px;">
							<small class="color-accent">MATURE TECHNICAL TEAM</small>
							<h3>We Are Committed To Providing Our Customers <span class="color-accent">With The Highest Quality Service</span>
							</h3>
						</div>
						<p class="m-auto" style="max-width: 800px;">Sed auctor porta nulla a ultrices. Nunc vestibulum quis dolor a laoreet. Praesent porttitor ex sed sollicitudin tempus. Phasellus blandit placerat aliquam. Proin vel neque urna. Aenean cursus ac tortor vel tincidunt. Integer eu tempor metus, id ultricies elit.</p>
						<div class="spacer height-60"></div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<img class="img-Lazy" src="/Portals/_default/ContentBuilder/minis-page/our-service02/ourservice02-img03.png" alt="Images"></div>
				</div>
			</div>
		</div>
	</div>
</div>
<style class="build-css" data-class="title-08">${csstemplate["title-08"]}</style>

`
}, {
    'thumbnail': 'minis-section/page/section-48.jpg',
    'category': '816',
    'html': `<div class="is-section is-box layout-no-mt layout-no-mb">
	<div class="is-boxes">
		<div class="is-box-centered">
			<div class="is-container layout-container" dragwithouthandle="">
				<div class="row">
					<div class="col-md-12" data-noedit="">
						<div class="spacer height-120 mb-30 mb-lg-0"></div>
					</div>
				</div>
				<div class="row align-items-center">
					<div class="col-md-6 col-lg-4 pr-30">
						<div class="iconbox-27">
							<div class="icon">
								<i class="sico lnr-hammer-wrench"><svg>
										<use xlink:href="#lnr-hammer-wrench"></use>
									</svg></i>
							</div>
							<div class="cont edit-box">
								<h6 class="mb-15">Powerful Support</h6>
								<p>Suspendisse rhoncus sapien fermentum ipsum blandit facilisis venenatis vel eros.</p>
							</div>
						</div>
						<div class="spacer height-40"></div>
						<div class="iconbox-27">
							<div class="icon">
								<i class="sico lnr-signal"><svg>
										<use xlink:href="#lnr-signal"></use>
									</svg></i>
							</div>
							<div class="cont edit-box">
								<h6 class="mb-15">Premium Efficiency </h6>
								<p>Sed consequat venenatis neque in tempus. Praesent egestas aliquet erat.</p>
							</div>
						</div>
						<div class="spacer height-40 d-lg-none"></div>
					</div>
					<div class="col-md-6 col-lg-4 pr-lg-30">
						<div class="iconbox-27">
							<div class="icon">
								<i class="sico lnr-bubbles"><svg>
										<use xlink:href="#lnr-bubbles"></use>
									</svg></i>
							</div>
							<div class="cont edit-box">
								<h6 class="mb-15">Prompt Reply</h6>
								<p>Aenean blandit nulla vitae nisi auctor condimentum. Quisque ut tempor elit. </p>
							</div>
						</div>
						<div class="spacer height-40"></div>
						<div class="iconbox-27">
							<div class="icon">
								<i class="sico lnr-clipboard-check"><svg>
										<use xlink:href="#lnr-clipboard-check"></use>
									</svg></i>
							</div>
							<div class="cont edit-box">
								<h6 class="mb-15">Professional Solution</h6>
								<p>Aenean eu dui volutpat, placerat augue ac, imperdiet nisl. In feugiat mi in augue.</p>
							</div>
						</div>
						<div class="spacer height-40 d-lg-none"></div>
					</div>
					<div class="col-md-12 col-lg-4">
						<div class="OurService03-section01">
							<div class="title-08 mb-35">
								<small style="color: rgb(51, 51, 51);">RICH EXPERIENCE</small>
								<h4>Provide Smart Digital Services</h4>
							</div>
							<p>Vivamus iaculis ex eros, ut placerat justo matt donec molestie sit amet purus in semper quisque bibendum arcu ante, vel auctor risus pharetra ut vitae dolor.</p>
							<p class="mb-0 pt-10">
								<a href="#" title="VIEW MORE" class="link">
									<i class="icon-svg icon-arrows-right-double animated"><svg version="1.1" xmlns="http://www.w3.org/2000/svg"
										 xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="64px" height="64px" viewBox="0 0 64 64"
										 enable-background="new 0 0 64 64" xml:space="preserve" id="icon-svg-lw7rjf1gyvn">
											<g xmlns="http://www.w3.org/2000/svg">
												<path stroke-linejoin="bevel" d="M31,15L48,32L31,49" style="stroke-dasharray: 49, 51; stroke-dashoffset: 0;"></path>
											</g>
											<g xmlns="http://www.w3.org/2000/svg">
												<path stroke-linejoin="bevel" d="M16,15L33,32L16,49" style="stroke-dasharray: 49, 51; stroke-dashoffset: 0;"></path>
											</g>
										</svg></i>Read More</a>
							</p>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="spacer height-120 mb-40 mb-lg-0"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<style class="build-css" data-class="iconbox-27">${csstemplate["iconbox-27"]}</style>
<style class="build-css" data-class="title-08">${csstemplate["title-08"]}</style>
<style class="build-css" data-class="OurService03-section01">
.OurService03-section01 {
    border: 6px solid var(--accent-color);
    padding: 40px 48px;
}

.OurService03-section01 .link i {
    vertical-align: middle;
    margin-bottom: 5px;
    margin-right: 5px;
}

.OurService03-section01 .link path {
    stroke-width: 4;
}

@media only screen and (max-width:767px) {
    .OurService03-section01 {
        padding: 25px;
    }
}
</style>

`
}, {
    'thumbnail': 'minis-section/page/section-49.jpg',
    'category': '816',
    'html': `<div class="is-section is-box is-light-text">
	<div class="is-overlay">
		<div class="is-overlay-bg" style="background-image:url(&quot;/Portals/_default/ContentBuilder/minis-page/our-service03/ourservice03-bg01.jpg&quot;)"></div>
	</div>
	<div class="is-boxes">
		<div class="is-box-centered">
			<div class="is-container layout-container" dragwithouthandle="">
				<div class="row">
					<div class="col-md-12" data-noedit="">
						<div class="spacer height-40 d-none d-lg-block"></div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="counter-14">
							<div class="custom-module d-custom-active loading" id="module-YMoR2o0" data-moduleid="YMoR2o0" data-effect="Number"
							 data-html="false" data-noedit="true" data-module-desc="Number" data-module="shortcode" data-settings="%7B%22number%22%3A%22100000%22%2C%22separator%22%3A%22checked%22%7D"></div>
							<h6 class="title">HAPPY CUSTOMERS</h6>
						</div>
						<div class="spacer height-20 mb-10"></div>
						<p class="m-auto" style="font-size: 36px; text-align: center; max-width: 850px; line-height: 1.4;">Make Your Site Have Better Ranking and Draw More Attention from Visitors</p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12" data-noedit="">
						<div class="spacer height-40 mb-10 d-none d-lg-block"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div> 
<style class="build-css" data-class="counter-14">${csstemplate["counter-14"]}</style>
`
}, {
    'thumbnail': 'minis-section/page/section-50.jpg',
    'category': '816',
    'html': `<div class="is-section is-box layout-no-mb">
	<div class="is-boxes">
		<div class="is-box-centered">
			<div class="is-container layout-container" dragwithouthandle="">
				<div class="row">
					<div class="col-md-12">
						<div class="title-08 ml-auto mr-auto mt-10 text-center" style="max-width: 650px;">
							<small class="color-accent">PROVIDE A REASONABLE PROCESS</small>
							<h3>Create Your Own Awesome Website Very <span class="color-accent">Quickly And Easily</span>
							</h3>
						</div>
						<div class="spacer height-20"></div>
					</div>
				</div>
				<div class="row step-04">
					<div class="col-md-4">
						<span class="step-number">01</span>
						<h6 class="title">Demand Analysis</h6>
						<p>Vestibulum eu tortor eu ex volutpat interdum et non elit. Vivamus aliquam vitae orci in porttitor.</p>
					</div>
					<div class="col-md-4">
						<span class="step-number">02</span>
						<h6 class="title">Web Design</h6>
						<p>Donec venenatis odio et risus fermentum et eleife nd nisi pretium. Integer vestibulum risus augue luctus tellus.</p>
					</div>
					<div class="col-md-4">
						<span class="step-number">03</span>
						<h6 class="title">Program Test</h6>
						<p>Curabitur lacinia sodales erat, id egestas maurisde finibus eget odio sapien, scelerisque eget ex.</p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12" data-noedit="">
						<div class="spacer height-100"></div>
					</div>
				</div>
			</div>
		</div>
	</div> 
</div>
<style class="build-css" data-class="step-04">${csstemplate["step-04"]}</style>
<style class="build-css" data-class="title-08">${csstemplate["title-08"]}</style>
`
}, {
    'thumbnail': 'minis-section/page/section-51.jpg',
    'category': '816',
    'html': `<div class="is-section is-box" style="background-color: rgb(245, 245, 245);">
	<div class="is-boxes">
		<div class="is-box-centered">
			<div class="is-container layout-container" dragwithouthandle="">
				<div class="row">
					<div class="col-md-12">
						<div class="title-08 ml-auto mr-auto mt-10 text-center" style="max-width: 650px;">
							<small class="color-accent">PARTNERS WHO TRUST ON US</small>
							<h3>Partners Who Are Willing To 
								<span class="color-accent">Work With </span>
								<span class="color-accent">Us For A Long Time</span>
							</h3>
						</div>
					</div>
				</div>
				<div class="row clientlist05 border-style2">
					<div class="col-md-3">
						<a href="#" title="">
							<img src="/Portals/_default/ContentBuilder/minis-page/our-service03/ourservice03-img01.png" class="img-Lazy" alt=""></a>
					</div>
					<div class="col-md-3">
						<a href="#" title="">
							<img src="/Portals/_default/ContentBuilder/minis-page/our-service03/ourservice03-img02.png" class="img-Lazy" alt=""></a>
					</div>
					<div class="col-md-3">
						<a href="#" title="">
							<img src="/Portals/_default/ContentBuilder/minis-page/our-service03/ourservice03-img03.png" class="img-Lazy" alt=""></a>
					</div>
					<div class="col-md-3">
						<a href="#" title="">
							<img src="/Portals/_default/ContentBuilder/minis-page/our-service03/ourservice03-img04.png" class="img-Lazy" alt=""></a>
					</div>
					<div class="col-md-3">
						<a href="#" title="">
							<img src="/Portals/_default/ContentBuilder/minis-page/our-service03/ourservice03-img05.png" class="img-Lazy" alt=""></a>
					</div>
					<div class="col-md-3">
						<a href="#" title="">
							<img src="/Portals/_default/ContentBuilder/minis-page/our-service03/ourservice03-img06.png" class="img-Lazy" alt=""></a>
					</div>
					<div class="col-md-3">
						<a href="#" title="">
							<img src="/Portals/_default/ContentBuilder/minis-page/our-service03/ourservice03-img07.png" class="img-Lazy" alt=""></a>
					</div>
					<div class="col-md-3">
						<a href="#" title="">
							<img src="/Portals/_default/ContentBuilder/minis-page/our-service03/ourservice03-img08.png" class="img-Lazy" alt=""></a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<style class="build-css" data-class="clientlist05">${csstemplate["clientlist05"]}</style>
<style class="build-css" data-class="title-08">${csstemplate["title-08"]}</style>
`
}, {
    'thumbnail': 'minis-section/page/section-52.jpg',
    'category': '816',
    'html': `<div class="is-section is-box">
    <div class="is-boxes is-section-md-auto">
        <div class="is-box-centered">
            <div class="is-container layout-container mt-md-50 mb-md-50" dragwithouthandle="">
                <div class="row">
                    <div class="col-md-12">
                        <div class="promo01">
                            <div class="promo-content">
                                <h4 class="mb-10" style="font-weight:700;">Great Design Makes Your Site Stand Out</h4>
                                <p class="mb-10 size-16">Fusce aliquam pellentesque semper feugiat arcu vel luctus sagittis neque nisl lobortis enim dacule.</p>
                            </div>
                            <div class="promo-button">
                                <a href="#" title="GET START NOW" class="button-01">GET START NOW</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style class="build-css" data-class="promo01">${csstemplate["promo01"]}</style>
<style class="build-css" data-class="button-01">${csstemplate["button-01"]}</style>
`
}, {
    'thumbnail': 'minis-section/page/section-53.jpg',
    'category': '816',
    'html': `<div class="is-section is-box layout-no-mt layout-no-mb">
	<div class="is-boxes">
		<div class="is-box-centered">
			<div class="is-container layout-container" dragwithouthandle="">
				<div class="row">
					<div class="col-md-12" data-noedit="">
						<div class="spacer height-100 mb-10"></div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4 pr-lg-40">
						<div class="title-14 text-left">
							<h3>Want To Make Your Site Stand Out?</h3>
							<p class="size-18" style="color:#424242">Our years of experience will help you.</p>
						</div>
					</div>
					<div class="col-md-4">
						<p>Curabitur ac purus est. Curabitur felis metus, porta a semper ut, ultrices a justo. Nam tempus diam et sapien iaculis vehicula. Aenean ipsum nunc, venenatis non diam quis. </p>
						<p>Duis ut diam nibh. Aliquam quis urna et mauris malesuada volutpat. Vivamus et ligula sed nunc venenatis aliquet. Integer ex mauris, faucibus in gravida eget, iaculis a justo. </p>
					</div>
					<div class="col-md-4">
						<p>Interdum et malesuada fames ac ante ipsum primis in faucibus. Aliquam lectus metus, vestibulum a erat vel, sagittis pretium massa. Nullam luctus urna placerat ligula mollis.</p>
						<p>Donec eu nisi a augue rutrum semper. Mauris ut sollicitudin elit. Vivamus et tortor lobortis, consectetur est nec, condimentum sem. Morbi euismod blandit ante.</p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12" data-noedit="">
						<div class="spacer height-40 mb-10"></div>
					</div>
				</div>
				<div class="row align-items-center">
					<div class="col-md-12">
						<fieldset class="title-24">
							<legend>OUR SERVICE FEATURES</legend>
						</fieldset>
					</div>
				</div>
				<div class="row align-items-center">
					<div class="col-md-12" data-noedit="">
						<div class="spacer height-40"></div>
					</div>
				</div>
                ${getSnippetCode("iconbox-28")}
				<div class="row">
					<div class="col-md-12" data-noedit="">
						<div class="spacer height-80 mb-md-15 d-none d-md-block"></div>
						<div class="pb-30 d-block d-md-none"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<style class="build-css" data-class="title-24">${csstemplate["title-24"]}</style>
<style class="build-css" data-class="title-14">${csstemplate["title-14"]}</style>
`
}, {
    'thumbnail': 'minis-section/page/section-54.jpg',
    'category': '816',
    'html': `<div class="is-section is-shadow-1 is-section-auto">
	<div class="is-boxes">
		<div class="is-box-6 is-box is-dark-text is-align-center">
			<div class="is-overlay">
				<div class="is-overlay-bg" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/our-service04/ourservice04-bg01.jpg&quot;);"></div>
				<div class="is-overlay-color"></div>
				<div class="is-overlay-content"></div>
			</div>
			<div class="is-boxes">
				<div class="is-box-centered">
					<div class="is-container container-fluid" style="max-width: 680px;" dragwithouthandle="">
						<div class="row">
							<div class="col-md-12">
								<div class="OurService04-section03 edit-box">
									<p class="size-18 color-accent" style="font-weight: 600;">SEO Basic Optimization</p>
									<h4 style="font-weight: 600;">Make Your Site Have Better Ranking, And Draw More Attention From Visitors.</h4>
									<div class="spacer height-20"></div>
									<p class="mb-0">
										<a href="#" title="CONTACT US" class="button-01">CONTACT US</a>
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="is-box-6 is-box is-align-center">
			<div class="is-overlay">
				<div class="is-overlay-bg" style="background-image:url(&quot;/Portals/_default/ContentBuilder/minis-page/our-service04/ourservice04-bg02.jpg&quot;)"></div>
			</div>
			<div class="is-boxes">
				<div class="is-box-centered">
					<div class="is-container container-fluid" style="max-width: 678px;" dragwithouthandle="">
						<div class="row">
							<div class="col-md-12">
								<div class="OurService04-section03 edit-box">
									<p class="size-18 color-accent" style="font-weight: 600;">Amazing Support</p>
									<h4 style="font-weight: 600;">We Are Committed To Providing Our Customers With The Highest Quality Service.</h4>
									<div class="spacer height-20"></div>
									<p class="mb-0">
										<a href="#" title="CONTACT US" class="button-01">CONTACT US</a>
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
    <style class="build-css" data-class="button-01">${csstemplate["button-01"]}</style>
	<style class="build-css" data-class="OurService04-section03">
		.OurService04-section03 {
			background-color: rgba(255, 255, 255, 0.85);
			padding: 90px 50px 75px;
			margin: 10px 0;
		}

		@media only screen and (max-width: 767px) {
			.OurService04-section03 {
				padding: 35px 25px 35px;
			}
		}
	</style>
</div>
`
}, {
    'thumbnail': 'minis-section/page/section-55.jpg',
    'category': '816',
    'html': `<div class="is-section is-box layout-no-mb">
	<div class="is-overlay">
		<div class="is-overlay-bg" style="background-image:url(&quot;/Portals/_default/ContentBuilder/minis-page/our-service04/ourservice04-bg03.jpg&quot;)"></div>
	</div>
	<div class="is-boxes">
		<div class="is-box-centered">
			<div class="is-container layout-container" dragwithouthandle="">
				<div class="row">
					<div class="col-md-12">
						<div class="title-08 ml-auto mr-auto mt-10 text-center" style="max-width: 650px;">
							<small class="color-accent">OUR SERVICE PROCESS</small>
							<h3>Help To Finish The Product's Design &amp; Development Easily </h3>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12" data-noedit="">
						<div class="spacer height-20"></div>
					</div>
				</div>
                ${getSnippetCode("step05")}
				<div class="row">
					<div class="col-md-12" data-noedit="">
						<div class="spacer height-40 mb-15 d-none d-md-block"></div>
						<div class=" mb-20 d-nblock d-md-one"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<style class="build-css" data-class="title-08">${csstemplate["title-08"]}</style>
`
}, {
    'thumbnail': 'minis-section/page/section-56.jpg',
    'category': '816',
    'html': `<div class="is-section is-shadow-1 layout-no-mt layout-no-mb is-section-auto">
	<div class="is-boxes">
		<div class="is-box-4 is-light-text is-box is-align-center">
			<div class="is-overlay">
				<div class="is-overlay-bg" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/our-service04/ourservice04-bg04.jpg&quot;);"></div>
				<div class="is-overlay-color"></div>
				<div class="is-overlay-content"></div>
			</div>
			<div class="is-boxes">
				<div class="is-box-centered">
					<div class="is-container is-content-500 container-fluid" dragwithouthandle="">
						<div class="row align-items-center">
							<div class="col-md-12">
								<div class="spacer height-40"></div>
								<p class="mb-3 size-18">Email Us</p>
								<h6 class="mt-0 mb-10">dnnskindev@gmail.com</h6>
								<p class="mb-0">
									<i class="sico lnr-envelope-open size-24"><svg>
											<use xlink:href="#lnr-envelope-open"></use>
										</svg></i>
								</p>
								<div class="spacer height-40"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="is-box-4 is-light-text is-box is-align-center">
			<div class="is-overlay">
				<div class="is-overlay-bg" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/our-service04/ourservice04-bg05.jpg&quot;);"></div>
				<div class="is-overlay-color"></div>
				<div class="is-overlay-content"></div>
			</div>
			<div class="is-boxes">
				<div class="is-box-centered">
					<div class="is-container is-content-500 container-fluid" dragwithouthandle="">
						<div class="row align-items-center">
							<div class="col-md-12">
								<div class="spacer height-40"></div>
								<p class="mb-3 size-18">Call Us</p>
								<h6 class="mt-0 mb-10">+1 845-359-0545</h6>
								<p class="mb-0">
									<i class="sico lnr-phone-wave size-24"><svg>
											<use xlink:href="#lnr-phone-wave"></use>
										</svg></i>
								</p>
								<div class="spacer height-40"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="is-box-4 is-light-text is-box is-align-center">
			<div class="is-overlay">
				<div class="is-overlay-bg" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/our-service04/ourservice04-bg06.jpg&quot;);"></div>
				<div class="is-overlay-color"></div>
				<div class="is-overlay-content"></div>
			</div>
			<div class="is-boxes">
				<div class="is-box-centered">
					<div class="is-container is-content-500 container-fluid" dragwithouthandle="">
						<div class="row align-items-center">
							<div class="col-md-12">
								<div class="spacer height-40"></div>
								<p class="mb-3 size-18">Visit us</p>
								<h6 class="mt-0 mb-10">Yanta W Rd, Yanta District, Xian Shi, Shaanxi</h6>
								<p class="mb-0">
									<i class="sico lnr-map-marker size-24"><svg>
											<use xlink:href="#lnr-map-marker"></use>
										</svg></i>
								</p>
								<div class="spacer height-40"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="is-section section-no-padding is-box is-bg-grey is-section-auto">
	<div class="is-boxes">
		<div class="is-box-centered">
			<div class="is-container no-space layout-container is-container-fluid" dragwithouthandle="">
				<div class="row">
					<div class="col-md-12">
						<div class="custom-module loading" data-effect="gmap" id="module-{id}" data-moduleid="{id}" data-settings="%7B%22address%22%3A%22740%20Prince%20Avenue%20Athens%2C%20Georgia%2030601%2C%20740%20Prince%20Ave%2C%20Athens%2C%20GA%2030606%2C%20740%22%2C%22markerscolor%22%3A%22%23303030%22%2C%22markersbg%22%3A%22%23ffffff%22%2C%22markersicon%22%3A%22%5BSkinPath%5Dresource%2Fthumbnails%2Fmap%2Fmapicon06.png%22%2C%22zoom%22%3A%2216%22%2C%22linktype%22%3A%22roadmap%22%2C%22xxl%22%3A%22700%22%2C%22xl%22%3A%22665%22%2C%22l%22%3A%22665%22%2C%22m%22%3A%22665%22%2C%22s%22%3A%22400%22%2C%22xs%22%3A%22360%22%2C%22type%22%3A%22roadmap%22%2C%22position%22%3A%22right%22%2C%22markersshow%22%3A%22checked%22%7D"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
`
}, {
    'thumbnail': 'minis-section/page/section-57.jpg',
    'category': '816',
    'html': `<div class="is-section is-box is-align-center layout-no-mb">
	<div class="is-boxes">
		<div class="is-box-centered">
			<div class="is-container layout-container" dragwithouthandle="">
				<div class="row">
					<div class="col-md-12">
						<div class="title-14 ml-auto mr-auto mt-10" style="max-width: 640px;">
							<h3>We Are Committed To Providing <span class="color-accent">The Best Solution </span>For Our Customers</h3>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<p class="m-auto" style="max-width: 770px;">Etiam nunc purus, finibus nec diam sit amet, ornare molestie odio. Etiam id diam eu est porta feugiat eu ac eros. In vitae purus non nisl cursus vehicula. Quisque at dui mollis ante commodo.</p>
						<div class="spacer height-20 mb-15"></div>
						<a href="#" title="CONTACT US" class="button-01">CONTACT US</a>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12" data-noedit="">
						<div class="spacer height-60"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="is-section is-box layout-no-mb layout-no-mt">
	<div class="is-boxes">
		<div class="is-box-centered">
			<div class="is-container layout-container" dragwithouthandle="">
                ${getSnippetCode("img-box12")}
				<div class="row">
					<div class="col-md-12" data-noedit="">
						<div class="spacer height-60 mb-md-10"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<style class="build-css" data-class="title-14">${csstemplate["title-14"]}</style>
<style class="build-css" data-class="button-01">${csstemplate["button-01"]}</style>
`
}, {
    'thumbnail': 'minis-section/page/section-58.jpg',
    'category': '816',
    'html': `<div class="is-section is-section-auto">
   
	<div class="is-boxes">
		<div class="is-box-6 is-box is-light-text">
			<div class="is-overlay">
				<div class="is-overlay-bg" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/our-service05/ourservice05-bg01.jpg&quot;); background-position: 50% 60%;"></div>
			</div>
			<div class="is-boxes">
				<div class="is-box-centered">
					<div class="is-container is-content-500 container-fluid mr-80" dragwithouthandle="">
						<div class="row">
							<div class="col-md-12">
								<div class="spacer height-60 d-none d-lg-block"></div>
								<h3>Our Years of Tailoring Experience will Impress You</h3>
								<div class="spacer height-20"></div>
								<p>Phasellus eu consectetur diam nullam justo dolor posuere quis urna quis condimentum scelerisque risus vivamus. </p>
								<div class="spacer height-20"></div>
                                 ${getSnippetCode("list-05")}
								<div class="spacer height-60 d-none d-lg-block"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="is-box-6 is-light-text is-box">
			<div class="is-overlay">
				<div class="is-overlay-bg" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/our-service05/ourservice05-bg02.jpg&quot;); background-position: 50% 60%;"></div>
				<div class="is-overlay-content"></div>
			</div>
		</div>
    </div>
    <div class="OurService05-section03-line container d-none d-lg-block"></div>
</div>
<style data-class="OurService05-section03-line" class="build-css"> 
		.OurService05-section03-line {
			position: absolute;
			top: 100px;
			left: 0;
			right: 0;
			bottom: 100px;
			z-index: 10;
			pointer-events: none;
		}

		.OurService05-section03-line::after {
			content: "";
			position: absolute;
			left: -30px;
			right: -30px;
			top: 0;
			bottom: 0;
			border: 6px solid #ffffff;
			z-index: 10;
			pointer-events: none;
		}
	</style>
`
}, {
    'thumbnail': 'minis-section/page/section-59.jpg',
    'category': '816',
    'html': `<div class="is-section is-shadow-1 is-section-auto">
	<div class="is-boxes">
		<div class="is-box-6 is-box">
			<div class="is-overlay">
				<div class="is-overlay-bg" style="background-image:url(&quot;/Portals/_default/ContentBuilder/minis-page/our-service05/ourservice05-bg03.jpg&quot;)"></div>
			</div>
		</div>
		<div class="is-box-6 is-box is-dark-text is-align-center">
			<div class="is-overlay">
				<div class="is-overlay-bg" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/our-service05/ourservice05-bg04.jpg&quot;); background-position: 50% 60%;"></div>
				<div class="is-overlay-content"></div>
			</div>
			<div class="is-boxes">
				<div class="is-box-centered">
					<div class="is-container container-fluid" style="max-width: 630px;" dragwithouthandle="">
						<div class="row">
							<div class="col-md-12">
								<div class="spacer d-none d-md-block height-40"></div>
								<div class="title-14">
									<h3>
										<span class="color-accent">High Efficiency </span>And <span class="color-accent">High Quality </span>Service For You</h3>
								</div>
								<p>Aenean justo nisl accumsan ac diam gravida volutpat elementum ex integer ultricies lorem. Nam elementum in lacus ut posuere nulla tristique metus augue eget accumsan leo dapibus posuere lorem.</p>
								<div class="spacer height-20"></div>
								<div class="row" style="color: rgb(51, 51, 51);">
									<div class="col-md-3">
										<i class="sico lnr-pencil-ruler2 size-42"><svg>
												<use xlink:href="#lnr-pencil-ruler2"></use>
											</svg></i>
										<p>Innovative Ideas</p>
									</div>
									<div class="col-md-3">
										<i class="sico lnr-repeat size-42"><svg>
												<use xlink:href="#lnr-repeat"></use>
											</svg></i>
										<p>Fully Flexible</p>
									</div>
									<div class="col-md-3">
										<i class="sico lnr-desktop size-42"><svg>
												<use xlink:href="#lnr-desktop"></use>
											</svg></i>
										<p>Creative Design</p>
									</div>
									<div class="col-md-3">
										<i class="sico lnr-cog size-42"><svg>
												<use xlink:href="#lnr-cog"></use>
											</svg></i>
										<p>Easy Installation</p>
									</div>
								</div>
								<div class="spacer d-none d-md-block height-40"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<style class="build-css" data-class="title-14">${csstemplate["title-14"]}</style>
`
}, {
    'thumbnail': 'minis-section/page/section-60.jpg',
    'category': '816',
    'html': `<div class="is-section is-box is-light-text is-align-center">
    <div class="is-overlay">
        <div class="is-overlay-bg" style="background-image:url(&quot;/Portals/_default/ContentBuilder/minis-page/our-service05/ourservice05-bg05.jpg&quot;)"></div>
    </div>
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container" dragwithouthandle="">
                <div class="row">
                    <div class="col-md-12">
                        <div class="title-14">
                            <h3>We Offer You A Limited Time Price List</h3>
                        </div>
                        <p class="m-auto" style="max-width: 700px;">Pellentesque est libero, gravida vitae eros vitae, congue mattis ipsum. Donec consectetur arcu eleifend orci viverra, eu posuere orcli</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12" data-noedit="">
                        <div class="spacer height-40"></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="custom-module loading" data-style="countdown-01" data-effect="soon" data-module-desc="Coming soon" id="module-{id}" data-moduleid="{id}" data-settings="%7B%22moduleid%22%3A%22UPIOenW%22%2C%22number%22%3A%224444%22%2C%22format%22%3A%22d%2Ch%2Cm%2Cs%22%2C%22years%22%3A%22Y%22%2C%22months%22%3A%22MM%22%2C%22weeks%22%3A%22W%22%2C%22days%22%3A%22Days%22%2C%22hours%22%3A%22Hours%22%2C%22minutes%22%3A%22Minutes%22%2C%22seconds%22%3A%22Seconds%22%2C%22milliseconds%22%3A%22SS%22%2C%22date%22%3A%22monday%20at%2023%3A55%22%2C%22style%22%3A%22countdown-01%22%7D"></div>
                        <style> .countdown-01 .soon-value span{ color:var(--accent-color); } </style>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12" data-noedit="">
                        <div class="spacer height-40"></div>
                    </div>
                </div>
                <div class="row" style="margin-bottom: -20px;">
                    <div class="col-lg-4 col-md-6">
                        <div class="price-06">
                            <div class="front">
                                <div class="price-header">
                                    <div class="price-pic">
                                        <img src="/Portals/_default/ContentBuilder/minis-page/seo01/seo01-section05-img01.png" class="img-Lazy"></div>
                                    <div class="price-title">
                                        <h5>Standard Plan</h5>
                                    </div>
                                </div>
                                <div class="price-info">
                                    <span class="pricing"><sup>$</sup>105</span>
                                    <span class="unit">Per Month</span>
                                </div>
                                <div class="price-description">
                                    <p>Fusce velit tellus, elementum et libero mollis, pulvinar varius massa. Nullam at rhoncus augue.</p>
                                </div>
                                <div class="price-btn">
                                    <p>LEARN MORE 
                                        <i class="sico lnr-arrow-right"><svg><use xlink:href="#lnr-arrow-right"></use></svg></i>
                                    </p>
                                </div>
                            </div>
                            <div class="back">
                                <div class="price-title">
                                    <h5>Achievable</h5>
                                </div>
                                <div class="price-features">
                                    <ul>
                                        <li>
                                            <i class="sico lnr-check"><svg><use xlink:href="#lnr-check"></use></svg></i>60 Change Keywords</li>
                                        <li>
                                            <i class="sico lnr-check"><svg><use xlink:href="#lnr-check"></use></svg></i>20 Social Media Reviews</li>
                                        <li>
                                            <i class="sico lnr-check"><svg><use xlink:href="#lnr-check"></use></svg></i>10 Blog Posts</li>
                                        <li>
                                            <i class="sico lnr-check"><svg><use xlink:href="#lnr-check"></use></svg></i>Full Time Support</li>
                                        <li>
                                            <i class="sico lnr-check"><svg><use xlink:href="#lnr-check"></use></svg></i>Free Optimization</li>
                                    </ul>
                                </div>
                                <div class="price-btn">
                                    <a href="#" title="WATCH VIDEO" class="button-06">GET START NOW</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="price-06">
                            <div class="front">
                                <div class="price-header">
                                    <div class="price-pic">
                                        <img src="/Portals/_default/ContentBuilder/minis-page/seo01/seo01-section05-img02.png" class="img-Lazy"></div>
                                    <div class="price-title">
                                        <h5>Superior Plan</h5>
                                    </div>
                                </div>
                                <div class="price-info">
                                    <span class="pricing"><sup>$</sup>120</span>
                                    <span class="unit">Per Month</span>
                                </div>
                                <div class="price-description">
                                    <p>Ut venenatis tincidunt nibh, vitae pellentesque arcu imperdiet consequat. Vestibulum non mollis.</p>
                                </div>
                                <div class="price-btn">
                                    <p>LEARN MORE 
                                        <i class="sico lnr-arrow-right"><svg><use xlink:href="#lnr-arrow-right"></use></svg></i>
                                    </p>
                                </div>
                            </div>
                            <div class="back">
                                <div class="price-title">
                                    <h5>Achievable</h5>
                                </div>
                                <div class="price-features">
                                    <ul>
                                        <li>
                                            <i class="sico lnr-check"><svg><use xlink:href="#lnr-check"></use></svg></i>100 Change Keywords</li>
                                        <li>
                                            <i class="sico lnr-check"><svg><use xlink:href="#lnr-check"></use></svg></i>40 Social Media Reviews</li>
                                        <li>
                                            <i class="sico lnr-check"><svg><use xlink:href="#lnr-check"></use></svg></i>25 Blog Posts</li>
                                        <li>
                                            <i class="sico lnr-check"><svg><use xlink:href="#lnr-check"></use></svg></i>Full Time Support</li>
                                        <li>
                                            <i class="sico lnr-check"><svg><use xlink:href="#lnr-check"></use></svg></i>Free Optimization</li>
                                    </ul>
                                </div>
                                <div class="price-btn">
                                    <a href="#" title="WATCH VIDEO" class="button-06">GET START NOW</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="price-06">
                            <div class="front">
                                <div class="price-header">
                                    <div class="price-pic">
                                        <img src="/Portals/_default/ContentBuilder/minis-page/seo01/seo01-section05-img03.png" class="img-Lazy"></div>
                                    <div class="price-title">
                                        <h5>Maximum Plan</h5>
                                    </div>
                                </div>
                                <div class="price-info">
                                    <span class="pricing"><sup>$</sup>150</span>
                                    <span class="unit">Per Month</span>
                                </div>
                                <div class="price-description">
                                    <p>Nam volutpat pulvinar iaculis. Suspendisse erat eros, semper eget egestas sit amet.</p>
                                </div>
                                <div class="price-btn">
                                    <p>LEARN MORE 
                                        <i class="sico lnr-arrow-right"><svg><use xlink:href="#lnr-arrow-right"></use></svg></i>
                                    </p>
                                </div>
                            </div>
                            <div class="back">
                                <div class="price-title">
                                    <h5>Achievable</h5>
                                </div>
                                <div class="price-features">
                                    <ul>
                                        <li>
                                            <i class="sico lnr-check"><svg><use xlink:href="#lnr-check"></use></svg></i>180 Change Keywords</li>
                                        <li>
                                            <i class="sico lnr-check"><svg><use xlink:href="#lnr-check"></use></svg></i>100 Social Media Reviews</li>
                                        <li>
                                            <i class="sico lnr-check"><svg><use xlink:href="#lnr-check"></use></svg></i>50 Blog Posts</li>
                                        <li>
                                            <i class="sico lnr-check"><svg><use xlink:href="#lnr-check"></use></svg></i>Full Time Support</li>
                                        <li>
                                            <i class="sico lnr-check"><svg><use xlink:href="#lnr-check"></use></svg></i>Free Optimization</li>
                                    </ul>
                                </div>
                                <div class="price-btn">
                                    <a href="#" title="WATCH VIDEO" class="button-06">GET START NOW</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12" data-noedit="">
                        <div class="mb-10 d-lg-none"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style class="build-css" data-class="title-14">${csstemplate["title-14"]}</style>
<style class="build-css" data-class="price-06">${csstemplate["price-06"]}</style>
<style class="build-css" data-class="button-06">${csstemplate["button-06"]}</style>
`
}, {
    'thumbnail': 'minis-section/page/section-61.jpg',
    'category': '816',
    'html': `<div class="is-section is-box">
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container" dragwithouthandle="">
                <div class="row align-items-center">
                    <div class="col-md-6">
                        <div class="spacer height-40"></div>
                        <div class="title-14 text-left">
                            <h3>The Most Popular Question <span class="color-accent">Recently Answered</span>
                            </h3>
                        </div>
                        <p>Nulla scelerisque dolor in rhoncus varius dolor nisl commodo lectus sit amet venenatis sem magna scelerisque mauris vestibulum suscipit. Nunc molestie lorem eget neque aliquam nec congue est euismod.</p>
                        <div class="spacer height-40"></div>
                        <a class="button-05 is-lightbox" data-ilightbox="youtube" href="https://www.youtube.com/embed/AkcUoA2f-jU" data-type="iframe" data-options="width: '1280',height: '720',skin:'dark'" title="">
                            <span>
                                <i class="sico fas-play"><svg><use xlink:href="#fas-play"></use></svg></i>
                            </span>WATCH VIDEO</a>
                        <div class="spacer height-40"></div>
                    </div>
                    <div class="col-md-6">
                        <div class="custom-module loading" id="module-{id}" data-moduleid="{id}" data-effect="Accordions" data-style="accordion07" data-settings="%7B%22size%22%3A%22large%22%2C%22items%22%3A%7B%22item0%22%3A%7B%22title%22%3A%22Can%20I%20get%20a%20discount%20for%20this%20theme%3F%22%2C%22description%22%3A%22Phasellus%20semper%20at%20justo%20eget%20interdum.%20Quisque%20mollis%2C%20libero%20non%20feugiat%20hendrerit%2C%20nibh%20ante%20imperdiet%20velit%2C%20tincidunt%20gravida%20ipsum%20neque%20nec%20sem.%20Sed%20et%20purus%20condimentum.%22%7D%2C%22item1%22%3A%7B%22title%22%3A%22Is%20there%20a%20child%20theme%20included%20in%20this%20theme%3F%22%2C%22description%22%3A%22Phasellus%20semper%20at%20justo%20eget%20interdum.%20Quisque%20mollis%2C%20libero%20non%20feugiat%20hendrerit%2C%20nibh%20ante%20imperdiet%20velit%2C%20tincidunt%20gravida%20ipsum%20neque%20nec%20sem.%20Sed%20et%20purus%20condimentum.%22%7D%2C%22item2%22%3A%7B%22title%22%3A%22I%20forgot%20my%20password%2C%20how%20do%20I%20reset%20it%3F%22%2C%22description%22%3A%22Phasellus%20semper%20at%20justo%20eget%20interdum.%20Quisque%20mollis%2C%20libero%20non%20feugiat%20hendrerit%2C%20nibh%20ante%20imperdiet%20velit%2C%20tincidunt%20gravida%20ipsum%20neque%20nec%20sem.%20Sed%20et%20purus%20condimentum.%22%7D%2C%22item3%22%3A%7B%22title%22%3A%22How%20do%20I%20install%20the%20page%20template%3F%22%2C%22description%22%3A%22Phasellus%20semper%20at%20justo%20eget%20interdum.%20Quisque%20mollis%2C%20libero%20non%20feugiat%20hendrerit%2C%20nibh%20ante%20imperdiet%20velit%2C%20tincidunt%20gravida%20ipsum%20neque%20nec%20sem.%20Sed%20et%20purus%20condimentum.%22%7D%2C%22item4%22%3A%7B%22title%22%3A%22Can%20I%20delete%20the%20existing%20page%3F%22%2C%22description%22%3A%22Phasellus%20semper%20at%20justo%20eget%20interdum.%20Quisque%20mollis%2C%20libero%20non%20feugiat%20hendrerit%2C%20nibh%20ante%20imperdiet%20velit%2C%20tincidunt%20gravida%20ipsum%20neque%20nec%20sem.%20Sed%20et%20purus%20condimentum.%22%7D%7D%2C%22accopen%22%3A%22checked%22%2C%22accopenindex%22%3A%222%22%7D"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style class="build-css" data-class="title-14">${csstemplate["title-14"]}</style>
<style class="build-css" data-class="button-05">${csstemplate["button-05"]}</style>
`
}, {
    'thumbnail': 'minis-section/page/section-62.jpg',
    'category': '816',
    'html': `<div class="is-section is-box layout-no-mt layout-no-mb" style="background-color: rgb(244, 244, 244);">
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container" dragwithouthandle="">
                <div class="row">
                    <div class="col-md-12" data-noedit="">
                        <div class="spacer height-60"></div>
                    </div>
                </div>
                <div class="row clientlist01">
                    <div class="col-md-2 col-6">
                        <a href="#" title="">
                            <img src="/Portals/_default/ContentBuilder/minis-page/our-service05/ourservice05-img04.png" class="img-Lazy" alt=""></a>
                    </div>
                    <div class="col-md-2 col-6">
                        <a href="#" title="">
                            <img src="/Portals/_default/ContentBuilder/minis-page/our-service05/ourservice05-img05.png" class="img-Lazy" alt=""></a>
                    </div>
                    <div class="col-md-2 col-6">
                        <a href="#" title="">
                            <img src="/Portals/_default/ContentBuilder/minis-page/our-service05/ourservice05-img06.png" class="img-Lazy" alt=""></a>
                    </div>
                    <div class="col-md-2 col-6">
                        <a href="#" title="">
                            <img src="/Portals/_default/ContentBuilder/minis-page/our-service05/ourservice05-img07.png" class="img-Lazy" alt=""></a>
                    </div>
                    <div class="col-md-2 col-6">
                        <a href="#" title="">
                            <img src="/Portals/_default/ContentBuilder/minis-page/our-service05/ourservice05-img08.png" class="img-Lazy" alt=""></a>
                    </div>
                    <div class="col-md-2 col-6">
                        <a href="#" title="">
                            <img src="/Portals/_default/ContentBuilder/minis-page/our-service05/ourservice05-img09.png" class="img-Lazy" alt=""></a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12" data-noedit="">
                        <div class="spacer height-60"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style class="build-css" data-class="clientlist01">${csstemplate["clientlist01"]}</style>
`
})


/*Coming-Soon*/

data_basic.designs.push({
    'thumbnail': 'minis-section/coming-soon/coming-soon01.jpg',
    'category': '817',
    'html': `<div class="is-section is-box is-section-100 is-light-text is-align-center">
    <div class="is-overlay">
        <div class="is-overlay-bg" style="background-image:url(&quot;/Portals/_default/ContentBuilder/minis-page/coming-soon/coming-soon.jpg&quot;)"></div>
    </div>
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container position-static">
                <div class="row">
                    <div class="col-md-12">
                        <h1 style="font-weight: 300;">Our Extraordinary Websit Is <br><span class="color-accent bold">Coming Soon</span></h1>
                        <div class="spacer height-20"></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                    ${getSnippetCode("countdown-05")}
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 m-auto" style="max-width: 475px;">
                        <h6 style="margin-bottom: 25px;">Will Update You Once It Is Live</h6>
                        ${getSnippetCode("ajaxform-04","{id2}")}
                    </div>
                </div>
                <div class="row" style="position: absolute; bottom: 0px; left: 0px; right: 0px;">
                    <div class="col-md-12">
                        <p style="font-size:20px"> <i class="mr-5 sico lnr-phone-wave"></i> +1 845-359-0545
                            <br class="d-block d-md-none"> <i class="ml-md-45 mr-5 sico far-envelope-open"></i> service.simple@gmail.com
                        </p>
                        <div class="spacer height-20"></div>
                    </div>
                </div>


            </div>
        </div>
    </div>
</div>
</div>`
})


/*Organic Food*/

data_basic.designs.push({
        'thumbnail': 'minis-section/organicfood/banner.jpg',
        'category': '818',
        'html': `<div class="is-section section-slider swiper-container is-section-100" data-pagination="false" data-autoplay="{&quot;delay&quot;:8000}">
    <div class="is-boxes swiper-wrapper">
        <div class="is-box swiper-slide is-light-text is-align-left">
            <div class="is-overlay">
                <div class="is-overlay-bg" style="background-image: url(&quot;http://minis.test.dnngo.net/Portals/_default/ContentBuilder/minis-page/organicfood/organicfood-banner-bg01.png&quot;);"></div>
            </div>
            <div class="is-boxes">
                <div class="is-box-centered">
                    <div class="is-container" style="max-width: 1415px;" dragwithouthandle="">
                        <div class="row">
                            <div class="col-md-7 col-lg-5">
                                <div class="spacer height-60 d-none d-lg-block"></div>
                                <h1 style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/organicfood/organicfood-banner-img01.png&quot;); background-repeat: no-repeat; background-position: left center; background-size: contain; font-weight: 700;">100% Natural Organic Healthy Foods</h1>
                                <div class="spacer height-40"></div>
                                <p>In auctor, diam eget varius tristique, ante ligula posuere diam, eu posuere ante velit ut mi. Vivamus quis bibendum mauris. Suspendisse et felis ac lectus laoreet facilisis sit amet id elit. </p>
                                <div class="spacer height-20 mb-10"></div>
                                <a href="#" title="CONTACT US" class="button-01 bg-accent3">CONTACT US</a>
                            </div>
                            <div class="col-md-5 col-lg-7">
                                <div dragwithouthandle=""></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="is-box swiper-slide is-light-text is-align-left">
            <div class="is-overlay">
                <div class="is-overlay-bg" style="background-image: url(&quot;http://minis.test.dnngo.net/Portals/_default/ContentBuilder/minis-page/organicfood/organicfood-banner-bg02.png&quot;);"></div>
            </div>
            <div class="is-boxes">
                <div class="is-box-centered">
                    <div class="is-container" style="max-width: 1264px;" dragwithouthandle="">
                        <div class="row">
                            <div class="col-md-5 col-lg-7">
                                <div dragwithouthandle=""></div>
                            </div>
                            <div class="col-md-7 col-lg-5">
                                <div class="spacer height-60"></div>
                                <h2 style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/organicfood/organicfood-banner-img02.png&quot;); background-repeat: no-repeat; background-position: left center; background-size: contain;" data-keep-background="">Everything Is 
                                    <br>
                                    <span class="size-60">Freshly Picked</span>
                                </h2>
                                <div class="spacer height-20 mb-15"></div>
                                <p>Nulla sit amet egestas ligula, posuere posuere enim. Sed facilisis risus eget facilisis ullamcorper. Nulla eget semper dui, vel laoreet lectus. Sed blandit posuere feugiat.</p>
                                <div class="spacer height-20 mb-10"></div>
                                <a href="#" title="LEARN MORE" class="button-01 bg-accent3">LEARN MORE</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="is-box swiper-slide is-light-text is-align-left">
            <div class="is-overlay">
                <div class="is-overlay-bg" style="background-image: url(&quot;http://minis.test.dnngo.net/Portals/_default/ContentBuilder/minis-page/organicfood/organicfood-banner-bg03.png&quot;);"></div>
            </div>
            <div class="is-boxes">
                <div class="is-box-centered">
                    <div class="is-container" style="max-width: 1415px;" dragwithouthandle="">
                        <div class="row">
                            <div class="col-md-7 col-lg-5 pr-lg-80">
                                <div class="spacer height-60"></div>
                                <h2 style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/organicfood/organicfood-banner-img03.png&quot;); background-repeat: no-repeat; background-position: left center; background-size: auto; font-weight: 400;" data-keep-background="">All Kinds Of 
                                    <br>
                                    <span class="size-60" style="font-weight: 600;">Green Vegetables</span>
                                </h2>
                                <div class="spacer height-20 mb-15"></div>
                                <p class="pr-lg-20">Nulla sit amet egestas ligula, posuere posuere enim. Sed facilisis risus eget facilisis ullamcorper eget semper dui.</p>
                                <div class="spacer height-20 mb-10"></div>
                                <a href="#" title="LEARN MORE" class="button-01 bg-accent3">LEARN MORE</a>
                            </div>
                            <div class="col-md-5 col-lg-7">
                                <div dragwithouthandle=""></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style class="build-css" data-class="button-01">${csstemplate["button-01"]}</style>
    `
    }, {
        'thumbnail': 'minis-section/organicfood/section-01.jpg',
        'category': '818',
        'html': `<div class="is-section is-box organicfood-section01 is-align-center anchorTag" id="About" data-title="About Us">
    <div class="is-overlay">
        <div class="is-overlay-bg" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/organicfood/organicfood-section01-bg.jpg&quot;);"></div>
    </div>
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container" style="max-width: 970px;" dragwithouthandle="">
                <div class="row">
                    <div class="col-md-12">
                        <div class="spacer height-40 d-none d-md-block"></div>
                        <div class="m-auto" style="max-width: 620px;">
                            <h3 style="line-height: 1.4;">Specialized In The Study Of Organic Nutrition
                                <span class="color-accent"> Without Substances</span>
                            </h3>
                        </div>
                        <div class="spacer height-20 mb-10"></div>
                        <p>Etiam eleifend eget elit nec ultricies. Donec ultrices molestie eros et suscipit. Curabitur blandit et magna in mattis. Suspendisse pellentesque, risus vulputate vestibulum tempus, enim augue malesuada tortor, vel interdum magna lectus ut ante. Etiam vel metus nisi. Fusce ligula augue, pharetra quis posuere eu, tincidunt at nisi.</p>
                        <div class="spacer height-20" style="margin-bottom:12px;"></div>
                    </div>
                </div>
                ${getSnippetCode("counter-08")}
                <div class="row">
                    <div class="col-md-12">
                        <div class="spacer height-80"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    `
    }, {
        'thumbnail': 'minis-section/organicfood/section-02.jpg',
        'category': '818',
        'html': `<div class="is-section is-box is-light-text organicfood-section02" data-title="">
    <div class="is-overlay">
        <div class="is-overlay-bg" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/organicfood/organicfood-section02-bg.png&quot;);"></div>
    </div>
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container" dragwithouthandle="">
                <div class="row">
                    <div class="col-md-12">
                        <div class="spacer height-80 mt-15"></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6"></div>
                    <div class="col-lg-6">
                        <div class="pl-lg-30">
                            <div>
                                <img src="/Portals/_default/ContentBuilder/minis-page/organicfood/organicfood-section02-icon.png" alt="icon" class="img-Lazy"></div>
                            <h3 class="mt-20 pt-2">A Reliable Organic Farm</h3>
                            <div class="spacer height-40"></div>
                            <p>Fusce congue, sem quis luctus euismod, ipsum libero iaculis tortor, nec euismod nisl est nec nisl. Aenean ut scelerisque nibh. Nam ultrices, quam at hendrerit dapibus, felis orci pulvinar elit</p>
                            <div class="spacer height-20"></div>
                            <div class="row">
                                <div class="col-6">
                                    <div class="iconbox-03" style="margin-bottom: 6px;">
                                        <div class="icon">
                                            <img src="/Portals/_default/ContentBuilder/minis-page/organicfood/organicfood-section02-img01.png" alt="icon" class="img-Lazy"></div>
                                        <h6>Natural Food</h6>
                                        <p>Donec sed scelerisque libero. Etiam vitae tellus mattis.</p>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="iconbox-03" style="margin-bottom: 6px;">
                                        <div class="icon">
                                            <img src="/Portals/_default/ContentBuilder/minis-page/organicfood/organicfood-section02-img02.png" alt="icon" class="img-Lazy"></div>
                                        <h6>Wide Varieties</h6>
                                        <p>Curabitur mollis, purus quis nunc odio eleifend dignissim.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="spacer height-40"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style class="build-css" data-class="iconbox-03">${csstemplate["iconbox-03"]}</style>
    `
    }, {
        'thumbnail': 'minis-section/organicfood/section-03.jpg',
        'category': '818',
        'html': `
    <div class="is-section is-box organicfood-section03 anchorTag" data-title="New Products" id="Products">
    <div class="is-overlay">
        <div class="is-overlay-bg" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/organicfood/organicfood-section03-bg.jpg&quot;);"></div>
    </div>
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container" dragwithouthandle="">
                <div class="row">
                    <div class="col-md-12">
                        <div class="spacer height-20"></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="text-center m-auto" style="max-width: 770px;">
                            <img src="/Portals/_default/ContentBuilder/minis-page/organicfood/organicfood-section03-icon.png" alt="icon">
                            <h3 class="mt-20 pt-2 mb-5">Fresh Fruits And Vegetables In Season</h3>
                            <div class="spacer height-20"></div>
                            <p class="pb-10">Donec luctus, lectus vel porttitor pellentesque, justo quam sollicitudin felis, et mattis quam nulla nec eros. Vivamus pellentesque sem quis urna tempus congue.</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                    ${getSnippetCode("carousel06")}
                     </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="spacer height-20" style="margin-bottom:-10px;"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    `
    }, {
        'thumbnail': 'minis-section/organicfood/section-04.jpg',
        'category': '818',
        'html': `<div class="is-section is-box is-light-text organicfood-section04 anchorTag" id="Service" data-title="Service">
    <div class="is-overlay">
        <div class="is-overlay-bg" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/organicfood/organicfood-section04-bg.png&quot;);"></div>
    </div>
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container" dragwithouthandle="">
                <div class="row">
                    <div class="col-md-12">
                        <div class="spacer height-20"></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="text-center m-auto" style="max-width: 770px;">
                            <img src="/Portals/_default/ContentBuilder/minis-page/organicfood/organicfood-section04-icon.png" alt="icon" class="img-Lazy">
                            <h3 class="mt-20 pt-2 mb-5 pb-3">Why Choose Us</h3>
                            <div class="spacer height-20"></div>
                            <p class="pb-10">Integer non neque ante. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum id turpis est. Proin porta quis mauris quis gravida.</p>
                        </div>
                    </div>
                </div>
                ${getSnippetCode("iconbox-17")}
            </div>
        </div>
    </div>
</div>
<style class="build-css" data-class="organicfood-section04">@media only screen and (min-width: 1200px) { .organicfood-section04 .imageBox{ margin-left: -55px; margin-right: -55px; } }</style>
    `
    }, {
        'thumbnail': 'minis-section/organicfood/section-05.jpg',
        'category': '818',
        'html': `<div class="is-section is-box organicfood-section05 anchorTag" id="Portfolios" data-title="Portfolios">
    <div class="is-overlay">
        <div class="is-overlay-bg" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/organicfood/organicfood-section05-bg.jpg&quot;);"></div>
    </div>
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container" dragwithouthandle="">
                <div class="row">
                    <div class="col-md-12">
                        <div class="spacer height-20 mt-10"></div>
                    </div>
                </div>
                <div class="row align-items-center">
                    <div class="col-lg-4">
                        <img src="/Portals/_default/ContentBuilder/minis-page/organicfood/organicfood-section05-icon.png" alt="icon">
                        <h3 class="mt-20 pt-2">Our Amazing Garden And Orchard</h3>
                        <div class="spacer height-20"></div>
                        <p class="pr-lg-40">Sed vel varius justo. Nulla facilisi. Nam nec aliquet arcu. Ut vestibulum lobortis massa, sit amet porta ligula.</p>
                        <div class="spacer height-40" style="margin-bottom:-8px;"></div>
                        <a href="#" title="LEARN MORE" class="button-01">LEARN MORE</a>
                        <div class="spacer height-20 mb-10"></div>
                    </div>
                    <div class="col-lg-8">
                    ${getSnippetCode("img-box10")}
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="spacer height-40"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style class="build-css" data-class="button-01">${csstemplate["button-01"]}</style>
    `
    }, {
        'thumbnail': 'minis-section/organicfood/section-06.jpg',
        'category': '818',
        'html': `<div class="is-section is-box is-light-text organicfood-section06" data-title="">
    <div class="is-overlay">
        <div class="is-overlay-bg" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/organicfood/organicfood-section06-bg.png&quot;);"></div>
    </div>
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container" style="max-width: 1500px;" dragwithouthandle="">
                <div class="row">
                    <div class="col-md-12">
                        <div class="spacer height-40 mb-10"></div>
                    </div>
                </div>
                <div class="row justify-content-between">
                    <div class="col-md-6">
                        <div class="pl-xl-50" style="max-width:500px;">
                            <p class="size-120" style="color: rgb(255, 102, 71); font-weight: bold; font-style: italic; line-height: 1; margin-bottom: 0px;">50%</p>
                            <p class="size-32 pb-10" style="font-weight: bold; line-height: 1.3125;">Discount off for all different kinds of organic foods</p>
                            <a href="#" title="LEARN MORE" class="button-01">CONTACT US</a>
                        </div>
                    </div>
                    <div class="col-md-6 col-xl-4">
                        <div class="spacer height-160 mt-10"></div>
                        <h2>Deal Of The Day</h2>
                        <div class="spacer height-20" style="margin-bottom: -7px;"></div>
                        <p class="size-18">Etiam auctor magna ut metus laoreet, id cursus dolor tristique. Fusce molestie sit amet ante.</p>
                        <div class="spacer pb-10" style="margin-bottom: -12px;"></div>
                        ${getSnippetCode("countdown-04")}
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="spacer height-80 mb-5"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style class="build-css" data-class="button-01">${csstemplate["button-01"]}</style>
<style class="build-css" data-class="organicfood-section06">.organicfood-section06{ z-index: 1; margin-top: -40px; }</style>
    `
    }, {
        'thumbnail': 'minis-section/organicfood/section-07.jpg',
        'category': '818',
        'html': `<div class="is-section is-box organicfood-section07 anchorTag" style="background-color: rgb(242, 247, 247);" id="Team" data-title="Oure Team">
    <div class="is-overlay">
        <div class="is-overlay-bg" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/organicfood/organicfood-section07-bg.png&quot;);"></div>
    </div>
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container" dragwithouthandle="">
                <div class="row">
                    <div class="col-md-12">
                        <div class="spacer height-40"></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="text-center m-auto" style="max-width: 770px;">
                            <img src="/Portals/_default/ContentBuilder/minis-page/organicfood/organicfood-section07-icon.png" alt="icon">
                            <h3 class="mt-20 pt-2">Our Experienced Farmers</h3>
                            <div class="spacer height-20"></div>
                            <p class="pb-5">Praesent porta velit a imperdiet imperdiet. Aliquam eu elementum augue. Mauris laoreet lobortis purus. Mauris orci leo, ultricies sed faucibus et, bibendum et dui.</p>
                            <div class="spacer height-40"></div>
                        </div>
                    </div>
                </div>
                ${getSnippetCode("ourteam-03")}
                <div class="row">
                    <div class="col-md-12">
                        <div class="spacer height-40"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style class="build-css" data-class="organicfood-section07">.organicfood-section07{ z-index: 0; margin-top: -20px; margin-bottom: -20px; }</style>
    `
    }, {
        'thumbnail': 'minis-section/organicfood/footer.jpg',
        'category': '818',
        'html': `<div class="is-section is-box is-light-text organicfood-footer anchorTag" id="Contact" data-title="Contact">
    <div class="is-overlay">
        <div class="is-overlay-bg" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/organicfood/organicfood-footer-bg.png&quot;);"></div>
    </div>
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container" dragwithouthandle="">
                <div class="row">
                    <div class="col-md-12">
                        <div class="spacer height-20" style="margin-bottom: -10px"></div>
                    </div>
                </div>
                <div class="row justify-content-between align-items-center">
                    <div class="col-md-7">
                        <h3 style="font-weight:600;">Wanna Get Our Natural Organic Food Or Just Visit Our Organic Farm</h3>
                    </div>
                    <div class="col-md-3 col-xl-2">
                        <a href="#" title="CONTACT US" class="button-01">CONTACT US</a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="spacer height-40 mb-15"></div>
                    </div>
                </div>
                <div class="row align-items-center text-center ml-0 mr-0" style="background-color: rgb(255, 255, 255); border-radius: 3px;">
                    <div class="col-6 col-md-1/5 mt-25 mb-25">
                        <span>
                            <a href="#">
                                <img alt="logo" src="/Portals/_default/ContentBuilder/minis-page/organicfood/organicfood-footer-img01.png" class="img-Lazy"></a>
                        </span>
                    </div>
                    <div class="col-6 col-md-1/5 mt-25 mb-25">
                        <span>
                            <a href="#">
                                <img alt="logo" src="/Portals/_default/ContentBuilder/minis-page/organicfood/organicfood-footer-img02.png" class="img-Lazy"></a>
                        </span>
                    </div>
                    <div class="col-6 col-md-1/5 mt-25 mb-25">
                        <span>
                            <a href="#">
                                <img alt="logo" src="/Portals/_default/ContentBuilder/minis-page/organicfood/organicfood-footer-img03.png" class="img-Lazy"></a>
                        </span>
                    </div>
                    <div class="col-6 col-md-1/5 mt-25 mb-25">
                        <span>
                            <a href="#">
                                <img alt="logo" src="/Portals/_default/ContentBuilder/minis-page/organicfood/organicfood-footer-img04.png" class="img-Lazy"></a>
                        </span>
                    </div>
                    <div class="col-6 col-md-1/5 mt-25 mb-25">
                        <span>
                            <a href="#">
                                <img alt="logo" src="/Portals/_default/ContentBuilder/minis-page/organicfood/organicfood-footer-img05.png" class="img-Lazy"></a>
                        </span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="spacer height-60 mb-5"></div>
                    </div>
                </div>
                <div class="row size-16" style="color: rgb(135, 135, 135);">
                    <div class="col-md-4 mb-20 mb-md-0">
                        <img src="/Portals/_default/ContentBuilder/minis-page/logo-white.png" style="margin-bottom: 31px;margin-top: 5px;" alt="image" class="img-Lazy">
                        <p class="mb-10">@ 2019 by DNNGo Corp. All Rights Resevered</p>
                        <div class="edit-box organicfood-footer-social organicfood-footer-linklist">
                            <a href="#" title="facebook">
                                <i class="sico fab-facebook-f" style="font-size: 15px; margin-right: 15px;"><svg><use xlink:href="#fab-facebook-f"></use></svg></i>
                            </a>
                            <a href="#" title="linkedin">
                                <i class="sico fab-linkedin-in" style="font-size: 15px; margin-right: 15px;"><svg><use xlink:href="#fab-linkedin-in"></use></svg></i>
                            </a>
                            <a href="#" title="twitter">
                                <i class="sico fab-twitter" style="font-size: 15px; margin-right: 15px;"><svg><use xlink:href="#fab-twitter"></use></svg></i>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 mb-20 mb-md-0">
                        <p class="mb-0">Free legal advice</p>
                        <p class="size-24 mb-10 mt-2 color-accent" style=" font-style: italic;">(845) 359-7777</p>
                        <p class="mb-0">Yanta W Rd, Yanta District, Xian Shi, Shaanxi</p>
                        <p class="mb-0">service.simple@gmail.com</p>
                    </div>
                    <div class="col-md-4 mb-20 mb-md-0">
                        <div class="row organicfood-footer-linklist">
                            <div class="col-sm-6">
                                <p>
                                    <a href="#" title="What we do">What we do</a>
                                </p>
                                <p>
                                    <a href="#" title="Our service">Our service</a>
                                </p>
                                <p>
                                    <a href="#" title="Creative design">Creative design</a>
                                </p>
                                <p>
                                    <a href="#" title="Industry experts">Industry experts</a>
                                </p>
                            </div>
                            <div class="col-sm-6">
                                <p>
                                    <a href="#" title="Our team">Our team</a>
                                </p>
                                <p>
                                    <a href="#" title="Our pricing">Our pricing</a>
                                </p>
                                <p>
                                    <a href="#" title="FAQ">FAQ</a>
                                </p>
                                <p>
                                    <a href="#" title="The latest news">The latest news</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style class="build-css" data-class="button-01">${csstemplate["button-01"]}</style>
<style data-class="organicfood-footer-linklist">.organicfood-footer-linklist a,.organicfood-footer-linklist a:link {color: currentColor;}.organicfood-footer-linklist a:hover,.organicfood-footer-linklist a:hover>i {color: var(--accent-color)}.organicfood-footer-linklist p {margin-bottom: 10px;} </style><style data-class="organicfood-footer-social">.organicfood-footer-social a:hover,.organicfood-footer-social a:hover>i {color: var(--accent-color);transition: color ease 300ms;-webkit-transition: color ease 300ms;} </style>

    `
    }

)



data_basic.designs.push({
        'thumbnail': 'minis-section/saas/banner.jpg',
        'category': '819',
        'html': `<div class="is-section is-box is-light-text is-align-center saas-banner">
    <div class="is-overlay">
        <div class="is-overlay-bg" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/saas/saas-banner-bg.jpg&quot;);"></div>
    </div>
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container" dragwithouthandle="">
                <div class="row">
                    <div class="col-md-12">
                        <div class="spacer height-80"></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="size-50">Work With Us</h2>
                        <h3 style="font-weight: 400;">Boost Up Your Business Like A Charm!</h3>
                        <div class="spacer height-20 mb-4"></div>
                        <a class="play-button is-lightbox" href="https://www.youtube.com/embed/AkcUoA2f-jU" data-ilightbox="youtube" title="">
                            <span style="width: 60px; height: 60px;">
                                <i class="sico fas-play color-accent2 size-18"><svg><use xlink:href="#fas-play"></use></svg></i>
                            </span>
                        </a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="spacer height-100 mb-15"></div>
                        <div class="spacer height-100 d-none d-lg-block"></div>
                        <div class="spacer height-100 d-none d-lg-block"></div>
                        <div class="spacer height-100 d-none d-lg-block"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="is-section is-box is-align-center is-section-auto layout-no-mt layout-no-mb saas-section01">
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container is-container-fluid" dragwithouthandle="">
                <div class="row">
                    <div class="col-md-12">
                        <img alt="image" title="" src="/Portals/_default/ContentBuilder/minis-page/saas/saas-section01-img.png" class="img-Lazy"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<style class="build-css" data-class="play-button">${csstemplate["play-button"]}</style>
<style class="build-css" data-class="saas-section01">
	.saas-section01 {
		margin-top: -414px;
	}

	@media only screen and (max-width: 991px) {
		.saas-section01 {
			margin-top: -100px;
		}
	}

	@media only screen and (max-width: 767px) {
		.saas-section01 {
			margin-top: -80px;
		}
	}

	@media only screen and (max-width: 575px) {
		.saas-section01 {
			margin-top: -100px;
		}
	}
</style>
    `
    }, {
        'thumbnail': 'minis-section/saas/section-01.jpg',
        'category': '819',
        'html': `
    <div class="is-section is-box layout-no-mt layout-no-mb saas-section02 anchorTag" id="Features" data-title="Features">
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container" dragwithouthandle="">
                <div class="row">
                    <div class="col-md-12">
                        <div class="spacer height-60"></div>
                        <div class="text-center m-auto" style="max-width: 770px;">
                            <h2 class="color-accent2">Our Features</h2>
                            <div class="spacer height-20"></div>
                            <p style="font-size:18px;">Quisque ut ipsum eget quam posuere sodales. Donec sagittis faucibus justo, sed laoreet risus posuere posuere. Integer ultricies lacus sit amet turpis porta molestie.</p>
                        </div>
                        <div class="spacer height-40" style="margin-bottom: -2px"></div>
                    </div>
                </div>
                ${getSnippetCode("flipbox-01")}
            </div>
        </div>
    </div>
</div>  `
    }, {
        'thumbnail': 'minis-section/saas/section-02.jpg',
        'category': '819',
        'html': `<div class="is-section is-box layout-no-mt layout-no-mb saas-section03 anchorTag" data-title="About" id="About">
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container" dragwithouthandle="">
                <div class="row">
                    <div class="col-md-12">
                        <div class="spacer height-60" style="margin-bottom: 4px;"></div>
                    </div>
                </div>
                <div class="row align-items-center">
                    <div class="col-md-6">
                        <h2 class="color-accent2">About Our SaaS</h2>
                        <div class="spacer height-40" style="margin-bottom: -10px;"></div>
                        <p>Mauris vitae faucibus purus, nec scelerisque turpis. Cras sit amet elit vel est scelerisque consequat id sit amet ante. Morbi sit amet purus elit. Morbi suscipit eu nisi et fermentum.</p>
                        ${getSnippetCode("blockquotes-04")}
                        <div class="spacer height-40 d-md-none"></div>
                    </div>
                    <div class="col-md-6 right-full-column">
                        <div class="full-column-inner text-left">
                            <img src="/Portals/_default/ContentBuilder/minis-page/saas/saas-section03-img.png" class="img-Lazy" alt="About Our SaaS"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    `
    }, {
        'thumbnail': 'minis-section/saas/section-03.jpg',
        'category': '819',
        'html': `<div class="is-section is-box layout-no-mt saas-section04 layout-no-mb">
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container" dragwithouthandle="">
                <div class="row">
                    <div class="col-md-12">
                        <div class="spacer height-60" style="margin-bottom: -8px;"></div>
                    </div>
                </div>
                <div class="row align-items-center">
                    <div class="col-md-6 pl-lg-35">
                        <h2 class="color-accent2">Why Choose Us</h2>
                        <div class="spacer height-40" style="margin-bottom: -10px;"></div>
                        <p>In tincidunt justo elit. Vivamus augue velit, scelerisque eu varius aliquet, cursus et metus. Aliquam non sodales nulla. Donec elementum est ac auctor luctus. Lorem ipsum dolor sit amet.</p>
                        <div class="spacer height-20" style="margin-bottom: -5px;"></div>
                        ${getSnippetCode("list-02")}
                        <div class="spacer height-40 d-md-none"></div>
                    </div>
                    <div class="col-md-6 left-full-column order-md-first">
                        <div class="full-column-inner text-right">
                            <img src="/Portals/_default/ContentBuilder/minis-page/saas/saas-section04-img.png" class="img-Lazy" alt="Why Choose Us"></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12" data-noedit="">
                        <div class="spacer height-80"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    `
    }, {
        'thumbnail': 'minis-section/saas/section-04.jpg',
        'category': '819',
        'html': `<div class="is-section is-box is-light-text layout-no-mb saas-section05">
    <div class="is-overlay">
        <div class="is-overlay-bg" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/saas/saas-section05-bg.jpg&quot;);"></div>
    </div>
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container" dragwithouthandle="">
                <div class="row">
                    <div class="col-md-12">
                        <div class="spacer height-20" style="margin-bottom: -15px;"></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="m-auto text-center" style="max-width: 700px;">
                            <h2>Well-designed Interface Has Everything You Desire</h2>
                            <div class="spacer height-20"></div>
                        </div>
                        <div class="m-auto text-center" style="max-width: 940px;">
                            <p style="font-size:18px;">Quisque ut ipsum eget quam posuere sodales. Donec sagittis faucibus justo, sed laoreet risus posuere posuere. Integer ultricies lacus sit amet turpis porta molestie. </p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="spacer height-20" style="margin-bottom: -7px;"></div>
                        ${getSnippetCode("swiper01")}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style class="build-css" data-class="saas-section05">.saas-section05::after{content: '';position: absolute;left: 0;bottom: 0;z-index: 1;width: 100%;padding-bottom: 14%;background: #fff;}.saas-section05 .is-overlay{z-index: 0;}.saas-section05 .is-boxes{z-index: 2;} </style>
    `
    }, {
        'thumbnail': 'minis-section/saas/section-05.jpg',
        'category': '819',
        'html': `<div class="is-section is-box layout-no-mt layout-no-mb saas-section06 anchorTag" id="Price" data-title="Price">
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container" dragwithouthandle="">
                <div class="row">
                    <div class="col-md-12">
                        <div class="spacer height-80" style="margin-bottom: -6px;"></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="text-center m-auto" style="max-width: 770px;">
                            <h2 class="color-accent2">Reasonable Price</h2>
                            <div class="spacer height-20"></div>
                            <p style="font-size:18px;">Quisque ut ipsum eget quam posuere sodales. Donec sagittis faucibus justo, sed laoreet risus posuere posuere. Integer ultricies lacus sit amet.</p>
                        </div>
                        <div class="spacer height-40" style="margin-bottom: -2px"></div>
                    </div>
                </div>
                ${getSnippetCode("price-07")}
                <div class="row">
                    <div class="col-md-12">
                        <div class="spacer height-80"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    `
    }, {
        'thumbnail': 'minis-section/saas/section-06.jpg',
        'category': '819',
        'html': `<div class="is-section is-box is-light-text saas-section07">
    <div class="is-overlay">
        <div class="is-overlay-bg" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/saas/saas-section08-bg.jpg&quot;);"></div>
    </div>
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container" dragwithouthandle="">
                <div class="row">
                    <div class="col-md-12">
                        <div class="spacer height-40"></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="text-center">
                            <h2>What Are You Waiting For? </h2>
                            <h3 class="mt-20" style="font-weight: 300;">Subscribe to get the latest news or best deals.</h3>
                        </div>
                        <div class="spacer height-20" style="margin-bottom: 10px"></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 m-auto" style="max-width: 508px">
                    ${getSnippetCode("ajaxform-02")}
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="spacer height-60" style="margin-bottom: -5px;"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    `
    }, {
        'thumbnail': 'minis-section/saas/section-07.jpg',
        'category': '819',
        'html': `<div class="is-section is-box saas-section08 anchorTag" data-title="Team" id="Team">
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container" dragwithouthandle="">
                <div class="row">
                    <div class="col-md-12">
                        <div class="spacer height-20"></div>
                    </div>
                </div>
                <div class="row align-items-center">
                    <div class="col-lg-6">
                        <div class="pr-lg-40">
                            <h2 class="color-accent2">Professional Team</h2>
                            <div class="spacer height-40" style="margin-bottom: -10px;"></div>
                            <p>Mauris vitae faucibus purus, nec scelerisque turpis. Cras sit amet elit vel est scelerisque consequat id sit amet ante. Morbi sit amet purus elit. Morbi suscipit eu nisi et fermentum.</p>
                            <div class="spacer height-20"></div>
                            ${getSnippetCode("infolist-02")}
                            <div class="spacer height-20" style="margin-bottom: -10px;"></div>
                            <a href="#" title="JOIN US NOW" class="button-14 bg-accent2">JOIN US NOW</a>
                            <div class="spacer height-40 d-lg-none"></div>
                        </div>
                    </div>
                    <div class="col-lg-6" style="margin-bottom: -30px;">
                     ${getSnippetCode("ourteam-04")}
                        
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="spacer height-20"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style class="build-css" data-class="button-14">${csstemplate["button-14"]}</style>

    `
    }, {
        'thumbnail': 'minis-section/saas/section-08.jpg',
        'category': '819',
        'html': `<div class="is-section saas-section09 anchorTag" id="Contact" data-title="Contact">
    <div class="is-boxes">
        <div class="is-light-text is-box layout-no-mt layout-no-mb is-box-6">
            <div class="is-overlay">
                <div class="is-overlay-bg" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/saas/saas-section10-leftBg.jpg&quot;);"></div>
            </div>
            <div class="is-boxes">
                <div class="is-box-centered">
                    <div class="is-container container-fluid" style="max-width: 654px;" dragwithouthandle="">
                        <div class="row saas-section09-info">
                            <div class="col-md-12 pr-lg-40">
                                <div class="spacer height-40"></div>
                                <h2 class="pb-5">Ready To Get Started? Contact Us!</h2>
                                <div class="spacer height-40" style="margin-bottom: -10px;"></div>
                                <p class="pr-lg-20">Aenean convallis tempor tempor. Morbi mollis tempus quam sed sodales. </p>
                                <div class="spacer height-20" style="margin-bottom: 2px;"></div>
                                <ul class="list-02 size-18">
                                    <li>
                                        <i class="sico lnr-phone-wave"><svg><use xlink:href="#lnr-phone-wave"></use></svg></i>(845) 359-7777</li>
                                    <li>
                                        <i class="sico lnr-envelope-open"><svg><use xlink:href="#lnr-envelope-open"></use></svg></i>service.simple@gmail.com</li>
                                    <li>
                                        <i class="sico lnr-map-marker"><svg><use xlink:href="#lnr-map-marker"></use></svg></i>252 NY-303, Orangeburg, NY 10965</li>
                                </ul>
                                <div class="spacer height-40"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="is-box layout-no-plr layout-no-mt layout-no-mb is-box-6">
            <div class="is-boxes">
                <div class="is-box-centered">
                    <div class="is-container container-fluid is-container-fluid" dragwithouthandle="">
                        <div class="row saas-section09-map">
                            <div class="col-md-12">
                                <div class="custom-module loading" data-effect="gmap" id="module-{id}" data-moduleid="{id}" data-settings="%7B%22address%22%3A%22740%20Prince%20Ave%2C%20Athens%2C%20GA%2030606%22%2C%22markerscolor%22%3A%22%23303030%22%2C%22markersbg%22%3A%22%23ffffff%22%2C%22position%22%3A%22right%22%2C%22markersicon%22%3A%22%5BSkinPath%5Dresource%2Fthumbnails%2Fmap%2Fmapicon01.png%22%2C%22zoom%22%3A%2216%22%2C%22type%22%3A%22roadmap%22%2C%22xl%22%3A%22769%22%2C%22l%22%3A%22500%22%2C%22m%22%3A%22400%22%2C%22s%22%3A%22300%22%2C%22xs%22%3A%22230%22%2C%22mapstyle%22%3A%22%5B%5Cn%20%20%20%20%7B%5Cn%20%20%20%20%20%20%20%20%5C%22featureType%5C%22%3A%20%5C%22landscape.natural%5C%22%2C%5Cn%20%20%20%20%20%20%20%20%5C%22stylers%5C%22%3A%20%5B%5Cn%20%20%20%20%20%20%20%20%20%20%20%20%7B%5Cn%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%5C%22color%5C%22%3A%20%5C%22%23bcddff%5C%22%5Cn%20%20%20%20%20%20%20%20%20%20%20%20%7D%5Cn%20%20%20%20%20%20%20%20%5D%5Cn%20%20%20%20%7D%2C%5Cn%20%20%20%20%7B%5Cn%20%20%20%20%20%20%20%20%5C%22featureType%5C%22%3A%20%5C%22road.highway%5C%22%2C%5Cn%20%20%20%20%20%20%20%20%5C%22elementType%5C%22%3A%20%5C%22geometry.fill%5C%22%2C%5Cn%20%20%20%20%20%20%20%20%5C%22stylers%5C%22%3A%20%5B%5Cn%20%20%20%20%20%20%20%20%20%20%20%20%7B%5Cn%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%5C%22color%5C%22%3A%20%5C%22%235fb3ff%5C%22%5Cn%20%20%20%20%20%20%20%20%20%20%20%20%7D%5Cn%20%20%20%20%20%20%20%20%5D%5Cn%20%20%20%20%7D%2C%5Cn%20%20%20%20%7B%5Cn%20%20%20%20%20%20%20%20%5C%22featureType%5C%22%3A%20%5C%22road.arterial%5C%22%2C%5Cn%20%20%20%20%20%20%20%20%5C%22stylers%5C%22%3A%20%5B%5Cn%20%20%20%20%20%20%20%20%20%20%20%20%7B%5Cn%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%5C%22color%5C%22%3A%20%5C%22%23ebf4ff%5C%22%5Cn%20%20%20%20%20%20%20%20%20%20%20%20%7D%5Cn%20%20%20%20%20%20%20%20%5D%5Cn%20%20%20%20%7D%2C%5Cn%20%20%20%20%7B%5Cn%20%20%20%20%20%20%20%20%5C%22featureType%5C%22%3A%20%5C%22road.local%5C%22%2C%5Cn%20%20%20%20%20%20%20%20%5C%22elementType%5C%22%3A%20%5C%22geometry.fill%5C%22%2C%5Cn%20%20%20%20%20%20%20%20%5C%22stylers%5C%22%3A%20%5B%5Cn%20%20%20%20%20%20%20%20%20%20%20%20%7B%5Cn%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%5C%22color%5C%22%3A%20%5C%22%23ebf4ff%5C%22%5Cn%20%20%20%20%20%20%20%20%20%20%20%20%7D%5Cn%20%20%20%20%20%20%20%20%5D%5Cn%20%20%20%20%7D%2C%5Cn%20%20%20%20%7B%5Cn%20%20%20%20%20%20%20%20%5C%22featureType%5C%22%3A%20%5C%22road.local%5C%22%2C%5Cn%20%20%20%20%20%20%20%20%5C%22elementType%5C%22%3A%20%5C%22geometry.stroke%5C%22%2C%5Cn%20%20%20%20%20%20%20%20%5C%22stylers%5C%22%3A%20%5B%5Cn%20%20%20%20%20%20%20%20%20%20%20%20%7B%5Cn%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%5C%22visibility%5C%22%3A%20%5C%22on%5C%22%5Cn%20%20%20%20%20%20%20%20%20%20%20%20%7D%2C%5Cn%20%20%20%20%20%20%20%20%20%20%20%20%7B%5Cn%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%5C%22color%5C%22%3A%20%5C%22%2393c8ff%5C%22%5Cn%20%20%20%20%20%20%20%20%20%20%20%20%7D%5Cn%20%20%20%20%20%20%20%20%5D%5Cn%20%20%20%20%7D%2C%5Cn%20%20%20%20%7B%5Cn%20%20%20%20%20%20%20%20%5C%22featureType%5C%22%3A%20%5C%22landscape.man_made%5C%22%2C%5Cn%20%20%20%20%20%20%20%20%5C%22elementType%5C%22%3A%20%5C%22geometry%5C%22%2C%5Cn%20%20%20%20%20%20%20%20%5C%22stylers%5C%22%3A%20%5B%5Cn%20%20%20%20%20%20%20%20%20%20%20%20%7B%5Cn%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%5C%22color%5C%22%3A%20%5C%22%23c7e2ff%5C%22%5Cn%20%20%20%20%20%20%20%20%20%20%20%20%7D%5Cn%20%20%20%20%20%20%20%20%5D%5Cn%20%20%20%20%7D%2C%5Cn%20%20%20%20%7B%5Cn%20%20%20%20%20%20%20%20%5C%22featureType%5C%22%3A%20%5C%22transit.station.airport%5C%22%2C%5Cn%20%20%20%20%20%20%20%20%5C%22elementType%5C%22%3A%20%5C%22geometry%5C%22%2C%5Cn%20%20%20%20%20%20%20%20%5C%22stylers%5C%22%3A%20%5B%5Cn%20%20%20%20%20%20%20%20%20%20%20%20%7B%5Cn%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%5C%22saturation%5C%22%3A%20100%5Cn%20%20%20%20%20%20%20%20%20%20%20%20%7D%2C%5Cn%20%20%20%20%20%20%20%20%20%20%20%20%7B%5Cn%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%5C%22gamma%5C%22%3A%200.82%5Cn%20%20%20%20%20%20%20%20%20%20%20%20%7D%2C%5Cn%20%20%20%20%20%20%20%20%20%20%20%20%7B%5Cn%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%5C%22hue%5C%22%3A%20%5C%22%230088ff%5C%22%5Cn%20%20%20%20%20%20%20%20%20%20%20%20%7D%5Cn%20%20%20%20%20%20%20%20%5D%5Cn%20%20%20%20%7D%2C%5Cn%20%20%20%20%7B%5Cn%20%20%20%20%20%20%20%20%5C%22elementType%5C%22%3A%20%5C%22labels.text.fill%5C%22%2C%5Cn%20%20%20%20%20%20%20%20%5C%22stylers%5C%22%3A%20%5B%5Cn%20%20%20%20%20%20%20%20%20%20%20%20%7B%5Cn%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%5C%22color%5C%22%3A%20%5C%22%231673cb%5C%22%5Cn%20%20%20%20%20%20%20%20%20%20%20%20%7D%5Cn%20%20%20%20%20%20%20%20%5D%5Cn%20%20%20%20%7D%2C%5Cn%20%20%20%20%7B%5Cn%20%20%20%20%20%20%20%20%5C%22featureType%5C%22%3A%20%5C%22road.highway%5C%22%2C%5Cn%20%20%20%20%20%20%20%20%5C%22elementType%5C%22%3A%20%5C%22labels.icon%5C%22%2C%5Cn%20%20%20%20%20%20%20%20%5C%22stylers%5C%22%3A%20%5B%5Cn%20%20%20%20%20%20%20%20%20%20%20%20%7B%5Cn%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%5C%22saturation%5C%22%3A%2058%5Cn%20%20%20%20%20%20%20%20%20%20%20%20%7D%2C%5Cn%20%20%20%20%20%20%20%20%20%20%20%20%7B%5Cn%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%5C%22hue%5C%22%3A%20%5C%22%23006eff%5C%22%5Cn%20%20%20%20%20%20%20%20%20%20%20%20%7D%5Cn%20%20%20%20%20%20%20%20%5D%5Cn%20%20%20%20%7D%2C%5Cn%20%20%20%20%7B%5Cn%20%20%20%20%20%20%20%20%5C%22featureType%5C%22%3A%20%5C%22poi%5C%22%2C%5Cn%20%20%20%20%20%20%20%20%5C%22elementType%5C%22%3A%20%5C%22geometry%5C%22%2C%5Cn%20%20%20%20%20%20%20%20%5C%22stylers%5C%22%3A%20%5B%5Cn%20%20%20%20%20%20%20%20%20%20%20%20%7B%5Cn%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%5C%22color%5C%22%3A%20%5C%22%234797e0%5C%22%5Cn%20%20%20%20%20%20%20%20%20%20%20%20%7D%5Cn%20%20%20%20%20%20%20%20%5D%5Cn%20%20%20%20%7D%2C%5Cn%20%20%20%20%7B%5Cn%20%20%20%20%20%20%20%20%5C%22featureType%5C%22%3A%20%5C%22poi.park%5C%22%2C%5Cn%20%20%20%20%20%20%20%20%5C%22elementType%5C%22%3A%20%5C%22geometry%5C%22%2C%5Cn%20%20%20%20%20%20%20%20%5C%22stylers%5C%22%3A%20%5B%5Cn%20%20%20%20%20%20%20%20%20%20%20%20%7B%5Cn%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%5C%22color%5C%22%3A%20%5C%22%23209ee1%5C%22%5Cn%20%20%20%20%20%20%20%20%20%20%20%20%7D%2C%5Cn%20%20%20%20%20%20%20%20%20%20%20%20%7B%5Cn%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%5C%22lightness%5C%22%3A%2049%5Cn%20%20%20%20%20%20%20%20%20%20%20%20%7D%5Cn%20%20%20%20%20%20%20%20%5D%5Cn%20%20%20%20%7D%2C%5Cn%20%20%20%20%7B%5Cn%20%20%20%20%20%20%20%20%5C%22featureType%5C%22%3A%20%5C%22transit.line%5C%22%2C%5Cn%20%20%20%20%20%20%20%20%5C%22elementType%5C%22%3A%20%5C%22geometry.fill%5C%22%2C%5Cn%20%20%20%20%20%20%20%20%5C%22stylers%5C%22%3A%20%5B%5Cn%20%20%20%20%20%20%20%20%20%20%20%20%7B%5Cn%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%5C%22color%5C%22%3A%20%5C%22%2383befc%5C%22%5Cn%20%20%20%20%20%20%20%20%20%20%20%20%7D%5Cn%20%20%20%20%20%20%20%20%5D%5Cn%20%20%20%20%7D%2C%5Cn%20%20%20%20%7B%5Cn%20%20%20%20%20%20%20%20%5C%22featureType%5C%22%3A%20%5C%22road.highway%5C%22%2C%5Cn%20%20%20%20%20%20%20%20%5C%22elementType%5C%22%3A%20%5C%22geometry.stroke%5C%22%2C%5Cn%20%20%20%20%20%20%20%20%5C%22stylers%5C%22%3A%20%5B%5Cn%20%20%20%20%20%20%20%20%20%20%20%20%7B%5Cn%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%5C%22color%5C%22%3A%20%5C%22%233ea3ff%5C%22%5Cn%20%20%20%20%20%20%20%20%20%20%20%20%7D%5Cn%20%20%20%20%20%20%20%20%5D%5Cn%20%20%20%20%7D%2C%5Cn%20%20%20%20%7B%5Cn%20%20%20%20%20%20%20%20%5C%22featureType%5C%22%3A%20%5C%22administrative%5C%22%2C%5Cn%20%20%20%20%20%20%20%20%5C%22elementType%5C%22%3A%20%5C%22geometry.stroke%5C%22%2C%5Cn%20%20%20%20%20%20%20%20%5C%22stylers%5C%22%3A%20%5B%5Cn%20%20%20%20%20%20%20%20%20%20%20%20%7B%5Cn%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%5C%22saturation%5C%22%3A%2086%5Cn%20%20%20%20%20%20%20%20%20%20%20%20%7D%2C%5Cn%20%20%20%20%20%20%20%20%20%20%20%20%7B%5Cn%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%5C%22hue%5C%22%3A%20%5C%22%230077ff%5C%22%5Cn%20%20%20%20%20%20%20%20%20%20%20%20%7D%2C%5Cn%20%20%20%20%20%20%20%20%20%20%20%20%7B%5Cn%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%5C%22weight%5C%22%3A%200.8%5Cn%20%20%20%20%20%20%20%20%20%20%20%20%7D%5Cn%20%20%20%20%20%20%20%20%5D%5Cn%20%20%20%20%7D%2C%5Cn%20%20%20%20%7B%5Cn%20%20%20%20%20%20%20%20%5C%22elementType%5C%22%3A%20%5C%22labels.icon%5C%22%2C%5Cn%20%20%20%20%20%20%20%20%5C%22stylers%5C%22%3A%20%5B%5Cn%20%20%20%20%20%20%20%20%20%20%20%20%7B%5Cn%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%5C%22hue%5C%22%3A%20%5C%22%230066ff%5C%22%5Cn%20%20%20%20%20%20%20%20%20%20%20%20%7D%2C%5Cn%20%20%20%20%20%20%20%20%20%20%20%20%7B%5Cn%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%5C%22weight%5C%22%3A%201.9%5Cn%20%20%20%20%20%20%20%20%20%20%20%20%7D%5Cn%20%20%20%20%20%20%20%20%5D%5Cn%20%20%20%20%7D%2C%5Cn%20%20%20%20%7B%5Cn%20%20%20%20%20%20%20%20%5C%22featureType%5C%22%3A%20%5C%22poi%5C%22%2C%5Cn%20%20%20%20%20%20%20%20%5C%22elementType%5C%22%3A%20%5C%22geometry.fill%5C%22%2C%5Cn%20%20%20%20%20%20%20%20%5C%22stylers%5C%22%3A%20%5B%5Cn%20%20%20%20%20%20%20%20%20%20%20%20%7B%5Cn%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%5C%22hue%5C%22%3A%20%5C%22%230077ff%5C%22%5Cn%20%20%20%20%20%20%20%20%20%20%20%20%7D%2C%5Cn%20%20%20%20%20%20%20%20%20%20%20%20%7B%5Cn%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%5C%22saturation%5C%22%3A%20-7%5Cn%20%20%20%20%20%20%20%20%20%20%20%20%7D%2C%5Cn%20%20%20%20%20%20%20%20%20%20%20%20%7B%5Cn%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%5C%22lightness%5C%22%3A%2024%5Cn%20%20%20%20%20%20%20%20%20%20%20%20%7D%5Cn%20%20%20%20%20%20%20%20%5D%5Cn%20%20%20%20%7D%5Cn%5D%22%7D"></div>
                            </div>
                        </div>
                        <div class="row saas-section09-from">
                            <div class="col-md-12">
                                <h4>Email Us</h4>
                                <div class="spacer height-20" style="margin-bottom: -17px;"></div>
                                <p style="margin-bottom: 6px;">Pellentesque in maximus leo, eget ultricies arcu. Nam ut nunc sapien. </p>
                                ${getSnippetCode("ajaxform-15")}
                                <div class="spacer height-20"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style class="build-css" data-class="list-02">${csstemplate["list-02"]}</style>
<style class="build-css" data-class="saas-section09">
	.saas-section09 .saas-section09-info {
		max-width: calc(50vw - 430px/2 - (50vw - 450px) / 2);
	}

	.saas-section09 .saas-section09-map {
		padding-top: 0 !important;
		padding-bottom: 0 !important;
	}

	.saas-section09 .saas-section09-from {
		position: absolute;
		top: 50%;
		left: 0;
		transform: translate3d(-50%, -50%, 0);
		background:
			#fff;
		margin-left: 0;
		margin-right: 0;
		padding-left: 35px;
		padding-right: 35px;
		padding-bottom: 20px !important;
		padding-top:
			36px !important;
		max-width: 430px;
		border-radius: 3px;
		-webkit-box-shadow: 0px 0px 25px rgba(0, 0, 0, 0.1);
		box-shadow: 0px 0px 25px rgba(0, 0, 0, 0.1);
	}

	.saas-section09 .saas-section09-from .ajaxform-06 .btn {
		background:
			var(--accent-color2);
	}

	.saas-section09 .saas-section09-from .ajaxform-06 .btn:hover {
		background:
			var(--accent-color);
	}

	@media (max-width: 1199px) {
		.saas-section09 .saas-section09-info {
			max-width:
				initial;
		}

		.saas-section09 .saas-section09-from {
			position: relative;
			top: 0;
			transform: none;
			max-width: 100%;
		}
	}
</style>
    `
    }, {
        'thumbnail': 'minis-section/saas/section-09.jpg',
        'category': '819',
        'html': `<div class="is-section is-box saas-section10">
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container" dragwithouthandle="">
                <div class="row">
                    <div class="col-md-12 text-center m-auto" style="max-width: 770px;">
                        <h2 class="color-accent2">Frequently Asked Questions</h2>
                        <div class="spacer height-20"></div>
                        <p style="font-size:18px;">Quisque ut ipsum eget quam posuere sodales. Donec sagittis faucibus justo, sed laoreet risus posuere posuere. Integer ultricies lacus sit amet.</p>
                        <div class="spacer height-40" style="margin-bottom: -2px"></div>
                    </div>
                </div>
                <div class="row accordion04-color-accent2" style="margin-bottom: -10px;">
                    <div class="col-lg-6">
                        <div class="custom-module loading" id="module-{id}" data-moduleid="{id}" data-effect="Accordions" data-style="accordion04" data-settings="%7B%22items%22%3A%7B%22item0%22%3A%7B%22title%22%3A%22What%20aftersales%20support%20do%20you%20offer%3F%22%2C%22description%22%3A%22Aliquam%20erat%20volutpat.%20Suspendisse%20sollicitudin%20enim%20ac%20turpis%20imperdiet%2C%20aliquet%20sollicitudin%20metus%20sagittis.%20Nulla%20in%20nunc%20egestas%2C%20blandit%20quam%20sed%2C%20vestibulum%20elit.%20%22%7D%2C%22item1%22%3A%7B%22title%22%3A%22What%20services%20do%20you%20provide%3F%22%2C%22description%22%3A%22Aliquam%20erat%20volutpat.%20Suspendisse%20sollicitudin%20enim%20ac%20turpis%20imperdiet%2C%20aliquet%20sollicitudin%20metus%20sagittis.%20Nulla%20in%20nunc%20egestas%2C%20blandit%20quam%20sed%2C%20vestibulum%20elit.%20%22%7D%2C%22item2%22%3A%7B%22title%22%3A%22Can%20you%20help%20me%20change%20the%20color%3F%22%2C%22description%22%3A%22Aliquam%20erat%20volutpat.%20Suspendisse%20sollicitudin%20enim%20ac%20turpis%20imperdiet%2C%20aliquet%20sollicitudin%20metus%20sagittis.%20Nulla%20in%20nunc%20egestas%2C%20blandit%20quam%20sed%2C%20vestibulum%20elit.%20%22%7D%2C%22item3%22%3A%7B%22title%22%3A%22Is%20there%20a%20child%20theme%20included%20in%20this%20theme%3F%22%2C%22description%22%3A%22Aliquam%20erat%20volutpat.%20Suspendisse%20sollicitudin%20enim%20ac%20turpis%20imperdiet%2C%20aliquet%20sollicitudin%20metus%20sagittis.%20Nulla%20in%20nunc%20egestas%2C%20blandit%20quam%20sed%2C%20vestibulum%20elit.%20%22%7D%7D%7D"></div>
                    </div>
                    <div class="col-lg-6">
                        <div class="custom-module loading" id="module-{id}" data-moduleid="{id}" data-effect="Accordions" data-style="accordion04" data-settings="%7B%22items%22%3A%7B%22item0%22%3A%7B%22title%22%3A%22The%20card%20attached%20would%20say%20thank%20you%3F%22%2C%22description%22%3A%22Aliquam%20erat%20volutpat.%20Suspendisse%20sollicitudin%20enim%20ac%20turpis%20imperdiet%2C%20aliquet%20sollicitudin%20metus%20sagittis.%20Nulla%20in%20nunc%20egestas%2C%20blandit%20quam%20sed%2C%20vestibulum%20elit.%20%22%7D%2C%22item1%22%3A%7B%22title%22%3A%22Can%20I%20get%20a%20discount%20for%20this%20theme%3F%22%2C%22description%22%3A%22Aliquam%20erat%20volutpat.%20Suspendisse%20sollicitudin%20enim%20ac%20turpis%20imperdiet%2C%20aliquet%20sollicitudin%20metus%20sagittis.%20Nulla%20in%20nunc%20egestas%2C%20blandit%20quam%20sed%2C%20vestibulum%20elit.%20%22%7D%2C%22item2%22%3A%7B%22title%22%3A%22What%20should%20I%20do%20to%20create%20a%20website%3F%22%2C%22description%22%3A%22Aliquam%20erat%20volutpat.%20Suspendisse%20sollicitudin%20enim%20ac%20turpis%20imperdiet%2C%20aliquet%20sollicitudin%20metus%20sagittis.%20Nulla%20in%20nunc%20egestas%2C%20blandit%20quam%20sed%2C%20vestibulum%20elit.%20%22%7D%2C%22item3%22%3A%7B%22title%22%3A%22I%20forgot%20my%20password%2C%20how%20do%20I%20reset%20it%3F%22%2C%22description%22%3A%22Aliquam%20erat%20volutpat.%20Suspendisse%20sollicitudin%20enim%20ac%20turpis%20imperdiet%2C%20aliquet%20sollicitudin%20metus%20sagittis.%20Nulla%20in%20nunc%20egestas%2C%20blandit%20quam%20sed%2C%20vestibulum%20elit.%20%22%7D%7D%7D"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style class="build-css" data-class="accordion04-color-accent2">.accordion04-color-accent2 .accordion04 .panel-heading .arrow::after, .accordion04-color-accent2 .accordion04 .panel-heading .arrow::before, .accordion04-color-accent2 .accordion04 .accordion-item.opened{background-color:var(--accent-color2); } 
    </style>
</div>
    `
    }, {
        'thumbnail': 'minis-section/saas/footer.jpg',
        'category': '819',
        'html': `<div class="is-section is-box is-light-text saas-footer">
    <div class="is-overlay">
        <div class="is-overlay-bg" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/saas/saas-footer-bg.jpg&quot;);"></div>
    </div>
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container mb-md-50" dragwithouthandle="">
                <div class="row size-16">
                    <div class="col-md-4">
                        <img src="/Portals/_default/ContentBuilder/minis-page/logo-white2.png" alt="">
                        <div class="spacer height-40"></div>
                        <p class="mb-25">Aliquam efficitur, nisl tempor imperdiet aliquam, neque sem mattis est.</p>
                        <div class="edit-box">
                            <a class="social-03" href="#">
                                <i class="sico fab-facebook-f"><svg><use xlink:href="#fab-facebook-f"></use></svg></i>
                            </a>
                            <a class="social-03" href="#">
                                <i class="sico fab-linkedin-in"><svg><use xlink:href="#fab-linkedin-in"></use></svg></i>
                            </a>
                            <a class="social-03" href="#">
                                <i class="sico fab-twitter"><svg><use xlink:href="#fab-twitter"></use></svg></i>
                            </a>
                        </div>
                        <div class="spacer height-20" style="margin-bottom: -11px"></div>
                        <p>@ 2019 by DNNGo Corp.</p>
                    </div>
                    <div class="col-md-4">
                        <div class="row saas-footer-linklist">
                            <div class="col-sm-6">
                                <p>
                                    <a href="#" title="About Us">About Us</a>
                                </p>
                                <p>
                                    <a href="#" title="Contact Us">Contact Us</a>
                                </p>
                                <p>
                                    <a href="#" title="Term &amp; Conditions">Term &amp; Conditions</a>
                                </p>
                                <p>
                                    <a href="#" title="Task Management">Task Management</a>
                                </p>
                                <p>
                                    <a href="#" title="FAQs">FAQs</a>
                                </p>
                                <p>
                                    <a href="#" title="Project Management">Project Management</a>
                                </p>
                            </div>
                            <div class="col-sm-6">
                                <p>
                                    <a href="#" title="Blogs">Blogs</a>
                                </p>
                                <p>
                                    <a href="#" title="Portfolios">Portfolios</a>
                                </p>
                                <p>
                                    <a href="#" title="Online Documentation">Online Documentation</a>
                                </p>
                                <p>
                                    <a href="#" title="Elements">Elements</a>
                                </p>
                                <p>
                                    <a href="#" title="Live Demo">Live Demo</a>
                                </p>
                                <p>
                                    <a href="#" title="Help Center">Help Center</a>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <h4>Are you ready?</h4>
                        <div class="spacer height-20" style="margin-bottom: -7px;"></div>
                        <p>Praesent turpis nibh, fringilla at ornare ac, consectetur in ante. Duis at urna rutrum sapien interdum pulvinar .</p>
                        <div class="spacer height-20" style="margin-bottom: -2px;"></div>
                        <a href="#" title="FREE TRIAL" class="button-14 bg-accent3">FREE TRIAL</a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="spacer height-40"></div>
                        <hr style="border-bottom: 1px solid rgba(255, 255, 255, 0.5); margin: 0 0 5px;">
                    </div>
                </div>
                <div class="row pt-50">
                    <div class="col-6 col-md-4 col-lg-2">
                        <div class="mt-15 mb-15 text-center">
                            <span>
                                <a href="#" title="">
                                    <img src="/Portals/_default/ContentBuilder/minis-page/saas/saas-footer-img01.png" class="img-Lazy" alt="logo"></a>
                            </span>
                        </div>
                    </div>
                    <div class="col-6 col-md-4 col-lg-2">
                        <div class="mt-15 mb-15 text-center">
                            <span>
                                <a href="#" title="">
                                    <img src="/Portals/_default/ContentBuilder/minis-page/saas/saas-footer-img02.png" class="img-Lazy" alt="logo"></a>
                            </span>
                        </div>
                    </div>
                    <div class="col-6 col-md-4 col-lg-2">
                        <div class="mt-15 mb-15 text-center">
                            <span>
                                <a href="#" title="">
                                    <img src="/Portals/_default/ContentBuilder/minis-page/saas/saas-footer-img03.png" class="img-Lazy" alt="logo"></a>
                            </span>
                        </div>
                    </div>
                    <div class="col-6 col-md-4 col-lg-2">
                        <div class="mt-15 mb-15 text-center">
                            <span>
                                <a href="#" title="">
                                    <img src="/Portals/_default/ContentBuilder/minis-page/saas/saas-footer-img04.png" class="img-Lazy" alt="logo"></a>
                            </span>
                        </div>
                    </div>
                    <div class="col-6 col-md-4 col-lg-2">
                        <div class="mt-15 mb-15 text-center">
                            <span>
                                <a href="#" title="">
                                    <img src="/Portals/_default/ContentBuilder/minis-page/saas/saas-footer-img05.png" class="img-Lazy" alt="logo"></a>
                            </span>
                        </div>
                    </div>
                    <div class="col-6 col-md-4 col-lg-2">
                        <div class="mt-15 mb-15 text-center">
                            <span>
                                <a href="#" title="">
                                    <img src="/Portals/_default/ContentBuilder/minis-page/saas/saas-footer-img06.png" class="img-Lazy" alt="logo"></a>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style class="build-css" data-class="saas-footer-linklist">.saas-footer-linklist a,.saas-footer-linklist a:link {color: currentColor;}.saas-footer-linklist a:hover {color:var(--accent-color);}.saas-footer-linklist p {margin-bottom: 20px;} </style>
<style class="build-css" data-class="social-03">${csstemplate["social-03"]}</style>
<style class="build-css" data-class="button-14">${csstemplate["button-14"]}</style>

    `
    }

)
/*Design Studio*/

data_basic.designs.push({
        'thumbnail': 'minis-section/design-studio/banner.jpg',
        'category': '820',
        'html': `<div class="is-section is-box is-light-text is-section-auto">
    <div class="is-overlay">
        <div class="is-overlay-bg" style="background-image:url(&quot;/Portals/_default/ContentBuilder/minis-page/design-studio/designStudio-banner.jpg&quot;)"></div>
    </div>
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container position-static" dragwithouthandle="">
                <div class="row">
                    <div class="col-md-12" data-noedit="">
                        <div class="spacer height-260"></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="size-72 color-accent3" style="font-weight: 700;">Imagination</h1>
                        <h2 class="size-50">Is The Source Of Creation</h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12" data-noedit="">
                        <div class="spacer height-260"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    `
    }, {
        'thumbnail': 'minis-section/design-studio/section-01.jpg',
        'category': '820',
        'html': `<div class="is-section is-box">
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container" dragwithouthandle="">
                <div class="row">
                    <div class="col-md-12" data-noedit="">
                        <div class="spacer height-20"></div>
                    </div>
                </div>
                <div class="row align-items-center">
                    <div class="col-md-6 left-full-column">
                        <div class="full-column-inner text-center text-md-right">
                            <img src="/Portals/_default/ContentBuilder/minis-page/design-studio/designStudio-img01.jpg" class="img-Lazy" alt=""></div>
                        <h6 class="size-24" style="max-width: 335px; position: absolute; font-weight: 700; top: 76%; right: 0%;">30 + Years Of Experience On Designing</h6>
                    </div>
                    <div class="col-md-6 pl-lg-65">
                        <div class="spacer pt-30 d-md-none"></div>
                        <div class="title-22 pr-lg-80">
                            <small class="bg-accent2">ABOUT US</small>
                            <h3>We Are A professional Team Who Are Full Of Passion.</h3>
                        </div>
                        <u class="color-accent2 size-18" style="font-weight: 700;">The heart and soul of a company is innovation.</u>
                        <div class="spacer height-20 mb-3"></div>
                        <p>Duis semper nisl ex, a euismod nulla egestas eget. Praesent cursus volutpat dui nec consequat. Duis ut felis dolor. Nulla facilisi. Nullam ac diam at leo volutpat aliquet id eget libero.</p>
                        <div class="spacer height-20 mb-5"></div>
                        <a href="#" title="LEARN MORE" class="button-01 bg-accent2">LEARN MORE</a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12" data-noedit="">
                        <div class="spacer height-20"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style class="build-css" data-class="button-01">${csstemplate["button-01"]}</style>
<style class="build-css" data-class="title-22">${csstemplate["title-22"]}</style>
    `
    }, {
        'thumbnail': 'minis-section/design-studio/section-02.jpg',
        'category': '820',
        'html': `<div class="is-section is-box is-light-text is-align-center">
    <div class="is-overlay">
        <div class="is-overlay-bg" style="background-image:url(&quot;/Portals/_default/ContentBuilder/minis-page/design-studio/designStudio-bg01.jpg&quot;)"></div>
    </div>
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container" dragwithouthandle="">
                <div class="row">
                    <div class="col-md-12">
                        <h3 class="mt-10" style="max-width: 635px; display: inline-block; font-weight:600;">We Promise That The Service We Provide For You Is All-round</h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12" data-noedit="">
                        <div class="spacer height-20 mb-10"></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p style="display: inline-block; max-width: 768px;">Praesent et nisi sed ante sodales tempus consequat sed magna. Donec id turpis lorem. Aliquam mollis efficitur mauris, at tincidunt nunc ultricies accumsan.</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12" data-noedit="">
                        <div style="height:160px" class="mt-80 mt-lg-40"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><div class="is-section is-box layout-no-mb layout-no-mt" style="margin-top: -270px;">
<div class="is-boxes no-min-height">
    <div class="is-box-centered">
        <div class="is-container layout-container" dragwithouthandle="" >
        ${getSnippetCode("flipbox-02")}
        </div>
    </div>
</div>
 
</div>
    `
    }, {
        'thumbnail': 'minis-section/design-studio/section-03.jpg',
        'category': '820',
        'html': `<div class="is-section is-box">
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container" dragwithouthandle="">
                <div class="row align-items-center">
                    <div class="col-md-12">
                        <div class="title-22 text-center mt-5 ml-auto mr-auto" style="max-width: 640px;">
                            <small class="bg-accent2">OUR STRENGTH</small>
                            <h3>The Aim Of Our Team Is To Provide Clients The Best Products &amp; Services.</h3>
                        </div>
                    </div>
                </div>
                <div class="row row row row align-items-center">
                    <div class="col-md-3">
                        <div class="counter-10">
                            <div class="icon">
                                <img src="/Portals/_default/ContentBuilder/minis-page/design-studio/designStudio-icon01.jpg" class="img-Lazy" alt=""></div>
                            <div class="cont">
                                <div class="custom-module loading" id="module-{id}" data-moduleid="{id}" data-effect="Number" data-settings="%7B%22number%22%3A%223256%22%7D"></div>
                                <h6 class="title">Successful Projects</h6>
                            </div>
                        </div>
                        <div class="counter-10">
                            <div class="icon">
                                <img src="/Portals/_default/ContentBuilder/minis-page/design-studio/designStudio-icon02.jpg" class="img-Lazy" alt=""></div>
                            <div class="cont">
                                <div class="custom-module loading" id="module-{id}" data-moduleid="{id}" data-effect="Number" data-settings="%7B%22number%22%3A%22450%22%7D"></div>
                                <h6 class="title">Awards Winning</h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6" style="text-align: center;">
                        <div class="spacer height-40 d-md-none"></div>
                        <img src="/Portals/_default/ContentBuilder/minis-page/design-studio/designStudio-img06.png" class="img-Lazy" alt="">
                    </div>
                    <div class="col-md-3">
                        <div class="counter-10">
                            <div class="icon">
                                <img src="/Portals/_default/ContentBuilder/minis-page/design-studio/designStudio-icon03.jpg" class="img-Lazy" alt=""></div>
                            <div class="cont">
                                <div class="custom-module loading" id="module-{id}" data-moduleid="{id}" data-effect="Number" data-settings="%7B%22number%22%3A%224260%22%7D"></div>
                                <h6 class="title">Wonderful Praises</h6>
                            </div>
                        </div>
                        <div class="counter-10">
                            <div class="icon">
                                <img src="/Portals/_default/ContentBuilder/minis-page/design-studio/designStudio-icon04.jpg" class="img-Lazy" alt=""></div>
                            <div class="cont">
                                <div class="custom-module loading" id="module-{id}" data-moduleid="{id}" data-effect="Number" data-settings="%7B%22number%22%3A%224500%22%7D"></div>
                                <h6 class="title">Satisfied Clients</h6>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12" data-noedit="">
                        <div class="spacer height-20"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style class="build-css" data-class="title-22">${csstemplate["title-22"]}</style>
<style class="build-css" data-class="counter-10">${csstemplate["counter-10"]}</style>
    `
    }, {
        'thumbnail': 'minis-section/design-studio/section-04.jpg',
        'category': '820',
        'html': `<div class="is-section is-shadow-1 is-section-auto designStudio-section05">
    <div class="is-boxes">
        <div class="is-box-6 is-light-text is-box is-align-center">
            <div class="is-overlay">
                <div class="is-overlay-bg" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/design-studio/designStudio-img07.jpg&quot;); background-position: 50% 60%;"></div>
                <div class="is-overlay-content"></div>
            </div>
            <div class="is-boxes">
                <div class="is-box-centered">
                    <div class="is-container container-fluid" style="max-width: 535px;" dragwithouthandle="">
                        <div class="row">
                            <div class="col-md-12">
                                <a class="play-button2 is-lightbox bg-accent3 m-auto" href="https://www.youtube.com/embed/AkcUoA2f-jU" data-ilightbox="youtube" title="" style="position: relative; top: 0px; left: 0px;">
                                    <span class="icon"></span>
                                </a>
                                <div class="spacer height-60 mb-10"></div>
                                <h3 style="font-weight: 100;">Break The Inherent Thinking
                                    <span style="font-weight: 600;"> Constantly Looking For New Ideas</span> And Methods.</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="is-box-6 is-box" style="background-color: rgb(255, 241, 233);">
            <div class="is-boxes">
                <div class="is-box-centered pr-50">
                    <div class="is-container container-fluid ml-xl-70 ml-lg-30" style="max-width: 530px;" dragwithouthandle="">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="title-22 mt-10">
                                    <small class="bg-accent2">OUR SKILLS</small>
                                    <h3>Use Superb Skills To Design You A Stylish Site</h3>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                               ${getSnippetCode("progressbar03")}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style class="build-css" data-class="title-22">${csstemplate["title-22"]}</style>
    <style class="build-css" data-class="play-button2">${csstemplate["play-button2"]}</style>

    <style class="build-css" data-class="designStudio-section05">@media (min-width:992px) {.designStudio-section05{ padding-top: 60px; } .designStudio-section05 .is-boxes > .is-box:first-child{ -webkit-transform: translateY(-60px); transform: translateY(-60px); z-index: 2; } .designStudio-section05 .is-boxes > .is-box:last-child::after{ content: ""; height: 100%; position: absolute; left: -285px; right: 100%; top: 0; background: inherit; }}</style>
</div>
    `
    }, {
        'thumbnail': 'minis-section/design-studio/section-05.jpg',
        'category': '820',
        'html': `<div class="is-section is-box">
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container" dragwithouthandle="">
                <div class="row">
                    <div class="col-md-12" data-noedit="">
                        <div class="spacer height-20"></div>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-md-12">
                        <div class="title-22 text-center ml-auto mr-auto" style="max-width: 570px;">
                            <small class="bg-accent2">OUR PROCESS</small>
                            <h3>Are You Wondering How We Make All These Original Ideas Into Reality?</h3>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-md-10">
                    ${getSnippetCode("step02")}  
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12" data-noedit="">
                        <div class="spacer height-20"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style class="build-css" data-class="title-22">${csstemplate["title-22"]}</style>
    `
    }, {
        'thumbnail': 'minis-section/design-studio/section-06.jpg',
        'category': '820',
        'html': `<div class="is-section is-box is-align-center is-light-text layout-no-mb">
    <div class="is-overlay">
        <div class="is-overlay-bg" style="background-image:url(&quot;/Portals/_default/ContentBuilder/minis-page/design-studio/designStudio-bg03.jpg&quot;)"></div>
    </div>
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container" dragwithouthandle="">
                <div class="row">
                    <div class="col-md-12">
                        <h3>Though Different People Have Different Views</h3>
                        <h3 style="font-weight: 300;" class="mb-20">They All Think Our Product Rocks</h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                    ${getSnippetCode("hotspot01")}  
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12" data-noedit="">
                        <div class="spacer height-20 mt-10"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    `
    }, {
        'thumbnail': 'minis-section/design-studio/section-07.jpg',
        'category': '820',
        'html': `<div class="is-section is-box">
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container" dragwithouthandle="">
                <div class="row">
                    <div class="col-md-12" data-noedit="">
                        <div class="spacer height-20"></div>
                    </div>
                </div>
                <div class="row align-items-center pb-10">
                    <div class="col-md-6 pr-lg-60">
                        <div class="title-22 pr-lg-80">
                            <small class="bg-accent2">OUR TEAM</small>
                            <h3>We Work Day And Night To Deliver Our Clients With The Best Projects.</h3>
                        </div>
                        <p>Duis semper nisl ex, a euismod nulla egestas eget. Praesent cursus volutpat dui nec consequat. Duis ut felis dolor. Nulla facilisi. Nullam ac diam at leo volutpat aliquet id eget libero.</p>
                        <div class="spacer height-20 mb-5"></div>
                        <a href="#" title="CONTACT US" class="button-01 bg-accent3">JOIN US NOW</a>
                        <div class="spacer height-40 d-md-none"></div>
                    </div>
                    <div class="col-md-6 right-full-column pr-md-0">
                        <div class="full-column-inner">
                            <div class="custom-module loading" id="module-{id}" data-moduleid="{id}" data-effect="carousel-leftzoomout" data-style="carousel-leftzoomout01" data-settings="%7B%22size%22%3A%22large%22%2C%22items%22%3A%7B%22item0%22%3A%7B%22title%22%3A%22Ettle%20Halsey%22%2C%22job%22%3A%22Manager%22%2C%22single%22%3A%22%2FPortals%2F_default%2FContentBuilder%2Fminis-page%2Fdesign-studio%2FdesignStudio-img08.jpg%22%2C%22singlewidth%22%3A%22370%22%2C%22singleheight%22%3A%22491%22%2C%22icon%22%3A%22sico%20fab-twitter%22%2C%22iconcolor%22%3A%22%231da1f2%22%2C%22iconlink%22%3A%22%23%22%2C%22icon2%22%3A%22sico%20fab-pinterest-p%22%2C%22iconcolor2%22%3A%22%23e60124%22%2C%22iconlink2%22%3A%22%23%22%2C%22icon3%22%3A%22sico%20fab-linkedin-in%22%2C%22iconcolor3%22%3A%22%230073b0%22%2C%22iconlink3%22%3A%22%23%22%7D%2C%22item1%22%3A%7B%22title%22%3A%22Alice%20Blanche%22%2C%22job%22%3A%22Visual%20Designer%22%2C%22single%22%3A%22%2FPortals%2F_default%2FContentBuilder%2Fminis-page%2Fdesign-studio%2FdesignStudio-img09.jpg%22%2C%22singlewidth%22%3A%22370%22%2C%22singleheight%22%3A%22491%22%2C%22icon%22%3A%22sico%20fab-twitter%22%2C%22iconcolor%22%3A%22%231da1f2%22%2C%22iconlink%22%3A%22%23%22%2C%22icon2%22%3A%22sico%20fab-pinterest-p%22%2C%22iconcolor2%22%3A%22%23e60124%22%2C%22iconlink2%22%3A%22%23%22%2C%22icon3%22%3A%22sico%20fab-linkedin-in%22%2C%22iconcolor3%22%3A%22%230073b0%22%2C%22iconlink3%22%3A%22%23%22%7D%2C%22item2%22%3A%7B%22title%22%3A%22Wendy%20William%22%2C%22job%22%3A%22Developer%20Programmer%22%2C%22single%22%3A%22%2FPortals%2F_default%2FContentBuilder%2Fminis-page%2Fdesign-studio%2FdesignStudio-img10.jpg%22%2C%22singlewidth%22%3A%22370%22%2C%22singleheight%22%3A%22491%22%2C%22icon%22%3A%22sico%20fab-twitter%22%2C%22iconcolor%22%3A%22%231da1f2%22%2C%22iconlink%22%3A%22%23%22%2C%22icon2%22%3A%22sico%20fab-pinterest-p%22%2C%22iconcolor2%22%3A%22%23e60124%22%2C%22iconlink2%22%3A%22%23%22%2C%22icon3%22%3A%22sico%20fab-linkedin-in%22%2C%22iconcolor3%22%3A%22%230073b0%22%2C%22iconlink3%22%3A%22%23%22%7D%2C%22item3%22%3A%7B%22title%22%3A%22Julie%20Ann%22%2C%22job%22%3A%22Front-end%20Engineer%22%2C%22single%22%3A%22%2FPortals%2F_default%2FContentBuilder%2Fminis-page%2Fdesign-studio%2FdesignStudio-img11.jpg%22%2C%22singlewidth%22%3A%22370%22%2C%22singleheight%22%3A%22491%22%2C%22icon%22%3A%22sico%20fab-twitter%22%2C%22iconcolor%22%3A%22%231da1f2%22%2C%22iconlink%22%3A%22%23%22%2C%22icon2%22%3A%22sico%20fab-pinterest-p%22%2C%22iconcolor2%22%3A%22%23e60124%22%2C%22iconlink2%22%3A%22%23%22%2C%22icon3%22%3A%22sico%20fab-linkedin-in%22%2C%22iconcolor3%22%3A%22%230073b0%22%2C%22iconlink3%22%3A%22%23%22%7D%2C%22item4%22%3A%7B%22title%22%3A%22Louisa%20Kelly%22%2C%22job%22%3A%22Graphic%20Designer%22%2C%22single%22%3A%22%2FPortals%2F_default%2FContentBuilder%2Fminis-page%2Fdesign-studio%2FdesignStudio-img12.jpg%22%2C%22singlewidth%22%3A%22370%22%2C%22singleheight%22%3A%22491%22%2C%22icon%22%3A%22sico%20fab-twitter%22%2C%22iconcolor%22%3A%22%231da1f2%22%2C%22iconlink%22%3A%22%23%22%2C%22icon2%22%3A%22sico%20fab-pinterest-p%22%2C%22iconcolor2%22%3A%22%23e60124%22%2C%22iconlink2%22%3A%22%23%22%2C%22icon3%22%3A%22sico%20fab-linkedin-in%22%2C%22iconcolor3%22%3A%22%230073b0%22%2C%22iconlink3%22%3A%22%23%22%7D%7D%2C%22accopen%22%3A%22checked%22%2C%22accopenindex%22%3A%221%22%2C%22%22%3A%22linkurl%22%2C%22link%22%3A%22%23%22%2C%22displayxxl%22%3A%222.5%22%2C%22displayxl%22%3A%221.5%22%2C%22displayl%22%3A%221.5%22%2C%22displaym%22%3A%221.5%22%2C%22displays%22%3A%221.5%22%2C%22displayxs%22%3A%221%22%2C%22pagination%22%3A%22checked%22%2C%22autoplay%22%3A%22checked%22%2C%22delay%22%3A%225000%22%2C%22lazy%22%3A%22checked%22%7D"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style class="build-css" data-class="title-22">${csstemplate["title-22"]}</style>
<style class="build-css" data-class="button-01">${csstemplate["button-01"]}</style>
    `
    }, {
        'thumbnail': 'minis-section/design-studio/section-08.jpg',
        'category': '820',
        'html': `<div class="is-section is-box is-light-text">
    <div class="is-overlay">
        <div class="is-overlay-bg" style="background-image:url(&quot;/Portals/_default/ContentBuilder/minis-page/design-studio/designStudio-bg04.jpg&quot;)"></div>
    </div>
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container" dragwithouthandle="">
                <div class="row justify-content-center">
                    <div class="col-md-12">
                        <h3 class="ml-auto mr-auto mt-10" style="max-width: 600px; text-align: center;">Please Feel Free To Contact Us, We Will Respond To You ASAP.</h3>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-md-12" data-noedit="">
                        <div class="spacer height-40"></div>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-md-12" style="max-width: 768px;">
                    ${getSnippetCode("ajaxform-14")}  
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12" data-noedit="">
                        <div class="spacer height-20"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    `
    }, {
        'thumbnail': 'minis-section/design-studio/section-09.jpg',
        'category': '820',
        'html': `<div class="is-section is-box layout-no-mb">
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container" dragwithouthandle="">
                <div class="row align-items-center">
                    <div class="col-md-12">
                        <div class="spacer height-20"></div>
                        <div class="title-22 text-center ml-auto mr-auto mb-50" style="max-width: 570px;">
                            <small class="bg-accent2">QUESTIONS</small>
                            <h3>FAQs Here May Help You Out If You Are Troubled By Any Issues</h3>
                        </div>
                    </div>
                </div>
                <div class="row align-items-center">
                    <div class="col-md-6">
                        <img src="/Portals/_default/ContentBuilder/minis-page/design-studio/designStudio-img05.jpg" class="img-Lazy" alt="">
                        <div class="spacer height-40 d-md-none"></div>
                    </div>
                    <div class="col-md-6">
                        ${getSnippetCode("accordion05")}  
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12" data-noedit="">
                        <div class="spacer height-80 mb-10"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style class="build-css" data-class="title-22">${csstemplate["title-22"]}</style>
`
    }, {
        'thumbnail': 'minis-section/design-studio/section-10.jpg',
        'category': '820',
        'html': `<div class="is-section is-box layout-no-mt layout-no-mb" style="background-color: rgb(255, 241, 233);">
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container" dragwithouthandle="">
                <div class="row">
                    <div class="col-md-12" data-noedit="">
                        <div class="spacer height-60"></div>
                    </div>
                </div>
                <div class="row align-items-center">
                    <div class="col-md-8">
                        <h4 style="font-weight: 600;">What Are You Waiting For?</h4>
                        <p style="color: rgb(102, 102, 102);">Phasellus sagittis ipsum erat, sit amet luctus arcu porttitor vel rna faucibus.</p>
                    </div>
                    <div class="col-md-4">
                        <a class="button-01 bg-accent3 mb-5 mr-10" href="#" title="LEARN MORE">LEARN MORE</a>
                        <a class="button-09 color-accent2 mb-5" href="#" title="CONTACT US">CONTACT US</a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12" data-noedit="">
                        <div class="spacer height-40 mt-10"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style class="build-css" data-class="button-01">${csstemplate["button-01"]}</style>
<style class="build-css" data-class="button-09">${csstemplate["button-09"]}</style>

    `
    }, {
        'thumbnail': 'minis-section/design-studio/footer.jpg',
        'category': '820',
        'html': `<div class="is-section is-box is-light-text layout-no-mb">
    <div class="is-overlay">
        <div class="is-overlay-bg" style="background-image:url(&quot;/Portals/_default/ContentBuilder/minis-page/design-studio/designStudio-bg05.jpg&quot;)"></div>
    </div>
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container" dragwithouthandle="">
                <div class="row pt-10">
                    <div class="col-md-8 col-lg-4 pr-md-60">
                        <h5 class="mt-0 mb-25">Do Not Want To Miss Anything? 
                            <br>Subscribe Now!</h5>
                        <p class="mb-25">Don't you worry, your email address is strictly confidential and will not be published</p>
                        ${getSnippetCode("ajaxform-09")}  
                        <div class="spacer height-40 mb-15"></div>
                    </div>
                    <div class="col-6 col-md-4 col-lg-2 order-md-first order-lg-0">
                        <div class="design-studio-links">
                            <p>
                                <a href="#" title="">About Us</a>
                            </p>
                            <p>
                                <a href="#" title="">Contact Us</a>
                            </p>
                            <p>
                                <a href="#" title="">Term &amp; Conditions</a>
                            </p>
                            <p>
                                <a href="#" title="">Task Management</a>
                            </p>
                            <p>
                                <a href="#" title="">FAQs</a>
                            </p>
                        </div>
                        <div class="spacer height-40 mb-15"></div>
                    </div>
                    <div class="col-6 col-md-4 col-lg-2">
                        <div class="design-studio-links">
                            <p>
                                <a href="#" title="">Blogs</a>
                            </p>
                            <p>
                                <a href="#" title="">Portfolios</a>
                            </p>
                            <p>
                                <a href="#" title="">Help Center</a>
                            </p>
                            <p>
                                <a href="#" title="">Elements</a>
                            </p>
                            <p>
                                <a href="#" title="">Live Demo</a>
                            </p>
                        </div>
                        <div class="spacer height-40 mb-15"></div>
                    </div>
                    <div class="col-md-8 col-lg-4">
                        <h5 class="mt-0">Follow Us</h5>
                        <p>Donec sit amet posuere neque. Etiam eget tincidunt tellus, viverra porta felis.</p>
                        <div class="edit-box pt-5">
                            <a class="social-10" href="#">
                                <i class="sico fab-pinterest-p" style="background-color: rgb(227, 50, 77);"><svg><use xlink:href="#fab-pinterest-p"></use></svg></i>Pinterest</a>
                            <a class="social-10" href="#">
                                <i class="sico fab-linkedin-in" style="background-color: rgb(27, 137, 196);"><svg><use xlink:href="#fab-linkedin-in"></use></svg></i>Linkedin</a>
                            <a class="social-10" href="#">
                                <i class="sico fab-twitter" style="background-color: rgb(30, 164, 255);"><svg><use xlink:href="#fab-twitter"></use></svg></i>Twitter</a>
                            <a class="social-10" href="#">
                                <i class="sico fab-dribbble" style="background-color: rgb(234, 76, 137);"><svg><use xlink:href="#fab-dribbble"></use></svg></i>Dribbble</a>
                        </div>
                        <div class="spacer height-40 mb-15"></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <hr style="border-top: 1px solid rgba(255, 255, 255, 0.1);" class="mb-30"></div>
                </div>
                <div class="row">
                    <div class="col-md-6 text-center text-md-left">
                        <p class="mb-0">@ 2019 by DNNGo Corp. All Rights Resevered</p>
                    </div>
                    <div class="col-md-6 text-center text-md-right design-studio-links">
                        <p class="mb-0">
                            <a href="#" title="Privacy Policy">Privacy Policy</a>&nbsp;&nbsp; 
                            <span class="sep">|&nbsp;</span>
                            <a href="#" title="Terms And Conditions">Terms And Conditions</a>
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12" data-noedit="">
                        <div class="spacer height-20 mt-10"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style class="build-css" data-class="social-10">${csstemplate["social-10"]}</style>

    <style class="build-css" data-class="design-studio-links">.design-studio-links a, .design-studio-links a:link, .design-studio-links a:active, .design-studio-links a:visited{ color:inherit; }.design-studio-links a:hover{ color:var(--accent-color3); } .design-studio-links p{ margin-bottom: 20px; }</style>
    <style data-class="social-10">body .social-10{ min-width: 180px; display: inline-block; margin: 0 0 20px 0; font-size: 18px; color: #fff; } body .social-10 i { display: inline-flex; justify-content: center; align-items: center; width: 42px; height: 42px; text-align: center; border-radius: 50%; color: #ffffff; text-decoration: none; -webkit-transition: all 0.3s cubic-bezier(0.420, 0.000, 1.000, 1.000) 0s; transition: all 0.3s cubic-bezier(0.420, 0.000, 1.000, 1.000) 0s; background-color: var(--accent-color); margin-right: 12px; } body .social-10:hover i{ background-color: var(--accent-color); } body .social-10:link, body .social-10:active, body .social-10:visited{ color: #fff; }</style>
</div>
    `
    }

)

data_basic.designs.push({
        'thumbnail': 'minis-section/app02/banner.jpg',
        'category': '821',
        'html': `<div class="is-section is-shadow-1 is-section-auto layout-no-mb app02-banner">
    <div class="is-boxes">
        <div class="is-box-5 is-box">
            <div class="is-overlay">
                <div class="is-overlay-bg" style="background-image:url(&quot;/Portals/_default/ContentBuilder/minis-page/app02/app02-banner-bg01.png&quot;);background-position: top left;background-size: contain;"></div>
            </div>
            <div class="is-boxes">
                <div class="is-box-centered">
                    <div class="is-container container-fluid banner-left" dragwithouthandle="">
                        <div class="row">
                            <div class="col-md-12">
                                <h2>
                                    <span class="color-accent">Great App</span> Ensures You Twice The Result With Half The Effort.</h2>
                                <div class="spacer height-40"></div>
                                <p>Aenean at est mattis, facilisis justo a, dapibus leo. Vivamus in eleifend libero, elementum sollicitudin diam. Morbi quis vestibulum felis, sit amet malesuada felis.</p>
                                <div class="spacer height-60"></div>
                                <a href="#" title="LEARN MORE" class="button-13 bg-accent2"> LEARN MORE&nbsp; 
                                    <i class="sico lnr-arrow-right"><svg><use xlink:href="#lnr-arrow-right"></use></svg></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="is-box-7 is-light-text is-box">
            <div class="is-overlay">
                <div class="is-overlay-bg" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/app02/app02-banner-bg02.png&quot;); background-position: 100% 0%; background-size: contain;"></div>
                <div class="is-overlay-content"></div>
            </div>
            <div class="is-boxes">
                <div class="is-box-centered">
                    <div class="is-container container-fluid is-container-fluid" dragwithouthandle="">
                        <div class="row">
                            <div class="col-md-12 pl-md-60">
                                <img src="/Portals/_default/ContentBuilder/minis-page/app02/app02-img01.png" alt="Image" class="img-Lazy"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style class="build-css" data-class="button-13">${csstemplate["button-13"]}</style>
    <style class="build-css" data-class="app02-banner">
    @media (min-width:1400px) {
        .app02-banner .banner-left {
            max-width: 490px;
            display: block;
            margin-left: calc((100vw - 1410px)/2);
        }
    }
    
    @media only screen and (max-width: 991px) {
        .app02-banner .is-boxes {
            display: -ms-flexbox;
            display: flex;
            -ms-flex-wrap: wrap;
            flex-wrap: wrap;
        }
    
        .app02-banner .is-boxes>.is-box {
            display: block;
        }
    
        .app02-banner .is-boxes>.is-box:last-child {
            order: -1;
        }
    }
    </style>
</div>
    `
    }, {
        'thumbnail': 'minis-section/app02/section-01.jpg',
        'category': '821',
        'html': `<div class="is-section is-box is-align-center">
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container" dragwithouthandle="">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <h3 class="color-accent2 mb-30 mt-10">Awesome App Features</h3>
                        <p style="max-width: 770px; display: inline-block;">Sed porttitor elit nec ligula efficitur ornare. Vivamus malesuada sed eros nec pretium. Etiam id vestibulum mauris. Nunc quis varius odio. Nam auctor ligula eu nisl hendrerit rutrum.</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12" data-noedit="">
                        <div class="spacer height-40"></div>
                    </div>
                </div>
                ${getSnippetCode("iconbox-18")}  
                <div class="row">
                    <div class="col-md-12">
                        <div class="spacer height-20"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
   
</div>
    `
    }, {
        'thumbnail': 'minis-section/app02/section-02.jpg',
        'category': '821',
        'html': `<div class="is-section is-box is-light-text layout-no-mb layout-no-mt">
    <div class="is-overlay">
        <div class="is-overlay-bg" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/app02/app02-bg01.png&quot;); background-position: 100% 80%; background-size: cover;"></div>
    </div>
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container" dragwithouthandle="">
                <div class="row align-items-center justify-content-center">
                    <div class="col-md-6 text-center">
                        ${getSnippetCode("counter-11")}  
                    </div>
                    <div class="col-lg-6 pl-lg-80">
                        <div class="spacer height-160"></div>
                        <h3 class="pr-60" style="font-weight: 600;">We Devote To Delivering The Most Recent Trend</h3>
                        <div class="spacer height-20 mb-10"></div>
                        <p>Donec quis rutrum tortor. Aenean in eleifend lorem. Donec porttitor varius condimentum. Mauris nec vestibulum mi. Vestibulum in imperdiet risus. Vivamus a tempus arcu.</p>
                        <div class="spacer height-20 mb-10"></div>
                        ${getSnippetCode("list-03")} 
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-md-12" data-noedit="">
                        <div class="spacer height-60 mb-10"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    `
    }, {
        'thumbnail': 'minis-section/app02/section-03.jpg',
        'category': '821',
        'html': `<div class="is-section is-box layout-no-mb">
    <div class="is-overlay">
        <div class="is-overlay-bg" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/app02/app02-section03-bg.png&quot;); background-size: auto; background-position: 0% 100%;"></div>
    </div>
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container" dragwithouthandle="">
                <div class="row align-items-center">
                    <div class="col-lg-5">
                        <h3>Our Years Of Tailoring Experience Will Impress You.</h3>
                        <div class="spacer height-20"></div>
                        <p>Nunc quam ligula, interdum eget felis nec, varius vehicula enim. Pellentesque condimentum ultricies ligula non ultrices. us.</p>
                        <div class="spacer height-20 mb-5"></div>
                        ${getSnippetCode("iconbox-19")} 
                        <div class="spacer height-20"></div>
                    </div>
                    <div class="col-lg-7">
                        <img src="/Portals/_default/ContentBuilder/minis-page/app02/app02-img03.png" alt="" class="mt-10 img-Lazy"></div>
                </div>
                <div class="row">
                    <div class="col-md-12" data-noedit="">
                        <div class="spacer height-60"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style class="build-css" data-class="app02-banner">.app02-section03 .is-overlay .is-overlay-bg{ background-size:auto; background-position: left bottom; } .app02-section03 .section-left{ margin-right: -40px; }</style>
</div>
    `
    }, {
        'thumbnail': 'minis-section/app02/section-04.jpg',
        'category': '821',
        'html': `<div class="is-section is-box is-light-text layout-no-mt layout-no-mb">
    <div class="is-overlay">
        <div class="is-overlay-bg" style="background-image:url(&quot;/Portals/_default/ContentBuilder/minis-page/app02/app02-bg02.png&quot;)"></div>
    </div>
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container" dragwithouthandle="">
                <div class="row">
                    <div class="col-md-12" data-noedit="">
                        <div class="spacer height-60 mb-10"></div>
                    </div>
                </div>
                <div class="row align-items-center">
                    <div class="col-md-8">
                        <h3 style="font-weight: 400;">Easily present your app with style.</h3>
                        <p class="mb-0 size-18">Beautiful websites are easy to create now! Are you ready to Purchase DNNGo Template?</p>
                    </div>
                    <div class="col-md-4 text-md-right">
                        <a href="#" title="LEARN MORE" class="button-13 mt-15 mb-15">BUY IT NOW 
                            <i class="sico lnr-arrow-right mr-0 ml-10"><svg><use xlink:href="#lnr-arrow-right"></use></svg></i>
                        </a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12" data-noedit="">
                        <div class="spacer height-60 mb-10"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style class="build-css" data-class="button-13">${csstemplate["button-13"]}</style>
    `
    }, {
        'thumbnail': 'minis-section/app02/section-05.jpg',
        'category': '821',
        'html': `<div class="is-section is-box is-align-center layout-no-mb">
    <div class="is-overlay">
        <div class="is-overlay-bg" style="background-image:url(&quot;/Portals/_default/ContentBuilder/minis-page/app02/app02-bg03.png&quot;)"></div>
    </div>
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container mb-0" dragwithouthandle="">
                <div class="row">
                    <div class="col-md-12">
                        <h3 class="color-accent2 mb-30 mt-10">Awesome App Features</h3>
                        <p style="max-width: 770px; display: inline-block;">Sed porttitor elit nec ligula efficitur ornare. Vivamus malesuada sed eros nec pretium. Etiam id vestibulum mauris. Nunc quis varius odio. Nam auctor ligula eu nisl hendrerit rutrum.</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12" data-noedit="">
                        <div class="spacer height-40"></div>
                    </div>
                </div>
            </div>
            <div class="section-tabs-list container">
                <ul>
                    <li class="item cog-tab-active active">
                        <span class="title">MONTHLY</span>
                    </li>
                    <li class="item">YEARLY</li>
                </ul>
            </div>
            <div class="section-tabs-container">
                <div class="is-container active" style="display: block;" dragwithouthandle="">
                ${getSnippetCode("price-08")} 
                </div>
                <div class="is-container" style="display: none;" dragwithouthandle="">
                    <div class="row">
                        <div class="col-md-6 col-lg-4">
                            <div class="price-08 color-1">
                                <div class="price-item">
                                    <div class="price-header">
                                        <div class="icon">
                                            <img src="/Portals/_default/ContentBuilder/minis-page/app02/app02-icon09.png" class="img-Lazy" alt=""></div>
                                        <h4 class="price-title">Standard Plan</h4>
                                        <p>Nulla fringilla aliquet est nec dapibus</p>
                                    </div>
                                    <div class="price-content">
                                        <div class="price-features">
                                            <ul>
                                                <li>Full Time Support</li>
                                                <li>Unlimited Data Transfer</li>
                                                <li>60GB Space</li>
                                                <li>Multiple Header Styles</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="price-box">
                                        <span class="pricing"><sup>$</sup>180</span>
                                    </div>
                                </div>
                                <div class="price-footer">
                                    <a href="#" title="">PURCHASE NOW 
                                        <i class="sico lnr-arrow-right"><svg><use xlink:href="#lnr-arrow-right"></use></svg></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-4">
                            <div class="price-08 color-2">
                                <div class="price-item">
                                    <div class="price-header">
                                        <div class="icon">
                                            <img src="/Portals/_default/ContentBuilder/minis-page/app02/app02-icon10.png" class="img-Lazy" alt=""></div>
                                        <h4 class="price-title">Superior Plan</h4>
                                        <p>Nulla fringilla aliquet est nec dapibus</p>
                                    </div>
                                    <div class="price-content">
                                        <div class="price-features">
                                            <ul>
                                                <li>Full Time Support</li>
                                                <li>Unlimited Data Transfer</li>
                                                <li>120GB Space</li>
                                                <li>Multiple Header Styles</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="price-box">
                                        <span class="pricing"><sup>$</sup>420</span>
                                    </div>
                                </div>
                                <div class="price-footer">
                                    <a href="#" title="">PURCHASE NOW 
                                        <i class="sico lnr-arrow-right"><svg><use xlink:href="#lnr-arrow-right"></use></svg></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-4">
                            <div class="price-08 color-3">
                                <div class="price-item">
                                    <div class="price-header">
                                        <div class="icon">
                                            <img src="/Portals/_default/ContentBuilder/minis-page/app02/app02-icon11.png" class="img-Lazy" alt=""></div>
                                        <h4 class="price-title">Maximum Plan</h4>
                                        <p>Nulla fringilla aliquet est nec dapibus</p>
                                    </div>
                                    <div class="price-content">
                                        <div class="price-features">
                                            <ul>
                                                <li>Full Time Support</li>
                                                <li>Unlimited Data Transfer</li>
                                                <li>250GB Space</li>
                                                <li>Multiple Header Styles</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="price-box">
                                        <span class="pricing"><sup>$</sup>650</span>
                                    </div>
                                </div>
                                <div class="price-footer">
                                    <a href="#" title="">PURCHASE NOW 
                                        <i class="sico lnr-arrow-right"><svg><use xlink:href="#lnr-arrow-right"></use></svg></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="is-container mt-0" dragwithouthandle="">
                <div class="row">
                    <div class="col-md-12" data-noedit="">
                        <div class="spacer height-60 mb-10"></div>
                        <div class="spacer height-20"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style class="build-css" data-class="section-tabs-list"> .section-tabs-list ul{ margin: 0 0 50px; padding: 0; display: inline-block; box-shadow: 0 0 15px rgba(0,0,0,.1); border-radius: 25px; } .section-tabs-list li{ display: inline-block; cursor: pointer; height: 50px; line-height: 50px; padding:0 30px; white-space: nowrap; transition:all 200ms ease-in ; border-radius: 25px; font-weight: 600; } .section-tabs-list li.active{ color: #fff; background-color: var(--accent-color2); } .section-tabs-container .is-container{ display: none; margin-top: 0!important; margin-bottom: 0!important; -webkit-transition: none; -moz-transition: none; -ms-transition: none; -o-transition: none; transition: none; } .section-tabs-container .is-container.active{ display: block; } </style>
</div>
    `
    }, {
        'thumbnail': 'minis-section/app02/section-06.jpg',
        'category': '821',
        'html': `<div class="is-section is-box is-light-text layout-no-mb">
    <div class="is-overlay">
        <div class="is-overlay-bg" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/app02/app02-bg04.png&quot;); background-position: 0% 60%; background-size: cover;"></div>
    </div>
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container" dragwithouthandle="">
                <div class="row align-items-center">
                    <div class="col-lg-6 pr-lg-80">
                        <h2>Free Trial Is Available</h2>
                        <h4 style="font-weight: 400;">Why not giving it a try now, we guarantee you won't regret it.</h4>
                        <div class="spacer height-40"></div>
                        <p>Nunc quam ligula, interdum eget felis nec, varius vehicula enim. Pellentesque condimentum ultricies ligula non ultrices. Phasellus lobortis nisi eget nibh luctus varius. </p>
                        <div class="spacer height-40"></div>
                        <p>
                            <a href="#" title="" class="mr-20 mb-10 d-inline-block">
                                <img src="/Portals/_default/ContentBuilder/minis-page/app02/app02-btn01.png" alt="" class="img-Lazy"></a>
                            <a href="#" title="" class="mb-10 d-inline-block">
                                <img src="/Portals/_default/ContentBuilder/minis-page/app02/app02-btn02.png" alt="" class="img-Lazy"></a>
                        </p>
                        <div class="spacer height-40"></div>
                    </div>
                    <div class="col-lg-6 right-full-column">
                        <div class="full-column-inner">
                            <img src="/Portals/_default/ContentBuilder/minis-page/app02/app02-img04.png" style="margin-bottom: -50px;" class="img-Lazy" alt=""></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    `
    }, {
        'thumbnail': 'minis-section/app02/section-07.jpg',
        'category': '821',
        'html': `<div class="is-section is-box is-align-center">
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container" dragwithouthandle="">
                <div class="row">
                    <div class="col-md-12">
                        <h3 class="color-accent2 mb-30 mt-10">How It Works</h3>
                        <p style="max-width: 770px; display: inline-block;">Sed porttitor elit nec ligula efficitur ornare. Vivamus malesuada sed eros nec pretium. Etiam id vestibulum mauris. Nunc quis varius odio. Nam auctor ligula eu nisl hendrerit rutrum.</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="spacer height-60"></div>
                    </div>
                </div>
                ${getSnippetCode("step03")} 
            </div>
        </div>
    </div>
</div>
    `
    }, {
        'thumbnail': 'minis-section/app02/section-08.jpg',
        'category': '821',
        'html': `<div class="is-section is-box is-align-center layout-no-plr layout-no-mb" style="background-color: rgb(237, 235, 253);">
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container is-container-fluid" dragwithouthandle="">
                <div class="row">
                    <div class="col-md-12">
                        <h3 class="color-accent2 mb-30 mt-10" style="max-width: 570px; display: inline-block;">Apply A Better Way To Beautifully Showcase Your APPs</h3>
                        <div class="spacer height-20"></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                    ${getSnippetCode("carousel07")} 
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12" data-noedit="">
                        <div class="spacer height-40 mb-5 d-none d-lg-block"></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12" data-noedit="">
                        <div class="spacer height-20"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    `
    }, {
        'thumbnail': 'minis-section/app02/section-09.jpg',
        'category': '821',
        'html': `<div class="is-section is-box">
    <div class="is-overlay">
        <div class="is-overlay-bg" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/app02/app02-bg05.png&quot;); background-size: auto; background-position: 0% 100%;"></div>
    </div>
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container" dragwithouthandle="">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <h3 class="color-accent2 mb-30 mt-10">Frequently Asked Question</h3>
                        <p style="max-width: 770px; display: inline-block;">Sed porttitor elit nec ligula efficitur ornare. Vivamus malesuada sed eros nec pretium. Etiam id vestibulum mauris. Nunc quis varius odio. Nam auctor ligula eu nisl hendrerit rutrum.</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12" data-noedit="">
                        <div class="spacer height-40"></div>
                    </div>
                </div>
                ${getSnippetCode("faq-05")} 
            </div>
        </div>
    </div>
     
</div>
    `
    }, {
        'thumbnail': 'minis-section/app02/footer.jpg',
        'category': '821',
        'html': `<div class="is-section is-box is-align-left is-light-text layout-no-mb layout-no-mt">
    <div class="is-overlay">
        <div class="is-overlay-bg" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/app02/app02-bg06.png&quot;); background-size: cover; background-position: 100% 20%;"></div>
    </div>
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container" dragwithouthandle="">
                <div class="row text-center" style="background-color: rgb(255, 255, 255); box-shadow: rgba(0, 0, 0, 0.1) 0px 0px 15px; margin-left: 0px; margin-right: 0px;">
                    <div class="col-md-12">
                        <div class="spacer height-40 mb-10"></div>
                        <h3 class="color-accent2">Trusted Partner For Years</h3>
                        <div class="spacer height-20 mb-15"></div>
                    </div>
                    <div class="col-md-12">
                        <a href="#" class="mr-20 ml-20 mb-20 d-inline-block" title="">
                            <img src="/Portals/_default/ContentBuilder/minis-page/app02/app02-client01.png" class="img-Lazy" alt=""></a>
                        <a href="#" class="mr-20 ml-20 mb-20 d-inline-block" title="">
                            <img src="/Portals/_default/ContentBuilder/minis-page/app02/app02-client02.png" class="img-Lazy" alt=""></a>
                        <a href="#" class="mr-20 ml-20 mb-20 d-inline-block" title="">
                            <img src="/Portals/_default/ContentBuilder/minis-page/app02/app02-client03.png" class="img-Lazy" alt=""></a>
                        <a href="#" class="mr-20 ml-20 mb-20 d-inline-block" title="">
                            <img src="/Portals/_default/ContentBuilder/minis-page/app02/app02-client04.png" class="img-Lazy" alt=""></a>
                        <a href="#" class="mr-20 ml-20 mb-20 d-inline-block" title="">
                            <img src="/Portals/_default/ContentBuilder/minis-page/app02/app02-client05.png" class="img-Lazy" alt=""></a>
                    </div>
                    <div class="col-md-12">
                        <div class="spacer height-40"></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12" data-noedit="">
                        <div class="spacer height-80 mb-10"></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <img src="/Portals/_default/ContentBuilder/minis-page/logo-white.png" alt="logo" style="margin-bottom: 32px; margin-top: 5px;" class="img-Lazy">
                        <p>In fringilla magna fringilla, consequat augue vitae, scelerisque ligula.</p>
                        <p style="font-size: 20px; margin-bottom: 0px; line-height: 32px;">
                            <i class="sico lnr-phone-wave" style="margin-right: 10px;position: relative; top: 3px;"><svg><use xlink:href="#lnr-phone-wave"></use></svg></i>+1 845-359-0545</p>
                        <p style="font-size: 20px; margin-bottom: 16px; line-height: 32px;">
                            <i class="sico lnr-envelope-open" style="margin-right: 10px;position: relative; top: 3px;"><svg><use xlink:href="#lnr-envelope-open"></use></svg></i>service.simple@gmail.com</p>
                        <div class="spacer height-20 mb-10"></div>
                    </div>
                    <div class="col-md-2">
                        <div class="app02-footer-links">
                            <p>
                                <a href="#" title="">About Us</a>
                            </p>
                            <p>
                                <a href="#" title="">Contact Us</a>
                            </p>
                            <p>
                                <a href="#" title="">Term &amp; Conditions</a>
                            </p>
                            <p>
                                <a href="#" title="">Task Management</a>
                            </p>
                            <p>
                                <a href="#" title="">FAQs</a>
                            </p>
                            <p>
                                <a href="#" title="">Project Management</a>
                            </p>
                        </div>
                        <div class="spacer height-20 mb-10"></div>
                    </div>
                    <div class="col-md-2" style="text-align: left;">
                        <div class="app02-footer-links">
                            <p>
                                <a href="#" title="">Blogs</a>
                            </p>
                            <p>
                                <a href="#" title="">Portfolios</a>
                            </p>
                            <p>
                                <a href="#" title="">Online Documentation</a>
                            </p>
                            <p>
                                <a href="#" title="">Elements</a>
                            </p>
                            <p>
                                <a href="#" title="">Live Demo</a>
                            </p>
                            <p>
                                <a href="#" title="">Help Center</a>
                            </p>
                        </div>
                        <div class="spacer height-20 mb-10"></div>
                    </div>
                    <div class="col-md-4">
                        <div class="row">
                            <div class="col-auto">
                                <img src="/Portals/_default/ContentBuilder/minis-page/app02/app02-icon15.png" class="img-Lazy" alt=""></div>
                            <div class="col">
                                <p style="font-size: 20px; line-height: 1.4;">What are you waiting for? Subscribe now!</p>
                            </div>
                        </div>
                        <p class="mb-25">Aliquam mollis diam vel purus facilisis cursus. Suspendisse et nisl.</p>
                        ${getSnippetCode("ajaxform-09")} 
                        <div class="spacer height-20 mb-10"></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <hr style="border-top:1px solid rgba(255,255,255,0.1)"></div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="spacer height-20 mb-15"></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="edit-box text-center">
                            <a class="social-06" href="#">
                                <i class="sico fab-pinterest-p"><svg><use xlink:href="#fab-pinterest-p"></use></svg></i>
                            </a>
                            <a class="social-06" href="#">
                                <i class="sico fab-linkedin-in"><svg><use xlink:href="#fab-linkedin-in"></use></svg></i>
                            </a>
                            <a class="social-06" href="#">
                                <i class="sico fab-twitter"><svg><use xlink:href="#fab-twitter"></use></svg></i>
                            </a>
                            <a class="social-06" href="#">
                                <i class="sico fab-dribbble"><svg><use xlink:href="#fab-dribbble"></use></svg></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p class="text-center app02-footer-links">@ 2019 by DNNGo Corp. All Rights Resevered&nbsp; |&nbsp;&nbsp; 
                            <a href="#" title="Privacy Statement">Privacy Statement</a>&nbsp; |&nbsp;&nbsp; 
                            <a href="#" title="Terms of Use">Terms of Use</a>
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="spacer height-20"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style class="build-css" data-class="social-06">${csstemplate["social-06"]}</style>
<style class="build-css" data-class="app02-footer-links"> .app02-footer-links a, .app02-footer-links a:link, .app02-footer-links a:active, .app02-footer-links a:visited{ color:inherit; } .app02-footer-links p{ margin-bottom: 10px; }.app02-footer-links a:hover{color: var(--accent-color); } </style>
    `
    }

)

data_basic.designs.push({
        'thumbnail': 'minis-section/design-studio2/banner.jpg',
        'category': '822',
        'html': `<div class="is-section is-box is-section-md-auto is-section-auto layout-no-mb layout-no-mt">
	<div class="is-overlay">
		<div class="is-overlay-bg" style="background-image:url(&quot;/Portals/_default/ContentBuilder/minis-page/design-studio2/designStudio2-banner.jpg&quot;)"></div>
	</div>
	<div class="is-boxes">
		<div class="is-box-centered">
			<div class="is-container layout-container" dragwithouthandle="">
				<div class="row align-items-center">
					<div class="col-md-3 col-lg-2 left-full-column">
						<div class="spacer height-80 mb-30 mb-md-0 d-lg-none"></div>
						<div class="full-column-inner pl-15">
							<div class="designStudio2-banner-text">
								<h1 style="color: rgb(214, 9, 3);">Be Brave Enough To Live 
									<span class="color-accent2" style="font-weight: 700;">Life Creatively</span>
								</h1>
							</div>
							<div class="spacer height-60"></div>
						</div>
					</div>
					<div class="col-md-5 col-lg-7 col-xl-8 order-last order-sm-0">
						<div class="spacer height-100"></div>
						<img src="/Portals/_default/ContentBuilder/minis-page/design-studio2/designStudio2-banner-img.png" alt="" class="img-Lazy">
					</div>
					<div class="col-md-4 col-lg-3 col-xl-2 right-full-column">
						<div class="full-column-inner" style="max-width:380px">
							<div class="spacer d-none d-lg-block height-100"></div>
							<h2 class="color-accent2 mb-20" style="font-weight: 700;">Dare For More</h2>
							<p style="color: rgb(76, 27, 0);" class="size-18 mb-10">Aliquam luctus finibus neque et porta. Aliquam tempus et
								lectus.</p>
							<p>
								<a href="#" title="VIEW MORE" style="color: rgb(214, 9, 3); font-weight: 700;">VIEW MORE
									<span class="size-22 ml-4">+</span>
								</a>
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
	<style data-class="designStudio2-banner-text" class="build-css">
		.designStudio2-banner-text {
			width: 378px;
			margin: 0 -60px 0 auto;
			z-index: 10;
			position: relative;
		}

		.designStudio2-banner-text::before {
			content: "";
			height: 8px;
			width: 50px;
			background: #d60903;
			display: block;
			margin: 0 0 28px;
			font-weight: 700;
		}

		.designStudio2-banner-text>* {
			font-weight: 600;
		}

		@media only screen and (max-width: 991px) {
			.designStudio2-banner-text {
				width: auto;
			}
		}
	</style>
    `
    }, {
        'thumbnail': 'minis-section/design-studio2/section-01.jpg',
        'category': '822',
        'html': `<div class="is-section is-box is-align-center">
	<div class="is-boxes">
		<div class="is-box-centered">
			<div class="is-container layout-container" dragwithouthandle="">
				<div class="row">
					<div class="col-md-12 m-auto" style="max-width: 840px;">
						<h3 class="title-23 ml-auto mr-auto mt-10" style="max-width: 500px;">Our Product 
							<span class="border-accent3">Speaks For Itself</span>, Nothing Else Needed</h3>
						<div class="spacer height-20"></div>
						<p>Sed nibh justo, posuere in ullamcorper eget, finibus ut turpis. Etiam odio nulla, venenatis vel pulvinar
							interdum, varius quis turpis. Cras nunc dui, finibus sit amet interdum non, luctus sit amet mauris.</p>
						<div class="spacer height-40"></div>
					</div>
				</div>
                ${getSnippetCode("iconbox-21")} 
			</div>
		</div>
	</div>
</div>
	<style class="build-css" data-class="title-23">${csstemplate["title-23"]}</style>

    `
    }, {
        'thumbnail': 'minis-section/design-studio2/section-02.jpg',
        'category': '822',
        'html': `<div class="is-section is-shadow-1 is-section-auto">
	<div class="is-boxes">
		<div class="is-box-6 is-light-text is-box">
			<div class="is-overlay">
				<div class="is-overlay-bg" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/design-studio2/designStudio2-img01.jpg&quot;);"></div>
				<div class="is-overlay-color"></div>
				<div class="is-overlay-content"></div>
			</div>
			<div class="is-boxes">
				<div class="is-box-centered"></div>
			</div>
		</div>
		<div class="is-box-6 is-box" style="background-color: rgb(246, 245, 254);">
			<div class="is-boxes">
				<div class="is-box-centered">
					<div class="is-container container-fluid pr-md-50 pl-md-50" style="max-width: 660px;" dragwithouthandle="">
						<div class="row">
							<div class="col-md-12">
								<div class="spacer height-40 d-none d-lg-block"></div>
								<div class="title-23">
									<small>What we do</small>
									<h3>We Are Always 
										<span class="border-accent3">Your Solid Backing</span>
									</h3>
								</div>
								<div class="spacer height-20"></div>
								<p>Curabitur a lacus arcu. Morbi ac ex accumsan, semper risus non, mollis diam. Phasellus pulvinar congue
									consectetur.</p>
								<div class="spacer height-20"></div>
								<p class="designStudio2-text-left-line">Pellentesque at arcu vitae mi malesuada aliquam id a enim. Maecenas
									mattis euismod efficitur. Nunc ut justo sit amet sapien dictum dapibus. Proin ornare elementum risus eget
									mattis.</p>
								<div class="spacer height-20"></div>
								<p class="mb-0">
									<a href="#" title="VIEW MORE" class="button-01 border-radius-3 bg-accent3">VIEW MORE</a>
								</p>
								<div class="spacer height-40 d-none d-lg-block"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
    </div>
</div>
    <style class="build-css" data-class="title-23">${csstemplate["title-23"]}</style>
    <style class="build-css" data-class="button-01">${csstemplate["button-01"]}</style>
	<style class="build-css" data-class="designStudio2-text-left-line">
		.designStudio2-text-left-line {
			position: relative;
			padding: 0 0 0 25px;
		}

		.designStudio2-text-left-line::after {
			content: "";
			border-left: 4px solid var(--accent-color3);
			position: absolute;
			top: 8px;
			bottom: 8px;
			left: 0;
		}
	</style>
    `
    }, {
        'thumbnail': 'minis-section/design-studio2/section-03.jpg',
        'category': '822',
        'html': `<div class="is-section is-box">
	<div class="is-boxes">
		<div class="is-box-centered">
			<div class="is-container layout-container" dragwithouthandle="">
				<div class="row align-items-center">
					<div class="col-md-12 m-auto text-center" style="max-width: 830px;">
						<h3 class="title-23 ml-auto mr-auto mt-10" style="max-width: 570px;">We Provide Our Customers With The 
							<span class="border-accent3">Highest Quality Service</span>
						</h3>
						<div class="spacer height-20 mb-5"></div>
						<p>Sed nibh justo, posuere in ullamcorper eget, finibus ut turpis. Etiam odio nulla, venenatis vel pulvinar
							interdum, varius quis turpis. Cras nunc dui, finibus sit amet interdum non, luctus sit amet mauris.</p>
						<div class="spacer height-40"></div>
					</div>
				</div>
				<div class="row align-items-center">
					<div class="col-md-6 left-full-column text-right">
						<div class="full-column-inner pr-30">
							<img src="/Portals/_default/ContentBuilder/minis-page/design-studio2/designStudio2-img02.png" alt="" class="img-Lazy"></div>
						<div class="spacer height-20"></div>
					</div>
					<div class="col-md-6">
                        ${getSnippetCode("infobox-15")} 
					</div>
				</div>
			</div>
		</div>
    </div>
</div>
    <style class="build-css" data-class="title-23">${csstemplate["title-23"]}</style>
    `
    }, {
        'thumbnail': 'minis-section/design-studio2/section-04.jpg',
        'category': '822',
        'html': `<div class="is-section is-box is-light-text layout-no-plr layout-no-mb layout-no-mt" style="background-color: rgb(42, 37, 119);">
	<div class="is-boxes">
		<div class="is-box-centered">
			<div class="is-container layout-container is-container-fluid" dragwithouthandle="">
				<div class="row">
					<div class="col-md-12">
						<div class="spacer mb-10 height-80 mb-20"></div>
						<h3 class="title-23 m-auto text-center pr-15 pl-15" style="max-width: 780px;font-weight: 600;">These Are Only Some Of 
							<span class="border-accent3">The Great Works</span> We Delivered To Our Clients </h3>
						<div class="spacer height-20"></div>
						<div class="spacer height-100"></div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
                        ${getSnippetCode("carousel-3d-style02")} 
					</div>
				</div>
			</div>
		</div>
    </div>
</div>
    <style class="build-css" data-class="title-23">${csstemplate["title-23"]}</style>
    `
    }, {
        'thumbnail': 'minis-section/design-studio2/section-05.jpg',
        'category': '822',
        'html': `<div class="is-section is-box">
	<div class="is-boxes">
		<div class="is-box-centered">
			<div class="is-container layout-container" dragwithouthandle="">
				<div class="row">
					<div class="col-md-12">
						<div class="spacer height-40 mb-10"></div>
						<h3 class="title-23 m-auto text-center" style="max-width: 570px;">Affordable <span class="border-accent3">Pricing Plans</span>
						</h3>
						<div class="spacer height-20 mb-10"></div>
						<p class="m-auto text-center" style="max-width: 750px;">Phasellus luctus, dolor sit amet sodales porta, eros eros
							pellentesque metus, in facilisis lacus odio vitae lorem. Proin maximus purus libero.</p>
						<div class="spacer height-40 mb-15"></div>
					</div>
				</div>
				${getSnippetCode("price-10")} 
			</div>
		</div>
	</div>
</div>
    <style class="build-css" data-class="title-23">${csstemplate["title-23"]}</style>
    `
    }, {
        'thumbnail': 'minis-section/design-studio2/section-06.jpg',
        'category': '822',
        'html': `<div class="is-section is-box is-light-text is-align-center layout-no-plr" style="background-color: rgb(42, 37, 119);">
	<div class="is-boxes">
		<div class="is-box-centered">
			<div class="is-container layout-container is-container-fluid" dragwithouthandle="">
				<div class="row" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/design-studio2/designStudio2-clock.png&quot;); background-position: center center; background-repeat: no-repeat; background-size: contain;">
					<div class="col-md-12">
						<div class="spacer height-20"></div>
						<h3 class="ml-auto mr-auto mt-10" style="max-width: 500px;">Big Events In Past And Important Plans For Future</h3>
						<div class="spacer height-20"></div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12" data-noedit="">
						<div class="spacer height-40"></div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
                    ${getSnippetCode("timeline01")} 
					</div>
				</div>
				<div class="row">
					<div class="col-md-12" data-noedit="">
						<div class="spacer height-20"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
    `
    }, {
        'thumbnail': 'minis-section/design-studio2/section-07.jpg',
        'category': '822',
        'html': `<div class="is-section is-box">
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container" dragwithouthandle="">
                <div class="row">
                    <div class="col-md-12" data-noedit="">
                        <div class="spacer height-20"></div>
                    </div>
                </div>
                <div class="row align-items-center">
                    <div class="col-md-6 pr-lg-60">
                        <div class="title-23">
                            <small>Our features</small>
                            <h3>Highly Powerful And Useful 
                                <span class="border-accent3">Website Functionality</span>
                            </h3>
                        </div>
                        <div class="spacer height-20 mb-5"></div>
                        <p>Curabitur a lacus arcu. Morbi ac ex accumsan, semper risus non, mollis diam. Phasellus pulvinar congue consectetur.</p>
                        <div class="spacer height-20"></div>
                        <div class="row">
                            <div class="col-lg-6">
                                <ul class="list-04">
                                    <li>
                                        <i class="sico lnr-check"><svg><use xlink:href="#lnr-check"></use></svg></i>Fast Support</li>
                                    <li>
                                        <i class="sico lnr-check"><svg><use xlink:href="#lnr-check"></use></svg></i>Instant Notifications</li>
                                    <li>
                                        <i class="sico lnr-check"><svg><use xlink:href="#lnr-check"></use></svg></i>2-factor Authentication</li>
                                </ul>
                            </div>
                            <div class="col-lg-6">
                                <ul class="list-04">
                                    <li>
                                        <i class="sico lnr-check"><svg><use xlink:href="#lnr-check"></use></svg></i>Unlimited Options</li>
                                    <li>
                                        <i class="sico lnr-check"><svg><use xlink:href="#lnr-check"></use></svg></i>Creative Design</li>
                                    <li>
                                        <i class="sico lnr-check"><svg><use xlink:href="#lnr-check"></use></svg></i>Fully Compatible</li>
                                </ul>
                            </div>
                        </div>
                        <div class="spacer height-40"></div>
                        <p>
                            <a href="#" title="VIEW MORE" class="button-01 border-radius-3 bg-accent3">VIEW MORE</a>
                        </p>
                    </div>
                    <div class="col-md-6">
                        <img src="/Portals/_default/ContentBuilder/minis-page/design-studio2/designStudio2-img08.png" alt="" class="img-Lazy"></div>
                </div>
                <div class="row">
                    <div class="col-md-12" data-noedit="">
                        <div class="spacer height-20"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    <style class="build-css" data-class="title-23">${csstemplate["title-23"]}</style>
    <style class="build-css" data-class="list-04">${csstemplate["list-04"]}</style>
    <style class="build-css" data-class="button-01">${csstemplate["button-01"]}</style>
    `
    }, {
        'thumbnail': 'minis-section/design-studio2/section-08.jpg',
        'category': '822',
        'html': `<div class="is-section is-box is-light-text">
    <div class="is-overlay">
        <div class="is-overlay-bg" style="background-image:url(&quot;/Portals/_default/ContentBuilder/minis-page/design-studio2/designStudio2-bg02.jpg&quot;)"></div>
    </div>
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container" dragwithouthandle="">
                <div class="row">
                    <div class="col-md-6">
                        <div dragwithouthandle=""></div>
                    </div>
                    <div class="col-md-6">
                        <div class="spacer height-100"></div>
                        <span class="color-accent2" style="font-size: 100px;line-height: 0px;position: absolute;">“</span>
                        <div class="spacer height-20"></div>
                        <h3>We Strongly Believe That Different People Will Bring 
                            <span style="color: rgb(146, 65, 13);">The Collision Spark Of Technology, Thought And Creativity.</span>
                        </h3>
                        <div class="spacer height-40"></div>
                        <p>
                            <a href="#" title="START NOW" class="button-01 border-radius-3 bg-accent2">START NOW</a>
                        </p>
                        <div class="spacer height-100"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    <style class="build-css" data-class="button-01">${csstemplate["button-01"]}</style>

    `
    }, {
        'thumbnail': 'minis-section/design-studio2/section-09.jpg',
        'category': '822',
        'html': `<div class="is-section is-box">
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container" dragwithouthandle="">
                <div class="row align-items-center">
                    <div class="col-md-12 m-auto text-center" style="max-width: 830px;">
                        <h3 class="title-23 mr-auto ml-auto mt-10 text-center" style="max-width: 570px;">Frequently Asked 
                            <span class="border-accent3">Questions</span>
                        </h3>
                        <div class="spacer height-20"></div>
                        <p>Sed nibh justo, posuere in ullamcorper eget, finibus ut turpis. Etiam odio nulla, venenatis vel pulvinar interdum, varius quis turpis. Cras nunc dui, finibus sit amet interdum non, luctus sit amet mauris.</p>
                        <div class="spacer height-40"></div>
                    </div>
                </div>
                <div class="row align-items-center">
                    <div class="col-md-8">
                        <div class="row faq-01-list">
                            <div class="col-md-6">
                                <div class="faq-01 edit-box">
                                    <div class="title">
                                        <h6>How do I change my password?</h6>
                                    </div>
                                    <p>Vestibulum suscipit vulputate dui vel molestie. Praesent et consequat tortor. </p>
                                    <div class="link">
                                        <a href="#" title="VIEW MORE" class="color-accent2" style="font-weight: 600;">VIEW MORE 
                                            <i class="sico fas-plus"><svg><use xlink:href="#fas-plus"></use></svg></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="faq-01 edit-box">
                                    <div class="title">
                                        <h6>What's the license for this theme?</h6>
                                    </div>
                                    <p>Donec vitae mi venenatis, rutrum libero sed, dictum purus. In dui neque tempus id volutpat.</p>
                                    <div class="link bold">
                                        <a href="#" title="VIEW MORE" class="color-accent2" style="font-weight: 600;">VIEW MORE 
                                            <i class="sico fas-plus"><svg><use xlink:href="#fas-plus"></use></svg></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row faq-01-list">
                            <div class="col-md-6">
                                <div class="faq-01 edit-box">
                                    <div class="title">
                                        <h6>How can I edit the page?</h6>
                                    </div>
                                    <p>Praesent dignissim dui at nulla consequat, necdign issim turpis facilisis.</p>
                                    <div class="link bold">
                                        <a href="#" title="VIEW MORE" class="color-accent2" style="font-weight: 600;">VIEW MORE 
                                            <i class="sico fas-plus"><svg><use xlink:href="#fas-plus"></use></svg></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="faq-01 edit-box">
                                    <div class="title">
                                        <h6>Can I have an invoice for my order?</h6>
                                    </div>
                                    <p>Aenean ac mi quis lectus tempor iaculis. Proin nec lectus sed ipsum venenatis suscipit. </p>
                                    <div class="link bold">
                                        <a href="#" title="VIEW MORE" class="color-accent2" style="font-weight: 600;">VIEW MORE 
                                            <i class="sico fas-plus"><svg><use xlink:href="#fas-plus"></use></svg></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 text-lg-right">
                    ${getSnippetCode("img-box11")} 
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12" data-noedit="">
                        <div class="spacer height-20"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style>.img-box11{position: relative;display: inline-block; } .img-box11 .cont{position: absolute;top: 0;left: 0;text-align: left;padding: 45px; } </style>
</div>
<style class="build-css" data-class="faq-01">${csstemplate["faq-01"]}</style>
<style class="build-css" data-class="title-23">${csstemplate["title-23"]}</style>
`
    }, {
        'thumbnail': 'minis-section/design-studio2/footer.jpg',
        'category': '822',
        'html': `<div class="is-section is-box is-align-center is-light-text layout-no-mb">
    <div class="is-overlay">
        <div class="is-overlay-bg" style="background-image:url(&quot;/Portals/_default/ContentBuilder/minis-page/design-studio2/designStudio2-footer-bg.jpg&quot;)"></div>
    </div>
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container" dragwithouthandle="">
                <div class="row">
                    <div class="col-md-12">
                        <img src="/Portals/_default/ContentBuilder/minis-page/logo-white-big.png" alt="">
                        <div class="spacer height-40"></div>
                        <p class="m-auto" style="max-width: 750px; color: rgb(188, 186, 200);">Proin quis neque faucibus, sodales nisl sit amet, finibus ipsum. Integer id sagittis mauris. Pellentesque habitant morbi tristique senectus et netus.</p>
                        <div class="spacer height-60"></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 designStudio2-footer-line">
                        <div class="spacer height-20"></div>
                        <h5 class="mb-25">About Us</h5>
                        <ul class="designStudio2-footer-list">
                            <li>
                                <a href="#" title="What we do">What we do</a>
                            </li>
                            <li>
                                <a href="#" title="Our features">Our features</a>
                            </li>
                            <li>
                                <a href="#" title="Our team">Our team</a>
                            </li>
                            <li>
                                <a href="#" title="Reasonable price">Reasonable price</a>
                            </li>
                        </ul>
                        <div class="spacer height-20"></div>
                    </div>
                    <div class="col-md-4 designStudio2-footer-line">
                        <div class="spacer height-20"></div>
                        <h5 class="mb-25">Contact Us</h5>
                        <p class="mb-0" style="color: rgb(188, 186, 200);">service.simple@gmail.com</p>
                        <p class="mb-0 color-accent3" style="font-size: 22px; font-weight: 700;">(845) 359-7777</p>
                        <p class="mb-0" style="color: rgb(188, 186, 200);">Yanta W Rd, Yanta District, Xian Shi, Shaanxi</p>
                        <div class="spacer height-20"></div>
                        <p>
                            <a href="#" title="VIEW MORE" class="designStudio2-footer-link">VIEW MORE 
                                <i class="sico lnr-arrow-right size-24 ml-10"><svg><use xlink:href="#lnr-arrow-right"></use></svg></i>
                            </a>
                        </p>
                    </div>
                    <div class="col-md-4">
                        <div class="spacer height-20"></div>
                        <h5 class="mb-25">Friendly links</h5>
                        <ul class="designStudio2-footer-list">
                            <li>
                                <a href="#" title="">Industry experts</a>
                            </li>
                            <li>
                                <a href="#" title="">FAQs</a>
                            </li>
                            <li>
                                <a href="#" title="">Contact us</a>
                            </li>
                            <li>
                                <a href="#" title="">Recent posts</a>
                            </li>
                        </ul>
                        <div class="spacer height-20"></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12" data-noedit="">
                        <div class="spacer height-60"></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <hr style="border-top-color:rgba(255,255,255,0.3)"></div>
                </div>
                <div class="row pt-10 pb-15">
                    <div class="col-md-6 text-md-left">
                        <p class="mb-15 mb-md-0 mt-md-4" style="color: rgb(188, 186, 200);">@ 2019 by DNNGo Corp. All Rights Resevered</p>
                    </div>
                    <div class="col-md-6 text-md-right">
                        <div class="edit-box">
                            <a class="social-03 facebook-f" href="#" title="">
                                <i class="sico fab-facebook-f"><svg><use xlink:href="#fab-facebook-f"></use></svg></i>
                            </a>
                            <a class="social-03 linkedin-in" href="#" title="">
                                <i class="sico fab-linkedin-in"><svg><use xlink:href="#fab-linkedin-in"></use></svg></i>
                            </a>
                            <a class="social-03 twitter mr-0" href="#" title="">
                                <i class="sico fab-twitter"><svg><use xlink:href="#fab-twitter"></use></svg></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style class="build-css" data-class="social-03">${csstemplate["social-03"]}</style>
    <style class="build-css" data-class="designStudio2-footer-line"> .designStudio2-footer-line{border-right: 1px dashed rgba(255, 255, 255, .3); }@media only screen and (max-width: 767px) {.designStudio2-footer-line{border:none;} } </style>
    <style class="build-css" data-class="designStudio2-footer-list"> .designStudio2-footer-list{margin: 0;padding: 0;list-style: none; } .designStudio2-footer-list li{margin: 0 0 10px; } .designStudio2-footer-list li a, .designStudio2-footer-list li a:link{color: #bcbac8; } .designStudio2-footer-list li a:hover{color: var(--color-accent3); } </style>
    <style class="build-css" data-class="designStudio2-footer-link">.designStudio2-footer-link,.designStudio2-footer-link:link{color: rgb(188, 186, 200);}.designStudio2-footer-link:hover{color:#FFF;}.designStudio2-footer-link .sico{vertical-align: top;} </style>

</div>
    `
    }

)


data_basic.designs.push({
        'thumbnail': 'minis-section/saas02/banner.jpg',
        'category': '823',
        'html': `<div class="is-section is-box is-light-text">
    <div class="is-overlay">
        <div class="is-overlay-bg" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/saas2/saas2-banner-bg.png&quot;); background-position: 40% 60%;"></div>
    </div>
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container" dragwithouthandle="">
                <div class="row">
                    <div class="col-md-12" data-noedit="">
                        <div class="spacer height-40 d-none d-md-block"></div>
                    </div>
                </div>
                <div class="row align-items-center">
                    <div class="col-md-5 left-full-column">
                        <div class="full-column-inner">
                            <div style="max-width:550px; margin-left: auto;" class="mr-lg-50 pl-md-30 pl-15">
                                <h1 style="font-weight: 600;">Designed For Events Management</h1>
                                <div class="spacer height-40"></div>
                                <p>Aliquam finibus venenatis velit. Vivas's'smus vulputate sodales mi ultrices imperdiet. Donec bibendum dignissim dui in venenatis. Vivamus rutrum porta sapien. Sed lectus orci porttitor ut pulvinar.</p>
                                <div class="spacer height-40"></div>
                                <div class="custom-module loading mr-lg-80" data-style="ajaxform-02" data-effect="ajaxform" data-module-desc="Contact Form" id="module-{id}" data-moduleid="{id}" data-settings="%7B%22style%22%3A%22ajaxform-02%22%2C%22message%22%3A%22Thank%20you%2C%20your%20message%20has%20been%20submitted%20to%20us.%22%7D"></div>
                            </div>
                            <div class="spacer height-40"></div>
                        </div>
                    </div>
                    <div class="col-md-7 right-full-column">
                        <div class="full-column-inner">
                            <div class="saas2-banner-img">
                                <img src="/Portals/_default/ContentBuilder/minis-page/saas2/saas2-banner-img.png" alt="" class="img-Lazy">
                                <a class="play-button is-lightbox" href="https://www.youtube.com/embed/AkcUoA2f-jU" data-ilightbox="youtube" title="">
                                    <span class="bg-accent2 is-light-text">
                                        <i class="sico fas-play size-18"><svg><use xlink:href="#fas-play"></use></svg></i>
                                    </span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style class="build-css" data-class="play-button">${csstemplate["play-button"]}</style>
<style class="build-css" data-class="saas2-banner-img"> .saas2-banner-img{position: relative;display: inline-block; } .saas2-banner-img .play-button{position: absolute;left: 50%;top: 50%;transform: translate(-50%,-50%); }</style>
    `
    }, {
        'thumbnail': 'minis-section/saas02/section-01.jpg',
        'category': '823',
        'html': `<div class="is-section is-box is-align-center layout-no-mb">
    <div class="is-overlay">
        <div class="is-overlay-bg" style="background-image: url(&quot;/Portals/_default/ContentBuilder/minis-page/saas2/saas2-bg01.png&quot;); background-size: auto; background-position: 50% 50%;"></div>
    </div>
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container" dragwithouthandle="">
            <div class="row align-items-center">
            <div class="col-md-12">
            <div class="m-auto" style="max-width: 770px;">
                <h3 style="font-weight: 600;">Why You Should Choose 
                    <span class="color-accent2" style="font-weight:700;">Our SaaS</span>
                </h3>
                <div class="spacer height-20"></div>
                <p>Suspendisse placerat blandit arcu et tempor. Duis sollicitudin arcu a pretium volutpat. Morbi quis eleifend nibh. Sed venenatis luctus gravida. Nulla non laoreet ante. </p>
            </div>
            <div class="spacer height-40"></div>
        </div>
            </div>
             ${getSnippetCode("infobox-16")} 
                <div class="row">
                    <div class="col-md-12" data-noedit="">
                        <div class="spacer height-80"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    `
    }, {
        'thumbnail': 'minis-section/saas02/section-02.jpg',
        'category': '823',
        'html': `<div class="is-section is-box is-align-center is-light-text">
	<div class="is-overlay">
		<div class="is-overlay-bg" style="background-image:url(&quot;/Portals/_default/ContentBuilder/minis-page/saas2/saas2-bg02.png&quot;)"></div>
	</div>
	<div class="is-boxes">
		<div class="is-box-centered">
			<div class="is-container layout-container" dragwithouthandle="">
				<div class="row">
					<div class="col-md-12 m-auto" style="max-width: 770px;">
						<h3 style="font-weight: 600;">Our Powerful Functions</h3>
						<div class="spacer height-20"></div>
						<p>Nullam eget arcu vel nibh placerat ullamcorper eu vitae justo. Fusce quis eros ultricies, luctus ante a,
							dignissim ex. Pellentesque eleifend</p>
						<div class="spacer height-20"></div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12" data-noedit="">
						<div class="spacer height-20"></div>
					</div>
				</div>
                ${getSnippetCode("iconbox-22")} 
			</div>
		</div>
	</div>
</div>
    `
    }, {
        'thumbnail': 'minis-section/saas02/section-03.jpg',
        'category': '823',
        'html': `<div class="is-section is-box layout-no-mb">
	<div class="is-boxes">
		<div class="is-box-centered">
			<div class="is-container layout-container" dragwithouthandle="">
				<div class="row align-items-center">
					<div class="col-lg-6 left-full-column pl-0 order-last order-lg-0">
						<div class="full-column-inner">
							<img src="/Portals/_default/ContentBuilder/minis-page/saas2/saas2-img02.png" alt="" class="img-Lazy"></div>
					</div>
					<div class="col-lg-6">
						<div class="spacer height-40 d-none d-md-block"></div>
						<h3 style="font-weight: 600;">This Wonderful SaaS Will Help
							<span class="color-accent2" style="font-weight:700;">Make Everything Easy</span>
						</h3>
						<div class="spacer height-20 mb-15"></div>
						 ${getSnippetCode("tab02")} 
						<div class="spacer height-40 d-none d-md-block"></div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12" data-noedit="">
						<div class="spacer height-40"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
    `
    }, {
        'thumbnail': 'minis-section/saas02/section-04.jpg',
        'category': '823',
        'html': `<div class="is-section is-box layout-no-mt">
	<div class="is-boxes">
		<div class="is-box-centered">
			<div class="is-container layout-container" dragwithouthandle="">
				<div class="row align-items-center">
					<div class="col-lg-6">
						<h3 style="font-weight: 600;">Our Superb Skills Will Guarantee You
							<span class="color-accent2" style="font-weight: 700;"> A Perfect SAAS Site</span>
						</h3>
						<div class="spacer height-20 mb-3"></div>
						<p>Pellentesque felis odio, suscipit ut diam sed, bibendum egestas neque. Morbi facilisis turpis a sapien
							elementum, sit amet convallis ipsum maximus. Proin quis maximus elit, id tempor libero.</p>
						<div class="spacer height-20"></div>
                        ${getSnippetCode("chart01")} 
					</div>
					<div class="col-lg-6 right-full-column pr-0">
						<div class="full-column-inner">
							<img src="/Portals/_default/ContentBuilder/minis-page/saas2/saas2-img03.png" alt="" class="img-Lazy"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
</div>
    `
    }, {
        'thumbnail': 'minis-section/saas02/section-05.jpg',
        'category': '823',
        'html': `<div class="is-section is-box is-light-text is-align-center">
	<div class="is-overlay">
		<div class="is-overlay-bg" style="background-image:url(&quot;/Portals/_default/ContentBuilder/minis-page/saas2/saas2-bg03.png&quot;)"></div>
	</div>
	<div class="is-boxes">
		<div class="is-box-centered">
			<div class="is-container layout-container" dragwithouthandle="">
				<div class="row justify-content-center">
					<div class="col-md-12" data-noedit="">
						<div class="spacer height-60"></div>
					</div>
				</div>
				<div class="row justify-content-center">
					<div class="col-md-12">
						<h2>Do Not Hesitate</h2>
						<h4 style="font-weight: 400;">A Free Opportunity To Explore Our Wonderful SAAS</h4>
					</div>
				</div>
				<div class="row justify-content-center">
					<div class="col-md-12" data-noedit="">
						<div class="spacer height-60"></div>
					</div>
				</div>
				<div class="row m-auto justify-content-center saas2-counter-01-line" style="max-width: 770px;">
					<div class="col-md-4">
						<div class="counter-01">
							<div class="box-title">
								<div class="custom-module loading" id="module-{id}" data-moduleid="{id}" data-effect="Number" data-html="false"
								 data-noedit="true" data-module-desc="Number" data-module="shortcode" data-settings="%7B%22number%22%3A%22300%22%7D"></div>
								<p>Buy and use</p>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="spacer height-40 d-md-none"></div>
						<div class="counter-01">
							<div class="box-title">
								<div class="custom-module loading" id="module-{id}" data-moduleid="{id}" data-effect="Number" data-html="false"
								 data-noedit="true" data-module-desc="Number" data-module="shortcode" data-settings="%7B%22number%22%3A%22278%22%7D"></div>
								<p>Free trial downloaded</p>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="spacer height-40 d-md-none"></div>
						<div class="counter-01">
							<div class="box-title">
								<div class="custom-module loading" id="module-{id}" data-moduleid="{id}" data-effect="Number" data-html="false"
								 data-noedit="true" data-module-desc="Number" data-module="shortcode" data-settings="%7B%22number%22%3A%22150%22%7D"></div>
								<p>Good reviews</p>
							</div>
						</div>
					</div>
				</div>
				<div class="row justify-content-center">
					<div class="col-md-12" data-noedit="">
						<div class="spacer height-60"></div>
					</div>
				</div>
				<div class="row justify-content-center">
					<div class="col-md-12">
						<a href="#" title="DOWNLOAD NOW" class="button-13">DOWNLOAD NOW</a>
					</div>
				</div>
				<div class="row justify-content-center">
					<div class="col-md-12" data-noedit="">
						<div class="spacer height-60"></div>
					</div>
				</div>
			</div>
		</div>
    </div>
    <style class="build-css" data-class="counter-01">${csstemplate["counter-01"]}</style>
    <style class="build-css" data-class="button-13">${csstemplate["button-13"]}</style>
	<style data-class="saas2-counter-01-line>" class="build-css">
		@media only screen and (min-width: 768px) {
			.saas2-counter-01-line>div {
				border-right: 1px dashed rgba(255, 255, 255, .2);
			}
			.saas2-counter-01-line>div:last-child {
				border-right: none
			}
		}
	</style>
</div>
    `
    }, {
        'thumbnail': 'minis-section/saas02/section-06.jpg',
        'category': '823',
        'html': `<div class="is-section is-box">
	<div class="is-boxes">
		<div class="is-box-centered">
			<div class="is-container layout-container" dragwithouthandle="">
				<div class="row text-center align-items-start justify-content-start">
					<div class="col-md-12 m-auto" style="max-width: 770px;">
						<h3>
							<span style="font-weight: 600;">Affordable </span>
							<span class="color-accent2" style="font-weight: 700;">Pricing Plans</span>
						</h3>
						<div class="spacer height-20"></div>
						<p>Etiam quis facilisis augue. Maecenas a lorem sit amet sem egestas sodales ut quis augue. Vestibulum accumsan
							suscipit ligula, a dapibus elit malesuada et.</p>
					</div>
				</div>
				<div class="row text-center align-items-start justify-content-start">
					<div class="col-md-12" data-noedit="">
						<div class="spacer height-80"></div>
					</div>
				</div>
                ${getSnippetCode("price-11")} 
			</div>
		</div>
	</div>
	<style>
		
	</style>
</div>
    `
    }, {
        'thumbnail': 'minis-section/saas02/section-07.jpg',
        'category': '823',
        'html': `<div class="is-section is-box is-align-center is-light-text layout-no-mb">
    <div class="is-overlay">
        <div class="is-overlay-bg" style="background-image:url(&quot;/Portals/_default/ContentBuilder/minis-page/saas2/saas2-bg04.png&quot;)"></div>
    </div>
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container" dragwithouthandle="">
                <div class="row">
                    <div class="col-md-12" data-noedit="">
                        <div class="spacer height-100"></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 m-auto" style="max-width: 770px;">
                        <h3 class="m-auto" style="max-width: 570px; font-weight: 600;">
                            <span style="font-weight: 700;">Get Started With Our Free Trial,</span> You Will Love It So Much.</h3>
                        <div class="spacer height-40"></div>
                        <p>Mauris sagittis lorem sit amet libero pretium, varius molestie quam vehicula. Proin nec pellentesque tortor. In ex nibh, elementum ut commodo eu, semper ut arcu.</p>
                        <div class="spacer height-20"></div>
                        <p class="mb-0">
                            <a href="#" title="">
                                <img class="mr-10 ml-10 mb-20 img-Lazy" src="/Portals/_default/ContentBuilder/minis-page/saas2/saas2-btn01.png" alt=""></a>
                            <a href="#" title="">
                                <img class="mr-10 ml-10 mb-20 img-Lazy" src="/Portals/_default/ContentBuilder/minis-page/saas2/saas2-btn02.png" alt=""></a>
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        ${getSnippetCode("carousel-3d-style01")} 
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12" data-noedit="">
                        <div class="spacer height-80 mb-md-15"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    `
    }, {
        'thumbnail': 'minis-section/saas02/section-08.jpg',
        'category': '823',
        'html': `<div class="is-section is-box layout-no-mb layout-no-mt">
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container layout-container" dragwithouthandle="">
                <div class="row align-items-center">
                    <div class="col-md-5">
                        <h3>
                            <span style="font-weight: 600;">All Those Words Come From </span>
                            <span class="color-accent2" style="font-weight: 700;">Deep Inside Of Clients</span>
                        </h3>
                        <div class="spacer height-20 mb-10"></div>
                        <p class="mb-25">Sed non aliquam sapien. Curabitur a volutpat urna, ut posuere felis. Curabitur venenatis, justo quis suscipit dapibus.</p>
                        <h6>Would like to work more efficient? Be one of our clients now.</h6>
                        <div class="spacer height-20"></div>
                        <p>
                            <a href="#" title="Buy It Now" class="button-13 bg-accent mb-15" style="margin-right: 12px;">BUY IT NOW</a>
                            <a href="#" title="Contact us" class="button-13 bg-accent2 mb-15">CONTACT US</a>
                        </p>
                        <div class="spacer height-40"></div>
                    </div>
                    <div class="col-md-7">
                    ${getSnippetCode("ourteam-05")} 
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12" data-noedit="">
                        <div class="spacer height-60 mb-5 d-none d-lg-block"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><style class="build-css" data-class="button-13">${csstemplate["button-13"]}</style>
    `
    }, {
        'thumbnail': 'minis-section/saas02/section-09.jpg',
        'category': '823',
        'html': `<div class="is-section is-box is-align-center layout-no-mb layout-no-mt">
	<div class="is-boxes">
		<div class="is-box-centered">
			<div class="is-container layout-container" dragwithouthandle="">
				<div class="row">
					<div class="col-md-12" data-noedit="">
						<div class="spacer height-80 mb-md-10 d-none d-lg-block"></div>
					</div>
				</div>
				<div class="row align-items-center justify-content-center">
					<div class="col-md-12 m-auto" style="max-width: 770px;">
						<h3>
							<span style="font-weight: 600;">Our Loyalty Cooperation </span>
							<span class="color-accent2" style="font-weight: 700;">Partners</span>
						</h3>
						<div class="spacer height-20"></div>
						<p>Quisque dignissim ligula at magna bibendum vehicula. Aliquam porta eu lacus eget consequat. Aenean consequat
							sem et ipsum eleifend, nec luctus urna accumsan. </p>
					</div>
				</div>
				<div class="row align-items-center justify-content-center">
					<div class="col-md-12" data-noedit="">
						<div class="spacer height-20"></div>
					</div>
				</div>
				<div class="row align-items-center justify-content-center">
					<div class="col-md-1/5 mb-25 mb-md-50">
						<span>
							<a href="#" title="">
								<img src="/Portals/_default/ContentBuilder/minis-page/saas2/saas2-client01.png" alt="" class="img-Lazy"></a>
						</span>
					</div>
					<div class="col-md-1/5 mb-25 mb-md-50">
						<span>
							<a href="#" title="">
								<img src="/Portals/_default/ContentBuilder/minis-page/saas2/saas2-client02.png" alt="" class="img-Lazy"></a>
						</span>
					</div>
					<div class="col-md-1/5 mb-25 mb-md-50">
						<span>
							<a href="#" title="">
								<img src="/Portals/_default/ContentBuilder/minis-page/saas2/saas2-client03.png" alt="" class="img-Lazy"></a>
						</span>
					</div>
					<div class="col-md-1/5 mb-25 mb-md-50">
						<span>
							<a href="#" title="">
								<img src="/Portals/_default/ContentBuilder/minis-page/saas2/saas2-client04.png" alt="" class="img-Lazy"></a>
						</span>
					</div>
					<div class="col-md-1/5 mb-25 mb-md-50">
						<span>
							<a href="#" title="">
								<img src="/Portals/_default/ContentBuilder/minis-page/saas2/saas2-client05.png" alt="" class="img-Lazy"></a>
						</span>
					</div>
					<div class="col-md-1/5 mb-25 mb-md-50">
						<span>
							<a href="#" title="">
								<img src="/Portals/_default/ContentBuilder/minis-page/saas2/saas2-client06.png" alt="" class="img-Lazy"></a>
						</span>
					</div>
					<div class="col-md-1/5 mb-25 mb-md-50">
						<span>
							<a href="#" title="">
								<img src="/Portals/_default/ContentBuilder/minis-page/saas2/saas2-client07.png" alt="" class="img-Lazy"></a>
						</span>
					</div>
					<div class="col-md-1/5 mb-25 mb-md-50">
						<span>
							<a href="#" title="">
								<img src="/Portals/_default/ContentBuilder/minis-page/saas2/saas2-client08.png" alt="" class="img-Lazy"></a>
						</span>
					</div>
					<div class="col-md-1/5 mb-25 mb-md-50">
						<span>
							<a href="#" title="">
								<img src="/Portals/_default/ContentBuilder/minis-page/saas2/saas2-client09.png" alt="" class="img-Lazy"></a>
						</span>
					</div>
					<div class="col-md-1/5 mb-25 mb-md-50">
						<span>
							<a href="#" title="">
								<img src="/Portals/_default/ContentBuilder/minis-page/saas2/saas2-client10.png" alt="" class="img-Lazy"></a>
						</span>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12" data-noedit="">
						<div class="spacer height-100 mb-15 mb-md-0"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
    `
    }, {
        'thumbnail': 'minis-section/saas02/footer.jpg',
        'category': '823',
        'html': `<div class="is-section is-box layout-no-mt">
	<div class="is-overlay">
		<div class="is-overlay-bg" style="background-image:url(&quot;/Portals/_default/ContentBuilder/minis-page/saas2/saas2-footer-bg.png&quot;)"></div>
	</div>
	<div class="is-boxes">
		<div class="is-box-centered">
			<div class="is-container layout-container" dragwithouthandle="">
				<div class="row align-items-center">
					<div class="col-md-6">
						<div class="saas-footer-formbox">
							<h4 class="title">Send An Email To Us</h4>
                             ${getSnippetCode("ajaxform-03")} 
						</div>
					</div>
					<div class="col-md-6 is-light-text pl-lg-80">
						<div class="spacer height-40"></div>
                        ${getSnippetCode("iconbox-20")} 
						<p style="opacity: 0.7;">Feel free to contact us via following ways, we will try our best to answer all questions
							within 24 hours.</p>
						<div class="saas2-footer-info">
							<p>Phone:
								<span class="color-accent">+1 845-359-0545</span>
							</p>
							<p>Eamil: service.simple@gmail.com</p>
							<p>Address: Yanta W Rd, Yanta District, Xian Shi, Shaanxi</p>
						</div>
						<div class="saas2-footer-info2">
							<h6 class="title">Working Time</h6>
							<p>Monday-Friday: 8:00am-5:30pm</p>
							<p>Saturday: 10:00am-2:00pm</p>
							<p>Sundey: close</p>
						</div>
					</div>
				</div>
			</div>
			<div class="pt-35 pb-35 is-light-text layout-no-mb layout-no-mt" style="background-color:rgba(0,0,0,.2)">
				<div class="is-container layout-container" dragwithouthandle="">
					<div class="row align-items-center">
						<div class="col-md-3 text-center text-md-left pt-5 pb-5">
							<img src="/Portals/_default/ContentBuilder/minis-page/logo-white.png" alt="" class="img-Lazy"></div>
						<div class="col-md-6 pt-5 pb-5">
							<p class="mb-0 text-center" style="opacity: 0.7;">@ 2019 by DNNGo Corp. All Rights Resevered</p>
						</div>
						<div class="col-md-3 pt-5 pb-5">
							<div class="edit-box text-center text-md-right">
								<a class="social-03 mb-0 facebook-f" href="#" title="">
									<i class="sico fab-facebook-f"><svg>
											<use xlink:href="#fab-facebook-f"></use>
										</svg></i>
								</a>
								<a class="social-03 mb-0 linkedin-in" href="#" title="">
									<i class="sico fab-linkedin-in"><svg>
											<use xlink:href="#fab-linkedin-in"></use>
										</svg></i>
								</a>
								<a class="social-03 mb-0 twitter" href="#" title="">
									<i class="sico fab-twitter"><svg>
											<use xlink:href="#fab-twitter"></use>
										</svg></i>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
    <style class="build-css" data-class="social-03">${csstemplate["social-03"]}</style>
	<style class="build-css" data-class="saas-footer-formbox">
		.saas-footer-formbox {
			background-color: #FFF;
			padding: 50px;
			box-shadow: 0 0 20px rgba(0, 0, 0, .1);
		}

		.saas-footer-formbox .title {
			margin: 0 0 32px;
		}

		.saas-footer-formbox .ajaxform-03 label {
			margin: 0 0 10px;
		}

		.saas-footer-formbox .ajaxform-03 .submit_but {
			box-shadow: 0 0 8px rgba(0, 0, 0, .2);
		}

		@media only screen and (max-width:767px) {
			.saas-footer-formbox {
				padding: 30px;
			}
		}
	</style>
	<style class="build-css" data-class="saas2-footer-info">
		.saas2-footer-info {
			border-left: 2px solid #ffffff;
			padding-left: 21px;
			margin: 50px 0;
			font-size: 18px;
		}

		.saas2-footer-info p {
			margin: 0;
			padding: 3px 0;
		}

		.saas2-footer-info2 .title {
			font-size: 18px;
		}

		.saas2-footer-info2 p {
			margin: 0;
			opacity: 0.7;
		}
	</style>
</div>
    `
    }

)