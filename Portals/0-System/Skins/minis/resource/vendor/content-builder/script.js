﻿/*dnn*/
var currentVendorUrl = {};

var skrollrrefresh = {
	refresh: function () {
		return false
	}
};
var skrollr = {
	get: function () {
		return skrollrrefresh;
	}
};

// Snippet category
var selSnippetCat = {
	"Basic": 120,
	"Button": 700,
	"Title": 701,
	"Pricing": 702,
	"Our Team": 703,
	"Info Box": 704,
	"Icon Box": 705,
	"Image Box": 706,
	"Social": 707,
	"Client List": 708,
	"Accordion": 800,
	"Carousel": 801,
	"Progressbar": 802,
	"Testimonials": 803,
	"Counter": 805,
	"Countdown": 806,
	"Form": 807,
	"Google Map": 809,
	"Portfolio": 810,
	"Tab": 811,
	"FAQ": 812,
	"Step": 813,
	"Flip Box": 814,
	"List": 815,
	"Hotspot":816,
	"Time Line":817,
	"Chart":818
}
// Section category
var selSectionCat = {
	"Basic": 1,
	"Layout": 999,
	"Home01": 800,
	"Home02": 801,
	"Home03": 802,
	"Home04": 803,
	"Home05": 804,
	"Home06": 805,
	"Home07": 806,
	"Home08": 807,
	"Home09": 808,
	"Home10": 809,
	"Home11": 810,
	"Home12": 811,
	"Home13": 812,
	"Home14": 813,
	"APP 01": 814,
	"APP 02": 821,
	"SEO 01": 815,
	"Organic Food": 818,
	"SAAS": 819,
	"SAAS02": 823,
	"Design Studio": 820,
	"Design Studio2": 822,
	"Page": 816,
	"Coming Soon": 817
}

var  validationEngineLanguage = VerifyJS[1] ? VerifyJS[1].split("?")[0] : VerifyJS[0].split("?")[0] ;

var skinVendorUrl = {
	Accordions: {
		js: "[SkinPath]resource/vendor/Accordion/jquery-accordion.js",
		jsasync: true,
	},
	vivus: {
		js: "[SkinPath]resource/vendor/vivus/vivus.min.js",
		jsasync: true,
	},
	swiper: {
		js: "[SkinPath]resource/vendor/swiper/swiper.min.js",
		css: "[SkinPath]resource/vendor/swiper/swiper.min.css",
		jsasync: true,
		cssasync: true
	},
	minilightbox: {
		js: "[SkinPath]resource/vendor/minilightbox/minilightbox.min.js",
		css: "[SkinPath]resource/vendor/minilightbox/minilightbox.css",
		jsasync: true,
		cssasync: true
	},
	lazyloading: {
		js: "[SkinPath]resource/vendor/scrollLoading/scrollLoading.js",
		css: "[SkinPath]resource/vendor/scrollLoading/scrollLoading.css",
		jsasync: true,
	},
	visible: {
		js: "[SkinPath]resource/vendor/visible/visible.js",
		jsasync: true,
	},
	soon: {
		css: "[SkinPath]resource/vendor/soon/soon.min.css",
		js: "[SkinPath]resource/vendor/soon/soon.min.js",
		jsasync: true,
		cssasync: true
	},
	ajaxform: {
		css:  "[SkinPath]resource/vendor/ajaxform/jquery.ajaxform.css",
		js: "[SkinPath]resource/vendor/ajaxform/jquery.ajaxform.js",
		jsasync: true,
	},
	validationEngineLanguage: {
		js: validationEngineLanguage
	},
	validationEngine: {
		js: validationEngineLanguage.replace("jquery.validationEngine-en.js","jquery.validationEngine.js"),
		jsasync: true,
	},
	Masonry: {
		css: "[SkinPath]resource/vendor/isotope/isotope.css",
		js: "[SkinPath]resource/vendor/isotope/isotope.pkgd.min.js",
		jsasync: true,
	},
	gmap: {
		js: "[SkinPath]resource/vendor/map/jquery.gmap.min.js",
		jsasync: true,
	},
	tab: {
		js: "[SkinPath]resource/vendor/easyResponsiveTabs/easyResponsiveTabs.js",
		jsasync: true,
	},
	step: {
		js: "[SkinPath]resource/vendor/step/step.js",
		jsasync: true,
	},
	step2: {
		js: "[SkinPath]resource/vendor/step/step2.js",
		jsasync: true,
	},
	hotspot: {
		js: "[SkinPath]resource/vendor/hotspot/hotspot.js",
		jsasync: true,
	},
	easyPieChart: {
		js: "[SkinPath]resource/vendor/easyPieChart/jquery.easypiechart.min.js",
		jsasync: true,
	}
}
var VendorUrlAlias = {
	visible: [".animation.number", ".icon-svg", ".animation.bar", ".animation.text-slide"],
	swiper: [".swiper-container"],
	minilightbox: [".is-lightbox"],
	lazyloading: [".img-Lazy", ".iframe-Lazy"],
	vivus: [".icon-svg"],
	validationEngineLanguage: [".ajaxform"],
	validationEngine: [".ajaxform"],
	ajaxform: [".ajaxform"],
	Masonry: [".isotope-grid"],
	gmap: [".builder-gmap"],
	tab: [".verticalTab_Left", ".verticalTab_Right", ".horizontalTab_Bottom", ".horizontalTab_Top"],
	step: [".step-01"],
	step2: [".step-02"],
	easyPieChart: [".decorate"]

}
var modulesIcon = false;
var ClearSaveJS = false;

var svgAnimationIcon = false;
var svgLinearicons = false;
var svgFontawesome = false;


var customSectionCode = [];
var customSectionCodeNew = {};
var customSectionCodeIndex = false;

var AddReplace = false;

var isSectionCode = false;

var $isbox = $(".is-box").eq(0);
var $isSection = $(".is-section").eq(0);

var SectionCatNumber = 11;
var snippetCatNumber = 10;

var addHistory = true;
var History_cb =0;
var cuurHistory =0;
var maxHistory = 50;
var midHistoryIndexed =[];
var SelectHistory =false;
var HistoryTime = 1000;
if(typeof window.localStorage == "undefined"){
	var addHistory = false;
}

 
$.ajax({
	url: SkinPath + "resource/icons-svg/fontawesome.svg",
	async: true,
	success: function (data) {
		svgFontawesome = $(data);
	}
})
$.ajax({
	url: SkinPath + "resource/icons-svg/linearicons.svg",
	async: true,
	success: function (data) {
		svgLinearicons = $(data);
	}
})

var LinkNavSelect = $("<ul class=\"link-list-select\">");

if(typeof GetTabListServiceUrl != undefined){
	$.post(GetTabListServiceUrl + $(".d-wrapper").eq(0).data("mid"),function (data) {
			if(data){
				data = jQuery.parseJSON(data);
				for (var key in data) {
					if(data[key]["Url"]){
						LinkNavSelect.append('<li value="'+data[key]["Url"]+'">'+data[key]["IndentedTabName"]+'</li>')
					}
				}
				$(".link-list-select").each(function(){
					if(!$(this).html()){
						$(this).html(LinkNavSelect.html());
					}
				})
			}
	}
	);
}
jQuery(document).ready(function ($) {
	$.post(GetSectionListServiceUrl + $(".content-builder.d-wrapper").eq(0).data("mid"), function (data) {
		if (data) {
			data = jQuery.parseJSON(data);
			customSectionCode = customSectionCode.concat(data["Results"]);
		}
		customSectionCodeIndex = true;
	})


	function HasVendorUrl(g) {
		for (var key in g) {

			if (VendorUrlAlias[key]) {
				var has = false;
				VendorUrlAlias[key].forEach(function (j) {
					if ($(`${j}`).length) {
						has = true;
						return;
					}
				})
				if (!has) delete g[key];
			} else {
				if ($(`.custom-module[data-effect="${key}"]`).length == 0) delete g[key];
			}
		}




		return g;
	}

	function randomRangeId(num) {
		var returnStr = "",
			charStr = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
		for (var i = 0; i < num; i++) {
			var index = Math.round(Math.random() * (charStr.length - 1));
			returnStr += charStr.substring(index, index + 1);
		}
		return returnStr;
	}


	var googleFont = [];
	var contentFont = {};

	$("head link").each(function () {
		if (this.href.indexOf("googleapis") != -1) {
			if ($(this).attr("class") != "contentbox-google") {
				googleFontLink(this.href.split("family=")[1].split("&")[0]);
			} else {
				googleFont.push(this.href.split("family=")[1].split("&")[0]);
			}
		} else if ($(this).data("name") == "contentstyle") {
			contentFont[$(this).data("class")] = $(this).attr("href").replace(SkinPath,"[SkinPath]");
		}
	})






	googleFont = googleFont.join("|").split("|");
	googleFont = googleFont.filter(function (s) {
		return s && s.trim();
	});
	var new_googleFont = [];
	for (var i = 0; i < googleFont.length; i++) {
		var items = googleFont[i];
		if ($.inArray(items, new_googleFont) == -1) {
			new_googleFont.push(items);
		}
	}
	googleFont = new_googleFont;


	$(document).on("mousemove", '.is-sidebar-overlay', function () {
		if ($(".ui-sortable-placeholder").parents(".d-wrapper").length && !$(".ui-sortable-placeholder").parents(".d-wrapper").hasClass("is-wrapper")) {
			$(".d-wrapper").removeClass("is-wrapper");
			$(".ui-sortable-placeholder").parents(".d-wrapper").addClass("is-wrapper").focus().siblings().removeClass("is-wrapper").blur().change();
		}
	})



	function googleFontLink(g) {
		if (g) {
			$("head").append('<style type="text/css" class="googleapis-style">@import url(\'//fonts.googleapis.com/css?family=' + g + '\')</style>');
		}
	}
	googleFont.forEach(function (v, i) {
		googleFontLink(v);
	})

	function contentFontLink(g) {
		if (g) { 
			$("head").after('<style type="text/css" class="contentfont-style">@import url(\'' + g.replace("[SkinPath]",SkinPath) + '\')</style>');
		}
	}




	var lastIndex = 0,
		lastlength = 0;
	var pagemodification = false;
	var preview = false;
	//Enable editing

	$(".d-wrapper").each(function (index) {
		var ethis = this;
		var e = $(this);
		var index = index;
		var $isbox = '';
		e.data("index", index);
		e.find(".d-placeholder").remove();
		e.append('<div class="d-placeholder"><div><span>Click to add section</span></div></div>');


		if (!ethis["Builder"]) {
			ethis["Builder"] = {
				"settings": {},
			}
		}
		if (e.data("previewid")) {
			preview = e;
			e.data("change", true)
		}

		e.find('div[dragwithouthandle=""]').each(function () {

			if (!$(this).siblings().length && $(this).html() == "") {
				$(this).after('<div class="is-builder ui-sortable"></div>');
				$(this).remove();
			}
		})

		e.before(e.find(".section-svg"));


		if (ethis["Builder"]["VendorUrl"]) {
			for (j in ethis["Builder"]["VendorUrl"]) {
				currentVendorUrl[j] = ethis["Builder"]["VendorUrl"][j]
			}
		}

		//odd
		if (JSON.stringify(contentFont) != "{}") {
			if (!ethis["Builder"]["contentFont"]) {
				ethis["Builder"]["contentFont"] = {};
			}
			for (j in contentFont) {
				if (e.find("." + j).length) {
					ethis["Builder"]["contentFont"][j] = contentFont[j];
				}
			}
		}
		//new
		if (ethis["Builder"]["contentFont"]) {
			for (j in ethis["Builder"]["contentFont"]) {
				contentFont[j] = ethis["Builder"]["contentFont"][j];
			}
		}

		//odd
		if (googleFont) {
			ethis["Builder"]["googleFont"] = googleFont.join("|").split("|");
		}


		if (ethis["Builder"]["CodeCSS"]) {
			for (j in ethis["Builder"]["CodeCSS"]) {
				if(j){
					$('head').append(`<style class="build-css" data-class="${j}">${ethis["Builder"]["CodeCSS"][j]}</style>`)
				}
			}
			$("#ContentBuilder" + e.data("mid") + "-CodeCSS").remove();
		}


		e.find(".custom-module").each(function () {
			$(this).attr('data-noedit', "true");
			$(this).attr("contenteditable", "false");
			!$(this).attr('data-module-desc') ? $(this).attr('data-module-desc', $(this).data("effect")) : false;
			$(this).attr('data-module', "shortcode");
			if (ethis["Builder"]["settings"]) {
				$(this).attr("data-settings", ethis["Builder"]["settings"][$(this).data("moduleid")])
			}
			$(this).data('hasevent', true);
		})


		if (ethis["CodeCSS"]) {
			var CodeCSS = ethis["CodeCSS"];
			for (j in CodeCSS) {
				if(j){
					$('head').append(`<style class="build-css" data-class="${j}">${CodeCSS[j]}</style>`);
				}
			}
		}

		e.find(".edit-box").attr("contenteditable", "true");
		e.find(".img-Lazy,.iframe-Lazy").each(function () {
			if ($(this).attr("data-src")) {
				$(this).attr("src", $(this).attr("data-src")).removeAttr("data-src").removeClass("Lazy-loading");
			}
		})
		e.find(".img-Lazy-warp").removeClass("img-Lazy-warp");
		e.find(".load-over").removeClass("load-over");

		var historyDelay = '',initialHistory =true,HistoryscrollTop=0;

		if (!e.data('contentbox')) {

			e.contentbox({
				framework: 'bootstrap',
				//   disableStaticSection:true,
				modulePath: SkinPath + 'Resource/vendor/content-builder/assets/modules/',
				designPath: SkinPath + 'Resource/vendor/content-builder/assets/designs/',
				contentStylePath: SkinPath + 'Resource/vendor/content-builder/assets/styles/',
				iconselect: SkinPath + 'Resource/vendor/content-builder/assets/ionicons/icons.html',
				snippetData: SkinPath + 'Resource/vendor/content-builder/assets/minimalist-blocks/snippetlist.html',
				coverImageHandler: SaveImageServiceUrl + e.data("mid"),
				/* for uploading section background */
				largerImageHandler: SaveImageServiceUrlByLarger + e.data("mid"),
				/* for uploading larger image */
				moduleConfig: [{
					"moduleSaveImageHandler": SaveImageServiceUrlBySlider + e.data("mid"),
					/* for module purpose image saving (ex. slider) */
				}],
				htmlSyntaxHighlighting: true,
				animatedSorting: true,
				dragWithoutHandle: true,
				scrollableEditingToolbar: false,
				onRender: function () {
					var e = $(".is-wrapper");
					var has = false;

					e.find(".is-builder .is-builder").each(function () {
						if (!$(this).find(".row-add-initial").length) {
							if ($(this).find('.row > .col-md-12:not([class*="col-xl"]):not([class*="col-lg"]):not([class*="col-sm"]) > .row').length) {
								$(this).after($(this).children(".row").children(".col-md-12").children()).remove();
							} else {
								$(this).after($(this).children()).remove();
							}
						}
					})

					e.find('.row > .col-md-12:not([class*="col-xl"]):not([class*="col-lg"]):not([class*="col-sm"])').each(function () {
						if (($(this).children(".row").length && !$(this).children("p,h1,h2,h3,h4,h5,h6,a,span").length && !$(this).siblings('div[class*="col"]').length) || $(this).parent().parent().parent().hasClass("row")) {
							$(this).next(".is-row-tool").remove();
							$(this).unwrap();
							$(this).children().unwrap();
							$("#divCellTool").hide();
							has = true;
						}
					})
					if (has) {
						has = false;
						if (e.data("contentbox")) {
							e.data("contentbox").setup();
						}
					}
					e.find(".custom-module").each(function () {
						if ($(this).data("moduleid").indexOf("{id") != -1 || $('.custom-module[id="' + $(this).attr("id") + '"]').length > 1) {
							var id = makeid();
							$(this).attr("id", "module-" + id);
							$(this).attr("data-moduleid", id);
						}
					})
					e.find('div[dragwithouthandle=""]').each(function () {
						if (!$(this).siblings().length && $(this).html() == "") {

							$(this).after('<div class="is-builder ui-sortable"></div>');

							$(this).remove();
						}
					})
					e.find(".tabs-initialize").each(function () {
						var ibox = $(this);
						var row = $(this);
						if (ibox.parent(".col-md-12").parent(".row").length) {
							row = ibox.parent(".col-md-12").parent(".row");
						}
						ibox.children("li").each(function () {
							row.after('<div class="row tabs-content" data-content="' + ibox.attr("id") + '"><div class="col-md-12"><p>The placeholder text</p></div></div>');
						})
						ibox.removeClass("tabs-initialize");
						//	if(e.data("contentbox")){
						//		e.data("contentbox").setup();
						//	}
					})
					if(typeof AOS !="undefined"){
						AOS.init({
							duration: 1200
						});
					}

				},

				onChange: function () {
					var timeoutId;
					var e = $(".is-wrapper");
					var id = e.data("mid");
					clearTimeout(timeoutId);
					timeoutId = setTimeout(function () {
						saveImages(id);
					}, 1000);
					onChangeEvent();


					var hasplaceholder = false;
					e.find(".builder-placeholder").each(function () {
						if (!$(this).siblings().length) {
							$(this).removeClass("builder-placeholder");
							hasplaceholder = true;
						} else {
							$(this).remove();
						}
					})
					if (hasplaceholder) {
						hasplaceholder = false;
						e.data("contentbox").setup();

					}
					setTimeout(function () {
						e.find(".is-section-tool ~ .is-section-tool").remove();
					}, 200);


					contentBoxHistory();
					e.data("change", true);
				},

				onHistory:function () {
					var e = $(".is-wrapper");
					contentBoxHistory();
				},
				useSidebar: true,
				enableContentStyle: true
			})
			if (index != 0) {
				$("#divCb > svg ~ svg").remove();
				$(".is-sidebar").prevAll(".is-sidebar").remove();
				$(".is-modal.delsectionconfirm ~ .is-modal.delsectionconfirm").remove();
				$(".is-modal.editsection ~ .is-modal.editsection").remove();
				$(".is-modal.editmodule ~ .is-modal.editmodule").remove();
				$(".is-modal.editcustomcode ~ .is-modal.editcustomcode").remove();
				$(".is-modal.editbox ~ .is-modal.editbox").remove();
				$(".is-modal.gradientcolor ~ .is-modal.gradientcolor").remove();
				$(".is-modal.customcolor ~ .is-modal.customcolor").remove();
				$(".is-modal.viewfullhtml ~ .is-modal.viewfullhtml").remove();
				$(".is-modal.pickphoto ~ .is-modal.pickphoto").remove();
				$(".is-modal.applytypography ~ .is-modal.applytypography").remove();
				$(".is-modal.addsection ~ .is-modal.addsection").remove();
				$("#divSidebarSections ~ #divSidebarSections").remove();
				$("#divSidebarSnippets ~ #divSidebarSnippets").remove();
				$("#divSidebarSource ~ #divSidebarSource").remove();
				$("#divSidebarTypography ~ #divSidebarTypography").remove();
				$("#divCustomSidebarArea1 ~ #divCustomSidebarArea1").remove();
				$("#divCustomSidebarArea2 ~ #divCustomSidebarArea2").remove();

			} else {
				e.click();
			}
		function contentBoxHistory() {
			var e = $(".is-wrapper");
			if(addHistory){
				clearInterval(historyDelay);
				HistoryscrollTop = $(window).scrollTop();
				historyDelay =setTimeout(function(){
					if(SelectHistory && sessionStorage.getItem('cb-history-'+SelectHistory)){
						History_cb ++;
						sessionStorage.setItem('cb-history-'+History_cb, sessionStorage.getItem('cb-history-'+SelectHistory));
						if(History_cb - maxHistory > 0){
							sessionStorage.removeItem('cb-history-'+ (History_cb - maxHistory));
						}
						var mid = midHistoryIndexed[SelectHistory-1];
							midHistoryIndexed.push({"mid":mid["mid"],"position":mid["position"],"initial":false}) ;
						SelectHistory = false;
					}
					if(initialHistory && sessionStorage.getItem('cb-history-'+e.data("mid")+"-initial")){
						History_cb ++;
						sessionStorage.setItem('cb-history-'+History_cb, sessionStorage.getItem('cb-history-'+e.data("mid")+"-initial"));
						sessionStorage.removeItem('cb-history-'+e.data("mid")+"-initial");
						if(History_cb - maxHistory > 0){
							sessionStorage.removeItem('cb-history-'+ (History_cb - maxHistory));
						}
						midHistoryIndexed.push({"mid":e.data("mid"),"position":HistoryscrollTop,"initial":true}) ;
						initialHistory =false;
					}
					
					if(e.data("contentbox").html() && sessionStorage.getItem('cb-history-'+History_cb) != e.data("contentbox").html()){
						History_cb ++;
						sessionStorage.setItem('cb-history-'+History_cb,e.data("contentbox").html());
						if(History_cb - maxHistory > 0){
							sessionStorage.removeItem('cb-history-'+ (History_cb - maxHistory));
						}
						cuurHistory = History_cb;
						midHistoryIndexed.push({"mid":e.data("mid"),"position":HistoryscrollTop,"initial":false}) ;
						$("#historyPrevButton").removeClass("disabled");
						$("#historyNextButton").addClass("disabled");
					}
				},HistoryTime)
			};
		}
			if(addHistory){
				sessionStorage.setItem('cb-history-'+e.data("mid")+"-initial",e.data("contentbox").html());
			}

		}

		e.on("click focus", function () {
			$(".d-wrapper").removeClass("is-wrapper");
			e.addClass("is-wrapper").siblings().removeClass("is-wrapper").blur();

		}).on("blur blurin", function () {
			//  saveHTML(e);
		})


	

		e.on("mouseup mouseenter", ".custom-module", function () {
			var e = $(this);
			var top = e.offset().top;
			var left = e.offset().left + e.width() - 80;
			left = Math.min(left, $(window).width() - 130)
			$(".d-custom-active").removeClass("d-custom-active");
			e.addClass("d-custom-active");
			e.attr("contenteditable", "false");
			e.attr('data-noedit', "true");
			setTimeout(function () {
				jQuery("#divCustomModuleTool").data('active', e);
				$("#divCustomModuleTool").show().css({
					"top": top,
					"left": left,
				})
			}, 100)
		})
		e.on("mouseup", "i,a", function () {
			if ($(this).children("svg").length || ($(this).children("i").children("svg").length && $(this).children().length == 1)) {
				if ($(this).children("svg").length) {
					var $icon = $(this);
				} else {
					var $icon = $(this).children("i");
				}
				setTimeout(function () {
					jQuery("#divIconTool").data('active', $icon);
					if ($icon.parent().prop("tagName").toLowerCase() == 'a' && $icon.parent().children().length == 1) {
						jQuery('#divIconTool').find('.icon-link').css('display', 'block');
						jQuery('#divIconTool').find('.icon-add').css('display', 'block');
						jQuery('#divIconTool').find('.icon-remove').css('display', 'block')
					} else {
						jQuery('#divIconTool').find('.icon-link').css('display', 'none');
						jQuery('#divIconTool').find('.icon-add').css('display', 'none');
						jQuery('#divIconTool').find('.icon-remove').css('display', 'block')
					}
					var _toolheight = jQuery("#divIconTool").height();
					var _height = $icon.outerHeight();
					var _top = $icon.offset().top;
					if (_height > 80) {
						_top = _top + _height - 80
					}
					var _left = $icon.offset().left - 40;
					_top = _top - _toolheight + 1;
					jQuery("#divIconTool").css("top", _top + "px");
					jQuery("#divIconTool").css("left", _left + "px");
					jQuery("#divIconTool").css("display", "block");
					jQuery("#divLinkTool").css("display", "none");
				}, 32)
			}
		})
	})


	changeContentBuilderHTML();
	DnnVersions();

	function onChangeEvent() {
		var e = $(".is-wrapper");
		var id = e.data("mid");
		var ethis = e[0];

		e.find(".custom-module").each(function () {
			$(this).attr("contenteditable", "false");
			$(this).attr('data-noedit', "true");
			!$(this).attr('data-module-desc') ? $(this).attr('data-module-desc', $(this).data("effect")) : false;
			$(this).attr('data-module', "shortcode");
			if ($(this).text() == "") {
				$(this).html(decodeURIComponent($(this).data("html")));
			}
			if (!$(this).data("hasevent")) {
				$(this).addClass("loading")
			}
		})

		e.find(".custom-module.loading").each(function () {
			var m = $(this);
			var id = m.attr("id");
			$.ajax({
				url: SkinPath + "Resource/vendor/content-builder/code-template/" + m.data('effect') + ".html",
				async: false,
				success: function (data) {
					var htmlversions = randomRangeId(6);
					var settings = decodeURIComponent(m.attr('data-settings'));

					if (settings == "undefined" || settings == undefined || !settings || settings.length < 2 || settings.indexOf("{") == -1) {
						settings = {};
					} else {
						settings = JSON.parse(settings);
					}

					var HTMLsettings = encodeURIComponent(JSON.stringify(settings));

					settings["style"] = m.data('style');
					settings["moduleid"] = m.data('moduleid');

					var tmplHTML = $('<script type="text/x-jquery-tmpl">');
					data = data.split('<!--build HTML-->');

					//tmplHTML.html(data[1].replace(/\{id\}/g,htmlversions));
					tmplHTML[0].innerHTML = data[1].replace(/\{id\}/g, htmlversions)
					m.data("hasevent", true);
					var html = (tmplHTML.tmpl(settings))[0].innerHTML.replace(/\{LT\}/g, "<").replace(/\{GT\}/g, ">").replace(/\{SkinPath\}/g, SkinPath);
					m.attr("data-html", encodeURIComponent(html)).attr("data-settings", HTMLsettings).html(html).removeClass("loading");;

				}
			})
		})

		setTimeout(function () {

			e.find("link").each(function () {

				if (this.href.indexOf("googleapis") != -1) {
					var f = this.href.split("family=")[1].split("&")[0];
					f = f.replace(/\s/g, "%20");
					if (f && googleFont.indexOf(f) == -1) {
						googleFont.push(f);
						googleFontLink(f);
					};

					if (ethis.Builder["googleFont"]) {
						if (ethis.Builder["googleFont"].indexOf(f) == -1) {
							ethis.Builder["googleFont"].push(f);
						}
					} else {
						ethis.Builder["googleFont"] = [];
						ethis.Builder["googleFont"].push(f);
					}

				} else if ($(this).attr("data-name") == "contentstyle") {

					var name = $(this).attr("data-class");
					if (name) {
						if (!contentFont[name]) {
							contentFont[name] = $(this).attr("href").replace(SkinPath,"[SkinPath]");
							contentFontLink($(this).attr("href"));
						};

						ethis.Builder["contentFont"] ? (!ethis.Builder["contentFont"][name] ? ethis.Builder["contentFont"][name] = $(this).attr("href").replace(SkinPath,"[SkinPath]") : false) : ethis.Builder["contentFont"] = {}, ethis.Builder["contentFont"][name] = $(this).attr("href").replace(SkinPath,"[SkinPath]");
					}
				}
			})
			$('head link[data-name="contentstyle"]').each(function () {
				var name = $(this).attr("data-class");
				if (name) {
					if (!contentFont[name]) {
						contentFont[name] = $(this).attr("href").replace(SkinPath,"[SkinPath]");
						contentFontLink($(this).attr("href"));
					};
					ethis.Builder["contentFont"] ? (!ethis.Builder["contentFont"][name] ? ethis.Builder["contentFont"][name] = $(this).attr("href").replace(SkinPath,"[SkinPath]") : false) : ethis.Builder["contentFont"] = {}, ethis.Builder["contentFont"][name] = $(this).attr("href").replace(SkinPath,"[SkinPath]");
				}
			})
		}, 100)

		e.find(".build-css").each(function () {
			var box = $(this);
			var name = box.data("class");
			var css = this.innerHTML;
			if (css) {
				css = $('<textarea></textarea>').val(css).format({
					method: 'cssmin'
				}).val();
			}
			if (!ethis.Builder["CodeCSS"]) ethis.Builder["CodeCSS"] = {};
			if (box.data("editor") || AddReplace) {
				ethis.Builder["CodeCSS"][name] = css;
				if ($('head .build-css[data-class="' + name + '"]').length) {
					$('head .build-css[data-class="' + name + '"]').html(css);
					box.remove();
				} else {
					$("head").append(box);
				}

			} else {
				if (!ethis.Builder["CodeCSS"][name]) {
					ethis.Builder["CodeCSS"][name] = css;
					if ($('head .build-css[data-class="' + name + '"]').length) {
						box.remove();
					} else {
						$("head").append(box);
					}


				} else {
					box.remove();
				};

			}

		})


		e.find(".edit-box").attr("contenteditable", "true");
		pagemodification = true;
		if (pagemodification) {
			$("#ContentBuilderSaveHTML").removeAttr("disabled");
		}
		//	$(".hasBuilder").removeClass("hasBuilder");
		//	$(".layout-container .is-builder").parent().addClass("hasBuilder");

		if (e.find(".is-lightbox").length) {
			moduleEditModeJs("minilightbox", e.find(".custom-module"));
			moduleEditModeJs("minilightbox", e);
		}
		if (e.find(".img-Lazy,.iframe-Lazy").length) {
			moduleEditModeJs("lazyloading", e);
		}

		if (e.find(".icon-svg:not(.animated)").length) {
			moduleEditModeJs("vivus", e.find(".icon-svg:not(.animated)"), "svgicon");
			moduleEditModeJs("visible", e, "none");
		}
		if (e.find(".is-section.section-slider:not(.swiper-container-slide)").length) {
			moduleEditModeJs("swiper", e.find(".is-section.section-slider:not(.swiper-container-slide)"), "sectionSlider");
		}

		if (!e.find(".d-placeholder").length && EnableHBControls.indexOf("Section") != -1) {

			e.append('<div class="d-placeholder"><div><span>Click to add content</span></div></div>');
			e.find(".d-placeholder").addClass("row-add-initial").on("click", function () {
				$(this).parent().focus();
				$isSection = false;
				$('.is-sidebar-button[data-content="divSidebarSections"]').click();
			})
		}

		e.find(".img-Lazy,.iframe-Lazy").each(function () {
			$(this).attr("src", $(this).attr("data-src")).removeAttr("data-src").removeClass("Lazy-loading");
		})
		e.find(".img-Lazy-warp").removeClass("img-Lazy-warp");
		e.find(".load-over").removeClass("load-over");

		e.find(".is-section").each(function () {
			if ($(this).hasClass(".section-slider") && !$(this).find(".is-section-slider-options").length) {
				$(this).find(".is-section-tool").append(`<div class="is-section-slider-options" title="Slider Options"></div>`)
			}
			if (!$(this).find(".is-section-code").length) {
				$(this).find(".is-section-tool").prepend(`<div class="is-section-code" title="Section Code"><svg class="is-icon-flex" style="margin-right:-3px;width:14px;height:14px;"><use xlink:href="#ion-ios-arrow-left"></use></svg><svg class="is-icon-flex" style="margin-left:-2px;width:14px;height:14px;"><use xlink:href="#ion-ios-arrow-right"></use></svg></div>`)
			}
			if (!$(this).find(".is-section-save").length) {
				$(this).find(".is-section-tool").prepend(`<div class="is-section-save" title="Save Section"><i class="fa fa-save"></i></div>`)
			}
		})

		e.find(".is-overlay-bg").each(function () {
			if ($(this).data("bottom-top")) {
				$(this).after('<div class="is-overlay-bg" style=\'background-image:' + this.style.backgroundImage + '\'></div>');
				$(this).remove();
			}

		})
		e.find("i.sico").each(function () {
			if (!$(this).find("svg").length || $(this).hasClass("mod-icon") || !$(".section-svg-" + e.data("mid")).find($(this).find("svg use").attr("xlink:href")).length) {
				var svghtml = false;
				var name = $(this).attr("class").split("sico ")[1].split(" ")[0];
				if (!$(".section-svg-" + e.data("mid")).length) {
					var svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
					svg.display = "none";
					svg.setAttribute("class", "section-svg section-svg-" + e.data("mid"))
					svg.setAttribute("display", "none")
					e.before(svg);
				}
				if (!$(".section-svg-" + e.data("mid")).find("#" + name).length) {
					if (name.indexOf("lnr-") != -1) {
						if (svgLinearicons && svgLinearicons.find("#" + name).length) {
							svghtml = svgLinearicons.find("#" + name)[0].outerHTML;
						}
					} else if (name.indexOf("fas-") != -1 || name.indexOf("far-") != -1 || name.indexOf("fab-") != -1) {
						if (svgFontawesome && svgFontawesome.find("#" + name).length) {
							svghtml = svgFontawesome.find("#" + name)[0].outerHTML;
						}
					}
					if (svghtml) {
						var node = $.parseXML(svghtml);
						$(".section-svg-" + e.data("mid"))[0].appendChild(node.documentElement);
					}
				}

				$(this).html('<svg><use xlink:href="#' + name + '"></use></svg>').removeClass("mod-icon")

			}
		})





		/* if(e.contentbox().html().indexOf("{id2}") !=-1){
			e.contentbox().html()
		}
 */

	}



	function saveHTML(e, section) {
		ClearSaveJS = true;
		if ((e.data('contentbox') && e.data("change")) || section) {
			var eThis = e[0];
			var VendorUrl = eThis["Builder"]["VendorUrl"];
			var CodeCSS = eThis["Builder"]["CodeCSS"];
			var contentFontCSS = eThis["Builder"]["contentFont"];
			var GoogleFontCSS = eThis["Builder"]["googleFont"];

		//	e.find(".img-Lazy").each(function(){
		//		$(this).attr("iw",this.width)
		//		$(this).attr("ih",this.height)
		//	})
			if (section) {

				var sHTML = $(e.data('contentbox').html()).filter(".d-save-section").removeClass("d-save-section")[0].outerHTML;

				$(".d-save-section").removeClass("d-save-section");
			} else {
				e.data("change", false);
				e.find("link").each(function () {
					if (this.href.indexOf("googleapis") != -1 || $(this).data("name") == "contentstyle") {
						$(this).remove();
					}
				});
				eThis["Builder"]["settings"] = {};
				e.find(".custom-module").each(function () {
					if ($(this).data("module") == "shortcode") {
						eThis["Builder"]["settings"][$(this).data("moduleid")] = $(this).attr("data-settings");
					}
				})

				if (VendorUrl) {
					for (j in VendorUrl) {

						if (VendorUrlAlias[j]) {
							var has = false;
							VendorUrlAlias[j].forEach(function (k) {
								if (e.find(`${k}`).length) {
									has = true;
									return;
								}
							})
							if (!has) {
								delete eThis["Builder"]["VendorUrl"][j]
							} else {
								eThis["Builder"]["VendorUrl"][j] = skinVendorUrl[j];
							};
						} else {
							if (e.find(`.custom-module[data-effect="${j}"]`).length == 0) {
								delete eThis["Builder"]["VendorUrl"][j];
							} else {
								eThis["Builder"]["VendorUrl"][j] = skinVendorUrl[j];
							}
						}


					}
				}


				if (CodeCSS) {
					for (j in CodeCSS) {
						if (j && e.find("." + j).length == 0) {
							delete eThis["Builder"]["CodeCSS"][j];
						}
					}
				}


				if (contentFontCSS) {
					for (j in contentFontCSS) {
						if (e.find("." + j).length == 0 && j != "sMainCss") {
							delete eThis["Builder"]["contentFont"][j];
						}
					}
				}


				//var sSectionCss = '';
				//googleFont.filter(d => d);


				if (GoogleFontCSS) {
					//	$.unique(GoogleFontCSS);
					var HTML = e.data('contentbox').html();
					GoogleFontCSS.forEach(function (v, i) {
						if (v) {

							if (HTML.indexOf(v.split(":")[0].replace(/\%20/g, " ")) == -1) {
								if (GoogleFontCSS.length <= 1) {
									GoogleFontCSS = [];
								} else {
									GoogleFontCSS.splice(i, 1);
								}
							}
						} else {
							GoogleFontCSS.splice(i, 1);
						}
					})


					if (GoogleFontCSS.join("|")) {
						GoogleFontCSS = '<link href="https://fonts.googleapis.com/css?family=' + GoogleFontCSS.join("|") + '&display=swap" type="text/css" rel="stylesheet" class="contentbox-google" />';
					} else {
						delete eThis["Builder"]["googleFont"];
					}
				} else {
					GoogleFontCSS = "";
				}


				var sMainCss = '';

				$('head link').each(function () {
					var src = $(this).attr("href").toLowerCase();
					if (src.indexOf('basetype-') != -1) {
						sMainCss = $(this).attr("href").split("?")[0];
					}

				})

				if (sMainCss) {
					if (!eThis["Builder"]["contentFont"]) {
						eThis["Builder"]["contentFont"] = {};
					}
					eThis["Builder"]["contentFont"]["sMainCss"] = sMainCss.split("?")[0].replace(SkinPath,"[SkinPath]");

				} else {
					if (eThis["Builder"]["contentFont"] && eThis["Builder"]["contentFont"]["sMainCss"]) {
						delete eThis["Builder"]["contentFont"]["sMainCss"];
					}
				}

				if (eThis["Builder"]["contentFont"]) {
					var ThisContentFont = eThis["Builder"]["contentFont"];
					for (j in ThisContentFont) {
						ThisContentFont[j] = ThisContentFont[j].split("?")[0].replace(SkinPath,"[SkinPath]");
					}
				}

				var sHTML = e.data('contentbox').html();
			}
		
			var filter = $("<div>");
			filter.append(sHTML);
			
			filter.find("button.row-add,div.row-handle,button.row-up,button.row-down,button.row-duplicate,button.row-remove,.d-placeholder,.edit-mode-j,.builder-placeholder").remove();
			filter.find("style.build-css").remove();
			filter.find('.custom-module').each(function () {
				if ($(this).data("module") == "shortcode") {
					if (!$(this).attr('data-html')) {
					} else {
						$(this).removeAttr('data-html');
						$(this).removeAttr('data-settings');
						$(this).removeAttr('data-noedit');
						$(this).data('effect') == $(this).data('module-desc') ? $(this).removeAttr('data-module-desc') : false;
						$(this).removeAttr('data-module');
					}
				}
			})

			filter.find('.load-over').removeClass("load-over");
			filter.find('.icon-svg').find("i").remove();
			filter.find('.icon-svg').removeClass("animated").find("svg *").removeAttr("style");
			filter.find('i.sico').find("i").remove();
			filter.find('.hasBuilder').removeClass("hasBuilder");
			filter.find('.is-box.current-cover-bg').removeClass("current-cover-bg");
			filter.find('.img-Lazy-warp >.Lazy-loading,.img-Lazy-warp > .Lazy-loading-transparent').remove();
			filter.find('.edit-mode-js').remove();

			filter.find('.is-container').removeAttr("dragwithouthandle");
			filter.find('.is-section.section-slider').each(function () {
				EmptySectionSlider($(this));
			});
		
			if (section) {
				//	filter.
				if (CodeCSS) {
					for (j in CodeCSS) {
						if (j && filter.find("." + j).length) {
							filter.find(".is-section").append(`<style class="build-css" data-class="${j}">${CodeCSS[j]}</style>`)
						}
					}
				}
				if (contentFontCSS) {
					for (j in contentFontCSS) {
						if (filter.find("." + j).length) {
							filter.find(".is-section").append(`<link data-name="contentstyle" data-class="${j}" href="${contentFontCSS[j]}" type="text/css"/>`)
						}
					}
				}
				if (GoogleFontCSS) {
					var sectionHTML = filter.html();
					GoogleFontCSS.forEach(function (v, i) {
						if (v && sectionHTML.indexOf(v.split(":")[0].replace(/\%20/g, " ")) != -1) {
							filter.find(".is-section").append(`<link href="https://fonts.googleapis.com/css?family=${v}" type="text/css"/>`);
						}
					})
				}

				filter.find(".custom-module").each(function (index) {
					var settings = e.find("#" + $(this).attr("id")).attr("data-settings");
					$(this).attr("data-settings", settings);
					$(this).html("");
					$(this).addClass("loading");

					if (index >= 1) {
						$(this).attr("id", "module-{id" + (index + 1) + "}");
						$(this).attr("data-moduleid", "{id" + (index + 1) + "}");
					} else {
						$(this).attr("id", "module-{id}");
						$(this).attr("data-moduleid", "{id}");
					}
				})
			}
			
			filter.find('.iframe-Lazy').each(function () {
				$(this).attr('data-src', $(this).attr("src")).addClass("Lazy-loading");
				$(this).attr("src", "about:blank")
			})
			filter.find('.img-Lazy').each(function(){ $(this).width(); });
			setTimeout(function(){

				filter.find('.img-Lazy').each(function () {
					if ($(this).attr("src").indexOf("data:image/svg") == -1) {
						if (this.width){// || $(this).attr("iw")) {	
							var w = this.width;// || $(this).attr("iw");
							var h = this.height;// || $(this).attr("ih");
								$(this).attr('data-src', $(this).attr("src")).attr("src", `data:image/svg+xml,%3Csvg viewBox='0 0 ${w} ${h}' width='${w}' height='${h}' xmlns='http://www.w3.org/2000/svg'/%3E`).removeClass("load-over").parent().addClass("img-Lazy-warp");
						}
					}
				//	$(this).removeAttr("iw");
				//	$(this).removeAttr("ih");
				})

				sHTML = filter.html();

			//	e.find(".img-Lazy").each(function(){
			//		$(this).removeAttr("iw");
			//		$(this).removeAttr("ih");
			//	})

				if (section) {
					$("#add-section-name-text").val(sHTML);
					ClearSaveJS = false;
				} else {
					if ($(".section-svg-" + e.data("mid")).length) {
						var newssvg = "";
						$(".section-svg-" + e.data("mid") + " symbol").each(function () {
							if (e.find("." + $(this).attr("id")).length) {
								newssvg += this.outerHTML;
							}
						})
						if (newssvg) {
							sHTML = '<svg xmlns="http://www.w3.org/2000/svg" display="none" class="section-svg section-svg-' + e.data("mid") + '">' + newssvg + '</svg>' + sHTML
						}
					}
					$.post(SaveContentServiceUrl + e.data("mid"), {
							Content: sHTML,
							MainCss: GoogleFontCSS,
							SectionCss: "",
							ContentJSON: eThis["Builder"] ? JSON.stringify(eThis["Builder"]) : '{}'
						},
						function (data) {
							lastIndex++;
							if (lastIndex == lastlength) {
								setTimeout(function () {
									$('#ContentBuilderSaveHTML').removeClass('loading').addClass('success').delay(1000).queue(function () {
										$(this).removeClass('success').dequeue();
										ClearSaveJS = false;
										pagemodification = false;
									});
								}, 1000)
								if (preview) {
									window.location.href = preview.data("original");
								}

							}
						}
					);

				}

			},500)

		}

	}


	if (preview) {
		$("#ContentBuilderSaveHTML").removeAttr("disabled");
	}


	$("#ContentBuilderSaveHTML").removeClass("hide").on("click", function () {
		//	googleFont = HasGoogleFont(googleFont);
		currentVendorUrl = HasVendorUrl(currentVendorUrl);

		$(this).addClass("loading").attr("disabled", "disabled");
		lastIndex = lastlength = 0;
		$(".d-wrapper").each(function () {
			if ($(this).data('contentbox') && $(this).data("change")) {
				lastlength++;
			}
		})
		$(".d-wrapper").each(function (index) {
			saveHTML($(this));
		});

	});

	function saveImages(id) {

		$("#dng-wrapper").saveimages({
			handler: CatImageServiceUrl + id,
			onComplete: function () {

				$(".d-wrapper").each(function (index) {
					//   saveHTML($(this));
				});
			}
		});
		$("#dng-wrapper").data('saveimages').save();
	}

	jQuery(".is-wrapper").data("contentbox").settings.onRender();
	//	jQuery(".is-wrapper").data("contentbox").settings.onChange();



	var snippetlistsrc = setInterval(function () {
		if ($("#divFb .is-modal.snippets iframe").length) {
			$("#divFb .is-modal.snippets iframe").attr("src", SkinPath + "Resource/vendor/content-builder/assets/minimalist-blocks/snippetlist.html");
			clearTimeout(snippetlistsrc);
		}
	}, 500)


	function changeContentBuilderHTML() {
		$(".d-wrapper").removeClass("is-wrapper");
		$(".d-wrapper").eq(0).addClass("is-wrapper").click();


		$(".d-placeholder").addClass("row-add-initial").on("click", function () {
			$(this).parent().focus();
			$isSection = false;
			$('.is-sidebar-button[data-content="divSidebarSections"]').click();
		})

		$(".applytypography .input-ok.classic").removeClass("input-ok").addClass("dnn-input-ok");

		$(".is-section .is-section-tool").prepend(`<div class="is-section-code" title="Section Code"><svg class="is-icon-flex" style="margin-right:-3px;width:14px;height:14px;"><use xlink:href="#ion-ios-arrow-left"></use></svg><svg class="is-icon-flex" style="margin-left:-2px;width:14px;height:14px;"><use xlink:href="#ion-ios-arrow-right"></use></svg></div>`)

		$(".is-section .is-section-tool").prepend(`<div class="is-section-save" title="Save Section"><i class="fa fa-save"></i></div>`)

		$(".applytypography .dnn-input-ok").on("click", function () {
			var val = $('input[name=rdoApplyTypoStyle]:checked').val();
			var link = '';

			if (val == 1) {
				for (var key in contentFont) {
					if (jQuery(".d-wrapper").find('.' + key).length > 0) {
						jQuery(".d-wrapper").find('.' + key).removeClass(key);
					}
				}
				$('link[data-name = "contentstyle"]').remove();
				$(".contentfont-style").remove();
				contentFont = {};
			}
			var links = document.getElementsByTagName("link");
			for (var i = 0; i < links.length; i++) {
				var src = links[i].href.toLowerCase();
				if (src.indexOf('styles/basetype-') != -1) {
					jQuery(links[i]).attr('data-rel', '_del')
				}
			}
			jQuery('[data-rel="_del"]').remove();

			if (DNNapplyTypography.css != '') {
				link = '<link href="' + jQuery(".is-wrapper").data('contentbox').settings.contentStylePath + DNNapplyTypography.css + '" rel="stylesheet">';
				jQuery('head').append(link);
			}

			jQuery(".is-wrapper").data('contentbox').hideModal(jQuery('.is-modal.applytypography'));
			jQuery(".d-wrapper").each(function () {
				if ($(this).data('contentbox')) {
					$(this).data('contentbox').settings.onChange();
				}
			})
			$("head title").after($('link[href*="styles/basetype-"]'));
			return false;
		})

		$isbox = $(".is-box").eq(0);
		// $isSection = $(".is-section").eq(0);

		jQuery(".d-wrapper").on('mouseenter mouseleave', ".is-box", function (e) {
			$isbox = jQuery(this);

		});
		jQuery(".d-wrapper").on('mouseenter mouseleave', ".is-section", function (e) {
			$isSection = jQuery(this);
		});

		var fileEmbedImage = $('<button type="button" id="fileEmbedImageButton" style="position:absolute;top:0px;left:0;width:100%;height:50px;opacity: 0;cursor: pointer;"> </button>')
		$("#divImageTool #fileEmbedImage").hide().after(fileEmbedImage);
		fileEmbedImage.on("click", function () {
			$("#imgaesTEXTURL").val($(".elm-active").attr("src")).siblings(".img").css("background-image", "url('" + $(".elm-active").attr("src") + "')");
			$(".imgaes-setting-option .loadimg").show();
			if ($(".elm-active").hasClass("img-Lazy")) {
				$("#imgaesLoad").prop("checked", true);
			} else {
				$("#imgaesLoad").prop("checked", false);
			}
			$("#DnnMediaBox").attr("type", "image").fadeIn().find("iframe").attr("src", getMediaUrl({
				mimid: $(".is-wrapper").data("mid"),
				FileType:"image"
			}));

		})

		
		var fileCover = $('<button type="button" id="fileCoverButton" style="position:absolute;top:0px;left:0;width:100%;height:50px;opacity: 0;cursor: pointer;"> </button>')
		$("#divboxtool #fileCover").hide().after(fileCover);
		fileCover.on("click", function () {
			$(".current-cover-bg").removeClass("current-cover-bg");
			$isbox.addClass("current-cover-bg");

			if ($isbox.find(".is-overlay-bg").length && $isbox.find(".is-overlay-bg")[0].style.backgroundImage) {
				var bg = $isbox.find(".is-overlay-bg")[0].style.backgroundImage;
				bg = bg.substring(5, bg.length - 2);
				$("#imgaesTEXTURL").val(bg).siblings(".img").css("background-image", "url('" + bg + "')");
			} else {
				$("#imgaesTEXTURL").val("").siblings(".img").css("background-image", "");
			}

			$(".imgaes-setting-option .loadimg").hide();
			$("#DnnMediaBox").attr("type", "cover").fadeIn().find("iframe").attr("src", getMediaUrl({
				mimid: $(".is-wrapper").data("mid"),
				FileType:"image"
			}));
		})


		$('#divContentSize .cmd-box-size[data-value="380"]').before('<button class="cmd-box-full" data-value="full">Full</button><button class="cmd-box-auto" data-value="auto">Auto</button>')
		$('#divContentSize .cmd-box-size[data-value="800"]').before('<button class="cmd-box-size" data-value="640">640px</button>').after('<button class="cmd-box-size" data-value="970">970px</button><button class="cmd-box-size" data-value="980">980px</button><button class="cmd-box-size" data-value="1050">1050px</button><button class="cmd-box-size" data-value="1100">1100px</button><button class="cmd-box-size" data-value="1200">1200px</button>')

		$('#divContentSize .cmd-box-auto').on("click", function () {
			$isbox.find(".is-container").removeClass("is-content-380 is-content-500 is-content-640 is-content-800 is-content-970 is-content-980 is-content-1050 is-content-1100 is-content-1200 is-container-fluid").css("max-width", "");
		})
		$('#divContentSize .cmd-box-full').on("click", function () {
			$isbox.find(".is-container").addClass("is-container-fluid").removeClass("is-content-380 is-content-500 is-content-640 is-content-800 is-content-970 is-content-980 is-content-1050 is-content-1100 is-content-1200").css("max-width", "");
		})
		$('#divContentSize .cmd-box-size').on("mouseup", function () {
			$isbox.find(".is-container").removeClass("is-container-fluid")
		})

		$('.cmd-addclass-box .cmd-addclass').on("click", function () {
			$isbox.find(".is-container").addClass($(this).data("value"))
			$(this).siblings().each(function () {
				$isbox.find(".is-container").removeClass($(this).data("value"))
			})
		})

 		$("#divBoxImage .is-settings").last().after(`
			<div class="is-settings clearfix" style="margin-bottom:0"><div>Image Size:</div>
				<button title="auto" class="cmd-box-image-size" style="width:70px" data-value="auto">Auto</button>
				<button title="auto" class="cmd-box-image-size" style="width:70px" data-value="contain">Contain</button>
				<button title="auto" class="cmd-box-image-size" style="width:70px" data-value="cover">Cover</button>
				<button title="auto" class="cmd-box-image-size" style="width:70px" data-value="custom">Custom</button>
				<input type="text" class="cmd-box-image-size-custom" value="0px 0px" style="width: 138px; height: 45px; display:none" >
				</div>	

				<div class="is-settings clearfix" style="margin-bottom:0"><div>Remove Image:</div>
						<button title="auto" class="del-image" style="width:120px" data-value="auto">Remove Image</button>
				</div>	
			
		`)

		$('#divBoxImage .cmd-box-image-size').on("click", function () {

			if ($(this).data("value") != "custom") {
				if ($isSection) {
					$isbox.find(".is-overlay-bg").css("background-size", $(this).data("value"))
				}
				$("#divBoxImage .cmd-box-image-size-custom").hide();
			} else {
				$("#divBoxImage .cmd-box-image-size-custom").show();
			}
		})
		$('#divBoxImage .cmd-box-image-size-custom').on("change", function () {
			$isbox.find(".is-overlay-bg").css("background-size", $(this).val())
		})
		$('#divBoxImage .del-image').on("click", function () {
			$isbox.find(".is-overlay-bg").remove();
			$(".is-modal.editbox.active").removeClass("active");

		})

		$("#DnnMediaBox .close,#DnnMediaBox #MediaClose").on("click", function () {
			$("#DnnMediaBox").removeAttr("type").fadeOut();
			$("#MediaAddImages").off();
		})
		$(".is-side-close").on("click", function () {
			modulesIcon = false;
			$(".is-modal.is-side.viewicons.active").removeClass("active")
			$(".is-modal.custommodule .is-modal-overlay").css("z-index", "-1");
		})

		$('#selSnippetCat option[value="120"]').after('<option value="800">Short Code</option>')

		$('#selSnippetCat').html("");


		for (var key in selSnippetCat) {
			$('#selSnippetCat').append(`<option value="${selSnippetCat[key]}">${key}</option>`);
		}



		/* */
		$(".is-modal.editsection .is-modal-bar").siblings("div").wrap(`<div id="divBoxBasic" class="is-tab-content" style="display:block">`)

		$("#divBoxBasic >div").append(`
			<div id="divBoxLayoutSettings">
			<div>Content position</div>
			<select class="form-control" id="Layout_placement" data-type="align-items-">
				<option value="">Default</option>
				<option value="align-items-start">Top</option>
				<option value="align-items-center">Middle</option>
				<option value="align-items-end">Bottom</option>
			</select>
			<div>Justify content</div>
			<select class="form-control" id="Layout_alignment" data-type="justify-content-">
				<option value="">Default</option>
				<option value="justify-content-start">justify-content-start</option>
				<option value="justify-content-center">justify-content-center</option>
				<option value="justify-content-end">justify-content-end</option>
				<option value="justify-content-around">justify-content-around</option>
				<option value="justify-content-between">justify-content-between</option>
			</select>
			<div>Spacing</div>
			<div>
				<label for="layout_tvspace_no" style="margin:0;"><input id="layout_tvspace_no" type="checkbox" data-class="layout-no-mt"> No top spacing</label>
			</div>
			<div>
				<label for="layout_bvspace_no" style="margin:0;"><input id="layout_bvspace_no" type="checkbox" data-class="layout-no-mb"> No bottom spacing</label>
			</div>
			<div>
				<label for="layout_hspace_no" style="margin:0;"><input id="layout_hspace_no" type="checkbox" data-class="layout-no-plr"> No left and right spacing</label>
			</div>
		</div>
		`);

		$("#Layout_placement,#Layout_alignment").on("change", function () {

			var className = $(this).val();
			var type = $(this).data("type");

			var row = $isSection.find(".layout-container > .row,.is-container > .row");

			row.each(function () {
				if ($(this).attr("class").indexOf(type) != -1) {

					$(this).attr("class", jQuery.trim($(this).attr("class").replace(new RegExp(type + ".+?(\\s|$)"), className ? className + " " : "")))
				} else {
					$(this).addClass(className)
				}
			})

		})
		$("#layout_tvspace_no,#layout_bvspace_no,#layout_hspace_no").on("change", function () {
			var has = $(this).is(":checked");

			if (has) {
				$isSection.addClass($(this).data("class"));
			} else {
				$isSection.removeClass($(this).data("class"));
			}
		})


		if ($(".dng-main").data("page-type") == "onepage" || $(".dng-main").data("page-type") == "verticalfullpage") {
			$("#divBoxBasic >div").append(`
			<div>Anchor ID</div>
			<input type="text" id="layout_anchorid" />
			<div class="pt-5">Anchor Title </div>
			<input type="text" id="layout_anchortitle" />
			`);

			$("#layout_anchorid").on("change", function () {
				if ($(this).val()) {
					$isSection.attr("id", $(this).val());
					$isSection.addClass("anchorTag");
				} else {
					$isSection.removeAttr("id");
					$isSection.removeClass("anchorTag");
				}
			})
			$("#layout_anchortitle").on("change", function () {
				$isSection.attr("data-title", $(this).val())
			})
		}


		$(".is-modal.editsection .is-tabs a").on("click", function (e) {
			e.preventDefault();
			$(this).addClass("active").siblings().removeClass("active");
			$("#" + $(this).data("content")).show().siblings(".is-tab-content").hide();
		})
		$("#elementsettingJSON").on("change", function () {
			$isSection.attr("data-element", $(this).val());

		})
		$("#Body").on("click", ".is-section-edit", function () {
			$(".is-modal.editsection").addClass("loader");
			//	if ($isSection.find(".layout-container > .row").length) {

			var className = $isSection.find(".layout-container > .row,.is-container > .row").attr("class");

			if (className.indexOf("align-items-") != -1) {
				$("#Layout_placement option").each(function () {
					if ($(this).attr("value") && className.indexOf($(this).attr("value")) != -1) {
						$(this).attr("selected", "selected").siblings().removeAttr("selected").parent().val($(this).attr("value"));
					}
				})
			} else {
				$("#Layout_placement option").eq(0).attr("selected", "selected").siblings().removeAttr("selected").parent().val("");
			}
			if (className.indexOf("justify-content-") != -1) {
				$("#Layout_alignment option").each(function () {
					if ($(this).attr("value") && className.indexOf($(this).attr("value")) != -1) {
						$(this).attr("selected", "selected").siblings().removeAttr("selected").parent().val($(this).attr("value"));
					}
				})
			} else {
				$("#Layout_alignment option").eq(0).attr("selected", "selected").siblings().removeAttr("selected").parent().val("");
			}



			//		$("#divBoxLayoutSettings").show();
			//	} else {
			//		$("#divBoxLayoutSettings").hide();
			//	}


			$("#layout_tvspace_no,#layout_bvspace_no,#layout_hspace_no").each(function () {
				if ($isSection && $isSection.hasClass($(this).data("class"))) {
					$(this).prop("checked", true);
				} else {
					$(this).prop("checked", false);
				}
			})

			if ($(".dng-main").data("page-type") == "onepage" || $(".dng-main").data("page-type") == "verticalfullpage") {
				if ($isSection && $isSection.attr("id")) {
					$("#layout_anchorid").val($isSection.attr("id"));
				} else {
					$("#layout_anchorid").val("");
				};
				if ($isSection && $isSection.attr("data-title")) {
					$("#layout_anchortitle").val($isSection.attr("data-title"));
				} else {
					$("#layout_anchortitle").val("");
				}
			}

			setTimeout(function () {
				$("#elementsettingJSON").val($isSection.attr("data-element")).change();
				$(".is-modal.editsection").removeClass("loader");
			}, 100)
		})

		$("#divElementBox").append(`
			<div style="margin-top: 25px;font-weight: bold;line-height: 1.7;">Class</div>
			<textarea rows="2" style="width:100%" id="elmClassName"></textarea>
		`)

		function getClassName() {
			var className = $(".elm-inspected").attr("class");
			if (className) {
				$("#elmClassName").val($(".elm-inspected").attr("class").replace("elm-inspected", "").replace("elm-active", "").replace(/(^\s*)|(\s*$)/g, ""))
			} else {
				$("#elmClassName").val("")
			}
		}

		$("#elmClassName").on("change", function () {
			var curr = "elm-inspected " + $(this).val();

			if ($(".elm-inspected").hasClass("elm-active")) {
				"elm-active " + curr;
			}
			$(".elm-inspected").attr("class", curr)
		})

		$("#divElementBox > .is-settings").eq(0).after(`
		<div class="is-settings clearfix ImagesSetting" style="display:inline-block;width:100%;margin-bottom:0;">
			<div>Background Image:</div>
			<div style="position: relative;">
				<input type="text" id="inpElmBgImages" value="">
				<button title="Select Image" ID="inpElmBgImagesButton"><svg class="svg-icon"><use xlink:href="#ion-image"></use></svg></button>
			</div>
			<div class="ImagesAttr" style="display:inline-block;width:100%;margin-top:10px;">
			<div class="edit-select">
				<input type="text" class="select-text form-control" size="6" id="inpElmBgImagesX" value=""/>
				<ul class="insel"><li value="left">left</li><li value="center">center</li><li value="right">right</li></ul>
			</div>
			<div class="edit-select">
				<input type="text" class="select-text form-control" size="6" id="inpElmBgImagesY" value=""/>
				<ul class="insel"><li value="top">top</li><li value="center">center</li><li value="bottom">bottom</li></ul>
			</div>

			<select class="form-control" id="inpElmBgImagesRepeat" >
			<option value="no-repeat">no-repeat</option><option value="repeat-x">repeat-x</option><option value="repeat-y">repeat-y</option><option value="repeat">repeat</option>
			</select>
			<div class="edit-select">
				<input type="text" class="select-text form-control" size="17" id="inpElmBgImagesSize" value=""/>
				<ul class="insel"><li value="auto">auto</li><li value="contain">contain</li><li value="cover">cover</li></ul>
			</div>			
			</div>
		</div>
		`)

		$("#inpElmBgImagesButton").on("click", function () {
			$(".imgaes-setting-option .loadimg").hide();
			$("#DnnMediaBox").attr("type", "setting").fadeIn().find("iframe").attr("src", getMediaUrl({
				mimid: $(".is-wrapper").data("mid"),
				FileType:"image"
			}));
		})
		$(".is-settings.ImagesSetting").on("change", "select,input", function () {
			var url = $("#inpElmBgImages").val(),
				x = $("#inpElmBgImagesX").val(),
				y = $("#inpElmBgImagesY").val(),
				Repeat = $("#inpElmBgImagesRepeat").val(),
				Size = $("#inpElmBgImagesSize").val();
			if (url) {
				$(".elm-inspected").css({
					"background-image": "url('" + url + "')",
					"background-position": x + " " + y,
					"background-repeat": Repeat,
					"background-size": Size
				})
				$(".ImagesSetting .ImagesAttr").show();
			} else {
				$(".elm-inspected").css({
					"background-image": "",
					"background-position": "",
					"background-repeat": "",
					"background-size": ""
				})
				$(".ImagesSetting .ImagesAttr").hide();
			}
		})

		function getBgImage() {
			var bg = $(".elm-inspected")[0] ? $(".elm-inspected")[0].style.backgroundImage : false;

			if (bg && bg != "none") {

				var p = $(".elm-inspected")[0].style.backgroundPosition;
				p = p ? p.split(" ") : false;
				var x = p ? p[0] : "",
					y = p ? p[1] : "",
					Repeat = $(".elm-inspected").css("background-repeat"),
					Size = $(".elm-inspected").css("background-size");


				$("#inpElmBgImages").val(bg.substring(5, bg.length - 2));
				$("#inpElmBgImagesX").val(x);
				$("#inpElmBgImagesY").val(y);
				$("#inpElmBgImagesRepeat").find('option[value="' + Repeat + '"]').attr("selected", "selected").siblings().removeAttr("selected");
				$("#inpElmBgImagesSize").val(Size);
				$(".ImagesSetting .ImagesAttr").show();
			} else {
				$("#inpElmBgImages").val("");
				$(".ImagesSetting .ImagesAttr").hide();
			}
		}


		$(".element-edit").on("click mouseup", function () {
			setTimeout(function () {
				getBgImage();
				getClassName();
			}, 100)
		})
		$(".is-modal.is-side").on("mouseup", ".elm-list a", function () {
			setTimeout(function () {
				getClassName();
				getBgImage();
			}, 100)
		})

		$(".d-wrapper").on("mouseup", function () {
			if ($isSection && $isSection.find(".elm-inspected").length) {
				setTimeout(function () {
					getClassName();
					getBgImage();
				}, 100)
			}
		})


		$(".is-sidebar").after(`<div class="is-sidebar2"></div>`)

		$(".is-sidebar").append(`<div class="is-sidebar-button" id="divSidebarSourceEditCSS" data-content="divSidebarSourceCSS" data-command="css" title="CSS"><svg class="svg-icon"><use xlink:href="#ion-code-working"></use></svg></div>`)


		$("#divSidebarSource").after(`
		<div id="divSidebarSourceCSS" class="is-sidebar-content" style="-webkit-transition-duration:0.2s;transition-duration:0.2s;">
		    <div>
				<div style="position:absolute;width:260px;height:35px;right:18px;top:7px;z-index:1;">
				<button title="Cancel" class="secondary"> Cancel </button>
				<button title="Ok" class="primary"> Ok </button>
				</div>
		        <div style="height:100%;box-sizing:border-box;border-top:#f3f3f3 50px solid;position:relative">
		            <div id="divSidebarSourceEditCSSBox"></div>
		        </div>
			</div>
			<iframe id="ifrCSSFormatted" src="about:blank"></iframe>
		</div>
		`)
		$("#divSidebarSourceEditCSS").on("click", function () {
			if (!$("#divSidebarSourceCSS").hasClass("active")) {
				$("#ifrCSSFormatted").attr('src', 'about:blank')

				$("#divSidebarSourceCSS").addClass("active").siblings().removeClass("active");
				$("#divSidebarSourceEditCSSBox").empty();

				$("#ifrCSSFormatted").attr('src', SkinPath + 'Resource/vendor/content-builder/contentbuilder/css.html?1');

				if ($(".is-wrapper")[0].Builder["CodeCSS"]) {
					var css = $(".is-wrapper")[0].Builder["CodeCSS"];
					for (var key in css) {
						if (css[key] && css[key].indexOf("{") !== -1) {

							format = $('<textarea></textarea>').val(css[key]).format({
								method: 'css'
							}).val();
							$("#divSidebarSourceEditCSSBox").append(`
							<textarea class="EditCSSBox" contenteditable="true" data-class="${key}">${format}</textarea>
						`)
						} else {
							$("#divSidebarSourceEditCSSBox").append(`<textarea class="EditCSSBox" contenteditable="true" data-class="${key}">${css[key]}</textarea>`)
						}
					}
				}

			} else {
				$("#divSidebarSourceCSS").removeClass("active")
			}
		})

		if(addHistory){
			$(".is-sidebar").append(`<div class="is-sidebar-button disabled prev" id="historyPrevButton" title="Undo"><svg class="is-icon-flex"  viewBox="0 0 1024 1024" style="width:18px;height:18px;"><path d="M76 463.7l294.8 294.9c19.5 19.4 52.8 5.6 52.8-21.9V561.5c202.5-8.2 344.1 59.5 501.6 338.3 8.5 15 31.5 7.9 30.6-9.3-30.5-554.7-453-571.4-532.3-569.6v-174c0-27.5-33.2-41.3-52.7-21.8L75.9 420c-12 12.1-12 31.6 0.1 43.7z" fill="currentColor"></path></svg></div><div class="is-sidebar-button next disabled" id="historyNextButton" title="Redo"><svg class="is-icon-flex" viewBox="0 0 1024 1024" style="width:18px;height:18px;"><path d="M946.8 420L651.9 125.1c-19.5-19.5-52.7-5.7-52.7 21.8v174c-79.3-1.8-501.8 14.9-532.3 569.6-0.9 17.2 22.1 24.3 30.6 9.3C255 621 396.6 553.3 599.1 561.5v175.2c0 27.5 33.3 41.3 52.8 21.9l294.8-294.9c12.1-12.1 12.1-31.6 0.1-43.7z" fill="currentColor"></svg></div>`)

			$("#historyPrevButton,#historyNextButton").on("click", function () {
				if(addHistory){
					var prev =$(this).hasClass("prev");
						addHistory =false;
						prev ? cuurHistory-- :cuurHistory++;
					var mid= midHistoryIndexed[cuurHistory-1];

					var position = false;

					if(!prev){
						position =  midHistoryIndexed[cuurHistory-1]?midHistoryIndexed[cuurHistory-1]["position"]:false;
					}else{
						position = midHistoryIndexed[cuurHistory]?midHistoryIndexed[cuurHistory]["position"]:false;
					}

					if(mid && mid["initial"] && !prev){
						cuurHistory++;
					}			
					var html = sessionStorage.getItem('cb-history-'+cuurHistory);
						if(cuurHistory == 1){
							$("#historyPrevButton").addClass("disabled");	
						}else{
							$("#historyPrevButton").removeClass("disabled");	
						}
						if(cuurHistory == History_cb){
							$("#historyNextButton").addClass("disabled");
						}else{
							$("#historyNextButton").removeClass("disabled");
						}
					
						if(mid && mid["initial"] && prev){
							 cuurHistory--;
						}
					
						if(html !== null && html != "unll"){
							var box =jQuery('.d-wrapper[data-mid="'+mid["mid"]+'"]');
							box.html(html).click();
							_cb.applyBehavior();
							box.data("contentbox").setup();
							box.data("contentbox").settings.onRender();
							box.data("contentbox").settings.onChange();
						}
						if(position){
							jQuery('body,html').stop().animate({
								scrollTop: position
							}, 300);
						}

						if(cuurHistory != History_cb){
							SelectHistory = cuurHistory;	
						}else{
							SelectHistory =false;
						}
					
						box.find(".d-placeholder").addClass("row-add-initial").on("click", function () {
							$(this).parent().focus();
							$isSection = false;
							$('.is-sidebar-button[data-content="divSidebarSections"]').click();
						})
					
					addHistory =true;
				} 
			})
			
		} 

		$(".is-sidebar").append($(".SkinPluginPro_icon").addClass("sidebarIcon").show())
		$(".SkinPluginPro_icon a").each(function () {
			$(this).attr("title", $(this).find("span").text())
		})

		$(".is-sidebar").append(`<div class="is-sidebar-button" id="dnnpersonabarhide" title="Hide Persona Bar"><svg class="is-icon-flex" style="width:24px;height:24px;"><use xlink:href="#ion-ios-arrow-left"></use></svg><svg class="is-icon-flex" style="width:24px;height:24px;margin-left: -17px;"><use xlink:href="#ion-ios-arrow-left"></use></svg></div>`)

		$("#dnnpersonabarhide").on("click",function(){
			$(this).toggleClass("active");
			if($(this).hasClass("active")){
				$(this).attr("title","Display Persona Bar");
			}else{
				$(this).attr("title","Hide Persona Bar");
			}

			$("html").toggleClass("dnnpersonabarhide");
		})


		$("#divSidebarSource").append(`
			<iframe id="ifrHTMLFormattedMain" src="about:blank"></iframe>
		`)

		$('.is-sidebar-button[title="HTML"]').on("click", function () {
			$("#ifrHTMLFormattedMain").attr('src', SkinPath + 'Resource/vendor/content-builder/contentbuilder/html_small3.html?1');
		})



		$("#divSidebarSourceCSS .primary").on("click", function () {
			if ($("#divSidebarSourceCSS .EditCSSBox.change").length) {
				$("#divSidebarSourceCSS .EditCSSBox.change").each(function () {
					if ($(".is-wrapper")[0].Builder["CodeCSS"]) {
						var v = $(this).data("class"),
							h = $(this).val();
						//	h = new formathtmljscss(h, 2, 'compress');
						//	h.formatcss();
						//	h = h.source;
						h = $('<textarea></textarea>').val(h).format({
							method: 'cssmin'
						}).val();

						if(v){
							$(".is-wrapper")[0].Builder["CodeCSS"][v] = h;
							$('.build-css[data-class="' + v + '"]').remove();
							$('head').append(`<style class="build-css" data-class="${v}">${h}</style>`);
						}
					}
				})
				jQuery(".is-wrapper").data("contentbox").settings.onChange();
			}
			$("#divSidebarSourceCSS").removeClass("active");


		})
		$("#divSidebarSourceCSS .secondary,.dng-main").on("click", function () {
			$("#divSidebarSourceCSS").removeClass("active")
		})


		$("#DnnMediaBox .media-footer").append(`
		<ul class="imgaes-setting-option">
		<li class="imgurl"><label>URL:</label> <input type="text" id="imgaesTEXTURL" /><i class="img"></i> </li>
		<li class="loadimg"><label>Lazy Load:</label> <input type="checkbox" id="imgaesLoad" /> </li>
		</ul>	
		`)
		$("#imgaesTEXTURL").on("change", function () {
			$(this).siblings(".img").css("background-image", "url('" + $(this).val() + "')");

		})

		
		modalCreateLinkSetting();

		


		$("#DnnMediaBox").addClass("is-modal");
	//	$("#divElementType").find("select,input").on("change", function () {
	//		jQuery(".is-wrapper").data("contentbox").settings.onChange();
	//	})

		$(".cell-tool-option-container .cell-image").on("click mouseup", function () {

			$("#imgaesTEXTURL").val('').siblings(".img").css("background-image", '');
			$(".imgaes-setting-option .loadimg").show();
			$("#imgaesLoad").prop("checked", false);

			$("#DnnMediaBox").attr("type", "insertimage").fadeIn().find("iframe").attr("src", getMediaUrl({
				mimid: $(".is-wrapper").data("mid"),
				FileType:"image"
			}));

		})


		$(".custom-module").on("click", function () {
			$(this).mouseover();
		})

		jQuery('.cell-block-options > div > div[data-block="h4"]').after('<div title="Heading 5" data-block="h5"><h5>Heading 5</h5></div><div title="Heading 6" data-block="h6"><h6>Heading 6</h6></div>')

		jQuery('.cell-block-options > div > div').off('click');

		jQuery('.cell-block-options > div > div').on('click', function (e) {
			var s = jQuery(this).data('block');
			var elm = $(".elm-active");
			if (elm.length && Array("br", "hr", "img", "input", "param", "meta", "link").indexOf(elm[0].tagName.toLowerCase()) == -1) {
				var currhtml = elm[0].outerHTML;
				var html = "<" + s + currhtml.substring(currhtml.indexOf(" "), currhtml.lastIndexOf("<")) + "</" + s + ">";
				elm.after(html).remove();
			}
			jQuery(".cell-block-options").css("display", "none");
		});
		jQuery('.edit-select').each(function () {
			var e = $(this);
			e.children(".select-text").on('focus', function () {
				$(this).siblings(".insel").find('li[value="' + $(this).val() + '"]').addClass("active").siblings().removeClass("active");
				$(this).siblings(".insel").fadeIn(100);
			}).on('blur', function () {
				$(this).siblings(".insel").fadeOut(100);
			});
			e.find(".insel li").on('click', function () {
				e.children('.select-text').val($(this).attr("value")).change();
			})
		});



		SectionSaveSetting();

		SectionCodeSetting();

		jQuery('.element-remove').on('mousedown', function () {
			var $el = jQuery("#divElementTool").data('active');
			if (!$el.siblings(":not(.is-row-tool)").length && $el.parent().parent().hasClass("row")) {
				$el.siblings(".is-row-tool").remove();
				$el.after('<div class="is-builder ui-sortable builder-placeholder"><button class="row-add-initial">Empty<br><span>+ Click to add section</span></button></div>');
			}
		})

		$("#chkParallaxBg").parent().parent().remove();
		$("#chkAnimateBg").parent().parent().after(`<label for="ParallaxBg" style="margin:0;"><input id="ParallaxBg" type="checkbox"> Parallax</label>`);

		jQuery('#ParallaxBg').on('click', function (e) {
			if ($(this).is(":checked")) {
				$isSection.addClass("bg-attachment-fixed");
			} else {
				$isSection.removeClass("bg-attachment-fixed");
			}
		})
		$("#lnkeditbox").on("mouseup", function () {
			if ($isSection.hasClass("bg-attachment-fixed")) {
				jQuery('#ParallaxBg').prop("checked", true)
			} else {
				jQuery('#ParallaxBg').prop("checked", false)
			}
		
			if ($isSection.hasClass("section-slider")) {
				jQuery('#divBoxSize').children().hide();
			} else {
				jQuery('#divBoxSize').children().show();
			}

		})

		modalImageLinkSetting();


		ChangeCellSetting();
		
		CustomModuleSetting();

		contentVideoSetting();
		sectionTasbsPanel();
		sectionSliderPanel();


		//display Save icon
		$("#divBoxBasic select,#divBoxBasic input,.is-modal.elementstyles input,.is-modal.elementstyles select,#divBoxImage input").on("change", function () {
			DisplaySaveButton();
		})
		$(".is-modal.elementstyles select").on("click", function () {
			DisplaySaveButton();
		})
	

	}

	function DisplaySaveButton() {
		jQuery(".is-wrapper").data("change", true);
		jQuery(".is-wrapper").data("contentbox").settings.onHistory();
		pagemodification = true;
		if (pagemodification) {
			$("#ContentBuilderSaveHTML").removeAttr("disabled");
		}		
	}

	function DnnVersions() {
		if ($("#personaBar-iframe").length) {
			$("body").addClass("dnn-9")
		} else if ($("#ControlBar_ControlPanel").length) {
			$("body").addClass("dnn-8")
		} else {
			$("body").addClass("dnn-editor")
		}
		$("body").addClass("builder-editor-mode")
	}

	window.addEventListener('beforeunload', (event) => {
		if (pagemodification && !preview) {
			event.preventDefault();
			$('body').removeClass('page-is-changing');
			event.returnValue = 'No save';
		} else {
			return;
		}
	});
	window.addEventListener('unload', (event) => {
		sessionStorage.clear();
	});
	if (EnableHBControls) {
		if (EnableHBControls.indexOf("Section") == -1) {
			$('.is-sidebar [data-content="divSidebarSections"],#divSidebarSections').remove();
			$(".d-placeholder").remove();
		}
		if (EnableHBControls.indexOf("Content") == -1) {
			$('.is-sidebar [data-content="divSidebarSnippets"],#divSidebarSnippets').remove();
			$(".cell-add-options .cell-add-more").remove();
		}
		if (EnableHBControls.indexOf("Typography") == -1) {
			$('.is-sidebar [data-content="divSidebarTypography"],#divSidebarTypography').remove();
			$(".cmd-box-typography").parents(".is-settings")[0].remove();
		}
		if (EnableHBControls.indexOf("Code") == -1) {
			$('.is-sidebar [data-content="divSidebarSource"],#divSidebarSource,#divSidebarSourceEditCSS').remove();
			$('.is-sidebar2').remove();
		}
		if (!$(".is-sidebar").html()) {
			$(".is-sidebar").remove();
			$(".sidebar-active").css("padding-left", 0)
		}
	}else{
		$(".is-sidebar").remove();
		$('.is-sidebar2').remove();
		$(".sidebar-active").css("padding-left", 0)
	}

	function saveCustomSectionCode() {

		var replace = false;
		$("body").append(`<div id="save-section-name-box" style="display:none">
						<div class="content">
							
							<div class="pic" id="add-section-name-images">
								<img src="data:image/svg+xml,%3Csvg viewBox='0 0 370 277' style='background:%23f3f3f3;' wdith='370' xmlns='http://www.w3.org/2000/svg'/%3E"  />
							</div>
							<div class="text"><input type="text" id="add-section-name" placeholder="Section Title"></div>

							<div><textarea type="text" id="add-section-name-text" style="display:none"></textarea></div>
							<div class="message" style="display:none"></div>
							<div class="button">
							<button id="cancel-section-name-button" class="input-cancel classic-secondary">Cancel</button> 
							<button id="add-section-name-button" class="input-ok classic-primary">Save</button>
							</div>
						</div>
					</div>
					<input type="text" id="add-section-Update-list" style="display:none">	
					`);


		var addsectionCode = function () {
			$("#save-section-name-box").addClass("loading");


			customSectionCodeNew = {
				"Title": $("#add-section-name").val(),
				"Content": $("#add-section-name-text").val(),
				"Image": $("#add-section-name-images img").attr("src")
			};


			$.post(SaveSectionServiceUrl + $(".is-wrapper").data("mid"), customSectionCodeNew, function (data) {
				if (data) {
					data = jQuery.parseJSON(data);
				}
				if (data && data["Success"]) {
					var currData = {
						"Title": data["Results"]["Title"],
						"Image": data["Results"]["Image"],
						"ID": data["Results"]["ID"]
					};
					customSectionCode.push(currData);


					customSectionCodeNew = currData;
					customSectionCodeNew["ID"] = data["Results"]["ID"];

					$("#add-section-Update-list").change();
					$("#save-section-name-box").addClass("success").delay(1000).queue(function () {
						$(this).fadeOut(function () {
							$(this).removeClass("success");
							closePanel();
						}).dequeue();
					});
				} else {
					var message = $("#save-section-name-box .message");
					message.after(message.clone(false));
					message.remove();
					$("#save-section-name-box .message").stop().addClass("error").html(data["Message"]).fadeIn(200).delay(3000).fadeOut(200);
					$("#save-section-name-box").removeClass("success loading");

				}

			})


		}
		var closePanel = function () {
			$("#save-section-name-box").removeClass("success");
			$("#add-section-name").val("");
			$("#add-section-name-text").val("");
			$("#save-section-name-box .text").show();
			$("#add-section-name-button").show();
			$("#save-section-name-box .message").hide().removeClass("error");
			$("#add-section-name-images").html("");
			$("html").css({
				"overflow": "",
				"padding-right": ''
			});
		}

		$("#add-section-name-button").on("click", function (event) {
			event.preventDefault();
			if ($("#add-section-name").val()) {
				addsectionCode();
			} else {
				$("#add-section-name").addClass("error").focus();
			}
		})

		$("#add-section-name").on("change", function () {
			if ($(this).val()) {
				$(this).removeClass("error");
			} else {
				$(this).addClass("error")
			}
		})

		$("#cancel-section-name-button").unbind().on("click", function (event) {
			event.preventDefault();
			$("#save-section-name-box").fadeOut(function () {
				closePanel();
			});
		})

	
		if(typeof AOS =="undefined"){
				$("#tabElementAnimation").off().attr("title","Please Activate CSS3 Animations").css("opacity","0.4").on("click",function(){ return false; });
		}


	}
	saveCustomSectionCode();

	//is-modal image link
	function modalImageLinkSetting(){
		$(".is-modal.imagelink").each(function () {
			var box = $(this);

			box.find(".image-src").before(`
				<div class="pb-10">Link Type: </span>
				<select id="image-link-type">
					<option value="link">Link</option>
					<option value="image">Image (Lightbox)</option>
					<option value="youtube">YouTube Video (Lightbox)</option>
					<option value="vimeo">Vimeo Video (Lightbox)</option>
					<option value="html5">HTML5 Video (Lightbox)</option>
					<option value="map">Google Maps (Lightbox)</option>
				</select><span>
			`);		

			box.find(".input-ok").parent().before(`
			<div class="group-box">	<input id="image-input-group" type="text" placeholder="Image Group"></div>
			`);


			$("#image-link-type").on("change", function () {
				var v = $(this).val();
				if (v != "link") {
					box.find(".input-newwindow").parent().hide();
				} else {
					box.find(".input-newwindow").parent().show();
				}
				box.find(".group-box").hide();

				var n =v.replace(v[0], v[0].toUpperCase());
				if(n=="Html5"){
					n="MP4 Video"
				}

				box.find(".input-link").val("").attr("placeholder", n + " URL");
				box.find(".input-link-box").removeClass("two");

				if (v == "link") {
					box.find(".linkUrlButton").show();
					box.find(".linkImageButton").show().removeClass("icon-img icon-video");
					box.find(".input-link-box").addClass("two");
				}else if(v == "image") {
					box.find(".linkUrlButton").hide();
					box.find(".linkImageButton").show().removeClass("icon-video").addClass("icon-img");
					box.find(".group-box").show();
				}else if(v == "html5") {
					box.find(".linkUrlButton").hide();
					box.find(".linkImageButton").show().removeClass("icon-img").addClass("icon-video");
				} else {
					box.find(".linkUrlButton").hide();
					box.find(".linkImageButton").hide();
				}
			})		

			box.find(".input-ok").on("mouseup", function () {
				if ($("#image-link-type").val() != "link") {
					setTimeout(function () {
						jQuery("#divImageTool").data('active').parent("a").addClass("is-lightbox").attr("data-ilightbox", $("#image-link-type").val());
						if ($("#image-link-type").val() == "image" && $("#image-input-group").val()) {
							jQuery("#divImageTool").data('active').parent("a").attr("data-group", $("#image-input-group").val())
						}else{
							jQuery("#divImageTool").data('active').parent("a").removeAttr("data-group")
						}
					}, 100);
				} else {
					setTimeout(function () {
						jQuery("#divImageTool").data('active').parent("a").removeClass("is-lightbox").removeAttr("data-ilightbox").removeAttr("data-group");
					}, 100)
				}



			})
			$("#divImageTool button.image-link").on("mouseup", function () {
				var ilightbox = jQuery("#divImageTool").data('active').parent("a").attr("data-ilightbox");
				ilightbox ? $('#image-link-type option[value="' + ilightbox + '"]').prop("selected", "selected").siblings().prop("selected",false) : $('#image-link-type option').eq(0).prop("selected", "selected").siblings().prop("selected",false);
				$("#image-input-group").val(jQuery("#divImageTool").data('active').parent("a").attr("data-group"))

				$('#image-link-type').change();
			})
		 
			box.find(".image-src").hide();
			box.find("#form-upload-larger").parent().hide();

			box.find('input[type="text"]').each(function(){
				$(this).before('<p class="modal-title">'+$(this).attr("placeholder")+':</p>').removeAttr("style")
			});
			
			GetLinkSetting(box.find(".input-link"));
			$(".imagelink .input-ok").parent().addClass("control-button");

		})

	}
	//is-modal Create Link
	function modalCreateLinkSetting(){
		$(".is-modal.createlink").each(function () {
			var box = $(this);
			var linktype = "link";
			box.find(".link-src").before(`
				<div class="pb-10">Link Type: </span>
				<select id="link-type-select">
					<option value="link">Link</option>
					<option value="image">Image (Lightbox)</option>
					<option value="youtube">YouTube Video (Lightbox)</option>
					<option value="vimeo">Vimeo Video (Lightbox)</option>
					<option value="html5">Html5 Video (Lightbox)</option>
					<option value="map">Google Maps (Lightbox)</option>
				</select><span>
			`);
			$("#link-type-select").on("change", function () {
				var v = $(this).val();
				if (v != "link") {
					box.find(".input-newwindow").parent().hide();
				} else {
					box.find(".input-newwindow").parent().show();
				}
				var n =v.replace(v[0], v[0].toUpperCase());
				if(n=="Html5"){
					n="MP4 Video"
				}
				box.find(".input-url").val("").attr("placeholder", n + " URL");

				box.find(".input-link-box").removeClass("two");
				if (v == "link") {
					box.find(".linkUrlButton").show();
					box.find(".linkImageButton").show().removeClass("icon-img icon-video");
					box.find(".input-link-box").addClass("two");
				}else if(v == "image") {
					box.find(".linkUrlButton").hide();
					box.find(".linkImageButton").show().removeClass("icon-video").addClass("icon-img");
					box.find(".group-box").show();
				}else if(v == "html5") {
					box.find(".linkUrlButton").hide();
					box.find(".linkImageButton").show().removeClass("icon-img").addClass("icon-video");
				} else {
					box.find(".linkUrlButton").hide();
					box.find(".linkImageButton").hide();
				}

			})

			box.find(".input-ok").on("mouseup", function () {
				if (linktype == "icon") {
					var cur = jQuery("#divIconTool").data('active').parent();
				} else {
					var cur = jQuery("#divLinkTool").data('active');
				}

				if ($("#link-type-select").val() != "link") {
					cur.addClass("is-lightbox").attr("data-ilightbox", $("#link-type-select").val());
				} else {
					cur.removeClass("is-lightbox").removeAttr("data-ilightbox");
				}
			})

			$("#divIconTool .icon-link,#divLinkTool button").on("mouseup", function () {
				if ($(this).parent().attr("id") == "divIconTool") {
					linktype = "icon";
				} else {
					linktype = "link";
				}
				if (linktype == "icon") {
					var ilightbox = jQuery("#divIconTool").data('active').parent().attr("data-ilightbox");
				} else {
					var ilightbox = jQuery("#divLinkTool").data('active').attr("data-ilightbox");
				}
				var v = box.find(".input-url").val();
				ilightbox ? $('#link-type-select option[value="' + ilightbox + '"]').prop("selected", "selected").siblings().prop("selected",false).change() : $('#link-type-select option').eq(0).prop("selected", "selected").siblings().prop("selected",false).change();
				box.find(".input-url").val(v)
			})


			box.find('input[type="text"]').each(function(){ 
				if($(this).attr("placeholder")=="Url"){$(this).attr("placeholder","URL")};
				$(this).before('<p class="modal-title">'+$(this).attr("placeholder")+':</p>').removeAttr("style");
			});
			
			GetLinkSetting(box.find(".input-url"));
			$(".createlink .input-ok").parent().addClass("control-button");

		})
	}

	//Video modal
	function contentVideoSetting() {
		$("#divIframeTool").append(`<button title="Source" class="video-link" style="width:50px;height:50px;display:none"><svg class="is-icon-flex" style="fill:rgba(0, 0, 0, 0.65);width:17px;height:17px;"><use xlink:href="#ion-link"></use></svg></button>`)

		$(".d-wrapper").on("click", ".embed-responsive", function () {
			if ($(this).find("video").length) {
				$("#divIframeTool .iframe-link").hide();
				$("#divIframeTool .video-link").show();
			} else {
				$("#divIframeTool .iframe-link").show();
				$("#divIframeTool .video-link").hide();
			}
			$(".is-modal.videolink").data("active", $(this));
		})
		$("#divIframeTool .video-link").on("click", function () {

			var video = $(".is-modal.videolink").data("active").find("video");

			if (video.find('source[type="video/mp4"]').length) {
				$(".is-modal.videolink .video-mp4-src").val(video.find('source[type="video/mp4"]').attr("src"))
			} else {
				$(".is-modal.videolink .video-mp4-src").val("")
			}
			if (video.find('source[type="video/ogg"]').length) {
				$(".is-modal.videolink .video-ogg-src").val(video.find('source[type="video/ogg"]').attr("src"))
			} else {
				$(".is-modal.videolink .video-ogg-src").val("")
			}
			if (video.find('source[type="video/webm"]').length) {
				$(".is-modal.videolink .video-webm-src").val(video.find('source[type="video/webm"]').attr("src"))
			} else {
				$(".is-modal.videolink .video-webm-src").val("")
			}
			if (video.attr("poster")) {
				$(".is-modal.videolink .video-poster-src").val(video.attr("poster"))
			} else {
				$(".is-modal.videolink .video-poster-src").val("")
			}
			if (video.attr("autoplay")) {
				$(".is-modal.videolink .video-autoplay").prop("checked", true)
			} else {
				$(".is-modal.videolink .video-autoplay").prop("checked", false)
			}
			if (video.attr("controls")) {
				$(".is-modal.videolink .video-controls").prop("checked", true)
			} else {
				$(".is-modal.videolink .video-controls").prop("checked", false)
			}
			if (video.attr("loop")) {
				$(".is-modal.videolink .video-loop").prop("checked", true)
			} else {
				$(".is-modal.videolink .video-loop").prop("checked", false)
			}
			if (video.attr("muted")) {
				$(".is-modal.videolink .video-muted").prop("checked", true)
			} else {
				$(".is-modal.videolink .video-muted").prop("checked", false)
			}
			if (video.attr("preload")) {
				$('.is-modal.videolink .video-preload option[value="' + video.attr("preload") + '"]').attr("selected", "selected").siblings().prop("selected", false);
			} else {
				$('.is-modal.videolink .video-preload option').prop("selected", false);
			}
			$(".is-modal.videolink").addClass("active").siblings(".is-modal").removeClass("active");

		})

		$(".is-modal.iframelink").after(`
		<div class="is-modal videolink">
		<div class="is-modal-overlay" style="position:absolute;top:0;left:0;width:100%;height:100%;background:rgba(255,255,255,0.3);z-index:-1;"></div>
		<div style="max-width:550px;">
		<p class="modal-title">MP4 Source:</p>
		<input class="video-mp4-src" type="text" placeholder="MP4 Source" style="width: 100%; margin-bottom: 12px; display: block;">
		<p class="modal-title">OGG Source:</p>
		<input class="video-ogg-src" type="text" placeholder="OGG Source" style="width: 100%; margin-bottom: 12px; display: block;">
		<p class="modal-title">WEBM Source:</p>
		<input class="video-webm-src" type="text" placeholder="WEBM Source" style="width: 100%; margin-bottom: 12px; display: block;">
		<div class=" mb-20">
		<p class="modal-title">Poster:</p>
		<input class="video-poster-src" type="text" placeholder="Poster" style="width: 100%; margin-bottom: 12px; display: block;">
		</div>
		<label class="mr-10"><input type="checkbox" class="video-autoplay"> Auto Play</label>
		<label class="mr-10"><input type="checkbox" class="video-controls"> Controls</label>
		<label class="mr-10"><input type="checkbox" class="video-loop"> Loop</label>
		<label class="mr-10"><input type="checkbox" class="video-muted"> Muted</label>
		<p class="mb-0"><label>Preload</label>
		<select class="video-preload">
		<option value="auto">Auto</option>
		<option value="meta">Meta</option>
		<option value="none">None</option>
		</select></p>	
		<div style="text-align:right"><button title="Cancel" class="input-cancel classic-secondary">Cancel</button><button title="Ok" class="input-ok classic-primary">Ok</button></div>
		</div>
		</div>	
		`)
		$(".is-modal.videolink .input-cancel").on("click", function () {
			$(".is-modal.videolink").removeClass("active");
		})
		$(".is-modal.videolink .input-ok").on("click", function () {
			var video = $(".is-modal.videolink").data("active").find("video");
			var newvideo = $("<video>");
			newvideo.attr("class", video.attr("class"));
			newvideo.attr("style", video.attr("style"));
			newvideo.attr("id", video.attr("id"));
			if ($(".is-modal.videolink .video-mp4-src").val()) {
				newvideo.append('<source src="' + $(".is-modal.videolink .video-mp4-src").val() + '" type="video/mp4"/>')
			}
			if ($(".is-modal.videolink .video-ogg-src").val()) {
				newvideo.append('<source src="' + $(".is-modal.videolink .video-ogg-src").val() + '" type="video/ogg"/>')
			}
			if ($(".is-modal.videolink .video-webm-src").val()) {
				newvideo.append('<source src="' + $(".is-modal.videolink .video-webm-src").val() + '" type="video/webm"/>')
			}
			if ($(".is-modal.videolink .video-poster-src").val()) {
				newvideo.attr("poster", $(".is-modal.videolink .video-poster-src").val())
			}
			if ($(".is-modal.videolink .video-autoplay").is(":checked")) {
				newvideo.attr("autoplay", "autoplay")
			}
			if ($(".is-modal.videolink .video-controls").is(":checked")) {
				newvideo.attr("controls", "controls")
			}
			if ($(".is-modal.videolink .video-loop").is(":checked")) {
				newvideo.attr("loop", "loop")
			}
			if ($(".is-modal.videolink .video-muted").is(":checked")) {
				newvideo.attr("muted", true)
			}
			if ($(".is-modal.videolink .video-preload").val() && $(".is-modal.videolink .video-preload").val() != "auto") {
				newvideo.attr("preload", $(".is-modal.videolink .video-preload").val())
			}
			video.before(newvideo);
			video.remove();
			$(".is-modal.videolink").removeClass("active");
		})

		GetLinkSetting($(".is-modal.videolink .video-mp4-src"));
		GetLinkSetting($(".is-modal.videolink .video-ogg-src"));
		GetLinkSetting($(".is-modal.videolink .video-webm-src"));
		GetLinkSetting($(".is-modal.videolink .video-poster-src"));

		$(".is-modal.videolink .linkUrlButton").hide();
		$(".is-modal.videolink .video-mp4-src,.is-modal.videolink .video-ogg-src,.is-modal.videolink .video-webm-src").siblings(".link-button").find(".linkImageButton").addClass("icon-video");
		$(".is-modal.videolink .video-poster-src").siblings(".link-button").find(".linkImageButton").addClass("icon-img");
	 
		
	}


	//Link Setting

	function GetLinkSetting(e){
		e.wrap('<div class="input-link-box"></div>')
		var linkUrlButton=$('<button type="button" class="linkUrlButton"></button>');
		var linkImageButton=$('<button type="button" class="linkImageButton"></button>');
		var boxbutton =$('<div class="link-button"></div>');
		var box = e.parents(".is-modal");
		var list = LinkNavSelect.clone();
			boxbutton.append(linkUrlButton);
			boxbutton.append(linkImageButton);
			e.after(boxbutton);
			e.after(list);

			linkImageButton.on("click", function () {
				var val =$(this).siblings('input[type="text"]').val();

				$("#imgaesTEXTURL").val(val);
				var imgStr = /\.(jpg|jpeg|png|bmp|gif|webp|tif)$/;
				if(val && imgStr.test(val.toLowerCase())){
					$("#imgaesTEXTURL").siblings(".img").css("background-image", "url('" + $(".elm-active").attr("src") + "')");
				}
				$(".imgaes-setting-option .loadimg").hide();
				var type = false;
					if($(this).hasClass("icon-video")){
						type ="video";
					}if($(this).hasClass("icon-img")){
						type ="image";
					}
				$("#DnnMediaBox").data("active",e).attr("type", "link").fadeIn().find("iframe").attr("src", getMediaUrl({
					mimid: $(".is-wrapper").data("mid"),
					FileType:type
				}));

			})
			linkUrlButton.on("click", function () {	
				var h =$(window).height() +  $(window).scrollTop() - $(this).parent().offset().top -$(this).parent().height()-5;
				$(this).parent().siblings(".link-list-select").css("max-height",h).slideDown();

			})
			list.on("click","li",function(){
				$(this).parent().siblings('input[type="text"]').val($(this).attr("value")).focus();
				$(this).parent().slideUp();
			})
			box.on("click", function (e) {
				if (!$(e.target).closest(".input-link,.linkUrlButton,.link-list-select,li").length) {
					$(".link-list-select").slideUp();
				}
			})
	}
	//section save
	function SectionSaveSetting(){
		jQuery(".content-builder").on("click", ".is-section-save", function () {
			$("html").css({
				"overflow": "hidden",
				"padding-right": window.innerWidth - $(window).width()
			});
			$("#save-section-name-box").fadeIn().addClass("loading");
			if (typeof html2canvas != undefined) {
				var is_section = $(this).parents(".is-section").eq(0).clone(false);

				var clone_section = $("<div class=\"d-wrapper\" style=\"position: fixed;background-color: #fff; width: 100%;overflow: hidden;left: 0;top:100%;\"></div>");
				clone_section.append(is_section);

				$("body").append(clone_section);

				if (is_section.find(".sico").length) {
					is_section.find(".sico svg").each(function () {
						var color = $(this).css("color");
						var size = $(this).width();
						var svgbox = $(".section-svg-" + $(".is-wrapper").data("mid")).find($(this).find("use").attr("xlink:href"));
						$(this).html(svgbox.html());
						$(this).attr({
							"width": size,
							"height": size,
							"viewBox": svgbox.attr("viewBox"),
							"fill": color
						})
						$(this).find("*").attr("fill", color);
					});
					is_section.find(".icon-svg svg").each(function () {
						var color = $(this).css("color");
						$(this).find("*").removeAttr("style");
						$(this).find("*").attr("stroke", color);
						$(this).find("*").attr("fill", "none");
					});

				}

				$("#divFb,#divCb,#save-section-name-box,.mobilemenu-main,.personalBarContainer,.editBarFrameContainer,.is-section-tool,.dng-wrapper").attr("data-html2canvas-ignore", true);

				clone_section.find(".swiper-wrapper,.swiper-slide").css("transform", "translate3d(0px, 0px, 0px)");
				clone_section.find('.swiper-container.swiper-container-fade .swiper-slide').css({
					"transform": "translate3d(0px, 0px, 0px)",
					"opacity": 1
				});

				clone_section.find(".is-row-tool").remove();

				setTimeout(function () {
					html2canvas(clone_section.get(0), {
						logging: false,
						//	useCORS: true,
						//	allowTaint:true,
						scale: 0.7
					}).then(function (canvas) {
						var base64encodedstring = canvas.toDataURL("image/jpeg", 0.8);
						if (base64encodedstring) {
							$("#add-section-name-images").html(`<img src="${base64encodedstring}" />`);
						} else {
							$("#add-section-name-images").html(`<img src="data:image/svg+xml,%3Csvg viewBox='0 0 370 277' style='background:%23f3f3f3;' wdith='370' xmlns='http://www.w3.org/2000/svg'/%3E" />`);
						}

						$("#save-section-name-box").removeClass("loading");
						$("#add-section-name").focus();
						//	clone_section.remove();
					});
				}, 500)
			} else {
				$("#save-section-name-box").removeClass("loading");

			}

			$(this).parents(".is-section").addClass("d-save-section");
			saveHTML($(this).parents(".content-builder"), "section");
		})
	}
	//Section code
	function SectionCodeSetting(){
		jQuery(".content-builder").on("click", ".is-section-code", function () {
			var $modal = jQuery('.is-modal.viewhtmlformatted');
			$modal.find('#ifrHtmlFormatted').attr('src', 'about:blank')
			$modal.addClass('is-modal-small active');
			isSectionCode = $(this).parents(".is-section");
			isSectionCode.addClass("d-section-code");

			var sHTML = $($(".is-wrapper").data('contentbox').html()).filter(".d-section-code").removeClass("d-section-code");
			$(".d-section-code").removeClass("d-section-code");

			sHTML.find(".custom-module").each(function () {
				$(this).empty();
				$(this).attr("data-html", false);
				$(this).addClass("loading");
			})
			if (sHTML.hasClass("section-slider")) {
				EmptySectionSlider(sHTML);
			}

			$modal.find('textarea').val(sHTML[0].outerHTML);

			$modal.find('#ifrHtmlFormatted').attr('src', SkinPath + 'Resource/vendor/content-builder/contentbuilder/html_small2.html?1')
		})

	}

	// Change Cell Setting
	function ChangeCellSetting(){
		jQuery('.cell-duplicate').after(`<button title="Duplicate" class="cell-duplicate-clone" style="display: block;"><svg class="is-icon-flex" style="width:14px;height:14px;"><use xlink:href="#ion-ios-photos-outline"></use></svg></button>`);
		jQuery('.cell-duplicate').remove();

		var changecellClassName = function (name, item) {

			item = Math.max(Math.min(item, 12), 1);
			if (name.indexOf("col-xl-") != -1) {
				name = jQuery.trim(name.replace(/(col-xl-)(.+?(\s|$))/, "col-xl-" + item + " "));
			} else if (name.indexOf("col-lg-") != -1) {
				name = jQuery.trim(name.replace(/(col-lg-)(.+?(\s|$))/, "col-lg-" + item + " "));
			} else if (name.indexOf("col-md-") != -1) {
				name = jQuery.trim(name.replace(/(col-md-)(.+?(\s|$))/, "col-md-" + item + " "));
			} else if (name.indexOf("col-sm-") != -1) {
				name = jQuery.trim(name.replace(/(col-sm-)(.+?(\s|$))/, "col-sm-" + item + " "));
			} else if (name.indexOf("col-") != -1) {
				name = jQuery.trim(name.replace(/(col-)(.+?(\s|$))/, "col-" + item + " "));
			}
			return name;
		}
		var getcellClassItem = function (name) {
			if (name.indexOf("col-xl-") != -1) {
				return parseInt(name.split("col-xl-")[1].split(" ")[0]);
			} else if (name.indexOf("col-lg-") != -1) {
				return parseInt(name.split("col-lg-")[1].split(" ")[0]);
			} else if (name.indexOf("col-md-") != -1) {
				return parseInt(name.split("col-md-")[1].split(" ")[0]);
			} else if (name.indexOf("col-sm-") != -1) {
				return parseInt(name.split("col-sm-")[1].split(" ")[0]);
			} else if (name.indexOf("col-") != -1) {
				return parseInt(name.split("col-")[1].split(" ")[0]);
			}
			return 0;
		}

		jQuery('.cell-duplicate-clone').on('click', function (e) {
			var $block = jQuery('#divCellTool').data('active');
			if (!$block) return;
			jQuery('.cell-active').removeClass('cell-active');
			jQuery('.row-active').removeClass('row-active');
			$block.addClass('cell-active');
			var wrap = false;
			var width = 0;
			$block.parent().children().not('.is-row-tool').each(function () {
				width += $(this).innerWidth();
			})
			if (width > $block.parent().innerWidth()) {
				wrap = true;
			}
			$block.clone().insertBefore($block).removeClass('cell-active');

			if ($block.attr("class")) {
				var name = $block.attr("class");

				if ($block.parent().children().not('.is-row-tool').length <= 4 && !wrap) {
					var item = 12 / $block.parent().children().not('.is-row-tool').length;
					$block.attr("class", changecellClassName(name, item));
					$block.parent().children().not('.is-row-tool').each(function () {
						var childrenname = $(this).attr("class");
						$(this).attr("class", changecellClassName(childrenname, item));
					})
				}
			}

			jQuery(".is-wrapper").data("contentbox").setup();
			jQuery(".is-wrapper").data("contentbox").settings.onRender();
			jQuery(".is-wrapper").data("contentbox").settings.onChange();



		});

		jQuery('.cell-increase').after(`<button title="Increase" data-dec="plus" class="cell-increase-clone" style="display: inline-block;"><svg class="is-icon-flex"><use xlink:href="#icon-increase"></use></svg></button>`);

		jQuery('.cell-decrease').after(`<button title="Decrease" data-dec="minus" class="cell-decrease-clone" style="display: inline-block;"><svg class="is-icon-flex"><use xlink:href="#icon-decrease"></use></svg></button>`);
		jQuery('.cell-increase,.cell-decrease').remove();

		jQuery('.cell-increase-clone,.cell-decrease-clone').on('click', function (e) {
			cellolditem = 0;
			var $block = jQuery('#divCellTool').data('active');
			if (!$block) return;
			var name = $block.attr("class");
			var blockItem = getcellClassItem(name);
			var dec = $(this).data("dec");
			var $nextbox = false;
			if ($block.next(":not(.is-row-tool)").length) {
				$nextbox = $block.next();
				var afterDom = $nextbox.prevAll();
				var afterDomItem = 0;
				$(afterDom.toArray().reverse()).each(function () {
					if ($(this).attr("class")) {
						var i = getcellClassItem($(this).attr("class"));
						if (afterDomItem + i > 12) {
							afterDomItem = i;
						} else {
							afterDomItem += i;
						}
					}
				})
				afterDomItem += getcellClassItem($nextbox.attr("class"));
				if (afterDomItem > 12) {
					if ($block.prev(":not(.is-row-tool)").length) {
						$nextbox = $block.prev();
					}
				}
			} else if ($block.prev(":not(.is-row-tool)").length) {
				$nextbox = $block.prev();
			}
			if ($nextbox) {
				var nextname = $nextbox.attr("class");
				var nextItem = getcellClassItem(nextname);
				if (!blockItem || (nextItem == 1 && dec == "plus") || (blockItem == 11 && dec == "plus") || (dec != "plus" && blockItem == 1)) {
					return false;
				}
				nextname = changecellClassName(nextname, dec == "plus" ? nextItem - 1 : nextItem + 1);
				$nextbox.attr("class", nextname);
			}
			blockItem = dec == "plus" ? blockItem + 1 : blockItem - 1;
			name = changecellClassName(name, blockItem);
			$block.attr("class", name);
			jQuery(".is-wrapper").data("contentbox").settings.onChange();
		})

		var cellremove = jQuery('.is-modal.delconfirm button.input-ok');
		cellremove.after(`<button title="Delete" class="classic cell-remove-clone" style="display:none">Delete</button>`);
		//	cellremove.remove();

		var cellremoveclone = jQuery('.cell-remove-clone');

		$(".cell-remove").on("mousedown", function () {
			cellremoveclone.show();
			cellremove.hide();
			
		})
		$(".row-remove,.element-remove,.is-section-remove,.icon-remove").on("mousedown", function () {
			cellremoveclone.hide();
			cellremove.show();
		})

		jQuery('.cell-remove-clone').on('click', function (e) {
			jQuery(".is-modal.delconfirm").removeClass("active");
			var $block = jQuery('#divCellTool').data('active');
			if (!$block) return;
			jQuery('.cell-active').removeClass('cell-active');
			jQuery('.row-active').removeClass('row-active');

			var parent = $block.parent();
			$block.remove();
			if (parent.children().not('.is-row-tool').length <= 4) {
				parent.children().not('.is-row-tool').each(function () {
					var childrenname = $(this).attr("class");
					item = 12 / parent.children().not('.is-row-tool').length;
					$(this).attr("class", changecellClassName(childrenname, item));
				})
			}
			jQuery(".is-wrapper").data("contentbox").settings.onChange();

		})

		cellremoveclone.hide();
		cellremove.show();


	}


	//Custom Module Setting
	function CustomModuleSetting(){
		$("#divCustomModuleTool").append(`<button class="delete" title="Delete"><svg class="is-icon-flex" style="width:30px;height:30px;"><use xlink:href="#ion-ios-close-empty"></use></svg></button>`);
		$(".is-modal.delconfirm").after(`<div class="is-modal delmodule"><div class="is-modal-overlay" style="position:absolute;top:0;left:0;width:100%;height:100%;background:rgba(255,255,255,0.000001);z-index:-1;"></div><div style="max-width:526px;text-align:center;"><p>Are you sure you want to delete this module?</p><button title="Delete" class="classic module-remove-clone">Delete</button></div></div>`)
		$("#divCustomModuleTool .delete").off().on("click", function () {
			$(".is-modal.delmodule").addClass("active")
		})
		$(".is-modal.delmodule .is-modal-overlay").off().on("click", function () {
			$(".is-modal.delmodule").removeClass("active");
		})
		$(".module-remove-clone").off().on("click", function () {
			$(".custom-module.d-custom-active").remove();
			jQuery(".is-wrapper").data("contentbox").settings.onChange();
			$(".is-modal.delmodule").removeClass("active");
			$("#divCustomModuleTool").hide();
		})
		$(".is-modal.delsectionconfirm").off().on("mouseup", function () {
			$isSection = false;
		})
	}

	//Section Tabs
	function sectionTasbsPanel() {
		$("#divElementTool").after(`<div id="divTabsTool" class="is-tool">
		<button title="Add" class="element-add" style="width:35px;height:25px;"><svg class="is-icon-flex" style="fill:rgba(0, 0, 0, 0.8);width:16px;height:16px;"><use xlink:href="#ion-ios-plus-empty"></use></svg></button>

		<button title="Duplicate" class="element-move-left" style="width:35px;height:25px;"><svg class="is-icon-flex" style="fill:rgba(0, 0, 0, 0.7);width:15px;height:15px;"><use xlink:href="#ion-ios-arrow-thin-left"></use></svg></button>
		<button title="Duplicate" class="element-move-right" style="width:35px;height:25px;"><svg class="is-icon-flex" style="fill:rgba(0, 0, 0, 0.7);width:15px;height:15px;"><use xlink:href="#ion-ios-arrow-thin-right"></use></svg></button>

		<button title="Duplicate" class="element-duplicate" style="width:35px;height:25px;"><svg class="is-icon-flex" style="width:10px;height:10px;"><use xlink:href="#ion-ios-photos-outline"></use></svg></button>
		<button title="Delete" class="element-remove" style="width:35px;height:25px;font-size: 11px;"><svg class="is-icon-flex" style="fill:rgba(0, 0, 0, 0.8);width:19px;height:19px;"><use xlink:href="#ion-ios-close-empty"></use></svg></button></div>
		`)

		$(".d-wrapper").on("click", ".section-tabs-list .item", function () {
			$(".cog-tab-active").removeClass("cog-tab-active");
			var e = $(this).addClass("cog-tab-active");
			e.attr("contenteditable", "true");
			$("#divTabsTool").show().css({
				top: e.offset().top + e.height(),
				left: e.offset().left,
			})

			if (e.siblings(".item").length) {
				$("#divTabsTool .element-remove").show();
			} else {
				$("#divTabsTool .element-remove").hide();
			}
			DisplaySaveButton();
		})
		$("#divTabsTool .element-move-left").on("click", function () {
			var cog = $(".cog-tab-active");
			if(cog.prev().length){
				var cont = cog.parents(".section-tabs-list").siblings(".section-tabs-container").find(".is-container").eq(cog.index());
				cog.prev().before(cog);
				cont.prev().before(cont);
			}
			DisplaySaveButton();
		})
		$("#divTabsTool .element-move-right").on("click", function () {
			var cog = $(".cog-tab-active");
			if(cog.next().length){
				var cont = cog.parents(".section-tabs-list").siblings(".section-tabs-container").find(".is-container").eq(cog.index());
				cog.next().after(cog);
				cont.next().after(cont);
			}
			DisplaySaveButton();
		})
		$("#divTabsTool .element-duplicate").on("click", function () {
			var cog = $(".cog-tab-active");
			cog.after(cog.clone());
			var cont = cog.parents(".section-tabs-list").siblings(".section-tabs-container").find(".is-container").eq(cog.index());
			cont.after(cont.clone());
			_cb.applyBehavior();
			cog.next().click();
			DisplaySaveButton();
		})
		$("#divTabsTool .element-add").on("click", function () {
			var cog = $(".cog-tab-active");
			cog.after(cog.clone().text("item " + (cog.index() + 2)));
			var cont = cog.parents(".section-tabs-list").siblings(".section-tabs-container").find(".is-container").eq(cog.index());
			cont.after('<div class="is-container is-builder"></div>');
		//	jQuery(".is-wrapper").data("contentbox").setup();
			_cb.checkEmpty();

			cog.next().click();
			DisplaySaveButton();
		})
		$("#divTabsTool .element-remove").on("click", function () {
			if (confirm("Are you sure you want to delete this item?")) {
				var cog = $(".cog-tab-active");
				cog.parents(".section-tabs-list").siblings(".section-tabs-container").find(".is-container").eq(cog.index()).remove();


				var cl = cog.prev();
				if (!cl.length) {
					cl = cog.next();
				}
				cog.remove();
				cl.click();
				$("#divTabsTool").hide();
				DisplaySaveButton();
			}
		})
	}

 	//Section Slider
	function sectionSliderPanel() {
		//section slider
		jQuery(".d-wrapper").on('mouseenter mouseleave', ".is-section", function (e) {
			$isSection = jQuery(this);
			if ($isSection.hasClass("section-slider")) {
				$("#slider-delete,#slider-duplicate,#slider-add").show();
			} else {
				$("#slider-delete,#slider-duplicate,#slider-add").hide();
			}
			if ($isSection.children().children(".is-box:not(.swiper-slide-duplicate)").length <= 1) {
				$("#slider-delete").hide();
			}
		});
		$("#lnkeditbox").after(`
		<div  id="slider-duplicate" title="Duplicate" style="display:inline-block;width:40px;height:40px;background: rgb(154, 205, 50);line-height: 40px;">
		<svg class="is-icon-flex" style="width:16px;height:16px;fill:rgba(255,255,255,1);"><use xlink:href="#ion-ios-photos-outline"></use></svg>
		</div>
		<div  id="slider-add" title="Duplicate" style="display:inline-block;width:40px;height:40px;background: rgb(57, 205, 50);line-height: 40px;">
		<svg class="is-icon-flex" style="width:27px;height:27px;fill:rgba(255,255,255,1);"><use xlink:href="#ion-ios-plus-empty"></use></svg>
		</div>
		<div id="slider-delete" title="Delete" style="display:inline-block;width:40px;height:40px;background: rgb(247, 99, 46);line-height: 40px;">
		<svg class="is-icon-flex" style="width:35px;height:35px;fill:rgba(255,255,255,1);"><use xlink:href="#ion-ios-close-empty"></use></svg>
		</div>
		`)

		$("#slider-duplicate").on("click", function () {
			var newsisbox = $isbox.clone().removeClass("swiper-slide-next swiper-slide-duplicate-prev swiper-slide-active").removeAttr("data-swiper-slide-index");;
			$isSection[0].swiper.addSlide($isbox.index(), newsisbox);
			$(".is-wrapper").data("contentbox").setup();
			jQuery(".is-wrapper").data("contentbox").settings.onChange();
		})
		$("#slider-add").on("click", function () {
			$isSection[0].swiper.appendSlide(` <div class="is-box swiper-slide"><div class="is-boxes"><div class="is-box-centered"><div class="is-container"></div></div></div></div>`);
			$(".is-wrapper").data("contentbox").setup();
			jQuery(".is-wrapper").data("contentbox").settings.onChange();
		})
		$("#slider-delete").on("click", function () {
			if (confirm("Are you sure you want to delete this item?")) {
				$isSection[0].swiper.removeSlide($isbox.index())
				$(".is-wrapper").data("contentbox").setup();
				jQuery(".is-wrapper").data("contentbox").settings.onChange();

			}
		})
		//section modal
		$(".is-section.section-slider .is-section-tool").append(`<div class="is-section-slider-options" title="Slider Options"></div>`)

		$("#divCb").append(`<div class="is-modal is-modal-slider-options">
				<span class="is-modal-shade"></span>
				<div style="max-width: 400px;">
				<div class="is-settings">
					<label style="margin:0;">
						<input id="section-slider-pagination" type="checkbox"> Pagination
					</label>
				</div>
				<div class="is-settings">
					<label style="margin:0;">
						<input id="section-slider-navigation" type="checkbox"> Navigation
					</label>
				</div>
				<div class="is-settings">
				<label style="margin:0;">
					<input id="section-slider-loop" type="checkbox"> Loop
				</label>
				</div>
				<div class="is-settings">
					<label style="margin:0;">
						<input id="section-slider-autoplay" type="checkbox"> Auto play
					</label>
					
				</div>		
				<div class="is-settings autoplay-delay-box" style="display:none" >
				<label style="margin:0;">
					Auto play delay
				</label>
				<label style="margin:0;">
					<input type="number" id="section-slider-autoplay-delay" value="5000" style="width: 100px; display: inline-block;" placeholder="Auto play delay">
				</label>
				</div>			
					<button class="classic dnn-input-ok">Ok</button>
				</div>
			</div>`);

		$(".is-modal-shade").on("click", function () {
			$(this).parent(".is-modal").removeClass("active")
		})


		jQuery(".content-builder").on("click", ".is-section-slider-options", function () {
			if($isSection.data("pagination")===false){
				$("#section-slider-pagination").prop("checked", false);
			}else{
				$("#section-slider-pagination").prop("checked", true);
			}
			if($isSection.data("navigation")===false){
				$("#section-slider-navigation").prop("checked", false);
			}else{
				$("#section-slider-navigation").prop("checked", true);
			}
			if($isSection.data("loop")===false){
				$("#section-slider-loop").prop("checked", false);
			}else{
				$("#section-slider-loop").prop("checked", true);
			}
			if($isSection.attr("data-autoplay")){
				$("#section-slider-autoplay").prop("checked", true);
				var delay= jQuery.parseJSON($isSection.attr("data-autoplay"))["delay"];
					if(!delay){
						delay=5000;
					}
				$("#section-slider-autoplay-delay").val(delay).parents(".is-settings").show();

			}else{
				$("#section-slider-autoplay").prop("checked", false);
				$("#section-slider-autoplay-delay").parents(".is-settings").hide();
			}
			$(".is-modal-slider-options").addClass('active').siblings(".is-modal").removeClass("active");
		})


		$("#section-slider-autoplay").on("change",function(){
			if($(this).is(":checked")){
				$(".is-modal-slider-options .autoplay-delay-box").show();
			}else{
				$(".is-modal-slider-options .autoplay-delay-box").hide();
			}
		})

		$(".is-modal-slider-options .dnn-input-ok").on("click",function(){
		
			if ($("#section-slider-pagination").is(":checked")) {
				$isSection.removeAttr("data-pagination");
			}else{
				$isSection.attr("data-pagination","false");
			}
			if ($("#section-slider-navigation").is(":checked")) {
				$isSection.removeAttr("data-navigation");
			}else{
				$isSection.attr("data-navigation","false");
			}
			if ($("#section-slider-loop").is(":checked")) {
				$isSection.removeAttr("data-loop");
			}else{
				$isSection.attr("data-loop","false");
			}
			if ($("#section-slider-autoplay").is(":checked")) {
				 
				$isSection.attr("data-autoplay","{\"delay\":"+$("#section-slider-autoplay-delay").val()+"}");
			}else{
				$isSection.removeAttr("data-autoplay");
			}

			$(this).parents(".is-modal").removeClass("active");
			jQuery(".is-wrapper").data("contentbox").settings.onChange();

		})

	}


});



function applyMediaImage(url) {

	$("#MediaAddImages").off();
	url = $("#imgaesTEXTURL").val();
	var type = jQuery("#DnnMediaBox").attr("type");


	

	if (url) {
		if (type == "image") {
			jQuery(".elm-active").attr("src", url).removeAttr("data-src");
		}
		if (type == "link") {
			if($("#DnnMediaBox").data("active")){
				$("#DnnMediaBox").data("active").val(url).focus();
			}else{
				jQuery('.is-modal.imagelink .input-link').val(url).focus();
			}

		} else if (type == "cover") {

			var $activeBox = jQuery(".current-cover-bg");
			if ($activeBox.find(".is-overlay").length == 0) {
				$activeBox.prepend('<div class="is-overlay"></div>')
			}
			var $overlay = $activeBox.find(".is-overlay");
			if ($overlay.find(".is-overlay-bg").length == 0) {
				$overlay.prepend('<div class="is-overlay-bg" style="transform:scale(1.05)" data-bottom-top="transform:translateY(-120px) scale(1.05);" data-top-bottom="transform:translateY(120px) scale(1.05)"></div>');
				$overlay.find(".is-overlay-bg").prepend('<div class="is-overlay-color" style="opacity:0.1"></div>')
			}
			$activeBox.find(".is-overlay-bg").css("background-image", `url('${url}')`).css("background-color", "")
			$activeBox.removeClass("current-cover-bg");
			jQuery(".is-wrapper").data("contentbox").settings.onRender();
			jQuery(".is-wrapper").data("contentbox").settings.onChange();
		} else if (type == "modules") {
			$("#modulesImageVal").val(url).change();
		} else if (type == "setting") {
			$("#inpElmBgImages").val(url).change();
		} else if (type == "insertimage") {
			var hasimg = $(".elm-active img");
			$(".is-modal.insertimage .input-src").val(url);
			$(".is-modal.insertimage .input-ok").click();

			if ($("#imgaesLoad").is(":checked")) {
				if (hasimg.length) {
					hasimg.data("insertimage-odd", true);
					$(".elm-active img").each(function () {
						if ($(this).attr("src") == url && !$(this).data("insertimage-odd")) {
							$(this).addClass("img-Lazy");
						}
					})
					hasimg.data("insertimage-odd", false);
				} else {
					jQuery(".elm-active img").addClass("img-Lazy");
				}
			}

		}
	}
	if (type == "image") {
		if ($("#imgaesLoad").is(":checked")) {
			jQuery(".elm-active").addClass("img-Lazy");

		} else {
			jQuery(".elm-active").removeClass("img-Lazy").removeAttr("data-src");
		}
		$("#imgaesLoad").removeClass("is-change");
		jQuery(".is-wrapper").data("contentbox").settings.onChange();
	} else {
		//	jQuery(".elm-active").removeClass("img-Lazy");
	}


}

function getMediaUrl(options) {

	if(options.FileType){
		return $(".is-wrapper").data("mediaurl")+"&FileType="+options.FileType;
	}else{
		return $(".is-wrapper").data("mediaurl");
	}

}


function EmptySectionSlider(e) {
	e.removeClass("swiper-container-slide swiper-container-horizontal");
	e.children().children(".swiper-slide-duplicate").remove();
	e.children(".swiper-wrapper").removeAttr("style");
	e.children(".swiper-button-next").remove();
	e.children(".swiper-button-prev").remove();
	e.children(".swiper-pagination").remove();
	e.children(".swiper-notification").remove();
	e.children().children(".swiper-slide").removeClass("swiper-slide-next swiper-slide-duplicate-prev swiper-slide-active").removeAttr("data-swiper-slide-index");
}


