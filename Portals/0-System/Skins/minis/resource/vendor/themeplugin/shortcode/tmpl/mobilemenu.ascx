<%@ Control language="vb" AutoEventWireup="false" Explicit="True" Inherits="DotNetNuke.UI.Skins.Skin" %>
<div class="form-group hide">
  <label for="firstname" class="control-label">ID:</label>
  <div class="control-cont">
    <input type="text" class="dnnOptions form-control randomid" value="${id}" data-name="id">
  </div>
</div>
 
<div class="tab-content">
    <div class="form-group colorpicker-box">
      <label for="firstname" class="control-label">Navbar Title:</label>
      <div class="control-cont">
          <input type="text" class="dnnOptions form-control" data-name="navbartitle" value="${navbartitle}">
      </div>
    </div>
  <div class="form-typography-list">
    <div class="form-group">
      <label class="control-label">Level 1 Menu Typography:</label> 
      {{tmpl($data,options ={before:"lv1"}) '#tmpl-typography'}}
    </div>
  </div>
  <div class="form-typography-list">
    <div class="form-group">
      <label class="control-label">Level 2 Menu Typography:</label> 
      {{tmpl($data,options ={before:"lv2"}) '#tmpl-typography'}}
    </div>
  </div>
  <div class="form-typography-list">
    <div class="form-group">
      <label class="control-label">Level 3 Menu Typography:</label> 
      {{tmpl($data,options ={before:"lv3"}) '#tmpl-typography'}}
    </div>
  </div>
  <div class="form-typography-list">
    <div class="form-group">
      <label class="control-label">Level 4 Menu Typography:</label> 
      {{tmpl($data,options ={before:"lv4"}) '#tmpl-typography'}}
    </div>
  </div>
  <div class="form-typography-list">
    <div class="form-group">
      <label class="control-label">Level 5 Menu Typography:</label> 
      {{tmpl($data,options ={before:"lv5"}) '#tmpl-typography'}}
    </div>
  </div>

    <div class="form-group colorpicker-box">
      <label for="firstname" class="control-label">Accent Color:</label>
      <select class="form-control" id="currentselect">
        <option value="">Theme Options</option>
        <option value="custom" {{if accent }} selected="selected" {{/if}}>Custom</option>
      </select>
      <div class="colorpicker-group conditional" data-condition="#currentselect == 'custom'">
        <input type="text" class="dnnOptions form-control colorpicker" data-color-format="rgba" data-name="accent" value="${accent}">
        <i style="background-color:${accent}"></i>
      </div>
    </div>
                       

 
</div>


</div>
