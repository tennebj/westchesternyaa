#parse( "component/template.register.ascx" )
#parse( "component/template.css.ascx" )
#parse( "component/template.pageloaders.ascx" )



<div class="dng-main mm-page mm-slideout ${xf.ViewGlobalSetting("ContainerLayout","Wide")}"  id="mm-content-box">
  <div id="dng-wrapper"> 

    #parse( "component/template.pagetitle.ascx" )

    #parse( "component/template.breadcrumb.ascx" )


    <div class="BannerPane" id="BannerPane" runat="server"></div>

    <section id="dnn_content">   


      #set($sidebarcol = ${xf.ViewGlobalSetting("SidebarWidth",2)} )
      #set($sidebarcolrtl = 12 - ${xf.ViewGlobalSetting("SidebarWidth",2)} )
      #set($SidebarPosition = ${xf.ViewGlobalSetting("SidebarPosition","left")} )
      #set($Sidebarenabled = ${xf.ViewGlobalSetting("Sidebarenabled","false")} )

      #if(${Sidebarenabled}=="true")
      <div class="container has-sidebar" data-sticky="parent">
      <div class="row" >
        #if(${SidebarPosition}=="left")
        <div class="col-md-${sidebarcol} sidebar_dynamic sidebar_dynamic_sytle01 widget_line sidebar_pos_left">
        <div class="sidebar_sticky" data-offset="30" #if(${xf.ViewGlobalSetting("SidebarFixed",false)})data-sticky="on"#end>
              #if(${xf.ViewGlobalSetting("displaySidebarMenu",true)})
              <div class="sidebar-menu">
                <dnn:LEFTGOMENU runat="server" id="dnnGOMENU6" Effect="HTML" ViewLevel="${xf.ViewGlobalSetting("SidebarMenulevel","0")}"  />
              </div>
              #end
                <div class="panebox SidebarPane" id="SidebarPane" runat="server"></div>
        </div></div>
        #end
     
      <div class="col-md-${sidebarcolrtl}">
      #end

      <div class="container">
        <div class="row">
          <div class="col-sm-12">
            <div class="ContentPane" id="ContentPane" runat="server"></div>
          </div>
        </div>
      </div>

     ${xf.ViewIncludeLayoutHTML("Content","ContentLayout",false)} 
    #if(${Sidebarenabled}=="true")
    </div>

    #if(${SidebarPosition}=="right")
    
    <div class="col-md-${sidebarcol} sidebar_dynamic sidebar_dynamic_sytle01 widget_line sidebar_pos_right">  
    <div class="sidebar_sticky" data-offset="30" #if(${xf.ViewGlobalSetting("SidebarFixed",false)})data-sticky="on"#end>   
        #if(${xf.ViewGlobalSetting("displaySidebarMenu",true)})
         <div class="sidebar-menu">
          <dnn:LEFTGOMENU runat="server" id="dnnGOMENU7" Effect="HTML" ViewLevel="${xf.ViewGlobalSetting("SidebarMenulevel","0")}"  /> 
        </div>
        #end
        <div class="panebox SidebarPane" id="SidebarPane" runat="server"></div>
    </div>
    </div>
  
    #end



    </div>
    </div>
    #end


    </section>

    #parse( "component/template.footer.ascx" )

    #parse( "component/template.widget.ascx" )
  </div>
</div>
  #parse( "component/template.script.ascx" )


