
    #if(${xf.ViewGlobalSetting("EnabledWidget","true")} == "true")  
      ${xf.ViewIncludeCSS("resource/css/backtop.css",9)}
      <ul class="fixed-widget-list">
        #if(${xf.ViewGlobalSetting("e_behance","true")} == "true")  
        <li class="behance"><a href="${xf.ViewGlobalSetting("e_behance_Url","#")}"><span>${xf.ViewGlobalSetting("e_behance_text","Twitter")}</span></a></li>
        #end
        #if(${xf.ViewGlobalSetting("e_dribbble","true")} == "true")  
        <li class="dribbble"><a href="${xf.ViewGlobalSetting("e_dribbble_Url","#")}"><span>${xf.ViewGlobalSetting("e_dribbble_text","Twitter")}</span></a></li>
        #end
        #if(${xf.ViewGlobalSetting("e_twitter","true")} == "true")  
        <li class="twitter"><a href="${xf.ViewGlobalSetting("e_twitter_Url","#")}"><span>${xf.ViewGlobalSetting("e_twitter_text","Twitter")}</span></a></li>
        #end
        #if(${xf.ViewGlobalSetting("e_youtube","true")} == "true")  
        <li class="youtube"><a href="${xf.ViewGlobalSetting("e_youtube_Url","#")}"><span>${xf.ViewGlobalSetting("e_youtube_text","YouTube")}</span></a></li>
        #end
        #if(${xf.ViewGlobalSetting("e_instagram","true")} == "true")  
        <li class="instagram"><a href="${xf.ViewGlobalSetting("e_instagram_Url","#")}"><span>${xf.ViewGlobalSetting("e_instagram_text","Instagram")}</span></a></li>
        #end
        #if(${xf.ViewGlobalSetting("e_vimeo","true")} == "true")  
        <li class="vimeo"><a href="${xf.ViewGlobalSetting("e_vimeo_Url","#")}"><span>${xf.ViewGlobalSetting("e_vimeo_text","Vimeo")}</span></a></li>
        #end
        #if(${xf.ViewGlobalSetting("e_pinterest","true")} == "true")  
        <li class="pinterest"><a href="${xf.ViewGlobalSetting("e_pinterest_Url","#")}"><span>${xf.ViewGlobalSetting("e_pinterest_text","Pinterest")}</span></a></li>
        #end
        #if(${xf.ViewGlobalSetting("e_linkedin","true")} == "true")  
        <li class="linkedin"><a href="${xf.ViewGlobalSetting("e_linkedin_Url","#")}"><span>${xf.ViewGlobalSetting("e_linkedin_text","Linkedin")}</span></a></li>
        #end
        #if(${xf.ViewGlobalSetting("e_RSS","true")} == "true")  
        <li class="rss"><a href="${xf.ViewGlobalSetting("e_RSS_Url","#")}"><span>${xf.ViewGlobalSetting("e_RSS_text","RSS")}</span></a></li>
        #end
        #if(${xf.ViewGlobalSetting("e_Facebook","true")} == "true")  
        <li class="facebook"><a href="${xf.ViewGlobalSetting("e_Facebook_Url","#")}"><span>${xf.ViewGlobalSetting("e_Facebook_text","Facebook")}</span></a></li>
        #end
        #if(${xf.ViewGlobalSetting("e_phone","true")} == "true")  
        <li class="phone"><a href="${xf.ViewGlobalSetting("e_phone_Url","#")}"><span>${xf.ViewGlobalSetting("e_phone_text","Phone")}</span></a></li>
        #end
        #if(${xf.ViewGlobalSetting("e_envelope","true")} == "true")  
        <li class="envelope"><a href="${xf.ViewGlobalSetting("e_envelope_Url","#")}"><span>${xf.ViewGlobalSetting("e_envelope_text","Envelope")}</span></a></li>
        #end
        #if(${xf.ViewGlobalSetting("e_share","true")} == "true")  
       <li class="share"><span>${xf.ViewGlobalSetting("e_share_text","Contact info")}</span></a></li>
       #end
       #if(${xf.ViewGlobalSetting("e_backtop","true")} == "true")  
       <li id="back-to-top" class="backtop"><span>${xf.ViewGlobalSetting("e_backtop_text","Back to top")}</span></a></li>
       #end
      </ul>
    #end 


