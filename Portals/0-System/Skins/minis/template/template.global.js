
var GlobalThemeOptions = {
    accent:"${xf.ViewGlobalSetting("AccentColour","#20a3f0")} ",
    accent2:"${xf.ViewGlobalSetting("AccentColour2","#9b61dc")}",
    accent3:"${xf.ViewGlobalSetting("AccentColour3","#22cae4")}",
    accent4:"${xf.ViewGlobalSetting("AccentColour4","#22d3a7")}",
};


#if(${xf.ViewGlobalSetting("bootstrap",false)}) 
/* bootstrap.min.js start*/
${xf.ReadFile("resource/vendor/bootstrap/bootstrap.min.js",0)} 
/* bootstrap.min.js end*/
#end

#if(${xf.ViewGlobalSetting("aos_min",false)}) 
/* aos.js start*/
${xf.ReadFile("resource/vendor/aos/aos.js",0)}
/* aos.js start*/
#end

#if(${xf.ViewGlobalSetting("megamenu",true)}) 
/* megamenu.js start*/
${xf.ReadFile("resource/vendor/megamenu/megamenu.js",0)}
/* megamenu.js start*/
#end


/** dnngo.js start*/
${xf.ReadFile("resource/js/dnngo.js",0)}
/** dnngo.js end*/
/** custom.js start*/
${xf.ReadFile("resource/js/custom.js",0)}
/** custom.js end*/
