#parse( "component/template.register.ascx" )
#parse( "component/template.css.ascx" )
#parse( "component/template.pageloaders.ascx" )

<div class="dng-main mm-page mm-slideout ${xf.ViewGlobalSetting("ContainerLayout","Wide")}" id="mm-content-box" data-page-type="onepage">
  <div id="dng-wrapper"> 
    #if(${xf.ViewPageSetting("HeaderAnchor",false)})
      <div class="anchorTag headerAnchorTag" id="${xf.ViewPageSetting("HeaderAnchorID","")}" data-title="${xf.ViewPageSetting("HeaderAnchorTitle","")}"></div>
    #end

    #parse( "component/template.pagetitle.ascx" )
    #parse( "component/template.breadcrumb.ascx" )


    <div class="BannerPane" id="BannerPane" runat="server"></div>
 
    <section id="dnn_content">   

    #set($sidebarcol = ${xf.ViewGlobalSetting("SidebarWidth",2)} )
    #set($sidebarcolrtl = 12 - ${xf.ViewGlobalSetting("SidebarWidth",2)} )
    
    #if( ${xf.ViewPageSetting("SidebarPosition","inherit")} == "inherit")
      #set($SidebarPosition = ${xf.ViewGlobalSetting("SidebarPosition","left")} )
    #else 
      #set($SidebarPosition = ${xf.ViewPageSetting("SidebarPosition","left")} )
    #end

    #if( ${xf.ViewPageSetting("Sidebarenabled","inherit")} == "inherit")
      #set($Sidebarenabled = ${xf.ViewGlobalSetting("Sidebarenabled","false")} )
    #elseif(${xf.ViewPageSetting("Sidebarenabled","inherit")} == "on")
      #set($Sidebarenabled = "true" )
    #else 
      #set($Sidebarenabled ="false")
    #end

    #if( ${xf.ViewPageSetting("displaySidebarMenu","inherit")} == "inherit")
      #set($displaySidebarMenu = ${xf.ViewGlobalSetting("displaySidebarMenu","false")} )
    #elseif(${xf.ViewPageSetting("displaySidebarMenu","inherit")} == "on")
      #set($displaySidebarMenu = "true" )
    #else 
      #set($displaySidebarMenu ="false")
    #end
 
 #if(${Sidebarenabled}=="true")

      <div class="container has-sidebar" data-sticky="parent">
      <div class="row" >
    #if(${SidebarPosition}=="left" )
    <div class="col-md-${sidebarcol} sidebar_dynamic sidebar_dynamic_sytle01 widget_line sidebar_pos_left">
     <div class="sidebar_sticky" data-offset="30" #if(${xf.ViewGlobalSetting("SidebarFixed",false)})data-sticky="on"#end>
           #if(${displaySidebarMenu} =="true")
          <div class="sidebar-menu">
            <dnn:LEFTGOMENU runat="server" id="dnnGOMENU6" Effect="HTML" ViewLevel="${xf.ViewGlobalSetting("SidebarMenulevel","0")}"  />
          </div>
          #end
            <div class="panebox SidebarPane" id="SidebarPane" runat="server"></div>
    </div></div>

    #end
     
      <div class="col-md-${sidebarcolrtl}">
      #end

      <div class="container">
        <div class="row">
          <div class="col-sm-12">
            <div class="ContentPane" id="ContentPane" runat="server"></div>
          </div>
        </div>
      </div>

  #if(${xf.ViewPageSetting("ContentLayout","Inherit")}!="Inherit" )
      ${xf.ViewIncludeLayoutHTML("Content","ContentLayout",true)} 
    #else
      ${xf.ViewIncludeLayoutHTML("Content","ContentLayout",false)} 
    #end

    #if(${Sidebarenabled}=="true")
    </div>

    #if(${SidebarPosition}=="right")
    
    <div class="col-md-${sidebarcol} sidebar_dynamic sidebar_dynamic_sytle01 widget_line sidebar_pos_right">  
    <div class="sidebar_sticky" data-offset="30" #if(${xf.ViewGlobalSetting("SidebarFixed",false)})data-sticky="on"#end>   
        #if(${displaySidebarMenu} =="true")
         <div class="sidebar-menu">
          <dnn:LEFTGOMENU runat="server" id="dnnGOMENU7" Effect="HTML" ViewLevel="${xf.ViewGlobalSetting("SidebarMenulevel","0")}"  />
        </div>
        #end
        <div class="panebox SidebarPane" id="SidebarPane" runat="server"></div>
    </div>
    </div>

    #end



    </div>
    </div>
    #end


    </section>
    #if(${xf.ViewPageSetting("FooterAnchor",false)})
      <div class="anchorTag footerAnchorTag" id="${xf.ViewPageSetting("FooterAnchorID","")}" data-title="${xf.ViewPageSetting("FooterAnchorTitle","")}"></div>
    #end

    #parse( "component/template.footer.ascx" )

    #parse( "component/template.widget.ascx" )

  </div>
</div>

<script>
  var OnePageOption={
    navigation:${xf.ViewPageSetting("Navigation","true")},
    navigation_position:"${xf.ViewPageSetting("NavigationPosition","right")}",
    navigation_style:"${xf.ViewPageSetting("NavigationStyle","style01")}",
    easing:"${xf.ViewPageSetting("easing","linear")}",
    duration:${xf.ViewPageSetting("duration","1200")},
  }
</script>          
<script type="text/javascript" src="${xf.SkinPath}resource/vendor/easing/jquery.easing.min.js"></script> 
<script type="text/javascript" src="${xf.SkinPath}resource/vendor/anchor/anchor.js"></script> 
          
#parse( "component/template.script.ascx" )

 

