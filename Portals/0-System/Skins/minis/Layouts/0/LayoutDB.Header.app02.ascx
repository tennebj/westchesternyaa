﻿<div class="header-replace header-position"><header class="header_bg header-wrap header-default floating-nav floating-nav-sticky"><div id="megamenuWidthBox" class="container"></div>  <div class="header-center-bg floating-dark-color header-bg-box" data-height="70"  id="headerrow-996254"><div class="container"><div class="header-center header-container-box"> <div class="header-area area-left header-horizontal  popup-menu-left-width header-horizontal"   id="column-778751"><div class="header-content-wrap"> <div class="popup-menu-wrap position-relative" id="popup-menu-wrap41931">
	<div class="menu-icon"><i class="l_1"></i><i class="l_2"></i><i class="l_3"></i></div>
	<div class="popup-menu" id="popup-menu41931">
		<div class="popup-menu-main">
			<div class="popup-menu-header">
			<div class="row align-items-center">
					<div class="col-auto">
						<div class="logo"><dnn:LOGO runat="server" id="dnnLOGO41931" BorderWidth="0" /></div>
					</div>
					<div class="col text-right">
					 <span class="size-18">Contact: +1 845-359-0545</span>
<a href="#" title="LEARN MORE" class="button-13 ml-30 color-white">FREE TRIAL <i class="sico lnr-arrow-right mr-0 ml-10"><svg><use xlink:href="#lnr-arrow-right"></use></svg></i></a>
	</div>
			</div>
			</div>
			<div class="container popup-menu-container">
				<div class="row align-items-center">
					<div class="col-sm-8">
						<dnn:LEFTGOMENU runat="server" id="dnnGOMENU41931" Effect="HTML" />
					</div>
					<div class="col-sm-4"><img src="/Portals/_default/ContentBuilder/minis-page/app02/app02-header-img.png" alt="" style="margin: 0 0 -73px -83px;">
<h3 class="color-accent">Have any issue?</h3>
<p class="mb-45">Don't worry, we are always here for you.</p>

<p><b>Phone:</b> (+86)12345678091</p>

<p><b>Email:</b> creative.design@gmail.com</p>

<p class="mb-45"><b>Address:</b> Yanta W Rd, Yanta District, Xian Shi, Shaanxi, China</p>

<h6>Work Time</h6>
<p>Monday-Friday: 8:00am-5:30pm</p>

<p>Saturday: 10:00am-2:00pm</p></div>
				</div>
			</div>
			<div class="pagetitle">
				<%= IIf(String.IsNullOrEmpty( PortalSettings.ActiveTab.Title), PortalSettings.ActiveTab.TabName, PortalSettings.ActiveTab.Title)%>
			</div>
		</div>
	</div>
</div>
	<dnn:DnnCssInclude runat="server" FilePath="resource/header/popup-menu/popup-menu.css" PathNameAlias="SkinPath" Priority="321" HtmlAttributesAsString="media:'none',title:'all',onload:'this.media=this.title"  />
	<dnn:DnnJsInclude runat="server" FilePath="resource/header/popup-menu/popup-menu.js" ForceProvider ="DnnFormBottomProvider" PathNameAlias="SkinPath" Priority="9" HtmlAttributesAsString="async:'async'"   />	

	</div></div> <div class="header-area area-center header-vertical text-left header-vertical text-left"   id="column-267462"><div class="header-content-wrap"> <div class="logo" id="logo503464"><dnn:LOGO runat="server" id="dnnLOGO503464" cssClass="header-logo floating-header-logo" BorderWidth="0" /></div></div></div> <div class="header-area area-right header-horizontal  pr-lg-30 header-horizontal"   id="column-254407"><div class="header-content-wrap"> <div class="dngTextBox"><span class="size-18">Contact: +1 845-359-0545</span>
<a href="#" title="LEARN MORE" class="button-13 ml-30 color-white">FREE TRIAL <i class="sico lnr-arrow-right mr-0 ml-10"><svg><use xlink:href="#lnr-arrow-right"></use></svg></i></a>
</div></div></div> </div></div></div>  </header></div><div class="mobile-header-replace"><header class="mobile-header floating-nav"> <div class="header-mobile-bg mobile-header-shadow" id="mobileheader-820367"><div class="header-mobile"> <div class="header-area area-left header-horizontal text-left header-horizontal text-left"   id="column-155780"><div class="header-content-wrap"> <div class="search-popup-box" id="search-popup-box337483" placeholder="Enter any key words to search"><div class="icon hover-accent"><span class="magnifier"><i></i></span></div><div class="popup-reveal color-dark "></div><div class="popup-content color-dark bg-accent"><div class="popup-close hover-accent"><span class="icon-cross"></span></div><div class="popup-center"><dnn:SEARCH runat="server" id="dnnSEARCH337483" CssClass="search"  ShowSite="False" ShowWeb="False"  submit=" " /></div></div></div>
		<dnn:DnnCssInclude runat="server" FilePath="resource/header/search-popup/search-popup.css" PathNameAlias="SkinPath" Priority="318" HtmlAttributesAsString="media:'none',title:'all',onload:'this.media=this.title"  />
		<dnn:DnnJsInclude runat="server" FilePath="resource/header/search-popup/search-popup.js"  ForceProvider ="DnnFormBottomProvider" PathNameAlias="SkinPath" Priority="9" HtmlAttributesAsString="async:'async'"   />	
		</div></div> <div class="header-area area-center header-horizontal  header-horizontal"   id="column-582314"><div class="header-content-wrap"> <div class="logo" id="logo47227"><dnn:LOGO runat="server" id="dnnLOGO47227" cssClass="header-logo floating-header-logo" BorderWidth="0" /></div></div></div> <div class="header-area area-right header-horizontal text-right header-horizontal text-right"   id="column-787817"><div class="header-content-wrap"> <div class="mobile-menu-icon"><span class="dg-menu-anime"><i></i></span></div>
	<div class="mobilemenu-main" id="mobilemenu98903" data-title="Menu"><span class="mobileclose"></span>
	<div class="m-menu">
	<dnn:LEFTGOMENU runat="server" id="MOBILEMENU98903" Effect="HTML"/> 
	</div>
	</div>
	<dnn:DnnCssInclude runat="server" FilePath="resource/header/mobilemenu/mobilemenu.css" PathNameAlias="SkinPath" Priority="317" HtmlAttributesAsString="media:'none',title:'all',onload:'this.media=this.title"  />
	<dnn:DnnJsInclude runat="server" FilePath="resource/header/mobilemenu/mobilemenu.js" ForceProvider ="DnnFormBottomProvider" PathNameAlias="SkinPath" Priority="17" HtmlAttributesAsString="async:'async'"   />	
	</div></div> </div></div> </header></div>