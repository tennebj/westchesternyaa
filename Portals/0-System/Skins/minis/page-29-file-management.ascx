<%@ Control language="vb" AutoEventWireup="false" Explicit="True" Inherits="DotNetNuke.UI.Skins.Skin" %>
<%@ Register TagPrefix="dnn" TagName="LOGO" Src="~/Admin/Skins/Logo.ascx" %>
<%@ Register TagPrefix="dnn" TagName="SEARCH" Src="~/Admin/Skins/Search.ascx" %>
<%@ Register TagPrefix="dnn" TagName="GOMENU" Src="~/DesktopModules/DNNGo_ThemePluginPro/Skin_MultiMenu.ascx" %>
<%@ Register TagPrefix="dnn" TagName="MOBILEMENU" Src="~/DesktopModules/DNNGo_ThemePluginPro/Skin_Menus.ascx" %>
<%@ Register TagPrefix="dnn" TagName="MEGAMENU" Src="~/DesktopModules/DNNGo_ThemePluginPro/Skin_Megamenu.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LEFTGOMENU" Src="~/DesktopModules/DNNGo_ThemePluginPro/Skin_Html.ascx" %>
<%@ Register TagPrefix="dnn" TagName="BACKGROUND" Src="~/DesktopModules/DNNGo_ThemePluginPro/Skin_Background.ascx" %>
<%@ Register TagPrefix="dnn" TagName="BREADCRUMBBG" Src="~/DesktopModules/DNNGo_ThemePluginPro/Skin_Background.ascx" %>
<%@ Register TagPrefix="dnn" TagName="USERANDLOGIN" Src="~/Admin/Skins/UserAndLogin.ascx" %>
<%@ Register TagPrefix="dnn" TagName="login" Src="~/Admin/Skins/login.ascx" %>
<%@ Register TagPrefix="dnn" TagName="USER" Src="~/Admin/Skins/User.ascx" %> 
<%@ Register TagPrefix="dnn" TagName="COPYRIGHT" Src="~/Admin/Skins/Copyright.ascx" %>
<%@ Register TagPrefix="dnn" TagName="PRIVACY" Src="~/Admin/Skins/Privacy.ascx" %>
<%@ Register TagPrefix="dnn" TagName="TERMS" Src="~/Admin/Skins/Terms.ascx" %>
<%@ Register TagPrefix="dnn" TagName="STYLES" Src="~/Admin/Skins/Styles.ascx" %>
<%@ Register TagPrefix="dnn" TagName="Meta" Src="~/Admin/Skins/Meta.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LANGUAGE" Src="~/Admin/Skins/Language.ascx" %>
<%@ Register TagPrefix="dnn" TagName="SKINPLUGIN" Src="~/DesktopModules/DNNGo_ThemePluginPro/Skin_Plugin.ascx" %>
<%@ Register TagPrefix="dnn" Namespace="DotNetNuke.Web.Client.ClientResourceManagement" Assembly="DotNetNuke.Web.Client" %>
<%@ Register TagPrefix="dnn" TagName="LINKS" Src="~/Admin/Skins/Links.ascx" %>
<%@ Register TagPrefix="dnn" TagName="BREADCRUMB" Src="~/Admin/Skins/BreadCrumb.ascx" %>
<%@ Register TagPrefix="dnn" TagName="DNNGO_CONTENTBUILDER" Src="~/DesktopModules/DNNGo_ThemePluginPro/Skin_ContentBuilder.ascx" %>
<%@ Register TagPrefix="dnn" TagName="jQuery" Src="~/Admin/Skins/jQuery.ascx" %>
<dnn:jQuery runat="server" />

<dnn:DNNGO_CONTENTBUILDER runat="server" id="DNNGO_CONTENTBUILDER1" />

<dnn:SKINPLUGIN runat="server" id="SKINPLUGIN1" />
<dnn:Meta runat="server" Name="viewport" Content="width=device-width,height=device-height, minimum-scale=1.0, maximum-scale=1.0" />
<dnn:Meta runat="server" http-equiv="X-UA-Compatible" content="IE=edge" />
<dnn:Meta runat="server" name="format-detection" content="telephone=no" />
<dnn:DnnCssInclude ID="ViewGoogleFontLink" runat="server" FilePath="https://fonts.googleapis.com/css?family=Muli:300,regular,600,700,900|Anton:regular|ABeeZee&display=swap&subset=latin" Priority="9"  /> 

<script>var userAgent = navigator.userAgent;if((userAgent.indexOf("compatible") > -1 && userAgent.indexOf("MSIE") > -1) || (userAgent.indexOf('Trident') > -1 && userAgent.indexOf("rv:11.0") > -1)){document.getElementsByTagName('html')[0].className += 'ie-browser'; };</script>






<!--Layout-->
<dnn:DnnCssInclude ID="layoutdb_header_default" runat="server" FilePath="Layouts/0/LayoutDB.Header.default.css" PathNameAlias="SkinPath" Priority="10"  />

<dnn:DnnCssInclude ID="layoutdb_content_meetingdetails" runat="server" FilePath="Layouts/0/LayoutDB.Content.meetingdetails.css" PathNameAlias="SkinPath" Priority="10"  />

<dnn:DnnCssInclude ID="layoutdb_footer_default" runat="server" FilePath="Layouts/0/LayoutDB.Footer.default.css" PathNameAlias="SkinPath" Priority="10"  />

<!--bootstrap-->
<dnn:DnnCssInclude ID="bootstrap" runat="server" FilePath="resource/vendor/bootstrap/bootstrap.css" PathNameAlias="SkinPath" Priority="9"  />
<!--aos-->

<!--Theme-->
<dnn:DnnCssInclude ID="jquery_mmenu" runat="server" FilePath="resource/css/jquery.mmenu.css" PathNameAlias="SkinPath" Priority="9"  />

 
<dnn:DnnCssInclude ID="header" runat="server" FilePath="resource/css/header.css" PathNameAlias="SkinPath" Priority="9"  /> 

 
<dnn:DnnCssInclude ID="theme" runat="server" FilePath="resource/css/theme.css" PathNameAlias="SkinPath" Priority="9"  />
<dnn:DnnCssInclude ID="global_0" runat="server" FilePath="global-0.css" PathNameAlias="SkinPath" Priority="18"  />



     <dnn:DnnCssInclude ID="pageloaders" runat="server" FilePath="resource/css/loaders.css" PathNameAlias="SkinPath" Priority="10"  />
    <div class="page-loaders" style="background-color:#000D68;color:#ffffff;">
        <div id="loading-center">
            <div id="loading-center-absolute">
                <div class="object" id="object_one"></div>
                <div class="object" id="object_two"></div>
                <div class="object" id="object_three"></div>

            </div>
        </div>

    </div>



<div class="dng-main mm-page mm-slideout Wide"  id="mm-content-box">
  <div id="dng-wrapper"> 


     
    
          	 <!--#include file="Layouts/0/LayoutDB.Header.default.ascx"-->
          
    

    <div class="PageTitleFullPane" id="PageTitleFullPane" runat="server"></div>


        



    <div class="BannerPane" id="BannerPane" runat="server"></div>
 
    <section id="dnn_content">   

        
        
        
         
 
      <div class="container">
        <div class="row">
          <div class="col-sm-12">
            <div class="ContentPane" id="ContentPane" runat="server"></div>
          </div>
        </div>
      </div>

        <!--#include file="Layouts/0/LayoutDB.Content.meetingdetails.ascx"--> 
    
    

    </section>

            <footer class="footer"> 
         <!--#include file="Layouts/0/LayoutDB.Footer.default.ascx"-->
        </footer>
    

    
    


  </div>
</div>
    <div data-path="/Portals/0-System/Skins/minis/" data-mapapi="" id="SkinPath"></div>
<script type="text/javascript" src="/Portals/0-System/Skins/minis/global-0.js"></script> 




