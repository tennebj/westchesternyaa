﻿$(document).ready(function () {
    $('#menuToggle').click(function () {
        $(this).toggleClass('active');
        $('nav').toggleClass('open');
        $('.dropdown').removeClass('open');
        return false;
    });
    $('.dropdown > b').click(function () {
        $(this).parent('li').toggleClass('open');
        return false;
    });
});