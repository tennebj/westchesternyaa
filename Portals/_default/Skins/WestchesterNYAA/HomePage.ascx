﻿<%@ Control language="C#" AutoEventWireup="false" Explicit="True" Inherits="DotNetNuke.UI.Skins.Skin" %>
<%@ Register TagPrefix="dnn" Namespace="DotNetNuke.Web.Client.ClientResourceManagement" Assembly="DotNetNuke.Web.Client" %>
<%@ Register TagPrefix="dnn" TagName="Meta" Src="~/Admin/Skins/Meta.ascx" %>
<%@ Register TagPrefix="dnn" TagName="MENU" src="~/DesktopModules/DDRMenu/Menu.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LOGO" Src="~/Admin/Skins/Logo.ascx" %>
<%@ Register TagPrefix="dnn" TagName="USER" Src="~/Admin/Skins/User.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LOGIN" Src="~/Admin/Skins/Login.ascx" %>
<%@ Register TagPrefix="dnn" TagName="PRIVACY" Src="~/Admin/Skins/Privacy.ascx" %>
<%@ Register TagPrefix="dnn" TagName="TERMS" Src="~/Admin/Skins/Terms.ascx" %>
<%@ Register TagPrefix="dnn" TagName="COPYRIGHT" Src="~/Admin/Skins/Copyright.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LANGUAGE" Src="~/Admin/Skins/Language.ascx" %>
<%@ Register TagPrefix="dnn" TagName="SEARCH" Src="~/Admin/Skins/Search.ascx" %>
<%@ Register TagPrefix="dnn" TagName="BREADCRUMB" Src="~/Admin/Skins/BreadCrumb.ascx" %>
<%@ Register Src="~/admin/Skins/JavaScriptLibraryInclude.ascx" TagPrefix="ddr" TagName="JavaScriptLibraryInclude" %>

<dnn:Meta runat="server" Name="viewport" Content="initial-scale=1.0,width=device-width" /> 


<dnn:DnnCssInclude ID="DnnCssInclude1" runat="server" PathNameAlias="SkinPath" FilePath="css/homeDesktopHd.css" />
<dnn:DnnCssInclude ID="DnnCssInclude2" runat="server" PathNameAlias="SkinPath" FilePath="css/fonts/BeVietnam-Medium.ttf" />

<html>
    <head title="Westchester AA GSA">
         <meta content="width=1440, maximum-scale=1.0" name="viewport"/>
    </head>

   <body style="margin: 0;  background: rgba(255, 255, 255, 1.0);">
        <div class="homedesktophd">
            <div style="width: 1440px; height: 100%; position:relative; margin:auto;">
                <div class="bottomnavigation">
                    <div class="bottomnavigation1">
                        <div class="rectangle">
                        </div>
                        <div class="privacypolicy">
                            Privacy Policy
                        </div>
                        <div class="termsconditions">
                            Terms &amp; Conditions
                        </div>
                        <div class="termsconditionscopy">
                            FAQs
                        </div>
                        <div class="a251copyright2019wes">
                            © Copyright 2019 Westfchester GSA <br />All Rights Reserved.
                        </div>
                    </div>
                </div>
                <div class="footer">
                    <div class="footerstayconnected">
                        <div class="rectangle">
                        </div>
                        <div class="inputentered">
                            <div class="inputentered1">
                                <img http://<%=PortalSettings.PortalAlias.HTTPAlias%>/" src="<%=SkinPath%>img/get-involved---public-information-desktop-hd---copy-input.png"/>
                                <div class="inputlabel">
                                    Input Label
                                </div>
                                <div class="inputtext">
                                    Input Text
                                </div>
                            </div>
                        </div>
                        <div class="stayconnected">
                            STAY CONNECTED
                        </div>
                        <div class="signuptothewestc">
                            Sign-up to the Westchester GSA Newsletter for updates about area events and resources.
                        </div>
                    </div>
                </div>
                <div class="topnavigation">
                    <div class="navigationbar">
            
                    <div class="findameeting">
                  <dnn:MENU ID="MainMenu" MenuStyle="DDRNavPrimary" runat="server" />
                        </div>
                         
                <img src="<%=SkinPath%>img/meeting-page-desktop-hd-gsaaalogo-01@2x.png" class="gsaaalogo01" />
                  <a href="donatepagedesktophd.html">
                    <div class="donatebutton">
                        <div class="donatebutton1">
                            <div class="donate">
                                Donate
                            </div>
                            <div class="rectangle">
                            </div>
                        </div>
                    </div>
                </a>                        
            </div>
                    </div>
                    <div class="container">
                     <div id="ContentPane" runat="server"></div>
                     </div>
                      <div class="container">
                     <div id="ContentPane2" runat="server"></div>
                     </div>
                </div>
        </div>
    </body>
</html>
