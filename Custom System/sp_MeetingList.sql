USE [WestchesterGSA]
GO
/****** Object:  StoredProcedure [dbo].[sp_MeetingList]    Script Date: 5/31/2020 7:59:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[sp_MeetingList]
	-- Add the parameters for the stored procedure here
@DayofWeek varchar(15) = NULL,
@City varchar(50) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @SQL nvarchar(max)

	set @SQL = 
	N'SELECT ml.[Meeting_ID],FORMAT(CAST(m.Time AS datetime2), N'hh:mm tt') AS [Time]  ,[MeetingName] as Meeting ,[Location],[Address1] as Address
	      ,[City] as Region  ,[Address2]  ,[ZipCode]  ,[Notes]  ,[WheelchairAccessible] ,m.Day FROM [dbo].[MeetingLocation] ml  inner join meeting m on m.Meeting_ID = ml.Meeting_ID where 1 = 1 AND '


IF @DayofWeek <> 'Any Day'
 BEGIN
	SET @SQL = @SQL+ '  where m.Day = @DayofWeek and ml.City = @City'
 END



 SET @SQL = @SQL+ ' order by time'

 	EXEC sp_executesql @SQL,N'@DayofWeek varchar(15) = NULL
@City varchar(50) = NULL'
    ,@DayofWeek= @DayofWeek
,	@City = @City

END

